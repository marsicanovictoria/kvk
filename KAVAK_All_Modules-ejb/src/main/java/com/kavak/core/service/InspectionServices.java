package com.kavak.core.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.kavak.core.dto.CarFeatureCategoryDTO;
import com.kavak.core.dto.DimpleDTO;
import com.kavak.core.dto.InspectionCancelationReasonDTO;
import com.kavak.core.dto.InspectionCarPhotoTypeDTO;
import com.kavak.core.dto.InspectionDimpleDTO;
import com.kavak.core.dto.InspectionDiscountDTO;
import com.kavak.core.dto.InspectionDiscountTypeDTO;
import com.kavak.core.dto.InspectionOverviewDTO;
import com.kavak.core.dto.InspectionPointCarDTO;
import com.kavak.core.dto.InspectionPointCategoryDTO;
import com.kavak.core.dto.InspectionPointDTO;
import com.kavak.core.dto.InspectionPointDetailDTO;
import com.kavak.core.dto.InspectionPointSubCategoryDTO;
import com.kavak.core.dto.InspectionRepairDTO;
import com.kavak.core.dto.InspectionSummaryDTO;
import com.kavak.core.dto.InspectionsOverviewsDataDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.OfferCheckpointDTO;
import com.kavak.core.dto.PostInspectionPointsDetailRequestDTO;
import com.kavak.core.dto.RepairCategoryDTO;
import com.kavak.core.dto.request.PutInspectionOverviewRequestDTO;
import com.kavak.core.dto.response.GetFeaturesResponseDTO;
import com.kavak.core.dto.response.GetInspectionsOverviewsResponseDTO;
import com.kavak.core.dto.response.PostInspectionOverviewResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.InspectionTypeEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.enumeration.OfferTypeEnum;
import com.kavak.core.model.CarFeatureCategory;
import com.kavak.core.model.DimpleType;
import com.kavak.core.model.Inspection;
import com.kavak.core.model.InspectionCancelationReason;
import com.kavak.core.model.InspectionCarFeature;
import com.kavak.core.model.InspectionCarPhotoType;
import com.kavak.core.model.InspectionDimple;
import com.kavak.core.model.InspectionDiscount;
import com.kavak.core.model.InspectionDiscountType;
import com.kavak.core.model.InspectionOffer;
import com.kavak.core.model.InspectionPoint;
import com.kavak.core.model.InspectionPointCar;
import com.kavak.core.model.InspectionPointCategory;
import com.kavak.core.model.InspectionPointSubCategory;
import com.kavak.core.model.InspectionRepair;
import com.kavak.core.model.InspectionScheduleBlock;
import com.kavak.core.model.InspectionSummary;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.OfferCheckpoint;
import com.kavak.core.model.RepairCategory;
import com.kavak.core.repositories.CarFeatureCategoryRepository;
import com.kavak.core.repositories.DimpleRepository;
import com.kavak.core.repositories.DimpleTypeRepository;
import com.kavak.core.repositories.InspectionCancelationReasonRepository;
import com.kavak.core.repositories.InspectionCarPhotoTypeRepository;
import com.kavak.core.repositories.InspectionDimpleRepository;
import com.kavak.core.repositories.InspectionDiscountRepository;
import com.kavak.core.repositories.InspectionDiscountTypeRepository;
import com.kavak.core.repositories.InspectionOfferRepository;
import com.kavak.core.repositories.InspectionPointCarRepository;
import com.kavak.core.repositories.InspectionPointRepository;
import com.kavak.core.repositories.InspectionRepairRepository;
import com.kavak.core.repositories.InspectionRepository;
import com.kavak.core.repositories.InspectionScheduleBlockRepository;
import com.kavak.core.repositories.InspectionSummaryRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.OfferCheckpointRepository;
import com.kavak.core.repositories.RepairCategoryRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

@Stateless
public class InspectionServices {

    private List<MessageDTO> listMessages;

    @Inject
    private OfferCheckpointRepository offerCheckpointRepo;

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private DimpleRepository dimpleRepo;

    @Inject
    private RepairCategoryRepository repairCategoryRepo;

    @Inject
    private InspectionRepository inspectionRepo;

    @Inject
    private InspectionOfferRepository inspectionOfferRepo;

    @Inject
    private InspectionDiscountRepository inspectionDiscountRepo;

    @Inject
    private InspectionPointCarRepository inspectionPointCarRepo;

    @Inject
    private InspectionDimpleRepository inspectionDimpleRepo;

    @Inject
    private DimpleTypeRepository dimpleTypeRepo;

    @Inject
    private InspectionRepairRepository inspectionRepairRepo;

    @Inject
    private InspectionCarPhotoTypeRepository inspectionCarPhotoTypeRepo;

    @Inject
    private CarFeatureCategoryRepository carFeatureCategoryRepo;

    @Inject
    private InspectionSummaryRepository inspectionSummaryRepo;

//    @Inject
//    private SellCarDetailRepository sellCarDetailRepo;
//
//    @Inject
//    private UserRepository userRepo;
//
//    @Inject
//    private InspectionCarPhotoRepository inspectionCarPhotoRepo;
//
//    @Inject
//    private OfferTypeRepository offerTypeRepo;
//
//    @Inject
//    private SellCarMetaRepository sellCarMetaRepo;
//
//    @Inject
//    private CarDataRepository carDataRepo;
//
//    @Inject
//    private InspectionCarFeatureRepository inspectionCarFeatureRepo;
//
//    @Inject
//    private SellCarDimpleRepository sellCarDimpleRepo;
//
//    @Inject
//    private ApoloInspectionPointCarRepository apoloInspectionPointCarRepo;
//
//    @Inject
//    private ApoloInspectionPointsItemsRepository apoloInspectionPointsItemsRepo;
//
//    @Inject
//    private ApoloInspectionPointsHistoryRepository apoloInspectionPointsHistoryRepo;
//
//    @Inject
//    private ApoloInspectionPointsHistoryCarRepository apoloInspectionPointsHistoryCarRepo;
//
//    @Inject
//    private CarWarrantyPriceRepository carWarrantyPriceRepo;
//
//    @Inject
//    private SellCarWarrantyRepository sellCarWarrantyRepo;

    @Inject
    private InspectionCancelationReasonRepository inspectionCancelationReasonRepo;

    @Inject
    private InspectionDiscountTypeRepository inspectionDiscountTypeRepo;

    @Inject
    private InspectionScheduleBlockRepository inspectionScheduleBlockRepo;

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private InspectionPointRepository inspectionPointRepo;

//    @Inject
//    private InspectionSummaryOfferRepository inspectionSummaryOfferRepo;

    /**
     * Consulta los datos de una inspeccion
     * 
     * @author Enrique Marin
     * @param idsOfferCheckpoint
     *            identificar de la Inspeccion
     * @return InspectionDTO
     */
    public ResponseDTO getInspectionsOverviews(List<String> idsOfferCheckpoint) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getInspectionsOverviews) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	GetInspectionsOverviewsResponseDTO getInspectionsOverviewsResponseDTO = new GetInspectionsOverviewsResponseDTO();
	;
	List<InspectionsOverviewsDataDTO> listInspectionsOverviewsDataDTO = new ArrayList<>();
	for (String idOfferCheckpointActual : idsOfferCheckpoint) {
	    InspectionsOverviewsDataDTO inspectionsOverviewsDataDTO = new InspectionsOverviewsDataDTO();
	    Inspection inspectionBD = inspectionRepo.findByIdOffertCheckPoint(Long.valueOf(idOfferCheckpointActual));
	    if (inspectionBD != null) {
		List<InspectionPointCar> listInspectionPointCarBD = inspectionPointCarRepo.findByIdInspection(inspectionBD.getId());
		inspectionsOverviewsDataDTO.setIdInspection(inspectionBD.getId());
		if (listInspectionPointCarBD != null && !listInspectionPointCarBD.isEmpty()) {
		    inspectionsOverviewsDataDTO.setInspectionInitiated(true);
		} else {
		    inspectionsOverviewsDataDTO.setInspectionInitiated(false);
		}
	    } else {
		inspectionsOverviewsDataDTO.setIdInspection(null);
		inspectionsOverviewsDataDTO.setInspectionInitiated(false);
	    }

	    Optional<OfferCheckpoint> offerCheckpointActual = offerCheckpointRepo.findById(Long.valueOf(idOfferCheckpointActual));
	    OfferCheckpointDTO offerCheckpointDTO = offerCheckpointActual.get().getDTO();
	    offerCheckpointDTO.setCustomerPhone(offerCheckpointActual.get().getUser().getDTO().getPhone());
	    offerCheckpointDTO.setCustomerAddress(offerCheckpointActual.get().getUser().getDTO().getAddress());
	    Optional<MetaValue> metaValueBD = metaValueRepo.findById(offerCheckpointActual.get().getOfferType());
	    offerCheckpointDTO.setSelectedOffer(metaValueBD.get().getOptionName());
	    inspectionsOverviewsDataDTO.setOfferCheckpointDTO(offerCheckpointDTO);
	    listInspectionsOverviewsDataDTO.add(inspectionsOverviewsDataDTO);
	}

	getInspectionsOverviewsResponseDTO.setListInspectionsOverviewsDataDTO(listInspectionsOverviewsDataDTO);
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(getInspectionsOverviewsResponseDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getInspectionsOverviews) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Consulta la lista de puntos de inspecccion agrupados por categoria
     * 
     * @author Enrique Marin
     *
     */
    public ResponseDTO getInspectionPointsCategories(Long inspectionTypeId, Long inspectionId) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getInspectionPointsCategories) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	List<Long> listInspectionPointTypeIds = new ArrayList<>();
	Integer maxCountQuestionType1 = 0;
	Integer totalCountQuestionType1 = 0;
	List<Long> listQuestionMandatories = new ArrayList<>();
	if (inspectionTypeId != null) {
	    switch (inspectionTypeId.intValue()) {
	    case 1:
		listInspectionPointTypeIds.add(1L);
		listInspectionPointTypeIds.add(2L);
		listInspectionPointTypeIds.add(3L);
		break;
	    case 2:
		listInspectionPointTypeIds.add(1L);
		listInspectionPointTypeIds.add(2L);
		listInspectionPointTypeIds.add(3L);
		listInspectionPointTypeIds.add(4L);
		break;
	    case 3:
		listInspectionPointTypeIds.add(1L);
		listInspectionPointTypeIds.add(2L);
		listInspectionPointTypeIds.add(3L);
		listInspectionPointTypeIds.add(4L);
		break;
	    case 4:
		listInspectionPointTypeIds.add(3L);
		listInspectionPointTypeIds.add(5L);
		listInspectionPointTypeIds.add(6L);
		break;
	    }
	} else {
	    listInspectionPointTypeIds.add(1L);
	}
	List<InspectionPointCategory> listInspectionPointOrderByCategories = new ArrayList<>();

	List<InspectionPoint> listInspectionPointBD = inspectionPointRepo.findByInspectionPointTypeIds(listInspectionPointTypeIds);
	HashMap<String, List<InspectionPointCategory>> mapInspectionPointByCategories = new HashMap<>();
	String nameCategory;
	InspectionPointCategory inspectionPointCategory;
	for (InspectionPoint inspectionPoint : listInspectionPointBD) {
	    if (inspectionPoint.getInspectionPointCategory() == null) {
		inspectionPointCategory = inspectionPoint.getInspectionPointSubCategory().getInspectionPointCategory();
		nameCategory = inspectionPointCategory.getName();
	    } else {
		nameCategory = inspectionPoint.getInspectionPointCategory().getName();
		inspectionPointCategory = inspectionPoint.getInspectionPointCategory();
	    }
	    if (mapInspectionPointByCategories.get(nameCategory) == null) {
		List<InspectionPointCategory> listInspectionPointNewCategory = new ArrayList<>();
		listInspectionPointNewCategory.add(inspectionPointCategory);
		mapInspectionPointByCategories.put(nameCategory, listInspectionPointNewCategory);
	    } else {
		mapInspectionPointByCategories.get(nameCategory).add(inspectionPoint.getInspectionPointCategory());
	    }
	}
	List<String> listName = new ArrayList<>();

	for (String keyCategoryName : mapInspectionPointByCategories.keySet()) {
	    if (!listName.contains(keyCategoryName)) {
		listName.add(keyCategoryName);
		listInspectionPointOrderByCategories.add(mapInspectionPointByCategories.get(keyCategoryName).get(0));
	    }
	}

	List<InspectionPointCategoryDTO> listInspectionPointCategoryDTO = new ArrayList<>();
	List<InspectionPointCategoryDTO> listFinalInspectionPointCategoryDTO = new ArrayList<>();

	// Se busca Todos los puntos de inspeccion
	if (inspectionId == null) {
	    MetaValue metaValueBD = metaValueRepo.findByAlias("PIRNDPTS");
	    maxCountQuestionType1 = Integer.valueOf(metaValueBD.getOptionName());
	    LogService.logger.info(Constants.LOG_EXECUTING_START + " (getInspectionPointsCategories) - Se busca Todos los puntos de inspeccion");
	    for (InspectionPointCategory inspectionPointCategoryActual : listInspectionPointOrderByCategories) {
		InspectionPointCategoryDTO inspectionPointCategoryDTO = new InspectionPointCategoryDTO();
		inspectionPointCategoryDTO.setId(inspectionPointCategoryActual.getId());
		inspectionPointCategoryDTO.setName(inspectionPointCategoryActual.getName());
		inspectionPointCategoryDTO.setActive(inspectionPointCategoryActual.isActive());

		List<InspectionPointDTO> listInspectionPointDTO = new ArrayList<>();
		List<InspectionPointSubCategoryDTO> listInspectionPointSubCategoryDTO = new ArrayList<>();

		if (inspectionPointCategoryActual.getListInspectionPoint() != null && !inspectionPointCategoryActual.getListInspectionPoint().isEmpty()) {
		    for (InspectionPoint inspectionPointActual : inspectionPointCategoryActual.getListInspectionPoint()) {
			InspectionPointDTO inspectionPointDTO = new InspectionPointDTO();
			inspectionPointDTO.setId(inspectionPointActual.getId());
			inspectionPointDTO.setName(inspectionPointActual.getName());
			inspectionPointDTO.setIdFeature(inspectionPointActual.getIdFeature());
			inspectionPointDTO.setFeature(inspectionPointActual.isFeature());
			inspectionPointDTO.setActive(inspectionPointActual.isActive());
			inspectionPointDTO.setReprovesInspection(inspectionPointActual.isReprovesInspection());
			inspectionPointDTO.setAvailableRepairs(inspectionPointActual.getAvailableRepairs());
			inspectionPointDTO.setPhotoRequired(inspectionPointActual.isPhotoRequired());
			inspectionPointDTO.setNaButtonEnabled(inspectionPointActual.isNaButtonEnabled());
			inspectionPointDTO.setStandartButtonEnabled(inspectionPointActual.isStandarButtonEnabled());
			inspectionPointDTO.setRepairButtonEnabled(inspectionPointActual.isRepairButtonEnabled());
			inspectionPointDTO.setReprovedButtonEnabled(inspectionPointActual.isReprovedButtonEnabled());
			inspectionPointDTO.setShowAdditionalInfo(inspectionPointActual.isShowAdditionalInfo());
			inspectionPointDTO.setShowAdditionalInfoKey(inspectionPointActual.getShowAdditionalInfoKey());
			inspectionPointDTO.setCaptureAdditionalInfo(inspectionPointActual.isCaptureAdditionalInfo());
			inspectionPointDTO.setCaptureAdditionalInfoKey(inspectionPointActual.getCaptureAdditionalInfoKey());
			inspectionPointDTO.setGenerateDiscount(inspectionPointActual.isGenerateDiscount());
			inspectionPointDTO.setGenerateSummaryComment(inspectionPointActual.isGenerateSummaryComment());
			inspectionPointDTO.setGenerateEmailAlert(inspectionPointActual.isGenerateEmailAlert());
			inspectionPointDTO.setDiscountTypeId(inspectionPointActual.getDiscountTypeId());
			inspectionPointDTO.setInspectionPointTypeId(inspectionPointActual.getInspectionPointTypeId());

			if (inspectionPointActual.getInspectionPointTypeId() == 1) {
			    totalCountQuestionType1++;
			}

			listInspectionPointDTO.add(inspectionPointDTO);
		    }

		}
		inspectionPointCategoryDTO.setListInspectionPointDTO(listInspectionPointDTO);

		if (inspectionPointCategoryActual.getListInspectionPointSubCategory() != null && !inspectionPointCategoryActual.getListInspectionPointSubCategory().isEmpty()) {
		    for (InspectionPointSubCategory inspectionPointSubCategoryActual : inspectionPointCategoryActual.getListInspectionPointSubCategory()) {
			InspectionPointSubCategoryDTO inspectionPointSubCategoryDTO = new InspectionPointSubCategoryDTO();
			inspectionPointSubCategoryDTO.setId(inspectionPointSubCategoryActual.getId());
			inspectionPointSubCategoryDTO.setName(inspectionPointSubCategoryActual.getName());
			inspectionPointSubCategoryDTO.setActive(inspectionPointSubCategoryActual.isActive());
			List<InspectionPointDTO> listSubCategoryInspectionPointDTO = new ArrayList<>();

			for (InspectionPoint categoryInspectionPointActual : inspectionPointSubCategoryActual.getListInspectionPoint()) {
			    InspectionPointDTO newCategoryInspectionPointDTO = new InspectionPointDTO();
			    newCategoryInspectionPointDTO.setId(categoryInspectionPointActual.getId());
			    newCategoryInspectionPointDTO.setName(categoryInspectionPointActual.getName());
			    newCategoryInspectionPointDTO.setIdFeature(categoryInspectionPointActual.getIdFeature());
			    newCategoryInspectionPointDTO.setFeature(categoryInspectionPointActual.isFeature());
			    newCategoryInspectionPointDTO.setActive(categoryInspectionPointActual.isActive());
			    newCategoryInspectionPointDTO.setReprovesInspection(categoryInspectionPointActual.isReprovesInspection());
			    newCategoryInspectionPointDTO.setAvailableRepairs(categoryInspectionPointActual.getAvailableRepairs());
			    newCategoryInspectionPointDTO.setPhotoRequired(categoryInspectionPointActual.isPhotoRequired());
			    newCategoryInspectionPointDTO.setNaButtonEnabled(categoryInspectionPointActual.isNaButtonEnabled());
			    newCategoryInspectionPointDTO.setStandartButtonEnabled(categoryInspectionPointActual.isStandarButtonEnabled());
			    newCategoryInspectionPointDTO.setRepairButtonEnabled(categoryInspectionPointActual.isRepairButtonEnabled());
			    newCategoryInspectionPointDTO.setReprovedButtonEnabled(categoryInspectionPointActual.isReprovedButtonEnabled());
			    newCategoryInspectionPointDTO.setShowAdditionalInfo(categoryInspectionPointActual.isShowAdditionalInfo());
			    newCategoryInspectionPointDTO.setShowAdditionalInfoKey(categoryInspectionPointActual.getShowAdditionalInfoKey());
			    newCategoryInspectionPointDTO.setCaptureAdditionalInfo(categoryInspectionPointActual.isCaptureAdditionalInfo());
			    newCategoryInspectionPointDTO.setCaptureAdditionalInfoKey(categoryInspectionPointActual.getCaptureAdditionalInfoKey());
			    newCategoryInspectionPointDTO.setGenerateDiscount(categoryInspectionPointActual.isGenerateDiscount());
			    newCategoryInspectionPointDTO.setGenerateSummaryComment(categoryInspectionPointActual.isGenerateSummaryComment());
			    newCategoryInspectionPointDTO.setGenerateEmailAlert(categoryInspectionPointActual.isGenerateEmailAlert());
			    newCategoryInspectionPointDTO.setDiscountTypeId(categoryInspectionPointActual.getDiscountTypeId());

			    listSubCategoryInspectionPointDTO.add(newCategoryInspectionPointDTO);
			}
			inspectionPointSubCategoryDTO.setListInspectionPointDTO(listSubCategoryInspectionPointDTO);

			listInspectionPointSubCategoryDTO.add(inspectionPointSubCategoryDTO);
		    }
		}
		inspectionPointCategoryDTO.setListInspectionPointSubCategoryDTO(listInspectionPointSubCategoryDTO);

		listInspectionPointCategoryDTO.add(inspectionPointCategoryDTO);
	    }

	} else { // el listado de ptos de inspección registrados en
		 // v2_inspection_point_car donde inspection_id es el ID
		 // recibido de inspección.
	    LogService.logger.info(Constants.LOG_EXECUTING_START + " [getInspectionPointsCategories] - Se busca puntos de inspección para id: [" + inspectionId + "]");

	    for (InspectionPointCategory inspectionPointCategoryActual : listInspectionPointOrderByCategories) {
		InspectionPointCategoryDTO inspectionPointCategoryDTO = new InspectionPointCategoryDTO();
		inspectionPointCategoryDTO.setId(inspectionPointCategoryActual.getId());
		inspectionPointCategoryDTO.setName(inspectionPointCategoryActual.getName());
		inspectionPointCategoryDTO.setActive(inspectionPointCategoryActual.isActive());

		List<InspectionPointDTO> listInspectionPointDTO = new ArrayList<>();
		List<InspectionPointSubCategoryDTO> listInspectionPointSubCategoryDTO = new ArrayList<>();

		if (inspectionPointCategoryActual.getListInspectionPoint() != null && !inspectionPointCategoryActual.getListInspectionPoint().isEmpty()) {
		    for (InspectionPoint inspectionPointActual : inspectionPointCategoryActual.getListInspectionPoint()) {
			InspectionPointDTO inspectionPointDTO = new InspectionPointDTO();
			inspectionPointDTO.setId(inspectionPointActual.getId());
			inspectionPointDTO.setName(inspectionPointActual.getName());
			inspectionPointDTO.setIdFeature(inspectionPointActual.getIdFeature());
			inspectionPointDTO.setFeature(inspectionPointActual.isFeature());
			inspectionPointDTO.setActive(inspectionPointActual.isActive());
			inspectionPointDTO.setReprovesInspection(inspectionPointActual.isReprovesInspection());
			inspectionPointDTO.setAvailableRepairs(inspectionPointActual.getAvailableRepairs());
			inspectionPointDTO.setPhotoRequired(inspectionPointActual.isPhotoRequired());
			inspectionPointDTO.setNaButtonEnabled(inspectionPointActual.isNaButtonEnabled());
			inspectionPointDTO.setStandartButtonEnabled(inspectionPointActual.isStandarButtonEnabled());
			inspectionPointDTO.setRepairButtonEnabled(inspectionPointActual.isRepairButtonEnabled());
			inspectionPointDTO.setReprovedButtonEnabled(inspectionPointActual.isReprovedButtonEnabled());
			inspectionPointDTO.setShowAdditionalInfo(inspectionPointActual.isShowAdditionalInfo());
			inspectionPointDTO.setShowAdditionalInfoKey(inspectionPointActual.getShowAdditionalInfoKey());
			inspectionPointDTO.setCaptureAdditionalInfo(inspectionPointActual.isCaptureAdditionalInfo());
			inspectionPointDTO.setCaptureAdditionalInfoKey(inspectionPointActual.getCaptureAdditionalInfoKey());
			inspectionPointDTO.setGenerateDiscount(inspectionPointActual.isGenerateDiscount());
			inspectionPointDTO.setGenerateSummaryComment(inspectionPointActual.isGenerateSummaryComment());
			inspectionPointDTO.setGenerateEmailAlert(inspectionPointActual.isGenerateEmailAlert());
			inspectionPointDTO.setDiscountTypeId(inspectionPointActual.getDiscountTypeId());
			inspectionPointDTO.setInspectionPointTypeId(inspectionPointActual.getInspectionPointTypeId());

			List<InspectionPointCar> listInspectionPointCarBD = inspectionPointCarRepo.findByIdInspectionAndIdInspectioPointItem(inspectionId, inspectionPointActual.getId());
			if (listInspectionPointCarBD != null) {
			    List<InspectionPointCarDTO> listInspectionPointCarDTO = new ArrayList<>();
			    for (InspectionPointCar inspectionPointCarActual : listInspectionPointCarBD) {
				listInspectionPointCarDTO.add(inspectionPointCarActual.getDTO());
			    }
			    inspectionPointDTO.setListInspectionPointCarDTO(listInspectionPointCarDTO);
			}

			if (inspectionPointActual.getInspectionPointTypeId() == 1) {
			    totalCountQuestionType1++;
			}
			listInspectionPointDTO.add(inspectionPointDTO);
		    }
		}
		inspectionPointCategoryDTO.setListInspectionPointDTO(listInspectionPointDTO);

		if (inspectionPointCategoryActual.getListInspectionPointSubCategory() != null && !inspectionPointCategoryActual.getListInspectionPointSubCategory().isEmpty()) {
		    for (InspectionPointSubCategory inspectionPointSubCategoryActual : inspectionPointCategoryActual.getListInspectionPointSubCategory()) {
			InspectionPointSubCategoryDTO inspectionPointSubCategoryDTO = new InspectionPointSubCategoryDTO();
			inspectionPointSubCategoryDTO.setId(inspectionPointSubCategoryActual.getId());
			inspectionPointSubCategoryDTO.setName(inspectionPointSubCategoryActual.getName());
			inspectionPointSubCategoryDTO.setActive(inspectionPointSubCategoryActual.isActive());
			List<InspectionPointDTO> listSubCategoryInspectionPointDTO = new ArrayList<>();

			for (InspectionPoint categoryInspectionPointActual : inspectionPointSubCategoryActual.getListInspectionPoint()) {
			    InspectionPointDTO newCategoryInspectionPointDTO = new InspectionPointDTO();
			    newCategoryInspectionPointDTO.setId(categoryInspectionPointActual.getId());
			    newCategoryInspectionPointDTO.setName(categoryInspectionPointActual.getName());
			    newCategoryInspectionPointDTO.setIdFeature(categoryInspectionPointActual.getIdFeature());
			    newCategoryInspectionPointDTO.setFeature(categoryInspectionPointActual.isFeature());
			    newCategoryInspectionPointDTO.setActive(categoryInspectionPointActual.isActive());
			    newCategoryInspectionPointDTO.setReprovesInspection(categoryInspectionPointActual.isReprovesInspection());
			    newCategoryInspectionPointDTO.setAvailableRepairs(categoryInspectionPointActual.getAvailableRepairs());
			    newCategoryInspectionPointDTO.setPhotoRequired(categoryInspectionPointActual.isPhotoRequired());
			    newCategoryInspectionPointDTO.setNaButtonEnabled(categoryInspectionPointActual.isNaButtonEnabled());
			    newCategoryInspectionPointDTO.setStandartButtonEnabled(categoryInspectionPointActual.isStandarButtonEnabled());
			    newCategoryInspectionPointDTO.setRepairButtonEnabled(categoryInspectionPointActual.isRepairButtonEnabled());
			    newCategoryInspectionPointDTO.setReprovedButtonEnabled(categoryInspectionPointActual.isReprovedButtonEnabled());
			    newCategoryInspectionPointDTO.setShowAdditionalInfo(categoryInspectionPointActual.isShowAdditionalInfo());
			    newCategoryInspectionPointDTO.setShowAdditionalInfoKey(categoryInspectionPointActual.getShowAdditionalInfoKey());
			    newCategoryInspectionPointDTO.setCaptureAdditionalInfo(categoryInspectionPointActual.isCaptureAdditionalInfo());
			    newCategoryInspectionPointDTO.setCaptureAdditionalInfoKey(categoryInspectionPointActual.getCaptureAdditionalInfoKey());
			    newCategoryInspectionPointDTO.setGenerateDiscount(categoryInspectionPointActual.isGenerateDiscount());
			    newCategoryInspectionPointDTO.setGenerateSummaryComment(categoryInspectionPointActual.isGenerateSummaryComment());
			    newCategoryInspectionPointDTO.setGenerateEmailAlert(categoryInspectionPointActual.isGenerateEmailAlert());
			    newCategoryInspectionPointDTO.setDiscountTypeId(categoryInspectionPointActual.getDiscountTypeId());
			    newCategoryInspectionPointDTO.setInspectionPointTypeId(categoryInspectionPointActual.getInspectionPointTypeId());
			    listSubCategoryInspectionPointDTO.add(newCategoryInspectionPointDTO);
			}
			inspectionPointSubCategoryDTO.setListInspectionPointDTO(listSubCategoryInspectionPointDTO);

			listInspectionPointSubCategoryDTO.add(inspectionPointSubCategoryDTO);
		    }
		}
		inspectionPointCategoryDTO.setListInspectionPointSubCategoryDTO(listInspectionPointSubCategoryDTO);
		listInspectionPointCategoryDTO.add(inspectionPointCategoryDTO);
	    }
	}

	// Se calcula el radom
	if (inspectionTypeId != null) {
	    if (inspectionTypeId.intValue() == 1 || inspectionTypeId.intValue() == 2 || inspectionTypeId.intValue() == 3) {
		for (int indexMandatory = 1; indexMandatory <= maxCountQuestionType1; indexMandatory++) {
		    int randomNum = ThreadLocalRandom.current().nextInt(1, totalCountQuestionType1 + 1);
		    listQuestionMandatories.add(Long.valueOf(randomNum));
		}
	    }
	}

	//
	for (InspectionPointCategoryDTO inspectionPointCategoryDTOActual : listInspectionPointCategoryDTO) {
	    for (InspectionPointDTO inspectionPointDTOActual : inspectionPointCategoryDTOActual.getlistInspectionPointDTO()) {

		if (listQuestionMandatories.contains(inspectionPointCategoryDTOActual.getId())) {
		    inspectionPointDTOActual.setMandatory(true);
		}
	    }

	    listFinalInspectionPointCategoryDTO.add(inspectionPointCategoryDTOActual);
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listFinalInspectionPointCategoryDTO);

	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getInspectionPointsCategories) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Consulta todos los tipos de dimples
     * 
     * @author Enrique Marin
     * @return List<DimpleDTO>
     *
     */
    public ResponseDTO getDimples() {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getDimples) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	List<DimpleType> listDimples = dimpleRepo.findAll();
	List<DimpleDTO> listDimplesResponseDTO = new ArrayList<>();
	for (DimpleType dimpleActual : listDimples) {
	    listDimplesResponseDTO.add(dimpleActual.getDTO());
	}
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listDimplesResponseDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getDimples) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Consulta la lista de reparaciones agrupados por categorias
     * 
     * @author Enrique Marin
     * @return List<RepairCategoryDTO>
     *
     */
    public ResponseDTO repairsCategories() {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (repairsCategories) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	List<RepairCategory> listRepairCategories = repairCategoryRepo.findAll();
	List<RepairCategoryDTO> listRepairCategoriesResponseDTO = new ArrayList<>();
	for (RepairCategory repairCategoryActual : listRepairCategories) {
	    listRepairCategoriesResponseDTO.add(repairCategoryActual.getDTO());
	}
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listRepairCategoriesResponseDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (repairsCategories) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Regsitra el resultado de una inspection recorriendo los 248 puntos.
     * 
     * @author Enrique Marin
     * @param postInspectionPointsDetailRequestDTO
     * @return N/A
     *
     */
    public ResponseDTO postInspectionPointsDetail(PostInspectionPointsDetailRequestDTO postInspectionPointsDetailRequestDTO) {
        LogService.logger.info(Constants.LOG_EXECUTING_START + " [postInspectionPointsDetail] ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	for (InspectionPointDetailDTO inspectionPointDetailDTOActual : postInspectionPointsDetailRequestDTO.getListInspectionPointDetailDTO()) {
	    InspectionPointCar newInspectionPointCar = new InspectionPointCar();
	    newInspectionPointCar.setIdInspection(inspectionPointDetailDTOActual.getIdInspection());
	    newInspectionPointCar.setIdInspectioPointItem(inspectionPointDetailDTOActual.getIdInspectionPoint());
	    newInspectionPointCar.setIdInspectionPointResult(inspectionPointDetailDTOActual.getInspectionPointAnswer());
	    newInspectionPointCar.setInspectorEmail(inspectionPointDetailDTOActual.getInspectorEmail());
	    newInspectionPointCar.setAdditionalInfoDetail(inspectionPointDetailDTOActual.getAdditionalInfoDetail());
	    newInspectionPointCar.setInspectionTypeId(inspectionPointDetailDTOActual.getInspectionTypeId());
	    inspectionPointCarRepo.save(newInspectionPointCar);

	    // Caso lista Dimples
	    if (inspectionPointDetailDTOActual.getListInspectionDimpleDTO() != null && !inspectionPointDetailDTOActual.getListInspectionDimpleDTO().isEmpty()) {
		for (InspectionDimpleDTO inspectionDimpleDTOActual : inspectionPointDetailDTOActual.getListInspectionDimpleDTO()) {
		    InspectionDimple newInspectionDimple = new InspectionDimple();
		    newInspectionDimple.setIdInspection(inspectionPointDetailDTOActual.getIdInspection());
		    // newInspectionDimple.setIdInspectionPointCar(inspectionPointCarBD.getId());
		    newInspectionDimple.setIdDimpleType(inspectionDimpleDTOActual.getIdDimple());
		    newInspectionDimple.setCoordinateX(inspectionDimpleDTOActual.getCoordinateX().toString().replace(".", ""));
		    newInspectionDimple.setCoordinateY(inspectionDimpleDTOActual.getCoordinateY().toString().replace(".", ""));
		    newInspectionDimple.setOriginalHeight(inspectionDimpleDTOActual.getOriginalHeight());
		    newInspectionDimple.setOriginalWidth(inspectionDimpleDTOActual.getOriginalWidth());
		    newInspectionDimple.setInspectorEmail(inspectionPointDetailDTOActual.getInspectorEmail());
		    newInspectionDimple.setDimpleLocation(inspectionDimpleDTOActual.getDimpleLocation());
		    newInspectionDimple.setInspectionPointCar(newInspectionPointCar);
		    Optional<DimpleType> dimpleTypeBD = dimpleTypeRepo.findById(inspectionDimpleDTOActual.getIdDimple());
		    String nameDimpleFixed = dimpleTypeBD.get().getName().replace(' ', '_');

		    String nameImgDimple = Calendar.getInstance().getTimeInMillis() + "_" + nameDimpleFixed;
		    if (!inspectionDimpleDTOActual.getDimpleImage().equalsIgnoreCase(Constants.NO_PHOTO_INSPECTOR)) {
                        KavakUtils.generateFile(inspectionDimpleDTOActual.getDimpleImage(), nameImgDimple, null, Constants.FILE_TYPE_JPG,false);
			newInspectionDimple.setDimpleImage(Constants.NAME_BASE_IMAGE_BD + nameImgDimple);
		    } else {
			newInspectionDimple.setDimpleImage(Constants.NO_PHOTO_INSPECTOR);
		    }
		    if (!inspectionDimpleDTOActual.getDimpleImageLocation().equalsIgnoreCase(Constants.NO_PHOTO_INSPECTOR)) {
			String nameDimpleImageLocation = Calendar.getInstance().getTimeInMillis() + "_location_" + nameDimpleFixed;
                        KavakUtils.generateFile(inspectionDimpleDTOActual.getDimpleImageLocation(), nameDimpleImageLocation, null, Constants.FILE_TYPE_JPG,false);
			newInspectionDimple.setDimpleImageLocation(Constants.NAME_BASE_IMAGE_BD + nameDimpleImageLocation);
		    } else {
			newInspectionDimple.setDimpleImageLocation(Constants.NO_PHOTO_INSPECTOR);
		    }

		    inspectionDimpleRepo.save(newInspectionDimple);
		}
	    }

	    // Caso lista Repairs
	    if (inspectionPointDetailDTOActual.getListInspectionRepairDTO() != null && !inspectionPointDetailDTOActual.getListInspectionRepairDTO().isEmpty()) {
		InspectionRepair inspectionRepairBD = null;
		InspectionRepair newInspectionRepair = null;
		for (InspectionRepairDTO inspectionRepairDTOActual : inspectionPointDetailDTOActual.getListInspectionRepairDTO()) {

		    // TODO Borrar luego de pruebas de App Inspección
		    LogService.logger.info("inspection_id->" + inspectionPointDetailDTOActual.getIdInspection() + " repair_category->" + inspectionRepairDTOActual.getIdInspectionRepairCategory() + " repair_subcategory->" + inspectionRepairDTOActual.getIdInspectionRepairSubCategory() + " repair_type_id->" + inspectionRepairDTOActual.getIdInspectionRepairItem());

		    if (!(inspectionRepairDTOActual.getIdInspectionRepairCategory() == null || inspectionRepairDTOActual.getIdInspectionRepairSubCategory() == null || inspectionRepairDTOActual.getIdInspectionRepairItem() == null)) {
			inspectionRepairBD = inspectionRepairRepo.findIfExist(inspectionPointDetailDTOActual.getIdInspection(), inspectionRepairDTOActual.getIdInspectionRepairCategory(), inspectionRepairDTOActual.getIdInspectionRepairSubCategory(), inspectionRepairDTOActual.getIdInspectionRepairItem(), inspectionRepairDTOActual.getIdInspectionPointCar());
		    }

		    if (inspectionRepairBD != null) {
			inspectionRepairBD.setRepairAmount(inspectionRepairDTOActual.getRepairAmount().longValue());
			inspectionRepairBD.setInspectorEmail(inspectionPointDetailDTOActual.getInspectorEmail());

			if (inspectionRepairDTOActual.getRepairOtherText() == null || inspectionRepairDTOActual.getRepairOtherText().isEmpty()) {
			    inspectionRepairBD.setRepairOtherText(Constants.NO_VALUE_BD);
			} else {
			    inspectionRepairBD.setRepairOtherText(inspectionRepairDTOActual.getRepairOtherText());
			}

			if (!inspectionRepairDTOActual.getRepairImage().equalsIgnoreCase(Constants.NO_PHOTO_INSPECTOR)) {
			    String nameImgRepair = Calendar.getInstance().getTimeInMillis() + "_repair";
			    KavakUtils.generateFile(inspectionRepairDTOActual.getRepairImage(), nameImgRepair, null, Constants.FILE_TYPE_JPG,false);
			    inspectionRepairBD.setRepairImage(Constants.NAME_BASE_IMAGE_BD + nameImgRepair);
			} else {
			    inspectionRepairBD.setRepairImage(Constants.NO_PHOTO_INSPECTOR);
			}
			inspectionRepairRepo.save(inspectionRepairBD);
		    } else {
			newInspectionRepair = new InspectionRepair();
			newInspectionRepair.setIdInspection(inspectionPointDetailDTOActual.getIdInspection());
			newInspectionRepair.setIdInspectionRepairCategory(inspectionRepairDTOActual.getIdInspectionRepairCategory());
			newInspectionRepair.setIdInspectionRepairSubCategory(inspectionRepairDTOActual.getIdInspectionRepairSubCategory());
			newInspectionRepair.setIdInspectionRepairItem(inspectionRepairDTOActual.getIdInspectionRepairItem());
			newInspectionRepair.setRepairAmount(inspectionRepairDTOActual.getRepairAmount().longValue());
			newInspectionRepair.setInspectorEmail(inspectionPointDetailDTOActual.getInspectorEmail());
			newInspectionRepair.setInspectionTypeId(inspectionPointDetailDTOActual.getInspectionTypeId());

			if (inspectionRepairDTOActual.getRepairOtherText() == null || inspectionRepairDTOActual.getRepairOtherText().isEmpty()) {
			    newInspectionRepair.setRepairOtherText(Constants.NO_VALUE_BD);
			} else {
			    newInspectionRepair.setRepairOtherText(inspectionRepairDTOActual.getRepairOtherText());
			}

			newInspectionRepair.setInspectionPointCar(newInspectionPointCar);
			if (!inspectionRepairDTOActual.getRepairImage().equalsIgnoreCase(Constants.NO_PHOTO_INSPECTOR)) {
			    String nameImgRepair = Calendar.getInstance().getTimeInMillis() + "_repair";
                        KavakUtils.generateFile(inspectionRepairDTOActual.getRepairImage(), nameImgRepair, null, Constants.FILE_TYPE_JPG,false);
			    newInspectionRepair.setRepairImage(Constants.NAME_BASE_IMAGE_BD + nameImgRepair);
			} else {
			    newInspectionRepair.setRepairImage(Constants.NO_PHOTO_INSPECTOR);
			}
			inspectionRepairRepo.save(newInspectionRepair);
		    }
		}
	    }
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(new ArrayList<>());
        LogService.logger.info(Constants.LOG_EXECUTING_END+ " [postInspectionPointsDetail] [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Crea o Actualiza una lista de inspecciones
     * 
     * @author Enrique Marin
     * @param putInspectionOverviewRequestDTO
     * @return List<PostInpectionOverviewResponseDTO>
     *
     */
    public ResponseDTO putInspectionOverview(PutInspectionOverviewRequestDTO putInspectionOverviewRequestDTO) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (putInspectionOverview) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
//        List<PostInspectionOverviewResponseDTO> listPostInpectionOverviewResponseDTO = new ArrayList<>();
	PostInspectionOverviewResponseDTO inpectionOverviewResponseDTO = new PostInspectionOverviewResponseDTO();
	MessageDTO messageNoRecords = null;
	for (InspectionOverviewDTO inspectionOverviewDTOActual : putInspectionOverviewRequestDTO.getListInspectionOverviewDTO()) {
	    Optional<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findById(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getIdOfferCheckpoint());
	    Inspection inspectionExistBD = inspectionRepo.findByIdOffertCheckPoint(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getIdOfferCheckpoint());

	    if (inspectionExistBD == null) {
		if (!offerCheckpointBD.isPresent()) {
                    listMessages = new ArrayList<>();
		    if (messageNoRecords == null) {
			messageNoRecords = messageRepo.findByCode(MessageEnum.M0004.toString()).getDTO();
		    }
		    listMessages.add(messageNoRecords);
		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setListMessage(listMessages);

		} else {

		    Inspection newInspecion = new Inspection();
		    newInspecion.setIdOffertCheckPoint(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getIdOfferCheckpoint());
		    newInspecion.setCustomerName(offerCheckpointBD.get().getCustomerName() + " " + offerCheckpointBD.get().getCustomerLastName());
		    newInspecion.setCustomerPhone(offerCheckpointBD.get().getUser().getDTO().getPhone());
		    newInspecion.setCustomerEmail(offerCheckpointBD.get().getEmail());
		    newInspecion.setSendNetsuite(0L);
		    if (offerCheckpointBD.get().getUser().getDTO().getAddress() == null || offerCheckpointBD.get().getUser().getDTO().getAddress().isEmpty()) {
			newInspecion.setCustomerAddress("NA");
		    } else {
			newInspecion.setCustomerAddress(offerCheckpointBD.get().getUser().getDTO().getAddress());
		    }
		    newInspecion.setSku(offerCheckpointBD.get().getSku());
		    newInspecion.setCarMake(offerCheckpointBD.get().getCarMake());
		    newInspecion.setCarModel(offerCheckpointBD.get().getCarModel());
		    if (offerCheckpointBD.get().getFullVersion() == null || offerCheckpointBD.get().getFullVersion().isEmpty()) {
			newInspecion.setCarFullVersion("NA");
		    } else {
			newInspecion.setCarFullVersion(offerCheckpointBD.get().getFullVersion());
		    }
		    if (offerCheckpointBD.get().getCarVersion() == null || offerCheckpointBD.get().getCarVersion().isEmpty()) {
			newInspecion.setCarTrim("NA");
		    } else {
			newInspecion.setCarTrim(offerCheckpointBD.get().getCarVersion());
		    }

		    if (inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getIdHourBlock() != null) {
			newInspecion.setIdHourBlock(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getIdHourBlock());
		    } else if (inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getHourBlockText() != null) {
			InspectionScheduleBlock inspectionScheduleBlockBD = inspectionScheduleBlockRepo.findByHourFrom(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getHourBlockText());
			newInspecion.setIdHourBlock(inspectionScheduleBlockBD.getIdHourBlock());
		    }

		    if (offerCheckpointBD.get().getInspectionDate() == null || offerCheckpointBD.get().getInspectionDate().isEmpty()) {
			newInspecion.setInspectionDate(new Date());
		    } else {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String dateInString = offerCheckpointBD.get().getInspectionDate();
			try {
			    newInspecion.setInspectionDate(formatter.parse(dateInString));
			} catch (ParseException e) {
			    e.printStackTrace();
			}
		    }

		    newInspecion.setCarYear(offerCheckpointBD.get().getCarYear());
		    newInspecion.setCarKm(offerCheckpointBD.get().getCarKm());
		    newInspecion.setInspectionAddress(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getInspectionAddress());
		    newInspecion.setNameExperimentManager(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getNameExperimentManager());
		    newInspecion.setNameTowerContact(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getNameTowerContact());
		    newInspecion.setComments(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getComments());
		    newInspecion.setInspectorEmail(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getInspectorEmail());
		    newInspecion.setCanceled(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().isCanceled());
		    newInspecion.setCanceledReason(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getCanceledReason());
		    newInspecion.setRescheduled(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().isRescheduled());
		    newInspecion.setRescheduledReason(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getRescheduledReason());

		    if (offerCheckpointBD.get().getMarketPrice() == null || offerCheckpointBD.get().getMarketPrice().equals("NA")) {
			newInspecion.setMarketPrice(0L);
		    } else {
			// String marketPrice =
			// offerCheckpointBD.getMarketPrice().toString().substring(1,
			// offerCheckpointBD.getMarketPrice().length());
			newInspecion.setMarketPrice(KavakUtils.getRealNumber(offerCheckpointBD.get().getMarketPrice()));
		    }
		    if (offerCheckpointBD.get().getBuyPrice() == null || offerCheckpointBD.get().getBuyPrice().equals("NA")) {
			newInspecion.setBuyPriceGa(0L);
		    } else {
			// String buyPriceGa =
			// offerCheckpointBD.getBuyPrice().toString().substring(1,
			// offerCheckpointBD.getBuyPrice().length());
			newInspecion.setBuyPriceGa(KavakUtils.getRealNumber(offerCheckpointBD.get().getBuyPrice()));
		    }
		    if (offerCheckpointBD.get().getSalePrice() == null || offerCheckpointBD.get().getSalePrice().equals("NA")) {
			newInspecion.setSalePriceGa(0L);
		    } else {
			// String salePriceGa =
			// offerCheckpointBD.getSalePrice().toString().substring(1,
			// offerCheckpointBD.getSalePrice().length());
			newInspecion.setSalePriceGa(KavakUtils.getRealNumber(offerCheckpointBD.get().getSalePrice()));
		    }
		    if (offerCheckpointBD.get().getSalePriceKavak() == null || offerCheckpointBD.get().getSalePriceKavak().equals("NA")) {
			newInspecion.setSalePriceKavak(0L);
		    } else {
			// String sellPriceKavak =
			// offerCheckpointBD.getSellPrice().toString().substring(1,
			// offerCheckpointBD.getSalePrice().length());
			newInspecion.setSalePriceKavak(KavakUtils.getRealNumber(offerCheckpointBD.get().getSalePriceKavak()));
		    }

		    newInspecion.setZipCode(offerCheckpointBD.get().getZipCode());
		    Inspection inspectionBD = inspectionRepo.save(newInspecion);
		    InspectionOffer inspectionInstantOffer = new InspectionOffer();
		    InspectionOffer inspection30DaysOffer = new InspectionOffer();
		    InspectionOffer inspectionConsigmentOffer = new InspectionOffer();

		    inspectionInstantOffer.setIdInspection(inspectionBD.getId());
		    inspectionInstantOffer.setStatusOffer(0L);
		    inspectionInstantOffer.setSelectedOffer(false);
		    inspectionInstantOffer.setOfferType(1L);

		    inspection30DaysOffer.setIdInspection(inspectionBD.getId());
		    inspection30DaysOffer.setStatusOffer(0L);
		    inspection30DaysOffer.setSelectedOffer(false);
		    inspection30DaysOffer.setOfferType(2L);

		    inspectionConsigmentOffer.setIdInspection(inspectionBD.getId());
		    inspectionConsigmentOffer.setStatusOffer(0L);
		    inspectionConsigmentOffer.setSelectedOffer(false);
		    inspectionConsigmentOffer.setOfferType(3L);

		    Optional<MetaValue> metaVaueOfferType = metaValueRepo.findById(offerCheckpointBD.get().getOfferType());

		    if (OfferTypeEnum.getByAlias(metaVaueOfferType.get().getAlias()).equals(OfferTypeEnum.BUYINMEDIATE)) {
			inspectionInstantOffer.setSelectedOffer(true);
		    }
		    if (OfferTypeEnum.getByAlias(metaVaueOfferType.get().getAlias()).equals(OfferTypeEnum.BUY30DAYS)) {
			inspection30DaysOffer.setSelectedOffer(true);
		    }
		    if (OfferTypeEnum.getByAlias(metaVaueOfferType.get().getAlias()).equals(OfferTypeEnum.BUYCONSIGMENT)) {
			inspectionConsigmentOffer.setSelectedOffer(true);
		    }

		    if (offerCheckpointBD.get().getMinInstantOffer() != null) {
			inspectionInstantOffer.setMinValue(offerCheckpointBD.get().getMinInstantOffer());
			inspectionInstantOffer.setMaxValue(offerCheckpointBD.get().getMaxInstantOffer());
			inspectionOfferRepo.save(inspectionInstantOffer);
		    }
		    if (offerCheckpointBD.get().getMin30DaysOffer() != null) {
			inspection30DaysOffer.setMinValue(offerCheckpointBD.get().getMin30DaysOffer());
			inspection30DaysOffer.setMaxValue(offerCheckpointBD.get().getMax30DaysOffer());
			inspectionOfferRepo.save(inspection30DaysOffer);
		    }

		    if (offerCheckpointBD.get().getMinConsignationOffer() != null) {
			inspectionConsigmentOffer.setMinValue(offerCheckpointBD.get().getMinConsignationOffer());
			inspectionConsigmentOffer.setMaxValue(offerCheckpointBD.get().getMaxConsignationOffer());
			inspectionOfferRepo.save(inspectionConsigmentOffer);
		    }

		    for (InspectionDiscountDTO inspectionDiscountDTOActual : inspectionOverviewDTOActual.getListInspectionDiscountDTO()) {
			InspectionDiscount newInspectionDiscount = new InspectionDiscount();
			newInspectionDiscount.setIdInspection(inspectionBD.getId());
			newInspectionDiscount.setDiscountType(inspectionDiscountDTOActual.getDiscountType());
			newInspectionDiscount.setDiscountAmount(inspectionDiscountDTOActual.getDiscountAmount());
			newInspectionDiscount.setInspectorEmail(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getInspectorEmail());
			inspectionDiscountRepo.save(newInspectionDiscount);
		    }

		    inpectionOverviewResponseDTO.setIdInspection(inspectionBD.getId());
		    // listPostInpectionOverviewResponseDTO.add(inpectionOverviewResponseDTO);
		}
	    } else {

		// Si el tipo de inspeccion es post-inspeccion N1, N2 o Sanity
		// Check, no se debe actualizar la información de la tabla
		// v2_inspection.
		if (inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getInspectionTypeId() == InspectionTypeEnum.INSPECCION.getId()) {
		    // if(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getInspectionTypeId()
		    // == 1L) {
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    inspectionExistBD.setInspectorEmail(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getInspectorEmail());
		    Date inspectionDate;
		    try {
			inspectionDate = sdf.parse(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getInspectionDate());
			inspectionExistBD.setInspectionDate(inspectionDate);
		    } catch (ParseException e) {
			e.printStackTrace();
		    }

		    inspectionExistBD.setInspectionAddress(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getInspectionAddress());
		    inspectionExistBD.setNameExperimentManager(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getNameExperimentManager());
		    inspectionExistBD.setNameTowerContact(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getNameTowerContact());
		    inspectionExistBD.setComments(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getComments());
		}

		for (InspectionDiscountDTO inspectionDiscountDTOActual : inspectionOverviewDTOActual.getListInspectionDiscountDTO()) {
		    InspectionDiscount inspectionDiscountBD = inspectionDiscountRepo.findByIdInspectionAndDiscountType(inspectionExistBD.getId(), inspectionDiscountDTOActual.getDiscountType());
		    if (inspectionDiscountBD == null) {
			InspectionDiscount newInspectionDiscount = new InspectionDiscount();
			newInspectionDiscount.setIdInspection(inspectionExistBD.getId());
			newInspectionDiscount.setDiscountType(inspectionDiscountDTOActual.getDiscountType());
			newInspectionDiscount.setDiscountAmount(inspectionDiscountDTOActual.getDiscountAmount());
			newInspectionDiscount.setInspectorEmail(inspectionOverviewDTOActual.getInspectionOverviewDataDTO().getInspectorEmail());
			inspectionDiscountRepo.save(newInspectionDiscount);
		    } else {
			inspectionDiscountBD.setDiscountAmount(inspectionDiscountDTOActual.getDiscountAmount());
			inspectionDiscountRepo.save(inspectionDiscountBD);
		    }
		}
		inpectionOverviewResponseDTO.setIdInspection(inspectionExistBD.getId());
		// listPostInpectionOverviewResponseDTO.add(inpectionOverviewResponseDTO);
	    }
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(inpectionOverviewResponseDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (putInspectionOverview) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Lista de imagenes genericas de los lados de un carro
     * 
     * @author Enrique Marin
     * @return List<InpectionCarPhotoTypeDTO>
     */
    public ResponseDTO getCarPhotoTypes() {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getCarPhotoTypes) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	List<InspectionCarPhotoTypeDTO> listinpectionCarPhotoTypeDTO = new ArrayList<>();
	for (InspectionCarPhotoType inpectionCarPhotoTypeActual : inspectionCarPhotoTypeRepo.findAllByOrderByPositionAsc()) {
	    listinpectionCarPhotoTypeDTO.add(inpectionCarPhotoTypeActual.getDTO());
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listinpectionCarPhotoTypeDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getCarPhotoTypes) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Retorna todos los features agrupados por categorias
     * 
     * @author Enrique Marin
     * @return List<CarFeatureCategoryDTO>
     */
    public ResponseDTO getFeatures() {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getFeatures) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	GetFeaturesResponseDTO getFeaturesResponseDTO = new GetFeaturesResponseDTO();
	List<CarFeatureCategory> listCarFeatureCategoryBD = carFeatureCategoryRepo.findAll();

	List<CarFeatureCategoryDTO> listCarFeatureCategoryDTO = new ArrayList<>();
	for (CarFeatureCategory carFeatureCategoryActual : listCarFeatureCategoryBD) {
	    listCarFeatureCategoryDTO.add(carFeatureCategoryActual.getDTO());
	}
	getFeaturesResponseDTO.setListCarFeatureCategoryDTO(listCarFeatureCategoryDTO);
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(getFeaturesResponseDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getFeatures) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Registar un resumen general de una inspeccion + Registra en SellCarDetail
     * la informacion que Apolo necesita de la inspeccion
     * 
     * @author Enrique Marin
     * @param putSummaryRequesDTO
     * @return N/A
     */
/*    public ResponseDTO putSummary(PutSummaryRequesDTO putSummaryRequesDTO) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " [putSummary] ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	InspectionSummary inspectionSummaryO = new InspectionSummary();
	for (InspectionSummaryDataDTO inspectionSummaryDataDTOActual : putSummaryRequesDTO.getListInspectionSummaryDTO()) {
	    InspectionSummary inspectionSummaryBD = inspectionSummaryRepo.findByIdInspectionAndSelectedTrue(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
	    if (inspectionSummaryBD != null) {
		inspectionSummaryBD.setSelected(false);
		inspectionSummaryRepo.save(inspectionSummaryBD);
	    }
	    InspectionSummary newInspectionSummary = new InspectionSummary();
	    newInspectionSummary.setIdInspection(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
	    newInspectionSummary.setInspectorEmail(inspectionSummaryDataDTOActual.getSummaryDTO().getInspectorEmail());
	    newInspectionSummary.setIdOfferType(inspectionSummaryDataDTOActual.getSummaryDTO().getIdOfferType());
	    newInspectionSummary.setBaseOfferAmount(inspectionSummaryDataDTOActual.getSummaryDTO().getBaseOfferAmount());
	    newInspectionSummary.setTotalDiscount(inspectionSummaryDataDTOActual.getSummaryDTO().getTotalDiscount());
	    newInspectionSummary.setFinalOfferAmount(inspectionSummaryDataDTOActual.getSummaryDTO().getFinalOfferAmount());
	    newInspectionSummary.setSelected(inspectionSummaryDataDTOActual.getSummaryDTO().isSelected());
	    newInspectionSummary.setInspectionApproved(inspectionSummaryDataDTOActual.getSummaryDTO().isInspectionApproved());
	    newInspectionSummary.setInspectionStatus(inspectionSummaryDataDTOActual.getSummaryDTO().getInspectionStatus());
	    newInspectionSummary.setTotalRepairsAmount(inspectionSummaryDataDTOActual.getSummaryDTO().getTotalRepairsAmount());
	    newInspectionSummary.setTotalBonusAmount(inspectionSummaryDataDTOActual.getSummaryDTO().getTotalBonusAmount());
	    newInspectionSummary.setInspectionTypeId(inspectionSummaryDataDTOActual.getSummaryDTO().getInspectionTypeId());
	    newInspectionSummary.setLatitudeInspection(inspectionSummaryDataDTOActual.getSummaryDTO().getLatitudeInspection());
	    newInspectionSummary.setLongitudeInspection(inspectionSummaryDataDTOActual.getSummaryDTO().getLongitudInspection());
	    newInspectionSummary.setSelected(true);
            if(inspectionSummaryDataDTOActual.getSummaryDTO().getInspectorComment() == null || inspectionSummaryDataDTOActual.getSummaryDTO().getInspectorComment().trim().equals("")){			    
        	newInspectionSummary.setInspectorComment(""); 
	    }else{
		newInspectionSummary.setInspectorComment(inspectionSummaryDataDTOActual.getSummaryDTO().getInspectorComment());
	    }
	    if (inspectionSummaryDataDTOActual.getSummaryDTO().getCustomerSignatureImage() != null) {
		String imgName = "customer_signature_" + inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection() + Calendar.getInstance().getTimeInMillis();
		KavakUtils.generateFile(inspectionSummaryDataDTOActual.getSummaryDTO().getCustomerSignatureImage(), imgName, Constants.NAME_BASE_IMAGE_CUSTOMER_SIGNATURE, Constants.FILE_TYPE_JPG , false);
		newInspectionSummary.setCustomerSignatureImage(Constants.NAME_BASE_IMAGE_BD + Constants.NAME_BASE_IMAGE_CUSTOMER_SIGNATURE + imgName);
	    }
	    inspectionSummaryRepo.save(newInspectionSummary);
	    inspectionSummaryO = newInspectionSummary;

	    // Carga de contenido para Apolo
	    if (inspectionSummaryDataDTOActual.getSummaryDTO().isInspectionApproved()) {
		Optional<Inspection> inspectionBD = inspectionRepo.findById(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
		SellCarDetail sellCarDetailBD = sellCarDetailRepo.findByIdInspection(inspectionBD.get().getId());

		if (sellCarDetailBD == null) {
		    sellCarDetailBD = new SellCarDetail();
		}

		sellCarDetailBD.setIdInspection(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
		if (inspectionBD.get().getCarYear() == null) {
		    sellCarDetailBD.setCarYear(Constants.NO_VALUE_BD);
		} else {
		    sellCarDetailBD.setCarYear(inspectionBD.get().getCarYear().toString());
		}
		sellCarDetailBD.setCarMake(inspectionBD.get().getCarMake());
		sellCarDetailBD.setCarModel(inspectionBD.get().getCarModel());
		sellCarDetailBD.setCarKm(inspectionBD.get().getCarKm().toString());
		sellCarDetailBD.setCarTrim(inspectionBD.get().getCarTrim());
		User userBD = userRepo.findByEmailContainingIgnoreCase(inspectionBD.get().getCustomerEmail());
		sellCarDetailBD.setSellerId(userBD.getId());
		sellCarDetailBD.setPhone(userBD.getDTO().getPhone());
		sellCarDetailBD.setPrice(inspectionBD.get().getSalePriceKavak());
		sellCarDetailBD.setCertified(true);
		sellCarDetailBD.setSale(false);
		sellCarDetailBD.setValide(false);
		sellCarDetailBD.setActive(true);
		sellCarDetailBD.setZipCode(inspectionBD.get().getZipCode());
		List<InspectionCarPhoto> listInspectionCarPhoto = inspectionCarPhotoRepo.findByIdInspection(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
		int indexPhotoCar = 0;
		String inpectionCarPhotoUrl = "";
		for (InspectionCarPhoto inspectionCarPhotoActual : listInspectionCarPhoto) {
		    if (indexPhotoCar != listInspectionCarPhoto.size()) {
			inpectionCarPhotoUrl = inpectionCarPhotoUrl + inspectionCarPhotoActual.getImageUrl().substring(16, inspectionCarPhotoActual.getImageUrl().length()) + "#";
		    } else {
			inpectionCarPhotoUrl = inpectionCarPhotoUrl + inspectionCarPhotoActual.getImageUrl().substring(16, inspectionCarPhotoActual.getImageUrl().length());
		    }
		    indexPhotoCar++;
		}
		sellCarDetailBD.setImageCar(inpectionCarPhotoUrl);
		Optional<OfferType> offerTypeBD = offerTypeRepo.findById(inspectionSummaryDataDTOActual.getSummaryDTO().getIdOfferType());
		if (offerTypeBD.isPresent()) {
		    sellCarDetailBD.setOfferType(offerTypeBD.get().getName());
		}
		Date date = new Date();
		sellCarDetailBD.setPostDate(new Timestamp(date.getTime()));

		sellCarDetailBD.setSendNetsuite(0L); // 0 - nuevo;
		SellCarDetail sellCarDetailForSellCarMetaBD = sellCarDetailRepo.save(sellCarDetailBD);
		SellCarMeta newSellCarMeta = new SellCarMeta();
		newSellCarMeta.setSellCarDetail(sellCarDetailForSellCarMetaBD);

		// HashMap general incluye los 2 hashMap de distintos
		// niveles para serializar todo de una vez
		// Se agregan todos los valores del serializado de primer
		// lvl
		HashMap<Object, Object> mapToSerialize = new HashMap<>();

		MetaValue metaValueBookingPriceBD = metaValueRepo.findByAlias(Constants.BOOKING_PRICE);
		mapToSerialize.put(Constants.BOOKING_PRICE, metaValueBookingPriceBD.getOptionName());

		// Se agregan todos los valores del serializado de segundo
		// lvl >> INSPECTOR_DETAILS
		HashMap<Object, Object> mapInpectorDetails = new HashMap<>();
		//Inspector inspectorBD = inspectorRepo.findByEmail(inspectionSummaryDataDTOActual.getSummaryDTO().getInspectorEmail());
		//mapInpectorDetails.put(Constants.NAME, inspectorBD.getName());
		mapInpectorDetails.put(Constants.NAME, "");
		//mapInpectorDetails.put(Constants.ADDRESS, inspectorBD.getUrl());
		mapInpectorDetails.put(Constants.ADDRESS, "");
		mapInpectorDetails.put(Constants.EMAIL, inspectionSummaryDataDTOActual.getSummaryDTO().getInspectorEmail());
		//mapInpectorDetails.put(Constants.IMAGE, inspectorBD.getImage());
		mapInpectorDetails.put(Constants.IMAGE, "");
		MetaValue metaValueDimpleCcontentBD = metaValueRepo.findByAlias("DIMPLECONTENT");
		mapInpectorDetails.put(Constants.DIMPLES_CONTENT, metaValueDimpleCcontentBD.getOptionName());
		mapToSerialize.put(Constants.INSPECTOR_DETAILS, mapInpectorDetails);

		// Se agregan todos los valores del serializado de segundo
		// lvl >> BASIC_OVERVIEW
		HashMap<Object, Object> mapBasicOverview = new HashMap<>();
		mapBasicOverview.put(Constants.STOCK, sellCarDetailForSellCarMetaBD.getId());
		CarData carDataBD = carDataRepo.findBySku(inspectionBD.get().getSku());
		mapBasicOverview.put(Constants.DOORS, carDataBD.getDoors());
		if (!carDataBD.getTraction().equalsIgnoreCase(Constants.NO_VALUE_BD)) {
		    mapBasicOverview.put(Constants.TRACTION, carDataBD.getTraction());
		}
		String[] transmision = carDataBD.getTrasmissions().split(" ");
		mapBasicOverview.put(Constants.TRANSMISION, transmision[0]);
		if (!carDataBD.getCylinder().equalsIgnoreCase(Constants.NO_VALUE_BD)) {
		    mapBasicOverview.put(Constants.CYLINDER, carDataBD.getCylinder());
		}
		if (!carDataBD.getHp().equalsIgnoreCase(Constants.NO_VALUE_BD)) {
		    mapBasicOverview.put(Constants.HORSE_POWER, carDataBD.getHp());
		}

		List<InspectionCarFeature> listInspectionCarFeatureEntertainmentBD = inspectionCarFeatureRepo.findByInspectionAndCategory(inspectionBD.get().getId(), Constants.CATEGORY_ENTERTAINMENT);
		mapBasicOverview.put(Constants.ENTERTAINMENT, getNamesSeparatedByCommas(listInspectionCarFeatureEntertainmentBD));

		List<InspectionCarFeature> listInspectionCarFeatureExteriorBD = inspectionCarFeatureRepo.findByInspectionAndCategory(inspectionBD.get().getId(), Constants.CATEGORY_EXTERIOR);
		mapBasicOverview.put(Constants.EXTERIOR, getNamesSeparatedByCommas(listInspectionCarFeatureExteriorBD));

		List<InspectionCarFeature> listInspectionCarFeatureInteriorBD = inspectionCarFeatureRepo.findByInspectionAndCategory(inspectionBD.get().getId(), Constants.CATEGORY_INTERIOR);
		mapBasicOverview.put(Constants.INTERIOR, getNamesSeparatedByCommas(listInspectionCarFeatureInteriorBD));

		List<InspectionCarFeature> listInspectionCarFeatureMechanicalBD = inspectionCarFeatureRepo.findByInspectionAndCategory(inspectionBD.get().getId(), Constants.CATEGORY_MECHANICAL);
		mapBasicOverview.put(Constants.MECHANICAL, getNamesSeparatedByCommas(listInspectionCarFeatureMechanicalBD));

		List<InspectionCarFeature> listInspectionCarFeatureSafetyBD = inspectionCarFeatureRepo.findByInspectionAndCategory(inspectionBD.get().getId(), Constants.CATEGORY_SAFETY);
		mapBasicOverview.put(Constants.SAFETY, getNamesSeparatedByCommas(listInspectionCarFeatureSafetyBD));

		List<InspectionCarFeature> listInspectionCarFeatureOtherBD = inspectionCarFeatureRepo.findByInspectionAndCategory(inspectionBD.get().getId(), Constants.CATEGORY_OTHERS);
		mapBasicOverview.put(Constants.OTHER, getNamesSeparatedByCommas(listInspectionCarFeatureOtherBD));

		mapToSerialize.put(Constants.BASIC_OVERVIEW, mapBasicOverview);

		// Se agregan todos los valores del serializado de segundo
		// lvl >> PRICING
		HashMap<Object, Object> mapPricing = new HashMap<>();
		mapPricing.put(Constants.MARKET_PRICE, inspectionBD.get().getMarketPrice());

		MetaValue metaValuePriceKvkBD = metaValueRepo.findByAlias("INSPECTIONPRICEKVK");
		mapPricing.put(Constants.INCPECTIONKS, metaValuePriceKvkBD.getOptionName());

		MetaValue metaValueInspectionMarketBD = metaValueRepo.findByAlias("INSPECTIONPRICEMKT");
		mapPricing.put(Constants.INSPECTIONPRICEMKT, metaValueInspectionMarketBD.getOptionName());

		MetaValue metaValuePaperWorkKvktBD = metaValueRepo.findByAlias("PAPERWORKKVK");
		mapPricing.put(Constants.PAPERWORKKVK, metaValuePaperWorkKvktBD.getOptionName());

		MetaValue metaValuePaperWorkMktBD = metaValueRepo.findByAlias("PAPERWORKMKT");
		mapPricing.put(Constants.PAPERWORKMKT, metaValuePaperWorkMktBD.getOptionName());

		MetaValue metaValueDetailKvkBD = metaValueRepo.findByAlias("DETAILINGKVK");
		mapPricing.put(Constants.DETAILINGKVK, metaValueDetailKvkBD.getOptionName());

		MetaValue metaValueDetailMktBD = metaValueRepo.findByAlias("DETAILINGMKT");
		mapPricing.put(Constants.DETAILINGMKT, metaValueDetailMktBD.getOptionName());

		// Hace referencia a la antiguedad del carro
		Long carAge = Calendar.getInstance().get(Calendar.YEAR) - carDataBD.getCarYear();

		CarWarrantyPrice carWarrantyPriceBD = carWarrantyPriceRepo.findByOther7AndCarAge(carDataBD.getOther7(), carAge.intValue());
		mapPricing.put(Constants.WARRANTYMARKET, carWarrantyPriceBD.getTreeMonthsPrice());

		mapToSerialize.put(Constants.PRICING, mapPricing);

		// Se arman los valores del feature list
		List<InspectionCarFeature> listInspectionCarFeatureIconImages = inspectionCarFeatureRepo.findByInspectionAndIconImageNotNull(inspectionBD.get().getId());
		String featureList = "";
		if (listInspectionCarFeatureIconImages != null && !listInspectionCarFeatureIconImages.isEmpty()) {
		    int indexCarFeatureIconImage = 1;
		    for (InspectionCarFeature inspectionCarFeatureActual : listInspectionCarFeatureIconImages) {
			inspectionCarFeatureActual.getFeatureLabel();
			if (indexCarFeatureIconImage == listInspectionCarFeatureIconImages.size()) {
			    featureList = featureList + inspectionCarFeatureActual.getInspectionCarFeatureItem().getIconImage() + "##" + inspectionCarFeatureActual.getFeatureLabel();
			} else {
			    featureList = featureList + inspectionCarFeatureActual.getInspectionCarFeatureItem().getIconImage() + "##" + inspectionCarFeatureActual.getFeatureLabel() + "||";
			}
			indexCarFeatureIconImage++;
		    }
		}
		mapToSerialize.put(Constants.FEATURE_LIST, featureList);
		newSellCarMeta.setMetaValue(Pherialize.serialize(mapToSerialize));
		SellCarMeta sellCarMeta = new SellCarMeta();
		sellCarMeta = sellCarMetaRepo.findBySellCarId(newSellCarMeta.getSellCarDetail().getId());
		if (sellCarMeta == null) {
		    sellCarMetaRepo.save(newSellCarMeta);
		}
		SellCarWarranty newSellCarWarranty = new SellCarWarranty();
		newSellCarWarranty.setSellCarDetail(sellCarDetailForSellCarMetaBD);
		newSellCarWarranty.setIdMetaValue(2L);
		newSellCarWarranty.setWarrantyValue(carWarrantyPriceBD.getOneYearPrice());
		sellCarWarrantyRepo.save(newSellCarWarranty);

		// Se registran en SellCarDimple los InspectionDimple que
		// tengan el idInspection asociado
		List<InspectionDimple> listInpectionDimplesBD = inspectionDimpleRepo.findByIdInspection(inspectionBD.get().getId());
		sellCarDimpleRepo.deleteAll(sellCarDimpleRepo.findByIdSellCarDetail(sellCarDetailForSellCarMetaBD.getId()));
		for (InspectionDimple inspectionDimple : listInpectionDimplesBD) {
		    SellCarDimple newSellCarDimple = new SellCarDimple();
		    // newSellCarDimple.setIdSellCarDetail(sellCarDetailForSellCarMetaBD.getId());
		    newSellCarDimple.setSellCarDetail(sellCarDetailForSellCarMetaBD);
		    newSellCarDimple.setCoordanateX(inspectionDimple.getCoordinateX());
		    newSellCarDimple.setCoordanateY(inspectionDimple.getCoordinateY());
		    newSellCarDimple.setDimpleImageUrl(inspectionDimple.getDimpleImage());
		    if (inspectionDimple.getIdDimpleType() == null) {
			newSellCarDimple.setDimpleName(inspectionDimple.getDimpleName());
		    } else {
			newSellCarDimple.setDimpleName(dimpleTypeRepo.findById(inspectionDimple.getIdDimpleType()).get().getName());
		    }

		    if (inspectionDimple.getDimpleLocation() == 1) {
			newSellCarDimple.setDimpleType("ext");
		    } else {
			newSellCarDimple.setDimpleType("inner");
		    }
		    sellCarDimpleRepo.save(newSellCarDimple);
		}

		// Se guarda en la entity ApoloInspectionPointCar por cada
		// registro en ApoloInspectionPointsItem que cumpla las
		// condiciones para que Apolo lea la info (Codigo temporal)
		List<ApoloInspectionPointsItem> listApoloInspectionPointsItemBD = apoloInspectionPointsItemsRepo.findByActiveTrueAndDeselectTrue();
		if (listApoloInspectionPointsItemBD != null) {
		    List<ApoloInspectionPointCar> listApoloInspectionPointCarBD = apoloInspectionPointCarRepo.findByIdSellCarDetail(sellCarDetailForSellCarMetaBD.getId());
		    if (listApoloInspectionPointCarBD != null && !listApoloInspectionPointCarBD.isEmpty()) {
			apoloInspectionPointCarRepo.deleteAll(listApoloInspectionPointCarBD);
		    }
		    for (ApoloInspectionPointsItem apoloInspectionPointsItemActual : listApoloInspectionPointsItemBD) {
			ApoloInspectionPointCar newApoloInspectionPointCar = new ApoloInspectionPointCar();
			newApoloInspectionPointCar.setIdSellCarDetail(sellCarDetailForSellCarMetaBD.getId());
			newApoloInspectionPointCar.setApoloInspectionPointsItem(apoloInspectionPointsItemActual);
			newApoloInspectionPointCar.setStatus(true);
			apoloInspectionPointCarRepo.save(newApoloInspectionPointCar);
		    }
		}

		// Se guarda en la entity ApoloInspectionPointsHistoryCar
		// por cada en registro en listInspectionPointsHistoryBD que
		// cumpla con las condiciones para que Apolo la info (Codigo
		// Temporal)
		List<ApoloInspectionPointsHistory> listInspectionPointsHistoryBD = apoloInspectionPointsHistoryRepo.findByActiveTrueAndDeselectTrue();
		if (listInspectionPointsHistoryBD != null) {
		    List<ApoloInspectionPointsHistoryCar> listApoloInspectionPointsHistoryBD = apoloInspectionPointsHistoryCarRepo.findByIdSellCarDetail(sellCarDetailForSellCarMetaBD.getId());
		    if (listApoloInspectionPointsHistoryBD != null) {
			apoloInspectionPointsHistoryCarRepo.deleteAll(listApoloInspectionPointsHistoryBD);
		    }
		    for (ApoloInspectionPointsHistory apoloInspectionPointsHistoryActual : listInspectionPointsHistoryBD) {
			ApoloInspectionPointsHistoryCar newApoloInspectionPointsHistoryCar = new ApoloInspectionPointsHistoryCar();
			newApoloInspectionPointsHistoryCar.setIdSellCarDetail(sellCarDetailForSellCarMetaBD.getId());
			// newApoloInspectionPointsHistoryCar.setIdApoloinspectionPointHistory(apoloInspectionPointsHistoryActual.getId());
			newApoloInspectionPointsHistoryCar.setApoloInspectionPointsHistory(apoloInspectionPointsHistoryActual);
			newApoloInspectionPointsHistoryCar.setStatus(true);
			apoloInspectionPointsHistoryCarRepo.save(newApoloInspectionPointsHistoryCar);
		    }

		}
	    }
	    // }

	    // Se registran o actualizan los descuentos acociaodos a la
	    // inspeccion
	    for (InspectionDiscountDTO inspectionDiscountDTOActual : inspectionSummaryDataDTOActual.getLisInspectionDiscountDTO()) {
		InspectionDiscount inpectionDiscountBD = inspectionDiscountRepo.findByIdInspectionAndDiscountType(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection(), inspectionDiscountDTOActual.getDiscountType());
		if (inpectionDiscountBD == null) {
		    InspectionDiscount newInspectionDiscount = new InspectionDiscount();
		    newInspectionDiscount.setIdInspection(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
		    newInspectionDiscount.setInspectorEmail(inspectionSummaryDataDTOActual.getSummaryDTO().getInspectorEmail());
		    newInspectionDiscount.setDiscountType(inspectionDiscountDTOActual.getDiscountType());
		    newInspectionDiscount.setDiscountAmount(inspectionDiscountDTOActual.getDiscountAmount());
		    newInspectionDiscount.setPaidByKavak(inspectionDiscountDTOActual.isPaidByKavak());
		    if (inspectionDiscountDTOActual.isRemoved() != null) {
			if (inspectionDiscountDTOActual.isRemoved()) {
			    newInspectionDiscount.setRemoved(true);
			    newInspectionDiscount.setRemovedReason(inspectionDiscountDTOActual.getRemovedReason());
			}
		    }
		    inspectionDiscountRepo.save(newInspectionDiscount);
		} else {
		    // inpectionDiscountBD.setIdInspection(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
		    inpectionDiscountBD.setPaidByKavak(inspectionDiscountDTOActual.isPaidByKavak());
		    if (inspectionDiscountDTOActual.getDiscountAmount() != null && inspectionDiscountDTOActual.getDiscountAmount() > 0) {
			inpectionDiscountBD.setDiscountAmount(inspectionDiscountDTOActual.getDiscountAmount());
		    }
		    if (inspectionDiscountDTOActual.isRemoved() != null) {
			if (inspectionDiscountDTOActual.isRemoved()) {
			    inpectionDiscountBD.setRemoved(true);
			    inpectionDiscountBD.setRemovedReason(inspectionDiscountDTOActual.getRemovedReason());
			}
		    }
		    inspectionDiscountRepo.save(inpectionDiscountBD);
		}
	    }

	    // Se registran o actualizan las reparaciones asociados a la
	    // inspeccion
	    InspectionRepair inspectionRepairBD;
	    for (InspectionRepairDTO inspectionRepairDTOActual : inspectionSummaryDataDTOActual.getListInspectionRepairDTO()) {
		inspectionRepairBD = null;

		// TODO Borrar luego de pruebas de App Inspección
		LogService.logger.info("inspection_id->" + inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection() + " repair_category->" + inspectionRepairDTOActual.getIdInspectionRepairCategory() + " repair_subcategory->" + inspectionRepairDTOActual.getIdInspectionRepairSubCategory() + " repair_type_id->" + inspectionRepairDTOActual.getIdInspectionRepairItem());

		if (!(inspectionRepairDTOActual.getIdInspectionRepairCategory() == null || inspectionRepairDTOActual.getIdInspectionRepairSubCategory() == null || inspectionRepairDTOActual.getIdInspectionRepairItem() == null)) {
		    inspectionRepairBD = inspectionRepairRepo.findIfExist(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection(), inspectionRepairDTOActual.getIdInspectionRepairCategory(), inspectionRepairDTOActual.getIdInspectionRepairSubCategory(), inspectionRepairDTOActual.getIdInspectionRepairItem(), inspectionRepairDTOActual.getIdInspectionPointCar());
		}
		if (inspectionRepairBD != null) {
		    // inspectionRepairBD.setIdInspection(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
		    // inspectionRepairBD.setIdInspectionRepairCategory(inspectionRepairDTOActual.getIdInspectionRepairCategory());
		    // inspectionRepairBD.setIdInspectionRepairSubCategory(inspectionRepairDTOActual.getIdInspectionRepairSubCategory());
		    // inspectionRepairBD.setIdInspectionRepairItem(inspectionRepairDTOActual.getIdInspectionRepairItem());
		    inspectionRepairBD.setRepairAmount(inspectionRepairDTOActual.getRepairAmount().longValue());
		    // inspectionRepairBD.setIdInspectionPointCar(inspectionRepairDTOActual.getIdInspectionPointCar());
		    inspectionRepairBD.setPaidByKavak(inspectionRepairDTOActual.isPaidByKavak());
		    inspectionRepairBD.setInspectorEmail(inspectionRepairDTOActual.getInspectorEmail());
		    if (!inspectionRepairDTOActual.getRepairImage().equalsIgnoreCase(Constants.NO_PHOTO_INSPECTOR)) {
			String nameImgRepair = Calendar.getInstance().getTimeInMillis() + "_repair";
			KavakUtils.generateFile(inspectionRepairDTOActual.getRepairImage(), nameImgRepair, null, Constants.FILE_TYPE_JPG,false);
			inspectionRepairBD.setRepairImage(Constants.NAME_BASE_IMAGE_BD + nameImgRepair);
		    } else {
			inspectionRepairBD.setRepairImage(Constants.NO_PHOTO_INSPECTOR);
		    }
		    if (inspectionRepairDTOActual.getRepairOtherText() == null || inspectionRepairDTOActual.getRepairOtherText().isEmpty()) {
			inspectionRepairBD.setRepairOtherText(Constants.NO_VALUE_BD);
		    } else {
			inspectionRepairBD.setRepairOtherText(inspectionRepairDTOActual.getRepairOtherText());
		    }
		    inspectionRepairBD.setInspectionTypeId(inspectionSummaryDataDTOActual.getSummaryDTO().getInspectionTypeId());
		    inspectionRepairRepo.save(inspectionRepairBD);
		} else {
		    Optional<InspectionPointCar> inspectionPointCarBD = inspectionPointCarRepo.findById(inspectionRepairDTOActual.getIdInspectionPointCar());
		    InspectionRepair newInspectionRepairActual = new InspectionRepair();
		    newInspectionRepairActual.setIdInspection(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
		    newInspectionRepairActual.setIdInspectionRepairCategory(inspectionRepairDTOActual.getIdInspectionRepairCategory());
		    newInspectionRepairActual.setIdInspectionRepairSubCategory(inspectionRepairDTOActual.getIdInspectionRepairSubCategory());
		    newInspectionRepairActual.setIdInspectionRepairItem(inspectionRepairDTOActual.getIdInspectionRepairItem());
		    newInspectionRepairActual.setRepairAmount(inspectionRepairDTOActual.getRepairAmount().longValue());
		    // newInspectionRepairActual.setIdInspectionPointCar(inspectionRepairDTOActual.getIdInspectionPointCar());
		    newInspectionRepairActual.setPaidByKavak(inspectionRepairDTOActual.isPaidByKavak());
		    newInspectionRepairActual.setRepairImage(inspectionRepairDTOActual.getRepairImage());
		    newInspectionRepairActual.setInspectorEmail(inspectionRepairDTOActual.getInspectorEmail());
		    if (!inspectionRepairDTOActual.getRepairImage().equalsIgnoreCase(Constants.NO_PHOTO_INSPECTOR)) {
			String nameImgRepair = Calendar.getInstance().getTimeInMillis() + "_repair";
			KavakUtils.generateFile(inspectionRepairDTOActual.getRepairImage(), nameImgRepair, null, Constants.FILE_TYPE_JPG, false);
			newInspectionRepairActual.setRepairImage(Constants.NAME_BASE_IMAGE_BD + nameImgRepair);
		    } else {
			newInspectionRepairActual.setRepairImage(Constants.NO_PHOTO_INSPECTOR);
		    }
		    if (inspectionRepairDTOActual.getRepairOtherText() == null || inspectionRepairDTOActual.getRepairOtherText().isEmpty()) {
			newInspectionRepairActual.setRepairOtherText(Constants.NO_VALUE_BD);
		    } else {
			newInspectionRepairActual.setRepairOtherText(inspectionRepairDTOActual.getRepairOtherText());
		    }
		    newInspectionRepairActual.setInspectionTypeId(inspectionSummaryDataDTOActual.getSummaryDTO().getInspectionTypeId());
		    if(inspectionPointCarBD.isPresent()) {
			newInspectionRepairActual.setInspectionPointCar(inspectionPointCarBD.get());
		    }
		    inspectionRepairRepo.save(newInspectionRepairActual);
		}
	    }
	    if (inspectionSummaryDataDTOActual.getListInspectionOfferDTO() != null) {
		for (InspectionOfferDTO inspectionOfferDTO : inspectionSummaryDataDTOActual.getListInspectionOfferDTO()) {
		    InspectionSummaryOffer newInspectionSummaryOffer = new InspectionSummaryOffer();
		    newInspectionSummaryOffer.setIdOfferType(inspectionOfferDTO.getOfferTypeId());
		    newInspectionSummaryOffer.setInspectorEmail(inspectionSummaryO.getInspectorEmail());
		    newInspectionSummaryOffer.setOfferAmount(inspectionOfferDTO.getOfferAmount());
		    newInspectionSummaryOffer.setCreationDate(new Timestamp(System.currentTimeMillis()));
		    newInspectionSummaryOffer.setIdInspectionSummary(inspectionSummaryO.getId());
		    newInspectionSummaryOffer.setIdInspection(inspectionSummaryO.getIdInspection());
		    inspectionSummaryOfferRepo.save(newInspectionSummaryOffer);
		}
	    }

	    Optional<Inspection> inspectionActual = inspectionRepo.findById(inspectionSummaryDataDTOActual.getSummaryDTO().getIdInspection());
	    inspectionActual.get().setSendNetsuite(0L);
	    inspectionRepo.save(inspectionActual.get());
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(new GenericDTO());
	LogService.logger.info(Constants.LOG_EXECUTING_END + " [putSummary] [" + (Calendar.getInstance().getTimeInMillis() - starTime + "]"));
	return responseDTO;
    }*/

    /**
     * Metodo solo separar los nombres de features con comas
     * 
     * @author Enrique Marin
     * @param listInspectionCarFeature
     * @return List<String>
     */
    public List<String> getNamesSeparatedByCommas(List<InspectionCarFeature> listInspectionCarFeature) {
	Iterator<InspectionCarFeature> iterator = listInspectionCarFeature.iterator();
	List<String> listLabels = new ArrayList<>();
	while (iterator.hasNext()) {
	    if (!iterator.hasNext()) {
		listLabels.add(iterator.next().getFeatureLabel() + ",");
	    } else {
		listLabels.add(iterator.next().getFeatureLabel());
	    }
	}
	return listLabels;
    }

    /**
     * Consulta las razones para cancelar que esten activas (active == true)
     * 
     * @author Enrique Marin
     * @return List<InspectionCancelationReasonDTO>
     */
    public ResponseDTO getCancellationReasons() {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " [getCancellationReasons] ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	List<InspectionCancelationReason> listInspectionCancelationReasonBD = inspectionCancelationReasonRepo.findByActiveTrue();
	List<InspectionCancelationReasonDTO> listInspectionCancelationReasonDTO = new ArrayList<>();
	for (InspectionCancelationReason inspectionCancelationReasonActual : listInspectionCancelationReasonBD) {
	    listInspectionCancelationReasonDTO.add(inspectionCancelationReasonActual.getDTO());
	}
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listInspectionCancelationReasonDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " [getCancellationReasons] [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Consulta los tipos de descuentos que esten activos (active == true)
     * 
     * @author Enrique Marin
     * @return List<InspectionDiscountTypeDTO>
     */
    public ResponseDTO getDiscountTypes() {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getDiscountTypes) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	List<InspectionDiscountType> listInspectionDiscountTypeBD = inspectionDiscountTypeRepo.findByActiveTrue();
	List<InspectionDiscountTypeDTO> listInspectionDiscountTypeDTO = new ArrayList<>();
	for (InspectionDiscountType inspectionDiscountTypeActual : listInspectionDiscountTypeBD) {
	    listInspectionDiscountTypeDTO.add(inspectionDiscountTypeActual.getDTO());
	}
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listInspectionDiscountTypeDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getDiscountTypes) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Consulta el resumen de una inspection acorde a la etapa indicada en
     * inspectionTypeId
     * 
     * @param id
     *            el id de ls inspeccion a buscar
     * @param inspectionTypeId
     *            id del tipo de inspeccion o etapa de la misma
     */
    public ResponseDTO getSummary(Long id, Long inspectionTypeId) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getSummary) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();

	List<InspectionSummary> listInspectionSummaryBD = inspectionSummaryRepo.findByIdInspectionAndInspectionTypeId(id, inspectionTypeId);
	List<InspectionSummaryDTO> listInspectionSummaryDTO = new ArrayList<>();
	for (InspectionSummary inspectionSummaryActual : listInspectionSummaryBD) {
	    listInspectionSummaryDTO.add(inspectionSummaryActual.getDTO());
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listInspectionSummaryDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getSummary) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;

    }
}
