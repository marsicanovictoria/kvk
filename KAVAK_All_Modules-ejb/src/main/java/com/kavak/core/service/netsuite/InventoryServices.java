package com.kavak.core.service.netsuite;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.kavak.core.dto.netsuite.InventoryDTO;
import com.kavak.core.dto.netsuite.MakeAndModelDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.CatalogueCarStatusEnum;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.CarData;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.AppointmentScheduleDate;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.AppointmentScheduleDateRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

@Stateless
public class InventoryServices {

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;

    @Inject
    private CarDataRepository carDataRepo;
    
    @Inject
    private AppointmentScheduleDateRepository scheduleDateRepo;
    
    @Inject
    private SaleCheckpointRepository saleCheckpointRepo;

    /**
     * Consulta los carros que se tienen en inventario sin importar si estatus (disponible, reservado, vendido, etc)
     * @author Antony Casanova
     * @param N/A
     * @return List<CatalogueCarResponseDTO>
     */
    
    @SuppressWarnings("finally")
    public ResponseDTO getInventoryCars(){

        ResponseDTO responseListDTO = new ResponseDTO();
        InventoryDTO inventoryDTO = null;
        InventoryDTO preloadedInventoryDTO = null;
        List<InventoryDTO> listInventory = new ArrayList<>();
        long leadMinervaPLus = 1000;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
        try {

            LogService.logger.info("Se procede a consultar el inventario Kavak");

            List<SellCarDetail> listSellCarDetailBD = sellCarDetailRepo.getInventoryCars();
            List<SellCarDetail> listPreloadedCarsBD = sellCarDetailRepo.getPreloadedCars();

            if (listSellCarDetailBD.size() > 0){

                //LogService.logger.info("Total de autos por registrar/actualizar: " + listSellCarDetailBD.size());
                Long currentCarId = 0L;
                
                for (SellCarDetail sellCarDetail : listSellCarDetailBD){

                	if (currentCarId == 0 || currentCarId != sellCarDetail.getId()){
                		
                		LogService.logger.info("Auto registrado a registrar/actualizar: " + sellCarDetail.getId());
                		
                		 if ((sellCarDetail.getSku()!= null)&&(sellCarDetail.getSendNetsuite() == 0)){
                             inventoryDTO = new InventoryDTO();

                             CarData carDataBD = carDataRepo.findBySku(sellCarDetail.getSku());

                             if (carDataBD != null){

                                 inventoryDTO.setId(sellCarDetail.getId());
                                 //inventoryDTO.setLeadMinervaId((sellCarDetail.getSellerId() + leadMinervaPLus));
                                 inventoryDTO.setLeadMinervaId(sellCarDetail.getSellerId());
                                 inventoryDTO.setCarKm(sellCarDetail.getCarKm());
                                 inventoryDTO.setPrice(sellCarDetail.getPrice());
                                 inventoryDTO.setCarYear(carDataBD.getCarYear().toString().trim());
                                 inventoryDTO.setCarMake(carDataBD.getCarMake());
                                 inventoryDTO.setCarModel(carDataBD.getCarModel());
                                 inventoryDTO.setCarTrim(carDataBD.getFullVersion());
                                 inventoryDTO.setSku(carDataBD.getSku());
                                 inventoryDTO.setCertified(sellCarDetail.isCertified());
                                 inventoryDTO.setValide(sellCarDetail.isValide());
                                 inventoryDTO.setActive(sellCarDetail.isActive());
                                 inventoryDTO.setSale(sellCarDetail.isSale());
                                 inventoryDTO.setCarColor(sellCarDetail.getCarColor());
                                 inventoryDTO.setUberType(sellCarDetail.getUberType());
                                 inventoryDTO.setTransmission(sellCarDetail.getTransmission());
                                 inventoryDTO.setNetsuiteItemId(sellCarDetail.getNetsuiteItemId());
                                 inventoryDTO.setStatus(KavakUtils.getStatusCar(sellCarDetail));
                                 inventoryDTO.setWishlist(Long.parseLong(carDataBD.getWishList()));
                                 inventoryDTO.setDownpayment(false);
                                 inventoryDTO.setFakeBooking(sellCarDetail.getFakeBooking());
                                 
                                 if (sellCarDetail.getNetsuiteItemId() != null){
                                     inventoryDTO.setMinervaOfferId(sellCarDetail.getNetsuiteItemId());
                                 }
                                 
                                 if (sellCarDetail.getDeliveryDays() != null){
 	                            	inventoryDTO.setDeliveryDays(sellCarDetail.getDeliveryDays());
                                 }
                                 
                                 if (sellCarDetail.getPostDate() != null){
                                	 Date registerDateUnclean = formatter.parse(sellCarDetail.getPostDate());
                                	 inventoryDTO.setPostDate(formatter.format(registerDateUnclean));
                                 }
   
                                 if(sellCarDetail.getBookingDate() != null){
                                	 inventoryDTO.setBookingDate(formatter.format(sellCarDetail.getBookingDate()));
                                 }
                                 
                                 if(sellCarDetail.getCancelDate() != null){
                                	 inventoryDTO.setCancelDate(formatter.format(sellCarDetail.getCancelDate()));
                                 }
                                 
                                 if(sellCarDetail.getPreloadDate() != null){
                                	 Date preloadDateUnclean = formatter.parse(sellCarDetail.getPreloadDate());
                                	 inventoryDTO.setPreloadDate(formatter.format(preloadDateUnclean));
                                 }
                                 
                                 if(sellCarDetail.getSaleDate() != null){
                                	 inventoryDTO.setSaleDate(formatter.format(sellCarDetail.getSaleDate()));
                                 }
                                 
                                 if (inventoryDTO.getStatus() == CatalogueCarStatusEnum.BOOKED || inventoryDTO.getStatus() == CatalogueCarStatusEnum.SOLD){
                                 	List<SaleCheckpoint> sale = saleCheckpointRepo.findByCarId(sellCarDetail.getId());
                                 	
                                 	if(sale.size() > 0){
                                 		
                                 		for(SaleCheckpoint saleCheck : sale){
                                 			
                                 			if (saleCheck.getTypePaymentMethodTypeId() != null && ((saleCheck.getTypePaymentMethodTypeId().equals("143"))||(saleCheck.getTypePaymentMethodTypeId().equals("250")))){
                                 				inventoryDTO.setDownpayment(true);
                                 			}
                                 			break;
                                 		}
                                 	}else{
                                 		LogService.logger.info("No se tiene registros del id del auto " + sellCarDetail.getId() + " en compras_checkpoints");
                                 	}
                                 }
                                 listInventory.add(inventoryDTO);
                             }
                         }
                		 currentCarId = sellCarDetail.getId();
                	}
                }
            }else{
                LogService.logger.info("Total de autos por registrar/actualizar: 0");
            }


            if (listPreloadedCarsBD.size() > 0){

                LogService.logger.info("Total de autos precargados por registrar/actualizar: " + listPreloadedCarsBD.size());

                Long currentPreloadCardId = 0L;
                for (SellCarDetail perloadedSellCarDetail : listPreloadedCarsBD){

               	 	if (currentPreloadCardId == 0 || currentPreloadCardId != perloadedSellCarDetail.getId()){
                	
               	 		LogService.logger.info("Auto precargado a registrar/actualizar: " + perloadedSellCarDetail.getId());
               	 	
	                    if ((perloadedSellCarDetail.getSku()!= null)&&(perloadedSellCarDetail.getSendNetsuite() == 0)){
	                        preloadedInventoryDTO = new InventoryDTO();
	
	                        CarData carDataBD = carDataRepo.findBySku(perloadedSellCarDetail.getSku());
	
	                        if (carDataBD != null){
	                            preloadedInventoryDTO.setId(perloadedSellCarDetail.getId());
	                            //preloadedInventoryDTO.setLeadMinervaId((perloadedSellCarDetail.getSellerId() + leadMinervaPLus));
	                            preloadedInventoryDTO.setLeadMinervaId(perloadedSellCarDetail.getSellerId());
	                            preloadedInventoryDTO.setCarKm(perloadedSellCarDetail.getCarKm());
	                            preloadedInventoryDTO.setPrice(perloadedSellCarDetail.getPrice());
	                            preloadedInventoryDTO.setCarYear(carDataBD.getCarYear().toString().trim());
	                            preloadedInventoryDTO.setCarMake(carDataBD.getCarMake());
	                            preloadedInventoryDTO.setCarModel(carDataBD.getCarModel());
	                            preloadedInventoryDTO.setCarTrim(carDataBD.getFullVersion());
	                            preloadedInventoryDTO.setSku(carDataBD.getSku());
	                            preloadedInventoryDTO.setCertified(perloadedSellCarDetail.isCertified());
	                            preloadedInventoryDTO.setValide(perloadedSellCarDetail.isValide());
	                            preloadedInventoryDTO.setActive(perloadedSellCarDetail.isActive());
	                            preloadedInventoryDTO.setSale(perloadedSellCarDetail.isSale());
	                            preloadedInventoryDTO.setCarColor(perloadedSellCarDetail.getCarColor());
	                            preloadedInventoryDTO.setUberType(perloadedSellCarDetail.getUberType());
	                            preloadedInventoryDTO.setTransmission(perloadedSellCarDetail.getTransmission());
	                            preloadedInventoryDTO.setNetsuiteItemId(perloadedSellCarDetail.getNetsuiteItemId());
	                            preloadedInventoryDTO.setStatus(KavakUtils.getStatusCar(perloadedSellCarDetail));
	                            preloadedInventoryDTO.setWishlist(Long.parseLong(carDataBD.getWishList()));
	                            preloadedInventoryDTO.setFakeBooking(perloadedSellCarDetail.getFakeBooking());
	
	                            if (perloadedSellCarDetail.getNetsuiteItemId() != null){
	                                preloadedInventoryDTO.setMinervaOfferId(perloadedSellCarDetail.getNetsuiteItemId());
	                            }
	                            
	                            if (perloadedSellCarDetail.getDeliveryDays() != null){
	                            	preloadedInventoryDTO.setDeliveryDays(perloadedSellCarDetail.getDeliveryDays());
	                            }
	                            
                                if (preloadedInventoryDTO.getStatus() == CatalogueCarStatusEnum.BOOKED || preloadedInventoryDTO.getStatus() == CatalogueCarStatusEnum.SOLD){
                                 	List<SaleCheckpoint> salepreload = saleCheckpointRepo.findByCarId(perloadedSellCarDetail.getId());
                                 	
                                 	if(salepreload.size() > 0){
                                 		
                                 		for(SaleCheckpoint saleCheck : salepreload){
                                 			
                                 			if ((saleCheck.getTypePaymentMethodTypeId().equals("143"))||(saleCheck.getTypePaymentMethodTypeId().equals("250"))){
                                 				inventoryDTO.setDownpayment(true);
                                 			}
                                 			break;
                                 		}
                                 	}else{
                                 		LogService.logger.info("No se tiene registros del id del auto " + perloadedSellCarDetail.getId() + " en compras_checkpoints");
                                 	}
                                 }
	
	                            listInventory.add(preloadedInventoryDTO);
	                        }
	                    }
                    
	                    currentPreloadCardId = perloadedSellCarDetail.getId();
               	 	}
                    
                }
            }else{
                LogService.logger.info("Total de autos precargados por registrar/actualizar: 0");
            }
            
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseListDTO.setCode(enumResult.getValue());
            responseListDTO.setStatus(enumResult.getStatus());
            responseListDTO.setData(listInventory);
            responseListDTO.setNetsuiteMethod(Constants.INVENTORY_METHOD);
        } catch (Exception e) {
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0500.toString());
            responseListDTO.setCode(enumResult.getValue());
            responseListDTO.setStatus(enumResult.getStatus());
            responseListDTO.setData(listInventory);
            LogService.logger.error("Error consultando el inventario Kavak", e);
            e.printStackTrace();
        }finally{
            return responseListDTO;
        }
    }

    @SuppressWarnings("finally")
    public ResponseDTO putSendStatusInventory(String inventoryIds){
        ResponseDTO responseDTO = new ResponseDTO();
        long sentValue = 1;

        try {

            if (!inventoryIds.isEmpty()){
                LogService.logger.info("Actualizando estatus de envio a inventario Kavak con Stock Ids: " + inventoryIds);

                responseDTO = new ResponseDTO();
                List<Long> listInventoryIds = new ArrayList<Long>();

                for (String s : inventoryIds.split(",")){
                    listInventoryIds.add(Long.parseLong(s));
                }

                List<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findByIds(listInventoryIds);

                if (sellCarDetailBD.size() > 0){
                    for (SellCarDetail sellcarDetail : sellCarDetailBD){
                        sellcarDetail.setSendNetsuite(sentValue);
                    }

                    sellCarDetailRepo.saveAll(sellCarDetailBD);
                }

                LogService.logger.info("Actualizando estatus de envio a inventario Kavak: exitoso");

            }else{
                LogService.logger.info("No hay stockIds por actualizar estatus de enviado en Minerva ");
            }

            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());

        } catch (Exception e) {
            LogService.logger.error("Error al intentar actualizar el estatus de envío del inventario Kavak, " + e);
            e.printStackTrace();
        }finally{
            return responseDTO;
        }
    }
    
    public ResponseDTO getLastScheduledAppointment(Long stockId){
    	ResponseDTO responseDTO = null;
    	
    	LogService.logger.info("StockId obtenido para consultar: " + stockId);
    	
    	try {
    		if (stockId != null){
    			LogService.logger.info("Se procede a consultar la ultima fecha de cita agendada para el auto " + stockId);
    			
    			Optional<SellCarDetail> sellCarBD = sellCarDetailRepo.findById(stockId);
    			
    			if (sellCarBD.isPresent()){
    				
    	    		responseDTO = new ResponseDTO();
    	    		responseDTO.setData("");
    	    		
    	    		List<AppointmentScheduleDate> listScheduledDateBD = scheduleDateRepo.findMaxDateByCarId(stockId);
    	    		
    	    		if (!listScheduledDateBD.isEmpty()){
        	    		AppointmentScheduleDate scheduleDateBD = listScheduledDateBD.get(0);
        	    		
        	    		if (scheduleDateBD != null){
        	    			responseDTO.setData(scheduleDateBD.getVisitDate());
        	    		}else{
        	    			LogService.logger.info("El stockId " + stockId + " No tiene citas agendadas en Minerva.");
        	    		}
    	    		}else{
    	    			LogService.logger.info("El stockId " + stockId + " No tiene citas agendadas en Minerva.");
    	    		}

    			}else{
    				LogService.logger.error("El stockId " + stockId + " indicado no existe en Minerva.");
    			}
    		}else{
    			LogService.logger.error("El stockId enviado es nulo, se descarta llamado al servicio.");
    		}
    		
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
    		
		} catch (Exception e) {
            LogService.logger.error("Error al intentar consultar la ultima fecha de cita agendada del stockId: " + stockId+ ", " + e);
            e.printStackTrace();
		}
    
    	return responseDTO;
    }
    
    public ResponseDTO matchMakeAndModel(String keyWord){
    	ResponseDTO responseDTO = null;
    	List<MakeAndModelDTO> listMakeAndModel = null;
    	
    	try {
    		responseDTO = new ResponseDTO();
    		List<CarData> listCarDataBD = carDataRepo.findMakeAndModelByKeyWord(keyWord);
    		
    		if (listCarDataBD != null){
    			LogService.logger.info("Se encontraron " + listCarDataBD.size() + " registros con el keywork " + keyWord);
    			
    			listMakeAndModel = new ArrayList<>();
    			
    			for (CarData carData : listCarDataBD){
    				LogService.logger.info("Marca: " + carData.getCarMake() + " Modelo: " + carData.getCarModel());
    				
    				MakeAndModelDTO makeModel = new MakeAndModelDTO();
    				makeModel.setId(carData.getId());
    				makeModel.setCarMake(carData.getCarMake());
    				makeModel.setCarModel(carData.getCarModel());
    				
    				listMakeAndModel.add(makeModel);
    			}
    			
    		}else{
    			LogService.logger.info("No se encontraron marcas con el keyWord " + keyWord);
    		}
    		
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(listMakeAndModel);
    		
		} catch (Exception e) {
            LogService.logger.error("Error al intentar consultar las marcas con su modelo que inicien por la letra " + keyWord + ", " + e);
            e.printStackTrace();
            
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(null);
		}
    	
    	
    	return responseDTO;
   }
    
    
}
