package com.kavak.core.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.kavak.core.dto.response.FacebookResponseDTO;
import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.response.GoogleResponseDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.response.PutUserResponseDTO;
import com.kavak.core.dto.response.RegisterWithoutPasswordResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.dto.response.UserRequestDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

@Stateless
public class UserServices {

    @Inject
    private UserRepository userRepo;

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private UserMetaRepository userMetaRepo;

    /**
     * Metodo que se utiliza para identificar el tipo de Login que se va a realizar
     *
     * @author Oscar Montilla
     * @param userRequestDTO
     *            request del modelo de User
     * @return ResponseDTO Response Generico de servicio
     * @throws MessagingException
     */

    public ResponseDTO switcherLoginUser(UserRequestDTO userRequestDTO) throws MessagingException {

	LogService.logger.info("Iniciando Metodo para detectar el login que corresponde...");

	if (userRequestDTO.getSocialNetwork() == Constants.SOCIAL_NETWOK_GOOGLE) {
	    return registerUserGoogle(userRequestDTO);
	}
	if (userRequestDTO.getSocialNetwork() == Constants.SOCIAL_NETWOK_FACEBOOK) {
	    return LoginFacebook(userRequestDTO);
	}

	if (userRequestDTO.getIsRegisterWithoutPassword() != null) {

	    if (userRequestDTO.getIsRegisterWithoutPassword()) {
		return registerWithoutPassword(userRequestDTO);
	    }
	}
	return registerUser(userRequestDTO);

    }

    /**
     * Metodo que registra un usuario, validando la informacion necesaria para su registro
     *
     * @author Oscar Montilla
     * @param userRequesDTO
     *            request del modelo de User
     * @return ResponseDTO Response Generico de servicio
     */
    public ResponseDTO registerUser(UserRequestDTO userRequesDTO) {
	LogService.logger.info("Iniciando el metodo registerUser()..");

	ResponseDTO responseDTO = new ResponseDTO();
	List<MessageDTO> listMessageDTO = new ArrayList<>();
	User userBD = userRepo.findByEmailAndRole(userRequesDTO.getEmail(), Constants.CUSTOMER_ROLE);
	String source;

	if (StringUtils.isEmpty(userRequesDTO.getSource())) {
	    source = Constants.SOURCE_IOS_APP;
	} else {
	    source = userRequesDTO.getSource();
	}

	if (userRequesDTO.getName() == null || userRequesDTO.getEmail() == null || userRequesDTO.getPassword() == null || userRequesDTO.getName().length() == 0 || userRequesDTO.getEmail().length() == 0 || userRequesDTO.getPassword().length() == 0) {

	    LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
	    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (userRequesDTO.getName() == null ? "name, " : "") + (userRequesDTO.getEmail() == null ? "email, " : "") + (userRequesDTO.getPassword() == null ? "Password, " : ""));
	    listMessageDTO.add(messageDataNull);
	    responseDTO.setListMessage(listMessageDTO);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new ArrayList<>());

	    return responseDTO;
	}

	if (userBD != null) {
	    LogService.logger.info("El email ya esta siendo utilizado..");
	    MessageDTO messageEmailrepeated = messageRepo.findByCode(MessageEnum.M0012.toString()).getRemplaceParameterDTO(userRequesDTO.getEmail());
	    listMessageDTO.add(messageEmailrepeated);
	    responseDTO.setListMessage(listMessageDTO);
	}
	if (!KavakUtils.validateEmail(userRequesDTO.getEmail())) {
	    LogService.logger.info("El formato del email es incorrecto..");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0013.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);
	}
	if (userRequesDTO.getPassword().length() < 8 || userRequesDTO.getPassword().length() > 24) {
	    LogService.logger.info("La contraseña no debe ser menor 8 caracteres ni mayor a 24..");
	    MessageDTO messagePasswordBadFormat = messageRepo.findByCode(MessageEnum.M0014.toString()).getDTO();
	    listMessageDTO.add(messagePasswordBadFormat);
	    responseDTO.setListMessage(listMessageDTO);

	}
	if (KavakUtils.validIP(userRequesDTO.getIpUsuario())) {
	    LogService.logger.info("El formato de la ip suminitrada no es el correto..");
	    MessageDTO messageIPBadFormat = messageRepo.findByCode(MessageEnum.M0015.toString()).getDTO();
	    listMessageDTO.add(messageIPBadFormat);
	    responseDTO.setListMessage(listMessageDTO);

	}
	if (responseDTO.getListMessage() != null) {
	    LogService.logger.info("Existe una lista de Menssaje de validacion, devolver los mensajes..");
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new ArrayList<>());

	} else {

	    LogService.logger.info("Se procede a guardar el registro de usuario..");
	    UserRequestDTO dataResponse = new UserRequestDTO();

	    String passwordhashed = KavakUtils.changeSaltInHash(BCrypt.hashpw(userRequesDTO.getPassword().toString(), BCrypt.gensalt(12)), 1);
	    User userSave = new User();
	    userSave.setEmail(userRequesDTO.getEmail());
	    userSave.setName(userRequesDTO.getName());
	    userSave.setUserName(userRequesDTO.getEmail());
	    userSave.setPassword(passwordhashed);
	    userSave.setRole(Constants.CUSTOMER_ROLE);
	    userSave.setActive(Constants.USER_ACTIVE);
	    userSave.setCreationDate(new Timestamp(System.currentTimeMillis()));
	    userSave.setSource(source);

	    if (userRequesDTO.getIpUsuario() != null) {

		LogService.logger.info("registrando la ip suministrada");
		userSave.setIpUsuario(userRequesDTO.getIpUsuario());

	    }

	    User newUser = userRepo.save(userSave);

	    if (userRequesDTO.getPhone() != null) {

		LogService.logger.info("Se esta registrando el numero de telefonico..");
		UserMeta userMetaSave = new UserMeta();
		userMetaSave.setUser(newUser);
		userMetaSave.setMetaKey(Constants.META_VALUE_PHONE);
		userMetaSave.setMetaValue(userRequesDTO.getPhone());

		UserMeta userMetaDB = userMetaRepo.save(userMetaSave);
		dataResponse.setPhone(userMetaDB.getMetaValue());
	    }

	    dataResponse.setId(newUser.getId());
	    dataResponse.setName(newUser.getName());
	    dataResponse.setEmail(newUser.getEmail());
	    dataResponse.setIpUsuario(newUser.getIpUsuario());
	    dataResponse.setActive(newUser.isActive());
	    dataResponse.setSource(newUser.getSource());

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(dataResponse);

	}

	return responseDTO;
    }

    /**
     * Metodo que valida el Login de un Usuario
     *
     * @author Oscar Montilla
     *
     * @param userRequestDTO
     *            requestDTO de usuario
     * @return responseDTO Response Generico de servicio
     */

    public ResponseDTO loginUser(UserRequestDTO userRequestDTO) {

	User userDB = new User();

	if (userRequestDTO.getRole() == null || userRequestDTO.getRole().length() == 0 || userRequestDTO.getRole().equals(Constants.CUSTOMER_ROLE)) {
	    LogService.logger.info("Login CUSTOMER_ROLE..");
	    userDB = userRepo.findByEmailAndRole(userRequestDTO.getEmail(), Constants.CUSTOMER_ROLE);

	} else if (!userRequestDTO.getRole().equals(Constants.CUSTOMER_ROLE)) {
	    LogService.logger.info("Login other Role..");
	    userDB = userRepo.findByEmailAndRole(userRequestDTO.getEmail(), userRequestDTO.getRole());

	}

	ResponseDTO responseDTO = new ResponseDTO();
	List<MessageDTO> listMessageDTO = new ArrayList<>();

	if (userRequestDTO.getEmail() == null || userRequestDTO.getPassword() == null || userRequestDTO.getEmail().length() == 0 || userRequestDTO.getPassword().length() == 0) {

	    LogService.logger.info("Uno de los datos requerido llega NULL o vacios, se realiza return inmediatamente..");
	    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (userRequestDTO.getName() == null ? "email, " : "") + (userRequestDTO.getPassword() == null ? "Password, " : ""));
	    listMessageDTO.add(messageDataNull);
	    responseDTO.setListMessage(listMessageDTO);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;

	}

	if (userDB == null) {

	    LogService.logger.info("El usuario no existe en Base de datos, o se encuentra desactivado..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0016.toString()).getDTO();
	    listMessageDTO.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;

	}

	if (KavakUtils.validIP(userRequestDTO.getIpUsuario())) {
	    LogService.logger.info("El formato de la ip suminitrada no es el correto..");
	    MessageDTO messageIPBadFormat = messageRepo.findByCode(MessageEnum.M0015.toString()).getDTO();
	    listMessageDTO.add(messageIPBadFormat);
	    responseDTO.setListMessage(listMessageDTO);

	}

	if (!userDB.isActive()) {

	    LogService.logger.info("El usuario" + userRequestDTO.getEmail() + "se encuentra desactivado, esta intentado entrar..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0078.toString()).getDTO();
	    listMessageDTO.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessageDTO);

	}

	if (responseDTO.getListMessage() != null) {

	    LogService.logger.info("Existe una lista de Menssaje de validacion, devolver los mensajes..");
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;

	}

	if (userDB.getPassword() != null && BCrypt.checkpw(userRequestDTO.getPassword(), KavakUtils.changeSaltInHash(userDB.getPassword(), 2))) {

	    LogService.logger.info("Los datos son correctos, puede iniciar sesion ..");
	    UserRequestDTO dataResponse = new UserRequestDTO();

	    UserMeta userMetaPhoneBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, userDB.getId());

	    UserMeta userMetaAnddressBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_ADDRESS, userDB.getId());

	    if (userMetaPhoneBD != null) {

		LogService.logger.info("agregando el telefono al response");
		dataResponse.setPhone(userMetaPhoneBD.getMetaValue());

	    }
	    if (userMetaAnddressBD != null) {

		LogService.logger.info("agregando la direccion al response");
		int countSplitAnddress = userMetaAnddressBD.getMetaValue().split("#").length;

		String concatenatedAnddress = null;

		if (countSplitAnddress > 0) {

		    concatenatedAnddress = " Calle:" + userMetaAnddressBD.getMetaValue().split("#")[0];
		}
		if (countSplitAnddress > 1) {

		    concatenatedAnddress += ". Número Exterior:" + userMetaAnddressBD.getMetaValue().split("#")[1];
		}
		if (countSplitAnddress > 2 && userMetaAnddressBD.getMetaValue().split("#")[2].length() != 0) {

		    concatenatedAnddress += ". Número Interior:" + userMetaAnddressBD.getMetaValue().split("#")[2];
		}
		if (countSplitAnddress > 3) {

		    concatenatedAnddress += ". Código Postal:" + userMetaAnddressBD.getMetaValue().split("#")[3];
		}

		dataResponse.setAddress(concatenatedAnddress);
	    }

	    if (userRequestDTO.getIpUsuario() != null) {

		LogService.logger.info("registrando la ip suministrada");
		userDB.setIpUsuario(userRequestDTO.getIpUsuario());
		User updateuserDB = userRepo.save(userDB);
		dataResponse.setIpUsuario(updateuserDB.getIpUsuario());
	    }

	    dataResponse.setId(userDB.getId());
	    dataResponse.setName(userDB.getName());
	    dataResponse.setEmail(userDB.getEmail());
	    dataResponse.setActive(userDB.isActive());
	    dataResponse.setSource(userDB.getSource());
	    
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(dataResponse);

	} else {

	    LogService.logger.info("Las credenciales son incorrectas..");
	    MessageDTO messageBadCredentials = messageRepo.findByCode(MessageEnum.M0016.toString()).getDTO();
	    listMessageDTO.add(messageBadCredentials);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	}
	return responseDTO;
    }

    /**
     * @author Oscar Montilla
     * @param userRequestDTO
     *            request del modelo de User
     * @return responseDTO Response Generico de servicio
     */

    public ResponseDTO registerUserGoogle(UserRequestDTO userRequestDTO) {
	LogService.logger.info("Iniciando el metodo registerUserGoogle()..");

	ResponseDTO responseDTO = new ResponseDTO();
	List<MessageDTO> listMessageDTO = new ArrayList<>();
	User userResponse = new User();
	String source;

	if (StringUtils.isEmpty(userRequestDTO.getSource())) {
	    source = Constants.SOURCE_IOS_APP;
	} else {
	    source = userRequestDTO.getSource();
	}

	if (userRequestDTO.getSocialNetworkToken() == null || userRequestDTO.getSocialNetworkToken().length() == 0) {
	    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + ("google_plus_user_id OR facebook_user_id, social_network_token, "));
	    listMessageDTO.add(messageDataNull);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;
	}

	GoogleResponseDTO googleResponseDTO = KavakUtils.authGoogleLogin(userRequestDTO.getSocialNetworkToken());

	if (googleResponseDTO == null) {

	    LogService.logger.info("El token suministrado no es valido..");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0017.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;
	}

	User userBD = userRepo.findByEmailAndRole(googleResponseDTO.getEmail(), Constants.CUSTOMER_ROLE);

	if (userBD != null && !userBD.isActive()) {
	    LogService.logger.info("El usuario" + googleResponseDTO.getEmail() + "se encuentra desactivado, esta intentado entrar por red social Google..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0078.toString()).getDTO();
	    listMessageDTO.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;
	}

	LogService.logger.info("validar que el usuario no tenga registrada la red social, para validar los datos de la red antes de utilizarlos...");

	if (googleResponseDTO.getEmail() == null) {

	    LogService.logger.info("validar que pueda obtener el email suministrado por la red social...");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0018.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);
	}

	if (!KavakUtils.validateEmail(googleResponseDTO.getEmail())) {

	    LogService.logger.info("validar el formato del email...");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0013.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);
	}

	if (googleResponseDTO.getName() == null) {

	    LogService.logger.info("validar el nombre suminitrador por la red social...");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0028.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);
	}

	if (KavakUtils.validIP(userRequestDTO.getIpUsuario())) {

	    LogService.logger.info("El formato de la ip suminitrada no es el correto..");
	    MessageDTO messagePasswordBadFormat = messageRepo.findByCode(MessageEnum.M0015.toString()).getDTO();
	    listMessageDTO.add(messagePasswordBadFormat);
	    responseDTO.setListMessage(listMessageDTO);

	}

	if (responseDTO.getListMessage() != null) {

	    LogService.logger.info("validar que paso las validaciones basicas ...");
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	} else {

	    if (userBD != null) {

		LogService.logger.info("Ya se tiene registro en el sistema, Se validan los datos para hacer update si es necesario...");

		UserRequestDTO dataResponse = new UserRequestDTO();

		if (userBD.getName() == null || userBD.getName().length() == 0 || userBD.getName().split("(?=\\s)").length <= 1) {
		    if (googleResponseDTO.getFamilyName() == null) {
			LogService.logger.info("Registrando el nombre, porque de google no se obtiene el apellido..");
			userBD.setName(googleResponseDTO.getName());
		    } else {
			LogService.logger.info("Registrando el nombre y el apellido..");
			userBD.setName((googleResponseDTO.getName() + " " + googleResponseDTO.getFamilyName()).toString());
		    }
		}
		if (userBD.getEmail() == null || userBD.getEmail().length() == 0) {
		    userBD.setEmail(googleResponseDTO.getEmail());
		}
		if (userBD.getUserName() == null || userBD.getUserName().length() == 0) {
		    userBD.setUserName(googleResponseDTO.getEmail());
		}
		if (userBD.getGooglePlusId() == null || userBD.getGooglePlusId().length() == 0) {
		    userBD.setGooglePlusId(googleResponseDTO.getSub());
		}

		UserMeta userMetaPhoneBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, userBD.getId());

		UserMeta userMetaAnddressBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_ADDRESS, userBD.getId());

		if (userMetaPhoneBD != null) {

		    dataResponse.setPhone(userMetaPhoneBD.getMetaValue());

		}
		if (userMetaAnddressBD != null) {

		    int countSplitAnddress = userMetaAnddressBD.getMetaValue().split("#").length;
		    String concatenatedAnddress = null;

		    if (countSplitAnddress > 0) {

			concatenatedAnddress = "Calle:" + userMetaAnddressBD.getMetaValue().split("#")[0];
		    }
		    if (countSplitAnddress > 1) {

			concatenatedAnddress += ". Número Exterior:" + userMetaAnddressBD.getMetaValue().split("#")[1];
		    }
		    if (countSplitAnddress > 2 && userMetaAnddressBD.getMetaValue().split("#")[2].length() != 0) {

			concatenatedAnddress += ". Número Interior:" + userMetaAnddressBD.getMetaValue().split("#")[2];
		    }
		    if (countSplitAnddress > 3) {

			concatenatedAnddress += ". Código Postal:" + userMetaAnddressBD.getMetaValue().split("#")[3];
		    }

		    dataResponse.setAddress(concatenatedAnddress);
		}

		userBD.setIpUsuario(userRequestDTO.getIpUsuario());
		userResponse = userRepo.save(userBD);

		dataResponse.setId(userResponse.getId());
		dataResponse.setName(userResponse.getName());
		dataResponse.setEmail(userResponse.getEmail());
		dataResponse.setIpUsuario(userResponse.getIpUsuario());

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(dataResponse);

	    } else {

		LogService.logger.info("No se tiene un regisro de la red social de Facebook, se organiza el registro para insertar en BD...");
		User userSave = new User();

		userSave.setEmail(googleResponseDTO.getEmail());
		userSave.setName(googleResponseDTO.getName());
		userSave.setUserName(googleResponseDTO.getEmail());
		userSave.setFacebookUserId(googleResponseDTO.getSub());
		userSave.setRole(Constants.CUSTOMER_ROLE);
		userSave.setCreationDate(new Timestamp(System.currentTimeMillis()));
		userSave.setActive(Constants.USER_ACTIVE);
		userSave.setSource(source);
		userSave.setIpUsuario(userRequestDTO.getIpUsuario());

		userResponse = userRepo.save(userSave);

		UserRequestDTO dataResponse = new UserRequestDTO();
		dataResponse.setId(userResponse.getId());
		dataResponse.setName(userResponse.getName());
		dataResponse.setEmail(userResponse.getEmail());
		dataResponse.setIpUsuario(userResponse.getIpUsuario());

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(dataResponse);
	    }

	}
	return responseDTO;
    }

    public ResponseDTO LoginFacebook(UserRequestDTO userRequestDTO) {
	LogService.logger.info("Iniciando el metodo LoginFacebook()..");

	ResponseDTO responseDTO = new ResponseDTO();
	List<MessageDTO> listMessageDTO = new ArrayList<>();
	User userResponse = new User();
	String source;

	if (StringUtils.isEmpty(userRequestDTO.getSource())) {
	    source = Constants.SOURCE_IOS_APP;
	} else {
	    source = userRequestDTO.getSource();
	}

	if (userRequestDTO.getSocialNetworkToken() == null || userRequestDTO.getSocialNetworkToken().length() == 0) {

	    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + ("google_plus_user_id OR facebook_user_id, social_network_token, "));
	    listMessageDTO.add(messageDataNull);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;

	}

	FacebookResponseDTO facebookResponseDTO = KavakUtils.authFacebookLogin(userRequestDTO.getSocialNetworkToken());

	if (facebookResponseDTO == null) {

	    LogService.logger.info("El token suministrado no es valido..");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0029.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;

	}

	User userBD = userRepo.findByEmailAndRole(facebookResponseDTO.getEmail(), Constants.CUSTOMER_ROLE);

	if (userBD != null && !userBD.isActive()) {

	    LogService.logger.info("El usuario" + facebookResponseDTO.getEmail() + "se encuentra desactivado, esta intentado entrar por red social Facebook..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0078.toString()).getDTO();
	    listMessageDTO.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;
	}

	LogService.logger.info("validar que el usuario no tenga registrada la red social, para validar los datos de la red antes de utilizarlos...");

	if (facebookResponseDTO.getEmail() == null) {

	    LogService.logger.info("validar que pueda obtener el email suministrado por la red social...");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0019.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);
	}

	if (!KavakUtils.validateEmail(facebookResponseDTO.getEmail())) {

	    LogService.logger.info("validar el formato del email...");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0013.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);
	}

	if (facebookResponseDTO.getName() == null) {

	    LogService.logger.info("validar el nombre suminitrador por la red social...");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0020.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);
	}

	if (KavakUtils.validIP(userRequestDTO.getIpUsuario())) {

	    LogService.logger.info("El formato de la ip suminitrada no es el correto..");
	    MessageDTO messagePasswordBadFormat = messageRepo.findByCode(MessageEnum.M0015.toString()).getDTO();
	    listMessageDTO.add(messagePasswordBadFormat);
	    responseDTO.setListMessage(listMessageDTO);

	}

	if (responseDTO.getListMessage() != null) {

	    LogService.logger.info("validar que paso las validaciones basicas ...");
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	} else {

	    if (userBD != null) {

		UserRequestDTO dataResponse = new UserRequestDTO();
		LogService.logger.info("Ya se tiene registro en el sistema, Se validan los datos para hacer update si es necesario...");

		if (userBD.getName() == null || userBD.getName().length() == 0 || userBD.getName().split("(?=\\s)").length <= 1 || (!userBD.getName().equals(facebookResponseDTO.getName()) && userBD.getName().split("(?=\\s)").length < facebookResponseDTO.getName().split("(?=\\s)").length)) {
		    LogService.logger.info("Guardando o actualizando nombre de usuario obtenido de la red social...");
		    userBD.setName(facebookResponseDTO.getName());
		}
		if (userBD.getEmail() == null || userBD.getEmail().length() == 0) {
		    userBD.setEmail(facebookResponseDTO.getEmail());
		}
		if (userBD.getUserName() == null || userBD.getUserName().length() == 0) {
		    userBD.setUserName(facebookResponseDTO.getEmail());
		}
		if (userBD.getFacebookUserId() == null || userBD.getFacebookUserId().length() == 0) {
		    userBD.setFacebookUserId(facebookResponseDTO.getId());
		}

		UserMeta userMetaPhoneBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, userBD.getId());

		UserMeta userMetaAnddressBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_ADDRESS, userBD.getId());

		if (userMetaPhoneBD != null) {

		    dataResponse.setPhone(userMetaPhoneBD.getMetaValue());

		}
		if (userMetaAnddressBD != null) {

		    int countSplitAnddress = userMetaAnddressBD.getMetaValue().split("#").length;
		    String concatenatedAnddress = null;

		    if (countSplitAnddress > 0) {
			concatenatedAnddress = " Calle:" + userMetaAnddressBD.getMetaValue().split("#")[0];
		    }
		    if (countSplitAnddress > 1) {

			concatenatedAnddress += ". Número Exterior:" + userMetaAnddressBD.getMetaValue().split("#")[1];
		    }
		    if (countSplitAnddress > 2 && userMetaAnddressBD.getMetaValue().split("#")[2].length() != 0) {

			concatenatedAnddress += ". Número Interior:" + userMetaAnddressBD.getMetaValue().split("#")[2];
		    }
		    if (countSplitAnddress > 3) {

			concatenatedAnddress += ". Código Postal:" + userMetaAnddressBD.getMetaValue().split("#")[3];
		    }

		    dataResponse.setAddress(concatenatedAnddress);
		}

		userBD.setIpUsuario(userRequestDTO.getIpUsuario());

		userResponse = userRepo.save(userBD);

		dataResponse.setId(userResponse.getId());
		dataResponse.setName(userResponse.getName());
		dataResponse.setEmail(userResponse.getEmail());
		dataResponse.setIpUsuario(userResponse.getIpUsuario());

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(dataResponse);

	    } else {

		LogService.logger.info("No se tiene un regisro de la red social de Facebook, se organiza el registro para insertar en BD...");
		User userSave = new User();

		userSave.setEmail(facebookResponseDTO.getEmail());
		userSave.setName(facebookResponseDTO.getName());
		userSave.setUserName(facebookResponseDTO.getEmail());
		userSave.setFacebookUserId(facebookResponseDTO.getId());
		userSave.setRole(Constants.CUSTOMER_ROLE);
		userSave.setCreationDate(new Timestamp(System.currentTimeMillis()));
		userSave.setActive(Constants.USER_ACTIVE);
		userSave.setSource(source);
		userSave.setIpUsuario(userRequestDTO.getIpUsuario());

		userResponse = userRepo.save(userSave);

		UserRequestDTO dataResponse = new UserRequestDTO();
		dataResponse.setId(userResponse.getId());
		dataResponse.setName(userResponse.getName());
		dataResponse.setEmail(userResponse.getEmail());
		dataResponse.setIpUsuario(userResponse.getIpUsuario());

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(dataResponse);
	    }

	}

	return responseDTO;
    }

    /**
     * Metodo que genera la constraseña de un usuario solo con el email y nombre
     *
     * @author Oscar Montilla
     * @param userRequestDTO
     *            resquest del servicio
     * @return ResponseDTO response generico de servicio.
     * @throws MessagingException
     */
    private ResponseDTO registerWithoutPassword(UserRequestDTO userRequestDTO) throws MessagingException {

	ResponseDTO responseDTO = new ResponseDTO();
	List<MessageDTO> listMessageDTO = new ArrayList<>();
	String source;

	if (userRequestDTO.getName() == null || userRequestDTO.getName().length() == 0 || userRequestDTO.getEmail() == null || userRequestDTO.getEmail().length() == 0) {

	    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (userRequestDTO.getName() == null ? "name, " : "") + (userRequestDTO.getEmail() == null ? "email, " : ""));
	    listMessageDTO.add(messageDataNull);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;

	}
	User userBD = userRepo.findByEmailAndRole(userRequestDTO.getEmail(), Constants.CUSTOMER_ROLE);

	if (StringUtils.isEmpty(userRequestDTO.getSource())) {
	    source = Constants.SOURCE_IOS_APP;
	} else {
	    source = userRequestDTO.getSource();
	}

	if (userBD != null) {

	    if (userBD.isActive() == Constants.USER_ACTIVE) {

		RegisterWithoutPasswordResponseDTO registerWithoutPasswordResponseDTO = new RegisterWithoutPasswordResponseDTO();

		registerWithoutPasswordResponseDTO.setId(userBD.getId());
		registerWithoutPasswordResponseDTO.setEmail(userBD.getEmail());

		UserMeta userMetaPhoneBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, userBD.getId());

		if (userRequestDTO.getVersion() == null || userRequestDTO.getVersion() == 1L) {

		    registerWithoutPasswordResponseDTO.setId(userBD.getId());
		    registerWithoutPasswordResponseDTO.setEmail(userBD.getEmail());

		    if (!userRequestDTO.getName().equals(userBD.getName())) {
			LogService.logger.info("Actualizando nombre...");
			userBD.setName(userRequestDTO.getName());
			userRepo.save(userBD);
			registerWithoutPasswordResponseDTO.setName(userRequestDTO.getName());
		    } else if (userRequestDTO.getName().equals(userBD.getName())) {
			LogService.logger.info("No se actualiza el nombre...");
			registerWithoutPasswordResponseDTO.setName(userRequestDTO.getName());
		    }
		    if (userMetaPhoneBD != null && userRequestDTO.getPhone() == null) {
			LogService.logger.info("Existe telefono para devolver...");
			registerWithoutPasswordResponseDTO.setPhone(userMetaPhoneBD.getMetaValue());
		    } else if (userRequestDTO.getPhone() != null) {
			if (userMetaPhoneBD == null) {
			    LogService.logger.info("Se esta creando el numero de telefonico..");
			    UserMeta userMetaPhoneNew = new UserMeta();
			    userMetaPhoneNew.setUser(userBD);
			    userMetaPhoneNew.setMetaKey(Constants.META_VALUE_PHONE);
			    userMetaPhoneNew.setMetaValue(userRequestDTO.getPhone());

			    UserMeta userMetaDB = userMetaRepo.save(userMetaPhoneNew);
			    registerWithoutPasswordResponseDTO.setPhone(userMetaDB.getMetaValue());
			} else {
			    LogService.logger.info("Se esta Actualizando el numero de telefonico..");
			    userMetaPhoneBD.setMetaValue(userRequestDTO.getPhone());

			    UserMeta userMetaUpdate = userMetaRepo.save(userMetaPhoneBD);
			    registerWithoutPasswordResponseDTO.setPhone(userMetaUpdate.getMetaValue());
			}

		    }

		} else if (userRequestDTO.getVersion() == 2L) {

		    if (userMetaPhoneBD != null) {
			registerWithoutPasswordResponseDTO.setPhone(userMetaPhoneBD.getMetaValue());
		    }
		    registerWithoutPasswordResponseDTO.setName(userBD.getName());

		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setData(registerWithoutPasswordResponseDTO);
		    return responseDTO;
		}

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(registerWithoutPasswordResponseDTO);
		return responseDTO;

	    } else {

		LogService.logger.info("El usuario" + userRequestDTO.getEmail() + "se encuentra desactivado, esta intentado cambiar contraseña..");
		MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0078.toString()).getDTO();
		listMessageDTO.add(messageUserNotExist);
		responseDTO.setListMessage(listMessageDTO);

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(new GenericDTO());

		return responseDTO;
	    }
	}

	if (KavakUtils.validIP(userRequestDTO.getIpUsuario())) {
	    LogService.logger.info("El formato de la ip suminitrada no es el correto..");
	    MessageDTO messagePasswordBadFormat = messageRepo.findByCode(MessageEnum.M0015.toString()).getDTO();
	    listMessageDTO.add(messagePasswordBadFormat);
	    responseDTO.setListMessage(listMessageDTO);

	}

	if (!KavakUtils.validateEmail(userRequestDTO.getEmail())) {
	    LogService.logger.info("validar el formato del email...");
	    MessageDTO messageEmailBadFormat = messageRepo.findByCode(MessageEnum.M0013.toString()).getDTO();
	    listMessageDTO.add(messageEmailBadFormat);
	    responseDTO.setListMessage(listMessageDTO);
	}

	if (responseDTO.getListMessage() != null) {
	    LogService.logger.info("Existe una lista de Menssaje de validacion, devolver los mensajes..");
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;
	}

	LogService.logger.info("Generando la contraseña..");
	String password = KavakUtils.generatePassword(10);

	String passwordHashed = KavakUtils.changeSaltInHash(BCrypt.hashpw(password.toString(), BCrypt.gensalt(12)), 1);

	RegisterWithoutPasswordResponseDTO registerWithoutPasswordResponseDTO = new RegisterWithoutPasswordResponseDTO();
	User userNew = new User();

	userNew.setRole(Constants.CUSTOMER_ROLE);
	userNew.setActive(Constants.USER_ACTIVE);
	userNew.setEmail(userRequestDTO.getEmail());
	userNew.setUserName(userRequestDTO.getEmail());
	userNew.setName(userRequestDTO.getName());
	userNew.setCreationDate(new Timestamp(System.currentTimeMillis()));
	userNew.setPassword(passwordHashed);
	userNew.setSource(source);

	if (userRequestDTO.getIpUsuario() != null) {
	    LogService.logger.info("Registrando Ip..");
	    userNew.setIpUsuario(userRequestDTO.getIpUsuario());
	}

	User userNewBD = userRepo.save(userNew);

	if (userRequestDTO.getPhone() != null) {

	    LogService.logger.info("Se esta registrando el numero de telefonico..");
	    UserMeta userMetaSave = new UserMeta();
	    userMetaSave.setUser(userNewBD);
	    userMetaSave.setMetaKey(Constants.META_VALUE_PHONE);
	    userMetaSave.setMetaValue(userRequestDTO.getPhone());

	    UserMeta userMetaDB = userMetaRepo.save(userMetaSave);
	    registerWithoutPasswordResponseDTO.setPhone(userMetaDB.getMetaValue());
	}

	registerWithoutPasswordResponseDTO.setId(userNewBD.getId());
	registerWithoutPasswordResponseDTO.setName(userNewBD.getName());
	registerWithoutPasswordResponseDTO.setEmail(userNewBD.getEmail());

	List<String> emailCc = new ArrayList<String>();
	HashMap<String, Object> subjectData = new HashMap<>();
	subjectData.put(Constants.KEY_SUBJECT, "¡Te has registrado exitosamente en Kavak! ");

	HashMap<String, Object> dataEmail = new HashMap<>();

	dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, userRequestDTO.getEmail());
	dataEmail.put(Constants.KEY_EMAIL_USER_PASSWORD, password);
	dataEmail.put(Constants.KEY_EMAIL_LOGIN_URL, Constants.PROPERTY_HOST_URL + "login");
	dataEmail.put(Constants.KEY_EMAIL_USER_NAME, userRequestDTO.getName().substring(0, 1).toUpperCase() + userRequestDTO.getName().substring(1));
	dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));
	dataEmail.put(Constants.ATTACHMENT_FILES, null);

	Boolean sendEmail = KavakUtils.sendEmail(userRequestDTO.getEmail(), emailCc, subjectData, "user_register_email_template.html", null, dataEmail, null);

	LogService.logger.info("Envio el correo ==" + sendEmail);

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(registerWithoutPasswordResponseDTO);

	return responseDTO;
    }

    /**
     * Metodo que actualiza los datos de un usuario
     *
     * @author Oscar Montilla
     * @param userRequestDTO
     *            resquest del servicio
     * @return ResponseDTO response generico de servicio.
     */
    public ResponseDTO putUser(UserRequestDTO userRequestDTO) throws MessagingException {

	ResponseDTO responseDTO = new ResponseDTO();
	List<MessageDTO> listMessageDTO = new ArrayList<>();

		if (!KavakUtils.validateInputPutUser(userRequestDTO)) {
			LogService.logger.info("Uno de los datos requerido llega NULL o vacios, se realiza return inmediatamente..");
			MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + "id,[name or phone or password or andress(street,exterior_number,interior_number,postal), ");
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());

			return responseDTO;

		}

		Optional<User> userBD = userRepo.findById(userRequestDTO.getId());

		if (!userBD.isPresent()) {

			LogService.logger.info("El usuario suministrado no es valido..");
			MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());

			return responseDTO;

		}

		UserMeta phoneBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, userBD.get().getId());
		UserMeta andressBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_ADDRESS, userBD.get().getId());

		PutUserResponseDTO putUserResponseDTO = new PutUserResponseDTO();
		putUserResponseDTO.setUserId(userBD.get().getId());

		if (userRequestDTO.getName() != null && userRequestDTO.getName() != userBD.get().getName()) {

			LogService.logger.info("Actualizando o registrando el nombre..");
			userBD.get().setName(userRequestDTO.getName());
			User userUpdate = userRepo.save(userBD.get());
			putUserResponseDTO.setName(userUpdate.getName());
		}

		if (userRequestDTO.getStreet() != null && userRequestDTO.getExteriorNumber() != null && userRequestDTO.getPostalCode() != null) {

			String metaValue = (userRequestDTO.getStreet() + "#" + userRequestDTO.getExteriorNumber() + "#" + (userRequestDTO.getInteriorNumber() == null ? "#" : userRequestDTO.getInteriorNumber() + "#") + userRequestDTO.getPostalCode());

			if (andressBD == null) {
				LogService.logger.info("Registrando la direccion..");
				UserMeta andressNew = new UserMeta();
				andressNew.setUser(userBD.get());
				andressNew.setMetaKey(Constants.META_VALUE_ADDRESS);
				andressNew.setMetaValue(metaValue);
				UserMeta andressUpdate = userMetaRepo.save(andressNew);
				putUserResponseDTO.setAddress(andressUpdate.getMetaValue());
				putUserResponseDTO.setStreet(userRequestDTO.getStreet());
				putUserResponseDTO.setExteriorNumber(userRequestDTO.getExteriorNumber());
				putUserResponseDTO.setInteriorNumber(userRequestDTO.getInteriorNumber() == null ? null : userRequestDTO.getInteriorNumber());
				putUserResponseDTO.setPostalCode(userRequestDTO.getPostalCode());
			} else {
				LogService.logger.info("Actualizando la direccion..");
				andressBD.setMetaValue(metaValue);
				UserMeta andressUpdate = userMetaRepo.save(andressBD);
				putUserResponseDTO.setAddress(andressUpdate.getMetaValue());
				putUserResponseDTO.setStreet(userRequestDTO.getStreet());
				putUserResponseDTO.setExteriorNumber(userRequestDTO.getExteriorNumber());
				putUserResponseDTO.setInteriorNumber(userRequestDTO.getInteriorNumber() == null ? null : userRequestDTO.getInteriorNumber());
				putUserResponseDTO.setPostalCode(userRequestDTO.getPostalCode());
			}
		}

		if (userRequestDTO.getPhone() != null) {

			if (phoneBD == null) {
				UserMeta phoneNew = new UserMeta();
				LogService.logger.info("Registrando el Telefono..");
				phoneNew.setUser(userBD.get());
				phoneNew.setMetaKey(Constants.META_VALUE_PHONE);
				phoneNew.setMetaValue(userRequestDTO.getPhone());
				UserMeta phoneUpdate = userMetaRepo.save(phoneNew);
				putUserResponseDTO.setPhone(phoneUpdate.getMetaValue());
			} else {
				LogService.logger.info("Actualizando el Telefono..");
				phoneBD.setMetaValue(userRequestDTO.getPhone());
				UserMeta phoneUpdate = userMetaRepo.save(phoneBD);
				putUserResponseDTO.setPhone(phoneUpdate.getMetaValue());
			}

		}

		if (userRequestDTO.getPassword() != null) {
			LogService.logger.info("Actualizando Contrasena de Usuario..");

			userBD.get().setPassword(KavakUtils.changeSaltInHash(BCrypt.hashpw(userRequestDTO.getPassword().toString(), BCrypt.gensalt(12)), 1));

			userRepo.save(userBD.get());

			String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";
			String templateEmail = "user_password_updated.html";

			List<String> emailCc = new ArrayList<String>();
			HashMap<String, Object> subjectData = new HashMap<>();
			subjectData.put(Constants.KEY_SUBJECT, "Cambio de Contraseña");

			HashMap<String, Object> dataEmail = new HashMap<>();

			dataEmail.put(Constants.KEY_EMAIL_USER_NAME, userBD.get().getName());

			Boolean sendEmail = KavakUtils.sendEmail(userBD.get().getEmail(), emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);

			LogService.logger.info("Se envio el correode Aviso de cambio de contraseña al usuario" + userBD.get().getEmail() + " ==" + sendEmail);

		}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(putUserResponseDTO);

	return responseDTO;
    }

    /**
     * Metodo que invoca el hash para recuperar contraseña de un usuario
     *
     * @param userEmail Email de usuario
     * @return ResponseDTO response generico de servicio.
     * @author Oscar Montilla
     */
    public ResponseDTO getRecoverPassword(String userEmail) throws MessagingException {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();


		if (userEmail == null || userEmail.length() == 0) {

			MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (userEmail == null || userEmail.length() == 0 ? "user_email, " : ""));
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);

			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());

			return responseDTO;

		}

        User userBD = userRepo.findByEmailAndRole(userEmail, Constants.CUSTOMER_ROLE);

        if (userBD == null) {

            LogService.logger.info("El email Suministrado no es valido..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0079.toString()).getDTO();
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;
        }

        Date myDate = new Date();
        String token = DigestUtils.sha1Hex(userBD.getId() + userBD.getName() + Math.random() * (9999999) + new SimpleDateFormat("yyyy-MM-dd").format(myDate));

        UserMeta userMetaSave = new UserMeta();

		userMetaSave.setUser(userBD);
		userMetaSave.setMetaKey("TOKEN");
		userMetaSave.setMetaValue(token);

        userMetaRepo.save(userMetaSave);


        String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";
        String templateEmail ="get_recover_password.html";

        List<String> emailCc = new ArrayList<String>();
        HashMap<String, Object> subjectData = new HashMap<>();
        subjectData.put(Constants.KEY_SUBJECT, "Recuperar Contraseña");

        HashMap<String, Object> dataEmail = new HashMap<>();

        dataEmail.put(Constants.KEY_EMAIL_USER_NAME, userBD.getName());
        dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_PASSWORD, Constants.URL_KAVAK + "/login/reinicioContrasena/" + DigestUtils.sha1Hex(userBD.getId().toString()) + '/' + token);

        Boolean sendEmail = KavakUtils.sendEmail(userBD.getEmail(), emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);

        LogService.logger.info("Envio el correo para recuperar contraseña para el usuario" + userBD.getEmail() + " ==" + sendEmail);

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(new GenericDTO());

        return responseDTO;
    }

	/**
	 * Metodo que valida el Token suministrado
	 *
	 * @param token  Token generado para cambiar contrasena
	 * @param idSha1 identificador de usuario con SHA1 aplicado
	 * @return ResponseDTO response generico de servicio.
	 * @author Oscar Montilla
	 */

	public ResponseDTO getValidateRecoverPassword(String token, String idSha1) {

		ResponseDTO responseDTO = new ResponseDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();

		if (token == null || token.length() == 0 || idSha1.length() == 0 || idSha1 == null) {

			MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (token == null || token.length() == 0 ? "token, " : "")
					+ (idSha1.length() == 0 || idSha1 == null ? "id_sha1, " : ""));
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());

			return responseDTO;

		}

		UserMeta userMetaBD = userMetaRepo.findByMetaKeyAndMetaValue("TOKEN", token);

		if(userMetaBD == null || !(DigestUtils.sha1Hex(userMetaBD.getUser().getId().toString()).equals(idSha1))){

			LogService.logger.info("El token Suministrado no es valido..");
			MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0080.toString()).getDTO();
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());

            return responseDTO;
		}


		LogService.logger.info("El token suministrado es valido, se elimina el registro de token..");
		userMetaRepo.delete(userMetaBD);

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(new GenericDTO());

		return responseDTO;

	}
}
