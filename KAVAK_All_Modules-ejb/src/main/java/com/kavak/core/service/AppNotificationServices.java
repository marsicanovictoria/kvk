package com.kavak.core.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.request.AppNotificationTypeRequestDTO;
import com.kavak.core.dto.request.PostAppNotificationTokenRequestDTO;
import com.kavak.core.dto.request.PutAppNotificationSettingRequestDTO;
import com.kavak.core.dto.response.AppNotificationDataResponseDTO;
import com.kavak.core.dto.response.AppNotificationLogResponseDTO;
import com.kavak.core.dto.response.AppNotificationPushResponseDTO;
import com.kavak.core.dto.response.AppNotificationTypeResponseDTO;
import com.kavak.core.dto.response.NotificationPushResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.AppNotificationPushTypeEnum;
import com.kavak.core.enumeration.AppNotificationTokenTypeEnum;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.AppNotificationLog;
import com.kavak.core.model.AppNotificationSetting;
import com.kavak.core.model.AppNotificationToken;
import com.kavak.core.model.AppNotificationType;
import com.kavak.core.model.AppointmentScheduleDate;
import com.kavak.core.model.CustomerNotifications;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.User;
import com.kavak.core.repositories.AppNotificationLogRepository;
import com.kavak.core.repositories.AppNotificationSettingRepository;
import com.kavak.core.repositories.AppNotificationTokenRepository;
import com.kavak.core.repositories.AppNotificationTypeRepository;
import com.kavak.core.repositories.AppointmentScheduleDateRepository;
import com.kavak.core.repositories.CustomerNotificationsRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

@Stateless
@Transactional
public class AppNotificationServices {

    private List<MessageDTO> listMessages;

    @Inject
    private AppNotificationLogRepository appNotificationLogRepo;

    @Inject
    private AppNotificationSettingRepository appNotificationSettingRepo;

    @Inject
    private AppNotificationTokenRepository appNotificationTokenRepo;

    @Inject
    private AppNotificationTypeRepository appNotificationTypeRepo;

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private UserRepository userRepo;

    @Inject
    private CustomerNotificationsRepository customerNotificationsRepo;

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;

    @Inject
    private SaleCheckpointRepository saleCheckpointRepo;

    @Inject
    private AppointmentScheduleDateRepository scheduleDateRepo;

    public ResponseDTO postAppNotificationToken(PostAppNotificationTokenRequestDTO postAppNotificationTokenRequestDTO) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (postAppNotificationToken) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	Optional<User> userBd = userRepo.findById(postAppNotificationTokenRequestDTO.getUserId());

	if (!userBd.isPresent()) {
	    LogService.logger.info("El usuario no existe en Base de datos..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
	    listMessages.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessages);
	    return responseDTO;
	}

	if (postAppNotificationTokenRequestDTO.getDeviceId() == null || postAppNotificationTokenRequestDTO.getTokenDevice() == null || postAppNotificationTokenRequestDTO.getTokenType() == null) {
	    LogService.logger.info("El servicio no recibe los datos requeridos");
	    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (postAppNotificationTokenRequestDTO.getDeviceId() == null ? "device_id, " : "") + (postAppNotificationTokenRequestDTO.getTokenDevice() == null ? "token_device, " : "") + (postAppNotificationTokenRequestDTO.getTokenType() == null ? "token_type, " : ""));
	    listMessages.add(messageDataNull);
	    responseDTO.setListMessage(listMessages);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;
	}
	AppNotificationTokenTypeEnum type = AppNotificationTokenTypeEnum.getByName(postAppNotificationTokenRequestDTO.getTokenType());
	if (type == AppNotificationTokenTypeEnum.NOT_EQUAL) {
	    LogService.logger.info("No se recibe token_type o no existe en la siguiente lista {iOS, Android}");
	    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0059.toString()).getDTO();
	    listMessages.add(messageDataNull);
	    responseDTO.setListMessage(listMessages);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;
	}

	AppNotificationToken appNotificationTokenBd = appNotificationTokenRepo.findAllByIdUserAndTokenType(postAppNotificationTokenRequestDTO.getUserId(), postAppNotificationTokenRequestDTO.getTokenType());

	if (appNotificationTokenBd == null) {
	    AppNotificationToken newAppNotificationToken = new AppNotificationToken();
	    newAppNotificationToken.setUserId(postAppNotificationTokenRequestDTO.getUserId());
	    newAppNotificationToken.setDeviceId(postAppNotificationTokenRequestDTO.getDeviceId());
	    newAppNotificationToken.setTokenDevice(postAppNotificationTokenRequestDTO.getTokenDevice());
	    newAppNotificationToken.setTokenType(postAppNotificationTokenRequestDTO.getTokenType());
	    newAppNotificationToken.setActive(true);

	    appNotificationTokenRepo.save(newAppNotificationToken);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(newAppNotificationToken);

	} else {
	    appNotificationTokenBd.setTokenDevice(postAppNotificationTokenRequestDTO.getTokenDevice());
	    appNotificationTokenBd.setActive(true);

	    appNotificationTokenRepo.save(appNotificationTokenBd);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(appNotificationTokenBd);
	}
	List<AppNotificationSetting> listAppNotificationSettingBd = new ArrayList<>();
	listAppNotificationSettingBd = appNotificationSettingRepo.findAllByIdUser(postAppNotificationTokenRequestDTO.getUserId());

	if (listAppNotificationSettingBd.isEmpty()) {
	    List<AppNotificationType> listAppNotificationTypeBd = new ArrayList<>();
	    listAppNotificationTypeBd = appNotificationTypeRepo.findAllActive();

	    for (AppNotificationType appNotificationTypeActual : listAppNotificationTypeBd) {
		AppNotificationSetting newAppNotificationSetting = new AppNotificationSetting();
		newAppNotificationSetting.setUser(userBd.get());
		newAppNotificationSetting.setAppNotificationType(appNotificationTypeActual);
		newAppNotificationSetting.setActive(true);
		appNotificationSettingRepo.save(newAppNotificationSetting);
	    }
	}
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (postAppNotificationToken) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    public ResponseDTO deleteAppNotificationToken(Long userId, String deviceId, String tokenType) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (deleteAppNotificationToken) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	Optional<User> userBd = userRepo.findById(userId);

	if (!userBd.isPresent()) {
	    LogService.logger.info("El usuario no existe en Base de datos..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
	    listMessages.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessages);
	    return responseDTO;
	}
	if (deviceId == null || tokenType == null) {
	    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (deviceId == null ? "device_id, " : "") + (tokenType == null ? "token_type, " : ""));
	    listMessages.add(messageDataNull);
	    responseDTO.setListMessage(listMessages);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());

	    return responseDTO;
	}

	AppNotificationToken appNotificationTokenBd = new AppNotificationToken();
	appNotificationTokenBd = appNotificationTokenRepo.findAllByIdUserAndDeviceIdAndTokenType(userId, deviceId, tokenType);

	if (appNotificationTokenBd == null) {
	    LogService.logger.info("No se consigue registro con los parametros recibidos.");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0061.toString()).getDTO();
	    listMessages.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessages);
	    return responseDTO;
	} else {
	    appNotificationTokenBd.setActive(false);
	    appNotificationTokenRepo.save(appNotificationTokenBd);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(appNotificationTokenBd);
	}

	LogService.logger.info(Constants.LOG_EXECUTING_END + " (deleteAppNotificationToken) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    public ResponseDTO getAppNotificationSettingsByUser(Long userId) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getAppNotificationSettingsByUser) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	List<AppNotificationSetting> listAppNotificationSettingBd = new ArrayList<AppNotificationSetting>();
	List<AppNotificationTypeResponseDTO> listAppNotificationTypeDTO = new ArrayList<AppNotificationTypeResponseDTO>();
	Optional<User> userBd = userRepo.findById(userId);

	if (!userBd.isPresent()) {
	    LogService.logger.info("El usuario no existe en Base de datos..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
	    listMessages.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessages);
	    return responseDTO;
	}

	listAppNotificationSettingBd = appNotificationSettingRepo.findAllByIdUser(userId);
	List<AppNotificationType> listAppNotificationTypeBd = new ArrayList<>();
	listAppNotificationTypeBd = appNotificationTypeRepo.findAllActive();

	if (listAppNotificationSettingBd.isEmpty()) {
	    for (AppNotificationType appNotificationTypeActual : listAppNotificationTypeBd) {
		AppNotificationSetting newAppNotificationSetting = new AppNotificationSetting();
		newAppNotificationSetting.setUser(userBd.get());
		newAppNotificationSetting.setAppNotificationType(appNotificationTypeActual);
		newAppNotificationSetting.setActive(true);
		appNotificationSettingRepo.save(newAppNotificationSetting);
	    }
	} else {
	    boolean setting = false;
	    for (AppNotificationType appNotificationTypeActual : listAppNotificationTypeBd) {
		for (AppNotificationSetting appNotificationSettingActual : listAppNotificationSettingBd) {
		    if (appNotificationSettingActual.getAppNotificationType().getId() == appNotificationTypeActual.getId()) {
			setting = true;
		    }
		}
		if (!setting) {
		    AppNotificationSetting newAppNotificationSetting = new AppNotificationSetting();
		    newAppNotificationSetting.setUser(userBd.get());
		    newAppNotificationSetting.setAppNotificationType(appNotificationTypeActual);
		    newAppNotificationSetting.setActive(true);
		    appNotificationSettingRepo.save(newAppNotificationSetting);
		}
		setting = false;
	    }
	}

	for (AppNotificationSetting appNotificationSettingActual : userBd.get().getListAppNotificationSetting()) {
	    AppNotificationTypeResponseDTO appNotificationTypeResponseDTO = new AppNotificationTypeResponseDTO();
	    appNotificationTypeResponseDTO.setId(appNotificationSettingActual.getUser().getId());
	    appNotificationTypeResponseDTO.setDescription(appNotificationSettingActual.getAppNotificationType().getDescription());
	    appNotificationTypeResponseDTO.setTitle(appNotificationSettingActual.getAppNotificationType().getTitle());
	    appNotificationTypeResponseDTO.setActive(appNotificationSettingActual.isActive());
	    listAppNotificationTypeDTO.add(appNotificationTypeResponseDTO);
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listAppNotificationTypeDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getAppNotificationSettingsByUser) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    public ResponseDTO putAppNotificationSettings(PutAppNotificationSettingRequestDTO putAppNotificationSettingRequestDTO) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (putAppNotificationSettings) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	List<AppNotificationSetting> listAppNotificationSettingBd = new ArrayList<AppNotificationSetting>();
	Optional<User> userBd = userRepo.findById(putAppNotificationSettingRequestDTO.getUserId());

	if (!userBd.isPresent()) {
	    LogService.logger.info("El usuario no existe en Base de datos..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
	    listMessages.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessages);
	    return responseDTO;
	}
	listAppNotificationSettingBd = appNotificationSettingRepo.findAllByIdUser(putAppNotificationSettingRequestDTO.getUserId());

	for (AppNotificationSetting appNotificationSettingActual : listAppNotificationSettingBd) {
	    for (AppNotificationTypeRequestDTO appNotificationTypeRequestDTOActual : putAppNotificationSettingRequestDTO.getListAppNotificationTypeRequestDTO()) {
		if (appNotificationSettingActual.getAppNotificationType().getId() == appNotificationTypeRequestDTOActual.getId()) {
		    appNotificationSettingActual.setActive(appNotificationTypeRequestDTOActual.isActive());
		    appNotificationSettingRepo.save(appNotificationSettingActual);
		}
	    }
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (putAppNotificationSettings) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    public ResponseDTO getAppNotificationsByUser(Long userId) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getAppNotificationsByUser) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	List<AppNotificationLog> listAppNotificationLogBd = new ArrayList<AppNotificationLog>();
	Optional<User> userBd = userRepo.findById(userId);

	if (!userBd.isPresent()) {
	    LogService.logger.info("El usuario no existe en Base de datos..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
	    listMessages.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessages);
	    return responseDTO;
	}

	AppNotificationLogResponseDTO appNotificationResponseLogDTO = new AppNotificationLogResponseDTO();
	List<AppNotificationLogResponseDTO> listAppNotificationResponseLogDTO = new ArrayList<AppNotificationLogResponseDTO>();
	listAppNotificationLogBd = appNotificationLogRepo.findbyIdUser(userId);

	if (!listAppNotificationLogBd.isEmpty()) {
	    for (AppNotificationLog appNotificationLogActual : listAppNotificationLogBd) {
		appNotificationResponseLogDTO = new AppNotificationLogResponseDTO();
		appNotificationResponseLogDTO.setTypeId(appNotificationLogActual.getAppNotificationType().getId());
		appNotificationResponseLogDTO.setIconUrl(appNotificationLogActual.getAppNotificationType().getIconUrl());
		appNotificationResponseLogDTO.setTitle(appNotificationLogActual.getAppNotificationType().getTitle());
		appNotificationResponseLogDTO.setDescription(appNotificationLogActual.getAppNotificationType().getDescription());
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		date = appNotificationLogActual.getCreationDate();
		String dateInString = sdf.format(date);
		appNotificationResponseLogDTO.setCreationDate(dateInString);
		listAppNotificationResponseLogDTO.add(appNotificationResponseLogDTO);
	    }
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listAppNotificationResponseLogDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getAppNotificationsByUser) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * PUSH : Push V-207 Se cancela reserva availableCarsAppNotification
     */
    public ResponseDTO availableCarsAppNotification() {
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	int notificationsSent = 0;
	try {
	    List<CustomerNotifications> listCustomerNotificationsBD = customerNotificationsRepo.findAvailableCarsAppNotification();
	    if (!listCustomerNotificationsBD.isEmpty()) {
		for (CustomerNotifications customerNotificationsActual : listCustomerNotificationsBD) {
		    try {
			Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.AVAILABLE_CARS_APP_NOTIFICATION.getValue());
			AppNotificationSetting appNotificationSetting = appNotificationSettingRepo.findAllByIdUserAndTypeId(customerNotificationsActual.getUser().getId(), AppNotificationPushTypeEnum.AVAILABLE_CARS_APP_NOTIFICATION.getValue());
			if (appNotificationSetting != null) {
			    // mandar notificacion push al cliente
			    NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
			    AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
			    AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
			    RestTemplate restTemplate = new RestTemplate();
			    String url = Constants.PUSH_FIREBASE_URL_SEND;
			    Optional<SellCarDetail> sellCarDetail = sellCarDetailRepo.findById(customerNotificationsActual.getCarId());
			    List<AppNotificationToken> appNotificationToken = appNotificationTokenRepo.findByUserId(customerNotificationsActual.getUser().getId());

			    for (AppNotificationToken token : appNotificationToken) {
				//
				// Armo el header
				//
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", "key=" + Constants.PUSH_FIREBASE_API_KEY);
				headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
				//
				// Variables para el Request
				//
				notificationPushResponseDTO.setTitle("!Se liberó el auto que buscabas!");
				notificationPushResponseDTO.setBody("El auto " + sellCarDetail.get().getCarModel() + " " + sellCarDetail.get().getCarYear() + " que estabas buscando acaba de ser liberado. Corre y resérvalo, no dejes que te lo ganen.");
				appNotificationDataResponseDTO.setCarId(sellCarDetail.get().getId().toString());
				appNotificationDataResponseDTO.setCarUrl("http://www.kavak.com/" + sellCarDetail.get().getCarMake() + "-" + sellCarDetail.get().getCarModel() + "-" + sellCarDetail.get().getCarYear() + "-compra-de-autos-" + sellCarDetail.get().getId());
				appNotificationDataResponseDTO.setNotificationCode(Constants.KEY_PUSH_CODE_V207);
				appNotificationPushResponseDTO.setTo(token.getTokenDevice());
				appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
				appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
				//
				// Map para armar el body
				//
				Map<String, Object> body = new HashMap<String, Object>();
				body.put("notification", appNotificationPushResponseDTO.getNotification());
				body.put("data", appNotificationPushResponseDTO.getData());
				body.put("to", appNotificationPushResponseDTO.getTo());
				//
				// Map para mandar el MapBody y el header
				//
				HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

				// llamada al servicio de fireBase
				ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
				//
				//Armando el log
				AppNotificationLog newAppNotificationLog = new AppNotificationLog();
				newAppNotificationLog.setUserId(customerNotificationsActual.getUser().getId());
				newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
				newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
				newAppNotificationLog.setNotificationMessage(notificationPushResponseDTO.getBody());
				newAppNotificationLog.setNotificationCode(Constants.KEY_PUSH_CODE_V207);
				newAppNotificationLog.setTokenId(token.getId());
				newAppNotificationLog.setFcmMessage(responseFirebase.getBody());

				if (responseFirebase.getBody().toString().contains("\"failure\":1")) {
				    LogService.logger.info("ERROR Mandando notificacion Push availableCarsAppNotification A el usuario:  " + token.getUserId());
				} else {
				    newAppNotificationLog.setSuccessfulNotification(true);
				    notificationsSent++;
				}
				newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
			    }
			}
			// setar a 1 appNotificationSent
			customerNotificationsActual.setCarAppNotificationSent(true);
			customerNotificationsRepo.save(customerNotificationsActual);
		    } catch (Exception ex) {
			customerNotificationsActual.setCarAppNotificationSent(true);
			customerNotificationsRepo.save(customerNotificationsActual);
			LogService.logger.info("catch ERROR Mandando notificacion Push availableCarsAppNotification:  " + ex);
			ex.printStackTrace();
		    }
		}
	    }
	    LogService.logger.info("Total Push Notifications Enviadas de availableCarsAppNotification: [" + notificationsSent + "] ");
	} catch (Exception e) {
	    LogService.logger.info("catch Externo ERROR Mandando notificacion Push availableCarsAppNotification:  " + e);
	    e.printStackTrace();
	}
	return responseDTO;
    }

    public ResponseDTO bookingTimeReminderAppNotification() {
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	int notificationsSent = 0;
	try {
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findBookingTimeRemindersAppNotification();
	    if (listSaleCheckpointBD != null && !listSaleCheckpointBD.isEmpty()) {
		for (SaleCheckpoint saleCheckpointActual : listSaleCheckpointBD) {
		    try {
			Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.BOOKED_TIME_REMINDER_APP_NOTIFICATION.getValue());
			AppNotificationSetting appNotificationSetting = appNotificationSettingRepo.findAllByIdUserAndTypeId(saleCheckpointActual.getUserId(), AppNotificationPushTypeEnum.BOOKED_TIME_REMINDER_APP_NOTIFICATION.getValue());
			if (appNotificationSetting != null) {
			    // mandar notificacion
			    NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
			    AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
			    AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
			    RestTemplate restTemplate = new RestTemplate();
			    String url = Constants.PUSH_FIREBASE_URL_SEND;
			    Optional<SellCarDetail> sellCarDetail = sellCarDetailRepo.findById(saleCheckpointActual.getCarId());
			    List<AppNotificationToken> appNotificationToken = appNotificationTokenRepo.findByUserId(saleCheckpointActual.getUser().getId());

			    for (AppNotificationToken token : appNotificationToken) {
				//
				// Armo el header
				//
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", "key=" + Constants.PUSH_FIREBASE_API_KEY);
				headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
				//
				// Variables para el Request
				//
				notificationPushResponseDTO.setTitle(saleCheckpointActual.getUser().getName() + " tienes 24 horas para finalizar la compra de tu auto " + KavakUtils.getFriendlyCarName(sellCarDetail.get().getDTO()) + " " + sellCarDetail.get().getCarYear());
				notificationPushResponseDTO.setBody("En menos de 24 horas vence el plazo de apartado del auto " + KavakUtils.getFriendlyCarName(sellCarDetail.get().getDTO()) + " " + sellCarDetail.get().getCarYear() + ", por favor contacta a tu Experience Manager para completar la compra.");
				appNotificationDataResponseDTO.setCarId(sellCarDetail.get().getId().toString());
				appNotificationDataResponseDTO.setCarUrl("http://www.kavak.com/" + sellCarDetail.get().getCarMake() + "-" + sellCarDetail.get().getCarModel() + "-" + sellCarDetail.get().getCarYear() + "-compra-de-autos-" + sellCarDetail.get().getId());
				appNotificationPushResponseDTO.setTo(token.getTokenDevice());
				appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
				appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
				//
				// Map para armar el body
				//
				Map<String, Object> body = new HashMap<String, Object>();
				body.put("notification", appNotificationPushResponseDTO.getNotification());
				body.put("data", appNotificationPushResponseDTO.getData());
				body.put("to", appNotificationPushResponseDTO.getTo());
				//
				// Map para mandar el MapBody y el header
				//
				HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

				// llamada al servicio de fireBase
				ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
				//
				//Seteando en la tabla log
				AppNotificationLog newAppNotificationLog = new AppNotificationLog();
				newAppNotificationLog.setUserId(saleCheckpointActual.getUserId());
				newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
				newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
				newAppNotificationLog.setNotificationMessage(notificationPushResponseDTO.getBody());
				newAppNotificationLog.setNotificationCode("not code");
				newAppNotificationLog.setTokenId(token.getId());
				newAppNotificationLog.setFcmMessage(responseFirebase.getBody());

				if (responseFirebase.getBody().toString().contains("\"failure\":1")) {
				    LogService.logger.info("ERROR Mandando notificacion Push bookingTimeReminderAppNotification A el usuario:  " + token.getUserId());
				} else {
				    newAppNotificationLog.setSuccessfulNotification(true);
				    notificationsSent++;
				}
				newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
			    }
			}
			// setar a 1 appNotificationSent
			saleCheckpointActual.setBookingTimeReminderNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
		    } catch (Exception ex) {
			saleCheckpointActual.setBookingTimeReminderNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
			LogService.logger.info("catch ERROR Mandando notificacion Push bookingTimeReminderAppNotification:  " + ex);
			ex.printStackTrace();
		    }
		}
	    }
	    LogService.logger.info("Total Push Notifications Enviadas de bookingTimeReminderAppNotification: [" + notificationsSent + "] ");
	} catch (Exception e) {
	    LogService.logger.info("catch Externo ERROR Mandando notificacion Push bookingTimeReminderAppNotification:  " + e);
	    e.printStackTrace();
	}
	return responseDTO;
    }

    public ResponseDTO bookingTimeExpiredAppNotification() {
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	int notificationsSent = 0;
	try {
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findBookingTimeExpiredAppNotification();
	    if (listSaleCheckpointBD != null && !listSaleCheckpointBD.isEmpty()) {
		for (SaleCheckpoint saleCheckpointActual : listSaleCheckpointBD) {
		    try {
			Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.BOOKED_TIME_EXPIRED_APP_NOTIFICATION.getValue());
			AppNotificationSetting appNotificationSetting = appNotificationSettingRepo.findAllByIdUserAndTypeId(saleCheckpointActual.getUserId(), AppNotificationPushTypeEnum.BOOKED_TIME_EXPIRED_APP_NOTIFICATION.getValue());
			if (appNotificationSetting != null) {
			    // mandar notificacion
			    NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
			    AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
			    AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
			    RestTemplate restTemplate = new RestTemplate();
			    String url = Constants.PUSH_FIREBASE_URL_SEND;
			    Optional<SellCarDetail> sellCarDetail = sellCarDetailRepo.findById(saleCheckpointActual.getCarId());
			    List<AppNotificationToken> appNotificationToken = appNotificationTokenRepo.findByUserId(saleCheckpointActual.getUser().getId());

			    for (AppNotificationToken token : appNotificationToken) {
				//
				// Armo el header
				//
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", "key=" + Constants.PUSH_FIREBASE_API_KEY);
				headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
				//
				// Variables para el Request
				//
				notificationPushResponseDTO.setTitle(saleCheckpointActual.getUser().getName() + " ha expirado tu apartado del auto " + KavakUtils.getFriendlyCarName(sellCarDetail.get().getDTO()) + " " + sellCarDetail.get().getCarYear());
				notificationPushResponseDTO.setBody("Ha expirado tu apartado del auto " + KavakUtils.getFriendlyCarName(sellCarDetail.get().getDTO()) + " " + sellCarDetail.get().getCarYear() + ", por favor contacta a tu Experience Manager si deseas extender el apartado.");
				appNotificationDataResponseDTO.setCarId(sellCarDetail.get().getId().toString());
				appNotificationDataResponseDTO.setCarUrl("http://www.kavak.com/" + sellCarDetail.get().getCarMake() + "-" + sellCarDetail.get().getCarModel() + "-" + sellCarDetail.get().getCarYear() + "-compra-de-autos-" + sellCarDetail.get().getId());
				appNotificationPushResponseDTO.setTo(token.getTokenDevice());
				appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
				appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
				//
				// Map para armar el body
				//
				Map<String, Object> body = new HashMap<String, Object>();
				body.put("notification", appNotificationPushResponseDTO.getNotification());
				body.put("data", appNotificationPushResponseDTO.getData());
				body.put("to", appNotificationPushResponseDTO.getTo());
				//
				// Map para mandar el MapBody y el header
				//
				HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

				// llamada al servicio de fireBase
				ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
				//
				//Guardando en la tabla log
				AppNotificationLog newAppNotificationLog = new AppNotificationLog();
				newAppNotificationLog.setUserId(saleCheckpointActual.getUserId());
				newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
				newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
				newAppNotificationLog.setNotificationMessage("Ha expirado tu apartado del auto " + KavakUtils.getFriendlyCarName(sellCarDetail.get().getDTO()) + " " + sellCarDetail.get().getCarYear() + ", por favor contacta a tu Experience Manager si deseas extender el apartado.");
				newAppNotificationLog.setNotificationCode("V-207");
				newAppNotificationLog.setTokenId(token.getId());
				newAppNotificationLog.setFcmMessage(responseFirebase.getBody());

				if (responseFirebase.getBody().toString().contains("\"failure\":1")) {
				    LogService.logger.info("ERROR Mandando notificacion Push bookingTimeExpiredAppNotification A el usuario:  " + token.getUserId());
				} else {
				    newAppNotificationLog.setSuccessfulNotification(true);
				    notificationsSent++;
				}
				newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
			    }
			}
			// setar a 1 appNotificationSent
			saleCheckpointActual.setBookingTimeExpiredNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
		    } catch (Exception ex) {
			saleCheckpointActual.setBookingTimeExpiredNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
			LogService.logger.info("catch ERROR Mandando notificacion Push bookingTimeExpiredAppNotification:  " + ex);
			ex.printStackTrace();
		    }
		}
	    }
	    LogService.logger.info("Total Push Notifications Enviadas de bookingTimeExpiredAppNotification: [" + notificationsSent + "] ");
	} catch (Exception e) {
	    LogService.logger.info("catch Externo ERROR notificacion Push bookingTimeExpiredAppNotification:  " + e);
	    e.printStackTrace();
	}
	return responseDTO;
    }

    // TODO: Consulta de inspeccciones debe venir de NETSUITE
    // ================================================= >>
    // public ResponseDTO inspectionReminderAppNotification() {
    // LogService.logger.info(Constants.LOG_EXECUTING_START + " (inspectionReminderAppNotification) ");
    // Long starTime = Calendar.getInstance().getTimeInMillis();
    // ResponseDTO responseDTO = new ResponseDTO();
    // listMessages = new ArrayList<MessageDTO>();
    // try {
    // List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findIsInspectionReminderNotificationSent();
    // if (listOfferCheckpointBD != null && !listOfferCheckpointBD.isEmpty()) {
    // for (OfferCheckpoint offerCheckpointActual : listOfferCheckpointBD) {
    // try {
    // Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.INSPECTION_REMINDER_APP_NOTIFICATION.getValue());
    // AppNotificationSetting appNotificationSetting = appNotificationSettingRepo.findAllByIdUserAndTypeId(offerCheckpointActual.getIdUser(), AppNotificationPushTypeEnum.INSPECTION_REMINDER_APP_NOTIFICATION.getValue());
    // if (appNotificationSetting != null) {
    // NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
    // AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
    // AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
    // RestTemplate restTemplate = new RestTemplate();
    // String url = Constants.PUSH_FIREBASE_URL_SEND;
    // AppNotificationToken appNotificationToken = appNotificationTokenRepo.findByUserId(offerCheckpointActual.getIdUser());
    // //
    // // Armo el header
    // //
    // HttpHeaders headers = new HttpHeaders();
    // headers.set("Authorization", "key="+Constants.PUSH_FIREBASE_API_KEY);
    // headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
    // //
    // // Variables para el Request
    // //
    // SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    // String offerDate = sdf.format(offerCheckpointActual.getOfferDate());
    // notificationPushResponseDTO.setTitle(offerCheckpointActual.getCustomerName() + " " + offerCheckpointActual.getCustomerLastName() + " tienes una inspeccion el día " + offerDate + " para tu " + offerCheckpointActual.getCarMake() + " " + offerCheckpointActual.getCarModel() + " " + offerCheckpointActual.getCarYear());
    // notificationPushResponseDTO.setBody("Tienes una inspeccion el día " + offerDate + " para tu " + offerCheckpointActual.getCarMake() + " " + offerCheckpointActual.getCarModel() + " " + offerCheckpointActual.getCarYear() + " ¡Prepárate para aprender mucho sobre tu auto!.");
    // appNotificationPushResponseDTO.setTo(appNotificationToken.getTokenDevice());
    // appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
    // appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
    // //
    // // Map para armar el body
    // //
    // Map<String, Object> body = new HashMap<String, Object>();
    // body.put("notification", appNotificationPushResponseDTO.getNotification());
    // body.put("data", appNotificationPushResponseDTO.getData());
    // body.put("to", appNotificationPushResponseDTO.getTo());
    // //
    // // Map para mandar el MapBody y el header
    // //
    // HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);
    //
    // // llamada al servicio de fireBase
    // ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    // if (responseFirebase.getBody().toString().contains("InvalidRegistration")) {
    // LogService.logger.info("ERROR Mandando notificacion Push bookingTimeReminderAppNotification A el usuario: " + appNotificationToken.getUserId());
    // } else {
    // AppNotificationLog newAppNotificationLog = new AppNotificationLog();
    // newAppNotificationLog.setUserId(offerCheckpointActual.getIdUser());
    // newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
    // newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
    // newAppNotificationLog.setTokenId(appNotificationToken.getId());
    // newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
    // }
    // }
    // // setar a 1 appNotificationSent
    // offerCheckpointActual.setInspectionReminderNotificationSent(true);
    // offerCheckpointRepo.save(offerCheckpointActual);
    // } catch (Exception ex) {
    // offerCheckpointActual.setInspectionReminderNotificationSent(true);
    // offerCheckpointRepo.save(offerCheckpointActual);
    // LogService.logger.info("catch ERROR Mandando notificacion Push inspectionReminderAppNotification: " + ex);
    // ex.printStackTrace();
    // }
    // }
    // }
    // } catch (Exception e) {
    // LogService.logger.info("catch Externo ERROR notificacion Push inspectionReminderAppNotification: " + e);
    // e.printStackTrace();
    // }
    // LogService.logger.info(Constants.LOG_EXECUTING_END + " (inspectionReminderAppNotification) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
    // return responseDTO;
    // }

    public ResponseDTO newCarsAppNotification() {
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	int notificationsSent = 0;
	try {
	    List<SellCarDetail> listSellCarDetailBD = sellCarDetailRepo.findNewCarsAppNotification();
	    if (listSellCarDetailBD != null && !listSellCarDetailBD.isEmpty() && listSellCarDetailBD.size() >= Long.valueOf(Constants.PROPERTY_CATALOGUE_NEWCARS)) {
		Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.NEW_CAR_APP_NOTIFICATION.getValue());
		List<AppNotificationSetting> listAppNotificationSetting = appNotificationSettingRepo.findAllByTypeId(AppNotificationPushTypeEnum.NEW_CAR_APP_NOTIFICATION.getValue());
		if (listAppNotificationSetting != null && !listAppNotificationSetting.isEmpty()) {
		    for (AppNotificationSetting appNotificationSettingActual : listAppNotificationSetting) {
			// mandar notificacion
			try {
			    NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
			    AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
			    AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
			    RestTemplate restTemplate = new RestTemplate();
			    String url = Constants.PUSH_FIREBASE_URL_SEND;
			    List<AppNotificationToken> appNotificationToken = appNotificationTokenRepo.findByUserId(appNotificationSettingActual.getUser().getId());

			    for (AppNotificationToken token : appNotificationToken) {
				//
				// Armo el header
				//
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", "key=" + Constants.PUSH_FIREBASE_API_KEY);
				headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
				//
				// Variables para el Request
				//
				notificationPushResponseDTO.setTitle(appNotificationSettingActual.getUser().getName() + " llegaron nuevos autos a nuestro catálogo!");
				notificationPushResponseDTO.setBody("Llegaron nuevos autos a nuestro catálogo, no esperes más y revisa estos increibles autos.");
				appNotificationPushResponseDTO.setTo(token.getTokenDevice());
				appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
				appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
				//
				// Map para armar el body
				//
				Map<String, Object> body = new HashMap<String, Object>();
				body.put("notification", appNotificationPushResponseDTO.getNotification());
				body.put("data", appNotificationPushResponseDTO.getData());
				body.put("to", appNotificationPushResponseDTO.getTo());
				//
				// Map para mandar el MapBody y el header
				//
				HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

				// llamada al servicio de fireBase
				ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
				//Guardando en la tabla log
				AppNotificationLog newAppNotificationLog = new AppNotificationLog();
				newAppNotificationLog.setUserId(appNotificationSettingActual.getUser().getId());
				newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
				newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
				newAppNotificationLog.setTokenId(token.getId());
				newAppNotificationLog.setNotificationMessage(notificationPushResponseDTO.getBody());
				newAppNotificationLog.setNotificationCode("not code");
				newAppNotificationLog.setFcmMessage(responseFirebase.getBody());

				if (responseFirebase.getBody().toString().contains("\"failure\":1")) {
				    LogService.logger.info("ERROR Mandando notificacion Push newCarsAppNotification A el usuario:  " + token.getUserId());
				} else {
				    newAppNotificationLog.setSuccessfulNotification(true);
				    notificationsSent++;
				}
				newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
			    }

			} catch (Exception ex) {
			    LogService.logger.info("catch ERROR Mandando notificacion Push NewCarsAppNotification:  " + ex);
			    ex.printStackTrace();
			}
		    }
		}
		// setar a 1 appNotificationSent
		for (SellCarDetail sellCarDetailActual : listSellCarDetailBD) {
		    sellCarDetailActual.setNewCarNotificationSent(true);
		    sellCarDetailRepo.save(sellCarDetailActual);
		}
		LogService.logger.info("Total Push Notifications Enviadas de NewCarsAppNotification: [" + notificationsSent + "] ");
	    }
	} catch (Exception e) {
	    LogService.logger.info("catch externo ERROR Mandando notificacion Push NewCarsAppNotification:  " + e);
	    e.printStackTrace();
	}
	return responseDTO;
    }

    // TODO: Consulta de citas debe venir de NETSUITE
    // ================================================ >>
    // public ResponseDTO appointmentReminderAppNotification() {
    // LogService.logger.info(Constants.LOG_EXECUTING_START + " (appointmentReminderAppNotification) ");
    // Long starTime = Calendar.getInstance().getTimeInMillis();
    // ResponseDTO responseDTO = new ResponseDTO();
    // listMessages = new ArrayList<MessageDTO>();
    // try {
    // List<ScheduleDate> listScheduleDateBD = scheduleDateRepo.findAppointmentReminderAppNotification();
    // if (listScheduleDateBD != null && !listScheduleDateBD.isEmpty()) {
    // for (ScheduleDate scheduleDateActual : listScheduleDateBD) {
    // try {
    // Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.APPOINTMENT_REMINDER_APP_NOTIFICATION.getValue());
    // AppNotificationSetting appNotificationSetting = appNotificationSettingRepo.findAllByIdUserAndTypeId(scheduleDateActual.getUserId(), AppNotificationPushTypeEnum.APPOINTMENT_REMINDER_APP_NOTIFICATION.getValue());
    // if (appNotificationSetting != null) {
    // // mandar notificacion
    // NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
    // AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
    // AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
    // RestTemplate restTemplate = new RestTemplate();
    // String url = Constants.PUSH_FIREBASE_URL_SEND;
    // Optional<User> userBD = userRepo.findById(scheduleDateActual.getUserId());
    // Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(scheduleDateActual.getCarId());
    // AppNotificationToken appNotificationToken = appNotificationTokenRepo.findByUserId(userBD.get().getId());
    // SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    // String appointmentDate = sdf.format(scheduleDateActual.getVisitDate());
    // //
    // // Armo el header
    // //
    // HttpHeaders headers = new HttpHeaders();
    // headers.set("Authorization", "key="+Constants.PUSH_FIREBASE_API_KEY);
    // headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
    // //
    // // Variables para el Request
    // //
    // appNotificationDataResponseDTO.setAppointmentDate(appointmentDate.toString());
    // appNotificationDataResponseDTO.setAppointmentTime(scheduleDateActual.getStarHourAppointment() + " a " + scheduleDateActual.getEndHourAppointment());
    // appNotificationDataResponseDTO.setAppointmentAddress(scheduleDateActual.getVisitAddress());
    //
    // notificationPushResponseDTO.setTitle(userBD.get().getName() + " tienes una visita el día " + appointmentDate + " para el auto " + KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()) + " " + sellCarDetailBD.get().getCarYear());
    // notificationPushResponseDTO.setBody("Tienes una visita el día " + appointmentDate + " para ver nuestro increíble " + KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()) + " " + sellCarDetailBD.get().getCarYear());
    // appNotificationPushResponseDTO.setTo(appNotificationToken.getTokenDevice());
    // appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
    // appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
    // //
    // // Map para armar el body
    // //
    // Map<String, Object> body = new HashMap<String, Object>();
    // body.put("notification", appNotificationPushResponseDTO.getNotification());
    // body.put("data", appNotificationPushResponseDTO.getData());
    // body.put("to", appNotificationPushResponseDTO.getTo());
    // //
    // // Map para mandar el MapBody y el header
    // //
    // HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);
    //
    // // llamada al servicio de fireBase
    // ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    // if (responseFirebase.getBody().toString().contains("InvalidRegistration")) {
    // LogService.logger.info("ERROR Mandando notificacion Push bookingTimeReminderAppNotification A el usuario: " + appNotificationToken.getUserId());
    // } else {
    // AppNotificationLog newAppNotificationLog = new AppNotificationLog();
    // newAppNotificationLog.setUserId(scheduleDateActual.getUserId());
    // newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
    // newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
    // newAppNotificationLog.setTokenId(appNotificationToken.getId());
    // newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
    // }
    // }
    // // setar a 1 AppointmentReminderNotificationSent
    // scheduleDateActual.setAppointmentReminderNotificationSent(true);
    // scheduleDateRepo.save(scheduleDateActual);
    // } catch (Exception ex) {
    // scheduleDateActual.setAppointmentReminderNotificationSent(true);
    // scheduleDateRepo.save(scheduleDateActual);
    // LogService.logger.info("catch ERROR Mandando notificacion Push appointmentReminderAppNotification: " + ex);
    // ex.printStackTrace();
    // }
    // }
    // }
    // } catch (Exception e) {
    // LogService.logger.info("catch Externo ERROR notificacion Push appointmentReminderAppNotification: " + e);
    // e.printStackTrace();
    // }
    // LogService.logger.info(Constants.LOG_EXECUTING_END + " (appointmentReminderAppNotification) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
    // return responseDTO;
    // }

    /**
     * PUSH : Push V-204 Se registra reserva en la pagina bookingRegisteredAppNotification
     */
    public ResponseDTO bookedRegisteredAppNotification() {
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	int notificationsSent = 0;
	try {
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findBookedRegisteredAppNotification();
	    if (listSaleCheckpointBD != null && !listSaleCheckpointBD.isEmpty()) {
		for (SaleCheckpoint saleCheckpointActual : listSaleCheckpointBD) {
		    try {
			Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.BOOKED_REGISTERED_APP_NOTIFICATION.getValue());
			AppNotificationSetting appNotificationSetting = appNotificationSettingRepo.findAllByIdUserAndTypeId(saleCheckpointActual.getUserId(), AppNotificationPushTypeEnum.BOOKED_REGISTERED_APP_NOTIFICATION.getValue());
			if (appNotificationSetting != null) {
			    // mandar notificacion
			    NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
			    AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
			    AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
			    RestTemplate restTemplate = new RestTemplate();
			    String url = Constants.PUSH_FIREBASE_URL_SEND;
			    Optional<SellCarDetail> sellCarDetail = sellCarDetailRepo.findById(saleCheckpointActual.getCarId());
			    List<AppNotificationToken> appNotificationToken = appNotificationTokenRepo.findByUserId(saleCheckpointActual.getUser().getId());

			    for (AppNotificationToken token : appNotificationToken) {
				//
				// Armo el header
				//
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", "key=" + Constants.PUSH_FIREBASE_API_KEY);
				headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
				//
				// Variables para el Request
				//
				notificationPushResponseDTO.setTitle("Tus 72h comienzan ahora");
				notificationPushResponseDTO.setBody("Gracias por apartar el auto de tus sueños. Te estaremos contactando para seguir apoyándote con el proceso de compra.");
				appNotificationDataResponseDTO.setCarId(sellCarDetail.get().getId().toString());
				appNotificationDataResponseDTO.setCarUrl("http://www.kavak.com/" + sellCarDetail.get().getCarMake() + "-" + sellCarDetail.get().getCarModel() + "-" + sellCarDetail.get().getCarYear() + "-compra-de-autos-" + sellCarDetail.get().getId());
				appNotificationDataResponseDTO.setNotificationCode(Constants.KEY_PUSH_CODE_V204);
				appNotificationPushResponseDTO.setTo(token.getTokenDevice());
				appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
				appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
				//
				// Map para armar el body
				//
				Map<String, Object> body = new HashMap<String, Object>();
				body.put("notification", appNotificationPushResponseDTO.getNotification());
				body.put("data", appNotificationPushResponseDTO.getData());
				body.put("to", appNotificationPushResponseDTO.getTo());
				//
				// Map para mandar el MapBody y el header
				//
				HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

				// llamada al servicio de fireBase
				ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
				//
				//Guardando en la tabla de log
				AppNotificationLog newAppNotificationLog = new AppNotificationLog();
				newAppNotificationLog.setUserId(saleCheckpointActual.getUserId());
				newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
				newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
				newAppNotificationLog.setTokenId(token.getId());
				newAppNotificationLog.setNotificationMessage(notificationPushResponseDTO.getBody());
				newAppNotificationLog.setNotificationCode(Constants.KEY_PUSH_CODE_V204);
				newAppNotificationLog.setFcmMessage(responseFirebase.getBody());

				if (responseFirebase.getBody().toString().contains("\"failure\":1")) {
				    LogService.logger.info("ERROR Mandando notificacion Push bookedRegisteredAppNotification A el usuario:  " + token.getUserId());
				} else {
				    newAppNotificationLog.setSuccessfulNotification(true);
				    notificationsSent++;
				}
				newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
			    }
			}
			// setar a 1 appNotificationSent
			saleCheckpointActual.setBookedRegisteredNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
		    } catch (Exception ex) {
			saleCheckpointActual.setBookedRegisteredNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
			LogService.logger.info("catch ERROR Mandando notificacion Push bookedRegisteredAppNotification:  " + ex);
			ex.printStackTrace();
		    }
		}
	    }
	    LogService.logger.info("Total Push Notifications Enviadas de bookedRegisteredAppNotification: [" + notificationsSent + "] ");
	} catch (Exception e) {
	    LogService.logger.info("catch Externo ERROR Mandando notificacion Push bookedRegisteredAppNotification:  " + e);
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * PUSH : Push V-201.1 = PEDREGAL / V-201.2 = PONIENTE / V-201.3 = LONDRE 189 Se registra reserva en la pagina appointmentBookedRegisteredAppNotification
     */
    public ResponseDTO appointmentBookedRegisteredAppNotification() {
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	int notificationsSent = 0;
	try {
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findAppointmentBookedRegisteredAppNotification();
	    if (listSaleCheckpointBD != null && !listSaleCheckpointBD.isEmpty()) {
		for (SaleCheckpoint saleCheckpointActual : listSaleCheckpointBD) {
		    try {
			Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.APPOINTMENT_BOOKED_APP_NOTIFICATION.getValue());
			AppNotificationSetting appNotificationSetting = appNotificationSettingRepo.findAllByIdUserAndTypeId(saleCheckpointActual.getUserId(), AppNotificationPushTypeEnum.APPOINTMENT_BOOKED_APP_NOTIFICATION.getValue());
			Optional<AppointmentScheduleDate> scheduleDateBD = scheduleDateRepo.findById(saleCheckpointActual.getScheduleDateId());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String appointmentDate = sdf.format(scheduleDateBD.get().getVisitDate());
			if (appNotificationSetting != null) {
			    // mandar notificacion
			    NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
			    AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
			    AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
			    RestTemplate restTemplate = new RestTemplate();
			    String url = Constants.PUSH_FIREBASE_URL_SEND;
			    Optional<SellCarDetail> sellCarDetail = sellCarDetailRepo.findById(saleCheckpointActual.getCarId());
			    List<AppNotificationToken> appNotificationToken = appNotificationTokenRepo.findByUserId(saleCheckpointActual.getUser().getId());

			    for (AppNotificationToken token : appNotificationToken) {
				//
				// Armo el header
				//
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", "key=" + Constants.PUSH_FIREBASE_API_KEY);
				headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
				//
				// Variables para el Request
				//
				notificationPushResponseDTO.setTitle("¡Tienes una cita con KAVAK!");
				if (scheduleDateBD.get().getAppointmentLocationId() == 1L) {
				    notificationPushResponseDTO.setBody("Confirmo tu cita el día " + appointmentDate + " en Londre 189. Recuerda que tienes 72hr para ver el auto y finalizar el pago.");
				    appNotificationDataResponseDTO.setNotificationCode(Constants.KEY_PUSH_CODE_V201_3);
				} else if (scheduleDateBD.get().getAppointmentLocationId() == 4L) {
				    notificationPushResponseDTO.setBody("Confirmo tu cita el día " + appointmentDate + " en Pedregal. Recuerda que tienes 72hr para ver el auto y finalizar el pago.");
				    appNotificationDataResponseDTO.setNotificationCode(Constants.KEY_PUSH_CODE_V201_1);
				} else if (scheduleDateBD.get().getAppointmentLocationId() == 3L) {
				    notificationPushResponseDTO.setBody("Confirmo tu cita el día " + appointmentDate + " en Poniente. Recuerda que tienes 72hr para ver el auto y finalizar el pago.");
				    appNotificationDataResponseDTO.setNotificationCode(Constants.KEY_PUSH_CODE_V201_2);
				} else if (scheduleDateBD.get().getAppointmentLocationId() == 2L) {
				    throw new EmptyStackException();
				}

				appNotificationDataResponseDTO.setCarId(sellCarDetail.get().getId().toString());
				appNotificationDataResponseDTO.setCarUrl("http://www.kavak.com/" + sellCarDetail.get().getCarMake() + "-" + sellCarDetail.get().getCarModel() + "-" + sellCarDetail.get().getCarYear() + "-compra-de-autos-" + sellCarDetail.get().getId());
				appNotificationPushResponseDTO.setTo(token.getTokenDevice());
				appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
				appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
				//
				// Map para armar el body
				//
				Map<String, Object> body = new HashMap<String, Object>();
				body.put("notification", appNotificationPushResponseDTO.getNotification());
				body.put("data", appNotificationPushResponseDTO.getData());
				body.put("to", appNotificationPushResponseDTO.getTo());
				//
				// Map para mandar el MapBody y el header
				//
				HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

				// llamada al servicio de fireBase
				ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
				//
				//Guardando en la tabla de log
				AppNotificationLog newAppNotificationLog = new AppNotificationLog();
				newAppNotificationLog.setUserId(saleCheckpointActual.getUserId());
				newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
				newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
				newAppNotificationLog.setTokenId(token.getId());
				newAppNotificationLog.setNotificationMessage(notificationPushResponseDTO.getBody());
				newAppNotificationLog.setNotificationCode(appNotificationDataResponseDTO.getNotificationCode());
				newAppNotificationLog.setFcmMessage(responseFirebase.getBody());

				if (responseFirebase.getBody().toString().contains("\"failure\":1")) {
				    LogService.logger.info("ERROR Mandando notificacion Push appointmentBookedRegisteredAppNotification A el usuario:  " + token.getUserId());
				} else {
				    newAppNotificationLog.setSuccessfulNotification(true);
				    notificationsSent++;
				}
				newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
			    }
			}
			// setar a 1 appNotificationSent
			saleCheckpointActual.setAppointmentBookedNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
		    } catch (Exception ex) {
			saleCheckpointActual.setAppointmentBookedNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
			LogService.logger.info("catch ERROR Mandando notificacion Push appointmentBookedRegisteredAppNotification:  " + ex);
			ex.printStackTrace();
		    }
		}
	    }
	    LogService.logger.info("Total Push Notifications Enviadas de appointmentBookedRegisteredAppNotification: [" + notificationsSent + "] ");
	} catch (Exception e) {
	    LogService.logger.info("catch Externo ERROR Mandando notificacion Push appointmentBookedRegisteredAppNotification:  " + e);
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * PUSH : Push V-201 Cita Agendada appointmentRegisteredAppNotification
     */
    public ResponseDTO appointmentRegisteredAppNotification() {
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	int notificationsSent = 0;
	try {
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findAppointmentRegisteredAppNotification();
	    if (listSaleCheckpointBD != null && !listSaleCheckpointBD.isEmpty()) {
		for (SaleCheckpoint saleCheckpointActual : listSaleCheckpointBD) {
		    try {
			Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.APPOINTMENT_REGISTERED_APP_NOTIFICATION.getValue());
			AppNotificationSetting appNotificationSetting = appNotificationSettingRepo.findAllByIdUserAndTypeId(saleCheckpointActual.getUserId(), AppNotificationPushTypeEnum.APPOINTMENT_REGISTERED_APP_NOTIFICATION.getValue());
			Optional<AppointmentScheduleDate> scheduleDateBD = scheduleDateRepo.findById(saleCheckpointActual.getScheduleDateId());

			if (appNotificationSetting != null && scheduleDateBD.isPresent()) {
			    String appointmentDate = sdf.format(scheduleDateBD.get().getVisitDate());
			    // mandar notificacion
			    NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
			    AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
			    AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
			    RestTemplate restTemplate = new RestTemplate();
			    String url = Constants.PUSH_FIREBASE_URL_SEND;
			    Optional<SellCarDetail> sellCarDetail = sellCarDetailRepo.findById(saleCheckpointActual.getCarId());
			    List<AppNotificationToken> appNotificationToken = appNotificationTokenRepo.findByUserId(saleCheckpointActual.getUser().getId());

			    for (AppNotificationToken token : appNotificationToken) {
				//
				// Armo el header
				//
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", "key=" + Constants.PUSH_FIREBASE_API_KEY);
				headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
				//
				// Variables para el Request
				//
				notificationPushResponseDTO.setTitle("¡Tienes una cita con KAVAK!");
				notificationPushResponseDTO.setBody("Confirmo tu cita el día " + appointmentDate + " en KAVAK.com. Te recuerdo el apartado de nuestros autos es con 5,000 MXN y por 72hr.");
				appNotificationDataResponseDTO.setCarId(sellCarDetail.get().getId().toString());
				appNotificationDataResponseDTO.setCarUrl("http://www.kavak.com/" + sellCarDetail.get().getCarMake() + "-" + sellCarDetail.get().getCarModel() + "-" + sellCarDetail.get().getCarYear() + "-compra-de-autos-" + sellCarDetail.get().getId());
				appNotificationDataResponseDTO.setNotificationCode(Constants.KEY_PUSH_CODE_V201);
				appNotificationPushResponseDTO.setTo(token.getTokenDevice());
				appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
				appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
				//
				// Map para armar el body
				//
				Map<String, Object> body = new HashMap<String, Object>();
				body.put("notification", appNotificationPushResponseDTO.getNotification());
				body.put("data", appNotificationPushResponseDTO.getData());
				body.put("to", appNotificationPushResponseDTO.getTo());
				//
				// Map para mandar el MapBody y el header
				//
				HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

				// llamada al servicio de fireBase
				ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
				//
				//Guardando en la table de log
				AppNotificationLog newAppNotificationLog = new AppNotificationLog();
				newAppNotificationLog.setUserId(saleCheckpointActual.getUserId());
				newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
				newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
				newAppNotificationLog.setTokenId(token.getId());
				newAppNotificationLog.setNotificationMessage(notificationPushResponseDTO.getBody());
				newAppNotificationLog.setNotificationCode(Constants.KEY_PUSH_CODE_V201);
				newAppNotificationLog.setFcmMessage(responseFirebase.getBody());

				if (responseFirebase.getBody().toString().contains("\"failure\":1")) {
				    LogService.logger.info("ERROR Mandando notificacion Push appointmentRegisteredAppNotification A el usuario:  " + token.getUserId());
				} else {    
				    newAppNotificationLog.setSuccessfulNotification(true);
				    notificationsSent++;
				}
				newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
			    }
			}
			// setar a 1 appNotificationSent
			saleCheckpointActual.setAppointmentRegisteredNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
		    } catch (Exception ex) {
			saleCheckpointActual.setAppointmentRegisteredNotificationSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);
			LogService.logger.info("catch ERROR Mandando notificacion Push appointmentRegisteredAppNotification:  " + ex);
			ex.printStackTrace();
		    }
		}
	    }
	    LogService.logger.info("Total Push Notifications Enviadas de appointmentRegisteredAppNotification: [" + notificationsSent + "] ");
	} catch (Exception e) {
	    LogService.logger.info("catch Externo ERROR Mandando notificacion Push appointmentRegisteredAppNotification:  " + e);
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * PUSH : Push V-206 Notificación activa Registrar datos del cliente en Alerta de Notificación appointmentRegisteredAppNotification
     */
    public ResponseDTO carNotificationBookedAppNotification() {
	ResponseDTO responseDTO = new ResponseDTO();
	listMessages = new ArrayList<MessageDTO>();
	int notificationsSent = 0;
	try {
	    List<CustomerNotifications> listCustomerNotificationsBD = customerNotificationsRepo.findCarNotificationBookedPush();
	    if (listCustomerNotificationsBD != null && !listCustomerNotificationsBD.isEmpty()) {
		for (CustomerNotifications customerNotificationsActual : listCustomerNotificationsBD) {
		    try {
			Optional<AppNotificationType> appNotificationTypeBD = appNotificationTypeRepo.findById(AppNotificationPushTypeEnum.BOOKED_NOTIFICATION_APP_NOTIFICATION.getValue());
			AppNotificationSetting appNotificationSetting = appNotificationSettingRepo.findAllByIdUserAndTypeId(customerNotificationsActual.getUser().getId(), AppNotificationPushTypeEnum.BOOKED_NOTIFICATION_APP_NOTIFICATION.getValue());
			if (appNotificationSetting != null) {
			    // mandar notificacion
			    NotificationPushResponseDTO notificationPushResponseDTO = new NotificationPushResponseDTO();
			    AppNotificationPushResponseDTO appNotificationPushResponseDTO = new AppNotificationPushResponseDTO();
			    AppNotificationDataResponseDTO appNotificationDataResponseDTO = new AppNotificationDataResponseDTO();
			    RestTemplate restTemplate = new RestTemplate();
			    String url = Constants.PUSH_FIREBASE_URL_SEND;
			    Optional<SellCarDetail> sellCarDetail = sellCarDetailRepo.findById(customerNotificationsActual.getCarId());
			    List<AppNotificationToken> appNotificationToken = appNotificationTokenRepo.findByUserId(customerNotificationsActual.getUser().getId());

			    for (AppNotificationToken token : appNotificationToken) {
				//
				// Armo el header
				//
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", "key=" + Constants.PUSH_FIREBASE_API_KEY);
				headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
				//
				// Variables para el Request
				//
				notificationPushResponseDTO.setTitle("Notificación activa");
				notificationPushResponseDTO.setBody("No pierdas la oportunidad de comprar con KAVAK, te avisaremos si el auto que buscas se libera ¡Mantente alerta!");
				appNotificationDataResponseDTO.setCarId(sellCarDetail.get().getId().toString());
				appNotificationDataResponseDTO.setCarUrl("https://www.kavak.com/compra-de-autos");
				appNotificationDataResponseDTO.setNotificationCode(Constants.KEY_PUSH_CODE_V206);
				appNotificationPushResponseDTO.setTo(token.getTokenDevice());
				appNotificationPushResponseDTO.setNotification(notificationPushResponseDTO);
				appNotificationPushResponseDTO.setData(appNotificationDataResponseDTO);
				//
				// Map para armar el body
				//
				Map<String, Object> body = new HashMap<String, Object>();
				body.put("notification", appNotificationPushResponseDTO.getNotification());
				body.put("data", appNotificationPushResponseDTO.getData());
				body.put("to", appNotificationPushResponseDTO.getTo());
				//
				// Map para mandar el MapBody y el header
				//
				HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

				// llamada al servicio de fireBase
				ResponseEntity<String> responseFirebase = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
				//
				//Guardando en la tabla de log
				AppNotificationLog newAppNotificationLog = new AppNotificationLog();
				newAppNotificationLog.setUserId(customerNotificationsActual.getUser().getId());
				newAppNotificationLog.setDescription(appNotificationTypeBD.get().getTitle());
				newAppNotificationLog.setAppNotificationType(appNotificationTypeBD.get());
				newAppNotificationLog.setTokenId(token.getId());
				newAppNotificationLog.setNotificationMessage(notificationPushResponseDTO.getBody());
				newAppNotificationLog.setNotificationCode(Constants.KEY_PUSH_CODE_V206);
				newAppNotificationLog.setFcmMessage(responseFirebase.getBody());

				if (responseFirebase.getBody().toString().contains("\"failure\":1")) {
				    LogService.logger.info("ERROR Mandando notificacion Push carNotificationBookedAppNotification A el usuario:  " + token.getUserId());
				} else {
				    newAppNotificationLog.setSuccessfulNotification(true);
				    notificationsSent++;
				}
				newAppNotificationLog = appNotificationLogRepo.save(newAppNotificationLog);
			    }
			}
			// setar a 1 appNotificationSent
			customerNotificationsActual.setCarNotificationActiveAppNotificationSent(true);
			customerNotificationsRepo.save(customerNotificationsActual);
		    } catch (Exception ex) {
			customerNotificationsActual.setCarNotificationActiveAppNotificationSent(true);
			customerNotificationsRepo.save(customerNotificationsActual);
			LogService.logger.info("catch ERROR Mandando notificacion Push carNotificationBookedAppNotification:  " + ex);
			ex.printStackTrace();
		    }
		}
	    }
	    LogService.logger.info("Total Push Notifications Enviadas de carNotificationBookedAppNotification: [" + notificationsSent + "] ");
	} catch (Exception e) {
	    LogService.logger.info("catch Externo ERROR Mandando notificacion Push carNotificationBookedAppNotification:  " + e);
	    e.printStackTrace();
	}
	return responseDTO;
    }

}
