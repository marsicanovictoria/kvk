package com.kavak.core.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.WordUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.financing.FinancingGenericDTO;
import com.kavak.core.dto.financing.FinancingUserPinCodeAuthorizationApiAttributesDTO;
import com.kavak.core.dto.financing.FinancingUserPinCodeAuthorizationApiDataDTO;
import com.kavak.core.dto.financing.FinancingUserProfileDTO;
import com.kavak.core.dto.financing.FinancingUserRegisterApiAttributesDTO;
import com.kavak.core.dto.financing.FinancingUserRegisterApiDataDTO;
import com.kavak.core.dto.financing.FinancingUserUpdateApiAttributesDTO;
import com.kavak.core.dto.financing.FinancingUserUpdateApiDataDTO;
import com.kavak.core.dto.financing.FinancingUserUpdateDomicileApiAttributesDTO;
import com.kavak.core.dto.financing.FinancingUserUpdateDomicileApiDataDTO;
import com.kavak.core.dto.financing.FinancingUserUpdateIncomeApiAttributesDTO;
import com.kavak.core.dto.financing.FinancingUserUpdateIncomeApiDataDTO;
import com.kavak.core.dto.financing.request.FinancingUserDomicileRequestDTO;
import com.kavak.core.dto.financing.request.FinancingUserEscoreApiAttributesRequestDTO;
import com.kavak.core.dto.financing.request.FinancingUserEscoreApiDataRequestDTO;
import com.kavak.core.dto.financing.request.FinancingUserIncomeRequestDTO;
import com.kavak.core.dto.financing.request.FinancingUserPinCodeApiBodyRequestDTO;
import com.kavak.core.dto.financing.request.FinancingUserRfcApiAttributesRequestDTO;
import com.kavak.core.dto.financing.request.FinancingUserRfcApiDataRequestDTO;
import com.kavak.core.dto.financing.request.FinancingUserScoreRequestDTO;
import com.kavak.core.dto.financing.request.FinancingVerifyUserInformationRequestDTO;
import com.kavak.core.dto.financing.request.FinancingVerifyUserInformationResponseDTO;
import com.kavak.core.dto.financing.response.FinancingApiMarketPlaceRfcGeneratorResponseDTO;
import com.kavak.core.dto.financing.response.FinancingGenericResponseDTO;
import com.kavak.core.dto.financing.response.FinancingPostFinancingUserPinCodeResponseDTO;
import com.kavak.core.dto.financing.response.FinancingUserAuthorizationPinCodeApiBodyResponseDTO;
import com.kavak.core.dto.financing.response.FinancingUserEscoreApiBodyResponseDTO;
import com.kavak.core.dto.financing.response.FinancingUserEscoreProfileResponseDTO;
import com.kavak.core.dto.financing.response.FinancingUserEscoreResponseDTO;
import com.kavak.core.dto.financing.response.FinancingUserProfileExistResponseDTO;
import com.kavak.core.dto.financing.response.FinancingUserResponseDTO;
import com.kavak.core.dto.financing.response.FinancingUserRfcApiBodyResponseDTO;
import com.kavak.core.dto.financing.response.FinancingUserUpdateDomicileApiResponseDTO;
import com.kavak.core.dto.financing.response.FinancingUserUpdateIncomeApiResponseDTO;
import com.kavak.core.dto.financing.response.FinancingVerifyUserInformationRegisterApiResponseDTO;
import com.kavak.core.dto.financing.response.FinancingVerifyUserUpdatePersonalDataApiResponseDTO;
import com.kavak.core.dto.financing.response.GetFinancingUserRfcResponseDTO;
import com.kavak.core.dto.request.PostFinancingUserPinCodeAuthorizationRequestDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.FinancingUser;
import com.kavak.core.model.FinancingUserDomicile;
import com.kavak.core.model.FinancingUserIncome;
import com.kavak.core.model.FinancingUserProfile;
import com.kavak.core.model.FinancingUserScore;
import com.kavak.core.model.FormOptionValue;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.User;
import com.kavak.core.repositories.FinancingUserDomicileRepository;
import com.kavak.core.repositories.FinancingUserIncomeRepository;
import com.kavak.core.repositories.FinancingUserProfileRepository;
import com.kavak.core.repositories.FinancingUserRepository;
import com.kavak.core.repositories.FinancingUserScoreRepository;
import com.kavak.core.repositories.FormOptionValueRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.LogService;

@Stateless
public class FinancingServices {

    private List<MessageDTO> listMessages;

    @Inject
    private UserRepository userRepo;

    @Inject
    private FinancingUserRepository financingUserRepo;

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private FinancingUserProfileRepository financingUserProfileRepo;

    @Inject
    private FormOptionValueRepository formOptionValueRepo;

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private FinancingUserDomicileRepository financingUserDomicileRepo;

    @Inject
    private FinancingUserIncomeRepository financingUserIncomeRepo;
    
    @Inject
    private FinancingUserScoreRepository financingUserScoreRepo;
    
    @Inject
    private SaleCheckpointRepository saleCheckpointRepo;
    
    @Inject
    private SellCarDetailRepository SellCarDetailRepo;
    
    
    

    /**
     * Servicio para saber si un cliente tiene un perfil de financiamiento creado.
     * @author Juan Tolentino
     * @param path {userId}
     * @return ResponseDTO Response Generico de servicio
     */
    public ResponseDTO getFinancingUserProfileExist(Long userId){
	ResponseDTO responseDTO = new ResponseDTO();
	FinancingUserProfileExistResponseDTO financingUserProfileExistResponseDTO = new FinancingUserProfileExistResponseDTO();
	LogService.logger.info("Iniciando Metodo para saber si un cliente tiene un perfil de financiamiento creado");
	List<MessageDTO> listMessageDTO = new ArrayList<>();
	MessageDTO messageNoRecords = null;
	if (userId == null) {
	    // error en los parametros
	    messageNoRecords = messageRepo.findByCode(MessageEnum.M0054.toString()).getDTO();
	    listMessageDTO.add(messageNoRecords);
	    EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult2.getValue());
	    responseDTO.setStatus(enumResult2.getStatus());
	    responseDTO.setListMessage(listMessageDTO);
	    return responseDTO;
	}

	Optional<User> userDB = userRepo.findById(userId);

	if (userDB == null) {
	    LogService.logger.info("El usuario no existe en Base de datos..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0016.toString()).getDTO();
	    listMessageDTO.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessageDTO);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(new GenericDTO());
	}

	List<FinancingUser> listFinancingUserBD = new ArrayList<>();
	listFinancingUserBD = financingUserRepo.findByUserIdAndActiveTrue(userId);
	if(listFinancingUserBD.isEmpty()) {
	    financingUserProfileExistResponseDTO.setExist("false");
	}else {
	    financingUserProfileExistResponseDTO.setExist("true");
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(financingUserProfileExistResponseDTO);
	LogService.logger.info("FIN Metodo para saber si un cliente tiene un perfil de financiamiento creado");
	return responseDTO;

    }


    /**
     * Servicio que lista los diferentes perfiles de financiamiento
     * @author Juan Tolentino
     * @return listFinancingUserProfileDTO
     */
    public ResponseDTO getFinancingUserProfiles(){
	ResponseDTO responseDTO = new ResponseDTO();
	LogService.logger.info("Iniciando Metodo para saber la lista los diferentes perfiles de financiamiento");

	List<FinancingUserProfile> listFinancingUserProfile = financingUserProfileRepo.findAll();
	List<FinancingUserProfileDTO> listFinancingUserProfileDTO = new ArrayList<>();

	if(!listFinancingUserProfile.isEmpty()){
	    for(FinancingUserProfile financingUserProfileActual : listFinancingUserProfile) {
		FinancingUserProfileDTO financingUserProfileDTO = new FinancingUserProfileDTO();
		financingUserProfileDTO.setId(financingUserProfileActual.getId());
		financingUserProfileDTO.setContent(financingUserProfileActual.getContent());
		financingUserProfileDTO.setName(financingUserProfileActual.getName());
		listFinancingUserProfileDTO.add(financingUserProfileDTO);
	    }
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listFinancingUserProfileDTO);
	LogService.logger.info("FIN Metodo para saber la lista los diferentes perfiles de financiamiento");
	return responseDTO;

    }


    /**
     * Servicio para listar el RFC de un usuario
     * @author Juan Tolentino
     * @return listFinancingUserProfileDTO
     */
    public ResponseDTO getFinancingUserRfc(String firstSurname, String secondSurname, String birthdate, String name){
	ResponseDTO responseDTO = new ResponseDTO();
	LogService.logger.info("Iniciando Metodo para saber el RFC de un usuario");
	RestTemplate restTemplate = new RestTemplate();

	GetFinancingUserRfcResponseDTO financingUserRfcResponseDTO = new GetFinancingUserRfcResponseDTO();
	String url = "https://jfhe88-rfc-generator-mexico.p.mashape.com/rest1/rfc/get?apellido_materno="+firstSurname+"&apellido_paterno="+secondSurname+"&fecha="+birthdate+"&nombre="+name+"&solo_homoclave=0";
	try {
	    // Armo el header
	    HttpHeaders headers = new HttpHeaders();
	    headers.set("X-Mashape-Key", Constants.FINANCING_API_MARKET_RFC_GENERATOR_API_KEY);	    
	    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

	    HttpEntity<Object> entity = new HttpEntity<Object>(headers);

	    HttpComponentsClientHttpRequestFactory requestFactory =  new  HttpComponentsClientHttpRequestFactory ();
	    restTemplate.setRequestFactory(requestFactory);

	    ResponseEntity<FinancingApiMarketPlaceRfcGeneratorResponseDTO> financingApiMarketPlaceRfcGeneratorResponseDTO = restTemplate.exchange(url, HttpMethod.GET, entity, FinancingApiMarketPlaceRfcGeneratorResponseDTO.class);
	    LogService.logger.info("FIN Metodo para saber el RFC de un usuario");
	    financingUserRfcResponseDTO.setRfc(financingApiMarketPlaceRfcGeneratorResponseDTO.getBody().getResponse().getData().getRfc());
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus()); 
	    responseDTO.setData(financingUserRfcResponseDTO);
	    return responseDTO;

	} catch (Exception ex) {
	    LogService.logger.error("Catch error en el api FinancingApiMarketPlaceRfcGenerator");
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus()); 
	    return responseDTO;
	}
    }


    /**
     * Servicio para saber los diferentes tipos de living_type
     * @author Juan Tolentino
     * @return getFinancingLivingTypes
     */
    public ResponseDTO getFinancingLivingTypes(){
	ResponseDTO responseDTO = new ResponseDTO();
	FinancingGenericResponseDTO genericResponseDTO = new FinancingGenericResponseDTO();
	List<FinancingGenericResponseDTO> listGenericResponseDTO = new ArrayList<>();	
	LogService.logger.info("Iniciando Metodo para saber los diferentes tipos de living_type");	
	List<FormOptionValue> listFormOptionValue = formOptionValueRepo.findByCategoryAndActiveTrueOrderById(Constants.FINANCING_LIVING_TYPES);

	if(!listFormOptionValue.isEmpty()){
	    for(FormOptionValue formOptionValueActual : listFormOptionValue) {
		genericResponseDTO = new FinancingGenericResponseDTO();
		genericResponseDTO.setName(formOptionValueActual.getName());
		genericResponseDTO.setId(formOptionValueActual.getValue());
		listGenericResponseDTO.add(genericResponseDTO);
	    }
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listGenericResponseDTO);
	LogService.logger.info("FIN Metodo para saber los diferentes tipos de living_type");
	return responseDTO;

    }


    /**
     * Servicio para saber los diferentes tipos de income profile
     * @author Juan Tolentino
     * @return getFinancingIncomeProfiles
     */
    public ResponseDTO getFinancingIncomeProfiles(){
	ResponseDTO responseDTO = new ResponseDTO();
	FinancingGenericResponseDTO genericResponseDTO = new FinancingGenericResponseDTO();
	List<FinancingGenericResponseDTO> listGenericResponseDTO = new ArrayList<>();	
	LogService.logger.info("Iniciando Metodo para saber los diferentes tipos de income profile");	
	List<FormOptionValue> listFormOptionValue = formOptionValueRepo.findByCategoryAndActiveTrueOrderById(Constants.FINANCING_INCOME_PROFILE);

	if(!listFormOptionValue.isEmpty()){
	    for(FormOptionValue formOptionValueActual : listFormOptionValue) {
		genericResponseDTO = new FinancingGenericResponseDTO();
		genericResponseDTO.setName(formOptionValueActual.getName());
		genericResponseDTO.setId(formOptionValueActual.getValue());
		listGenericResponseDTO.add(genericResponseDTO);
	    }
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listGenericResponseDTO);
	LogService.logger.info("FIN Metodo para saber los diferentes tipos de income profile");
	return responseDTO;

    }


    /**
     * Servicio para saber los diferentes tipos de automotive_car_use_type
     * @author Juan Tolentino
     * @return getFinancingCarUseType
     */
    public ResponseDTO getFinancingAutomotiveCarUseTypes(){
	ResponseDTO responseDTO = new ResponseDTO();
	FinancingGenericResponseDTO genericResponseDTO = new FinancingGenericResponseDTO();
	List<FinancingGenericResponseDTO> listGenericResponseDTO = new ArrayList<>();	
	LogService.logger.info("Iniciando Metodo para saber los diferentes tipos de automotive_car_use_types");	
	List<FormOptionValue> listFormOptionValue = formOptionValueRepo.findByCategoryAndActiveTrueOrderById(Constants.FINANCING_AUTOMOTIVE_CAR_USE_TYPES);

	if(!listFormOptionValue.isEmpty()){
	    for(FormOptionValue formOptionValueActual : listFormOptionValue) {
		genericResponseDTO = new FinancingGenericResponseDTO();
		genericResponseDTO.setId(formOptionValueActual.getValue());
		genericResponseDTO.setName(formOptionValueActual.getName());
		listGenericResponseDTO.add(genericResponseDTO);
	    }
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listGenericResponseDTO);
	LogService.logger.info("FIN Metodo para saber los diferentes tipos de automotive_car_use_type");
	return responseDTO;

    }


    /**
     * Servicio para saber la firma para el request a arbol financiero
     * @author Juan Tolentino
     * @return getFinancingCarUseType
     */
    public ResponseDTO getFinancingSignature(){
	ResponseDTO responseDTO = new ResponseDTO();
	//FinancingSignatureResponseDTO financingSignatureResponseDTO = new FinancingSignatureResponseDTO();
	LogService.logger.info("INICIO Metodo para saber la firma");
	String apiKey = Constants.FINANCING_API_KEY;
	String secretKey = Constants.FINANCING_SECRET_KEY;
	Long unixTime = System.currentTimeMillis() / 1000L;
	String value = apiKey + unixTime.toString().substring(0,9);

	try {
	    // Get an hmac_sha1 key from the raw key bytes
	    byte[] keyBytes = secretKey.getBytes();           
	    SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

	    // Get an hmac_sha1 Mac instance and initialize with the signing key
	    Mac mac = Mac.getInstance("HmacSHA1");
	    mac.init(signingKey);

	    // Compute the hmac on input data bytes
	    byte[] rawHmac = mac.doFinal(value.getBytes());

	    // Convert raw bytes to Hex
	    byte[] hexBytes = new Hex().encode(rawHmac);

	    //financingSignatureResponseDTO.setSignature( new String(hexBytes, "UTF-8"));
	    String signature2 = new String(hexBytes, "UTF-8");

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(signature2);
	    LogService.logger.info("FIN Metodo para saber la firma");

	    return responseDTO;
	} catch (Exception e) {
	    e.printStackTrace();
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    LogService.logger.info("FIN Metodo para saber la firma");
	    return responseDTO;
	}

    }



    /**
     * Servicio para verificar información de un usuario de árbol financiero
     * @author Juan Tolentino
     * @return getFinancingCarUseType
     */
    /*    public ResponseDTO postFinancingVerifyUserInformation(FinancingVerifyUserInformationRequestDTO financingVerifyUserInformationRequestDTO){
	ResponseDTO responseDTO = new ResponseDTO();
	ResponseEntity<FinancingVerifyUserInformationRegisterApiResponseDTO> financingUserRegisterApiResponseDTO = null;
	GenericDTO genericDTO = new GenericDTO();
	FinancingUser financingUserEmailExistBD = new FinancingUser();
	FinancingVerifyUserInformationResponseDTO financingVerifyUserInformationResponseDTO = new FinancingVerifyUserInformationResponseDTO();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	LogService.logger.info("Iniciando Servicio para verificar información de un usuario de árbol financiero");	
	boolean exist = false;
	List<FinancingUser> listFinancingUserBD = financingUserRepo.findByUserIdOrderBycreationDateDescAndActiveTrue(financingVerifyUserInformationRequestDTO.getUserId());
	financingUserEmailExistBD = financingUserRepo.findByEmail(financingVerifyUserInformationRequestDTO.getEmail());
	if(!listFinancingUserBD.isEmpty()) {
	    if(financingUserEmailExistBD != null) {
		//Contando el registro que viene debe tener 3 o menos
		MetaValue metaValueBD = metaValueRepo.findByAlias(Constants.FINANCING_MAXIMUM_VERIFY_USER_INFORMATION);
		if(listFinancingUserBD.size() <= Long.valueOf(metaValueBD.getOptionName())) {
		    for(FinancingUser financingUserActual : listFinancingUserBD) {

			//Se verifica si el email existe asociado a otro rfc- retorna falso (debe cambiar el email o el rfc)
			//if(financingUserActual.getEmail().equals(financingVerifyUserInformationRequestDTO.getEmail())) {
			//validando que email/phone y rfc sean unicos
			if(financingUserActual.getEmail().equals(financingVerifyUserInformationRequestDTO.getEmail())) {

			    if(!financingUserActual.getPhone().equals(financingVerifyUserInformationRequestDTO.getPhone())) {
				genericDTO.setValue("false");
				genericDTO.setMessage("Tus datos de contactos estan asociados a otro RFC "+financingVerifyUserInformationRequestDTO.getRfc()+" sugerimos cambiar correo y telefono para continuar"); // crear el mensaje en tabla de message
				EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
				responseDTO.setCode(enumResult.getValue());
				responseDTO.setStatus(enumResult.getStatus());
				responseDTO.setData(genericDTO);
				return responseDTO;
			    }

			    if(!financingUserActual.getRfc().equals(financingVerifyUserInformationRequestDTO.getRfc())) {
				genericDTO.setValue("false");
				genericDTO.setMessage("Tus datos de contactos estan asociados a otro RFC "+financingVerifyUserInformationRequestDTO.getRfc()+" sugerimos cambiar correo y telefono para continuar"); // crear el mensaje en tabla de message
				EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
				responseDTO.setCode(enumResult.getValue());
				responseDTO.setStatus(enumResult.getStatus());
				responseDTO.setData(genericDTO);
				return responseDTO;
			    }

			    if(!financingUserActual.getEmail().equals(financingVerifyUserInformationRequestDTO.getEmail())) {
				genericDTO.setValue("false");
				genericDTO.setMessage("Tus datos de contactos estan asociados a otro RFC "+financingVerifyUserInformationRequestDTO.getRfc()+" sugerimos cambiar correo y telefono para continuar"); // crear el mensaje en tabla de message
				EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
				responseDTO.setCode(enumResult.getValue());
				responseDTO.setStatus(enumResult.getStatus());
				responseDTO.setData(genericDTO);
				return responseDTO;
			    }

			    Optional<FinancingUser> financingUserBD = financingUserRepo.findById(financingUserActual.getId());

			    FinancingUser newFinancingUserUpdateBD = new FinancingUser();

			    if(!financingUserBD.get().getFullName().equals(financingVerifyUserInformationRequestDTO.getFullName()) 
				    || !financingUserBD.get().getFirstName().equals(financingVerifyUserInformationRequestDTO.getFirstSurname()) 
				    || !financingUserBD.get().getSecondName().equals(financingVerifyUserInformationRequestDTO.getSecondSurname())
				    || !financingUserBD.get().getGender().equals(financingVerifyUserInformationRequestDTO.getGender())
				    || !financingUserBD.get().getPhone().equals(financingVerifyUserInformationRequestDTO.getPhone())
				    || !financingUserBD.get().getRfc().equals(financingVerifyUserInformationRequestDTO.getRfc())) {

				//Actualizar usuario en arbol financiero
				ResponseEntity<FinancingVerifyUserUpdatePersonalDataApiResponseDTO> financingUserUpdateApiResponse = postFinancingUserUpdateApi(financingUserBD.get().getFinancingUserId().longValue(), financingVerifyUserInformationRequestDTO);
				if(financingUserUpdateApiResponse == null) {
				    LogService.logger.info("Bad Request / No Actualizo");
				    EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
				    responseDTO.setCode(enumResultUpdate.getValue());
				    responseDTO.setStatus(enumResultUpdate.getStatus());
				    return responseDTO;
				}

				//Actualizando la tabla v2_financing_user 
				financingUserBD.get().setFullName(financingVerifyUserInformationRequestDTO.getFullName());
				financingUserBD.get().setFirstName(financingVerifyUserInformationRequestDTO.getFirstSurname());
				financingUserBD.get().setSecondName(financingVerifyUserInformationRequestDTO.getSecondSurname());
				financingUserBD.get().setGender(financingVerifyUserInformationRequestDTO.getGender());
				financingUserBD.get().setPhone(financingVerifyUserInformationRequestDTO.getPhone());
				financingUserBD.get().setRfc(financingVerifyUserInformationRequestDTO.getRfc());

				newFinancingUserUpdateBD = financingUserRepo.save(financingUserBD.get());

				//llenado parcial
				financingVerifyUserInformationResponseDTO.setIdFinancing(newFinancingUserUpdateBD.getFinancingUserId().toString());
				financingVerifyUserInformationResponseDTO.setEmail(newFinancingUserUpdateBD.getEmail());
				financingVerifyUserInformationResponseDTO.setAuthenticationToken(newFinancingUserUpdateBD.getAuthenticationToken());
				financingVerifyUserInformationResponseDTO.setFullName(newFinancingUserUpdateBD.getFullName());
				financingVerifyUserInformationResponseDTO.setFirstSurname(newFinancingUserUpdateBD.getFirstName());//cambiar a firstSurname
				financingVerifyUserInformationResponseDTO.setSecondSurname(newFinancingUserUpdateBD.getSecondName());//cambiar a secondSurname
				financingVerifyUserInformationResponseDTO.setGender(newFinancingUserUpdateBD.getGender());
				financingVerifyUserInformationResponseDTO.setBirthday(newFinancingUserUpdateBD.getBirthday().toString());
				financingVerifyUserInformationResponseDTO.setPhone(newFinancingUserUpdateBD.getPhone());
				financingVerifyUserInformationResponseDTO.setRfc(newFinancingUserUpdateBD.getRfc());

				try {
				    financingVerifyUserInformationResponseDTO.setBirthday(newFinancingUserUpdateBD.getBirthday().toString());
				} catch (Exception e) {
				    e.printStackTrace();
				}

			    }else {
				//llenado parcial
				financingVerifyUserInformationResponseDTO.setIdFinancing(financingUserBD.get().getId().toString());
				financingVerifyUserInformationResponseDTO.setEmail(financingUserBD.get().getEmail());
				financingVerifyUserInformationResponseDTO.setAuthenticationToken(financingUserBD.get().getAuthenticationToken());
				financingVerifyUserInformationResponseDTO.setFullName(financingUserBD.get().getFullName());
				financingVerifyUserInformationResponseDTO.setFirstSurname(financingUserBD.get().getFirstName());//cambiar a firstSurname
				financingVerifyUserInformationResponseDTO.setSecondSurname(financingUserBD.get().getSecondName());//cambiar a secondSurname
				financingVerifyUserInformationResponseDTO.setGender(financingUserBD.get().getGender());
				financingVerifyUserInformationResponseDTO.setPhone(financingUserBD.get().getPhone());
				financingVerifyUserInformationResponseDTO.setRfc(financingUserBD.get().getRfc());

				try {
				    financingVerifyUserInformationResponseDTO.setBirthday(financingUserBD.get().getBirthday().toString());
				} catch (Exception e) {
				    e.printStackTrace();
				}
			    }


			    try {
				if(financingUserBD.get().getBuroDate() != null) {
				    String today = dateFormat.format(new Date());  
				    Date startDate=dateFormat.parse(financingUserBD.get().getBuroDate().toString());
				    Date endDate=dateFormat.parse(today);
				    int dates=(int) ((endDate.getTime()-startDate.getTime())/86400000);

				    if(financingUserActual.getBuroDate() == null || dates >= 90 ) {
					financingVerifyUserInformationResponseDTO.setShowBuro("true");
				    }else {
					financingVerifyUserInformationResponseDTO.setShowBuro("false");
				    }
				}else {
				    financingVerifyUserInformationResponseDTO.setShowBuro("true");
				}

			    } catch (Exception e) {
				e.printStackTrace();
			    }

			    FinancingUserDomicile FinancingUserDomicileBD = financingUserDomicileRepo.findByUserIdAndActiveTrue(financingUserBD.get().getUserId());
			    if(FinancingUserDomicileBD != null) {

				//llenado completo
				financingVerifyUserInformationResponseDTO.setStreet(FinancingUserDomicileBD.getStreet());
				financingVerifyUserInformationResponseDTO.setExternalNumber(FinancingUserDomicileBD.getExternalNumber());
				financingVerifyUserInformationResponseDTO.setInternalNumber(FinancingUserDomicileBD.getInternalNumber());
				financingVerifyUserInformationResponseDTO.setColony(FinancingUserDomicileBD.getColony());
				financingVerifyUserInformationResponseDTO.setDelegation(FinancingUserDomicileBD.getDelegation());
				financingVerifyUserInformationResponseDTO.setState(FinancingUserDomicileBD.getState());
				financingVerifyUserInformationResponseDTO.setZipCode(FinancingUserDomicileBD.getZipCode());
				financingVerifyUserInformationResponseDTO.setResidentYears(FinancingUserDomicileBD.getResidentYears());
				financingVerifyUserInformationResponseDTO.setLivingType(FinancingUserDomicileBD.getLivingType());
				//financingVerifyUserInformationResponseDTO.setNetIncomeVerified(FinancingUserDomicileBD.);
				//financingVerifyUserInformationResponseDTO.setIncomeProfile(FinancingUserDomicileBD.);
				///financingVerifyUserInformationResponseDTO.setPositionAge(positionAge);
				//financingVerifyUserInformationResponseDTO.setAutomotiveCarUseType(automotiveCarUseType);

			    }

			    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
			    responseDTO.setCode(enumResult.getValue());
			    responseDTO.setStatus(enumResult.getStatus());
			    responseDTO.setData(financingVerifyUserInformationResponseDTO);
			    return responseDTO;

			    //    }//email

			}else {
			    exist = true;
			}
		    }//for


		}else {//limite exedido de perfiles de usuario permitidos
		    //que pasa si tiene 3 o mas?
		    // da error no puede anexar mas perfiles 
		    genericDTO.setValue("false");
		    genericDTO.setMessage("Ya supero el limite de perfiles permitidos"); // crear el mensaje en tabla de message
		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setData(genericDTO);
		    return responseDTO;
		}
	    }else {exist = true;}
	}
	if(exist || listFinancingUserBD.isEmpty()) {//no tiene perfil hay que crearlo y actualizarlo en arbol financiero

	    financingUserEmailExistBD = financingUserRepo.findByEmail(financingVerifyUserInformationRequestDTO.getEmail());
	    if(financingUserEmailExistBD != null) {
		LogService.logger.info("email existe");
		genericDTO.setMessage("Email ya existe");
		EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
		responseDTO.setCode(enumResultUpdate.getValue());
		responseDTO.setStatus(enumResultUpdate.getStatus());
		return responseDTO;
	    }

	    //Registrar usuario en arbol financiero
	    financingUserRegisterApiResponseDTO = postFinancingUserRegisterApi(financingVerifyUserInformationRequestDTO);
	    if(financingUserRegisterApiResponseDTO == null) {
		LogService.logger.info("email ya existe en Arbol financiero");
		genericDTO.setMessage("Email ya existe en arbol financiero");
		EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
		responseDTO.setCode(enumResultUpdate.getValue());
		responseDTO.setStatus(enumResultUpdate.getStatus());
		responseDTO.setData(genericDTO);
		return responseDTO;
	    }

	    //llenar la tabla v2_financing_user
	    FinancingUser newFinancingUser = new FinancingUser();
	    newFinancingUser.setFinancingUserId(Long.valueOf(financingUserRegisterApiResponseDTO.getBody().getData().getIdFinancing()));
	    newFinancingUser.setAuthenticationToken(financingUserRegisterApiResponseDTO.getBody().getData().getAttributes().getAuthenticationToken().toString());
	    newFinancingUser.setUserId(financingVerifyUserInformationRequestDTO.getUserId());
	    newFinancingUser.setEmail(financingVerifyUserInformationRequestDTO.getEmail());
	    newFinancingUser.setFullName(financingVerifyUserInformationRequestDTO.getFullName());
	    newFinancingUser.setFirstName(financingVerifyUserInformationRequestDTO.getFirstSurname());//cambiar a firstSurname
	    newFinancingUser.setSecondName(financingVerifyUserInformationRequestDTO.getSecondSurname());//cambiar a secondSurname
	    newFinancingUser.setGender(financingVerifyUserInformationRequestDTO.getGender());
	    newFinancingUser.setBirthday(financingVerifyUserInformationRequestDTO.getBirthday());
	    newFinancingUser.setPhone(financingVerifyUserInformationRequestDTO.getPhone());
	    newFinancingUser.setRfc(financingVerifyUserInformationRequestDTO.getRfc());
	    newFinancingUser.setActive(true);

	    financingUserRepo.save(newFinancingUser);

	    //Actualizar usuario en arbol financiero
	    ResponseEntity<FinancingVerifyUserUpdatePersonalDataApiResponseDTO> financingUserUpdateApiResponse = postFinancingUserUpdateApi(Long.valueOf(financingUserRegisterApiResponseDTO.getBody().getData().getIdFinancing()), financingVerifyUserInformationRequestDTO);

	    if(financingUserUpdateApiResponse == null) {
		LogService.logger.info("Bad Request / No Actualizo");
		EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		responseDTO.setCode(enumResultUpdate.getValue());
		responseDTO.setStatus(enumResultUpdate.getStatus());
		return responseDTO;
	    }

	    //llenando el DTO de respuesta
	    financingVerifyUserInformationResponseDTO.setIdFinancing(financingUserRegisterApiResponseDTO.getBody().getData().getIdFinancing().toString());
	    financingVerifyUserInformationResponseDTO.setAuthenticationToken(financingUserRegisterApiResponseDTO.getBody().getData().getAttributes().getAuthenticationToken().toString());
	    financingVerifyUserInformationResponseDTO.setEmail(financingVerifyUserInformationRequestDTO.getEmail());
	    financingVerifyUserInformationResponseDTO.setFullName(financingVerifyUserInformationRequestDTO.getFullName());
	    financingVerifyUserInformationResponseDTO.setFirstSurname(financingVerifyUserInformationRequestDTO.getFirstSurname());//cambiar a firstSurname
	    financingVerifyUserInformationResponseDTO.setSecondSurname(financingVerifyUserInformationRequestDTO.getSecondSurname());//cambiar a secondSurname
	    financingVerifyUserInformationResponseDTO.setGender(financingVerifyUserInformationRequestDTO.getGender());
	    financingVerifyUserInformationResponseDTO.setPhone(financingVerifyUserInformationRequestDTO.getPhone());
	    financingVerifyUserInformationResponseDTO.setBirthday(financingVerifyUserInformationRequestDTO.getBirthday().toString());
	    financingVerifyUserInformationResponseDTO.setRfc(financingVerifyUserInformationRequestDTO.getRfc());
	    financingVerifyUserInformationResponseDTO.setShowBuro("true");	    


	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(financingVerifyUserInformationResponseDTO);
	    LogService.logger.info("FIN Servicio para verificar información de un usuario de árbol financiero");
	    return responseDTO;

	}
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(genericDTO);
	LogService.logger.info("FIN Servicio para verificar información de un usuario de árbol financiero");
	return responseDTO;

    }*/




    /**
     * Servicio para registrar un usuario en arbol financiero
     * @author Juan Tolentino
     * @return postFinancingUserRegisterApi
     */
    public ResponseEntity<FinancingVerifyUserInformationRegisterApiResponseDTO> postFinancingUserRegisterApi(FinancingVerifyUserInformationRequestDTO financingVerifyUserInformationRequestDTO){
	ResponseEntity<FinancingVerifyUserInformationRegisterApiResponseDTO> financingUserRegisterApiResponseDTO = null;

	try {
	    // Armo el header		
	    ResponseDTO signature = getFinancingSignature();
	    RestTemplate restTemplate = new RestTemplate();
	    String urlRegistry = Constants.FINANCING_API_URL + "/api/v1/users";
	    HttpHeaders headers = new HttpHeaders();
	    headers.set("x-api-key", Constants.FINANCING_API_KEY);
	    headers.set("x-signature", signature.getData().toString());
	    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

	    // Variables para el Request
	    FinancingUserRegisterApiDataDTO financingUserRegisterApiDataDTO = new FinancingUserRegisterApiDataDTO();
	    FinancingUserRegisterApiAttributesDTO financingUserRegisterApiAttributesDTO = new FinancingUserRegisterApiAttributesDTO();
	    financingUserRegisterApiDataDTO.setType("users");
	    financingUserRegisterApiAttributesDTO.setEmail(financingVerifyUserInformationRequestDTO.getEmail());
	    financingUserRegisterApiDataDTO.setAttributes(financingUserRegisterApiAttributesDTO);

	    // Map para armar el body
	    Map<String, Object> body = new HashMap<String, Object>();
	    body.put("data", financingUserRegisterApiDataDTO);
	    //
	    // Map para mandar el MapBody y el header
	    HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

	    // llamada al servicio de arbol financiero
	    financingUserRegisterApiResponseDTO = restTemplate.exchange(urlRegistry, HttpMethod.POST, entity, FinancingVerifyUserInformationRegisterApiResponseDTO.class);

	    LogService.logger.info("FIN Servicio para registrar información de un usuario de árbol financiero");
	    return financingUserRegisterApiResponseDTO;

	} catch (Exception e) {
	    e.printStackTrace();
	    LogService.logger.info("CATCH user register api : "+e);
	    return financingUserRegisterApiResponseDTO;
	}
    }



    /**
     * Servicio para actualizar un usuario en arbol financiero
     * @author Juan Tolentino
     * @return postFinancingUserRegisterApi
     */
    public ResponseEntity<FinancingVerifyUserUpdatePersonalDataApiResponseDTO> postFinancingUserUpdateApi(Long idUserFinancing, FinancingVerifyUserInformationRequestDTO financingVerifyUserInformationRequestDTO){
	ResponseEntity<FinancingVerifyUserUpdatePersonalDataApiResponseDTO> responseUserUpdateApi = null;
	LogService.logger.info("INICIO Servicio para update de información de un usuario de árbol financiero");
	try {
	    //servicio de actualiar user en arbol financiero
	    ResponseDTO signatureUpdate = getFinancingSignature();
	    RestTemplate restTemplateUpdate = new RestTemplate();		
	    String urlUpdate = Constants.FINANCING_API_URL + "/api/v1/users/"+idUserFinancing+"/personal_data";
	    HttpHeaders headersUpdate = new HttpHeaders();
	    headersUpdate.set("x-api-key", Constants.FINANCING_API_KEY);
	    headersUpdate.set("x-signature", signatureUpdate.getData().toString());
	    headersUpdate.set("Accept", MediaType.APPLICATION_JSON_VALUE);
	    headersUpdate.set("X-HTTP-Method-Override", "PATCH");


	    //variables para el request
	    FinancingUserUpdateApiDataDTO financingUserUpdateApiDataDTO = new FinancingUserUpdateApiDataDTO();
	    FinancingUserUpdateApiAttributesDTO financingUserUpdateApiAttributesDTO = new FinancingUserUpdateApiAttributesDTO();
	    financingUserUpdateApiDataDTO.setType("personalData");
	    financingUserUpdateApiAttributesDTO.setName(financingVerifyUserInformationRequestDTO.getFullName());
	    financingUserUpdateApiAttributesDTO.setFirstSurname(financingVerifyUserInformationRequestDTO.getFirstSurname());
	    financingUserUpdateApiAttributesDTO.setSecondSurname(financingVerifyUserInformationRequestDTO.getSecondSurname());
	    financingUserUpdateApiAttributesDTO.setGender(financingVerifyUserInformationRequestDTO.getGender());
	    financingUserUpdateApiAttributesDTO.setMobilePhone(financingVerifyUserInformationRequestDTO.getPhone()); 
	    financingUserUpdateApiAttributesDTO.setBirthday(financingVerifyUserInformationRequestDTO.getBirthday().toString());
	    financingUserUpdateApiAttributesDTO.setRfc(financingVerifyUserInformationRequestDTO.getRfc());
	    financingUserUpdateApiDataDTO.setAttributes(financingUserUpdateApiAttributesDTO);
	    //
	    // Map para armar el body de update
	    Map<String, Object> bodyUpdate = new HashMap<String, Object>();
	    bodyUpdate.put("data", financingUserUpdateApiDataDTO);
	    //
	    // Map para mandar el MapBody y el header de update
	    HttpEntity<Object> entityUpdate = new HttpEntity<Object>(bodyUpdate, headersUpdate);

	    HttpComponentsClientHttpRequestFactory requestFactory =  new  HttpComponentsClientHttpRequestFactory ();
	    restTemplateUpdate.setRequestFactory(requestFactory);

	    // llamada al servicio de arbol financiero
	    responseUserUpdateApi = restTemplateUpdate.exchange(urlUpdate, HttpMethod.PATCH, entityUpdate, FinancingVerifyUserUpdatePersonalDataApiResponseDTO.class);
	    LogService.logger.info("FIN Servicio para update de información de un usuario de árbol financiero");

	    return responseUserUpdateApi;
	} catch (Exception ex) {
	    LogService.logger.error("Catch error en el api Arbol Financiero user personal data update:"+ ex);

	    return responseUserUpdateApi;
	}
    }



    /**
     * Servicio para actualizar el domicilio de un usuario
     * @author Juan Tolentino
     * @return postFinancingUserDomicile
     */
    public ResponseDTO postFinancingUserDomicile(Long idUserFinancing , FinancingUserDomicileRequestDTO financingUserDomicileRequestDTO){
	ResponseDTO responseDTO = new ResponseDTO();
	ResponseEntity<FinancingUserUpdateDomicileApiResponseDTO> financingUserUpdateDomicileApiResponseDTO = null;
	LogService.logger.info("INICIO Servicio para update de domicile de un usuario de árbol financiero");
	GenericDTO genericDTO = new GenericDTO();
	FinancingUser financingUserBD = financingUserRepo.findByFinancingUserIdAndActiveTrue(idUserFinancing);

	if(financingUserBD != null) {
	    FinancingUserDomicile financingUserDomicileBD = financingUserDomicileRepo.findByFinancingIdAndActiveTrue(financingUserBD.getFinancingUserId());

	    if(financingUserDomicileBD != null){//si existe en la BD
		if(financingUserDomicileRequestDTO.getColony().equals(financingUserDomicileBD.getColony()) &&
			financingUserDomicileRequestDTO.getDelegation().equals(financingUserDomicileBD.getDelegation()) &&
			financingUserDomicileRequestDTO.getExternalNumber().equals(financingUserDomicileBD.getExternalNumber()) &&
			financingUserDomicileRequestDTO.getLivingType().equals(financingUserDomicileBD.getLivingType()) &&
			financingUserDomicileRequestDTO.getResidentYears().equals(financingUserDomicileBD.getResidentYears()) &&
			financingUserDomicileRequestDTO.getState().equals(financingUserDomicileBD.getState()) &&
			financingUserDomicileRequestDTO.getStreet().equals(financingUserDomicileBD.getStreet()) &&
			financingUserDomicileRequestDTO.getZipCode().equals(financingUserDomicileBD.getZipCode()) &&
			financingUserDomicileRequestDTO.getInternalNumber().equals(financingUserDomicileBD.getInternalNumber())) {

		    LogService.logger.info("FIN Servicio para update de domicile de un usuario de árbol financiero");
		    EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		    responseDTO.setCode(enumResultUpdate.getValue());
		    responseDTO.setStatus(enumResultUpdate.getStatus());
		    return responseDTO;

		}else {
		    //servicio de actualiar user domicile en arbol financiero
		    financingUserUpdateDomicileApiResponseDTO = postFinancingUserUpdateDomicileApi(idUserFinancing ,financingUserDomicileRequestDTO);

		    if(financingUserUpdateDomicileApiResponseDTO == null) {
			LogService.logger.info("Bad Request / No Found");
			EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResultUpdate.getValue());
			responseDTO.setStatus(enumResultUpdate.getStatus());
			return responseDTO;
		    }
		    financingUserDomicileBD.setActive(false);
		    financingUserDomicileRepo.save(financingUserDomicileBD);

		    FinancingUserDomicile newFinancingUserDomicile = new FinancingUserDomicile();
		    newFinancingUserDomicile.setUserId(financingUserBD.getUserId());
		    newFinancingUserDomicile.setColony(financingUserDomicileRequestDTO.getColony());
		    newFinancingUserDomicile.setDelegation(financingUserDomicileRequestDTO.getDelegation());
		    newFinancingUserDomicile.setExternalNumber(financingUserDomicileRequestDTO.getExternalNumber());
		    newFinancingUserDomicile.setLivingType(financingUserDomicileRequestDTO.getLivingType());
		    newFinancingUserDomicile.setResidentYears(financingUserDomicileRequestDTO.getResidentYears());
		    newFinancingUserDomicile.setState(financingUserDomicileRequestDTO.getState());
		    newFinancingUserDomicile.setStreet(financingUserDomicileRequestDTO.getStreet());
		    newFinancingUserDomicile.setZipCode(financingUserDomicileRequestDTO.getZipCode());
		    newFinancingUserDomicile.setInternalNumber(financingUserDomicileRequestDTO.getInternalNumber());
		    newFinancingUserDomicile.setActive(true);
		    newFinancingUserDomicile.setFinancingId(idUserFinancing.longValue());
		    financingUserDomicileRepo.save(newFinancingUserDomicile);
		}
	    }else {//No existe en la BD
		//servicio de actualiar user domicile en arbol financiero
		financingUserUpdateDomicileApiResponseDTO = postFinancingUserUpdateDomicileApi(idUserFinancing, financingUserDomicileRequestDTO);

		if(financingUserUpdateDomicileApiResponseDTO == null) {
		    LogService.logger.info("Bad Request / No Found");
		    EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		    responseDTO.setCode(enumResultUpdate.getValue());
		    responseDTO.setStatus(enumResultUpdate.getStatus());
		    return responseDTO;
		}
		FinancingUserDomicile newFinancingUserDomicile = new FinancingUserDomicile();
		newFinancingUserDomicile.setUserId(financingUserBD.getUserId());
		newFinancingUserDomicile.setFinancingId(idUserFinancing.longValue());
		newFinancingUserDomicile.setColony(financingUserDomicileRequestDTO.getColony());
		newFinancingUserDomicile.setDelegation(financingUserDomicileRequestDTO.getDelegation());
		newFinancingUserDomicile.setExternalNumber(financingUserDomicileRequestDTO.getExternalNumber());
		newFinancingUserDomicile.setLivingType(financingUserDomicileRequestDTO.getLivingType());
		newFinancingUserDomicile.setResidentYears(financingUserDomicileRequestDTO.getResidentYears());
		newFinancingUserDomicile.setState(financingUserDomicileRequestDTO.getState());
		newFinancingUserDomicile.setStreet(financingUserDomicileRequestDTO.getStreet());
		newFinancingUserDomicile.setZipCode(financingUserDomicileRequestDTO.getZipCode());
		newFinancingUserDomicile.setInternalNumber(financingUserDomicileRequestDTO.getInternalNumber());
		newFinancingUserDomicile.setActive(true);
		financingUserDomicileRepo.save(newFinancingUserDomicile);
	    }
	}else {//crear el message de  The financing_id not exist
	    //	    LogService.logger.info("The financing_id not exist"); 
	    //	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
	    //	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    //	    responseDTO.setCode(enumResult.getValue()); 
	    //	    listMessages.add(messageUserNotExist);
	    //	    responseDTO.setListMessage(listMessages);	    
	    //	    return responseDTO;

	    genericDTO.setMessage("The financing_id not exist in v2_financing_user"); // crear el mensaje en tabla de message
	    EndPointCodeResponseEnum enumResultt = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResultt.getValue());
	    responseDTO.setStatus(enumResultt.getStatus());
	    responseDTO.setData(genericDTO);
	    return responseDTO;
	}

	LogService.logger.info("FIN Servicio para update de domicile de un usuario de árbol financiero");
	EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResultUpdate.getValue());
	responseDTO.setStatus(enumResultUpdate.getStatus());
	return responseDTO;
    }



    /**
     * Servicio para actualizar el domicilio de un usuario en arbol financiero API
     * @author Juan Tolentino
     * @return postFinancingUserUpdateDomicileApi
     */
    public ResponseEntity<FinancingUserUpdateDomicileApiResponseDTO> postFinancingUserUpdateDomicileApi(Long idUserFinancing ,FinancingUserDomicileRequestDTO financingUserDomicileRequestDTO){
	FinancingUserUpdateDomicileApiAttributesDTO financingUserUpdateDomicileApiAttributesDTO = new FinancingUserUpdateDomicileApiAttributesDTO();
	FinancingUserUpdateDomicileApiDataDTO financingUserUpdateDomicileApiDataDTO = new FinancingUserUpdateDomicileApiDataDTO();
	ResponseEntity<FinancingUserUpdateDomicileApiResponseDTO> responseUserUpdateDomicileApi = null;
	LogService.logger.info("INICIO Servicio para update del domicilio de un usuario de árbol financiero");

	try {
	    //servicio de actualiar user domicile en arbol financiero 
	    ResponseDTO signatureUpdateDomicile = getFinancingSignature();
	    RestTemplate restTemplateUpdateDomicile = new RestTemplate();		
	    String urlUpdate = Constants.FINANCING_API_URL + "/api/v1/users/"+idUserFinancing+"/address";
	    HttpHeaders headersUpdateDomicile = new HttpHeaders();
	    headersUpdateDomicile.set("x-api-key", Constants.FINANCING_API_KEY);
	    headersUpdateDomicile.set("x-signature", signatureUpdateDomicile.getData().toString());
	    headersUpdateDomicile.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);

	    //variables para el request
	    financingUserUpdateDomicileApiAttributesDTO.setCityCouncil(financingUserDomicileRequestDTO.getDelegation());
	    financingUserUpdateDomicileApiAttributesDTO.setExternalNumber(financingUserDomicileRequestDTO.getExternalNumber());
	    financingUserUpdateDomicileApiAttributesDTO.setInternalNumber(financingUserDomicileRequestDTO.getInternalNumber());
	    financingUserUpdateDomicileApiAttributesDTO.setLivingType(financingUserDomicileRequestDTO.getLivingType());
	    financingUserUpdateDomicileApiAttributesDTO.setResidentYears(Long.valueOf(financingUserDomicileRequestDTO.getResidentYears()));
	    financingUserUpdateDomicileApiAttributesDTO.setState(financingUserDomicileRequestDTO.getState());
	    financingUserUpdateDomicileApiAttributesDTO.setStreet(financingUserDomicileRequestDTO.getStreet());
	    financingUserUpdateDomicileApiAttributesDTO.setSuburb(financingUserDomicileRequestDTO.getColony());
	    financingUserUpdateDomicileApiAttributesDTO.setZipCode(financingUserDomicileRequestDTO.getZipCode());
	    financingUserUpdateDomicileApiDataDTO.setType("addresses");
	    financingUserUpdateDomicileApiDataDTO.setAttributes(financingUserUpdateDomicileApiAttributesDTO);

	    // Map para armar el body de update
	    Map<String, Object> bodyUpdateDomicile = new HashMap<String, Object>();
	    bodyUpdateDomicile.put("data", financingUserUpdateDomicileApiDataDTO);

	    // Map para mandar el MapBody y el header de update
	    HttpEntity<Object> entityUpdateDomicile = new HttpEntity<Object>(bodyUpdateDomicile, headersUpdateDomicile);

	    HttpComponentsClientHttpRequestFactory requestFactory =  new  HttpComponentsClientHttpRequestFactory ();
	    restTemplateUpdateDomicile.setRequestFactory(requestFactory);

	    // llamada al servicio de arbol financiero
	    responseUserUpdateDomicileApi = restTemplateUpdateDomicile.exchange(urlUpdate, HttpMethod.PATCH, entityUpdateDomicile, FinancingUserUpdateDomicileApiResponseDTO.class);
	    LogService.logger.info("FIN Servicio para update del domicilio de un usuario de árbol financiero: "+ responseUserUpdateDomicileApi);

	    return responseUserUpdateDomicileApi;
	} catch (Exception e) {
	    e.printStackTrace();
	    LogService.logger.error("CATCH respuesta Servicio para update de domicile de un usuario de árbol financiero : "+e);	    
	    return responseUserUpdateDomicileApi;	
	}	
    }


    public ResponseDTO financingApiCodeResponse(String code){
	ResponseDTO responseDTO = new ResponseDTO();
	LogService.logger.info("INICIO financingApiCodeResponse");

	if(code.equals("404 Not Found")) {
	    LogService.logger.info("Usuario de arbol financiero no existe");
	    LogService.logger.info("FIN financingApiCodeResponse 1");
	    EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResultUpdate.getValue());
	    responseDTO.setStatus(enumResultUpdate.getStatus());
	    return responseDTO;
	}else if(code.equals("401 Unauthorized")) {
	    LogService.logger.info("Signature invalida");
	    LogService.logger.info("FIN financingApiCodeResponse 2");
	    EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0401.toString());
	    responseDTO.setCode(enumResultUpdate.getValue());
	    responseDTO.setStatus(enumResultUpdate.getStatus());
	    return responseDTO;
	}
	LogService.logger.info("FIN financingApiCodeResponse 3");
	return responseDTO;
    }



    /**
     * Servicio para actualizar o crear información de ingresos
     * @author Juan Tolentino
     * @return postFinancingUserIncome
     */
    public ResponseDTO postFinancingUserIncome(Long idUserFinancing , FinancingUserIncomeRequestDTO financingUserIncomeRequestDTO){
	ResponseDTO responseDTO = new ResponseDTO();
	ResponseEntity<FinancingUserUpdateIncomeApiResponseDTO> financingUserUpdateIncomeApiResponseDTO = null;
	LogService.logger.info("INICIO Servicio para update de domicile de un usuario de árbol financiero");

	FinancingUser financingUserBD = financingUserRepo.findByFinancingUserIdAndActiveTrue(idUserFinancing);

	if(financingUserBD != null) {
	    FinancingUserIncome financingUserIncomeBD = financingUserIncomeRepo.findByFinancingIdAndActiveTrue(financingUserBD.getFinancingUserId());

	    if(financingUserIncomeBD != null){//si existe en la BD
		if(financingUserIncomeRequestDTO.getAutomotiveCarUseType().equals(financingUserIncomeBD.getAutomotiveCarUseType()) &&
			financingUserIncomeRequestDTO.getAutomotiveDownpayment().equals(financingUserIncomeBD.getAutomotiveDownPayment().toString()) &&
			financingUserIncomeRequestDTO.getIncomeProfile().equals(financingUserIncomeBD.getIncomeProfile()) &&
			financingUserIncomeRequestDTO.getNetIncomeVerified().equals(financingUserIncomeBD.getNetIncomeVerified().toString())
			) {

		    LogService.logger.info("FIN Servicio para update de income de un usuario de árbol financiero");
		    EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		    responseDTO.setCode(enumResultUpdate.getValue());
		    responseDTO.setStatus(enumResultUpdate.getStatus());
		    return responseDTO;
		}else {
		    //servicio de actualiar user domicile en arbol financiero
		    financingUserUpdateIncomeApiResponseDTO = postFinancingUserUpdateIncomeApi(financingUserBD.getFinancingUserId(), financingUserIncomeRequestDTO);

		    if(financingUserUpdateIncomeApiResponseDTO == null) {
			LogService.logger.info("Bad Request / No Found");
			EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResultUpdate.getValue());
			responseDTO.setStatus(enumResultUpdate.getStatus());
			return responseDTO;
		    }
		    financingUserIncomeBD.setActive(false);
		    financingUserIncomeRepo.save(financingUserIncomeBD);

		    FinancingUserIncome newFinancingUserIncome = new FinancingUserIncome();
		    newFinancingUserIncome.setFinancingId(financingUserBD.getFinancingUserId());
		    if(financingUserIncomeRequestDTO.getAutomotiveCarPrice() != null) {
			newFinancingUserIncome.setAutomotiveCarPrice(Long.valueOf(financingUserIncomeRequestDTO.getAutomotiveCarPrice()));
		    }
		    newFinancingUserIncome.setAutomotiveCarUseType(financingUserIncomeRequestDTO.getAutomotiveCarUseType());
		    newFinancingUserIncome.setAutomotiveDownPayment(Long.valueOf(financingUserIncomeRequestDTO.getAutomotiveDownpayment()));
		    newFinancingUserIncome.setIncomeProfile(financingUserIncomeRequestDTO.getIncomeProfile());
		    newFinancingUserIncome.setNetIncomeVerified(Long.valueOf(financingUserIncomeRequestDTO.getNetIncomeVerified()));
		    newFinancingUserIncome.setActive(true);
		    newFinancingUserIncome.setPositionAge(Long.valueOf(financingUserIncomeRequestDTO.getPositionAge()));
		    financingUserIncomeRepo.save(newFinancingUserIncome);
		}
	    }else {//No existe en la BD
		//servicio de actualiar user domicile en arbol financiero
		financingUserUpdateIncomeApiResponseDTO = postFinancingUserUpdateIncomeApi(financingUserBD.getFinancingUserId(),financingUserIncomeRequestDTO);

		if(financingUserUpdateIncomeApiResponseDTO == null) {
		    LogService.logger.info("Bad Request / No Found");
		    EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		    responseDTO.setCode(enumResultUpdate.getValue());
		    responseDTO.setStatus(enumResultUpdate.getStatus());
		    return responseDTO;
		}
		FinancingUserIncome newFinancingUserIncome = new FinancingUserIncome();
		newFinancingUserIncome.setFinancingId(financingUserBD.getFinancingUserId());
		if(financingUserIncomeRequestDTO.getAutomotiveCarPrice() != null) {
			newFinancingUserIncome.setAutomotiveCarPrice(Long.valueOf(financingUserIncomeRequestDTO.getAutomotiveCarPrice()));
		}
		newFinancingUserIncome.setAutomotiveCarUseType(financingUserIncomeRequestDTO.getAutomotiveCarUseType());
		newFinancingUserIncome.setAutomotiveDownPayment(Long.valueOf(financingUserIncomeRequestDTO.getAutomotiveDownpayment()));
		newFinancingUserIncome.setIncomeProfile(financingUserIncomeRequestDTO.getIncomeProfile());
		newFinancingUserIncome.setNetIncomeVerified(Long.valueOf(financingUserIncomeRequestDTO.getNetIncomeVerified()));
		newFinancingUserIncome.setPositionAge(Long.valueOf(financingUserIncomeRequestDTO.getPositionAge()));
		newFinancingUserIncome.setActive(true);
		financingUserIncomeRepo.save(newFinancingUserIncome);

	    }
	}else {//crear el message de  The financing_id not exist
	    LogService.logger.info("The financing_id not exist"); 
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    listMessages.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessages);	    
	    return responseDTO;
	}

	LogService.logger.info("FIN Servicio para update de income de un usuario de árbol financiero");
	EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResultUpdate.getValue());
	responseDTO.setStatus(enumResultUpdate.getStatus());
	return responseDTO;
    }


    /**
     * Servicio para actualizar el income de un usuario en arbol financiero en el API
     * @author Juan Tolentino
     * @return postFinancingUserUpdateIncomeApi
     */
    public ResponseEntity<FinancingUserUpdateIncomeApiResponseDTO> postFinancingUserUpdateIncomeApi(Long financingId, FinancingUserIncomeRequestDTO financingUserIncomeRequestDTO){
	FinancingUserUpdateIncomeApiAttributesDTO financingUserUpdateIncomeApiAttributesDTO = new FinancingUserUpdateIncomeApiAttributesDTO();
	FinancingUserUpdateIncomeApiDataDTO financingUserUpdateIncomeApiDataDTO = new FinancingUserUpdateIncomeApiDataDTO();
	ResponseEntity<FinancingUserUpdateIncomeApiResponseDTO> responseUserUpdateIncomeApi = null;
	LogService.logger.info("INICIO Servicio para update del income de un usuario de árbol financiero");

	try {
	    //servicio de actualiar user income en arbol financiero
	    ResponseDTO signatureUpdateIncome = getFinancingSignature();
	    RestTemplate restTemplateUpdateIncome = new RestTemplate();		
	    String urlUpdate = Constants.FINANCING_API_URL + "/api/v1/users/"+financingId+"/financial_data";
	    HttpHeaders headersUpdateIncome = new HttpHeaders();
	    headersUpdateIncome.set("x-api-key", Constants.FINANCING_API_KEY);
	    headersUpdateIncome.set("x-signature", signatureUpdateIncome.getData().toString());
	    headersUpdateIncome.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);

	    //variables para el request
	    financingUserUpdateIncomeApiAttributesDTO.setIncomeProfile(financingUserIncomeRequestDTO.getIncomeProfile());
	    financingUserUpdateIncomeApiAttributesDTO.setNetIncomeVerified(financingUserIncomeRequestDTO.getNetIncomeVerified());
	    financingUserUpdateIncomeApiAttributesDTO.setPositionAge(financingUserIncomeRequestDTO.getPositionAge());
	    financingUserUpdateIncomeApiDataDTO.setType("financialData");
	    financingUserUpdateIncomeApiDataDTO.setAttributes(financingUserUpdateIncomeApiAttributesDTO);

	    // Map para armar el body de update
	    Map<String, Object> bodyUpdateIncome = new HashMap<String, Object>();
	    bodyUpdateIncome.put("data", financingUserUpdateIncomeApiDataDTO);

	    // Map para mandar el MapBody y el header de update
	    HttpEntity<Object> entityUpdateIncome = new HttpEntity<Object>(bodyUpdateIncome, headersUpdateIncome);

	    HttpComponentsClientHttpRequestFactory requestFactory =  new  HttpComponentsClientHttpRequestFactory ();
	    restTemplateUpdateIncome.setRequestFactory(requestFactory);

	    // llamada al servicio de arbol financiero
	    responseUserUpdateIncomeApi = restTemplateUpdateIncome.exchange(urlUpdate, HttpMethod.PATCH, entityUpdateIncome, FinancingUserUpdateIncomeApiResponseDTO.class);
	    LogService.logger.info("FIN Servicio para update del income de un usuario de árbol financiero: "+ responseUserUpdateIncomeApi);

	    return responseUserUpdateIncomeApi;
	} catch (Exception e) {
	    e.printStackTrace();
	    LogService.logger.error("CATCH respuesta Servicio para update de income de un usuario de árbol financiero : "+e);	    
	    return responseUserUpdateIncomeApi;	
	}	
    }



    /**
     * Servicio para Obtención código autorización consulta de buró
     * @author Juan Tolentino
     * @return postFinancingUserPinCode  
     */
    public ResponseDTO postFinancingUserPinCode(Long idUserFinancing){
	ResponseDTO responseDTO = new ResponseDTO();
	GenericDTO genericDTO = new GenericDTO();
	List<MessageDTO> listMessageDTO = new ArrayList<>();
	FinancingPostFinancingUserPinCodeResponseDTO financingPostFinancingUserPinCodeResponseDTO = new FinancingPostFinancingUserPinCodeResponseDTO();
	ResponseEntity<String> financingUserPinCodeApiDataResponseDTO = null;
	LogService.logger.info("INICIO Servicio para Obtención código autorización consulta de buró");
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	FinancingUser financingUserBD = financingUserRepo.findByFinancingUserIdAndActiveTrue(idUserFinancing);

	if(financingUserBD != null) {	
	    if(financingUserBD.getBuroDate() == null) {
		//consultar el servivio de arbol financiero API
		financingUserPinCodeApiDataResponseDTO = postFinancingBuroApi(idUserFinancing);

		if(financingUserPinCodeApiDataResponseDTO == null) {
		    genericDTO.setMessage("No se ha podido enviar el código de autorización por SMS, puede ser que haz solicitado códigos de verificación muy seguidos o el número del teléfono celular es incorrecto, por favor verifica el número de teléfono celular o espera unos minutos para solicitar un nuevo código.");
		    LogService.logger.info("No se ha podido enviar el código de autorización por SMS, por favor verifica que el número este correcto y sea un celular.");
		    EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		    responseDTO.setCode(enumResultUpdate.getValue());
		    responseDTO.setStatus(enumResultUpdate.getStatus());
		    responseDTO.setData(genericDTO);
		    return responseDTO;
		}

//		java.util.Date utilDate = new java.util.Date();
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(utilDate);
//		cal.set(Calendar.MILLISECOND, 0);	    
//		System.out.println(new java.sql.Timestamp(utilDate.getTime()));	    
//		financingUserBD.setBuroDate(new java.sql.Timestamp(utilDate.getTime()));//verificar el cast que da error
//		financingUserRepo.save(financingUserBD);
		
		financingPostFinancingUserPinCodeResponseDTO.setStatus("send");
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(financingPostFinancingUserPinCodeResponseDTO);
		return responseDTO;
	    }else {
		try {
		    String today = dateFormat.format(new Date());  
		    Date startDate=dateFormat.parse(financingUserBD.getBuroDate().toString());
		    Date endDate=dateFormat.parse(today);
		    int days=(int) ((endDate.getTime()-startDate.getTime())/86400000);

		    if(days >= 90) {
			//consultar el servivio de arbol financiero API 
			financingUserPinCodeApiDataResponseDTO = postFinancingBuroApi(idUserFinancing);

			if(financingUserPinCodeApiDataResponseDTO == null) {
			    genericDTO.setMessage("No se ha podido enviar el código de autorización por SMS, por favor verifica que el número este correcto y sea un celular.");
			    LogService.logger.info("No se ha podido enviar el código de autorización por SMS, por favor verifica que el número este correcto y sea un celular.");
			    EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			    responseDTO.setCode(enumResultUpdate.getValue());
			    responseDTO.setStatus(enumResultUpdate.getStatus());
			    responseDTO.setData(genericDTO);
			    return responseDTO;
			}

//			java.util.Date utilDate = new java.util.Date();
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(utilDate);
//			cal.set(Calendar.MILLISECOND, 0);	    
//			System.out.println(new java.sql.Timestamp(utilDate.getTime()));	    
//			financingUserBD.setBuroDate(new java.sql.Timestamp(utilDate.getTime()));//verificar el cast que da error
//			financingUserRepo.save(financingUserBD);

			financingPostFinancingUserPinCodeResponseDTO.setStatus("send");
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(financingPostFinancingUserPinCodeResponseDTO);
			LogService.logger.info("FIN Servicio para Obtención código autorización consulta de buró");
			return responseDTO;
		    }else {	
			LogService.logger.info("FIN Servicio para Obtención código autorización consulta de buró / Tiene menos de 3 meses");
			genericDTO.setValue("false");
			genericDTO.setMessage("El Buro se consulto hace menos de 3 meses");
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(genericDTO);
			return responseDTO;
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }

	}else {
	    LogService.logger.info("El usuario no existe en Base de datos..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
	    listMessageDTO.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessageDTO);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	}

	LogService.logger.info("FIN Servicio para Obtención código autorización consulta de buró");
	return responseDTO;
    }


    /**
     * Servicio para consultar el Buro
     * @author Juan Tolentino
     * @return postFinancingUserRegisterApi
     */
    public ResponseEntity<String> postFinancingBuroApi(Long financingId){
	ResponseEntity<String> financingBuroApiResponseDTO = null;
	LogService.logger.info("INICIO Servicio para consultar el Buro de un usuario de árbol financiero");

	try {
	    // Armo el header		
	    ResponseDTO signature = getFinancingSignature();
	    RestTemplate restTemplate = new RestTemplate();
	    String urlRegistry = Constants.FINANCING_API_URL + "/api/v1/users/"+financingId+"/credit_report/request_pin_code";
	    HttpHeaders headers = new HttpHeaders();
	    headers.set("x-api-key", Constants.FINANCING_API_KEY);
	    headers.set("x-signature", signature.getData().toString());
	    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

	    // Variables para el Request
	    FinancingUserPinCodeApiBodyRequestDTO financingUserPinCodeApiBodyRequestDTO = new FinancingUserPinCodeApiBodyRequestDTO();
	    financingUserPinCodeApiBodyRequestDTO.setStatus("null");

	    // Map para armar el body
	    Map<String, Object> body = new HashMap<String, Object>();
	    body.put("data", "null");
	    //
	    // Map para mandar el MapBody y el header
	    HttpEntity<Object> entity = new HttpEntity<Object>(body,headers);

	    LogService.logger.info("Body ="+body.toString());

	    // llamada al servicio de arbol financiero
	    financingBuroApiResponseDTO = restTemplate.exchange(urlRegistry, HttpMethod.POST, entity, String.class);

	    LogService.logger.info("FIN Servicio para consultar el Buro de un usuario de árbol financiero");

	    return financingBuroApiResponseDTO;

	} catch (Exception e) {
	    e.printStackTrace();
	    return financingBuroApiResponseDTO;
	}

    }

    /**
     * Servicio para autorización código consulta de buró
     * @author Juan Tolentino
     * @return postFinancingUserPinCode
     */
    public ResponseDTO postFinancingUserPinCodeAuthorization(Long financingId, PostFinancingUserPinCodeAuthorizationRequestDTO postFinancingUserPinCodeAuthorizationRequestDTO){
	ResponseDTO responseDTO = new ResponseDTO();
	GenericDTO genericDTO = new GenericDTO();
	FinancingPostFinancingUserPinCodeResponseDTO financingPostFinancingUserPinCodeResponseDTO = new FinancingPostFinancingUserPinCodeResponseDTO();
	LogService.logger.info("INICIO Servicio consulta de buró");
	List<MessageDTO> listMessageDTO = new ArrayList<>();
	FinancingUser financingUserBD = financingUserRepo.findByFinancingUserIdAndActiveTrue(financingId);

	if(financingUserBD != null) {
	    ResponseEntity<FinancingUserAuthorizationPinCodeApiBodyResponseDTO> responseApi = postFinancingUserPinCodeAuthorizationApi(financingId, postFinancingUserPinCodeAuthorizationRequestDTO);
	    if(responseApi != null) {
		
		String urlBuro = responseApi.getBody().getData().getAttributes().getUrl();
		financingUserBD.setUrlBuro(urlBuro);
		java.util.Date utilDate = new java.util.Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(utilDate);
		cal.set(Calendar.MILLISECOND, 0);	    
		System.out.println(new java.sql.Timestamp(utilDate.getTime()));	    
		financingUserBD.setBuroDate(new java.sql.Timestamp(utilDate.getTime()));
		financingUserRepo.save(financingUserBD);
		
		LogService.logger.info("FIN Servicio consulta de buró");
		financingPostFinancingUserPinCodeResponseDTO.setStatus("accepted");
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(financingPostFinancingUserPinCodeResponseDTO);
		return responseDTO;
	    }else {
		financingPostFinancingUserPinCodeResponseDTO.setStatus("El código de verificación reportado no coincide con el enviado a su teléfono celular, por favor revise su código de verificación");
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
		genericDTO.setMessage("El código de verificación reportado no coincide con el enviado a su teléfono celular, por favor revise su código de verificación");
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(genericDTO);
		return responseDTO;
	    }

	}else {
	    LogService.logger.info("El usuario no existe en Base de datos..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
	    listMessageDTO.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessageDTO);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	}
	return responseDTO;
    }



    /**
     * Servicio para autorización código consulta de buró
     * @author Juan Tolentino
     * @return postFinancingUserPinCode
     */
    public ResponseEntity<FinancingUserAuthorizationPinCodeApiBodyResponseDTO> postFinancingUserPinCodeAuthorizationApi(Long financingId, PostFinancingUserPinCodeAuthorizationRequestDTO postFinancingUserPinCodeAuthorizationRequestDTO){
	LogService.logger.info("INICIO Servicio para autorización consulta de buró");
	ResponseEntity<FinancingUserAuthorizationPinCodeApiBodyResponseDTO> financingBuroApiResponseDTO = null;
	FinancingUserPinCodeAuthorizationApiDataDTO financingUserPinCodeAuthorizationApiDataDTO = new FinancingUserPinCodeAuthorizationApiDataDTO();
	FinancingUserPinCodeAuthorizationApiAttributesDTO financingUserPinCodeAuthorizationApiAttributesDTO = new FinancingUserPinCodeAuthorizationApiAttributesDTO();
	try {
	    // Armo el header		
	    ResponseDTO signature = getFinancingSignature();
	    RestTemplate restTemplate = new RestTemplate();
	    String urlRegistry = Constants.FINANCING_API_URL + "/api/v1/users/"+financingId+"/credit_report/request_report";
	    HttpHeaders headers = new HttpHeaders();
	    headers.set("x-api-key", Constants.FINANCING_API_KEY);
	    headers.set("x-signature", signature.getData().toString());
	    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

	    // Variables para el Request
	    financingUserPinCodeAuthorizationApiDataDTO.setType("creditReports");
	    financingUserPinCodeAuthorizationApiAttributesDTO.setPinCode(postFinancingUserPinCodeAuthorizationRequestDTO.getPinCode());
	    financingUserPinCodeAuthorizationApiDataDTO.setAttributes(financingUserPinCodeAuthorizationApiAttributesDTO);

	    // Map para armar el body
	    Map<String, Object> body = new HashMap<String, Object>();
	    body.put("data", financingUserPinCodeAuthorizationApiDataDTO);
	    //
	    // Map para mandar el MapBody y el header
	    HttpEntity<Object> entity = new HttpEntity<Object>(body,headers);

	    // llamada al servicio de arbol financiero
	    financingBuroApiResponseDTO = restTemplate.exchange(urlRegistry, HttpMethod.POST, entity, FinancingUserAuthorizationPinCodeApiBodyResponseDTO.class);

	    LogService.logger.info("FIN Servicio para autorizacion Buro de un usuario de árbol financiero");

	    return financingBuroApiResponseDTO;

	} catch (Exception e) {
	    e.printStackTrace();
	    LogService.logger.info("CATCH Servicio para autorización consulta de buró");
	    return financingBuroApiResponseDTO;
	}
    }




    /**
     * Servicio para de preaprobación
     * @author Juan Tolentino
     * @return postFinancingUserEscore
     */
    public ResponseDTO postFinancingUserEscore(Long financingId, FinancingUserScoreRequestDTO financingUserScoreRequestDTO){
	ResponseDTO responseDTO = new ResponseDTO();
	GenericDTO genericDTO = new GenericDTO();
	LogService.logger.info("INICIO Servicio para consulta de Escore");
	List<MessageDTO> listMessageDTO = new ArrayList<>();
	//MetaValue metaValue = new MetaValue();
	FinancingUserEscoreResponseDTO financingUserEscoreResponseDTO = new FinancingUserEscoreResponseDTO();	
	FinancingUser financingUserBD = financingUserRepo.findByFinancingUserIdAndActiveTrue(financingId);
	String automotiveCarPrice = null;
	String automotiveCarModel = null;
	String automotiveCarManufacturer = null;
	String automotiveCarVersion = null;
	String automotiveCarVersionYear = null;
	SaleCheckpoint newSaleCheckpoint = new SaleCheckpoint();
	if(financingUserScoreRequestDTO.getCarId() != null) {
	    Optional<SellCarDetail> sellCarDetailBD = SellCarDetailRepo.findById(financingUserScoreRequestDTO.getCarId());
	    automotiveCarPrice = sellCarDetailBD.get().getPrice().toString();
	    automotiveCarModel = sellCarDetailBD.get().getCarModel();
	    if(sellCarDetailBD.get().getCarTrim() != null) {
		automotiveCarVersion = sellCarDetailBD.get().getCarTrim().split("##")[0];
	    }
	    automotiveCarVersionYear =  sellCarDetailBD.get().getCarYear();
	    automotiveCarManufacturer = sellCarDetailBD.get().getCarMake();

	    newSaleCheckpoint.setCarId(financingUserScoreRequestDTO.getCarId());	    
	    newSaleCheckpoint.setSku(sellCarDetailBD.get().getSku());
	    newSaleCheckpoint.setCarKm(Long.valueOf(sellCarDetailBD.get().getCarKm()));

	}else{
	    newSaleCheckpoint.setCarId(-1L);
	}
	if(financingUserBD != null) {
	    ResponseEntity<FinancingUserEscoreApiBodyResponseDTO> responseApi = postFinancingUserEscoreApi(financingId, automotiveCarPrice, automotiveCarModel, automotiveCarManufacturer , automotiveCarVersion, automotiveCarVersionYear);

	    if(responseApi != null) {
		//Lógica
		FinancingUserIncome incomeProfile = financingUserIncomeRepo.findByFinancingIdAndActiveTrue(financingId);
		if(incomeProfile != null) {
		    financingUserEscoreResponseDTO.setDownpayment(incomeProfile.getAutomotiveDownPayment().toString());
		    financingUserEscoreResponseDTO.setNetIncomeVerified(incomeProfile.getNetIncomeVerified().toString());
		}
		List<FinancingUserEscoreProfileResponseDTO> listFinancingUserEscoreProfileResponseDTO = new ArrayList<>();
		if(responseApi.getBody().getData().getAttributes().getScoreText().trim().equals("Bueno")) {
		    financingUserEscoreResponseDTO.setScoreText("AAA");
		}else if(responseApi.getBody().getData().getAttributes().getScoreText().trim().equals("Regular")) {
		    financingUserEscoreResponseDTO.setScoreText("AA");
		}else if(responseApi.getBody().getData().getAttributes().getScoreText().trim().equals("Malo")) {
		    financingUserEscoreResponseDTO.setScoreText("A");
		}
		
		if(responseApi.getBody().getData().getAttributes().getScore() != null) {
		    financingUserEscoreResponseDTO.setScore(responseApi.getBody().getData().getAttributes().getScore());
		}
		
		financingUserEscoreResponseDTO.setfinancingAmount(responseApi.getBody().getData().getAttributes().getPreapprovedAmount());
		financingUserEscoreResponseDTO.setMaximumFinancingAmount(responseApi.getBody().getData().getAttributes().getMaximumFinancingAmount());


		//Perfil Excelente
		FinancingUserEscoreProfileResponseDTO financingUserEscoreProfileEResponseDTO = new FinancingUserEscoreProfileResponseDTO();
		MetaValue metaValueE = new MetaValue();
		//id Excelente
		financingUserEscoreProfileEResponseDTO.setId("1");
		//Interest Rate Excelente
		metaValueE = metaValueRepo.findByAliasAndActiveTrue(Constants.FINANCING_META_VALUE_ALIAS_FSIRE.toString());
		financingUserEscoreProfileEResponseDTO.setInterestRate(metaValueE.getOptionName());
		//Downpayment Excelente
		metaValueE = new MetaValue();
		metaValueE = metaValueRepo.findByAliasAndActiveTrue(Constants.FINANCING_META_VALUE_ALIAS_FSDE.toString());
		financingUserEscoreProfileEResponseDTO.setDownpayment(metaValueE.getOptionName());
		//Name Excelente
		metaValueE = new MetaValue();
		metaValueE = metaValueRepo.findByAliasAndActiveTrue(Constants.FINANCING_META_VALUE_ALIAS_FSNE.toString());
		financingUserEscoreProfileEResponseDTO.setName(metaValueE.getOptionName());
		//Conten Excelente
		Optional<FinancingUserProfile> financingUserProfileE = financingUserProfileRepo.findById(1L);
		financingUserEscoreProfileEResponseDTO.setContent(financingUserProfileE.get().getContent());

		if(responseApi.getBody().getData().getAttributes().getScoreText().trim().equals("Bueno")) {
		    financingUserEscoreProfileEResponseDTO.setActive("true");
		}else {
		    financingUserEscoreProfileEResponseDTO.setActive("false");
		}


		listFinancingUserEscoreProfileResponseDTO.add(financingUserEscoreProfileEResponseDTO);


		//Perfil BUENO
		FinancingUserEscoreProfileResponseDTO financingUserEscoreProfileGResponseDTO = new FinancingUserEscoreProfileResponseDTO();
		MetaValue metaValueG = new MetaValue();
		//id Bueno
		financingUserEscoreProfileGResponseDTO.setId("2");
		//Interest Rate Bueno
		metaValueG = metaValueRepo.findByAliasAndActiveTrue(Constants.FINANCING_META_VALUE_ALIAS_FSIRB.toString());
		financingUserEscoreProfileGResponseDTO.setInterestRate(metaValueG.getOptionName());
		//Downpayment Bueno
		metaValueG = new MetaValue();
		metaValueG = metaValueRepo.findByAliasAndActiveTrue(Constants.FINANCING_META_VALUE_ALIAS_FSDB.toString());
		financingUserEscoreProfileGResponseDTO.setDownpayment(metaValueG.getOptionName());
		//Name Bueno
		metaValueG = new MetaValue();
		metaValueG = metaValueRepo.findByAliasAndActiveTrue(Constants.FINANCING_META_VALUE_ALIAS_FSNB.toString());
		financingUserEscoreProfileGResponseDTO.setName(metaValueG.getOptionName());
		//Conten Bueno
		Optional<FinancingUserProfile> financingUserProfileG = financingUserProfileRepo.findById(2L);
		financingUserEscoreProfileGResponseDTO.setContent(financingUserProfileG.get().getContent());

		if(responseApi.getBody().getData().getAttributes().getScoreText().trim().equals("Regular")) {
		    financingUserEscoreProfileGResponseDTO.setActive("true");
		}else {
		    financingUserEscoreProfileGResponseDTO.setActive("false");
		}

		listFinancingUserEscoreProfileResponseDTO.add(financingUserEscoreProfileGResponseDTO);

		//Perfil Malo
		FinancingUserEscoreProfileResponseDTO financingUserEscoreProfileBResponseDTO = new FinancingUserEscoreProfileResponseDTO();
		MetaValue metaValueB = new MetaValue();
		//id Malo
		financingUserEscoreProfileBResponseDTO.setId("3");
		//Interest Rate Malo
		metaValueB = metaValueRepo.findByAliasAndActiveTrue(Constants.FINANCING_META_VALUE_ALIAS_FSIRM.toString());
		financingUserEscoreProfileBResponseDTO.setInterestRate(metaValueB.getOptionName());
		//Downpayment Malo
		metaValueB = new MetaValue();
		metaValueB = metaValueRepo.findByAliasAndActiveTrue(Constants.FINANCING_META_VALUE_ALIAS_FSDM.toString());
		financingUserEscoreProfileBResponseDTO.setDownpayment(metaValueB.getOptionName());
		//Name Malo
		metaValueB = new MetaValue();
		metaValueB = metaValueRepo.findByAliasAndActiveTrue(Constants.FINANCING_META_VALUE_ALIAS_FSNM.toString());
		financingUserEscoreProfileBResponseDTO.setName(metaValueB.getOptionName());
		//Conten Malo
		Optional<FinancingUserProfile> financingUserProfileB = financingUserProfileRepo.findById(3L);
		financingUserEscoreProfileBResponseDTO.setContent(financingUserProfileB.get().getContent());

		if(responseApi.getBody().getData().getAttributes().getScoreText().trim().equals("Malo")) {
		    financingUserEscoreProfileBResponseDTO.setActive("true");
		}else {
		    financingUserEscoreProfileBResponseDTO.setActive("false");
		}

		listFinancingUserEscoreProfileResponseDTO.add(financingUserEscoreProfileBResponseDTO);


		financingUserEscoreResponseDTO.setListFinancingUserEscoreProfileResponseDTO(listFinancingUserEscoreProfileResponseDTO);


		//Guardando en la tabla v2_financing_user_score
		FinancingUserScore newFinancingUserScore = new FinancingUserScore();
		
		newFinancingUserScore.setFinancingUserId(financingId);
		newFinancingUserScore.setFinancingUserScoreId(responseApi.getBody().getData().getId());
		newFinancingUserScore.setType(responseApi.getBody().getData().getType());
		newFinancingUserScore.setCode(responseApi.getBody().getData().getAttributes().getCode());
		newFinancingUserScore.setScore(responseApi.getBody().getData().getAttributes().getScore());
		newFinancingUserScore.setScoreText(responseApi.getBody().getData().getAttributes().getScoreText());
		newFinancingUserScore.setVerticalType(responseApi.getBody().getData().getAttributes().getVerticalType());
		newFinancingUserScore.setMaximumFinancingAmount(responseApi.getBody().getData().getAttributes().getMaximumFinancingAmount());
		newFinancingUserScore.setPreapprovedAmount(responseApi.getBody().getData().getAttributes().getPreapprovedAmount());
		newFinancingUserScore.setFinanciersCount(responseApi.getBody().getData().getAttributes().getFinanciersCount().toString());
		newFinancingUserScore.setMinInterestRate(responseApi.getBody().getData().getAttributes().getMinInterestRate());
		newFinancingUserScore.setMaxInterestRate(responseApi.getBody().getData().getAttributes().getMaxInterestRate());
		newFinancingUserScore.setMonthlyPayment(responseApi.getBody().getData().getAttributes().getMonthlyPayment());
		newFinancingUserScore.setNextUrl(responseApi.getBody().getData().getAttributes().getNextUrl());
		newFinancingUserScore.setUserFullName(responseApi.getBody().getData().getAttributes().getUserFullName());
		if(financingUserScoreRequestDTO.getCarId() != null) {
		    newFinancingUserScore.setCarId(financingUserScoreRequestDTO.getCarId());
		}
		financingUserScoreRepo.save(newFinancingUserScore);
		
		//Guardando en la tabla compra_checkpoint
		Optional<User> userBD = userRepo.findById(financingUserBD.getUserId());
		MetaValue metaValueOffertTypeBD = metaValueRepo.findByAlias(Constants.FINANCING_META_VALUE_ALIAS_OTF);
		MetaValue metaValueStatusBD = metaValueRepo.findByAlias(Constants.FINANCING_META_VALUE_ALIAS_CPRSF);

		newSaleCheckpoint.setUser(userBD.get());
		String[] fragment = userBD.get().getName().split(" ");
		
		if(fragment.length > 1){
		    newSaleCheckpoint.setCustomerLastName(WordUtils.capitalize(fragment[1]));
		}
		newSaleCheckpoint.setEmail(userBD.get().getEmail()); 
		newSaleCheckpoint.setCustomerName(WordUtils.capitalize(fragment[0]));
		newSaleCheckpoint.setSaleOpportunityTypeId(metaValueOffertTypeBD.getId());
		newSaleCheckpoint.setStatus(metaValueStatusBD.getId());

		newSaleCheckpoint.setSentNetsuite(0L);
		newSaleCheckpoint.setFinancingId(financingUserBD.getFinancingUserId());
		
		//Especificaciones del auto
		if(automotiveCarPrice != null) {
		    newSaleCheckpoint.setCarCost(automotiveCarPrice);
		}
		if(automotiveCarModel != null) {
		    newSaleCheckpoint.setCarModel(automotiveCarModel);
		}
		if(automotiveCarVersion != null) {
		    newSaleCheckpoint.setCarVersion(automotiveCarVersion);
		}
		if(automotiveCarVersionYear != null) {
		    newSaleCheckpoint.setCarYear(Long.valueOf(automotiveCarVersionYear));
		}
		if(automotiveCarManufacturer != null) {
		    newSaleCheckpoint.setCarMake(automotiveCarManufacturer);
		}
		saleCheckpointRepo.save(newSaleCheckpoint);
		LogService.logger.info("Guardo la oportunidad de arbol financiero en compra_checkpoint");
		
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(financingUserEscoreResponseDTO);
		LogService.logger.info("FIN Servicio para consulta de Escore");
		return responseDTO;
	    }else {
		//financingPostFinancingUserPinCodeResponseDTO.setStatus("not accepted");
		genericDTO.setMessage("El código de verificación reportado no coincide con el enviado a su teléfono celular, por favor revise su código de verificación");
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(genericDTO);
		return responseDTO;
	    }

	}else {
	    LogService.logger.info("El usuario no existe en Base de datos..");
	    MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
	    listMessageDTO.add(messageUserNotExist);
	    responseDTO.setListMessage(listMessageDTO);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	}
	return responseDTO;
    }



    /**
     * Servicio para escore de arbol financiero api
     * @author Juan Tolentino
     * @return postFinancingUserPinCode
     */
    public ResponseEntity<FinancingUserEscoreApiBodyResponseDTO> postFinancingUserEscoreApi(Long financingId, String automotiveCarPrice, String automotiveCarModel, String automotiveCarManufacturer, String automotiveCarVersion, String automotiveCarVersionYear){
	LogService.logger.info("INICIO Servicio para el Escore consulta de buró");
	ResponseEntity<FinancingUserEscoreApiBodyResponseDTO> financingUserEscoreApiDataResponseDTO = null;
	FinancingUserEscoreApiDataRequestDTO financingUserEscoreApiDataRequestDTO = new FinancingUserEscoreApiDataRequestDTO();
	FinancingUserEscoreApiAttributesRequestDTO financingUserEscoreApiAttributesRequestDTO = new FinancingUserEscoreApiAttributesRequestDTO();
	try {
	    // Armo el header		
	    ResponseDTO signature = getFinancingSignature();
	    RestTemplate restTemplate = new RestTemplate();
	    String urlRegistry = Constants.FINANCING_API_URL + "/api/v1/users/"+financingId+"/scores";
	    HttpHeaders headers = new HttpHeaders();
	    headers.set("x-api-key", Constants.FINANCING_API_KEY);
	    headers.set("x-signature", signature.getData().toString());
	    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

	    // Variables para el Request
	    financingUserEscoreApiAttributesRequestDTO.setCurrentApplyingVertical("automotive");
	    if(automotiveCarPrice != null) {
		FinancingUserIncome financingUserIncomeBD = financingUserIncomeRepo.findByFinancingIdAndActiveTrue(financingId);
		financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarPrice(automotiveCarPrice);
		if(financingUserIncomeBD != null) {
		    financingUserEscoreApiAttributesRequestDTO.setAutomotiveDownPayment(financingUserIncomeBD.getAutomotiveDownPayment().toString());  
		    financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarUseType(financingUserIncomeBD.getAutomotiveCarUseType());
		}		
		financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarManufacturer(automotiveCarManufacturer);
		financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarModel(automotiveCarModel);
		financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarVersion(automotiveCarVersion);
		financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarVersionYear(automotiveCarVersionYear);
	    }
	    //	    financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarPrice("230000");
	    //	    financingUserEscoreApiAttributesRequestDTO.setAutomotiveDownPayment("50000");
	    //	    financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarManufacturer("Toyota");
	    //	    financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarModel("Prius");
	    //	    financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarVersion("XE");
//	    financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarVersionYear("2015");
//	    financingUserEscoreApiAttributesRequestDTO.setAutomotiveCarUseType("for_personal_use");
	    financingUserEscoreApiDataRequestDTO.setAttributes(financingUserEscoreApiAttributesRequestDTO);
	    financingUserEscoreApiDataRequestDTO.setType("scores");


	    // Map para armar el body
	    Map<String, Object> body = new HashMap<String, Object>();
	    body.put("data", financingUserEscoreApiDataRequestDTO);
	    //
	    // Map para mandar el MapBody y el header
	    HttpEntity<Object> entity = new HttpEntity<Object>(body,headers);

	    // llamada al servicio de arbol financiero
	    financingUserEscoreApiDataResponseDTO = restTemplate.exchange(urlRegistry, HttpMethod.POST, entity, FinancingUserEscoreApiBodyResponseDTO.class);

	    LogService.logger.info("FIN Servicio para consultar el Escore de un usuario de árbol financiero");

	    return financingUserEscoreApiDataResponseDTO;

	} catch (Exception e) {
	    e.printStackTrace();
	    LogService.logger.info("CATCH Servicio para el Escore consulta de buró");
	    return financingUserEscoreApiDataResponseDTO;
	}
    }




    /**
     * Servicio para verificar información de un usuario de árbol financiero
     * @author Juan Tolentino
     * @return getFinancingCarUseType
     */
    public ResponseDTO postFinancingVerifyUserInformation(FinancingVerifyUserInformationRequestDTO financingVerifyUserInformationRequestDTO){
	ResponseDTO responseDTO = new ResponseDTO();
	ResponseEntity<FinancingVerifyUserInformationRegisterApiResponseDTO> financingUserRegisterApiResponseDTO = null;
	GenericDTO genericDTO = new GenericDTO();
	FinancingVerifyUserInformationResponseDTO financingVerifyUserInformationResponseDTO = new FinancingVerifyUserInformationResponseDTO();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	LogService.logger.info("Iniciando Servicio para verificar información de un usuario de árbol financiero");	
	boolean emailExist = false;
	boolean register = false;
	FinancingUser financingUserBD = new FinancingUser();
	MetaValue metaValueBD = metaValueRepo.findByAlias(Constants.FINANCING_MAXIMUM_VERIFY_USER_INFORMATION);
	List<FinancingUser> listFinancingUserBD = financingUserRepo.findByUserIdOrderBycreationDateDescAndActiveTrue(financingVerifyUserInformationRequestDTO.getUserId());


	if(!listFinancingUserBD.isEmpty()) {
	    for(FinancingUser FinancingUserActual : listFinancingUserBD) {
		if(FinancingUserActual.getEmail().trim().equals(financingVerifyUserInformationRequestDTO.getEmail().trim())) {
		    emailExist = true;
		    financingUserBD = FinancingUserActual;
		}
	    }
	    if(emailExist) {
		//actualizo v2 y actualizo arbol financiero
//		if(financingUserBD.getPhone().equals(financingVerifyUserInformationRequestDTO.getPhone())) {
//		    genericDTO.setValue("false");
//		    //genericDTO.setMessage("Tus datos de contactos estan asociados a otro RFC "+financingVerifyUserInformationRequestDTO.getRfc()+" sugerimos cambiar correo y telefono para continuar"); // crear el mensaje en tabla de message
//		    genericDTO.setMessage("El teléfono asociado "+financingVerifyUserInformationRequestDTO.getPhone()+" al RFC "+financingVerifyUserInformationRequestDTO.getRfc()+" es diferente al perfilamiento anterior"); // crear el mensaje en tabla de message
//		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
//		    responseDTO.setCode(enumResult.getValue());
//		    responseDTO.setStatus(enumResult.getStatus());
//		    responseDTO.setData(genericDTO);
//		    return responseDTO;
//		}
//
//		if(financingUserBD.getRfc().equals(financingVerifyUserInformationRequestDTO.getRfc())) {
//		    genericDTO.setValue("false");
//		    genericDTO.setMessage("Tus datos de contactos estan asociados a otro RFC "+financingVerifyUserInformationRequestDTO.getRfc()+" sugerimos cambiar correo y telefono para continuar"); // crear el mensaje en tabla de message
//		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
//		    responseDTO.setCode(enumResult.getValue());
//		    responseDTO.setStatus(enumResult.getStatus());
//		    responseDTO.setData(genericDTO);
//		    return responseDTO;
//		}
//
//		if(!financingUserBD.getEmail().equals(financingVerifyUserInformationRequestDTO.getEmail())) {
//		    genericDTO.setValue("false");
//		    //genericDTO.setMessage("Tus datos de contactos estan asociados a otro RFC "+financingVerifyUserInformationRequestDTO.getRfc()+" sugerimos cambiar correo y telefono para continuar"); // crear el mensaje en tabla de message
//		    genericDTO.setMessage(" El email asociado "+financingVerifyUserInformationRequestDTO.getEmail()+" al RFC "+financingVerifyUserInformationRequestDTO.getRfc()+" es diferente al perfilamiento anterior"); // crear el mensaje en tabla de message
//		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
//		    responseDTO.setCode(enumResult.getValue());
//		    responseDTO.setStatus(enumResult.getStatus());
//		    responseDTO.setData(genericDTO);
//		    return responseDTO;
//		}

		//Optional<FinancingUser> financingUserBD = financingUserRepo.findById(financingUserBD.get().getId());

		FinancingUser newFinancingUserUpdateBD = new FinancingUser();

		if(!financingUserBD.getFullName().equals(financingVerifyUserInformationRequestDTO.getFullName()) 
			|| !financingUserBD.getFirstName().equals(financingVerifyUserInformationRequestDTO.getFirstSurname()) 
			|| !financingUserBD.getSecondName().equals(financingVerifyUserInformationRequestDTO.getSecondSurname())
			|| !financingUserBD.getGender().equals(financingVerifyUserInformationRequestDTO.getGender())
			|| !financingUserBD.getPhone().equals(financingVerifyUserInformationRequestDTO.getPhone())
			|| !financingUserBD.getRfc().equals(financingVerifyUserInformationRequestDTO.getRfc())) {

		    //Actualizar usuario en arbol financiero
		    ResponseEntity<FinancingVerifyUserUpdatePersonalDataApiResponseDTO> financingUserUpdateApiResponse = postFinancingUserUpdateApi(financingUserBD.getFinancingUserId().longValue(), financingVerifyUserInformationRequestDTO);
		    if(financingUserUpdateApiResponse == null) {
			LogService.logger.info("Bad Request / No Actualizo");
			EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResultUpdate.getValue());
			responseDTO.setStatus(enumResultUpdate.getStatus());
			return responseDTO;
		    }

		    //Actualizando la tabla v2_financing_user 
		    financingUserBD.setFullName(financingVerifyUserInformationRequestDTO.getFullName());
		    financingUserBD.setFirstName(financingVerifyUserInformationRequestDTO.getFirstSurname());
		    financingUserBD.setSecondName(financingVerifyUserInformationRequestDTO.getSecondSurname());
		    financingUserBD.setGender(financingVerifyUserInformationRequestDTO.getGender());
		    financingUserBD.setPhone(financingVerifyUserInformationRequestDTO.getPhone());
		    financingUserBD.setRfc(financingVerifyUserInformationRequestDTO.getRfc());

		    newFinancingUserUpdateBD = financingUserRepo.save(financingUserBD);

		    //llenado parcial
		    financingVerifyUserInformationResponseDTO.setIdFinancing(newFinancingUserUpdateBD.getFinancingUserId().toString());
		    financingVerifyUserInformationResponseDTO.setEmail(newFinancingUserUpdateBD.getEmail());
		    financingVerifyUserInformationResponseDTO.setAuthenticationToken(newFinancingUserUpdateBD.getAuthenticationToken());
		    financingVerifyUserInformationResponseDTO.setFullName(newFinancingUserUpdateBD.getFullName());
		    financingVerifyUserInformationResponseDTO.setFirstSurname(newFinancingUserUpdateBD.getFirstName());//cambiar a firstSurname
		    financingVerifyUserInformationResponseDTO.setSecondSurname(newFinancingUserUpdateBD.getSecondName());//cambiar a secondSurname
		    if(financingUserBD.getGender().equals("M")) {
			FinancingGenericDTO genericGenderDTO = new FinancingGenericDTO();
			genericGenderDTO.setId("M");
			genericGenderDTO.setName("HOMBRE");
			financingVerifyUserInformationResponseDTO.setGender(genericGenderDTO);
		    }else {
			FinancingGenericDTO genericGenderDTO = new FinancingGenericDTO();
			genericGenderDTO.setId("W");
			genericGenderDTO.setName("MUJER");
			financingVerifyUserInformationResponseDTO.setGender(genericGenderDTO);
		    }
		    financingVerifyUserInformationResponseDTO.setBirthday(newFinancingUserUpdateBD.getBirthday().toString());
		    financingVerifyUserInformationResponseDTO.setPhone(newFinancingUserUpdateBD.getPhone());
		    financingVerifyUserInformationResponseDTO.setRfc(newFinancingUserUpdateBD.getRfc());

		    try {
			financingVerifyUserInformationResponseDTO.setBirthday(newFinancingUserUpdateBD.getBirthday().toString());
		    } catch (Exception e) {
			e.printStackTrace();
		    }

		}else {
		    //llenado parcial
		    financingVerifyUserInformationResponseDTO.setIdFinancing(financingUserBD.getFinancingUserId().toString());
		    financingVerifyUserInformationResponseDTO.setEmail(financingUserBD.getEmail());
		    financingVerifyUserInformationResponseDTO.setAuthenticationToken(financingUserBD.getAuthenticationToken());
		    financingVerifyUserInformationResponseDTO.setFullName(financingUserBD.getFullName());
		    financingVerifyUserInformationResponseDTO.setFirstSurname(financingUserBD.getFirstName());//cambiar a firstSurname
		    financingVerifyUserInformationResponseDTO.setSecondSurname(financingUserBD.getSecondName());//cambiar a secondSurname
		    if(financingUserBD.getGender().equals("M")) {
			FinancingGenericDTO genericGenderDTO = new FinancingGenericDTO();
			genericGenderDTO.setId("M");
			genericGenderDTO.setName("HOMBRE");
			financingVerifyUserInformationResponseDTO.setGender(genericGenderDTO);
		    }else {
			FinancingGenericDTO genericGenderDTO = new FinancingGenericDTO();
			genericGenderDTO.setId("W");
			genericGenderDTO.setName("MUJER");
			financingVerifyUserInformationResponseDTO.setGender(genericGenderDTO);
		    }

		    financingVerifyUserInformationResponseDTO.setPhone(financingUserBD.getPhone());
		    financingVerifyUserInformationResponseDTO.setRfc(financingUserBD.getRfc());

		    try {
			financingVerifyUserInformationResponseDTO.setBirthday(financingUserBD.getBirthday().toString());
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		}


		try {
		    if(financingUserBD.getBuroDate() != null) {
			String today = dateFormat.format(new Date());  
			Date startDate=dateFormat.parse(financingUserBD.getBuroDate().toString());
			Date endDate=dateFormat.parse(today);
			int dates=(int) ((endDate.getTime()-startDate.getTime())/86400000);

			if(financingUserBD.getBuroDate() == null || dates >= 90 ) {
			    financingVerifyUserInformationResponseDTO.setShowBuro("true");
			}else {
			    financingVerifyUserInformationResponseDTO.setShowBuro("false");
			}
		    }else {
			financingVerifyUserInformationResponseDTO.setShowBuro("true");
		    }

		} catch (Exception e) {
		    e.printStackTrace();
		}

		FinancingUserDomicile FinancingUserDomicileBD = financingUserDomicileRepo.findByFinancingIdAndActiveTrue(financingUserBD.getFinancingUserId());
		if(FinancingUserDomicileBD != null) {

		    //llenado completo
		    financingVerifyUserInformationResponseDTO.setStreet(FinancingUserDomicileBD.getStreet());
		    financingVerifyUserInformationResponseDTO.setExternalNumber(FinancingUserDomicileBD.getExternalNumber());
		    financingVerifyUserInformationResponseDTO.setInternalNumber(FinancingUserDomicileBD.getInternalNumber());
		    financingVerifyUserInformationResponseDTO.setColony(FinancingUserDomicileBD.getColony());
		    financingVerifyUserInformationResponseDTO.setDelegation(FinancingUserDomicileBD.getDelegation());
		    financingVerifyUserInformationResponseDTO.setState(FinancingUserDomicileBD.getState());
		    financingVerifyUserInformationResponseDTO.setZipCode(FinancingUserDomicileBD.getZipCode());
		    financingVerifyUserInformationResponseDTO.setResidentYears(FinancingUserDomicileBD.getResidentYears());
		    FormOptionValue formOptionValueLivingTypeBD = formOptionValueRepo.findByValue(FinancingUserDomicileBD.getLivingType().toString().trim());
		    if(formOptionValueLivingTypeBD != null) {
			FinancingGenericDTO financingGenericLivingTypeDTO = new FinancingGenericDTO();
			financingGenericLivingTypeDTO.setId(formOptionValueLivingTypeBD.getValue());
			financingGenericLivingTypeDTO.setName(formOptionValueLivingTypeBD.getName());
			financingVerifyUserInformationResponseDTO.setLivingType(financingGenericLivingTypeDTO);
		    }

		}

		FinancingUserIncome financingUserIncomeBD = financingUserIncomeRepo.findByFinancingIdAndActiveTrue(financingUserBD.getFinancingUserId());

		if(financingUserIncomeBD != null) {
		    FormOptionValue formOptionValueIncomeProfileBD = formOptionValueRepo.findByValue(financingUserIncomeBD.getIncomeProfile().toString().trim());
		    FormOptionValue formOptionValueAutomotiveCarUseTypeBD = formOptionValueRepo.findByValue(financingUserIncomeBD.getAutomotiveCarUseType().toString().trim());
		    financingVerifyUserInformationResponseDTO.setNetIncomeVerified(financingUserIncomeBD.getNetIncomeVerified().toString());
		    if(formOptionValueIncomeProfileBD != null) {
			FinancingGenericDTO financingGenericIncomeProfileDTO = new FinancingGenericDTO();
			financingGenericIncomeProfileDTO.setId(formOptionValueIncomeProfileBD.getValue());
			financingGenericIncomeProfileDTO.setName(formOptionValueIncomeProfileBD.getName());
			financingVerifyUserInformationResponseDTO.setIncomeProfile(financingGenericIncomeProfileDTO);
		    }
		    if(formOptionValueAutomotiveCarUseTypeBD != null) {
			FinancingGenericDTO financingGenericAutomotiveCarUseTypeDTO = new FinancingGenericDTO();
			financingGenericAutomotiveCarUseTypeDTO.setId(formOptionValueAutomotiveCarUseTypeBD.getValue());
			financingGenericAutomotiveCarUseTypeDTO.setName(formOptionValueAutomotiveCarUseTypeBD.getName());
			financingVerifyUserInformationResponseDTO.setAutomotiveCarUseType(financingGenericAutomotiveCarUseTypeDTO);
		    }
		    financingVerifyUserInformationResponseDTO.setPositionAge(financingUserIncomeBD.getPositionAge().toString());
		}

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(financingVerifyUserInformationResponseDTO);
		return responseDTO;

	    }else if(listFinancingUserBD.size() <= Long.valueOf(metaValueBD.getOptionName())){
		register = true;
		//registro en v2 y registro y actualizo en arbol	

	    }else {
		//mensaje de que se paso de perfiles
		genericDTO.setValue("false");
		genericDTO.setMessage("Ya supero el límite de pérfiles permitidos"); // crear el mensaje en tabla de message
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(genericDTO);
		return responseDTO;
	    }
	}
	if(!listFinancingUserBD.isEmpty() && register || listFinancingUserBD.isEmpty()) {

	    List<FinancingUser> financingUserPhone  = financingUserRepo.findByPhone(financingVerifyUserInformationRequestDTO.getPhone());
	    List<FinancingUser> financingUserEmail  = financingUserRepo.findByEmails(financingVerifyUserInformationRequestDTO.getEmail());
	    List<FinancingUser> financingUserRfc  = financingUserRepo.findByRfc(financingVerifyUserInformationRequestDTO.getRfc());

	    if(!financingUserEmail.isEmpty()) {
		genericDTO.setValue("false");
		genericDTO.setMessage("el email "+financingVerifyUserInformationRequestDTO.getEmail()+" ya lo tomo otra persona"); // crear el mensaje en tabla de message
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(genericDTO);
		return responseDTO;
	    }

	    if(!financingUserPhone.isEmpty()) {
		genericDTO.setValue("false");
		genericDTO.setMessage("el teléfono "+financingVerifyUserInformationRequestDTO.getPhone()+" ya lo tomo otra persona"); // crear el mensaje en tabla de message
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(genericDTO);
		return responseDTO;
	    }
	    
	    if(!financingUserRfc.isEmpty()) {
		genericDTO.setValue("false");
		genericDTO.setMessage("el Rfc "+financingVerifyUserInformationRequestDTO.getRfc()+" ya lo tomo otra persona"); // crear el mensaje en tabla de message
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(genericDTO);
		return responseDTO;
	    }


	    //reigstro en v2 y en arbol
	    //Registrar usuario en arbol financiero
	    financingUserRegisterApiResponseDTO = postFinancingUserRegisterApi(financingVerifyUserInformationRequestDTO);
	    if(financingUserRegisterApiResponseDTO == null) {
		LogService.logger.info("email ya existe en Arbol financiero");
		genericDTO.setMessage("El e-mail ya esta asociado a otro usuario, sugerimos cambiar e-mail para continuar");
		EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
		responseDTO.setCode(enumResultUpdate.getValue());
		responseDTO.setStatus(enumResultUpdate.getStatus());
		responseDTO.setData(genericDTO);
		return responseDTO;
	    }

	    //llenar la tabla v2_financing_user 
	    FinancingUser newFinancingUser = new FinancingUser();
	    newFinancingUser.setFinancingUserId(Long.valueOf(financingUserRegisterApiResponseDTO.getBody().getData().getIdFinancing()));
	    newFinancingUser.setAuthenticationToken(financingUserRegisterApiResponseDTO.getBody().getData().getAttributes().getAuthenticationToken().toString());
	    newFinancingUser.setUserId(financingVerifyUserInformationRequestDTO.getUserId());
	    newFinancingUser.setEmail(financingVerifyUserInformationRequestDTO.getEmail());
	    newFinancingUser.setFullName(financingVerifyUserInformationRequestDTO.getFullName());
	    newFinancingUser.setFirstName(financingVerifyUserInformationRequestDTO.getFirstSurname());//cambiar a firstSurname
	    newFinancingUser.setSecondName(financingVerifyUserInformationRequestDTO.getSecondSurname());//cambiar a secondSurname
	    newFinancingUser.setGender(financingVerifyUserInformationRequestDTO.getGender());
	    newFinancingUser.setBirthday(financingVerifyUserInformationRequestDTO.getBirthday());
	    newFinancingUser.setPhone(financingVerifyUserInformationRequestDTO.getPhone());
	    newFinancingUser.setRfc(financingVerifyUserInformationRequestDTO.getRfc());
	    newFinancingUser.setActive(true);

	    financingUserRepo.save(newFinancingUser);

	    //Actualizar usuario en arbol financiero
	    ResponseEntity<FinancingVerifyUserUpdatePersonalDataApiResponseDTO> financingUserUpdateApiResponse = postFinancingUserUpdateApi(Long.valueOf(financingUserRegisterApiResponseDTO.getBody().getData().getIdFinancing()), financingVerifyUserInformationRequestDTO);

	    if(financingUserUpdateApiResponse == null) {
		LogService.logger.info("Bad Request / No Actualizo");
		EndPointCodeResponseEnum enumResultUpdate = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		responseDTO.setCode(enumResultUpdate.getValue());
		responseDTO.setStatus(enumResultUpdate.getStatus());
		return responseDTO;
	    }

	    //llenando el DTO de respuesta
	    financingVerifyUserInformationResponseDTO.setIdFinancing(financingUserRegisterApiResponseDTO.getBody().getData().getIdFinancing().toString());
	    financingVerifyUserInformationResponseDTO.setAuthenticationToken(financingUserRegisterApiResponseDTO.getBody().getData().getAttributes().getAuthenticationToken().toString());
	    financingVerifyUserInformationResponseDTO.setEmail(financingVerifyUserInformationRequestDTO.getEmail());
	    financingVerifyUserInformationResponseDTO.setFullName(financingVerifyUserInformationRequestDTO.getFullName());
	    financingVerifyUserInformationResponseDTO.setFirstSurname(financingVerifyUserInformationRequestDTO.getFirstSurname());//cambiar a firstSurname
	    financingVerifyUserInformationResponseDTO.setSecondSurname(financingVerifyUserInformationRequestDTO.getSecondSurname());//cambiar a secondSurname
	    if(financingVerifyUserInformationRequestDTO.getGender().equals("M")) {
		FinancingGenericDTO genericGenderDTO = new FinancingGenericDTO();
		genericGenderDTO.setId("M");
		genericGenderDTO.setName("HOMBRE");
		financingVerifyUserInformationResponseDTO.setGender(genericGenderDTO);
	    }else {
		FinancingGenericDTO genericGenderDTO = new FinancingGenericDTO();
		genericGenderDTO.setId("W");
		genericGenderDTO.setName("MUJER");
		financingVerifyUserInformationResponseDTO.setGender(genericGenderDTO);
	    }
	    financingVerifyUserInformationResponseDTO.setPhone(financingVerifyUserInformationRequestDTO.getPhone());
	    financingVerifyUserInformationResponseDTO.setBirthday(financingVerifyUserInformationRequestDTO.getBirthday().toString());
	    financingVerifyUserInformationResponseDTO.setRfc(financingVerifyUserInformationRequestDTO.getRfc());
	    financingVerifyUserInformationResponseDTO.setShowBuro("true");	    


	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(financingVerifyUserInformationResponseDTO);
	    LogService.logger.info("FIN Servicio para verificar información de un usuario de árbol financiero");
	    return responseDTO;

	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(genericDTO);
	LogService.logger.info("FIN Servicio para verificar información de un usuario de árbol financiero");
	return responseDTO;
    }



    /**
     * Servicio para listar el RFC de un usuario de arbol
     * @author Juan Tolentino
     * @return getFinancingUserRfcArbol
     */
    public ResponseDTO getFinancingUserRfcArbol(String firstSurname, String secondSurname, String birthdate, String name){
	ResponseDTO responseDTO = new ResponseDTO();
	LogService.logger.info("Iniciando Metodo para saber el RFC de un usuario desde arbol financiero");
	FinancingUserRfcApiAttributesRequestDTO financingUserRfcApiAttributesRequestDTO = new FinancingUserRfcApiAttributesRequestDTO();
	FinancingUserRfcApiDataRequestDTO financingUserRfcApiDataRequestDTO = new FinancingUserRfcApiDataRequestDTO();
	GetFinancingUserRfcResponseDTO financingUserRfcResponseDTO = new GetFinancingUserRfcResponseDTO();
	ResponseEntity<FinancingUserRfcApiBodyResponseDTO> financingUserRfcApiBodyResponseDTO = null;
	String url = Constants.FINANCING_API_URL +"/api/v1/rfc";
	try {	    
	    // Armo el header		
	    ResponseDTO signature = getFinancingSignature();
	    RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.set("x-api-key", Constants.FINANCING_API_KEY);
	    headers.set("x-signature", signature.getData().toString());
	    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

	    financingUserRfcApiAttributesRequestDTO.setName(name);
	    financingUserRfcApiAttributesRequestDTO.setFirstSurname(firstSurname);
	    financingUserRfcApiAttributesRequestDTO.setSecondSurname(secondSurname);
	    financingUserRfcApiAttributesRequestDTO.setDay(Long.valueOf(birthdate.substring(8,10)));
	    financingUserRfcApiAttributesRequestDTO.setMonth(Long.valueOf(birthdate.substring(5,7)));
	    financingUserRfcApiAttributesRequestDTO.setYear(Long.valueOf(birthdate.substring(0,4)));

	    financingUserRfcApiDataRequestDTO.setType("rfc");
	    financingUserRfcApiDataRequestDTO.setAttributes(financingUserRfcApiAttributesRequestDTO);

	    // Map para armar el body de update
	    Map<String, Object> body = new HashMap<String, Object>();
	    body.put("data", financingUserRfcApiDataRequestDTO);

	    // Map para mandar el MapBody y el header de update
	    HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);



	    // llamada al servicio de arbol financiero
	    financingUserRfcApiBodyResponseDTO = restTemplate.exchange(url, HttpMethod.POST, entity, FinancingUserRfcApiBodyResponseDTO.class);

	    financingUserRfcResponseDTO.setRfc(financingUserRfcApiBodyResponseDTO.getBody().getData().getAttributes().getRfc());
	    LogService.logger.info("FIN Metodo para saber el RFC de un usuario desde arbol financiero");
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus()); 
	    responseDTO.setData(financingUserRfcResponseDTO);
	    return responseDTO;

	} catch (Exception ex) {
	    LogService.logger.error("Catch error en el api RFC de arbol financiero");
	    try {
		responseDTO = getFinancingUserRfc(firstSurname, secondSurname, birthdate, name);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus()); 
		responseDTO.setData(responseDTO.getData());
		return responseDTO;
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus()); 
	    return responseDTO;
	}
    }


    /**
     * Servicio para consultar los perfiles de un usuario
     * @author Juan Tolentino
     * @return getFinancingUserProfiles
     */
    public ResponseDTO getFinancingUserProfiles(Long idUser){
	ResponseDTO responseDTO = new ResponseDTO();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	try {
	    List<FinancingUser> listFinancingUserBD = financingUserRepo.findByUserIdAndActiveTruee(idUser);

	    if(listFinancingUserBD == null) {
		LogService.logger.info("The financing_id not exist"); 
		MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
		responseDTO.setCode(enumResult.getValue());
		listMessages.add(messageUserNotExist);
		responseDTO.setListMessage(listMessages);	    
		return responseDTO;
	    }else {
		List<FinancingUserResponseDTO> listFinancingUserResponseDTO = new ArrayList<>();
		for(FinancingUser financingUserActual : listFinancingUserBD) {
		    FinancingUserResponseDTO financingUserResponseDTO = new FinancingUserResponseDTO();
		    Date creationDate = dateFormat.parse(financingUserActual.getCreationDate().toString());
		    String stringCreationDate = dateFormat.format(creationDate);
		    financingUserResponseDTO.setCreationDate(stringCreationDate);
		    financingUserResponseDTO.setRfc(financingUserActual.getRfc());
		    financingUserResponseDTO.setFullName(financingUserActual.getFullName() +" "+ financingUserActual.getFirstName() +" "+ financingUserActual.getSecondName());
		    financingUserResponseDTO.setFinancingUserId(financingUserActual.getFinancingUserId());
		    listFinancingUserResponseDTO.add(financingUserResponseDTO);
		}
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus()); 
		responseDTO.setData(listFinancingUserResponseDTO);
		return responseDTO;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
	return responseDTO;
    }


    /**
     * Servicio para consultar el perfil de un usuario
     * @author Juan Tolentino
     * @return getFinancingUser
     */
    public ResponseDTO getFinancingUser(Long idUser, Long financingId){
	ResponseDTO responseDTO = new ResponseDTO();
	FinancingVerifyUserInformationResponseDTO financingVerifyUserInformationResponseDTO = new FinancingVerifyUserInformationResponseDTO();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	try {
	    FinancingUser financingUserBD = financingUserRepo.findByFinancingUserIdAndUserIdAndActiveTrue(financingId,idUser);

	    if(financingUserBD == null) {
		LogService.logger.info("The financing_id not exist"); 
		MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
		responseDTO.setCode(enumResult.getValue());
		listMessages.add(messageUserNotExist);
		responseDTO.setListMessage(listMessages);	    
		return responseDTO;
	    }else {

		//llenado parcial
		financingVerifyUserInformationResponseDTO.setIdFinancing(financingUserBD.getFinancingUserId().toString());
		financingVerifyUserInformationResponseDTO.setEmail(financingUserBD.getEmail());
		financingVerifyUserInformationResponseDTO.setAuthenticationToken(financingUserBD.getAuthenticationToken());
		financingVerifyUserInformationResponseDTO.setFullName(financingUserBD.getFullName());
		financingVerifyUserInformationResponseDTO.setFirstSurname(financingUserBD.getFirstName());//cambiar a firstSurname
		financingVerifyUserInformationResponseDTO.setSecondSurname(financingUserBD.getSecondName());//cambiar a secondSurname
		if(financingUserBD.getGender().equals("M")) {
		    FinancingGenericDTO genericGenderDTO = new FinancingGenericDTO();
		    genericGenderDTO.setId("M");
		    genericGenderDTO.setName("HOMBRE");
		    financingVerifyUserInformationResponseDTO.setGender(genericGenderDTO);
		}else {
		    FinancingGenericDTO genericGenderDTO = new FinancingGenericDTO();
		    genericGenderDTO.setId("W");
		    genericGenderDTO.setName("MUJER");
		    financingVerifyUserInformationResponseDTO.setGender(genericGenderDTO);
		}

		financingVerifyUserInformationResponseDTO.setPhone(financingUserBD.getPhone());
		financingVerifyUserInformationResponseDTO.setRfc(financingUserBD.getRfc());

		try {
		    financingVerifyUserInformationResponseDTO.setBirthday(financingUserBD.getBirthday().toString());
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }

	    try {
		if(financingUserBD.getBuroDate() != null) {
		    String today = dateFormat.format(new Date());  
		    Date startDate=dateFormat.parse(financingUserBD.getBuroDate().toString());
		    Date endDate=dateFormat.parse(today);
		    int dates=(int) ((endDate.getTime()-startDate.getTime())/86400000);

		    if(financingUserBD.getBuroDate() == null || dates >= 90 ) {
			financingVerifyUserInformationResponseDTO.setShowBuro("true");
		    }else {
			financingVerifyUserInformationResponseDTO.setShowBuro("false");
		    }
		}else {
		    financingVerifyUserInformationResponseDTO.setShowBuro("true");
		}

	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    FinancingUserDomicile FinancingUserDomicileBD = financingUserDomicileRepo.findByFinancingIdAndActiveTrue(financingUserBD.getFinancingUserId());
	    if(FinancingUserDomicileBD != null) {

		//llenado completo
		financingVerifyUserInformationResponseDTO.setStreet(FinancingUserDomicileBD.getStreet());
		financingVerifyUserInformationResponseDTO.setExternalNumber(FinancingUserDomicileBD.getExternalNumber());
		financingVerifyUserInformationResponseDTO.setInternalNumber(FinancingUserDomicileBD.getInternalNumber());
		financingVerifyUserInformationResponseDTO.setColony(FinancingUserDomicileBD.getColony());
		financingVerifyUserInformationResponseDTO.setDelegation(FinancingUserDomicileBD.getDelegation());
		financingVerifyUserInformationResponseDTO.setState(FinancingUserDomicileBD.getState());
		financingVerifyUserInformationResponseDTO.setZipCode(FinancingUserDomicileBD.getZipCode());
		financingVerifyUserInformationResponseDTO.setResidentYears(FinancingUserDomicileBD.getResidentYears());
		FormOptionValue formOptionValueLivingTypeBD = formOptionValueRepo.findByValue(FinancingUserDomicileBD.getLivingType().toString().trim());
		if(formOptionValueLivingTypeBD != null) {
		    FinancingGenericDTO financingGenericLivingTypeDTO = new FinancingGenericDTO();
		    financingGenericLivingTypeDTO.setId(formOptionValueLivingTypeBD.getValue());
		    financingGenericLivingTypeDTO.setName(formOptionValueLivingTypeBD.getName());
		    financingVerifyUserInformationResponseDTO.setLivingType(financingGenericLivingTypeDTO);
		}

	    }

	    FinancingUserIncome financingUserIncomeBD = financingUserIncomeRepo.findByFinancingIdAndActiveTrue(financingUserBD.getFinancingUserId());

	    if(financingUserIncomeBD != null) {
		FormOptionValue formOptionValueIncomeProfileBD = formOptionValueRepo.findByValue(financingUserIncomeBD.getIncomeProfile().toString().trim());
		FormOptionValue formOptionValueAutomotiveCarUseTypeBD = formOptionValueRepo.findByValue(financingUserIncomeBD.getAutomotiveCarUseType().toString().trim());
		financingVerifyUserInformationResponseDTO.setNetIncomeVerified(financingUserIncomeBD.getNetIncomeVerified().toString());
		if(formOptionValueIncomeProfileBD != null) {
		    FinancingGenericDTO financingGenericIncomeProfileDTO = new FinancingGenericDTO();
		    financingGenericIncomeProfileDTO.setId(formOptionValueIncomeProfileBD.getValue());
		    financingGenericIncomeProfileDTO.setName(formOptionValueIncomeProfileBD.getName());
		    financingVerifyUserInformationResponseDTO.setIncomeProfile(financingGenericIncomeProfileDTO);
		}
		if(formOptionValueAutomotiveCarUseTypeBD != null) {
		    FinancingGenericDTO financingGenericAutomotiveCarUseTypeDTO = new FinancingGenericDTO();
		    financingGenericAutomotiveCarUseTypeDTO.setId(formOptionValueAutomotiveCarUseTypeBD.getValue());
		    financingGenericAutomotiveCarUseTypeDTO.setName(formOptionValueAutomotiveCarUseTypeBD.getName());
		    financingVerifyUserInformationResponseDTO.setAutomotiveCarUseType(financingGenericAutomotiveCarUseTypeDTO);
		}
		financingVerifyUserInformationResponseDTO.setPositionAge(financingUserIncomeBD.getPositionAge().toString());
	    }

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(financingVerifyUserInformationResponseDTO);
	    return responseDTO;

	} catch (Exception e) {
	    e.printStackTrace();
	}

	return responseDTO;
    }



}
