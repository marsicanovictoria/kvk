package com.kavak.core.service.netsuite;

import com.kavak.core.dto.UserDTO;
import com.kavak.core.dto.response.CustomerResponseDTO;
import com.kavak.core.dto.response.PostInspectionScheduleResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.LogService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Stateless
public class CustomerServices {

    @Inject
    private UserRepository userRepo;

    @Inject
    private UserMetaRepository  userMetaRepo;

    public ResponseDTO postNewCustomer(UserDTO userData){
        PostInspectionScheduleResponseDTO postResponseDTO =  new PostInspectionScheduleResponseDTO();
        ResponseDTO responseDTO = new ResponseDTO();

        try {
            User userBD = new User();
            userBD.setName(userData.getName());
            userBD.setUserName(userData.getUsername());
            userBD.setEmail(userData.getEmail());
            userBD.setRole(Constants.CUSTOMER_ROLE);
            userBD.setSentNetsuite(true);
            userBD.setCreationDate(new Timestamp(System.currentTimeMillis()));
            userBD.setActive(true);
            userBD.setPassword(Constants.DEFAULT_PASSWORD);

            User idNewCustomer = userRepo.save(userBD);

            if (userData.getPhone() != null){
                UserMeta userMetaPhone = new UserMeta();
                userMetaPhone.setUser(idNewCustomer);
                userMetaPhone.setMetaKey(Constants.META_VALUE_PHONE);
                userMetaPhone.setMetaValue(userData.getPhone());
                userMetaRepo.save(userMetaPhone);
            }

            if (userData.getAddress() != null){
                UserMeta userMetaAddress = new UserMeta();
                userMetaAddress.setUser(idNewCustomer);
                userMetaAddress.setMetaKey(Constants.META_VALUE_ADDRESS);
                userMetaAddress.setMetaValue(userData.getAddress());
                userMetaRepo.save(userMetaAddress);
            }

            postResponseDTO.setId(idNewCustomer.getId());

            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(postResponseDTO);
        } catch (Exception e) {
            LogService.logger.error("Error al intentar creara un customer desde netsuite webservices " + e);
        }

        return responseDTO;
    }

    public ResponseDTO getCustomerById(Long id){
        ResponseDTO responseDTO = new ResponseDTO();
        CustomerResponseDTO customerResponseDTO = new CustomerResponseDTO();

        try {
            Optional<User> userBD = userRepo.findById(id);
            customerResponseDTO.setId(userBD.get().getId());
            customerResponseDTO.setName(userBD.get().getName());
            customerResponseDTO.setUsername(userBD.get().getUserName());
            customerResponseDTO.setPhone(userBD.get().getDTO().getPhone());
            customerResponseDTO.setAddressNetsuite(userBD.get().getDTO().getAddress());
            customerResponseDTO.setEmail(userBD.get().getEmail());
            customerResponseDTO.setRole(userBD.get().getRole());

            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(customerResponseDTO);
        } catch (Exception e) {
            LogService.logger.error("Error al intentar consultar el customer con ID " +id+ ", " + e);
        }
        return responseDTO;
    }

    public ResponseDTO getCustomerByEmail(String email){
        ResponseDTO responseDTO = new ResponseDTO();
        CustomerResponseDTO customerResponseDTO = new CustomerResponseDTO();

        try {
            User userBD = userRepo.findByEmailContainingIgnoreCase(email);
            customerResponseDTO.setId(userBD.getId());
            customerResponseDTO.setName(userBD.getName());
            customerResponseDTO.setUsername(userBD.getUserName());
            customerResponseDTO.setPhone(userBD.getDTO().getPhone());
            customerResponseDTO.setAddressNetsuite(userBD.getDTO().getAddress());
            customerResponseDTO.setEmail(userBD.getEmail());
            customerResponseDTO.setRole(userBD.getRole());

            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(customerResponseDTO);
        } catch (Exception e) {
            LogService.logger.error("Error al intentar consultar el customer con email " +email+ ", " + e);
        }
        return responseDTO;
    }

    public ResponseDTO getNewUsersNetsuite() {
        ResponseDTO responseDTO = new ResponseDTO();

        try {
            responseDTO = new ResponseDTO();
            boolean sentNetsuite = false;
            List<User> listUserBD = userRepo.findTop30BySentNetsuite(sentNetsuite);
            List<CustomerResponseDTO> listUserResponseDTO = new ArrayList<>();

            for (User userlist : listUserBD) {

                UserDTO userDTO = userlist.getDTO();
                CustomerResponseDTO customerResponseDTO = new CustomerResponseDTO();
                customerResponseDTO.setId(userDTO.getId());
                customerResponseDTO.setName(userDTO.getName());
                customerResponseDTO.setUsername(userDTO.getUsername());
                customerResponseDTO.setEmail(userDTO.getEmail());
                customerResponseDTO.setRole(userDTO.getRole());
                customerResponseDTO.setAddressNetsuite(userDTO.getAddress());
                customerResponseDTO.setPhone(userDTO.getPhone());
                listUserResponseDTO.add(customerResponseDTO);
            }

            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(listUserResponseDTO);
        } catch (Exception e) {
            LogService.logger.error("Error al intentar consultar los nuevos customers para netsuite, " + e);
        }

        return responseDTO;

    }

    public ResponseDTO putSentNetsuiteUsers(List<String> usersId){
        ResponseDTO responseDTO = null;

        try {
            responseDTO = new ResponseDTO();
            List<Long> listNetsuiteUsersIds = new ArrayList<>();
            for(String id: usersId){
                listNetsuiteUsersIds.add(Long.valueOf(id));
            }

            List<User> listUsersBD = userRepo.findAllById(listNetsuiteUsersIds);
            for (User user : listUsersBD){
                user.setSentNetsuite(true);
            }

            userRepo.saveAll(listUsersBD);

            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());

        } catch (Exception e) {
            LogService.logger.error("Error al intentar actualizar el estatus de envío para netsuite de los nuevos Customers, " + e);
        }

        return responseDTO;

    }
}
