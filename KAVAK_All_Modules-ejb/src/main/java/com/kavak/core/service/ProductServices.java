package com.kavak.core.service;

import java.sql.Timestamp;
import java.util.*;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.github.ooxi.phparser.SerializedPhpParser;
import com.github.ooxi.phparser.SerializedPhpParserException;
import com.kavak.core.dto.*;
import com.kavak.core.dto.financing.response.FinancingGenericResponseDTO;
import com.kavak.core.dto.request.PostDimplesCarRequestDTO;
import com.kavak.core.dto.response.CarFeaturesResponseDTO;
import com.kavak.core.dto.response.CarResponseDTO;
import com.kavak.core.dto.response.CarResponseV2DTO;
import com.kavak.core.dto.response.GetCarContentOverviewResponse;
import com.kavak.core.dto.response.GetInspectionReportResponseDTO;
import com.kavak.core.dto.response.GetWhyKavakResponseDTO;
import com.kavak.core.dto.response.GetWhyLoveResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.*;
import com.kavak.core.model.ApoloInspectionPointCar;
import com.kavak.core.model.ApoloInspectionPointsHistoryCar;
import com.kavak.core.model.CarColor;
import com.kavak.core.model.CarData;
import com.kavak.core.model.InspectionCarFeature;
import com.kavak.core.model.InspectionDimple;
import com.kavak.core.model.InspectionLocation;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.ProductCarCompareSection;
import com.kavak.core.model.PromotionCarPrice;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.SellCarDimple;
import com.kavak.core.model.SellCarInsurance;
import com.kavak.core.model.SellCarWarranty;
import com.kavak.core.model.Testimonials;
import com.kavak.core.model.ZipCode;
import com.kavak.core.repositories.ApoloInspectionPointCarRepository;
import com.kavak.core.repositories.ApoloInspectionPointsHistoryCarRepository;
import com.kavak.core.repositories.CarColorRepository;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.InspectionCarFeatureRepository;
import com.kavak.core.repositories.InspectionDimpleRepository;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.ProductCarCompareSectionRepository;
import com.kavak.core.repositories.PromotionCarPriceRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.SellCarDimpleRepository;
import com.kavak.core.repositories.TestimonialsRepository;
import com.kavak.core.repositories.ZipCodeRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

/**
 * <h1>Data de Carros</h1>
 *
 * @author Enrique
 * @since 2017-05-09
 */

@Stateless
public class ProductServices {

	@Inject
	private SellCarDetailRepository sellCarDetailRepo;

	@Inject
	private MetaValueRepository metaValueRepo;

	@Inject
	private SellCarDimpleRepository sellCarDimpleRepo;

	@Inject
	private MessageRepository messageRepo;

	@Inject
	private CarDataRepository carDataRepo;

	@Inject
	private TestimonialsRepository testimonialsRepo;

	@Inject
	private ZipCodeRepository zipCodeRepo;

	@Inject
	private ProductCarCompareSectionRepository productCarCompareSectionRepo;

	@Inject
	private InspectionCarFeatureRepository inspectionCarFeatureRepo;

	@Inject
	private InspectionDimpleRepository inspectionDimpleRepo;

	@Inject
	private CarColorRepository carColorRepo;

	@Inject
	private ApoloInspectionPointsHistoryCarRepository apoloInspectionPointsHistoryCarRepo;

	@Inject
	private ApoloInspectionPointCarRepository apoloInspectionPointCarRepo;

	@Inject
	private PromotionCarPriceRepository promotionCarPriceRepo;
	
	@Inject
	private InspectionLocationRepository inspectionLocationRepo;
	
	

	/**
	 * Consulta las caracteristicas del carro.
	 *
	 * @param idSellCarDetail identificador del carro @Id from SellCarDetail
	 * @return CarResponseDTO contiene los datos de auto
     * @author Enrique Marin
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	public ResponseDTO getCar(Long idSellCarDetail,Long apiVersionController) {
		ResponseDTO responseDTO = new ResponseDTO();
		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(idSellCarDetail);
		if (!sellCarDetailBD.isPresent()) {
			MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0000.toString()).getDTO();
			List<MessageDTO> listMessageDTO = new ArrayList<>();
			listMessageDTO.add(messageNoRecords);
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setListMessage(listMessageDTO);
			responseDTO.setData(new ArrayList<>());
			return responseDTO;
		}

		CarResponseDTO carResponseDTO = new CarResponseDTO();
	    CarResponseV2DTO carResponseV2DTO = new CarResponseV2DTO();
		CarInformationDTO carInformationDTO = new CarInformationDTO();

		List<String> listProductPageEnumNames = new ArrayList<>();
	    FinancingGenericResponseDTO financingGenericResponseDTO = new FinancingGenericResponseDTO();
	    List<FinancingGenericResponseDTO> listProductPageEnumNamesV2 = new ArrayList<>();
		for (ProductPageEnum productPageEnumActual : ProductPageEnum.values()) {
		if(apiVersionController.equals(1L)) {
		    if(!productPageEnumActual.getName().equals("APLICO PARA CRÉDITO")) {
			listProductPageEnumNames.add(productPageEnumActual.getName());
		}
		}else if(apiVersionController.equals(2L)){
		    financingGenericResponseDTO = new FinancingGenericResponseDTO();
		    financingGenericResponseDTO.setId(productPageEnumActual.getValue().toString());
		    financingGenericResponseDTO.setName(productPageEnumActual.getName());
		    listProductPageEnumNamesV2.add(financingGenericResponseDTO);
		}			
	    }
	    if(apiVersionController.equals(2L)) {
		carResponseV2DTO.setListProductPageEnumNames(listProductPageEnumNamesV2);
	    }else if(apiVersionController.equals(1L)){
		carResponseDTO.setListProductPageEnumNames(listProductPageEnumNames);
	    }

	    //carResponseDTO.setListProductPageEnumNames(listProductPageEnumNames);
		SellCarDetailDTO sellCarDetailDTOBD = sellCarDetailBD.get().getDTO();
		carInformationDTO.setId(sellCarDetailDTOBD.getId());
		carInformationDTO.setName(KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()) + " " + sellCarDetailDTOBD.getCarPackage());
		carInformationDTO.setYear(sellCarDetailDTOBD.getCarYear());
		carInformationDTO.setKm(sellCarDetailDTOBD.getCarKm());
		carInformationDTO.setAllowsAppointments(sellCarDetailBD.get().isAllowsAppointments());
		if (sellCarDetailDTOBD.getListSellCarWarranty() == null) {
			carInformationDTO.setListSellCarWarranty(new ArrayList<>());
		} else {
			List<GenericDTO> listWarrantyGenericDTO = new ArrayList<>();
			for (SellCarWarranty sellCarWarrantyActual : sellCarDetailDTOBD.getListSellCarWarranty()) {
				GenericDTO newWarrantyGenericDTO = new GenericDTO();
				MetaValue metaValueBD = metaValueRepo.findByIdAndActiveTrue(sellCarWarrantyActual.getIdMetaValue());
				if (metaValueBD != null) {
					newWarrantyGenericDTO.setId(metaValueBD.getId());
					newWarrantyGenericDTO.setName(metaValueBD.getGroupName());
					newWarrantyGenericDTO.setContent(metaValueBD.getOptionName());
					newWarrantyGenericDTO.setAmount(sellCarWarrantyActual.getWarrantyValue());
					listWarrantyGenericDTO.add(newWarrantyGenericDTO);
				}
			}
			carInformationDTO.setListSellCarWarranty(listWarrantyGenericDTO);
		}

		if (sellCarDetailDTOBD.getListSellCarInsurance() == null) {
			carInformationDTO.setlistSellCarInsurance(new ArrayList<>());
		} else {
			HashMap<String, List<GenericDTO>> mapInsuranceGenericDTO = new HashMap<>();
			for (SellCarInsurance sellCarInsuranceActual : sellCarDetailDTOBD.getListSellCarInsurance()) {
				GenericDTO newInsuranceGenericDTO = new GenericDTO();
				MetaValue metaValueBD = metaValueRepo.findByIdAndActiveTrue(sellCarInsuranceActual.getIdMetaValue());
				if (metaValueBD != null) {
					newInsuranceGenericDTO.setId(metaValueBD.getId());
					newInsuranceGenericDTO.setAmount(sellCarInsuranceActual.getInsuranceValue());
					if (metaValueBD.getAlias() == null || metaValueBD.getAlias().isEmpty()) {
						newInsuranceGenericDTO.setContent(metaValueBD.getOptionName());
					} else {
						newInsuranceGenericDTO.setContent(metaValueBD.getAlias());
					}
					if (mapInsuranceGenericDTO.get(metaValueBD.getGroupName()) == null) {
						List<GenericDTO> listInsuranceGenericDTO = new ArrayList<>();
						listInsuranceGenericDTO.add(newInsuranceGenericDTO);
						mapInsuranceGenericDTO.put(metaValueBD.getGroupName(), listInsuranceGenericDTO);
					} else {
						mapInsuranceGenericDTO.get(metaValueBD.getGroupName()).add(newInsuranceGenericDTO);
					}
				}
			}
			List<InsuranceGroupDTO> listResultInsurance = new ArrayList<>();
			for (String keyName : mapInsuranceGenericDTO.keySet()) {
				InsuranceGroupDTO newInsuranceGroupDTO = new InsuranceGroupDTO();
				newInsuranceGroupDTO.setName(keyName);
				for (GenericDTO insuranceGenericActual : mapInsuranceGenericDTO.get(keyName)) {
					newInsuranceGroupDTO.setListInsuranceGenericDTO(mapInsuranceGenericDTO.get(keyName));
				}
				listResultInsurance.add(newInsuranceGroupDTO);
			}
			carInformationDTO.setlistSellCarInsurance(listResultInsurance);
		}

		if (sellCarDetailDTOBD.getListSellCarDimple() == null) {
			carInformationDTO.setListSellCarDimple(new ArrayList<>());
		} else {
			List<GenericDTO> listSellCarDimple = new ArrayList<>();
			for (SellCarDimple sellCarDimple : sellCarDimpleRepo.findByIdSellCarDetailOrderByDimpleType(idSellCarDetail)) {
				GenericDTO newSellCarDimpleGenericDTO = new GenericDTO();

                CharSequence http = "http";
                CharSequence https = "https";

                if (sellCarDimple.getDimpleImageUrl().contains(http) || sellCarDimple.getDimpleImageUrl().contains(https)) {
                        newSellCarDimpleGenericDTO.setUrl(sellCarDimple.getDimpleImageUrl());
                } else {
				newSellCarDimpleGenericDTO.setUrl(Constants.URL_KAVAK + sellCarDimple.getDimpleImageUrl());
                }
				newSellCarDimpleGenericDTO.setMessage(sellCarDimple.getDimpleName());
				listSellCarDimple.add(newSellCarDimpleGenericDTO);
			}
			carInformationDTO.setListSellCarDimple(listSellCarDimple);
		}

		// Para el deserializado en JAVA se usa una un parser que transforma el
		// string a hashmap
		String serialized = sellCarDetailBD.get().getSellCarMeta().getMetaValue();

		try {
			// En el primer level del hashmap tiene como value otro hashmap que
			// nos referimos a el como hasmap de segundo level
			Map<String, Map<String, String>> mapFirstLvlDeserialized = (Map<String, Map<String, String>>) new SerializedPhpParser(serialized).parse();
			// Se genera un deserealizado aparte ya que en esta variable esta
			// almacenada directamente como un String
			Map<String, String> mapFepmDeserialized = (Map<String, String>) new SerializedPhpParser(serialized).parse();
			Map<String, String> mapBasicOverview = mapFirstLvlDeserialized.get(Constants.BASIC_OVERVIEW);
			Map<String, String> mapPricing = mapFirstLvlDeserialized.get(Constants.PRICING);

			carInformationDTO.setBodyType(mapBasicOverview.get(Constants.BODY_TYPE));
			carInformationDTO.setTransmission(mapBasicOverview.get(Constants.TRANSMISION));
			carInformationDTO.setShippingCost("0");
			carInformationDTO.setPrice(sellCarDetailDTOBD.getPrice());
			carInformationDTO.setMarketPrice(Double.valueOf(mapPricing.get(Constants.MARKET_PRICE)));
			// carInformationDTO.setSavings(carInformationDTO.getMarketPrice() -
			// carInformationDTO.getPrice());
			carInformationDTO.setFepm(Double.valueOf(mapFepmDeserialized.get(Constants.FEPM)));
			carInformationDTO.setListImages(sellCarDetailDTOBD.getListImagesCar());
			carInformationDTO.setStatus(KavakUtils.getStatusCar(sellCarDetailBD.get()));
			// Se busca si el carro tiene promocion
		Optional<PromotionCarPrice> promotionCarPriceBD = promotionCarPriceRepo.findById(idSellCarDetail);
		if (promotionCarPriceBD.isPresent()) {
		    carInformationDTO.setPromotionPrice(promotionCarPriceBD.get().getPrice());
		    carInformationDTO.setPromotionName(promotionCarPriceBD.get().getPromotion().getName());
		    carInformationDTO.setPromotionColor(promotionCarPriceBD.get().getPromotion().getColor());
			}

			Long WarrantyMarket = 0L;
			Long paperworkMarket = 0L;
			Long inspectionMarket = 0L;
			Long detailingMarket = 0L;
			Long tenureMarket = 0L;

			if (mapPricing.get(Constants.WARRANTYMARKET) == null || mapPricing.get(Constants.WARRANTYMARKET).toString().isEmpty()) {
				WarrantyMarket = 0L;
			} else {
				WarrantyMarket = Long.valueOf(mapPricing.get(Constants.WARRANTYMARKET).toString());
			}
			if (mapPricing.get(Constants.PAPERWORKMKT) == null || String.valueOf(mapPricing.get(Constants.PAPERWORKMKT)).isEmpty()) {
				paperworkMarket = 0L;
			} else {
				paperworkMarket = Long.valueOf(String.valueOf(mapPricing.get(Constants.PAPERWORKMKT)));
			}
			if (mapPricing.get(Constants.INSPECTIONPRICEMKT) == null || String.valueOf(mapPricing.get(Constants.INSPECTIONPRICEMKT)).isEmpty()) {
				inspectionMarket = 0L;
			} else {
				inspectionMarket = Long.valueOf(String.valueOf(mapPricing.get(Constants.INSPECTIONPRICEMKT)));
			}
			if (mapPricing.get(Constants.DETAILINGMKT) == null || String.valueOf(mapPricing.get(Constants.DETAILINGMKT)).isEmpty()) {
				detailingMarket = 0L;
			} else {
				detailingMarket = Long.valueOf(String.valueOf(mapPricing.get(Constants.DETAILINGMKT)));
			}
			if (mapPricing.get(Constants.TENUREMARKET) == null || String.valueOf(mapPricing.get(Constants.TENUREMARKET)).isEmpty()) {
				tenureMarket = 0L;
			} else {
				tenureMarket = Long.valueOf(String.valueOf(mapPricing.get(Constants.TENUREMARKET)));
			}
		Optional<PromotionCarPrice> promotionCarPrice = promotionCarPriceRepo.findById(idSellCarDetail);
		if (!promotionCarPrice.isPresent()) {
				carInformationDTO.setSavings((carInformationDTO.getMarketPrice() - carInformationDTO.getPrice()) + WarrantyMarket + paperworkMarket + inspectionMarket + detailingMarket + tenureMarket);
			} else {
		    carInformationDTO.setSavings((carInformationDTO.getMarketPrice() - carInformationDTO.getPrice()) + (carInformationDTO.getPrice() - promotionCarPrice.get().getPrice()) + WarrantyMarket + paperworkMarket + inspectionMarket + detailingMarket + tenureMarket);
			}
		
		
		GenericDTO genericInspectionLocation = new GenericDTO();
// se comento locations
//		if(sellCarDetailBD.get().getIdWarehouseLocation() != null) {
//		    InspectionLocation inspectionLocation = inspectionLocationRepo.findAllActiveAndAliasNotNullAndIdWarehouseLocation(sellCarDetailBD.get().getIdWarehouseLocation());
//
//		    if(inspectionLocation != null) {
//			genericInspectionLocation.setValue(inspectionLocation.getAlias());
//			genericInspectionLocation.setName(Constants.AUTO_LOCATION);
//			genericInspectionLocation.setUrl(inspectionLocation.getUrlMap());
//			carInformationDTO.setLocation(genericInspectionLocation);
//		    }
//		}

		} catch (SerializedPhpParserException e) {
			e.printStackTrace();
		}

	    if(!listProductPageEnumNamesV2.isEmpty()) {
		// product_url : Link del auto
		if (KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get()).isEmpty() || KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get()) == null) {
		    carResponseV2DTO.setUrlProduct("NA");
		} else {
		    carResponseV2DTO.setUrlProduct(KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get()));
		}
		carResponseV2DTO.setCarInformationDTO(carInformationDTO);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(carResponseV2DTO);
		return responseDTO;
	    }else {
		// product_url : Link del auto
		if (KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get()).isEmpty() || KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get()) == null) {
			carResponseDTO.setUrlProduct("NA");
		} else {
			carResponseDTO.setUrlProduct(KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get()));
		}
		carResponseDTO.setCarInformationDTO(carInformationDTO);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(carResponseDTO);
		return responseDTO;
	}

	}

	/**
	 * Consultas los detalles o features que tenga el carro.
	 *
     * @param id identificador del carro @Id from @SellCarDetail
     * @return CarFeaturesResponseDTO
	 * @author Enrique Marin
	 */
	@SuppressWarnings("unchecked")
	public ResponseDTO getCarFeaturesById(Long id) {
		ResponseDTO responseDTO = new ResponseDTO();
		CarFeaturesResponseDTO carFeaturesResponseDTO = new CarFeaturesResponseDTO();
		List<GenericDTO> listDetails = new ArrayList<>();
		List<GenericDTO> listPerformances = new ArrayList<>();
		List<GenericDTO> listFuelConsumption = new ArrayList<>();
		List<String> listEntertainment = new ArrayList<>();
		List<String> listExterior = new ArrayList<>();
		List<String> listInterior = new ArrayList<>();
		List<String> listMechanics = new ArrayList<>();
		List<String> listSafety = new ArrayList<>();
		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(id);
		CarData carDataBD = carDataRepo.findBySku(sellCarDetailBD.get().getSku());

		if (carDataBD == null || !sellCarDetailBD.isPresent()) {
			MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0000.toString()).getDTO();
			List<MessageDTO> listMessageDTO = new ArrayList<>();
			listMessageDTO.add(messageNoRecords);
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setListMessage(listMessageDTO);
			responseDTO.setData(new ArrayList<>());
			return responseDTO;
		}

		GenericDTO stockGenericDTO = new GenericDTO();
		GenericDTO dataSheetGenericDTO = new GenericDTO();
		GenericDTO vinGenericDTO = new GenericDTO();
		GenericDTO reportHistoryGenericDTO = new GenericDTO();
		GenericDTO bodyTypeGenericDTO = new GenericDTO();
		GenericDTO exteriorColorGenericDTO = new GenericDTO();
		GenericDTO interiorColorGenericDTO = new GenericDTO();
		GenericDTO doorGenericDTO = new GenericDTO();
		GenericDTO keyGenericDTO = new GenericDTO();
		GenericDTO preOwnerGenericDTO = new GenericDTO();
		GenericDTO engineGenericDTO = new GenericDTO();
		GenericDTO horsePowerGenericDTO = new GenericDTO();
		GenericDTO fueltypeGenericDTO = new GenericDTO();
		GenericDTO cityGenericDTO = new GenericDTO();
        GenericDTO fuelComsuptionFreeway = new GenericDTO();
		GenericDTO fuelComsuptionCity = new GenericDTO();
		GenericDTO fuelComsuptionCombined = new GenericDTO();
		GenericDTO rapiGenericDTO = new GenericDTO();
		GenericDTO currentWarranty = new GenericDTO();
		GenericDTO numberPassengers = new GenericDTO();

		String entertainment;
		String exterior;
		String interior;
		String mechanical;
		String safety;

		stockGenericDTO.setName("STOCK#");
		dataSheetGenericDTO.setName("FICHA TÉCNICA");
		vinGenericDTO.setName("VIN(#SERIE)");
		reportHistoryGenericDTO.setName("REPORTE DE HISTORIAL");
		bodyTypeGenericDTO.setName("ESTILO");
		exteriorColorGenericDTO.setName("COLOR EXTERIOR");
		interiorColorGenericDTO.setName("COLOR INTERIOR");
		doorGenericDTO.setName("PUERTAS");
		keyGenericDTO.setName("CANTIDAD DE LLAVES");
		preOwnerGenericDTO.setName("DUEÑOS ANTERIORES");
		engineGenericDTO.setName("MOTOR");
		horsePowerGenericDTO.setName("CABALLOS DE FUERZA");
		fueltypeGenericDTO.setName("TIPO DE COMBUSTIBLE");
		cityGenericDTO.setName("CIUDAD");
		fuelComsuptionCity.setName("CIUDAD");
		fuelComsuptionFreeway.setName("AUTOPISTA");
		fuelComsuptionCombined.setName("COMBINADO");
		rapiGenericDTO.setName("RAPI");
		currentWarranty.setName("GARANTÍA VIGENTE");
		numberPassengers.setName("NÚMERO DE PASAJEROS");

		String serializedMetaValue = sellCarDetailBD.get().getSellCarMeta().getMetaValue();
		try {
			// En el primer level del hashmap tiene como value otro hashmap que
			// nos referimos a el como hasmap de segundo level
			Map<Object, Map<Object, Object>> mapFirstLvlDeserialized = (Map<Object, Map<Object, Object>>) new SerializedPhpParser(serializedMetaValue).parse();
			// Debido a que en PHP la variable esta almacenada de esta manera
			// $main['basic_overview']['engine'] los valores del primer lvl se
			// obtienen sacando el hashmap 'basic_overview'
			Map<Object, Object> mapBasicOverview = mapFirstLvlDeserialized.get(Constants.BASIC_OVERVIEW);

			// Una vez obtenido el hasmap interno o de segundo level obtenemos
			// los campos precisos que se necesitan
			// Details
	    /*if (mapBasicOverview.get(Constants.STOCK).getClass().getName().equalsIgnoreCase("com.github.ooxi.phparser.SerializedPhpParser$1")) {
		stockGenericDTO.setValue("Pre-aproved");
	    } else {
		stockGenericDTO.setValue(mapBasicOverview.get(Constants.STOCK).toString());
	    }*/

			stockGenericDTO.setValue(sellCarDetailBD.get().getId().toString());
			if(sellCarDetailBD.get().getCurrentWarranty() != null) {
			    currentWarranty.setValue(sellCarDetailBD.get().getCurrentWarranty().toUpperCase()); 
			}
			
			if(mapBasicOverview.get(Constants.PASSENGERS) != null) {
				numberPassengers.setValue(mapBasicOverview.get(Constants.PASSENGERS).toString().toUpperCase());
			}
			
			if(mapBasicOverview.get(Constants.RAPI_URL) != null) {
			    rapiGenericDTO.setValue(Constants.DOWNLOAD);
			    rapiGenericDTO.setUrl(mapBasicOverview.get(Constants.RAPI_URL).toString());
			}

			dataSheetGenericDTO.setValue("DESCARGAR");

			dataSheetGenericDTO.setUrl(Constants.URL_BASE_DATA_SHEET + id);
			vinGenericDTO.setValue(sellCarDetailBD.get().getVin());
			reportHistoryGenericDTO.setValue("REPUVE");

			if (mapBasicOverview.get(Constants.HISTORY_REPORT).toString() == null || mapBasicOverview.get(Constants.HISTORY_REPORT).toString().isEmpty()) {
				reportHistoryGenericDTO.setUrl(Constants.NO_VALUE_BD);
			} else {
				reportHistoryGenericDTO.setUrl(mapBasicOverview.get(Constants.HISTORY_REPORT).toString());
			}

			if (mapBasicOverview.get(Constants.BODY_TYPE).toString() == null || mapBasicOverview.get(Constants.BODY_TYPE).toString().isEmpty()) {
				bodyTypeGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				bodyTypeGenericDTO.setValue(mapBasicOverview.get(Constants.BODY_TYPE).toString().toUpperCase());
			}

			if (mapBasicOverview.get(Constants.EXT_COLOR).toString() == null || mapBasicOverview.get(Constants.EXT_COLOR).toString().isEmpty()) {
				exteriorColorGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				exteriorColorGenericDTO.setValue(mapBasicOverview.get(Constants.EXT_COLOR).toString().toUpperCase());
			}

			if (mapBasicOverview.get(Constants.INT_COLOR).toString() == null || mapBasicOverview.get(Constants.INT_COLOR).toString().isEmpty()) {
				interiorColorGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				interiorColorGenericDTO.setValue(mapBasicOverview.get(Constants.INT_COLOR).toString().toUpperCase());
			}

			if (mapBasicOverview.get(Constants.DOORS).toString() == null || mapBasicOverview.get(Constants.DOORS).toString().isEmpty()) {
				doorGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				doorGenericDTO.setValue(mapBasicOverview.get(Constants.DOORS).toString().toUpperCase());
			}

			if (mapBasicOverview.get(Constants.KEYS).toString() == null || mapBasicOverview.get(Constants.KEYS).toString().isEmpty()) {
				keyGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				keyGenericDTO.setValue(mapBasicOverview.get(Constants.KEYS).toString().toUpperCase());
			}

			if (mapBasicOverview.get(Constants.PREV_OWN).toString() == null || mapBasicOverview.get(Constants.PREV_OWN).toString().isEmpty()) {
				preOwnerGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				preOwnerGenericDTO.setValue(mapBasicOverview.get(Constants.PREV_OWN).toString().toUpperCase());
			}

			// Performance
			if (mapBasicOverview.get(Constants.ENGINE).toString() == null || mapBasicOverview.get(Constants.ENGINE).toString().isEmpty()) {
				engineGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				engineGenericDTO.setValue(mapBasicOverview.get(Constants.ENGINE).toString().toUpperCase());
			}

			if (mapBasicOverview.get(Constants.HORSE_POWER).toString() == null || mapBasicOverview.get(Constants.HORSE_POWER).toString().isEmpty()) {
				horsePowerGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				horsePowerGenericDTO.setValue(mapBasicOverview.get(Constants.HORSE_POWER).toString().toUpperCase());
			}

			if (mapBasicOverview.get(Constants.FUEL_TYPE).toString() == null || mapBasicOverview.get(Constants.FUEL_TYPE).toString().isEmpty()) {
				fueltypeGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				fueltypeGenericDTO.setValue(mapBasicOverview.get(Constants.FUEL_TYPE).toString().toUpperCase());
			}

			GenericDTO transmisionGenericDTO = new GenericDTO();
			transmisionGenericDTO.setName("TRANSMISIÓN");
			if (mapBasicOverview.get(Constants.TRANSMISION) == null || mapBasicOverview.get(Constants.TRANSMISION).toString().isEmpty()) {
				transmisionGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				transmisionGenericDTO.setValue(mapBasicOverview.get(Constants.TRANSMISION).toString().toUpperCase());
			}
			listPerformances.add(transmisionGenericDTO);

			GenericDTO tractionGenericDTO = new GenericDTO();
			tractionGenericDTO.setName("TRACCIÓN");
			if (mapBasicOverview.get(Constants.TRACTION) == null || mapBasicOverview.get(Constants.TRACTION).toString().isEmpty()) {
				tractionGenericDTO.setValue(Constants.NO_VALUE_BD);
			} else {
				tractionGenericDTO.setValue(mapBasicOverview.get(Constants.TRACTION).toString());
			}
			listPerformances.add(tractionGenericDTO);

			if (sellCarDetailBD.get().getIsNewArrival().equalsIgnoreCase("no")) {
				// Fuel Connsumption
				fuelComsuptionCity.setValue(mapBasicOverview.get(Constants.FUEL_CITY) + " KM/LT");
				fuelComsuptionFreeway.setValue(mapBasicOverview.get(Constants.FUEL_HIGHWAY) + " KM/LT");
				fuelComsuptionCombined.setValue(mapBasicOverview.get(Constants.FUEL_COMBINED) + " KM/LT");
				// Entertainment
				entertainment = mapBasicOverview.get(Constants.ENTERTAINMENT).toString();
				String[] arrayentertainment = entertainment.split(",");
				for (String entertaimentActual : arrayentertainment) {
					listEntertainment.add(entertaimentActual.toUpperCase());
				}
				// Exterior
				exterior = mapBasicOverview.get(Constants.EXTERIOR).toString();
				String[] arrayExterior = exterior.split(",");
				for (String exteriorActual : arrayExterior) {
					listExterior.add(exteriorActual.toUpperCase());
				}
				// Interior
				interior = mapBasicOverview.get(Constants.INTERIOR).toString();
				String[] arrayInterior = interior.split(",");
				for (String interiorActual : arrayInterior) {
					listInterior.add(interiorActual.toUpperCase());
				}
				// Mechanical
				mechanical = mapBasicOverview.get(Constants.MECHANICAL).toString();
				String[] arrayMechanical = mechanical.split(",");
				for (String mechanicalActual : arrayMechanical) {
					listMechanics.add(mechanicalActual.toUpperCase());
				}
				// Safety
				safety = mapBasicOverview.get(Constants.SAFETY).toString();
				String[] arraySafety = safety.split(",");
				for (String safetyActual : arraySafety) {
					listSafety.add(safetyActual.toUpperCase());
				}
			}

		} catch (SerializedPhpParserException e) {
			e.printStackTrace();
		}

		// Details
		listDetails.add(stockGenericDTO);
		
		GenericDTO genericInspectionLocation = new GenericDTO();
		
		if(sellCarDetailBD.get().getIdWarehouseLocation() != null) {
		    InspectionLocation inspectionLocation = inspectionLocationRepo.findAllActiveAndAliasNotNullAndIdWarehouseLocation(sellCarDetailBD.get().getIdWarehouseLocation());
		    int dias = 0;
		    boolean flag = false;
		    if(inspectionLocation != null) {
			String warehouseLocation = sellCarDetailBD.get().getWarehouseLocationDetail();
			Timestamp contractDate = sellCarDetailBD.get().getContractDate();
			if(contractDate != null) {
			    Date todayDate = new Date();		 
			    dias=(int) ((todayDate.getTime()-contractDate.getTime())/86400000);
			    flag = true;
			}
			if(warehouseLocation != null && !warehouseLocation.toUpperCase().equalsIgnoreCase("NULL") && (dias != 0 || flag == true)) {
			    genericInspectionLocation.setValue(inspectionLocation.getAlias() +" - "+warehouseLocation+ " ("+ dias +")");
			}else if(dias != 0 || flag == true) {
			    genericInspectionLocation.setValue(inspectionLocation.getAlias() + " ("+ dias +")");
			}else if(warehouseLocation != null && !warehouseLocation.toUpperCase().equalsIgnoreCase("NULL")){
			    genericInspectionLocation.setValue(inspectionLocation.getAlias() +" - "+warehouseLocation);
			}else{
			    genericInspectionLocation.setValue(inspectionLocation.getAlias());
			}
			genericInspectionLocation.setName(Constants.AUTO_LOCATION);		
			genericInspectionLocation.setUrl(inspectionLocation.getUrlMap());
			listDetails.add(genericInspectionLocation);
		    }
		}
		
		listDetails.add(dataSheetGenericDTO);
		listDetails.add(vinGenericDTO);
		listDetails.add(reportHistoryGenericDTO);
		listDetails.add(bodyTypeGenericDTO);
		listDetails.add(exteriorColorGenericDTO);
		listDetails.add(interiorColorGenericDTO);
		listDetails.add(doorGenericDTO);
		listDetails.add(keyGenericDTO);
		
		if(rapiGenericDTO.getValue() != null && rapiGenericDTO.getValue().trim() != "") {
		    listDetails.add(rapiGenericDTO);  
		}
		if(currentWarranty.getValue() != null && currentWarranty.getValue().trim() != "") {
		    listDetails.add(currentWarranty); 
		}
		if(numberPassengers.getValue() != null && numberPassengers.getValue().trim() != "") {
		    listDetails.add(numberPassengers);
		}


		// IssueID #1974 Eliminar información número de dueños ficha del auto (Villadiego)
		//listDetails.add(preOwnerGenericDTO);

		// Performance
		listPerformances.add(engineGenericDTO);
		listPerformances.add(horsePowerGenericDTO);
		listPerformances.add(fueltypeGenericDTO);

		// Fuel_consumption
		listFuelConsumption.add(fuelComsuptionCity);
		listFuelConsumption.add(fuelComsuptionFreeway);
		listFuelConsumption.add(fuelComsuptionCombined);

		if (sellCarDetailBD.get().getIsNewArrival().equalsIgnoreCase("yes")) {
			carFeaturesResponseDTO.setListDetails(listDetails);
			carFeaturesResponseDTO.setListPerformances(listPerformances);

			carFeaturesResponseDTO.setListFuelConsumption(new ArrayList<>());
			carFeaturesResponseDTO.setListEntertainment(new ArrayList<>());
			carFeaturesResponseDTO.setListExterior(new ArrayList<>());
			carFeaturesResponseDTO.setListInterior(new ArrayList<>());
			carFeaturesResponseDTO.setListMechanics(new ArrayList<>());
			carFeaturesResponseDTO.setListSafety(new ArrayList<>());
		} else {
			carFeaturesResponseDTO.setListDetails(listDetails);
			carFeaturesResponseDTO.setListPerformances(listPerformances);
			carFeaturesResponseDTO.setListFuelConsumption(listFuelConsumption);
			carFeaturesResponseDTO.setListEntertainment(listEntertainment);
			carFeaturesResponseDTO.setListExterior(listExterior);
			carFeaturesResponseDTO.setListInterior(listInterior);
			carFeaturesResponseDTO.setListMechanics(listMechanics);
			carFeaturesResponseDTO.setListSafety(listSafety);
		}

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(carFeaturesResponseDTO);
		return responseDTO;
	}

	/**
	 * Consulta el testimonio del usuario y el mensaje generico de Kavak de por
	 * que kavak?.
	 *
     * @param idSellcarDetail identificador del carro @Id from @SellCarDetail
     * @return GetWhyLoveResponseDTO
	 * @author Enrique Marin
	 */
	public ResponseDTO getWhyLove(Long idSellcarDetail) {
		ResponseDTO responseDTO = new ResponseDTO();
		GetWhyLoveResponseDTO getWhyLoveResponseDTO = new GetWhyLoveResponseDTO();
		Testimonials testimonialsBD = testimonialsRepo.findByIdSellCarDetail(idSellcarDetail);
		if (testimonialsBD != null) {
			if (testimonialsBD.getClientDescription() == null || testimonialsBD.getClientDescription().isEmpty()) {
				getWhyLoveResponseDTO.setOldOwnerTestimonial(Constants.NO_VALUE_BD);
			} else {
				getWhyLoveResponseDTO.setOldOwnerTestimonial(testimonialsBD.getClientDescription());
			}
			if (testimonialsBD.getIdTestimanialVideo() == null || testimonialsBD.getIdTestimanialVideo().isEmpty()) {
				getWhyLoveResponseDTO.setIdTestimanialVideo(Constants.NO_VALUE_BD);
			} else {
				getWhyLoveResponseDTO.setIdTestimanialVideo(testimonialsBD.getIdTestimanialVideo());
			}
		} else {
			getWhyLoveResponseDTO.setOldOwnerTestimonial(Constants.NO_VALUE_BD);
			getWhyLoveResponseDTO.setIdTestimanialVideo(Constants.NO_VALUE_BD);
		}
		List<MetaValue> listMetaValueWhyKavak = metaValueRepo.findByGroupCategory(Constants.META_VALUE_WHY_KAVAK);

		for (MetaValue metaValueActual : listMetaValueWhyKavak) {
			if (metaValueActual.getGroupName().equalsIgnoreCase("video")) {
				getWhyLoveResponseDTO.setIdInspectionVideo(metaValueActual.getOptionName());
			} else {
				getWhyLoveResponseDTO.setWhyCarText(metaValueActual.getOptionName());
			}
		}

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(getWhyLoveResponseDTO);
		return responseDTO;

	}

	/**
	 * Consultas el costo de envio de un auto asocido a su codigo postal
	 *
     * @param zipCode codigo postal del auto
     * @return Long
	 * @author Enrique Marin
	 */
	public ResponseDTO getShippingCost(String zipCode) {
		ResponseDTO responseDTO = new ResponseDTO();
		ZipCode zipCodeBD = zipCodeRepo.findByZipCode(Long.valueOf(zipCode));
		if (zipCodeBD == null) {
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());
			return responseDTO;
		}

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(zipCodeBD.getDTO());
		return responseDTO;
	}

	/**
	 * Consulta las secciones que indican por que es un carro Kavak
	 *
     * @param idSellCarDetail identificador del carro
	 * @return List<ProductCarCompareSectionDTO> lista secciones del por que
	 *         kavak
     * @author Enrique Marin
	 */
	@SuppressWarnings("unchecked")
	public ResponseDTO getWhyKavak(Long idSellCarDetail) {
		ResponseDTO responseDTO = new ResponseDTO();
		GetWhyKavakResponseDTO getWhyKavakResponseDTO = new GetWhyKavakResponseDTO();
		List<SectionDTO> listSectionDTO = new ArrayList<>();
		Long totalSavings = 0L;
		Long difSavingsAvg = 0L;
		Long saving = 0L;
		Long marketPrice = 0L;

		for (ProductCarCompareSection productCarCompareSectionActual : productCarCompareSectionRepo.findAll()) {
			SectionDTO newSectionDTO = new SectionDTO();
			SectionPriceDTO priceDTO = new SectionPriceDTO();
			Map<String, String> mapPricing = new HashMap<>();

			newSectionDTO.setSectionName(productCarCompareSectionActual.getName());
			newSectionDTO.setSectionDescrption(productCarCompareSectionActual.getDescription());
			newSectionDTO.setImage(Constants.URL_KAVAK + productCarCompareSectionActual.getImage());
			Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(idSellCarDetail);

			String serialized = sellCarDetailBD.get().getSellCarMeta().getMetaValue();
			try {
				// En el primer level del hashmap tiene como value otro hashmap
				// que nos referimos a el como hasmap de segundo level
				Map<String, Map<String, String>> mapFirstLvlDeserialized = (Map<String, Map<String, String>>) new SerializedPhpParser(serialized).parse();
				// Se genera un deserealizado aparte ya que en esta variable
				// esta almacenada directamente como un String
				mapPricing = mapFirstLvlDeserialized.get(Constants.PRICING);
			} catch (SerializedPhpParserException e) {
				e.printStackTrace();
			}

			if (productCarCompareSectionActual.getName().equalsIgnoreCase(Constants.AVGPRICE)) {
				Optional<PromotionCarPrice> promotionCarPriceBD = promotionCarPriceRepo.findById(idSellCarDetail);
				if (promotionCarPriceBD.isPresent()) {
					priceDTO.setKavakPrice(promotionCarPriceBD.get().getPrice().toString());
					priceDTO.setMarketPrice(mapPricing.get(Constants.MARKET_PRICE));
					difSavingsAvg = Long.valueOf(priceDTO.getMarketPrice()) - Long.valueOf(priceDTO.getKavakPrice());
					priceDTO.setSavings(difSavingsAvg.toString());
				} else {
					priceDTO.setKavakPrice(sellCarDetailBD.get().getPrice().toString());
					priceDTO.setMarketPrice(mapPricing.get(Constants.MARKET_PRICE));
					difSavingsAvg = Long.valueOf(priceDTO.getMarketPrice()) - Long.valueOf(priceDTO.getKavakPrice());
					priceDTO.setSavings(difSavingsAvg.toString());

				}

			} else if (productCarCompareSectionActual.getName().equalsIgnoreCase(Constants.WARRANTYKAVAK)) {
				priceDTO.setKavakPrice(mapPricing.get(Constants.WARRANTYKS));
				priceDTO.setMarketPrice(mapPricing.get(Constants.WARRANTYMARKET));
				difSavingsAvg = Long.valueOf(priceDTO.getMarketPrice()) - Long.valueOf(priceDTO.getKavakPrice());
				priceDTO.setSavings(difSavingsAvg.toString());
			} else {
				if (productCarCompareSectionActual.getName().equalsIgnoreCase(Constants.TRAMITES)) {
					marketPrice = Long.valueOf(productCarCompareSectionActual.getMarketPrice()) + Long.valueOf(String.valueOf(mapPricing.get(Constants.TENUREMARKET)));
					priceDTO.setKavakPrice(productCarCompareSectionActual.getKavakPrice());
					priceDTO.setMarketPrice(marketPrice.toString());
					saving = Long.valueOf(productCarCompareSectionActual.getSavings()) + Long.valueOf(String.valueOf(mapPricing.get(Constants.TENUREMARKET)));
					priceDTO.setSavings(saving.toString());
				} else {
					priceDTO.setKavakPrice(productCarCompareSectionActual.getKavakPrice());
					priceDTO.setMarketPrice(productCarCompareSectionActual.getMarketPrice());
					priceDTO.setSavings(productCarCompareSectionActual.getSavings());
				}
			}

			if (productCarCompareSectionActual.getSavings() == null || productCarCompareSectionActual.getSavings().equalsIgnoreCase(Constants.NOHAVEPRICE)) {
				if (productCarCompareSectionActual.getName().equalsIgnoreCase(Constants.AVGPRICE) || productCarCompareSectionActual.getName().equalsIgnoreCase(Constants.WARRANTYKAVAK)) {
					totalSavings = totalSavings + difSavingsAvg;
				} else {
					totalSavings = totalSavings + 0L;
				}
			} else {
				if (productCarCompareSectionActual.getName().equalsIgnoreCase(Constants.TRAMITES)) {
					totalSavings = totalSavings + Long.valueOf(productCarCompareSectionActual.getSavings()) + Long.valueOf(String.valueOf(mapPricing.get(Constants.TENUREMARKET)));
				} else {
					totalSavings = totalSavings + Long.valueOf(productCarCompareSectionActual.getSavings());
				}
			}

			newSectionDTO.setPriceDTO(priceDTO);
			listSectionDTO.add(newSectionDTO);
		}
		getWhyKavakResponseDTO.setListSectionDTO(listSectionDTO);
		getWhyKavakResponseDTO.setTotalSavings(totalSavings);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(getWhyKavakResponseDTO);
		return responseDTO;
	}

	/**
	 * Consulta los features y dimples generados en una inspeccion
	 *
     * @param idInspection idInspection identicador de la inspecccion
     * @return GetCarContentOverviewResponse
	 * @author Enrique Marin
	 */
	public ResponseDTO getCarContentOverview(Long idInspection) {
		ResponseDTO responseDTO = new ResponseDTO();

		GetCarContentOverviewResponse getCarContentOverviewResponse = new GetCarContentOverviewResponse();
		List<InspectionCarFeature> listInspectionCarFeature = inspectionCarFeatureRepo.findByIdInspection(idInspection);
		List<InspectionDimple> listInspectionDimple = inspectionDimpleRepo.findByIdInspection(idInspection);
		// List<Long> listIdInspectionCarFeature = new ArrayList<>();

		List<InspectionCarFeatureDTO> listInspectionCarFeatureDTO = new ArrayList<>();
		List<InspectionDimpleDTO> listInspectionDimpleDTO = new ArrayList<>();
		// boolean exists = users.stream().anyMatch(u -> u.getId() == 3);
		if (listInspectionCarFeature != null && !listInspectionCarFeature.isEmpty()) {
			for (InspectionCarFeature inspectionCarFeatureActual : listInspectionCarFeature) {
				// boolean exists =
				// listInspectionCarFeatureDTO.stream().anyMatch(inspectionCarFeatureDTO
				// -> inspectionCarFeatureDTO.getIdFeature() ==
				// inspectionCarFeatureActual.getDTO().getIdFeature());
				// if(!listIdInspectionCarFeature.contains(inspectionCarFeatureActual.getDTO().getIdFeature())){
				// listIdInspectionCarFeature.add(inspectionCarFeatureActual.getDTO().getIdFeature());
				if (!listInspectionCarFeatureDTO.stream().anyMatch(inspectionCarFeatureDTO -> inspectionCarFeatureDTO.getIdFeature() == inspectionCarFeatureActual.getDTO().getIdFeature())) {
					listInspectionCarFeatureDTO.add(inspectionCarFeatureActual.getDTO());
				}
			}
			getCarContentOverviewResponse.setListInspectionCarFeatureDTO(listInspectionCarFeatureDTO);
		}

		if (listInspectionDimple != null && !listInspectionDimple.isEmpty()) {
			for (InspectionDimple inspectionDimpleActual : listInspectionDimple) {
				InspectionDimpleDTO newInspectionDimpleDTO = new InspectionDimpleDTO();
				newInspectionDimpleDTO.setIdInspectionPointCar(inspectionDimpleActual.getIdInspectionPointCar());
				newInspectionDimpleDTO.setDimpleLocation(inspectionDimpleActual.getDimpleLocation());
				newInspectionDimpleDTO.setIdDimpleType(inspectionDimpleActual.getIdDimpleType());
				newInspectionDimpleDTO.setDimpleName(inspectionDimpleActual.getDimpleName());
				newInspectionDimpleDTO.setCoordinateX(inspectionDimpleActual.getCoordinateX());
				newInspectionDimpleDTO.setCoordinateY(inspectionDimpleActual.getCoordinateY());
				newInspectionDimpleDTO.setDimpleImage(Constants.URL_KAVAK + inspectionDimpleActual.getDimpleImage());
				newInspectionDimpleDTO.setOriginalWidth(inspectionDimpleActual.getOriginalWidth());
				newInspectionDimpleDTO.setOriginalHeight(inspectionDimpleActual.getOriginalHeight());
				listInspectionDimpleDTO.add(newInspectionDimpleDTO);
			}
			getCarContentOverviewResponse.setListInspectionDimpleDTO(listInspectionDimpleDTO);
		}

		if (getCarContentOverviewResponse.getListInspectionCarFeatureDTO() == null && getCarContentOverviewResponse.getListInspectionDimpleDTO() == null) {
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());
		} else {
			if (getCarContentOverviewResponse.getListInspectionCarFeatureDTO() == null) {
				getCarContentOverviewResponse.setListInspectionCarFeatureDTO(new ArrayList<>());
			}
			// else{
			// getCarContentOverviewResponse.setListInspectionDimpleDTO(new
			// ArrayList<>());
			// }

			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(getCarContentOverviewResponse);
		}
		return responseDTO;
	}

	/**
	 * Consulta todos los colores disponibles de los carros
	 *
     * @return List<CarColorDTO>
	 * @author Enrique Marin
	 */
	public ResponseDTO gerCarColors() {
		ResponseDTO responseListDTO = new ResponseDTO();

		List<CarColor> listCarColor = carColorRepo.findAllByActiveTrueOrderByPosition();
		List<CarColorDTO> listCarColorDTO = new ArrayList<>();
		for (CarColor carColorActual : listCarColor) {
			listCarColorDTO.add(carColorActual.getDTO());
		}

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseListDTO.setCode(enumResult.getValue());
		responseListDTO.setStatus(enumResult.getStatus());
		responseListDTO.setData(listCarColorDTO);
		return responseListDTO;
	}

	/**
	 * Consulta el resporte de una inspeccion
	 *
     * @param idSellCarDetail id del carro a consultar
     * @return GetInspectionReportResponseDTO
	 * @author Enrique Marin
	 */
	public ResponseDTO getInspectionReport(Long idSellCarDetail) {
		ResponseDTO responseDTO = new ResponseDTO();
		GetInspectionReportResponseDTO getInspectionReportResponseDTO = new GetInspectionReportResponseDTO();

		List<InspectionPointDataDTO> listInspectionPointDataDTO = new ArrayList<>();
		List<InspectionPointCarHistoryDataDTO> listInspectionPointCarHistoryDataDTO = new ArrayList<>();

		List<ApoloInspectionPointCar> listApoloInspectionPointCarBD = apoloInspectionPointCarRepo.findByIdSellCarDetail(idSellCarDetail);
		HashMap<String, List<GenericDTO>> mapInspectionPoint = new HashMap<>();
		if (listApoloInspectionPointCarBD != null && !listApoloInspectionPointCarBD.isEmpty()) {
			for (ApoloInspectionPointCar apoloInspectionPointCarActual : listApoloInspectionPointCarBD) {
				GenericDTO inspectionPointGenericDTO = new GenericDTO();
				inspectionPointGenericDTO.setName(apoloInspectionPointCarActual.getApoloInspectionPointsItem().getName().replace("<i>", "").replace("</i>", "").replace("<I>", "").replace("</I>", ""));
				inspectionPointGenericDTO.setChecked(apoloInspectionPointCarActual.isStatus());
				if (mapInspectionPoint.get(apoloInspectionPointCarActual.getApoloInspectionPointsItem().getApoloInspectionPointGroup().getName()) == null) {
					List<GenericDTO> listApoloInspectionPointCar = new ArrayList<>();
					listApoloInspectionPointCar.add(inspectionPointGenericDTO);
					mapInspectionPoint.put(apoloInspectionPointCarActual.getApoloInspectionPointsItem().getApoloInspectionPointGroup().getName(), listApoloInspectionPointCar);
				} else {
					mapInspectionPoint.get(apoloInspectionPointCarActual.getApoloInspectionPointsItem().getApoloInspectionPointGroup().getName()).add(inspectionPointGenericDTO);
				}
			}

			for (String categoryNameActual : mapInspectionPoint.keySet()) {
				InspectionPointDataDTO newInspectionPointDataDTO = new InspectionPointDataDTO();
				newInspectionPointDataDTO.setNameCategory(categoryNameActual);
				List<GenericDTO> listApoloInspectionPointCar = new ArrayList<>();
				for (GenericDTO inspectionPointGenericActual : mapInspectionPoint.get(categoryNameActual)) {
					listApoloInspectionPointCar.add(inspectionPointGenericActual);
				}
				newInspectionPointDataDTO.setListInpectionPointDataGenericDTO(listApoloInspectionPointCar);
				listInspectionPointDataDTO.add(newInspectionPointDataDTO);
			}
			getInspectionReportResponseDTO.setListInspectionPointDataDTO(listInspectionPointDataDTO);
		} else {
			getInspectionReportResponseDTO.setListInspectionPointDataDTO(null);
		}

		List<ApoloInspectionPointsHistoryCar> listApoloInspectionPointsHistoryCarBD = apoloInspectionPointsHistoryCarRepo.findByIdSellCarDetail(idSellCarDetail);
		if (listApoloInspectionPointsHistoryCarBD != null) {
			for (ApoloInspectionPointsHistoryCar apoloInspectionPointsHistoryCarActual : listApoloInspectionPointsHistoryCarBD) {
				InspectionPointCarHistoryDataDTO inspectionPointCarHistoryDataDTO = new InspectionPointCarHistoryDataDTO();
				inspectionPointCarHistoryDataDTO.setName(apoloInspectionPointsHistoryCarActual.getApoloInspectionPointsHistory().getName());
				inspectionPointCarHistoryDataDTO.setChecked(apoloInspectionPointsHistoryCarActual.isStatus());
				listInspectionPointCarHistoryDataDTO.add(inspectionPointCarHistoryDataDTO);
			}
			getInspectionReportResponseDTO.setListInspectionPointCarHistoryDataDTO(listInspectionPointCarHistoryDataDTO);
		} else {
			getInspectionReportResponseDTO.setListInspectionPointCarHistoryDataDTO(null);
		}

        List<DimpleDataDetailDTO> listDimpleExtDataDetailDTO = new ArrayList<>();
		List<SellCarDimple> listSellCarDimpleBD = sellCarDimpleRepo.findByIdSellCarDetailOrderByDimpleType(idSellCarDetail);
		;
		List<DimpleDataDetailDTO> listDimpleIntDataDetailDTO = new ArrayList<>();
		;
		DimpleDataDTO newDimpleDataDTO = new DimpleDataDTO();
		if ((listSellCarDimpleBD != null) && !listSellCarDimpleBD.isEmpty()) {
			for (SellCarDimple sellCarDimpleActual : listSellCarDimpleBD) {
				DimpleDataDetailDTO dimpleDataDetailDTO = new DimpleDataDetailDTO();

                CharSequence http = "http";
                CharSequence https = "https";

                if (sellCarDimpleActual.getDimpleImageUrl().contains(http) || sellCarDimpleActual.getDimpleImageUrl().contains(https)) {
                        dimpleDataDetailDTO.setUrl(sellCarDimpleActual.getDimpleImageUrl());
                } else {
				dimpleDataDetailDTO.setUrl(Constants.URL_KAVAK + sellCarDimpleActual.getDimpleImageUrl());
                }
				dimpleDataDetailDTO.setDetailText(sellCarDimpleActual.getDimpleName());
				CoordinateDimpleDTO newCoordinateDimpleDTO = new CoordinateDimpleDTO();
				newCoordinateDimpleDTO.setTop(sellCarDimpleActual.getCoordanateY().toString());
				newCoordinateDimpleDTO.setLeft(sellCarDimpleActual.getCoordanateX().toString());
				dimpleDataDetailDTO.setCoordinateDimpleDTO(newCoordinateDimpleDTO);
				if (sellCarDimpleActual.getDimpleType().equalsIgnoreCase(Constants.EXT)) {
					listDimpleExtDataDetailDTO.add(dimpleDataDetailDTO);
				} else {
					listDimpleIntDataDetailDTO.add(dimpleDataDetailDTO);
				}

			}
			newDimpleDataDTO.setListDimpleExtDataDetailDTO(listDimpleExtDataDetailDTO);
			newDimpleDataDTO.setListDimpleIntDataDetailDTO(listDimpleIntDataDetailDTO);
			getInspectionReportResponseDTO.setDimplesData(newDimpleDataDTO);
		}

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(getInspectionReportResponseDTO);
		return responseDTO;
	}

    /**
     * Metodo que registra url de imagenes a un carro en SellcarDetail
     *
     * @param urlImages Lista de url de imagenes
     * @param carId     Identificador SellCarDetail
     * @return responseDTO Response Generico de Servicio
     * @author Oscar Montilla
     */
    public ResponseDTO postImages(String carId, List<String> urlImages) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();

        try {

            if (carId == null || urlImages == null) {

                LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
                MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (carId == null ? "car_id, " : "")
                        + (urlImages == null ? "List<url_images>, " : ""));
                listMessageDTO.add(messageDataNull);
                responseDTO.setListMessage(listMessageDTO);
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData(new GenericDTO());

                return responseDTO;
            }

            Optional<SellCarDetail> sellCarDetailDB = sellCarDetailRepo.findById(Long.parseLong(carId));

            if (!sellCarDetailDB.isPresent()) {
                LogService.logger.info("El registro de SellCarDetail no existe");
                MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0009.toString()).getDTO();
                listMessageDTO.add(messageDataNull);
                responseDTO.setListMessage(listMessageDTO);
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData(new GenericDTO());

                return responseDTO;
            }

            if (urlImages.size() < 5) {

                LogService.logger.info("Esta suministrado por debajo del minimo de imagenes..");
                MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0082.toString()).getRemplaceParameterDTO(Integer.toString(urlImages.size()));
                listMessageDTO.add(messageDataNull);
                responseDTO.setListMessage(listMessageDTO);
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData(new GenericDTO());

                return responseDTO;

            }

            String imagesSave = null;

            for (Iterator<String> images = urlImages.iterator(); images.hasNext(); ) {
                String image = images.next();

                if (KavakUtils.URLvalidador(image)) {

                    if (imagesSave == null) {
                        imagesSave = image;
                    } else {
                        imagesSave += "#" + image;
                    }

                } else {

                    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0083.toString()).getRemplaceParameterDTO(image);
                    listMessageDTO.add(messageDataNull);
                    responseDTO.setListMessage(listMessageDTO);
                    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                    responseDTO.setCode(enumResult.getValue());
                    responseDTO.setStatus(enumResult.getStatus());
                    responseDTO.setData(new GenericDTO());

                    return responseDTO;
                }

            }
            sellCarDetailDB.get().setImageCar(imagesSave);

            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        } catch (NumberFormatException nfe) {

            LogService.logger.info("El valor suministrado no es valor correcto...");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0024.toString())
                    .getRemplaceParameterTypeValueDTO("car_id", Number.class.getSimpleName());
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }
    }

    public ResponseDTO postDimplesCar(String carId, PostDimplesCarRequestDTO postDimplesCarRequestDTO) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();

        try {

            if (carId == null || postDimplesCarRequestDTO.getMoment() == null) {

                LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
                MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (carId == null ? "car_id, " : "")
                        + (postDimplesCarRequestDTO.getMoment() == null ? "moment, " : ""));
                listMessageDTO.add(messageDataNull);
                responseDTO.setListMessage(listMessageDTO);
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData(new GenericDTO());

                return responseDTO;
            }

            Optional<SellCarDetail> sellCarDetailDB = sellCarDetailRepo.findById(Long.parseLong(carId));

            if (!sellCarDetailDB.isPresent()) {
                LogService.logger.info("El registro de SellCarDetail no existe..");
                MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0009.toString()).getDTO();
                listMessageDTO.add(messageDataNull);
                responseDTO.setListMessage(listMessageDTO);
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData(new GenericDTO());

                return responseDTO;
            }

            List<SellCarDimple> sellCarDimples = new ArrayList<>();

            for (DimplesDTO dimple : postDimplesCarRequestDTO.getDimplesDTOS()) {

                DimplesTypeEnum type = DimplesTypeEnum.getByName(dimple.getType() == null || dimple.getType().length() == 0 ? DimplesTypeEnum.NOT_EQUAL.getName() : dimple.getType());

                if (dimple.getName() == null || dimple.getName().length() == 0 || dimple.getPositionX() == null || dimple.getPositionX().length() == 0
                        || dimple.getPositionY() == null || dimple.getPositionY().length() == 0 || dimple.getUrl() == null || dimple.getUrl().length() == 0
                        || type == DimplesTypeEnum.NOT_EQUAL) {

                    LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente...");
                    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" dimples [" + (dimple.getName() == null || dimple.getName().length() == 0 ? " name," : "")
                            + (dimple.getPositionX() == null || dimple.getPositionX().length() == 0 ? " position_x," : "") + (dimple.getPositionY() == null || dimple.getPositionY().length() == 0 ? " position_y," : "")
                            + (dimple.getUrl() == null || dimple.getUrl().length() == 0 ? " url," : "") + (type == DimplesTypeEnum.NOT_EQUAL ? " type: exterior or interior," : "") + "], ");
                    listMessageDTO.add(messageDataNull);
                    responseDTO.setListMessage(listMessageDTO);
                    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                    responseDTO.setCode(enumResult.getValue());
                    responseDTO.setStatus(enumResult.getStatus());
                    responseDTO.setData(new GenericDTO());

                    return responseDTO;

                }

                SellCarDimple sellCarDimple = new SellCarDimple();

                sellCarDimple.setDimpleName(dimple.getName());
                sellCarDimple.setCoordanateX(dimple.getPositionX());
                sellCarDimple.setCoordanateY(dimple.getPositionY());
                sellCarDimple.setSellCarDetail(sellCarDetailDB.get());
                sellCarDimple.setMoment(postDimplesCarRequestDTO.getMoment());
                sellCarDimple.setDimpleType(type.getOpcion());


                if (KavakUtils.URLvalidador(dimple.getUrl())) {

                    sellCarDimple.setDimpleImageUrl(dimple.getUrl());

                } else {

                    LogService.logger.info("la url Suministrada no es valida..");
                    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0083.toString()).getRemplaceParameterDTO(dimple.getUrl());
                    listMessageDTO.add(messageDataNull);
                    responseDTO.setListMessage(listMessageDTO);
                    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                    responseDTO.setCode(enumResult.getValue());
                    responseDTO.setStatus(enumResult.getStatus());
                    responseDTO.setData(new GenericDTO());

                    return responseDTO;
                }

                sellCarDimples.add(sellCarDimple);
            }

            List<SellCarDimple> sellCarDimpleDeleteDB = sellCarDimpleRepo.findBySellCarDetailAndMoment(sellCarDetailDB.get(), postDimplesCarRequestDTO.getMoment());

            sellCarDimpleRepo.deleteAll(sellCarDimpleDeleteDB);
            sellCarDimpleRepo.saveAll(sellCarDimples);


            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;


        } catch (NumberFormatException nfe) {

            LogService.logger.info("El valor suministrado no es valor correcto....");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0024.toString())
                    .getRemplaceParameterTypeValueDTO("car_id", Number.class.getSimpleName());
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }
    }
}
