package com.kavak.core.service.netsuite;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.kavak.core.dto.InspectionLocationDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.InspectionLocation;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.LogService;

@Stateless
public class LocationKavakCarServices {

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;
    
    @Inject
    private InspectionLocationRepository inspectionLocationRepo;
    
    /**
     * Obtiene la locación de un auto según su stockId
     * @author Antony Delgado
     * @return List<InspectionLocationDTO>
     *
     */

    public ResponseDTO getLocationKavakCar(Long stockId) {
    	InspectionLocationDTO inspectionLocationDTO = null;
    	ResponseDTO responseDTO = null;
    	int result = 1;
    	
    	try {
    		
    		if (stockId != null){
    			LogService.logger.info("Stock ID a buscar: " + stockId);
    			
        		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(stockId);
        		
        		if (sellCarDetailBD.isPresent()){
        			responseDTO = new ResponseDTO();
        			
        			LogService.logger.info("Se procede a buscar el Location: " + sellCarDetailBD.get().getIdWarehouseLocation() + " del stock ID: " + stockId);
        			
        			if (sellCarDetailBD.get().getIdWarehouseLocation() == null){
    	    			LogService.logger.info("No se encontro el location del Stock ID " + stockId);
    	    			result = 0;
        			}else if (sellCarDetailBD.get().getIdWarehouseLocation() == 0){
    	    			LogService.logger.info("El Location del auto " + stockId + " es 0");
    	    			result = 0;
        			}else{
        				Optional<InspectionLocation> inspectionLocationBD = inspectionLocationRepo.findById(sellCarDetailBD.get().getIdWarehouseLocation());
        				
        				if (inspectionLocationBD.isPresent()){
        					inspectionLocationDTO = new InspectionLocationDTO();
        					inspectionLocationDTO = inspectionLocationBD.get().getDTO();
        				}else{
        	    			LogService.logger.info("No se encontro el location del Stock ID " + stockId);
        	    			result = 0;
        				}
        			}
        		}
        		
        		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                if (result == 0){
                	responseDTO.setData(0);
                }else{
                	responseDTO.setData(inspectionLocationDTO);
                }
                
                LogService.logger.info(Constants.LOG_EXECUTING_END + " [getFinancingFormValues] " + Calendar.getInstance().getTime());
    		}else{
    			LogService.logger.info("El stock id enviado es null, se descarta el llamado a este servicio de location kavak car.");
    		}

            
		} catch (Exception e) {
			LogService.logger.error("Error al consultar el location de un auto kavak, " + e);

    		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0500.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(null);
            LogService.logger.info(Constants.LOG_EXECUTING_END + " [getFinancingFormValues] " + Calendar.getInstance().getTime());
		}

    	return responseDTO;
    }
    
    
    public ResponseDTO putLocationKavakCar(String stockIds, Long location, String locationDetail){
    	ResponseDTO responseDTO = new ResponseDTO();
    	
    	try {
			
    		if (!stockIds.equals("null")){
    			List<Long> listStockId = new ArrayList<Long>();
    			
    			for (String s : stockIds.split(",")){
    				listStockId.add(Long.parseLong(s));
    			}
    			
    			List<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findByIds(listStockId);
    			
    			if (sellCarDetailBD != null){
    				
    				LogService.logger.debug("sellCarDetailBD TOTAL" + sellCarDetailBD.size());
    				
    				for(SellCarDetail kavakCar : sellCarDetailBD){
    					
    					// SET LOCATION
    					if (location == 0){
    						kavakCar.setIdWarehouseLocation(0L);
    					}else{
    						Optional<InspectionLocation> inspectionLocationBD = inspectionLocationRepo.findById(location);
    						if (inspectionLocationBD.isPresent()){
    							kavakCar.setIdWarehouseLocation(location);
    						}else{
    							LogService.logger.info("El Location ID " + location + " no existe, se descarta llamado del servicio de actualizacion de location");
    						}
    					}
    					
    					// SET LOCATION DETAIL (COORDENATE)
						if(locationDetail != null && locationDetail.trim() != "" && locationDetail!= "null"){
							kavakCar.setWarehouseLocationDetail(locationDetail.toUpperCase());
						}else{
							kavakCar.setWarehouseLocationDetail(null);
						}
    					
    					sellCarDetailRepo.save(kavakCar);
    					
    	        		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
    	                responseDTO.setCode(enumResult.getValue());
    	                responseDTO.setStatus(enumResult.getStatus());
    				}
    				
    			}else{
            		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
                    responseDTO.setCode(enumResult.getValue());
                    responseDTO.setStatus(enumResult.getStatus());
                    responseDTO.setData("El stock ID " + stockIds + " no existe, se descarta llamado del servicio de actualizacion de location");
    				LogService.logger.info("El stock ID " + stockIds + " no existe, se descarta llamado del servicio de actualizacion de location");
    			}
    		}else{
        		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("Sin sotck ID no se puede actualizar su location, se descarta llamado del servicio");
    			LogService.logger.info("Sin sotck ID no se puede actualizar su location, se descarta llamado del servicio");
    		}
    		
		} catch (Exception e) {
			LogService.logger.error("Error al actualizar el location de los stockIds: " + stockIds , e);

    		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0500.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(null);
            LogService.logger.info(Constants.LOG_EXECUTING_END + " [getFinancingFormValues] " + Calendar.getInstance().getTime());
		}
    	
    	
    	return responseDTO;
    }
}
