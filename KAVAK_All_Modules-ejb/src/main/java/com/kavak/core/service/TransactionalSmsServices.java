package com.kavak.core.service;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;

import com.kavak.core.dto.PostMessageRequestDTO;
import com.kavak.core.dto.response.ApiUrlShortenerResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.model.AppointmentSchedule;
import com.kavak.core.model.CustomerNotifications;
import com.kavak.core.model.InspectionLocation;
import com.kavak.core.model.OfferCheckpoint;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.AppointmentScheduleRepository;
import com.kavak.core.repositories.CustomerNotificationsRepository;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.OfferCheckpointRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;

@Stateless
public class TransactionalSmsServices {

    @Inject
    private SaleCheckpointRepository saleCheckpointRepo;

    @Inject
    private CustomerNotificationsRepository customerNotificationsRepo;

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;

    @Inject
    private AppointmentScheduleRepository appointmentScheduleRepo;

    @Inject
    private InspectionLocationRepository inspectionLocationRepo;
    
    @Inject
    private OfferCheckpointRepository offerCheckpointRepo;
    
    @Inject
    private UserRepository userRepo;


    @EJB(mappedName = LookUpNames.SESSION_CommonServices)
    CommonServices commonServices;



    /**
     * SMS : Reserva Servicios (cuando se realiza exitosamente una reserva)
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO bookedCarSms() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findBookedCarSms();
	    if (listSaleCheckpointBD.isEmpty()) {
		return responseDTO;
	    }
	    for (SaleCheckpoint saleCheckpointActual : listSaleCheckpointBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : saleCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null) {
			Optional<SellCarDetail> sellcarDetailBD = sellCarDetailRepo.findById(saleCheckpointActual.getCarId());
			PostMessageRequestDTO postMessageRequestDTO = new PostMessageRequestDTO();
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(saleCheckpointActual.getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postMessageRequestDTO.setMessage("Gracias por apartar nuestro "+sellcarDetailBD.get().getCarMake() +" "+sellcarDetailBD.get().getCarModel()+" "+sellcarDetailBD.get().getCarYear()+". Muy pronto uno de nuestros expertos se comunicara para seguir apoyandote. Tus 72hr con KAVAK comienzan ahora");
			commonServices.postMessage(postMessageRequestDTO);      
		    } 
		} catch (Exception e) {
		    saleCheckpointActual.setBookedCarSmsSent(true);
		    saleCheckpointRepo.save(saleCheckpointActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS bookedCarSms: [" + saleCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
		saleCheckpointActual.setBookedCarSmsSent(true);
		saleCheckpointRepo.save(saleCheckpointActual);
	    }
	    LogService.logger.info("Total SMS Enviados de bookedCarSms: [" + listSaleCheckpointBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de bookedCarSms");
	    e.printStackTrace();
	}
	return responseDTO;
    }



    /**
     * SMS : Notificación de auto reservado (Registrar en tabla de SMS postCarNotification, cuando el notification_type = 'AUTO_RESERVADO')
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO carNotificationBookedSms() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<CustomerNotifications> listCustomerNotificationsBD = customerNotificationsRepo.findCarNotificationBookedSms();
	    if (listCustomerNotificationsBD.isEmpty()) {
		return responseDTO;
	    }

	    for (CustomerNotifications customerNotificationsActual : listCustomerNotificationsBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : customerNotificationsActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null) {
			Optional<SellCarDetail> sellcarDetailBD = sellCarDetailRepo.findById(customerNotificationsActual.getCarId());
			PostMessageRequestDTO postMessageRequestDTO = new PostMessageRequestDTO();
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(customerNotificationsActual.getUser().getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postMessageRequestDTO.setMessage("Gracias por registrarte para ser notificado si nuestro auto "+sellcarDetailBD.get().getCarMake() +" "+sellcarDetailBD.get().getCarModel()+" "+sellcarDetailBD.get().getCarYear()+" se libera. Te recuerdo nuestros apartados duran max 8 dias naturales, esta atento. Saludos KAVAK");
			commonServices.postMessage(postMessageRequestDTO);    
		    } 
		} catch (Exception e) {
		    customerNotificationsActual.setCarNotificationSmsSent(true);
		    customerNotificationsRepo.save(customerNotificationsActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS carNotificationBookedSms: [" + customerNotificationsActual.getId() + "]");
		    e.printStackTrace();
		}
		customerNotificationsActual.setCarNotificationSmsSent(true);
		customerNotificationsRepo.save(customerNotificationsActual);
	    }
	    LogService.logger.info("Total SMS Enviados de carNotificationBookedSms: [" + listCustomerNotificationsBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de carNotificationBookedSms");
	    e.printStackTrace();
	}
	return responseDTO;
    }



    /**
     * SMS : Reserva Cancelada (Al cancelarse una reserva)
     * 
     * @author Juan Tolentino 
     **/
    public ResponseDTO cancelledBookedCarSms() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	ApiUrlShortenerResponseDTO apiUrlShortenerResponseDTO = new ApiUrlShortenerResponseDTO();
	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<CustomerNotifications> listCustomerNotificationsBD = customerNotificationsRepo.findCancelledBookedCarSms();
	    if (listCustomerNotificationsBD.isEmpty()) {
		return responseDTO;
	    }

	    for (CustomerNotifications customerNotificationsActual : listCustomerNotificationsBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : customerNotificationsActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null) {
			Optional<SellCarDetail> sellcarDetailBD = sellCarDetailRepo.findById(customerNotificationsActual.getCarId());
			if(sellcarDetailBD.get().getShortUrl() == null) {
			    String url = KavakUtils.getFriendlyCarUrl(sellcarDetailBD.get());
			    apiUrlShortenerResponseDTO = commonServices.postApiUrlShortener(url);
			    sellcarDetailBD.get().setShortUrl(apiUrlShortenerResponseDTO.getData().getUrl());
			    sellCarDetailRepo.save(sellcarDetailBD.get());
			}
			PostMessageRequestDTO postMessageRequestDTO = new PostMessageRequestDTO();
			String patternNumber = "$###,###,###";
			String patternNumberKM = "###,###,###";
			NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
			DecimalFormat decimalFormat = (DecimalFormat) nf;
			DecimalFormat decimalFormatKM = (DecimalFormat) nf;
			decimalFormat.applyPattern(patternNumber);
			decimalFormatKM.applyPattern(patternNumberKM);
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(customerNotificationsActual.getUser().getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			if(apiUrlShortenerResponseDTO.getData() != null && apiUrlShortenerResponseDTO.getData().getUrl() != null) {
			    postMessageRequestDTO.setMessage("El auto "+ sellcarDetailBD.get().getCarMake() +" "+sellcarDetailBD.get().getCarModel()+" "+sellcarDetailBD.get().getCarYear()+" que buscabas en KAVAK acaba de ser liberado. Ingresa en "+ apiUrlShortenerResponseDTO.getData().getUrl() +" y aparta con "+decimalFormat.format(5000L)+" MXN, no dejes que te lo ganen. Gracias por confiar en KAVAK");
			}else if(sellcarDetailBD.get().getShortUrl() != null){
			    postMessageRequestDTO.setMessage("El auto "+ sellcarDetailBD.get().getCarMake() +" "+sellcarDetailBD.get().getCarModel()+" "+sellcarDetailBD.get().getCarYear()+" que buscabas en KAVAK acaba de ser liberado. Ingresa en "+sellcarDetailBD.get().getShortUrl()+" y aparta con "+decimalFormat.format(5000L)+" MXN, no dejes que te lo ganen. Gracias por confiar en KAVAK");
			}else {
			    postMessageRequestDTO.setMessage("El auto "+ sellcarDetailBD.get().getCarMake() +" "+sellcarDetailBD.get().getCarModel()+" "+sellcarDetailBD.get().getCarYear()+" que buscabas en KAVAK acaba de ser liberado. Ingresa en www.kavak.com y aparta con "+decimalFormat.format(5000L)+" MXN, no dejes que te lo ganen. Gracias por confiar en KAVAK");
			}
			commonServices.postMessage(postMessageRequestDTO);     
		    } 
		    customerNotificationsActual.setCancelledCarBookedSmsSent(true);
		    customerNotificationsRepo.save(customerNotificationsActual);
		} catch (Exception e) {
		    customerNotificationsActual.setCancelledCarBookedSmsSent(true);
		    customerNotificationsRepo.save(customerNotificationsActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS cancelledBookedCarSms: [" + customerNotificationsActual.getId() + "]");
		    e.printStackTrace();
		}
	    }
	    LogService.logger.info("Total SMS Enviados de cancelledBookedCarSms: [" + listCustomerNotificationsBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de cancelledBookedCarSms");
	    e.printStackTrace();
	}
	return responseDTO;
    }


    /**
     * SMS : Envio SMS Cita (Generar registro en tabla de SMS cuando se agenda la cita)
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO scheduledAppointment() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat newSdfFormat = new SimpleDateFormat("dd/MM/yyyy");
	Timestamp appointmentDate = new Timestamp(System.currentTimeMillis());

	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findScheduledAppointmentSms();
	    if (listSaleCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    for (SaleCheckpoint saleCheckpointActual : listSaleCheckpointBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : saleCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null) {
			//Primer mensaje
			Optional<AppointmentSchedule> appointmentScheduleBD = appointmentScheduleRepo.findById(saleCheckpointActual.getScheduleDateId());
			InspectionLocation inspectionLocationBD = new InspectionLocation();
			if(appointmentScheduleBD.get().getAppointmentLocationId() != null) {
			    inspectionLocationBD = inspectionLocationRepo.findByIdLocation(appointmentScheduleBD.get().getAppointmentLocationId());			     
			}
			PostMessageRequestDTO postMessageRequestDTO = new PostMessageRequestDTO();
			Date dateParseAppointment = sdf.parse(appointmentScheduleBD.get().getAppoinmentDate().toString());
			String appoinmentDateString = newSdfFormat.format(dateParseAppointment);
			appointmentDate.setTime(dateParseAppointment.getTime());
			String patternNumber = "$###,###,###";
			String patternNumberKM = "###,###,###";

			NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
			DecimalFormat decimalFormat = (DecimalFormat) nf;
			DecimalFormat decimalFormatKM = (DecimalFormat) nf;
			decimalFormat.applyPattern(patternNumber);
			decimalFormatKM.applyPattern(patternNumberKM);
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(saleCheckpointActual.getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			//postOAMessageRequestDTO.setMessage("Hola, confirmo tu cita el día "+ appoinmentDateString +" en KAVAK.com. Te recuerdo el apartado de nuestros autos es con 5,000 MXN y por 72hr.");
			postMessageRequestDTO.setMessage("Hola, confirmo tu cita el dia "+ appoinmentDateString +" en KAVAK.com a las "+ appointmentScheduleBD.get().getStartDate()+". Te recuerdo, puedes apartar el auto por 72 hrs con 5,000 MXN a traves de nuestra pagina o en el almacen");
			commonServices.postMessage(postMessageRequestDTO);     
			saleCheckpointActual.setScheduledAppointmentSmsSent(true);
			saleCheckpointRepo.save(saleCheckpointActual);	

			//Segundo mensaje
			postMessageRequestDTO = new PostMessageRequestDTO();
			decimalFormat.applyPattern(patternNumber);
			decimalFormatKM.applyPattern(patternNumberKM);
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453");
			postMessageRequestDTO.setEmail(saleCheckpointActual.getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			if(inspectionLocationBD.getId() != null) {
			    if(inspectionLocationBD.getId().equals(Constants.INSPECTION_LOCATION_LERMA)) {
				//postOAMessageRequestDTO.setMessage("Te mandamos la ubicación de nuestro almacén "+inspectionLocationBD.getShortUrlMap()+" y un video referencial para que nos ubiques mas facil "+ inspectionLocationBD.getShortUrlVideo()+". Te esperamos!");
				postMessageRequestDTO.setMessage("Te enviamos nuestra ubicacion "+inspectionLocationBD.getShortUrlMap()+" y un video para que llegues mas facil "+ inspectionLocationBD.getShortUrlVideo()+". Si puedes apartalo para que no te lo ganen!");
			    }else {
				postMessageRequestDTO.setMessage("Te mandamos la ubicacion de la cita para que sea mas sencillo ubicarnos "+inspectionLocationBD.getShortUrlMap()+". Te esperamos!");
				//postOAMessageRequestDTO.setMessage("Te enviamos nuestra ubicación "+inspectionLocationBD.getShortUrlMap()+". Si puedes resérvalo para que no te lo ganen!");
			    }
			    commonServices.postMessage(postMessageRequestDTO);    
			}

		    }  
		    saleCheckpointActual.setScheduledAppointmentSmsSent(true);
		    saleCheckpointRepo.save(saleCheckpointActual);
		} catch (Exception e) {
		    saleCheckpointActual.setScheduledAppointmentSmsSent(true);
		    saleCheckpointRepo.save(saleCheckpointActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS scheduledAppointment: [" + saleCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }
	    LogService.logger.info("Total SMS Enviados de scheduledAppointment: [" + listSaleCheckpointBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de scheduledAppointment");
	    e.printStackTrace();
	}
	return responseDTO;
    }


    
    /**
     * SMS : SMS C-204 Etapa 4 y 5, Selecciona día, hora y lugar (Casa cliente)
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO selectDayAndHourAndPlaceCustomer() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat newSdfFormat = new SimpleDateFormat("dd/MM");
	Timestamp appointmentDate = new Timestamp(System.currentTimeMillis());	
	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findSelectDayAndHourAndPlaceCustomer();
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }
	    for (OfferCheckpoint offerCheckpointActual : listOfferCheckpointBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : offerCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null) {
			Date dateParseAppointment = sdf.parse(offerCheckpointActual.getInspectionDate().toString());
			String appoinmentDateString = newSdfFormat.format(dateParseAppointment);
			appointmentDate.setTime(dateParseAppointment.getTime());
			PostMessageRequestDTO postMessageRequestDTO = new PostMessageRequestDTO();
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(offerCheckpointActual.getUser().getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postMessageRequestDTO.setMessage("Gracias por agendar una inspeccion con KAVAK el dia "+ appoinmentDateString +". Para confirmarla recibiras una llamada en las prox 24h habiles, de lo contrario por favor contactanos");
			commonServices.postMessage(postMessageRequestDTO);     
			
			//Segundo mensaje
			postMessageRequestDTO = new PostMessageRequestDTO();
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(offerCheckpointActual.getUser().getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postMessageRequestDTO.setMessage("Si vendes con nosotros obtienes un descuento entre 1-2% en la compra de cualquiera de nuestros autos. Ve nuestro catalogo "+Constants.URL_SHORTENER_CATALOGUE);
			commonServices.postMessage(postMessageRequestDTO);   
		    }  
		    offerCheckpointActual.setSelectDayPlaceCustomerSmsSent(true);
		    offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    //offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		} catch (Exception e) {
		    offerCheckpointActual.setSelectDayPlaceCustomerSmsSent(true);
		    offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    //offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS selectDayAndPlaceCustomer: [" + offerCheckpointActual.getId() + "]");
		    e.printStackTrace();		    
		}
	    }
	    LogService.logger.info("Total SMS Enviados de selectDayAndPlaceCustomer: [" + listOfferCheckpointBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de selectDayAndPlaceCustomer");
	    e.printStackTrace();
	}
	return responseDTO;
    }
    
    
    
    /**
     * SMS : SMS C-204 Etapa 4 y 5, Selecciona día, hora y lugar (Centro)
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO selectDayAndHourAndPlaceCenter() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat newSdfFormat = new SimpleDateFormat("dd/MM");
	Timestamp appointmentDate = new Timestamp(System.currentTimeMillis());	
	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findSelectDayAndHourAndPlaceCenter();
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }
	    for (OfferCheckpoint offerCheckpointActual : listOfferCheckpointBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : offerCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null) {
			Date dateParseAppointment = sdf.parse(offerCheckpointActual.getInspectionDate().toString());
			String appoinmentDateString = newSdfFormat.format(dateParseAppointment);
			appointmentDate.setTime(dateParseAppointment.getTime());
			PostMessageRequestDTO postMessageRequestDTO = new PostMessageRequestDTO();
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(offerCheckpointActual.getUser().getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postMessageRequestDTO.setMessage("Gracias por agendar una inspeccion con KAVAK el dia "+ appoinmentDateString +". Garantiza tener todos los documentos ese dia. !Ven preparado para vender!");
			commonServices.postMessage(postMessageRequestDTO);     
			
			//Segundo mensaje
			postMessageRequestDTO = new PostMessageRequestDTO();
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(offerCheckpointActual.getUser().getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postMessageRequestDTO.setMessage("Si vendes con nosotros obtienes un descuento entre 1-2% en la compra de cualquiera de nuestros autos. Ve nuestro catalogo "+Constants.URL_SHORTENER_CATALOGUE);
			commonServices.postMessage(postMessageRequestDTO);    
		    } 
		    offerCheckpointActual.setSelectDayPlaceCenterSmsSent(true);
		    offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    //offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		} catch (Exception e) {
		    offerCheckpointActual.setSelectDayPlaceCenterSmsSent(true);
		    offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    //offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS selectDayAndHourAndPlaceCenter: [" + offerCheckpointActual.getId() + "]");
		    e.printStackTrace();		    
		}
	    }
	    LogService.logger.info("Total SMS Enviados de selectDayAndHourAndPlaceCenter: [" + listOfferCheckpointBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de selectDayAndHourAndPlaceCenter");
	    e.printStackTrace();
	}
	return responseDTO;
    }
    
    
    
    
    /**
     * SMS : SMS C-205 Se registra una oferta de auditoría (se libera oferta de auditoria)
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO releaseAuditOffer() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	ApiUrlShortenerResponseDTO apiUrlShortenerResponseDTO = new ApiUrlShortenerResponseDTO();
	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findReleaseAuditOffer();
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }
	    for (OfferCheckpoint offerCheckpointActual : listOfferCheckpointBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : offerCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null && offerCheckpointActual.getUrlRecoverOffer() != null) {
			apiUrlShortenerResponseDTO = commonServices.postApiUrlShortener(offerCheckpointActual.getUrlRecoverOffer());
			String shortUrl= apiUrlShortenerResponseDTO.getData().getUrl();
			PostMessageRequestDTO postMessageRequestDTO = new PostMessageRequestDTO();
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(offerCheckpointActual.getUser().getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postMessageRequestDTO.setMessage("KAVAK ya consiguio una oferta para ti. Sigue el siguiente link "+shortUrl+" y vive la mejor experiencia para vender tu auto.");
			commonServices.postMessage(postMessageRequestDTO);  
		    }   
		    offerCheckpointActual.setReleaseAuditOfferSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		} catch (Exception e) {
		    offerCheckpointActual.setReleaseAuditOfferSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS releaseAuditOffer: [" + offerCheckpointActual.getId() + "]");
		    e.printStackTrace();		    
		}
	    }
	    LogService.logger.info("Total SMS Enviados de releaseAuditOffer: [" + listOfferCheckpointBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de releaseAuditOffer");
	    e.printStackTrace();
	}
	return responseDTO;
    }
    
    
    
    /**
     * SMS : SMS G-200 Se registra usuario en KAVAK
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO userRegister() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<User> listUserBD = userRepo.findUserReister();
	    if (listUserBD.isEmpty()) {
		return responseDTO;
	    }
	    for (User userActual : listUserBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : userActual.getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null) {
			PostMessageRequestDTO postMessageRequestDTO = new PostMessageRequestDTO();
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(userActual.getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postMessageRequestDTO.setMessage("¡Gracias por registrarte en KAVAK.com con tu correo "+userActual.getEmail()+" Preparate para vivir una Experiencia Unica.");
			commonServices.postMessage(postMessageRequestDTO);   
		    }  
		    userActual.setUserRegisterSmsSent(true);
		    userRepo.save(userActual);
		} catch (Exception e) {
			userActual.setUserRegisterSmsSent(true);
			userRepo.save(userActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS userRegister: [" + userActual.getId() + "]");
		    e.printStackTrace();		    
		}
	    }
	    LogService.logger.info("Total SMS Enviados de userRegister: [" + listUserBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de userRegister");
	    e.printStackTrace();
	}
	return responseDTO;
    }
    
    
    
    /**
     * SMS : SMS C-202 Acepta oferta pero no continua 
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO acceptOfferNotContinue() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	ApiUrlShortenerResponseDTO apiUrlShortenerResponseDTO = new ApiUrlShortenerResponseDTO();
	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findAcceptOfferNotContinue();
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }
	    for (OfferCheckpoint offerCheckpointActual : listOfferCheckpointBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : offerCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null && offerCheckpointActual.getUrlRecoverOffer() != null) {
			apiUrlShortenerResponseDTO = commonServices.postApiUrlShortener(offerCheckpointActual.getUrlRecoverOffer());
			String shortUrl= apiUrlShortenerResponseDTO.getData().getUrl();
			PostMessageRequestDTO postMessageRequestDTO = new PostMessageRequestDTO();
			postMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postMessageRequestDTO.setEmail(offerCheckpointActual.getUser().getEmail());
			postMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postMessageRequestDTO.setMessage("Hola, gracias por ingresar en nuestra pag para vender tu auto con KAVAK. Haz click en "+shortUrl+" para agendar inspeccion.");
			commonServices.postMessage(postMessageRequestDTO);  
		    }   
		    offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		} catch (Exception e) {
		    offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS acceptOfferNotContinue: [" + offerCheckpointActual.getId() + "]");
		    e.printStackTrace();		    
		}
	    }
	    LogService.logger.info("Total SMS Enviados de acceptOfferNotContinue: [" + listOfferCheckpointBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de acceptOfferNotContinue");
	    e.printStackTrace();
	}
	return responseDTO;
    }
    
    
    
    /**
     * SMS : SMS C-203 Selecciona día y hora pero no continua   (falta terminar)
     * 
     * @author Juan Tolentino
     **/
    /*public ResponseDTO selectDayAndHourNotContinue() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	ApiGoogleUrlShortenerResponseDTO apiGoogleUrlShortenerResponseDTO = new ApiGoogleUrlShortenerResponseDTO();
	try {
	    String metaKey = "dealer_phn";
	    Timestamp ScheduleDate = new Timestamp(System.currentTimeMillis());
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findAcceptOfferNotContinue();
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }
	    for (OfferCheckpoint offerCheckpointActual : listOfferCheckpointBD) {
		try {
		    String userPhone = null;
		    for (UserMeta userMetaActual : offerCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null) {
			apiGoogleUrlShortenerResponseDTO = commonServices.postApiGoogleUrlShortener(offerCheckpointActual.getUrlRecoverOffer());
			String shortUrl= apiGoogleUrlShortenerResponseDTO.getId();
			PostMessageRequestDTO postOAMessageRequestDTO = new PostMessageRequestDTO();
			postOAMessageRequestDTO.setPhone(userPhone);
			//postOAMessageRequestDTO.setPhone("5554719453"); //telefono de antony
			postOAMessageRequestDTO.setEmail(offerCheckpointActual.getUser().getEmail());
			postOAMessageRequestDTO.setScheduleDate(ScheduleDate.toString());
			postOAMessageRequestDTO.setMessage("Hola, gracias por ingresar en nuestra pag para vender tu auto con KAVAK. Haz click en https://goo.gl/maps/CjPc8VhiLi82 para agendar inspeccion.");
			commonServices.postOAMessage(postOAMessageRequestDTO); 
		    }    
		    offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		} catch (Exception e) {
		    offerCheckpointActual.setAcceptOfferNotContinueSmsSent(true);
		    offerCheckpointRepo.save(offerCheckpointActual);
		    LogService.logger.info("Catch interno correo: No se envio SMS acceptOfferNotContinue: [" + offerCheckpointActual.getId() + "]");
		    e.printStackTrace();		    
		}
	    }
	    LogService.logger.info("Total SMS Enviados de acceptOfferNotContinue: [" + listOfferCheckpointBD.size() + "] ");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo SMS: No se envio SMS de acceptOfferNotContinue");
	    e.printStackTrace();
	}
	return responseDTO;
    }*/
    
    
    

}
