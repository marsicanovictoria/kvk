package com.kavak.core.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.kavak.core.dto.PurchaseInternalEmailDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.CarData;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.OfferCheckpoint;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.OfferCheckpointRepository;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;

@Stateless
public class StagePurchaseServices {

	@Inject
	private CarDataRepository carDataRepo;
	
	@Inject
	private OfferCheckpointRepository offerCheckpointRepo;
	
	@Inject
	private UserRepository userRepo;
	
	@Inject
	private UserMetaRepository userMetaRepo;
	
	@Inject
	private MetaValueRepository metaValueRepo;
	
	
	@EJB(mappedName = LookUpNames.SESSION_CelesteServices)
	CelesteServices celesteServices;
	
	Pageable pageable;
	
	ResponseDTO sentInternalEmail = null;
	ResponseDTO updateInternalEmail = null;
	List<Long> sentInternalEmailIds = null;
	String haveNoInformation = "";
	String bcc = "angie@kavak.com";
	Long maxLimit = 10L;

	/**
	 * Retrieve purchase opportunities at a first stage
	 * (Customer receives an offer)
	 * 
	 * @return List<NetsuiteInternalEmailDTO> emailInfoDTO
	 */
	
	public ResponseDTO getFirstStagePurchaseOpportunities(){
		ResponseDTO responseDTO = new ResponseDTO();
		PurchaseInternalEmailDTO emailInfoDTO = null;
		pageable = PageRequest.of(1, 10);

		
		try {
			Long stage				= 1L;
			String customerPhoneKey = "dealer_phn"; 
			List<PurchaseInternalEmailDTO> opportunitiesList = null;
			
			List<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findFirstStagePurchaseOpportunities();
			
			if (offerCheckpointBD.size() > 0){
				
				opportunitiesList = new ArrayList<>();

				for (OfferCheckpoint offerCheck : offerCheckpointBD){
					
					LogService.logger.info("En First Stage Se obtiene la oportunidad: " + offerCheck.getId() + " con fecha: " + offerCheck.getOfferDate().toString());
					
					emailInfoDTO = new PurchaseInternalEmailDTO();
					
					emailInfoDTO.setStage(stage);
					emailInfoDTO.setMinervaId(offerCheck.getId());
					emailInfoDTO.setAddressAppointmentSchedule(haveNoInformation);
					emailInfoDTO.setAddressAppointmentScheduleType(haveNoInformation);
					emailInfoDTO.setAppoinmentSchedule(haveNoInformation);
					emailInfoDTO.setCarInvoiceUrl(haveNoInformation);
					emailInfoDTO.setCirculationCardUrl(haveNoInformation);
					
					Optional<User> userBD = userRepo.findById(offerCheck.getIdUser());
					
					if (userBD.isPresent()){
						emailInfoDTO.setCustomerName(userBD.get().getName());
						emailInfoDTO.setCustomerEmail(userBD.get().getEmail());
						emailInfoDTO.setCustomerZipCode(offerCheck.getZipCode());
						
						UserMeta metaUserBD = userMetaRepo.findUserMetaByMetaKeyAndId(customerPhoneKey, userBD.get().getId());
						
						if(metaUserBD != null){
							emailInfoDTO.setCustomerPhone(metaUserBD.getMetaValue());
						}
					}
						
					CarData carDataBD = carDataRepo.findBySku(offerCheck.getSku());
					
					if (carDataBD != null){
						emailInfoDTO.setCarYear(carDataBD.getCarYear());
						emailInfoDTO.setCarMake(carDataBD.getCarMake());
						emailInfoDTO.setCarModel(carDataBD.getCarModel());
						emailInfoDTO.setCarFullVersion(offerCheck.getFullVersion());
						emailInfoDTO.setSegmentType(carDataBD.getSegmentType());
					}
					
					if (offerCheck.getMax30DaysOffer() != null){
						emailInfoDTO.setThirtyDaysOfferMax(offerCheck.getMax30DaysOffer().replace("$", ""));
						emailInfoDTO.setThirtyDaysOfferMin(offerCheck.getMin30DaysOffer().replace("$", ""));
					}
					
					if (offerCheck.getMaxInstantOffer() != null){
						emailInfoDTO.setInstantOfferMax(offerCheck.getMaxInstantOffer().replace("$", ""));
						emailInfoDTO.setInstantOfferMin(offerCheck.getMinInstantOffer().replace("$", ""));
					}
					
					if (offerCheck.getMaxConsignationOffer() != null){
						emailInfoDTO.setConsigmentOfferMax(offerCheck.getMaxConsignationOffer().replace("$", ""));
						emailInfoDTO.setConsigmentOfferMin(offerCheck.getMinConsignationOffer().replace("$", ""));
					}
					
					if(offerCheck.getIdStatus() != null){
						Optional<MetaValue> metaValuesBD = metaValueRepo.findById(offerCheck.getIdStatus());
						
						if(metaValuesBD.isPresent()){
							emailInfoDTO.setCurrentStatus(metaValuesBD.get().getOptionName());
						}
					}
					
					if(offerCheck.getWishList() == 0){
						emailInfoDTO.setIsWishlist("No");
					}else{
						emailInfoDTO.setIsWishlist("Sí");
					}
					
					emailInfoDTO.setBccEmail(bcc);
					emailInfoDTO.setAcceptedOffer(false);
					emailInfoDTO.setPendingStatus(offerCheck.getStatusDetail());
					emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					
					if(offerCheck.getInspectionLocationDescription()!= null){
						emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					}
					
					if(offerCheck.getHourInpectionSlotText() != null){
						emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					}
					
					if(offerCheck.getCarInvoiceUrl() != null && offerCheck.getCarInvoiceUrl() != ""){
						emailInfoDTO.setCarInvoiceUrl(Constants.URL_KAVAK + offerCheck.getCarInvoiceUrl());
					}
					
					if(offerCheck.getCirculationCardUrl() != null && offerCheck.getCirculationCardUrl() != ""){
						emailInfoDTO.setCirculationCardUrl(Constants.URL_KAVAK + offerCheck.getCirculationCardUrl());
					}
					
					if(offerCheck.getNetsuiteOpportunityId() != null){
						emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					}
					
					if(offerCheck.getNetsuiteOpportunityURL() != null){
						emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					}
					
					if(offerCheck.getDuplicatedOfferControl() != null){
						Long offerQuantity = offerCheckpointRepo.findOfferQuantityByDuplicatedOfferControl(offerCheck.getDuplicatedOfferControl());
						
						if (offerQuantity != null){
							emailInfoDTO.setOfferQuantity(offerQuantity);
						}
					}
					
					opportunitiesList.add(emailInfoDTO);
				}
				
				if (opportunitiesList.size() > 0){
					
					sentInternalEmail = new ResponseDTO();
					sentInternalEmail = celesteServices.sendPurchaseInternalEmail(opportunitiesList);
					
					if (sentInternalEmail.getData() != null){
						
						updateInternalEmail = new ResponseDTO();
						sentInternalEmailIds = new ArrayList<>();
						
						String ids = sentInternalEmail.getData().toString().replace("[", "").replace("]", "").replace(" ", "");
						
						if(ids != null){
			                for (String s : ids.split(",")){
		                        if(!StringUtils.isEmpty(s)) {
		                            sentInternalEmailIds.add(Long.parseLong(s));
		                        }
			                }
			                
			                updateInternalEmail = updateSentValueInternalEmail(sentInternalEmailIds);
			                
			                if (updateInternalEmail.getData().toString() == "true"){
			                	LogService.logger.debug("Se actualizo el campo de envio de correo interno con exito en First Stage");
			                }else{
			                	LogService.logger.debug("No se actualizo el campo de envio de correo interno con exito en First Stage");
			                }
						}
					}else{
						LogService.logger.info("No se obtuvo resultado exitoso enviando los correos internos en First Stage");
					}
				}else{
					LogService.logger.info("No hay oportunidades pendientes por enviar correo en First Stage");
				}
			}else{
				LogService.logger.info("No existen registros en First Stage en este momento");
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(opportunitiesList);
			
		} catch (Exception e) {
			LogService.logger.error("Error consultando First Stage Purchase Opportunities para la oportunidad ");
			e.printStackTrace();
		}
		
		return responseDTO;
	}
	
	
	
	/**
	 * Retrieve purchase opportunities at a second stage
	 * (Customer accepted an offer)
	 * 
	 * @return List<NetsuiteInternalEmailDTO> emailInfoDTO
	 */
	
	public ResponseDTO getSecondStagePurchaseOpportunities(){
		ResponseDTO responseDTO = new ResponseDTO();
		PurchaseInternalEmailDTO emailInfoDTO = null;
		pageable = PageRequest.of(1, 10);
		
		try {
			Long stage				= 2L;
			String customerPhoneKey = "dealer_phn"; 
			List<PurchaseInternalEmailDTO> opportunitiesList = null;
			
			//Page<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findSecondStagePurchaseOpportunities(pageable);
			List<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findSecondStagePurchaseOpportunities();
			
			if (offerCheckpointBD.size() > 0){
				
				opportunitiesList = new ArrayList<>();

				for (OfferCheckpoint offerCheck : offerCheckpointBD){
					
					LogService.logger.info("En Second Stage Se obtiene la oportunidad: " + offerCheck.getId() + " con fecha: " + offerCheck.getOfferDate().toString());
					
					emailInfoDTO = new PurchaseInternalEmailDTO();
						
					emailInfoDTO.setStage(stage);
					emailInfoDTO.setMinervaId(offerCheck.getId());
					emailInfoDTO.setAddressAppointmentSchedule(haveNoInformation);
					emailInfoDTO.setAddressAppointmentScheduleType("");
					emailInfoDTO.setAppoinmentSchedule(haveNoInformation);
					emailInfoDTO.setCarInvoiceUrl(haveNoInformation);
					emailInfoDTO.setCirculationCardUrl(haveNoInformation);
					emailInfoDTO.setCurrentStatus("No indicó dirección");
					emailInfoDTO.setPendingStatus("No indicó dirección");
					
					Optional<User> userBD = userRepo.findById(offerCheck.getIdUser());
					
					if (userBD.isPresent()){
						
						LogService.logger.info("En Second Stage Se obtiene el usuario ID: " + userBD.get().getId() );
						
						emailInfoDTO.setCustomerName(userBD.get().getName());
						emailInfoDTO.setCustomerEmail(userBD.get().getEmail());
						emailInfoDTO.setCustomerZipCode(offerCheck.getZipCode());
						
						UserMeta metaUserBD = userMetaRepo.findUserMetaByMetaKeyAndId(customerPhoneKey, userBD.get().getId());
						
						if(metaUserBD != null){
							emailInfoDTO.setCustomerPhone(metaUserBD.getMetaValue());
						}
					}
						
					CarData carDataBD = carDataRepo.findBySku(offerCheck.getSku());
					
					if (carDataBD != null){
						emailInfoDTO.setCarYear(carDataBD.getCarYear());
						emailInfoDTO.setCarMake(carDataBD.getCarMake());
						emailInfoDTO.setCarModel(carDataBD.getCarModel());
						emailInfoDTO.setCarFullVersion(offerCheck.getFullVersion());
						emailInfoDTO.setSegmentType(carDataBD.getSegmentType());
					}
					
					if (offerCheck.getMax30DaysOffer() != null){
						emailInfoDTO.setThirtyDaysOfferMax(offerCheck.getMax30DaysOffer());
						emailInfoDTO.setThirtyDaysOfferMin(offerCheck.getMin30DaysOffer());
					}
					
					if (offerCheck.getMaxInstantOffer() != null){
						emailInfoDTO.setInstantOfferMax(offerCheck.getMaxInstantOffer());
						emailInfoDTO.setInstantOfferMin(offerCheck.getMinInstantOffer());
					}
					
					if (offerCheck.getMaxConsignationOffer() != null){
						emailInfoDTO.setConsigmentOfferMax(offerCheck.getMaxConsignationOffer());
						emailInfoDTO.setConsigmentOfferMin(offerCheck.getMinConsignationOffer());
					}
					
					LogService.logger.info("En Second Stage Se obtiene el tipo oferta id: " + offerCheck.getOfferType());
					
					
					if (offerCheck.getOfferType() == 25 || offerCheck.getOfferType() == 171){ //Compra inmediata
						emailInfoDTO.setAcceptedOfferType(2L);
					}else if (offerCheck.getOfferType() == 26 || offerCheck.getOfferType() == 170){ //Compra a 30 días
						emailInfoDTO.setAcceptedOfferType(1L);
					}else{
						emailInfoDTO.setAcceptedOfferType(3L);
					}
					
					LogService.logger.info("En Second Stage este auto es wishlit: " + offerCheck.getWishList());
					
					
					if(offerCheck.getWishList() == 0){
						emailInfoDTO.setIsWishlist("No");
					}else{
						emailInfoDTO.setIsWishlist("Sí");
					}

					emailInfoDTO.setBccEmail(bcc);
					emailInfoDTO.setAcceptedOffer(true);
					//emailInfoDTO.setCurrentStatus("No indicó dirección");
					//emailInfoDTO.setPendingStatus(offerCheck.getStatusDetail());
					emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					
					if(offerCheck.getInspectionLocationDescription()!= null){
						emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					}
					
					if(offerCheck.getHourInpectionSlotText() != null){
						emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					}
					
					if(offerCheck.getCarInvoiceUrl() != null && offerCheck.getCarInvoiceUrl() != ""){
						emailInfoDTO.setCarInvoiceUrl(Constants.URL_KAVAK + offerCheck.getCarInvoiceUrl());
					}
					
					if(offerCheck.getCirculationCardUrl() != null && offerCheck.getCirculationCardUrl() != ""){
						emailInfoDTO.setCirculationCardUrl(Constants.URL_KAVAK + offerCheck.getCirculationCardUrl());
					}
					
					if(offerCheck.getNetsuiteOpportunityId() != null && offerCheck.getNetsuiteOpportunityId() != 0){
						emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					}
					
					if(offerCheck.getNetsuiteOpportunityURL() != null && offerCheck.getNetsuiteOpportunityURL() != ""){
						emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					}
					
					if(offerCheck.getDuplicatedOfferControl() != null){
						Long offerQuantity = offerCheckpointRepo.findOfferQuantityByDuplicatedOfferControl(offerCheck.getDuplicatedOfferControl());
						
						if (offerQuantity != null){
							emailInfoDTO.setOfferQuantity(offerQuantity);
						}
					}
					
					opportunitiesList.add(emailInfoDTO);
				}
				
				if (opportunitiesList.size() > 0){
					
					sentInternalEmail = new ResponseDTO();
					sentInternalEmail = celesteServices.sendPurchaseInternalEmail(opportunitiesList);
					
					if (sentInternalEmail.getData() != null){
						
						updateInternalEmail = new ResponseDTO();
						sentInternalEmailIds = new ArrayList<>();
						
						//LogService.logger.debug("Obtenido en el array de Second Stage para enviar correos: " + sentInternalEmail.getData().toString());
						
						String ids = sentInternalEmail.getData().toString().replace("[", "").replace("]", "").replace(" ", "");
						
						if (ids != null){
			                for (String s : ids.split(",")){
		                        if(!StringUtils.isEmpty(s)) {
		                            sentInternalEmailIds.add(Long.parseLong(s));
		                        }
			                }
			                
			                updateInternalEmail = updateSentValueInternalEmail(sentInternalEmailIds);
			                
			                if (updateInternalEmail.getData().toString() == "true"){
			                	LogService.logger.info("Se actualizo el campo de envio de correo interno con exito en Second Stage");
			                }else{
			                	LogService.logger.info("No se actualizo el campo de envio de correo interno con exito en Second Stage");
			                }
							
						}else{
							LogService.logger.info("No se obtuvo resultado exitoso enviando los correos internos en Second Stage");
						}
					}
				}else{
					LogService.logger.info("No hay oportunidades pendientes por enviar correo en Second Stage");
				}
			}else{
				LogService.logger.info("No existen registros en Second Stage en este momento");
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(opportunitiesList);
			
		} catch (Exception e) {
			LogService.logger.error("Error consultando Second Stage Purchase Opportunities, ");
			e.printStackTrace();
		}
		
		return responseDTO;
	}
	
	
	
	/**
	 * Retrieve purchase opportunities at a third stage
	 * (Customer selected an inspection location)
	 * 
	 * @return List<NetsuiteInternalEmailDTO> emailInfoDTO
	 */

	public ResponseDTO getThirdStagePurchaseOpportunities(){
		ResponseDTO responseDTO = new ResponseDTO();
		PurchaseInternalEmailDTO emailInfoDTO = null;
		pageable = PageRequest.of(1, 10);
		
		try {
			Long stage				= 3L;
			String customerPhoneKey = "dealer_phn"; 
			List<PurchaseInternalEmailDTO> opportunitiesList = null;
			
			List<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findThirdStagePurchaseOpportunities();
			
			if (offerCheckpointBD.size() > 0){
				
				opportunitiesList = new ArrayList<>();
				
				for (OfferCheckpoint offerCheck : offerCheckpointBD){
					
					LogService.logger.info("En Third Stage Se obtiene la oportunidad: " + offerCheck.getId() + " con fecha: " + offerCheck.getOfferDate().toString());
					
					
					emailInfoDTO = new PurchaseInternalEmailDTO();
					emailInfoDTO.setStage(stage);
					emailInfoDTO.setMinervaId(offerCheck.getId());
					emailInfoDTO.setAddressAppointmentSchedule(haveNoInformation);
					emailInfoDTO.setAddressAppointmentScheduleType(haveNoInformation);
					emailInfoDTO.setAppoinmentSchedule(haveNoInformation);
					emailInfoDTO.setCarInvoiceUrl(haveNoInformation);
					emailInfoDTO.setCirculationCardUrl(haveNoInformation);
					
					Optional<User> userBD = userRepo.findById(offerCheck.getIdUser());
					
					if (userBD.isPresent()){
						emailInfoDTO.setCustomerName(userBD.get().getName());
						emailInfoDTO.setCustomerEmail(userBD.get().getEmail());
						emailInfoDTO.setCustomerZipCode(offerCheck.getZipCode());
						
						UserMeta metaUserBD = userMetaRepo.findUserMetaByMetaKeyAndId(customerPhoneKey, userBD.get().getId());
						
						if(metaUserBD != null){
							emailInfoDTO.setCustomerPhone(metaUserBD.getMetaValue());
						}
					}
						
					CarData carDataBD = carDataRepo.findBySku(offerCheck.getSku());
					
					if (carDataBD != null){
						emailInfoDTO.setCarYear(carDataBD.getCarYear());
						emailInfoDTO.setCarMake(carDataBD.getCarMake());
						emailInfoDTO.setCarModel(carDataBD.getCarModel());
						emailInfoDTO.setCarFullVersion(offerCheck.getFullVersion());
						emailInfoDTO.setSegmentType(carDataBD.getSegmentType());
					}
					
					if (offerCheck.getMax30DaysOffer() != null){
						emailInfoDTO.setThirtyDaysOfferMax(offerCheck.getMax30DaysOffer());
						emailInfoDTO.setThirtyDaysOfferMin(offerCheck.getMin30DaysOffer());
					}
					
					if (offerCheck.getMaxInstantOffer() != null){
						emailInfoDTO.setInstantOfferMax(offerCheck.getMaxInstantOffer());
						emailInfoDTO.setInstantOfferMin(offerCheck.getMinInstantOffer());
					}
					
					if (offerCheck.getMaxConsignationOffer() != null){
						emailInfoDTO.setConsigmentOfferMax(offerCheck.getMaxConsignationOffer());
						emailInfoDTO.setConsigmentOfferMin(offerCheck.getMinConsignationOffer());
					}
					
					if (offerCheck.getOfferType() == 25 || offerCheck.getOfferType() == 171){ //Compra inmediata
						emailInfoDTO.setAcceptedOfferType(2L);
					}else if (offerCheck.getOfferType() == 26 || offerCheck.getOfferType() == 170){ //Compra a 30 días
						emailInfoDTO.setAcceptedOfferType(1L);
					}else{
						emailInfoDTO.setAcceptedOfferType(3L);
					}
					
					if(offerCheck.getIdStatus() != null){
						Optional<MetaValue> metaValuesBD = metaValueRepo.findById(offerCheck.getIdStatus());
						
						if(metaValuesBD.isPresent()){
							emailInfoDTO.setCurrentStatus(metaValuesBD.get().getOptionName());
						}
					}
					
					if(offerCheck.getWishList() == 0){
						emailInfoDTO.setIsWishlist("No");
					}else{
						emailInfoDTO.setIsWishlist("Sí");
					}
					
					emailInfoDTO.setBccEmail(bcc);
					emailInfoDTO.setAcceptedOffer(true);
					emailInfoDTO.setPendingStatus("No indicó horario ni día");
					emailInfoDTO.setCurrentStatus("No indicó horario ni día");
					emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					
					if(offerCheck.getInspectionLocationDescription()!= null){
						emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					}
					
					if(offerCheck.getHourInpectionSlotText() != null){
						emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					}
					
					if(offerCheck.getCarInvoiceUrl() != null && offerCheck.getCarInvoiceUrl() != ""){
						emailInfoDTO.setCarInvoiceUrl(Constants.URL_KAVAK + offerCheck.getCarInvoiceUrl());
					}
					
					if(offerCheck.getCirculationCardUrl() != null && offerCheck.getCirculationCardUrl() != ""){
						emailInfoDTO.setCirculationCardUrl(Constants.URL_KAVAK + offerCheck.getCirculationCardUrl());
					}
					
					if(offerCheck.getNetsuiteOpportunityId() != null){
						emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					}
					
					if(offerCheck.getNetsuiteOpportunityURL() != null){
						emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					}
					
					if (offerCheck.getInspectionCenterDescription() != null){
						emailInfoDTO.setAddressAppointmentScheduleType(offerCheck.getInspectionCenterDescription());
					}else{
						emailInfoDTO.setAddressAppointmentScheduleType("Dirección del Cliente");
					}
					
					if(offerCheck.getDuplicatedOfferControl() != null){
						Long offerQuantity = offerCheckpointRepo.findOfferQuantityByDuplicatedOfferControl(offerCheck.getDuplicatedOfferControl());
						
						if (offerQuantity != null){
							emailInfoDTO.setOfferQuantity(offerQuantity);
						}
					}
					
					opportunitiesList.add(emailInfoDTO);
				}
				
				if (opportunitiesList.size() > 0){
					
					sentInternalEmail = new ResponseDTO();
					sentInternalEmail = celesteServices.sendPurchaseInternalEmail(opportunitiesList);
					
					if (sentInternalEmail.getData() != null){
						
						updateInternalEmail = new ResponseDTO();
						sentInternalEmailIds = new ArrayList<>();
						
						String ids = sentInternalEmail.getData().toString().replace("[", "").replace("]", "").replace(" ", "");
						
						if (ids != null){
			                for (String s : ids.split(",")){
		                        if(!StringUtils.isEmpty(s)) {
		                            sentInternalEmailIds.add(Long.parseLong(s));
		                        }
			                }
			                
			                updateInternalEmail = updateSentValueInternalEmail(sentInternalEmailIds);
			                
			                if (updateInternalEmail.getData().toString() == "true"){
			                	LogService.logger.debug("Se actualizo el campo de envio de correo interno con exito en Third Stage");
			                }else{
			                	LogService.logger.debug("No se actualizo el campo de envio de correo interno con exito en Third Stage");
			                }
						}
					}else{
						LogService.logger.debug("No se obtuvo resultado exitoso enviando los correos internos en Third Stage");
					}
				}else{
					LogService.logger.info("No hay oportunidades pendientes por enviar correo en Third Stage");
				}
			}else{
				LogService.logger.info("No existen registros en Third Stage en este momento");
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(opportunitiesList);
			
		} catch (Exception e) {
			LogService.logger.error("Error consultando Third Stage Purchase Opportunities, ");
			e.printStackTrace();
		}
		
		return responseDTO;
	}

	
	
	/**
	 * Retrieve purchase opportunities at a forth stage
	 * (Customer selected an appointment schedule)
	 * 
	 * @return List<NetsuiteInternalEmailDTO> emailInfoDTO
	 */
	public ResponseDTO getFourthStagePurchaseOpportunities(){
		ResponseDTO responseDTO = new ResponseDTO();
		PurchaseInternalEmailDTO emailInfoDTO = null;
		pageable = PageRequest.of(1, 10);
		
		try {
			Long stage				= 4L;
			String customerPhoneKey = "dealer_phn"; 
			List<PurchaseInternalEmailDTO> opportunitiesList = null;
			
			List<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findFourthStagePurchaseOpportunities();
			
			if (offerCheckpointBD.size() > 0){
				
				opportunitiesList = new ArrayList<>();
				
				for (OfferCheckpoint offerCheck : offerCheckpointBD){
					
					LogService.logger.info("En Fourth Stage Se obtiene la oportunidad: " + offerCheck.getId() + " con fecha: " + offerCheck.getOfferDate().toString());
					
					emailInfoDTO = new PurchaseInternalEmailDTO();
					emailInfoDTO.setStage(stage);
					emailInfoDTO.setMinervaId(offerCheck.getId());
					emailInfoDTO.setAddressAppointmentSchedule(haveNoInformation);
					emailInfoDTO.setAddressAppointmentScheduleType(haveNoInformation);
					emailInfoDTO.setAppoinmentSchedule(haveNoInformation);
					emailInfoDTO.setCarInvoiceUrl(haveNoInformation);
					emailInfoDTO.setCirculationCardUrl(haveNoInformation);
					
					Optional<User> userBD = userRepo.findById(offerCheck.getIdUser());
					
					if (userBD.isPresent()){
						emailInfoDTO.setCustomerName(userBD.get().getName());
						emailInfoDTO.setCustomerEmail(userBD.get().getEmail());
						emailInfoDTO.setCustomerZipCode(offerCheck.getZipCode());
						
						UserMeta metaUserBD = userMetaRepo.findUserMetaByMetaKeyAndId(customerPhoneKey, userBD.get().getId());
						
						if(metaUserBD != null){
							emailInfoDTO.setCustomerPhone(metaUserBD.getMetaValue());
						}
					}
					
					CarData carDataBD = carDataRepo.findBySku(offerCheck.getSku());
					
					if (carDataBD != null){
						emailInfoDTO.setCarYear(carDataBD.getCarYear());
						emailInfoDTO.setCarMake(carDataBD.getCarMake());
						emailInfoDTO.setCarModel(carDataBD.getCarModel());
						emailInfoDTO.setCarFullVersion(offerCheck.getFullVersion());
						emailInfoDTO.setSegmentType(carDataBD.getSegmentType());
					}
					
					if (offerCheck.getMax30DaysOffer() != null){
						emailInfoDTO.setThirtyDaysOfferMax(offerCheck.getMax30DaysOffer());
						emailInfoDTO.setThirtyDaysOfferMin(offerCheck.getMin30DaysOffer());
					}
					
					if (offerCheck.getMaxInstantOffer() != null){
						emailInfoDTO.setInstantOfferMax(offerCheck.getMaxInstantOffer());
						emailInfoDTO.setInstantOfferMin(offerCheck.getMinInstantOffer());
					}
					
					if (offerCheck.getMaxConsignationOffer() != null){
						emailInfoDTO.setConsigmentOfferMax(offerCheck.getMaxConsignationOffer());
						emailInfoDTO.setConsigmentOfferMin(offerCheck.getMinConsignationOffer());
					}
						
					if (offerCheck.getOfferType() == 25 || offerCheck.getOfferType() == 171){ //Compra inmediata
						emailInfoDTO.setAcceptedOfferType(2L);
					}else if (offerCheck.getOfferType() == 26 || offerCheck.getOfferType() == 170){ //Compra a 30 días
						emailInfoDTO.setAcceptedOfferType(1L);
					}else{
						emailInfoDTO.setAcceptedOfferType(3L);
					}
					
					if(offerCheck.getWishList() == 0){
						emailInfoDTO.setIsWishlist("No");
					}else{
						emailInfoDTO.setIsWishlist("Sí");
					}
					
					emailInfoDTO.setBccEmail(bcc);
					emailInfoDTO.setAcceptedOffer(true);
					emailInfoDTO.setCurrentStatus("No cargó documentos");
					emailInfoDTO.setPendingStatus("No cargó documentos");
					emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					
					if(offerCheck.getInspectionLocationDescription()!= null){
						emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					}
					
					if(offerCheck.getHourInpectionSlotText() != null){
						emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					}
					
					if(offerCheck.getCarInvoiceUrl() != null && offerCheck.getCarInvoiceUrl() != ""){
						emailInfoDTO.setCarInvoiceUrl(Constants.URL_KAVAK + offerCheck.getCarInvoiceUrl());
					}
					
					if(offerCheck.getCirculationCardUrl() != null && offerCheck.getCirculationCardUrl() != ""){
						emailInfoDTO.setCirculationCardUrl(Constants.URL_KAVAK + offerCheck.getCirculationCardUrl());
					}
					
					if(offerCheck.getNetsuiteOpportunityId() != null){
						emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					}
					
					if(offerCheck.getNetsuiteOpportunityURL() != null){
						emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					}
					
					if (offerCheck.getInspectionCenterDescription() != null){
						emailInfoDTO.setAddressAppointmentScheduleType(offerCheck.getInspectionCenterDescription());
					}else{
						emailInfoDTO.setAddressAppointmentScheduleType("Dirección del Cliente");
					}
					
					if(offerCheck.getDuplicatedOfferControl() != null){
						Long offerQuantity = offerCheckpointRepo.findOfferQuantityByDuplicatedOfferControl(offerCheck.getDuplicatedOfferControl());
						
						if (offerQuantity != null){
							emailInfoDTO.setOfferQuantity(offerQuantity);
						}
					}
						
					opportunitiesList.add(emailInfoDTO);
				}
				
				if (opportunitiesList.size() > 0){
					
					sentInternalEmail = new ResponseDTO();
					sentInternalEmail = celesteServices.sendPurchaseInternalEmail(opportunitiesList);
					
					if (sentInternalEmail.getData() != null){
						
						updateInternalEmail = new ResponseDTO();
						sentInternalEmailIds = new ArrayList<>();
						
						String ids = sentInternalEmail.getData().toString().replace("[", "").replace("]", "").replace(" ", "");
						
						if(ids != null){
			                for (String s : ids.split(",")){
		                        if(!StringUtils.isEmpty(s)) {
		                            sentInternalEmailIds.add(Long.parseLong(s));
		                        }
			                }
			                
			                updateInternalEmail = updateSentValueInternalEmail(sentInternalEmailIds);
			                
			                if (updateInternalEmail.getData().toString() == "true"){
			                	LogService.logger.info("Se actualizo el campo de envio de correo interno con exito en Fourth Stage");
			                }else{
			                	LogService.logger.info("No se actualizo el campo de envio de correo interno con exito en Fourth Stage");
			                }
						}
						
					}else{
						LogService.logger.info("No se obtuvo resultado exitoso enviando los correos internos en Fourth Stage");
					}
				}else{
					LogService.logger.info("No hay oportunidades pendientes por enviar correo en Fourth Stage");
				}
			}else{
				LogService.logger.info("No existen registros en Fouth Stage en este momento");
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(opportunitiesList);
			
		} catch (Exception e) {
			LogService.logger.error("Error consultando Fourth Stage Purchase Opportunities, ");
			e.printStackTrace();
		}
		
		return responseDTO;
	}
	
	
	/**
	 * Retrieve purchase opportunities at a forth stage but without appointment scheduled selected
	 * (Customer didn't selected an appointment schedule, we need to contact him in order to define a appointment schedule)
	 * 
	 * @return List<NetsuiteInternalEmailDTO> emailInfoDTO
	 */

	public ResponseDTO getIncompleteFourthStagePurchaseOpportunities(){
		ResponseDTO responseDTO = new ResponseDTO();
		PurchaseInternalEmailDTO emailInfoDTO = null;
		pageable = PageRequest.of(1, 10);
		
		try {
			Long stage				= 4L;
			String customerPhoneKey = "dealer_phn"; 
			List<PurchaseInternalEmailDTO> opportunitiesList = null;
			
			List<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findIncompleteFourthStagePurchaseOpportunities();
			
			if (offerCheckpointBD.size() > 0){
				
				opportunitiesList = new ArrayList<>();
				
				for (OfferCheckpoint offerCheck : offerCheckpointBD){
					
					LogService.logger.info("En Incomplete Fourth Stage Se obtiene la oportunidad: " + offerCheck.getId() + " con fecha: " + offerCheck.getOfferDate().toString());
					
					emailInfoDTO = new PurchaseInternalEmailDTO();
						
					emailInfoDTO.setStage(stage);
					emailInfoDTO.setMinervaId(offerCheck.getId());
					emailInfoDTO.setAddressAppointmentSchedule(haveNoInformation);
					emailInfoDTO.setAddressAppointmentScheduleType(haveNoInformation);
					emailInfoDTO.setAppoinmentSchedule(haveNoInformation);
					emailInfoDTO.setCarInvoiceUrl(haveNoInformation);
					emailInfoDTO.setCirculationCardUrl(haveNoInformation);
					
					Optional<User> userBD = userRepo.findById(offerCheck.getIdUser());
					
					if (userBD.isPresent()){
						emailInfoDTO.setCustomerName(userBD.get().getName());
						emailInfoDTO.setCustomerEmail(userBD.get().getEmail());
						emailInfoDTO.setCustomerZipCode(offerCheck.getZipCode());
						
						UserMeta metaUserBD = userMetaRepo.findUserMetaByMetaKeyAndId(customerPhoneKey, userBD.get().getId());
						
						if(metaUserBD != null){
							emailInfoDTO.setCustomerPhone(metaUserBD.getMetaValue());
						}
					}
					
					CarData carDataBD = carDataRepo.findBySku(offerCheck.getSku());
					
					if (carDataBD != null){
						emailInfoDTO.setCarYear(carDataBD.getCarYear());
						emailInfoDTO.setCarMake(carDataBD.getCarMake());
						emailInfoDTO.setCarModel(carDataBD.getCarModel());
						emailInfoDTO.setCarFullVersion(offerCheck.getFullVersion());
						emailInfoDTO.setSegmentType(carDataBD.getSegmentType());
					}
					
					if (offerCheck.getMax30DaysOffer() != null){
						emailInfoDTO.setThirtyDaysOfferMax(offerCheck.getMax30DaysOffer());
						emailInfoDTO.setThirtyDaysOfferMin(offerCheck.getMin30DaysOffer());
					}
					
					if (offerCheck.getMaxInstantOffer() != null){
						emailInfoDTO.setInstantOfferMax(offerCheck.getMaxInstantOffer());
						emailInfoDTO.setInstantOfferMin(offerCheck.getMinInstantOffer());
					}
					
					if (offerCheck.getMaxConsignationOffer() != null){
						emailInfoDTO.setConsigmentOfferMax(offerCheck.getMaxConsignationOffer());
						emailInfoDTO.setConsigmentOfferMin(offerCheck.getMinConsignationOffer());
					}
						
					if (offerCheck.getOfferType() == 25 || offerCheck.getOfferType() == 171){ //Compra inmediata
						emailInfoDTO.setAcceptedOfferType(2L);
					}else if (offerCheck.getOfferType() == 26 || offerCheck.getOfferType() == 170){ //Compra a 30 días
						emailInfoDTO.setAcceptedOfferType(1L);
					}else{
						emailInfoDTO.setAcceptedOfferType(3L);
					}
					
					if(offerCheck.getIdStatus() != null){
						Optional<MetaValue> metaValuesBD = metaValueRepo.findById(offerCheck.getIdStatus());
						
						if(metaValuesBD.isPresent()){
							emailInfoDTO.setCurrentStatus(metaValuesBD.get().getOptionName());
						}
					}
					
					if(offerCheck.getWishList() == 0){
						emailInfoDTO.setIsWishlist("No");
					}else{
						emailInfoDTO.setIsWishlist("Sí");
					}
					
					emailInfoDTO.setBccEmail(bcc);
					emailInfoDTO.setAcceptedOffer(true);
					emailInfoDTO.setPendingStatus(offerCheck.getStatusDetail());
					emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					
					if(offerCheck.getInspectionLocationDescription()!= null){
						emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					}
					
					if(offerCheck.getHourInpectionSlotText() != null){
						emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					}
					
					if(offerCheck.getCarInvoiceUrl() != null && offerCheck.getCarInvoiceUrl() != ""){
						emailInfoDTO.setCarInvoiceUrl(Constants.URL_KAVAK + offerCheck.getCarInvoiceUrl());
					}
					
					if(offerCheck.getCirculationCardUrl() != null && offerCheck.getCirculationCardUrl() != ""){
						emailInfoDTO.setCirculationCardUrl(Constants.URL_KAVAK + offerCheck.getCirculationCardUrl());
					}
					
					if(offerCheck.getNetsuiteOpportunityId() != null){
						emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					}
					
					if(offerCheck.getNetsuiteOpportunityURL() != null){
						emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					}
					
					if (offerCheck.getInspectionCenterDescription() != null){
						emailInfoDTO.setAddressAppointmentScheduleType(offerCheck.getInspectionCenterDescription());
					}else{
						emailInfoDTO.setAddressAppointmentScheduleType("Dirección del Cliente");
					}
						
					if(offerCheck.getDuplicatedOfferControl() != null){
						Long offerQuantity = offerCheckpointRepo.findOfferQuantityByDuplicatedOfferControl(offerCheck.getDuplicatedOfferControl());
						
						if (offerQuantity != null){
							emailInfoDTO.setOfferQuantity(offerQuantity);
						}
					}
					
					opportunitiesList.add(emailInfoDTO);
				}
				
				if (opportunitiesList.size() > 0){
					
					sentInternalEmail = new ResponseDTO();
					sentInternalEmail = celesteServices.sendPurchaseInternalEmail(opportunitiesList);
					
					if (sentInternalEmail.getData() != null){
						
						updateInternalEmail = new ResponseDTO();
						sentInternalEmailIds = new ArrayList<>();
						
						String ids = sentInternalEmail.getData().toString().replace("[", "").replace("]", "").replace(" ", "");
						
						if(ids != null){
			                for (String s : ids.split(",")){
		                        if(!StringUtils.isEmpty(s)) {
		                            sentInternalEmailIds.add(Long.parseLong(s));
		                        }
			                }
			                
			                updateInternalEmail = updateSentValueInternalEmail(sentInternalEmailIds);
			                
			                if (updateInternalEmail.getData().toString() == "true"){
			                	LogService.logger.info("Se actualizo el campo de envio de correo interno con exito en Incomplete Fourth Stage");
			                }else{
			                	LogService.logger.info("No se actualizo el campo de envio de correo interno con exito en Incomplete Fourth Stage");
			                }
						}
					}else{
						LogService.logger.info("No se obtuvo resultado exitoso enviando los correos internos en Incomplete Fourth Stage");
					}
				}else{
					LogService.logger.debug("No hay oportunidades pendientes por enviar correo en Incomplete Fourth Stage");
				}
			}else{
				LogService.logger.info("No existen registros en Incomplete Fourth Stage en este momento");
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(opportunitiesList);
			
		} catch (Exception e) {
			LogService.logger.error("Error consultando Incomplete Fourth Stage Purchase Opportunities, " );
			e.printStackTrace();
		}
		
		return responseDTO;
	}

	
	
	/**
	 * Retrieve purchase opportunities at a fifth stage
	 * (Customer completed all purchase process)
	 * 
	 * @return List<NetsuiteInternalEmailDTO> emailInfoDTO
	 */

	public ResponseDTO getFifthStagePurchaseOpportunities(){
		ResponseDTO responseDTO = new ResponseDTO();
		PurchaseInternalEmailDTO emailInfoDTO = null;
		pageable = PageRequest.of(1, 10);
		
		try {
			Long stage				= 5L;
			String customerPhoneKey = "dealer_phn"; 
			List<PurchaseInternalEmailDTO> opportunitiesList = null;
			
			List<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findFifthStagePurchaseOpportunities();
			
			if (offerCheckpointBD.size() > 0){
				
				opportunitiesList = new ArrayList<>();
				
				for (OfferCheckpoint offerCheck : offerCheckpointBD){
					
					LogService.logger.info("En Fifth Stage Se obtiene la oportunidad: " + offerCheck.getId() + " con fecha: " + offerCheck.getOfferDate().toString());
					
					emailInfoDTO = new PurchaseInternalEmailDTO();
						
					emailInfoDTO.setStage(stage);
					emailInfoDTO.setMinervaId(offerCheck.getId());
					emailInfoDTO.setAddressAppointmentSchedule(haveNoInformation);
					emailInfoDTO.setAddressAppointmentScheduleType(haveNoInformation);
					emailInfoDTO.setAppoinmentSchedule(haveNoInformation);
					emailInfoDTO.setCarInvoiceUrl(haveNoInformation);
					emailInfoDTO.setCirculationCardUrl(haveNoInformation);
					
					Optional<User> userBD = userRepo.findById(offerCheck.getIdUser());
					
					if (userBD.isPresent()){
						emailInfoDTO.setCustomerName(userBD.get().getName());
						emailInfoDTO.setCustomerEmail(userBD.get().getEmail());
						emailInfoDTO.setCustomerZipCode(offerCheck.getZipCode());
						
						UserMeta metaUserBD = userMetaRepo.findUserMetaByMetaKeyAndId(customerPhoneKey, userBD.get().getId());
						
						if(metaUserBD != null){
							emailInfoDTO.setCustomerPhone(metaUserBD.getMetaValue());
						}
					}
					
					CarData carDataBD = carDataRepo.findBySku(offerCheck.getSku());
					
					if (carDataBD != null){
						emailInfoDTO.setCarYear(carDataBD.getCarYear());
						emailInfoDTO.setCarMake(carDataBD.getCarMake());
						emailInfoDTO.setCarModel(carDataBD.getCarModel());
						emailInfoDTO.setCarFullVersion(offerCheck.getFullVersion());
						emailInfoDTO.setSegmentType(carDataBD.getSegmentType());
					}
					
					if (offerCheck.getMax30DaysOffer() != null){
						emailInfoDTO.setThirtyDaysOfferMax(offerCheck.getMax30DaysOffer());
						emailInfoDTO.setThirtyDaysOfferMin(offerCheck.getMin30DaysOffer());
					}
					
					if (offerCheck.getMaxInstantOffer() != null){
						emailInfoDTO.setInstantOfferMax(offerCheck.getMaxInstantOffer());
						emailInfoDTO.setInstantOfferMin(offerCheck.getMinInstantOffer());
					}
					
					if (offerCheck.getMaxConsignationOffer() != null){
						emailInfoDTO.setConsigmentOfferMax(offerCheck.getMaxConsignationOffer());
						emailInfoDTO.setConsigmentOfferMin(offerCheck.getMinConsignationOffer());
					}
						
					if (offerCheck.getOfferType() == 25 || offerCheck.getOfferType() == 171){ //Compra inmediata
						emailInfoDTO.setAcceptedOfferType(2L);
					}else if (offerCheck.getOfferType() == 26 || offerCheck.getOfferType() == 170){ //Compra a 30 días
						emailInfoDTO.setAcceptedOfferType(1L);
					}else{
						emailInfoDTO.setAcceptedOfferType(3L);
					}
					
					if(offerCheck.getIdStatus() != null){
						Optional<MetaValue> metaValuesBD = metaValueRepo.findById(offerCheck.getIdStatus());
						
						if(metaValuesBD.isPresent()){
							emailInfoDTO.setCurrentStatus(metaValuesBD.get().getOptionName());
						}
					}
					
					if(offerCheck.getWishList() == 0){
						emailInfoDTO.setIsWishlist("No");
					}else{
						emailInfoDTO.setIsWishlist("Sí");
					}
					
					emailInfoDTO.setBccEmail(bcc);
					emailInfoDTO.setAcceptedOffer(true);
					emailInfoDTO.setPendingStatus("Finalizó Proceso");
					emailInfoDTO.setCurrentStatus("Finalizó Proceso");
					emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					emailInfoDTO.setCarInvoiceUrl(offerCheck.getCarInvoiceUrl());
					emailInfoDTO.setCirculationCardUrl(offerCheck.getCirculationCardUrl());
					emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					
					if(offerCheck.getInspectionLocationDescription()!= null){
						emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					}
					
					if(offerCheck.getHourInpectionSlotText() != null){
						emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					}
					
					if(offerCheck.getCarInvoiceUrl() != null && offerCheck.getCarInvoiceUrl() != ""){
						emailInfoDTO.setCarInvoiceUrl(Constants.URL_KAVAK + offerCheck.getCarInvoiceUrl());
					}
					
					if(offerCheck.getCirculationCardUrl() != null && offerCheck.getCirculationCardUrl() != ""){
						emailInfoDTO.setCirculationCardUrl(Constants.URL_KAVAK + offerCheck.getCirculationCardUrl());
					}
					
					if(offerCheck.getNetsuiteOpportunityId() != null){
						emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					}
					
					if(offerCheck.getNetsuiteOpportunityURL() != null){
						emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					}
					
					if (offerCheck.getInspectionCenterDescription() != null){
						emailInfoDTO.setAddressAppointmentScheduleType(offerCheck.getInspectionCenterDescription());
					}else{
						emailInfoDTO.setAddressAppointmentScheduleType("Dirección del Cliente");
					}
					
					if(offerCheck.getDuplicatedOfferControl() != null){
						Long offerQuantity = offerCheckpointRepo.findOfferQuantityByDuplicatedOfferControl(offerCheck.getDuplicatedOfferControl());
						
						if (offerQuantity != null){
							emailInfoDTO.setOfferQuantity(offerQuantity);
						}
					}
					
					opportunitiesList.add(emailInfoDTO);
				}
				
				if (opportunitiesList != null){
					
					sentInternalEmail = new ResponseDTO();
					sentInternalEmail = celesteServices.sendPurchaseInternalEmail(opportunitiesList);
					
					if (sentInternalEmail.getData() != null){
						
						updateInternalEmail = new ResponseDTO();
						sentInternalEmailIds = new ArrayList<>();
						
						String ids = sentInternalEmail.getData().toString().replace("[", "").replace("]", "").replace(" ", "");
						
						if(ids != null){
			                for (String s : ids.split(",")){
		                        if(!StringUtils.isEmpty(s)) {
		                            sentInternalEmailIds.add(Long.parseLong(s));
		                        }
			                }
			                
			                updateInternalEmail = updateSentValueInternalEmail(sentInternalEmailIds);
			                
			                if (updateInternalEmail.getData().toString() == "true"){
			                	LogService.logger.info("Se actualizo el campo de envio de correo interno con exito en Fifth Stage");
			                }else{
			                	LogService.logger.info("No se actualizo el campo de envio de correo interno con exito en Fifth Stage");
			                }
						}else{
							LogService.logger.info("No hay oportunidades pendientes por enviar correo en Fifth Stage");
						}
					}else{
						LogService.logger.info("No se obtuvo resultado exitoso enviando los correos internos en Fifth Stage");
					}
				}
			}else{
				LogService.logger.info("No existen registros en Fifth Stage en este momento");
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(opportunitiesList);
			
		} catch (Exception e) {
			LogService.logger.error("Error consultando Fifth Stage Purchase Opportunities, ");
			e.printStackTrace();
		}
		
		return responseDTO;
	}
	

	
	/**
	 * Retrieve purchase opportunities at a fifth stage but without an appointment schedule
	 * (Customer completed purchase process but didn't selected an appointment schedule)
	 * 
	 * @return List<NetsuiteInternalEmailDTO> emailInfoDTO
	 */
	
	public ResponseDTO getIncompleteFifthStagePurchaseOpportunities(){
		ResponseDTO responseDTO = new ResponseDTO();
		PurchaseInternalEmailDTO emailInfoDTO = null;
		pageable = PageRequest.of(1, 10);
		
		try {
			Long stage				= 5L;
			String customerPhoneKey = "dealer_phn"; 
			List<PurchaseInternalEmailDTO> opportunitiesList = null;
			
			List<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findIncompleteFifthStagePurchaseOpportunities();
			
			if (offerCheckpointBD.size() > 0){
				
				opportunitiesList = new ArrayList<>();
				
				for (OfferCheckpoint offerCheck : offerCheckpointBD){
					
					LogService.logger.info("En Incomplete Fifth Stage Se obtiene la oportunidad: " + offerCheck.getId() + " con fecha: " + offerCheck.getOfferDate().toString());
					
					emailInfoDTO = new PurchaseInternalEmailDTO();
						
					emailInfoDTO.setStage(stage);
					emailInfoDTO.setMinervaId(offerCheck.getId());
					emailInfoDTO.setAddressAppointmentSchedule(haveNoInformation);
					emailInfoDTO.setAddressAppointmentScheduleType(haveNoInformation);
					emailInfoDTO.setAppoinmentSchedule("Por contactar");
					emailInfoDTO.setCarInvoiceUrl(haveNoInformation);
					emailInfoDTO.setCirculationCardUrl(haveNoInformation);
					
					Optional<User> userBD = userRepo.findById(offerCheck.getIdUser());
					
					if (userBD.isPresent()){
						emailInfoDTO.setCustomerName(userBD.get().getName());
						emailInfoDTO.setCustomerEmail(userBD.get().getEmail());
						emailInfoDTO.setCustomerZipCode(offerCheck.getZipCode());
						
						UserMeta metaUserBD = userMetaRepo.findUserMetaByMetaKeyAndId(customerPhoneKey, userBD.get().getId());
						
						if(metaUserBD != null){
							emailInfoDTO.setCustomerPhone(metaUserBD.getMetaValue());
						}
					}
					
					CarData carDataBD = carDataRepo.findBySku(offerCheck.getSku());
					
					if (carDataBD != null){
						emailInfoDTO.setCarYear(carDataBD.getCarYear());
						emailInfoDTO.setCarMake(carDataBD.getCarMake());
						emailInfoDTO.setCarModel(carDataBD.getCarModel());
						emailInfoDTO.setCarFullVersion(offerCheck.getFullVersion());
						emailInfoDTO.setSegmentType(carDataBD.getSegmentType());
					}
					
					if (offerCheck.getMax30DaysOffer() != null){
						emailInfoDTO.setThirtyDaysOfferMax(offerCheck.getMax30DaysOffer());
						emailInfoDTO.setThirtyDaysOfferMin(offerCheck.getMin30DaysOffer());
					}
						
					if (offerCheck.getMaxInstantOffer() != null){
						emailInfoDTO.setInstantOfferMax(offerCheck.getMaxInstantOffer());
						emailInfoDTO.setInstantOfferMin(offerCheck.getMinInstantOffer());
					}
					
					if (offerCheck.getMaxConsignationOffer() != null){
						emailInfoDTO.setConsigmentOfferMax(offerCheck.getMaxConsignationOffer());
						emailInfoDTO.setConsigmentOfferMin(offerCheck.getMinConsignationOffer());
					}
					
					if (offerCheck.getOfferType() == 25 || offerCheck.getOfferType() == 171){ //Compra inmediata
						emailInfoDTO.setAcceptedOfferType(2L);
					}else if (offerCheck.getOfferType() == 26 || offerCheck.getOfferType() == 170){ //Compra a 30 días
						emailInfoDTO.setAcceptedOfferType(1L);
					}else{
						emailInfoDTO.setAcceptedOfferType(3L);
					}
					
					if(offerCheck.getIdStatus() != null){
						Optional<MetaValue> metaValuesBD = metaValueRepo.findById(offerCheck.getIdStatus());
						
						if(metaValuesBD.isPresent()){
							emailInfoDTO.setCurrentStatus(metaValuesBD.get().getOptionName());
						}
					}
					
					if(offerCheck.getWishList() == 0){
						emailInfoDTO.setIsWishlist("No");
					}else{
						emailInfoDTO.setIsWishlist("Sí");
					}
					
					emailInfoDTO.setBccEmail(bcc);
					emailInfoDTO.setAcceptedOffer(true);
					emailInfoDTO.setPendingStatus(offerCheck.getStatusDetail());
					emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					emailInfoDTO.setPendingStatus("Finalizó Proceso");
					emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					
					if(offerCheck.getInspectionLocationDescription()!= null){
						emailInfoDTO.setAddressAppointmentSchedule(offerCheck.getInspectionLocationDescription());
					}
					/*
					if(offerCheck.getHourInpectionSlotText() != null){
						emailInfoDTO.setAppoinmentSchedule(offerCheck.getHourInpectionSlotText());
					}
					*/
					if(offerCheck.getCarInvoiceUrl() != null && offerCheck.getCarInvoiceUrl() != ""){
						emailInfoDTO.setCarInvoiceUrl(Constants.URL_KAVAK + offerCheck.getCarInvoiceUrl());
					}
					
					if(offerCheck.getCirculationCardUrl() != null && offerCheck.getCirculationCardUrl() != ""){
						emailInfoDTO.setCirculationCardUrl(Constants.URL_KAVAK + offerCheck.getCirculationCardUrl());
					}
					
					if(offerCheck.getNetsuiteOpportunityId() != null){
						emailInfoDTO.setNetsuiteOpOpportunityId(offerCheck.getNetsuiteOpportunityId());
					}
					
					if(offerCheck.getNetsuiteOpportunityURL() != null){
						emailInfoDTO.setNetsuiteOpprotunityUrl(offerCheck.getNetsuiteOpportunityURL());
					}
					
					if (offerCheck.getInspectionCenterDescription() != null){
						emailInfoDTO.setAddressAppointmentScheduleType(offerCheck.getInspectionCenterDescription());
					}else{
						emailInfoDTO.setAddressAppointmentScheduleType("Dirección del Cliente");
					}
					
					if(offerCheck.getDuplicatedOfferControl() != null){
						Long offerQuantity = offerCheckpointRepo.findOfferQuantityByDuplicatedOfferControl(offerCheck.getDuplicatedOfferControl());
						
						if (offerQuantity != null){
							emailInfoDTO.setOfferQuantity(offerQuantity);
						}
					}
					
					opportunitiesList.add(emailInfoDTO);
				}
				
				if (opportunitiesList.size() > 0){
					
					sentInternalEmail = new ResponseDTO();
					sentInternalEmail = celesteServices.sendPurchaseInternalEmail(opportunitiesList);
					
					if (sentInternalEmail.getData() != null){
						
						updateInternalEmail = new ResponseDTO();
						sentInternalEmailIds = new ArrayList<>();
						
						String ids = sentInternalEmail.getData().toString().replace("[", "").replace("]", "").replace(" ", "");
						
						if(ids != null){
			                for (String s : ids.split(",")){
		                        if(!StringUtils.isEmpty(s)) {
		                            sentInternalEmailIds.add(Long.parseLong(s));
		                        }
			                }
			                
			                updateInternalEmail = updateSentValueInternalEmail(sentInternalEmailIds);
			                
			                if (updateInternalEmail.getData().toString() == "true"){
			                	LogService.logger.info("Se actualizo el campo de envio de correo interno con exito en Incomplete Fifth Stage");
			                }else{
			                	LogService.logger.info("No se actualizo el campo de envio de correo interno con exito en Incomplete Fifth Stage");
			                }
						}
						
					}else{
						LogService.logger.info("No se obtuvo resultado exitoso enviando los correos internos en Incomplete Fifth Stage");
					}
				}else{
					LogService.logger.info("No hay oportunidades pendientes por enviar correo en Incomplete Fifth Stage");
				}
			}else{
				LogService.logger.info("No existen registros en Incomplete Fifth Stage en este momento");
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(opportunitiesList);
			
		} catch (Exception e) {
			LogService.logger.error("Error consultando Incomplete Fifth Stage Purchase Opportunities, ");
			e.printStackTrace();
		}
		
		return responseDTO;
	}
	
	
	
	/**
	 * Update 'Sent Internal Email' Field
	 * 
	 * @param List<Long> purchaseOpportunitiesIds
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO updateSentValueInternalEmail(List<Long> purchaseOpportunitiesIds){
		ResponseDTO responseDTO = new ResponseDTO();
		
		if (purchaseOpportunitiesIds != null){
			for(Long ids : purchaseOpportunitiesIds){
				Optional<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findById(ids);
				
				if (offerCheckpointBD.isPresent()){
					offerCheckpointBD.get().setSentInternalEmail(true);
					offerCheckpointRepo.save(offerCheckpointBD.get());
				}
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(true);
		}
		
		return responseDTO;
	}
	
}
