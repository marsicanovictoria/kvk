package com.kavak.core.service.netsuite;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.kavak.core.dto.InspectionDiscountDTO;
import com.kavak.core.dto.InspectionRepairDTO;
import com.kavak.core.dto.InspectionSummaryDTO;
import com.kavak.core.dto.response.InspectionNetsuiteDataResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.Inspection;
import com.kavak.core.model.InspectionDiscount;
import com.kavak.core.model.InspectionRepair;
import com.kavak.core.model.InspectionSummary;
import com.kavak.core.model.RepairCategory;
import com.kavak.core.model.RepairSubCategory;
import com.kavak.core.model.RepairType;
import com.kavak.core.repositories.InspectionDiscountRepository;
import com.kavak.core.repositories.InspectionRepairRepository;
import com.kavak.core.repositories.InspectionRepository;
import com.kavak.core.repositories.InspectionSummaryRepository;
import com.kavak.core.repositories.RepairCategoryRepository;
import com.kavak.core.repositories.RepairSubcategoryRepository;
import com.kavak.core.repositories.RepairTypeRepository;
import com.kavak.core.util.LogService;

@Stateless
public class InspectionsServices {

	@Inject
	private InspectionSummaryRepository inspectSummaryRepo;

	@Inject
	private InspectionRepository inspectionRepo;

	@Inject
	private InspectionRepairRepository inspectionRepairRepo;

	@Inject
	private RepairCategoryRepository repairCategoryRepo;

	@Inject
	private RepairSubcategoryRepository repairSubcategoryRepo;

	@Inject
	private RepairTypeRepository repairItemTypeRepo;

	@Inject
	private InspectionDiscountRepository inspectionDiscountRepo;

	@SuppressWarnings("finally")
	// public ResponseDTO getInspectionsForNetsuite(List<String> ids){
	public ResponseDTO getInspectionsForNetsuite() {
		ResponseDTO responseDTO = new ResponseDTO();
		InspectionNetsuiteDataResponseDTO inspectionResponseDTO = null;

		try {
			LogService.logger.info("Consultando nuevas inspecciones realizadas para ser enviados a Netsuite");

			List<InspectionNetsuiteDataResponseDTO> inspectionList = new ArrayList<>();
			List<InspectionRepairDTO> inspectionRepairList;
			List<InspectionSummaryDTO> inspectionSummaryList;
			List<InspectionDiscountDTO> inspectionDiscountsList;

			List<Inspection> inspectionListBD = inspectionRepo.findBySendNetsuite();

			if (inspectionListBD != null) {

				LogService.logger.info("Total nuevas inspecciones realizadas encontradas: " + inspectionListBD.size());

				for (Inspection inspection : inspectionListBD) {

					inspectionResponseDTO = new InspectionNetsuiteDataResponseDTO();
					inspectionSummaryList = new ArrayList<>();
					inspectionRepairList = new ArrayList<>();
					inspectionDiscountsList = new ArrayList<>();
					
					inspectionResponseDTO.setId(inspection.getId());
					inspectionResponseDTO.setMinervaOfferId(inspection.getIdOffertCheckPoint());
					inspectionResponseDTO.setInpectorComments(inspection.getComments());
					inspectionResponseDTO.setCanceled(inspection.isCanceled());
					inspectionResponseDTO.setIsCanceledReason(inspection.getCanceledReason());
					inspectionResponseDTO.setRescheduled(inspection.isRescheduled());
					inspectionResponseDTO.setIsRescheduledReason(inspection.getRescheduledReason());

					if (inspection.getCreationDate() != null) {
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date registerDateUnclean = formatter.parse(inspection.getCreationDate().toString());
						inspectionResponseDTO.setCreationDate(formatter.format(registerDateUnclean));

					}

					/**
					 * INSPECTION SUMMARY
					 * 
					 */

					LogService.logger.info("Buscando summary del inspection Id: " + inspection.getId());

					List<InspectionSummary> inspectionSummaryBD = inspectSummaryRepo.findByInspectionId(inspection.getId());

					if (inspectionSummaryBD.size() > 0) {

						for (InspectionSummary inspectionSummary : inspectionSummaryBD) {

							InspectionSummaryDTO inspectionSummaryDTO = new InspectionSummaryDTO();
							inspectionSummaryDTO.setId(inspectionSummary.getId());
							inspectionSummaryDTO.setIdInspection(inspectionSummary.getIdInspection());
							inspectionSummaryDTO.setInspectorEmail(inspectionSummary.getInspectorEmail());
							inspectionSummaryDTO.setIdOfferType(inspectionSummary.getIdOfferType());
							inspectionSummaryDTO.setBaseOfferAmount(inspectionSummary.getBaseOfferAmount());
							inspectionSummaryDTO.setTotalDiscount(inspectionSummary.getTotalDiscount());
							inspectionSummaryDTO.setFinalOfferAmount(inspectionSummary.getFinalOfferAmount());
							inspectionSummaryDTO.setInspectionApproved(inspectionSummary.isInspectionApproved());
							inspectionSummaryDTO.setInspectionStatus(inspectionSummary.getInspectionStatus());
							inspectionSummaryDTO.setSelected(inspectionSummary.isSelected());
							inspectionSummaryDTO.setTotalRepairsAmount(inspectionSummary.getTotalRepairsAmount());
							inspectionSummaryDTO.setTotalBonusAmount(inspectionSummary.getTotalBonusAmount());
							inspectionSummaryDTO.setCustomerSignatureImage(inspectionSummary.getCustomerSignatureImage());

							inspectionSummaryList.add(inspectionSummaryDTO);
						}

						inspectionResponseDTO.setInspectionSummaryList(inspectionSummaryList);
					} else {
						LogService.logger.info("No se encontro summary para el inspection ID: " + inspection.getId());
					}

					
					/**
					 * INSPECTIONS REPAIRS
					 * 
					 */

					LogService.logger.debug("Buscando inspections repair del inspection Id: " + inspection.getId());
					
					List<InspectionRepair> inspectionRepairBD = inspectionRepairRepo.findByInspectionId(inspection.getId());
					
					if (inspectionRepairBD.size() > 0){
						for(InspectionRepair inspectionRepair : inspectionRepairBD){
							
							InspectionRepairDTO inspectionRepairDTO = new InspectionRepairDTO();
							
							if (inspectionRepair.getIdInspectionRepairCategory() != 0){
								
								LogService.logger.debug("Buscando la categoria ID: " + inspectionRepair.getIdInspectionRepairCategory());
								
								Optional<RepairCategory> repairCategoryBD = repairCategoryRepo.findById(inspectionRepair.getIdInspectionRepairCategory());

								if (repairCategoryBD.isPresent()){
									inspectionRepairDTO.setId(inspectionRepair.getId());
									
									if (repairCategoryBD.get().getId() != null){
										inspectionRepairDTO.setIdInspectionRepairCategory(repairCategoryBD.get().getId());
										inspectionRepairDTO.setInspectionRepairCategoryDescription(repairCategoryBD.get().getName().toUpperCase());
									}

								}else{
									LogService.logger.debug("No existe categoria con ese ID: " + inspectionRepair.getIdInspectionRepairCategory());
								}
							}else{
								LogService.logger.debug("Este no posee categoria registrada ");
								
								if (inspectionRepair.getRepairOtherText() != null){
									inspectionRepairDTO.setRepairOtherText(inspectionRepair.getRepairOtherText().toUpperCase());
								}
							}

							if (inspectionRepair.getIdInspectionRepairSubCategory() != null){

								LogService.logger.debug("Buscando la subcategoria ID: " + inspectionRepair.getIdInspectionRepairSubCategory());
								
								Optional<RepairSubCategory> repairSubcategoryBD = repairSubcategoryRepo.findById(inspectionRepair.getIdInspectionRepairSubCategory());
								
								if (repairSubcategoryBD.isPresent()){
									inspectionRepairDTO.setIdInspectionRepairSubCategory(repairSubcategoryBD.get().getId());
									inspectionRepairDTO.setInspectionRepairSubCategoryDescription(repairSubcategoryBD.get().getName().toUpperCase());
								}
							}
							
							if (inspectionRepair.getIdInspectionRepairItem() != null){
								LogService.logger.debug("Buscando el item type ID : " + inspectionRepair.getIdInspectionRepairItem());
								
								Optional<RepairType> repairTypeDB = repairItemTypeRepo.findById(inspectionRepair.getIdInspectionRepairItem());
								
								if (repairTypeDB.isPresent()){
									inspectionRepairDTO.setIdInspectionRepairItem(repairTypeDB.get().getId());
									inspectionRepairDTO.setInspectionRepairItemDescription(repairTypeDB.get().getName().toUpperCase());
								}
							}
							
							inspectionRepairDTO.setId(inspectionRepair.getId());
							inspectionRepairDTO.setInspctionId(inspectionRepair.getIdInspection());
							inspectionRepairDTO.setRepairAmount(inspectionRepair.getRepairAmount().doubleValue());
							inspectionRepairDTO.setInspectorEmail(inspectionRepair.getInspectorEmail());

							inspectionRepairList.add(inspectionRepairDTO);
						}
						
						inspectionResponseDTO.setInspectionRepairList(inspectionRepairList);
					} else {
						LogService.logger.info("No se encontro repair inspection para el inspection ID: " + inspection.getId());
					}
					
					
					/**
					 * INSPECTIONS DISCOUNTS
					 * 
					 */

					LogService.logger.debug("Buscando inspections discounts del inspection Id: " + inspection.getId());
					
					List<InspectionDiscount> inspectionDiscountBD = inspectionDiscountRepo.findByIdInspection(inspection.getId());
					
					if (inspectionDiscountBD.size() > 0){
						
						LogService.logger.debug("Total de inspections discounts: " + inspectionDiscountBD.size());
					
						for(InspectionDiscount discounts : inspectionDiscountBD){
							
							InspectionDiscountDTO inspectionDicountDTO = new InspectionDiscountDTO();
							inspectionDicountDTO.setId(discounts.getId());
							inspectionDicountDTO.setIdInspection(discounts.getIdInspection());
							inspectionDicountDTO.setInspectorEmail(discounts.getInspectorEmail());
							inspectionDicountDTO.setDiscountType(discounts.getDiscountType());
							inspectionDicountDTO.setDiscountAmount(discounts.getDiscountAmount());
							inspectionDicountDTO.setPaidByKavak(discounts.isPaidByKavak());
							inspectionDicountDTO.setRemoved(discounts.isRemoved());
							inspectionDicountDTO.setRemovedReason(discounts.getRemovedReason());
							inspectionDicountDTO.setCreationDate(discounts.getCreationDate());
							
							inspectionDiscountsList.add(inspectionDicountDTO);
						}
					
						inspectionResponseDTO.setInspectionDiscountList(inspectionDiscountsList);
					} else {
						LogService.logger.info("No se encontro discount inspection para el inspection ID: " + inspection.getId());
					}
					
					inspectionList.add(inspectionResponseDTO);
				}

			} else {
				LogService.logger.info("Total nuevas inspecciones realizadas encontradas: 0");
			}

			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(inspectionList);

		} catch (Exception e) {
			LogService.logger.error("Error al intentar consultar la inspecciones pendientes por enviar a netsuite, " + e);
		} finally {
			return responseDTO;
		}

	}

	@SuppressWarnings("finally")
	public ResponseDTO putSendStatusInspection(String inspectionsIds) {
		ResponseDTO responseDTO = new ResponseDTO();
		long sentValue = 1;

		try {
			LogService.logger.info("Se procede a marcar inspecciones que ya fueron enviadas a netsuite.");

			List<Long> listInspectionIds = new ArrayList<>();

			if (!inspectionsIds.isEmpty()) {

				for (String s : inspectionsIds.split(",")) {
					listInspectionIds.add(Long.parseLong(s));
				}

				List<Inspection> inspectionListBD = inspectionRepo.findByInspectionIds(listInspectionIds);

				if (inspectionListBD != null) {
					LogService.logger
							.info("Total nuevas inspecciones para actualizar encontradas: " + inspectionListBD.size());

					for (Inspection inspection : inspectionListBD) {
						Optional<Inspection> inspectionBD = inspectionRepo.findById(inspection.getId());

						inspectionBD.get().setSendNetsuite(sentValue);
					}
					inspectionRepo.saveAll(inspectionListBD);
				} else {
					LogService.logger.info("Total nuevas inspecciones para actualizar encontradas: 0");
				}

				EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
				responseDTO.setCode(enumResult.getValue());
				responseDTO.setStatus(enumResult.getStatus());

			} else {
				LogService.logger.info("Total nuevas inspecciones para actualizar encontradas: 0");
			}

		} catch (Exception e) {
			LogService.logger.error("Error al intentar actualizar el estatus de enviado a netsuite de las inspecciones con IDs: " + inspectionsIds + ", " + e);
		} finally {
			return responseDTO;
		}
	}

}
