package com.kavak.core.service.netsuite;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.kavak.core.dto.CarDataDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.MetaValueDTO;
import com.kavak.core.dto.MinervaAndNetsuiteIdsDTO;
import com.kavak.core.dto.RequestNetsuiteDTO;
import com.kavak.core.dto.SaleCheckpointDTO;
import com.kavak.core.dto.ScheduleDateDTO;
import com.kavak.core.dto.SellCarDetailDTO;
import com.kavak.core.dto.UserDTO;
import com.kavak.core.dto.netsuite.SaleOpportunityResponseDTO;
import com.kavak.core.dto.response.CustomerResponseDTO;
import com.kavak.core.dto.response.NetsuiteOpportunityDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.AppointmentScheduleDate;
import com.kavak.core.model.CarData;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.AppointmentScheduleDateRepository;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.LogService;

@Stateless
public class SaleOpportunityServices {

	@Inject
	private SaleCheckpointRepository saleCheckpointRepo;
	
	@Inject
	private UserRepository userRepo;
	
	@Inject
	private CarDataRepository carDataRepo;
	
	@Inject
	private SellCarDetailRepository sellCarDetailRepo;
	
	@Inject
	private UserMetaRepository userMetaRepo;
	
	@Inject
	private AppointmentScheduleDateRepository scheduleDateRepo;
	
	@Inject
	private MetaValueRepository metaValueRepo;
	
	@Inject
	private MessageRepository messageRepo;
	
	
	/**
	 * Get new pending Sales Opportunities for sending to Netsuite
	 * 
	 * @author Antony Delgado
	 * @param 
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO getNewSalesOpportunities(){
        ResponseDTO responseListDTO = new ResponseDTO();
		SaleCheckpointDTO saleCheckpointDTO = null;
		
		try {
			LogService.logger.info("Consultando nuevas oportunidades de venta para ser enviados a Netsuite");
			
			List<SaleOpportunityResponseDTO> listSaleOpportunityResponseDTO = new ArrayList<>();
			List<SaleCheckpoint> listSaleOpportunityBD       = saleCheckpointRepo.findBySentNetsuite();
			List<SaleCheckpoint> listResendSaleOpportunityBD = saleCheckpointRepo.findByResendNetsuite();
			
			if (listSaleOpportunityBD != null){
				LogService.logger.info("Total Sale Opportunities encontradas para enviar a Netsuite: " + listSaleOpportunityBD.size());
				
				for (SaleCheckpoint saleOpportunity : listSaleOpportunityBD){
					LogService.logger.info("Ids de Sales Opportunites: " + saleOpportunity.getId());
				}
				
				for (SaleCheckpoint saleOpportunity : listSaleOpportunityBD){
					
					if (saleOpportunity != null){
						saleCheckpointDTO = new SaleCheckpointDTO();
						saleCheckpointDTO = saleOpportunity.getDTO();
						
						SaleOpportunityResponseDTO saleOpportunityDTO = new SaleOpportunityResponseDTO();
						
						//LogService.logger.info("Transfiriendo de SaleCheckpointDTO a saleOpportunityDTO la oferta ID a enviar: " + saleOpportunity.getId());
						saleOpportunityDTO = transferSaleCheckpointToSaleOpportunity(saleCheckpointDTO);
						listSaleOpportunityResponseDTO.add(saleOpportunityDTO);
					}
				}
			}else{
				LogService.logger.info("Total Sales Opportunities encontradas para enviar a Netsuite: 0");
			}
			
			if ((listResendSaleOpportunityBD != null) && (listResendSaleOpportunityBD.size() > 0)){
				LogService.logger.info("Total Sale Opportunities encontradas para reenviar a Netsuite: " + listResendSaleOpportunityBD.size());
				
				for (SaleCheckpoint resendSaleOpportunity : listResendSaleOpportunityBD){
					
					if (resendSaleOpportunity != null){
						saleCheckpointDTO = new SaleCheckpointDTO();
						saleCheckpointDTO = resendSaleOpportunity.getDTO();
						
						SaleOpportunityResponseDTO saleOpportunityDTO = new SaleOpportunityResponseDTO();
						
						//LogService.logger.info("Transfiriendo de SaleCheckpointDTO a saleOpportunityDTO la oferta ID a reenviar: " + resendSaleOpportunity.getId());
						saleOpportunityDTO = transferSaleCheckpointToSaleOpportunity(saleCheckpointDTO);
						listSaleOpportunityResponseDTO.add(saleOpportunityDTO);
					}
				}
			}else{
				LogService.logger.info("Total Sale Opportunities encontradas para reenviar a Netsuite: 0");
			}
			
			Collections.sort(listSaleOpportunityResponseDTO, (o1, o2) -> o1.getId() - o2.getId());
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseListDTO.setCode(enumResult.getValue());
	        responseListDTO.setStatus(enumResult.getStatus());
	        responseListDTO.setData(listSaleOpportunityResponseDTO);
	        responseListDTO.setNetsuiteMethod(Constants.SALE_OPPORTUNITY_METHOD);
			
			LogService.logger.info("Consultando nuevas oportunidades de venta para ser enviados a Netsuite, exitoso.");
		} catch (Exception e) {
			LogService.logger.error("Error al intentar consultar los nuevos Sales Opportunities ");
			e.printStackTrace();
		}finally{
			LogService.logger.debug("Fin de consulta de Sales Opportunities. ");
		}
		
		return responseListDTO;
	}
	
	
	/**
	 * CURRENT STABLE VERSION
	 * 
	 * Update status Sales Opportunities (sent to Netsuite).
	 * 
	 * @author Antony Delgado
	 * @param String salesOpportunitiesId
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO putSentSalesOpportunities(String salesOpportunitiesId){
		ResponseDTO responseDTO = null;
		
		try {
			if (!salesOpportunitiesId.isEmpty()){
				
				salesOpportunitiesId = salesOpportunitiesId.replaceAll("\"","").replaceAll("code:200,status:OK,data:", "").replaceAll("\\{", "").replaceAll("\\}", "");

				LogService.logger.info("IDs de Sale Opportunities a actualizar como enviado en BD " + salesOpportunitiesId);
				
				responseDTO = new ResponseDTO();
				
				if (!salesOpportunitiesId.equals("null")){
					
					List<String> items = Arrays.asList(salesOpportunitiesId.split("\\s*,\\s*"));
					
					for (String idsAndStatus : items){
						
						String[] parts = idsAndStatus.split(":");
						
						Long opportunityId = Long.valueOf(parts[0]);
						Long statusOpportunityId = Long.valueOf(parts[1]);
						
						LogService.logger.info("Buscando purchase oportunidad ID " + opportunityId);
					
						Optional<SaleCheckpoint> saleCheckpointBD = saleCheckpointRepo.findById(opportunityId);
						
						if (saleCheckpointBD.isPresent()){
							
							if (saleCheckpointBD.get().getStatus().longValue() == statusOpportunityId.longValue()){
								LogService.logger.info("Sale Opportunity ID " + opportunityId + " con status encontrado en BD: " + saleCheckpointBD.get().getStatus() + ", status que trae desde netsuite " + statusOpportunityId + ", se marca en 1");
								saleCheckpointBD.get().setSentNetsuite(1L);
							}else{
								LogService.logger.info("Sale Opportunity ID " + opportunityId + " con status encontrado en BD: " + saleCheckpointBD.get().getStatus() + ", status que trae desde netsuite " + statusOpportunityId + ", se marca en 2");
								saleCheckpointBD.get().setSentNetsuite(2L);
							}
							
							saleCheckpointRepo.save(saleCheckpointBD.get());
							
							LogService.logger.info("Actualizando Sale Opportunity ID " + opportunityId + " con exito" );
						}
					}
					
				}

		        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		        responseDTO.setCode(enumResult.getValue());
		        responseDTO.setStatus(enumResult.getStatus());
			}

			
		} catch (Exception e) {
			LogService.logger.error("Error al intentar actualizar el estatus de envío para netsuite de Sales Opportunities, " + e);
			e.printStackTrace();
		}
		
		return responseDTO;
	}
	
	
	
	/**
	 * DISABLED VERSION
	 * 
	 * Update status Sales Opportunities (sent to Netsuite).
	 * 
	 * @author Antony Delgado
	 * @param String salesOpportunitiesId
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO putSentSalesOpportunities(){
		ResponseDTO responseDTO = null;
		int minutes       = 2;
		long sentValue    = 1;
		
		try {
			LogService.logger.info("Actualizando estatus de envio de Netsuite (Sale Opportunity) mayores a 15 minutos ");
			responseDTO = new ResponseDTO();
			
			MetaValue metaValueBD = metaValueRepo.findByAlias("MINUTES_SALES_CHECKPOINT");
			
			if (metaValueBD != null){
				minutes =   Integer.parseInt(metaValueBD.getOptionName());
			}
			
			List<SaleCheckpoint> saleOpportunityBD = saleCheckpointRepo.findBySentNetsuitePastMinute(minutes);
			
			if (saleOpportunityBD.size() > 0){
				
				LogService.logger.info("Cantidad de Sale Opportunities por actualizar estatus de enviado: " + saleOpportunityBD.size());
				
				for(SaleCheckpoint saleCheck : saleOpportunityBD){
					saleCheck.setSentNetsuite(sentValue);
				}
				saleCheckpointRepo.saveAll(saleOpportunityBD);
			}else{
				LogService.logger.info("Cantidad de Sale Opportunities por actualizar estatus de enviado: 0");
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        
		} catch (Exception e) {
			LogService.logger.error("Error al intentar actualizar el estatus de envío para netsuite de Sales Opportunities mayores a 15 minutos, " + e);
			e.printStackTrace();
		}
		
		return responseDTO;
	}
	
	
	/**
	 * Transfer data from SaleCheckpointDTO to SaleOpportunityResponseDTO in order to send to Netsuite.
	 * 
	 * @author Antony Delgado
	 * @param SaleCheckpointDTO saleCheckpointDTO
	 * @return SaleOpportunityResponseDTO saleOpportunityDTO
	 */
	
	@SuppressWarnings("finally")
	public SaleOpportunityResponseDTO transferSaleCheckpointToSaleOpportunity(SaleCheckpointDTO saleCheckpointDTO){
		SaleOpportunityResponseDTO saleOpportunityDTO = null;
		Long opportunityTypeId	= null;
		Long stockId			= null;
		
		try {
			saleOpportunityDTO = new SaleOpportunityResponseDTO();
			
			Optional<User> userBD = userRepo.findById(saleCheckpointDTO.getIdUser());
			
			

			if (userBD.isPresent()){
				saleOpportunityDTO.setCustomer(new CustomerResponseDTO());
				saleOpportunityDTO.getCustomer().setId(userBD.get().getId());
				saleOpportunityDTO.getCustomer().setName(userBD.get().getName());
				saleOpportunityDTO.getCustomer().setUsername(userBD.get().getUserName());
				saleOpportunityDTO.getCustomer().setEmail(userBD.get().getEmail());
				saleOpportunityDTO.getCustomer().setRole(userBD.get().getRole());
				
				UserMeta userPhoneBD = userMetaRepo.findUserMetaByMetaKeyAndId(Constants.META_VALUE_PHONE, userBD.get().getId());
				if (userPhoneBD != null){
					saleOpportunityDTO.getCustomer().setPhone(userPhoneBD.getMetaValue());
				}
				
				UserMeta userAddressBD = userMetaRepo.findUserMetaByMetaKeyAndId(Constants.META_VALUE_ADDRESS, userBD.get().getId());
				if (userAddressBD != null){ 
					saleOpportunityDTO.getCustomer().setAddressNetsuite(userAddressBD.getMetaValue());
				}
			}
			
			// NORMAL PROCEDURE
			if(saleCheckpointDTO.getCarId() > 0){
				CarData carDataBD     = carDataRepo.findBySku(saleCheckpointDTO.getSku());
				Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(saleCheckpointDTO.getCarId());
				
				
				if (carDataBD != null){
					saleOpportunityDTO.setCar(new CarDataDTO());
					saleOpportunityDTO.setCar(carDataBD.getDTO());
				}
				
				if (sellCarDetailBD.isPresent()){
					SellCarDetailDTO sellCarDetailDTO = sellCarDetailBD.get().getDTO();
					saleOpportunityDTO.setNetsuiteItemId(sellCarDetailDTO.getNetsuiteItemId());
					saleOpportunityDTO.setCarCost(sellCarDetailDTO.getPrice().longValue());
					saleOpportunityDTO.setFakeBooking(sellCarDetailBD.get().getFakeBooking());
					stockId = sellCarDetailBD.get().getId();
				}else{
					LogService.logger.info("No existe el auto consultado, por lo tanto no hay codigo_item_netsuite");
				}
				
				
				
			// ARBOL FINANCIERO - AUTO FICTICIO
			}else{
				saleOpportunityDTO.setNetsuiteItemId(saleCheckpointDTO.getId());
				saleOpportunityDTO.setFakeBooking(0L);
				stockId = saleCheckpointDTO.getCarId();
				saleOpportunityDTO.setCarCost(1L);
			}

			saleOpportunityDTO.setId(saleCheckpointDTO.getId().intValue());
			saleOpportunityDTO.setStockId(stockId);
			saleOpportunityDTO.setCarKm(saleCheckpointDTO.getCarKm());
			saleOpportunityDTO.setShippingAddressId(saleCheckpointDTO.getShippingAddressId());
			
			if (saleCheckpointDTO.getShippingAddressDescription() != null){
				saleOpportunityDTO.setShippingAddressDescription(saleCheckpointDTO.getShippingAddressDescription().replaceAll("\n", "").replaceAll("\r", "").replaceAll("\b", "").replaceAll("\t", ""));
			}
			
			saleOpportunityDTO.setWarrantyName(saleCheckpointDTO.getWarrantyName());
			saleOpportunityDTO.setWarrantyTime(saleCheckpointDTO.getWarrantyTime());
			saleOpportunityDTO.setWarrantyAmount(saleCheckpointDTO.getWarrantyAmount());
			saleOpportunityDTO.setWarrantyRange(saleCheckpointDTO.getWarrantyRange());
			saleOpportunityDTO.setInsuranceName(saleCheckpointDTO.getInsuranceName());
			saleOpportunityDTO.setInsuranceTime(saleCheckpointDTO.getInsuranceTime());
			saleOpportunityDTO.setInsuranceAmount(saleCheckpointDTO.getInsuranceAmount());
			saleOpportunityDTO.setInsuranceModality(saleCheckpointDTO.getInsuranceModality());
			saleOpportunityDTO.setPaymentMethodTypeId(saleCheckpointDTO.getPaymentMethodTypeId());
			saleOpportunityDTO.setIsFinanced(saleCheckpointDTO.getIsFinanced());
			saleOpportunityDTO.setIsFinancingCompleted(saleCheckpointDTO.getIsFinancingCompleted());
			saleOpportunityDTO.setDownPayment(saleCheckpointDTO.getDownPayment());
			saleOpportunityDTO.setAmountFinancing(saleCheckpointDTO.getAmountFinancing());
			saleOpportunityDTO.setMonthFinancing(saleCheckpointDTO.getMonthFinancing());
			saleOpportunityDTO.setCheckoutResult(saleCheckpointDTO.getCheckoutResult());
			saleOpportunityDTO.setPaymentPlatform(saleCheckpointDTO.getPaymentPlatform());
			saleOpportunityDTO.setPaymentPlatformId(saleCheckpointDTO.getPaymentPlatformId());
			saleOpportunityDTO.setTransactionNumber(saleCheckpointDTO.getTransactionNumber());
			saleOpportunityDTO.setPaymentStatus(saleCheckpointDTO.getPaymentStatus());
			saleOpportunityDTO.setTotalAmount(saleCheckpointDTO.getTotalAmount());
			saleOpportunityDTO.setCarReserve(saleCheckpointDTO.getCarReserve());
			saleOpportunityDTO.setCarShippingCost(saleCheckpointDTO.getCarShippingCost());
			saleOpportunityDTO.setRegisterDate(saleCheckpointDTO.getRegisterDate());
			saleOpportunityDTO.setUpdateDate(saleCheckpointDTO.getUpdateDate());
			saleOpportunityDTO.setDuplicatedSaleControl(saleCheckpointDTO.getDuplicatedOfferControl());
			saleOpportunityDTO.setSaleOpportunityTypeId(saleCheckpointDTO.getSaleOpportunityTypeId());
			saleOpportunityDTO.setAppointmentContact(saleCheckpointDTO.getAppointmentContact());
			saleOpportunityDTO.setAppoinmentDate(saleCheckpointDTO.getAppointmentDate());
			saleOpportunityDTO.setWatchedCar(saleCheckpointDTO.getWatchedCar());
			saleOpportunityDTO.setAppliedCoupon(saleCheckpointDTO.getAppliedCoupon());
			
			if(saleCheckpointDTO.getComments() != null) {
			    saleOpportunityDTO.setComments(saleCheckpointDTO.getComments().replaceAll("\n", "").replaceAll("\r", "").replaceAll("\b", "").replaceAll("\t", ""));
			}
			if(saleCheckpointDTO.getOtherInterestedCar() != null){
				saleOpportunityDTO.setOtherInterestedCar(saleCheckpointDTO.getOtherInterestedCar().replaceAll("\n", "").replaceAll("\r", "").replaceAll("\b", "").replaceAll("\t", ""));
			}

			if (saleCheckpointDTO.getScheduleDateId() != null){
				saleOpportunityDTO.setVisitDateId(saleCheckpointDTO.getScheduleDateId());
				
				Optional<AppointmentScheduleDate> scheduleDateBD = scheduleDateRepo.findById(saleCheckpointDTO.getScheduleDateId());
				
				if (scheduleDateBD.isPresent()){
					ScheduleDateDTO scheduleDateDTO = new ScheduleDateDTO();
					scheduleDateDTO = scheduleDateBD.get().getDTO();
					saleOpportunityDTO.setVisitDate(scheduleDateDTO.getVisitDate());
					saleOpportunityDTO.setVisitHour(scheduleDateDTO.getHourSlotDescription());
				}
			}
			
			if (saleCheckpointDTO.getStatus() != null){
				Optional<MetaValue> metaValueBD = metaValueRepo.findById(saleCheckpointDTO.getStatus());
				
				if (metaValueBD.isPresent()){
					MetaValueDTO metaValueDTO = metaValueBD.get().getDTO();
					saleOpportunityDTO.setStatusId(saleCheckpointDTO.getStatus());
					saleOpportunityDTO.setStatusDescription(metaValueDTO.getOptionName());
				}
			}
			
			if (saleCheckpointDTO.getZipCode() != null && StringUtils.isNumeric(saleCheckpointDTO.getZipCode()) && !StringUtils.isEmpty(saleCheckpointDTO.getZipCode())){
				Long zipCode = Long.parseLong(saleCheckpointDTO.getZipCode());
				saleOpportunityDTO.setZipCode(zipCode);
			}
			
			if (saleCheckpointDTO.getSaleOpportunityTypeId() != null){
				if(saleCheckpointDTO.getSaleOpportunityTypeId() == 200){ 		 // CITA SIN RESERVA
					opportunityTypeId = 280L;
				}else if (saleCheckpointDTO.getSaleOpportunityTypeId() == 201){  // CHECKOUT
					opportunityTypeId = 279L;
				}else if (saleCheckpointDTO.getSaleOpportunityTypeId() == 245){  // RESERVA POR MINERVA
					opportunityTypeId = 279L;
				}else if (saleCheckpointDTO.getSaleOpportunityTypeId() == 275){  // CITA CON RESERVA
					opportunityTypeId = 281L;
				}else if (saleCheckpointDTO.getSaleOpportunityTypeId() == 277){  // NOTIFICACION INBOUND
					opportunityTypeId = 300L;
				}else if (saleCheckpointDTO.getSaleOpportunityTypeId() == 296){  // RESERVA FICTICIA
					opportunityTypeId = 279L;
				}else if(saleCheckpointDTO.getSaleOpportunityTypeId() == 316){   // ARBOL FINANCIERO
					opportunityTypeId = 316L;
				}
				
				Optional<MetaValue> metaValueBD = metaValueRepo.findById(opportunityTypeId);
				
				if (metaValueBD.isPresent()){
					saleOpportunityDTO.setChannel(metaValueBD.get().getOptionName().toUpperCase());
					//saleOpportunityDTO.setPlatform(metaValueBD.get().getGroupName().toUpperCase());
					
				}
			}
			
			if(saleCheckpointDTO.getSource()!= null && !saleCheckpointDTO.getSource().isEmpty()){
				saleOpportunityDTO.setPlatform(saleCheckpointDTO.getSource().toUpperCase());
			}else{
				if(saleOpportunityDTO.getChannel().toUpperCase().equals("PERFILAMIENTO ARBOL")){
					saleOpportunityDTO.setPlatform("IOS_APP");
				}else{
					saleOpportunityDTO.setPlatform("MINERVA");
				}
			}
			
			if(saleCheckpointDTO.getInternalUserId() != null){
				Optional<User> wingmanBD = userRepo.findById(saleCheckpointDTO.getInternalUserId());
				
				if (wingmanBD.isPresent()){
					saleOpportunityDTO.setWingmanCheckout(new UserDTO());
					
					saleOpportunityDTO.getWingmanCheckout().setId(wingmanBD.get().getId());
					saleOpportunityDTO.getWingmanCheckout().setName(wingmanBD.get().getName());
					saleOpportunityDTO.getWingmanCheckout().setEmail(wingmanBD.get().getEmail());
					saleOpportunityDTO.getWingmanCheckout().setUsername(wingmanBD.get().getUserName());
					saleOpportunityDTO.getWingmanCheckout().setRole(wingmanBD.get().getRole());
				}
			}
			
		} catch (Exception e) {
			LogService.logger.error("Error al intentar transferir DTO SaleCheckpoint to SaleOpportunity ");
			e.printStackTrace();
		}finally{
			return saleOpportunityDTO;
		}
	}
	
	
	
	/**
	 * Clean String data of $,. characters
	 * 
	 * @author Antony Delgado
	 * @param String dirtyValue
	 * @return String clearmax30OfferLevel3
	 */
	
	public String clearCharacters(String dirtyValue){
		
		String clearmax30OfferLevel1 = dirtyValue.replace("$", "");
		String clearmax30OfferLevel2 = clearmax30OfferLevel1.replace(",", "");
		String clearmax30OfferLevel3 = clearmax30OfferLevel2.replace(".", "");
		
		return clearmax30OfferLevel3;
	}
	
	
	/**
	 * Retrieving Sales Opportunities Without Netsuite Id in compra_checkpoints fields
	 * 
	 * @author Antony Delgado
	 * @param 
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO getSaleOpportunityWithoutNetsuiteId(){
		ResponseDTO responseDTO = new ResponseDTO();
		ArrayList<String> minervasIds = null;
		int quantityMinervaIds = 10; 
		
		try {
			LogService.logger.info("Consultando las oportunidades de venta sin Netsuite ID" );
			@SuppressWarnings("deprecation")
			Pageable pageable = new PageRequest(1, 10);
			
			Page<SaleCheckpoint> listofferCheckpointBD = saleCheckpointRepo.findByNetsuiteOpportunityIdEmpty(pageable);
			
			if (listofferCheckpointBD != null){
				
				minervasIds = new ArrayList<>();
				int counter = 0;
				for(SaleCheckpoint minervaIds : listofferCheckpointBD){
					if (counter < quantityMinervaIds){
						minervasIds.add(minervaIds.getId().toString());
					}else{
						break;
					}
					counter++;
				}
			}else{
				LogService.logger.info("Total de oportunidades de venta sin Netsuite ID: 0");
			}
			
			LogService.logger.debug("IDs Minerva recuperados" + minervasIds);
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());;
	        responseDTO.setData(minervasIds);
			
		} catch (Exception e) {
			LogService.logger.error("Error al intentar consultar Sale Opportunity ID Without Netsuite Id");
			e.printStackTrace();
		}
		
		return responseDTO;
	}
	
	
	public ResponseDTO putNetsuiteOpportunityIdIntoMinerva(String netsuiteAndMinervaIds){
		ResponseDTO responseDTO = new ResponseDTO();
		
		LogService.logger.info("Se procede a actualizar campos netsuite_id y URL de la oportunidad en Netsuite ");
		
		
		if (!netsuiteAndMinervaIds.isEmpty()){
			try {
				netsuiteAndMinervaIds = netsuiteAndMinervaIds.replace("\"", "'").replace("{'code':200,'status':'OK','data':[{", "").replace("}]}", "").replace("},{", ",").replace("'", "").replace(",minerva_id", "#minerva_id");
				
				List<MinervaAndNetsuiteIdsDTO> minervaNetsuiteListDTO = new ArrayList<>();
				
				String[] pairs = netsuiteAndMinervaIds.split("#");
				
				for (int i=0;i<pairs.length;i++) {
				    String pair       = pairs[i];
				    String[] keyValue = pair.split(",");
				    String[] minerva  = keyValue[0].split(":");
				    String[] netsuite = keyValue[1].split(":");

				    MinervaAndNetsuiteIdsDTO netsuiteMinervaId = new MinervaAndNetsuiteIdsDTO();
				    netsuiteMinervaId.setMinervaId(Long.valueOf(minerva[1]));
				    netsuiteMinervaId.setNetsuiteId(Long.valueOf(netsuite[1]));
				    
				    minervaNetsuiteListDTO.add(netsuiteMinervaId);
				}

				for (MinervaAndNetsuiteIdsDTO mnIds : minervaNetsuiteListDTO){
					
					Optional<SaleCheckpoint> saleCheckpointBD = saleCheckpointRepo.findById(mnIds.getMinervaId());
					
					if (saleCheckpointBD.isPresent()){
						
						saleCheckpointBD.get().setNetsuiteOpportunityId(mnIds.getNetsuiteId());
						saleCheckpointBD.get().setNetsuiteIdUpdateDate(new Timestamp(System.currentTimeMillis()));
						
						if (mnIds.getNetsuiteId() != 0){
							saleCheckpointBD.get().setNetsuiteOpportunityURL(Constants.NETSUITE_OPPORTUNITY_URL + mnIds.getNetsuiteId());
						}
						
						saleCheckpointRepo.save(saleCheckpointBD.get());
					}
				}

				LogService.logger.info("Actualizados campos netsuite_id y URL oportunidad de Netsuite, exitoso.");
				
			} catch (Exception e) {
				LogService.logger.error("Error al intentar actualizar Netsuite Opportunity Id en la tabla oferta_checkpoints, " + e);
				e.printStackTrace();
			}
		}
		return responseDTO;
	}
	
	
	/**
	 * Create a Sale Opportunity By Customer Notification
	 * 
	 * @author Antony Delgado
	 * @param Long userId 
	 * @param Long carId
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO createSaleOpportunityByCustomerNotification(Long userId, Long carId){
		ResponseDTO responseDTO = new ResponseDTO();
		
		try {
			if (userId != null && carId != null){
				
				Optional<User> userBD = userRepo.findById(userId);
				Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(carId);
				
				if (userBD.isPresent() && sellCarDetailBD.isPresent()){
					
					CarData carDataBD = carDataRepo.findBySku(sellCarDetailBD.get().getSku());
					String[] identityUser = userBD.get().getName().split(" ");
					
					SaleCheckpoint saleCheckpoint = new SaleCheckpoint();
					
					saleCheckpoint.setUserId(userBD.get().getId());
					saleCheckpoint.setUser(userBD.get());
					saleCheckpoint.setCustomerName(identityUser[0]);
					saleCheckpoint.setCustomerLastName(identityUser[1]);
					saleCheckpoint.setEmail(userBD.get().getEmail());
					saleCheckpoint.setCarId(sellCarDetailBD.get().getId());
					saleCheckpoint.setSku(sellCarDetailBD.get().getSku());
					saleCheckpoint.setStatus(276L);
					saleCheckpoint.setCarYear(Long.parseLong(sellCarDetailBD.get().getCarYear()));
					saleCheckpoint.setCarMake(sellCarDetailBD.get().getCarMake());
					saleCheckpoint.setCarModel(sellCarDetailBD.get().getCarModel());
					saleCheckpoint.setCarVersion(carDataBD.getCarTrim());
					saleCheckpoint.setCarKm(Long.parseLong(sellCarDetailBD.get().getCarKm()));
					saleCheckpoint.setCarCost(sellCarDetailBD.get().getPrice().toString());
					saleCheckpoint.setRegisterDate(new Timestamp(System.currentTimeMillis()));
					saleCheckpoint.setUpdateDate(new Timestamp(System.currentTimeMillis()));
					saleCheckpoint.setSaleOpportunityTypeId(277L);
					saleCheckpoint.setSendEmail(true);
					saleCheckpoint.setSource("notificacion");
					saleCheckpoint.setAppointmentEmailSent(true);
					saleCheckpoint.setScheduledAppointmentSmsSent(true);
					saleCheckpoint.setBookedCarSmsSent(true);
					saleCheckpoint.setSentNetsuite(0L);
					
					SaleCheckpoint newSaleCheckpoint = saleCheckpointRepo.save(saleCheckpoint);
					
					//Actualizamos el registro que se acaba de generar, el campo "control_oferta_duplicada"
					Optional<SaleCheckpoint> existingCheckpointBD = saleCheckpointRepo.findById(newSaleCheckpoint.getId());
					
					if (existingCheckpointBD.isPresent()){
						existingCheckpointBD.get().setDuplicatedOfferControl(newSaleCheckpoint.getId());
						saleCheckpointRepo.save(existingCheckpointBD.get());
					}
					
			        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
			        responseDTO.setCode(enumResult.getValue());
			        responseDTO.setStatus(enumResult.getStatus());;
			        responseDTO.setData(existingCheckpointBD.get().getId());
					
					
				}else if (!userBD.isPresent()){
					LogService.logger.info("No existe registro del usuario ID " + userId);
				}else{
					LogService.logger.info("No existe registro del auto ID " + carId);
				}
				
			}else if (userId == null){
				LogService.logger.info("userId es nulo, se descarta crear Purchase Opportunity By Customer Notification");
			}else{
				LogService.logger.info("carId es nulo, se descarta crear Purchase Opportunity By Customer Notification");
			}
			
		} catch (Exception e) {
			LogService.logger.error("Error al intentar crear un Purchase Opportunity a partir de un Customer Notification, " + e);
			e.printStackTrace();
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());;
	        responseDTO.setData(null);
		}
		
		return responseDTO;
	}
	
	
	/**
	 * Send Sale Opportunities from BD to NETSUITE (Without a middleware)
	 * 
	 * @author Maria Victoria Marsicano
	 * @param 
	 * @return ResponseDTO responseDTO
	 */
	
	@Transactional
	public ResponseDTO salesOpportunitiesAuthNetsuite(){
		ResponseEntity<RequestNetsuiteDTO> response = null;
		ResponseDTO responseDTO = new ResponseDTO();

		ResponseDTO body = new ResponseDTO();
		boolean result = false;	
		
		ResponseDTO saleOpportunityRequest = getNewSalesOpportunities();

		try {
							
				EndPointCodeResponseEnum inEnumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
				body.setCode(inEnumResult.getValue());
				body.setStatus(inEnumResult.getStatus());
				body.setNetsuiteMethod(Constants.SALE_OPPORTUNITY_METHOD);
				body.setData(saleOpportunityRequest.getData());
				
				RestTemplate rt = new RestTemplate();
				String uri = Constants.NETSUITE_INTEGRATION_URI;
				String plainCreds = Constants.NETSUITE_INTEGRATION_HEADER;
				
				HttpHeaders headers = new HttpHeaders();
				headers.add("Authorization", plainCreds);
				headers.add("Content-Type","application/json");
				
				HttpEntity<Object> request = new HttpEntity<Object>(body,headers);
				
				response = rt.exchange(uri, HttpMethod.POST, request, RequestNetsuiteDTO.class);
				
				if (response.getStatusCode() == HttpStatus.OK){
					
					try{
						EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
						RequestNetsuiteDTO requestNetsuite = new RequestNetsuiteDTO();
						requestNetsuite.setCode(enumResult.getValue());
						requestNetsuite.setStatus(enumResult.getStatus());
						requestNetsuite.setData(response.getBody().getData());
						//LA RESPUESTA OBTENIDA SE ENVIA AL SERVICIO PUT PARA ACTUALIZAR SU STATUS EN BD
						ResponseDTO responsePut = putSaleOpportunity(requestNetsuite);
						
						if(responsePut.getCode() == EndPointCodeResponseEnum.C0201.getValue()){
							result = true;
						}
						
					}catch (Exception ez){
						LogService.logger.error("Ocurrio un error al llamar al metodo saleOpportunityDTO, " + ez);
						ez.printStackTrace();
					}
					
				}else{
					LogService.logger.info("El servicio de envio de saleOpportunity hacia netsuite, fallo. Body: " + response.getBody());
				}
				
			
		} catch (Exception ex) {
			LogService.logger.error("Ocurrio un error intentando enviar saleOpportunityDTO a Netsuite desde la Capa de Servicicos, " + ex);
			ex.printStackTrace();
		}
		
		
		
		if (result){
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(saleOpportunityRequest.getData());
		}else{
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(null);

		}
		
		return responseDTO;
	}
	
	
	
	/**
	 * Update status Sale Opportunities sent to Netsuite
	 * 
	 * @author Maria Victoria Marsicano
	 * @param 
	 * @return ResponseDTO responseDTO
	 */
	
	
	@Transactional
	public ResponseDTO putSaleOpportunity(RequestNetsuiteDTO requestNetsuite) throws InterruptedException {
		
		ResponseDTO responseDTO = new ResponseDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();
		
		
		//ResponseNetsuite responseNetsuite = new ResponseNetsuite(); 
		
		// ES NECESARIO ITERAR LOS VALORES QUE TRAE "DATA" EN requestNetsuite
		
		for (NetsuiteOpportunityDTO netsuiteOppo : requestNetsuite.getData()){
			
			LogService.logger.info("Id Minerva a Consultar: " + netsuiteOppo.getIdMinerva());
			Optional<SaleCheckpoint> saleCheckpointBD = saleCheckpointRepo.findById(netsuiteOppo.getIdMinerva());
				
		if (!saleCheckpointBD.isPresent()){
			
			LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
			MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0075.toString()).getDTO();
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new ArrayList<>());

			return responseDTO;
		}
		
		LogService.logger.info("------ Estatus obtenido de BD de la oppo ID " + saleCheckpointBD.get().getId() + " tiene status " + saleCheckpointBD.get().getStatus());
		LogService.logger.info("------ Estatus que viene de la opp ID " + netsuiteOppo.getIdMinerva() + " tiene status " + netsuiteOppo.getIdMinervaStatus());
		
		if(netsuiteOppo.getIdMinervaStatus().equals(saleCheckpointBD.get().getStatus())){	
			LogService.logger.info("------ Tiene el mismo estatus, se marca en 1");
			
			saleCheckpointBD.get().setNetsuiteOpportunityId(netsuiteOppo.getNetsuiteOpportunityId());
			saleCheckpointBD.get().setNetsuiteOpportunityURL(Constants.NETSUITE_OPPORTUNITY_URL+netsuiteOppo.getNetsuiteInternalId());
			saleCheckpointBD.get().setSentNetsuite(1L);
			saleCheckpointRepo.save(saleCheckpointBD.get());
			
		}else{
			LogService.logger.info("------ Tiene diferente estatus, se marca en 2");
			
			saleCheckpointBD.get().setNetsuiteOpportunityId(netsuiteOppo.getNetsuiteOpportunityId());
			saleCheckpointBD.get().setNetsuiteOpportunityURL(Constants.NETSUITE_OPPORTUNITY_URL+netsuiteOppo.getNetsuiteInternalId());
			saleCheckpointBD.get().setSentNetsuite(2L);
			saleCheckpointRepo.save(saleCheckpointBD.get());
		}
	}
		
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(requestNetsuite.getData());
		
		return responseDTO;
		
	}
	
	
}
