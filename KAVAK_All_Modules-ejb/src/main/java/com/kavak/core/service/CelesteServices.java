package com.kavak.core.service;

import java.io.File;
import java.net.Inet4Address;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.WordUtils;

import com.kavak.core.dto.PurchaseInternalEmailDTO;
import com.kavak.core.dto.netsuite.ConfirmedInspectionNetsuiteDTO;
import com.kavak.core.dto.netsuite.PaperworkCustomerEmailDTO;
import com.kavak.core.dto.netsuite.PaperworkInternalEmailDTO;
import com.kavak.core.dto.netsuite.PaperworkShippingManagerDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.CarData;
import com.kavak.core.model.ConfirmedInspections;
import com.kavak.core.model.CustomerNotifications;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.ConfirmedInspectionRepository;
import com.kavak.core.repositories.CustomerNotificationsRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;


@Stateless
public class CelesteServices {
	
	@EJB(mappedName = LookUpNames.SESSION_GenerateExcelFileServices)
	GenerateExcelFileServices generateExcelFileServices;
	
	@Inject
	private CarDataRepository carDataRepo;
	
	@Inject
	private CustomerNotificationsRepository customerNotificationRepo;
	
	@Inject
	private UserMetaRepository userMetaRepo;
	
	@Inject
	private SellCarDetailRepository sellCarDetailRepo;
	
	
	@Inject
	private UserRepository userRepo;
	
	@Inject
	private SellCarDetailRepository sellCarRepo;
	
	@Inject
	private ConfirmedInspectionRepository confirmedInspectionRepo;
	

	/*********************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * 																INTERNAL EMAILS
	 * 
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 */
	
	
	
	/**
	 * Send Stage Purchase Internal Email (Celeste)
	 * 
	 * @param List<PurchaseInternalEmailDTO> internalEmailData
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO sendPurchaseInternalEmail(List<PurchaseInternalEmailDTO> internalEmailData){
		ResponseDTO responseDTO   = new ResponseDTO();
		//String subject          = "";
        String templateEmail      = "purchase_internal_email.html";
        String isAcceptedOffer    = "No";
        String acceptedOfferType  = "";
        List<Long> sentminervaIds = null; 
        
    	String stageOneText           		 ="Etapa 1: ";
    	String stageTwoText           		 ="Etapa 2: ";
    	String stageThreeText         		 ="Etapa 3: ";
    	String stageFourText          		 ="Etapa 4: ";
    	String stageFiveText    	  		 ="Etapa 5: "; 
    	String thirtyDaysOffer        		 = "OF30 | ";
    	String instantOffer     	  		 = "OFI | ";
    	String consignmentOffer 	  		 = "OFC | ";
        String stageFiveContactText   		 = "SH | ";
        String stageFiveWithDocumentsText    = "C/D | ";
        String stageFiveWithoutDocumentsText = "S/D | ";
        //String developmentPrefixSubject      = "[PRUEBA] ";
        String finalSubject					 = "";
        
		try {
	        List<String> emailCc               = new ArrayList<String>();
	        String kavakEmail                  = null;
	        HashMap<String,Object> dataEmail   = new HashMap<>();
	        HashMap<String,Object> subjectData = new HashMap<>();
	        
			LogService.logger.info(Constants.LOG_EXECUTING_START + " [sendPurchaseInternalEmail] " + Calendar.getInstance().getTime());
			
			
			if (internalEmailData != null){
				
				sentminervaIds = new ArrayList<>();
				
				LogService.logger.info("Entorno de trabajo: " + Constants.PROPERTY_ENVIRONMENT);
				
				if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
					//finalSubject = developmentPrefixSubject;
				}
				
				for(PurchaseInternalEmailDTO internalMail : internalEmailData){
					
					LogService.logger.info("Enviado correo de la oportunidad " + internalMail.getMinervaId());
					
					if (internalMail.getStage()!= null){
						
						int stage = internalMail.getStage().intValue();
						
						switch(stage){
						case 1:
							finalSubject = stageOneText;
							break;
						case 2:
							finalSubject = stageTwoText;
							break;
						case 3:
							finalSubject = stageThreeText;
							break;
						case 4:
							finalSubject = stageFourText;
							break;
						case 5:
							finalSubject = stageFiveText;
							break;
						}
						
						// Se arma en el asunto el tipo de oferta seleccionado por el cliente
						if (internalMail.getAcceptedOfferType() != null){
			            	if (internalMail.getAcceptedOfferType() == 1){
			            		finalSubject = finalSubject + thirtyDaysOffer;
			            	}else if(internalMail.getAcceptedOfferType() == 2){
			            		finalSubject = finalSubject + instantOffer;
			            	}else if (internalMail.getAcceptedOfferType() == 3){
			            		finalSubject = finalSubject + consignmentOffer;
			            	}
						}
						
		            	if (stage == 5){
		            		
		            		// Validando si agendo horario de inspeccion [ASUNTO]
		            		if (internalMail.getAppoinmentSchedule()==null || (internalMail.getAppoinmentSchedule()!=null && internalMail.getAppoinmentSchedule().toUpperCase() == "POR CONTACTAR")){
		            			finalSubject = finalSubject + stageFiveContactText;
		            		}
		            		
		            		// Validando si tiene o no documentos adjuntos [ASUNTO]
		            		if (internalMail.getCarInvoiceUrl() != "" || internalMail.getCirculationCardUrl() != ""){
		            			finalSubject = finalSubject + stageFiveWithDocumentsText;
		            		}else{
		            			finalSubject = finalSubject + stageFiveWithoutDocumentsText;
		            		}
		            		
		            		// Se arma el asunto final para el correo en etapa 5
		            		finalSubject = finalSubject + internalMail.getCustomerName() + ", " +
		            				       internalMail.getCarModel() + ", " +
										   internalMail.getCarYear() + " OP: " + 
										   internalMail.getNetsuiteOpOpportunityId();
		            	}else{
		            		
		            		// Se arma el asunto final para el correo en estapas != 5
		            		finalSubject = finalSubject + internalMail.getCustomerName() + ", " + 
									internalMail.getCarMake() + ", " +
									internalMail.getCarModel() + ", " + 
									internalMail.getCarYear() + " OP: " + 
									internalMail.getNetsuiteOpOpportunityId();
		            	}
		            	
		            	
		            	LogService.logger.info(" Asunto final: " + finalSubject);
						
			            subjectData.put(Constants.KEY_SUBJECT, finalSubject);
			            subjectData.put(Constants.KEY_EMAIL_PREFIX, internalMail.getCustomerEmail());

			            dataEmail.put(Constants.KEY_EMAIL_USER_NAME, internalMail.getCustomerName());
			            dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, internalMail.getCustomerEmail());
			            dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, internalMail.getCustomerPhone());
			            dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, internalMail.getCarMake() + " " + internalMail.getCarModel() + " " + internalMail.getCarYear() + " (" + internalMail.getCarFullVersion() + ")");
			            dataEmail.put(Constants.KEY_EMAIL_USER_ZIPCODE, internalMail.getCustomerZipCode());
			            dataEmail.put(Constants.KEY_EMAIL_OFFERS_QUANTITY, internalMail.getOfferQuantity());
			            dataEmail.put(Constants.KEY_EMAIL_WISHLIST, internalMail.getIsWishlist());
			            
			            //Oferta 30 dias
			            if (internalMail.getThirtyDaysOfferMax()!= null){
				            dataEmail.put(Constants.KEY_EMAIL_THIRTY_OFFER_MAX, internalMail.getThirtyDaysOfferMax());
				            dataEmail.put(Constants.KEY_EMAIL_THIRTY_OFFER_MIN, internalMail.getThirtyDaysOfferMin());
			            }else{
				            dataEmail.put(Constants.KEY_EMAIL_THIRTY_OFFER_MAX, "");
				            dataEmail.put(Constants.KEY_EMAIL_THIRTY_OFFER_MIN, "");
			            }

			            
			            //Oferta instantanea
			            if (internalMail.getInstantOfferMax() != null){
				            dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, internalMail.getInstantOfferMax());
				            dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, internalMail.getInstantOfferMin());
			            }else{
				            dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "");
				            dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
			            }

			            
			            //Oferta en consignacion
			            if(internalMail.getConsigmentOfferMax() != null){
				            dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, internalMail.getConsigmentOfferMax());
				            dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, internalMail.getConsigmentOfferMin());
			            }else{
				            dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "");
				            dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "");
			            }

			            
			            if (internalMail.isAcceptedOffer()){
			            	isAcceptedOffer = "Sí";
			            	dataEmail.put(Constants.KEY_EMAIL_USER_IS_ACCEPTED_OFFER, "Sí");
			            	
			            	if (internalMail.getAcceptedOfferType() == 1){
			            		acceptedOfferType = "Oferta 30 d&iacute;as";
			            	}else if(internalMail.getAcceptedOfferType() == 2){
			            		acceptedOfferType = "Oferta Inmediata";
			            	}else{
			            		acceptedOfferType = "Oferta en Consignaci&oacute;n";
			            	}
			            }
			            
		            	dataEmail.put(Constants.KEY_EMAIL_USER_IS_ACCEPTED_OFFER, isAcceptedOffer);
		            	dataEmail.put(Constants.KEY_EMAIL_USER_ACCEPTED_OFFER_TYPE, acceptedOfferType);
		            	dataEmail.put(Constants.KEY_EMAIL_NETSUITE_OPPORTUNITY_URL, internalMail.getNetsuiteOpprotunityUrl());
		            	dataEmail.put(Constants.KEY_EMAIL_PENDING_STATUS, internalMail.getPendingStatus());
		            	dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_SCHEDULE, internalMail.getAppoinmentSchedule());
		            	dataEmail.put(Constants.KEY_EMAIL_ADDRESS_DESC, internalMail.getAddressAppointmentSchedule());
		            	dataEmail.put(Constants.KEY_EMAIL_ADDRESS_TYPE, internalMail.getAddressAppointmentScheduleType());
		            	dataEmail.put(Constants.KEY_EMAIL_URL_CAR_INVOICE, internalMail.getCarInvoiceUrl());
		            	dataEmail.put(Constants.KEY_EMAIL_URL_CIRCULATION_CARD, internalMail.getCirculationCardUrl());
		            	
		    			kavakEmail = "ayuda@kavak.com";
			            if (internalMail.getBccEmail() != ""){
			            	List<String> emailBccList = Arrays.asList(internalMail.getBccEmail().split("\\s*,\\s*"));
			            	
			            	for (String emailBcc : emailBccList){
			            		emailCc.add(emailBcc.replaceAll("`", "")); 
			            	}
			            }
			            
			            boolean emailSend = KavakUtils.sendInternalEmail(kavakEmail, emailCc, subjectData , templateEmail, null, dataEmail );
			            LogService.logger.info(Constants.LOG_EXECUTING_START+ " [putCheckout] - sendEmail retorno " + emailSend);
			            
			            if (emailSend){
			            	sentminervaIds.add(internalMail.getMinervaId());
			            }

					}else{
						LogService.logger.info("Este registro no tiene número de etapa, se descarta.");
						
				        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
				        responseDTO.setCode(enumResult.getValue());
				        responseDTO.setStatus(enumResult.getStatus());
				        responseDTO.setData(null);
					}
				}
				
		        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		        responseDTO.setCode(enumResult.getValue());
		        responseDTO.setStatus(enumResult.getStatus());
		        responseDTO.setData(sentminervaIds);
				
			}else{
				LogService.logger.info("No viene informacion para enviar correos internos de compras.");
			}
			
			LogService.logger.info(Constants.LOG_EXECUTING_END + " [sendPurchaseInternalEmail] " + Calendar.getInstance().getTime());
		} catch (Exception e) {
			LogService.logger.info("Ocurrio un error intentando enviar un correo interno de compras ");
			e.printStackTrace();
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(null);
		}
		
		return responseDTO;
	}
	
	
	/**
	 * Send PaperWork Internal Email (Celeste)
	 * 
	 * @param List<PaperWorkInternalEmailDTO> internalEmailData
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO sendPaperworkInternalEmail(List<PaperworkInternalEmailDTO> paperworkInternalEmailData){
		ResponseDTO responseDTO         = new ResponseDTO();
        String developmentPrefixSubject = "[PRUEBA] ";
        List<Long> sentminervaIds       = null;
    	String paperWorkReviewStatus    = "Revisión QA Pendiente ";
    	String delayedReviewStatus	    = "IMPORTANTE: Revisar trámites ";
    	String urgentReviewStatus  		= "URGENTE: Revisión trámites ";
    	String toStartStatus      		= "Iniciar trámite: ";
    	String startedStatus    		= "Llamar a ";
    	String outOfTimeStatus			= "URGENTE: Trámite fuera de tiempo ";
        
        String templateEmail            = "paperwork/internal_email/p-100.html";
        
		try {
	        List<String> emailCc               = new ArrayList<String>();
	        String kavakEmail                  = null;
	        HashMap<String,Object> dataEmail   = new HashMap<>();
	        HashMap<String,Object> subjectData = new HashMap<>();
	        
			LogService.logger.info(Constants.LOG_EXECUTING_START + " [sendPaperWorkInternalEmail] " + Calendar.getInstance().getTime());
			
			if (paperworkInternalEmailData != null){
				sentminervaIds = new ArrayList<>();
				
				
				LogService.logger.info("Total de correos internos a enviar para tramites: " + paperworkInternalEmailData.size());
				
				for(PaperworkInternalEmailDTO internalEmailData : paperworkInternalEmailData){
					
					String finalSubject = "";
					
					LogService.logger.info("Entorno de trabajo: " + Constants.PROPERTY_ENVIRONMENT);
					
					if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
						finalSubject = finalSubject + developmentPrefixSubject;
					}
					
					if (internalEmailData.getPaperworkStatusId() == 8){ // Revision de tramite
						//templateEmail = "paperwork/internal_email/p-100.html";
						
						finalSubject = finalSubject + paperWorkReviewStatus + internalEmailData.getCar();
						
			            dataEmail.put(Constants.KEY_EMAIL_CURRENT_PLATE_STATUS, internalEmailData.getCurrentPlateStatus());
			            dataEmail.put(Constants.KEY_EMAIL_FINAL_PLATE_STATUS, internalEmailData.getFinalPlateStatus());
			            
					}else if (internalEmailData.getPaperworkStatusId() == 9){  // Revision atrasada
						//templateEmail = "paperwork/internal_email/p-101.html";
						
						finalSubject = finalSubject + delayedReviewStatus + internalEmailData.getCar();
						
			            dataEmail.put(Constants.KEY_EMAIL_CURRENT_PLATE_STATUS, internalEmailData.getCurrentPlateStatus());
			            dataEmail.put(Constants.KEY_EMAIL_FINAL_PLATE_STATUS, internalEmailData.getFinalPlateStatus());

					}else if (internalEmailData.getPaperworkStatusId() == 10){ // Revision Urgente - Tramite Pendiente
						//templateEmail = "paperwork/internal_email/p-102.html";
						
						finalSubject = finalSubject + urgentReviewStatus + internalEmailData.getCar();
						
			            dataEmail.put(Constants.KEY_EMAIL_CURRENT_PLATE_STATUS, internalEmailData.getCurrentPlateStatus());
			            dataEmail.put(Constants.KEY_EMAIL_FINAL_PLATE_STATUS, internalEmailData.getFinalPlateStatus());
			            
						
					}else if (internalEmailData.getPaperworkStatusId() == 1){ // Por iniciar
						//templateEmail = "paperwork/internal_email/p-103.html";
						
						finalSubject = finalSubject + toStartStatus + internalEmailData.getPaperworkTypeDescription() + " " + internalEmailData.getCar();
						
			            dataEmail.put(Constants.KEY_EMAIL_CURRENT_PLATE_STATUS, internalEmailData.getCurrentPlateStatus());
			            dataEmail.put(Constants.KEY_EMAIL_FINAL_PLATE_STATUS, internalEmailData.getFinalPlateStatus());
			            
			            dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_TYPE, internalEmailData.getPaperworkTypeDescription());
			            dataEmail.put(Constants.KEY_EMAIL_SHIPPING_ADDRESS, internalEmailData.getShippingAddress());
			            
					}else if(internalEmailData.getPaperworkStatusId() == 2){  // Iniciado
						//templateEmail = "paperwork/internal_email/p-104.html";
						
						finalSubject = finalSubject + startedStatus + internalEmailData.getShippingManager() + internalEmailData.getCar();
					
			            dataEmail.put(Constants.KEY_EMAIL_SHIPPING_MANAGER, internalEmailData.getShippingManager());
			            dataEmail.put(Constants.KEY_EMAIL_STARTED_DATE, internalEmailData.getStartedDate());
			            
					
					}else if (internalEmailData.getPaperworkStatusId() == 6){ // Fuera de Tiempo
						//templateEmail = "paperwork/internal_email/p-105.html";
						
						finalSubject = finalSubject + outOfTimeStatus + internalEmailData.getCar();
						
			            dataEmail.put(Constants.KEY_EMAIL_SHIPPING_MANAGER, internalEmailData.getShippingManager());
			            dataEmail.put(Constants.KEY_EMAIL_STARTED_DATE, internalEmailData.getStartedDate());
					}
					
	            	LogService.logger.info("ASUNTO: " + finalSubject);

					subjectData.put(Constants.KEY_SUBJECT, finalSubject);
		            dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, internalEmailData.getCar());
		            dataEmail.put(Constants.KEY_EMAIL_DELIVERY_DATE, internalEmailData.getDeliveryDate());
		            dataEmail.put(Constants.KEY_EMAIL_URL_NETSUITE_PAPERWORK, internalEmailData.getNetsuiteURL());
		            
		            if(internalEmailData.getEmailTemplate() != null){
		            	LogService.logger.info("Template de correo interno a usar: " + internalEmailData.getEmailTemplate().toUpperCase());
		            	templateEmail = "paperwork/internal_email/"+ internalEmailData.getEmailTemplate().toUpperCase() +".html";
		            }
		            
		            //El destinatario pruebas: 
		            //kavakEmail = "angie@kavak.com";
	    			kavakEmail = "ayuda@kavak.com";
	    			
	    			// Copias de correos
		            if (!internalEmailData.getBccEmail().isEmpty()){
		            	List<String> emailBccList = Arrays.asList(internalEmailData.getBccEmail().split("\\s*,\\s*"));
		            	
		            	for (String emailBcc : emailBccList){
		            		emailCc.add(emailBcc.replaceAll("`", "")); 
		            	}
		            }
		            
		            //Revisar si tiene adjuntos
		            
		            if (internalEmailData.getAttachments() != null && internalEmailData.getAttachments().size() > 0){
		            	dataEmail.put(Constants.ATTACHMENT_FILES, internalEmailData.getAttachments());
		            }else{
		            	dataEmail.put(Constants.ATTACHMENT_FILES, null);
		            }
		            
		            boolean emailSend = KavakUtils.sendInternalEmail(kavakEmail, emailCc, subjectData , templateEmail, null, dataEmail);
		            LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] - sendEmail retorno " + emailSend);
		            
		            if (emailSend){
		            	sentminervaIds.add(internalEmailData.getId());
		            }
				}
				
		        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		        responseDTO.setCode(enumResult.getValue());
		        responseDTO.setStatus(enumResult.getStatus());
		        responseDTO.setData(sentminervaIds);
			
			}else{
				LogService.logger.info("No viene informacion para enviar correos internos de tramites.");
			}
			
			LogService.logger.info(Constants.LOG_EXECUTING_END + " [sendPaperWorkInternalEmail] " + Calendar.getInstance().getTime());
		} catch (Exception e) {
			LogService.logger.error("Ocurrio un error intentando enviar un correo interno de tramites ");
			e.printStackTrace();
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(null);
		}
		
		return responseDTO;
	}
	
	/**
	 * Send Available Car Notifications Excel (Celeste)
	 * 
	 * @param Long stockId
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO sendAvailableCarsNotificationsExcelEmail(Long stockId){

		ResponseDTO responseDTO            = new ResponseDTO();
        HashMap<String,Object> dataEmail   = new HashMap<>();
        HashMap<String,Object> subjectData = new HashMap<>();
        String developmentPrefixSubject    = "[PRUEBA] ";
        String titleSubject  	           = "Leads para AUTO ";
        String finalSubject				   = "";
        List<String> emailCc               = new ArrayList<String>();
        String kavakEmail                  = null;
        String templateEmail               = "cancelled_reservations/interested_customers_list.html";
        String fullVesionCar			   = "";
        boolean result					   = true;
        
		try {
			
			if (stockId != null){
				
				Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(stockId);
				
				if (sellCarDetailBD.isPresent()){
					
					CarData carDataBD = carDataRepo.findBySku(sellCarDetailBD.get().getSku());
					ResponseDTO generatedExcelFile = generateExcelFileServices.generateAvailableCarsNotificationExcel(stockId);
					
					if(generatedExcelFile.getData()!= null){
						
						LogService.logger.info("Entorno de trabajo: " + Constants.PROPERTY_ENVIRONMENT);
						
						if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
							finalSubject = developmentPrefixSubject;
						}
						
						if (carDataBD != null){
							fullVesionCar = carDataBD.getFullVersion();
						}
						
						finalSubject = titleSubject + sellCarDetailBD.get().getId() + " - " + sellCarDetailBD.get().getCarYear() + " " + sellCarDetailBD.get().getCarMake() + " " + sellCarDetailBD.get().getCarModel() + " " + fullVesionCar;
						
						List<CustomerNotifications> customerNotificationBD = customerNotificationRepo.findByCarId(stockId);
						
						if (customerNotificationBD.size() > 0){
							
							String htmlTable = "<table border='1'>"
												   + "<tr>"
												   		+ "<td align='center'><h5 style='color:#000000'>Lead</h5></td>"
												   		+ "<td align='center'><h5 style='color:#000000'>Nombre</h5></td>"
												   		+ "<td align='center'><h5 style='color:#000000'>Email</h5></td>"
												   		+ "<td align='center'><h5 style='color:#000000'>Teléfono</h5></td>"
												   		+ "<td align='center'><h5 style='color:#000000'>Fecha R.</h5></td>";
					
							
							for(CustomerNotifications customerNoti : customerNotificationBD){
								
								UserMeta userMetaBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, customerNoti.getUser().getId());
								
								htmlTable +="<tr><td align='center'><h5 style='color:#000000'>" + customerNoti.getId() + "</h5></td>";
								htmlTable +="<td align='center'><h5 style='color:#000000'>" + customerNoti.getUser().getName() + "</h5></td>";
								htmlTable +="<td align='center'><h5 style='color:#000000'>" + customerNoti.getUser().getEmail() + "</h5></td>";
								
								if (userMetaBD == null){
									htmlTable +="<td><h5 style='color:#000000'> N/A </h5></td>";
								}else{
									htmlTable +="<td><h5 style='color:#000000'><a href='tel:"+ userMetaBD.getMetaValue() +"'>"+ userMetaBD.getMetaValue() + "</a></h5></td>";
								}
								
								if (customerNoti.getCreationDate() != null){
									SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-YYYY");
									String registerDate = formatter.format(customerNoti.getCreationDate());
									
									htmlTable +="<td align='center'><h5 style='color:#000000'>" + registerDate+ "</h5></td>";
								}else{
									htmlTable +="<td align='center'><h5 style='color:#000000'> N/A </h5></td>";
								}
							}
							
							htmlTable.concat("</tablet>");
							
				            subjectData.put(Constants.KEY_SUBJECT, finalSubject);
				            //subjectData.put(Constants.KEY_EMAIL_PREFIX, "celeste@kavak.com");
				            
				            dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, sellCarDetailBD.get().getId() + " - " + sellCarDetailBD.get().getCarYear() + " " + sellCarDetailBD.get().getCarMake() + " " + sellCarDetailBD.get().getCarModel() + " " + fullVesionCar);
							dataEmail.put(Constants.KEY_EMAIL_CANCELLED_RESERVATIONS_LEADS, htmlTable);
							
							if (generatedExcelFile.getData() != null){
								ArrayList<String>  attachmentList = new ArrayList<>();
								attachmentList.add(generatedExcelFile.getData().toString());
								dataEmail.put(Constants.ATTACHMENT_FILES, attachmentList);
								
								File generatedTempFile = new File(generatedExcelFile.getData().toString());
								dataEmail.put(Constants.ATTACHMENT_FILES_TEMP_FOLDER, generatedTempFile.getParent());
								
							}else{
								dataEmail.put(Constants.ATTACHMENT_FILES, null);
							}
							
							kavakEmail = "ayuda@kavak.com";
							
							if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
								kavakEmail = "it@kavak.com";
							}else{
								emailCc.add("roger@kavak.com");
								emailCc.add("ricardo.sue@kavak.com");
								emailCc.add("experience@kavak.com");
								emailCc.add("it@kavak.com");
							}
							
				            boolean emailSend = KavakUtils.sendInternalEmail(kavakEmail, emailCc, subjectData , templateEmail, null, dataEmail);
				            LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] - sendEmail retorno " + emailSend);
				            
				            if (emailSend){
				    	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
				    	        responseDTO.setCode(enumResult.getValue());
				    	        responseDTO.setStatus(enumResult.getStatus());
				    	        responseDTO.setData(null);
				            }
							
						}else{
							LogService.logger.info("No existen personas interesadas en el stockId " + stockId);
						}
					}else{
						LogService.logger.error("Ocurrio un error al intentar generar excel para el stockId " + stockId);
						result = false;
					}
				}else{
					LogService.logger.info("El stockId " + stockId + " no existe, se descarta el servicio.");
				}					
					
			}else{
				LogService.logger.info("El stockId enviado esta vacio, se descarta servicio.");
			}
			
			
			if (result){
    	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
    	        responseDTO.setCode(enumResult.getValue());
    	        responseDTO.setStatus(enumResult.getStatus());
    	        responseDTO.setData(null);
			}else{
		        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		        responseDTO.setCode(enumResult.getValue());
		        responseDTO.setStatus(enumResult.getStatus());
		        responseDTO.setData(null);
			}
			
		} catch (Exception e) {
			LogService.logger.error("Ocurrio un error intentando enviar un correo interno de interesados en reservas canceladas.");
			e.printStackTrace();
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(null);
		}
		
		
		return responseDTO;
	}
	
	
	
	/*********************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * 																CUSTOMER EMAILS
	 * 
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 */
	
	
	/**
	 * Send PaperWork Customer Email
	 * 
	 * @param List<PaperworkCustomerEmailDTO> paperworkDataList
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO sendPaperworkCustomerEmail(List<PaperworkCustomerEmailDTO> paperworkDataList){
		ResponseDTO responseDTO         = new ResponseDTO();
        //String developmentPrefixSubject = "[PRUEBA] ";
		String deliveryDateCar	  		= "¡Gracias por confiar en KAVAK para comprar tu auto!";
		String toStart 			  		= "Tu período de prueba con KAVAK finalizó"; 
		String sentPaperwork      		= "Tus trámites con KAVAK están en camino ";
		String deliveredPaperwork 		= "Tus trámites con KAVAK han sido entregados";
		String templateEmail      		= "paperwork/customer_email/P-201.html";
		String templateEmailBase		= "/templates/new_base_email_customer_template_kavak.html";
		List<String> emailCc            = new ArrayList<String>();
		List<String> emailReplyTo		= new ArrayList<String>();
		List<Long> sentminervaIds       = null;
		boolean anyTemplate				= true;
		String courrierUrl				= "";
		String ReviewSeoUrl				= null;
		
		try{	
	        HashMap<String,Object> dataEmail   = new HashMap<>();
	        HashMap<String,Object> subjectData = new HashMap<>();
	        String kavakEmail                  = null;
	        
			if (paperworkDataList != null && paperworkDataList.size() > 0){
				
				sentminervaIds = new ArrayList<>();
				
				LogService.logger.info("Total de correos celeste de tramites para clientes: " + paperworkDataList.size());
				
				for(PaperworkCustomerEmailDTO paperwork : paperworkDataList){
					
					if (paperwork.getCustomerEmail() != null && paperwork.getCustomerMinervaId() != null){
						
						User userDB = userRepo.findByEmailAndRole(paperwork.getCustomerEmail(), "seller");
						
						if(userDB != null){
							String customerMInervaId = paperwork.getCustomerMinervaId().toString();
							customerMInervaId = customerMInervaId.replace(".0", "");
							
							if(userDB.getId() == Long.parseLong(customerMInervaId)){
								
								String finalSubject = "";
								
								LogService.logger.info("Entorno de trabajo: " + Constants.PROPERTY_ENVIRONMENT);
								
								if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
									//finalSubject = finalSubject + developmentPrefixSubject;
								}
								
								if(paperwork.getPaperworkStatusId() == 1 ){        // Por iniciar
									
									finalSubject  = finalSubject + toStart;
									//templateEmail = "paperwork/customer_email/p-201.html";
									
						            dataEmail.put(Constants.KEY_EMAIL_CURRENT_PLATE_STATUS, paperwork.getCurrentPlateStatus());
						            dataEmail.put(Constants.KEY_EMAIL_TENTATIVE_DATE, paperwork.getTentativeDeliveryDate());
						            dataEmail.put(Constants.KEY_EMAIL_PERSON_WHO_RECEIVE, paperwork.getPersonWhoReceive());
						            
								}else if(paperwork.getPaperworkStatusId() == 2 ){        // Iniciado  
						        
									finalSubject  = finalSubject + toStart;
									//templateEmail = "paperwork/customer_email/p-201.html";
									
									dataEmail.put(Constants.KEY_EMAIL_TENTATIVE_DATE, paperwork.getTentativeDeliveryDate());
									dataEmail.put(Constants.KEY_EMAIL_PERSON_WHO_RECEIVE, paperwork.getPersonWhoReceive());
									
									
								}else if(paperwork.getPaperworkStatusId() == 4 ){     // Enviado
									
									finalSubject  = finalSubject + sentPaperwork;
									//templateEmail = "paperwork/customer_email/p-202.html";
									
									if(paperwork.getCourrier().toUpperCase() != null && !paperwork.getCourrier().toUpperCase().isEmpty()){
										dataEmail.put(Constants.KEY_EMAIL_SHIPPING_COURRIER, paperwork.getCourrier().toUpperCase());
									}
									
									if(paperwork.getTrackingNumber() != null && !paperwork.getTrackingNumber().isEmpty()){
										dataEmail.put(Constants.KEY_EMAIL_TRACKING_NUMBER, paperwork.getTrackingNumber());
									}
									
									dataEmail.put(Constants.KEY_EMAIL_PERSON_WHO_RECEIVE, paperwork.getPersonWhoReceive());
									
									
								}else if(paperwork.getPaperworkStatusId() == 5 ){     // Entregado
									
									finalSubject  = finalSubject + deliveredPaperwork;
									//templateEmail = "paperwork/customer_email/p-203.html";
									
									if(paperwork.getCourrier().toUpperCase() != null && !paperwork.getCourrier().toUpperCase().isEmpty()){
										dataEmail.put(Constants.KEY_EMAIL_SHIPPING_COURRIER, paperwork.getCourrier().toUpperCase());
									}
									
									if(paperwork.getShippingManager() != null && !paperwork.getShippingManager().isEmpty()){
										dataEmail.put(Constants.KEY_EMAIL_SHIPPING_MANAGER, paperwork.getShippingManager());
									}
									
									if(paperwork.getTrackingNumber() != null && !paperwork.getTrackingNumber().isEmpty()){
										dataEmail.put(Constants.KEY_EMAIL_TRACKING_NUMBER, paperwork.getTrackingNumber());
									}
									
									dataEmail.put(Constants.KEY_EMAIL_PERSON_WHO_RECEIVE, paperwork.getPersonWhoReceive());
									
								}else if (paperwork.getPaperworkStatusId() == 999 ){  // Cuando se tiene fecha de entrega
									
									anyTemplate = false;
									
						            if(paperwork.getStockId() != null){
						            	
						            	Optional<SellCarDetail> sellCarDetailsBD = sellCarRepo.findById(paperwork.getStockId());
						            	
						            	if(sellCarDetailsBD.isPresent()){
						            		
						            		CarData carDataBD = carDataRepo.findBySku(sellCarDetailsBD.get().getSku());
						            		
						            		if (carDataBD != null){
						            			
												finalSubject  = finalSubject + deliveryDateCar;
												templateEmail = "paperwork/customer_email/P-200.html";
												
							            		dataEmail.put(Constants.KEY_EMAIL_CAR_ID, sellCarDetailsBD.get().getId());
									            dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR,carDataBD.getCarYear());
									            dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, carDataBD.getCarMake());
									            dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, carDataBD.getCarModel());
									            dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, carDataBD.getCarTrim());
									            dataEmail.put(Constants.KEY_EMAIL_CURRENT_PLATE_STATUS, paperwork.getCurrentPlateStatus());
									            dataEmail.put(Constants.KEY_EMAIL_RETURN_VALIDITY, paperwork.getValidityReturn());
									            dataEmail.put(Constants.KEY_EMAIL_USER_ID, userDB.getId());

									            if(paperwork.getCarKm() != null){
									            	dataEmail.put(Constants.KEY_EMAIL_CAR_KM, KavakUtils.splitThousandFormat(paperwork.getCarKm().toString()));
									            }else{
									            	dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No disponible");
									            }
									            
									            String userName = userDB.getName().replaceAll(" ", "%20");
									            String carLogo = carDataBD.getCarMake().replaceAll(" ","_");
									            
									            dataEmail.put(Constants.KEY_EMAIL_CAR_LOGO, Constants.KAVAK_EMAIL_CAR_LOGO + carLogo.toUpperCase() + ".png");
									            
									            ReviewSeoUrl =  Constants.URL_KAVAK + "review-" +
									            				carDataBD.getCarMake()+ "-" +
									            				carDataBD.getCarModel() + "-" +
									            				carDataBD.getCarYear().toString() + 
									            				"_stockId_" + sellCarDetailsBD.get().getId() + "&" +
									            				"utm_source=Email&utm_campaign=Email-Experiencia-Venta-" + 
									            				userName + "-" + 
									            				userDB.getId().toString() + "-" + 
									            				carDataBD.getCarMake() + "-" + 
									            				carDataBD.getCarModel() + "-" + 
									            				carDataBD.getCarYear().toString() + 
									            				"&utm_medium=organico&utm_content=Email-Experiencia-Venta-" + 
									            				userName + "-" + 
									            				userDB.getId().toString() + "-" + 
									            				carDataBD.getCarMake() + "-" + 
									            				carDataBD.getCarModel() + "-" + 
									            				carDataBD.getCarYear().toString();
									            
									            
									            dataEmail.put(Constants.KEY_EMAIL_CUSTOMER_REVIEW_SEO_URL, ReviewSeoUrl);
									            
									            //AFTER SALE KAVAKO DATA
									            
												if(paperwork.getAfterSaleKavakoName() != null && !paperwork.getAfterSaleKavakoName().isEmpty()){
													//LogService.logger.info("Hay personal postventa para el correo. Nombre " + paperwork.getAfterSaleKavakoName());
													dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_NAME, WordUtils.capitalize(paperwork.getAfterSaleKavakoName().toLowerCase()));
												}else{
													dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_NAME, "");
												}
												
												if(paperwork.getAfterSaleKavakoPhone()!= null && paperwork.getAfterSaleKavakoPhone() != ""){
													dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_PHONE, paperwork.getAfterSaleKavakoPhone().replaceAll(" ", ""));
												}else{
													dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_PHONE, "");
												}
												
												if(paperwork.getAfterSaleKavakoEmail() != null && !paperwork.getAfterSaleKavakoEmail().isEmpty()){
													dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_EMAIL, paperwork.getAfterSaleKavakoEmail());
													String kavakoName = paperwork.getAfterSaleKavakoEmail().replace(Constants.KAVAK_EMAIL_DOMAIN, "");
													dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_PHOTO, Constants.URL_BASE_IMAGES_KAVAKOS  + kavakoName.toLowerCase() + ".png");
												}else{
													dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_EMAIL, "");
													dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_PHOTO, Constants.URL_BASE_IMAGES_KAVAKOS_DEFAULT);
												}
												
												// PAPERWORK KAVAKO DATA
												
												if(paperwork.getPaperworkKavakoName() != null && !paperwork.getPaperworkKavakoName().isEmpty()){
													//LogService.logger.info("Hay personal de tramites para el correo. Nombre: " + paperwork.getPaperworkKavakoName());
													dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_KAVAKO_NAME, WordUtils.capitalize(paperwork.getPaperworkKavakoName().toLowerCase()));
												}else{
													dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_KAVAKO_NAME, "");
												}
												
												if(paperwork.getPaperworkKavakoPhone()!= null && paperwork.getPaperworkKavakoPhone() != ""){
													dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_KAVAKO_PHONE, paperwork.getPaperworkKavakoPhone().replaceAll(" ", ""));
												}else{
													dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_KAVAKO_PHONE, "");
												}
												
												if(paperwork.getPaperworkKavakoEmail() != null && !paperwork.getPaperworkKavakoEmail().isEmpty()){
													dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_KAVAKO_EMAIL, paperwork.getPaperworkKavakoEmail());
													String kavakoName = paperwork.getPaperworkKavakoEmail().replace(Constants.KAVAK_EMAIL_DOMAIN, "");
													dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_KAVAKO_PHOTO, Constants.URL_BASE_IMAGES_KAVAKOS + kavakoName.toLowerCase() + ".png");
												}else{
													dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_KAVAKO_EMAIL, "");
													dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_KAVAKO_PHOTO, Constants.URL_BASE_IMAGES_KAVAKOS_DEFAULT);
												}
									            
									            
						            		}else{
							            		LogService.logger.info("No se encuentra el SKU del stock id " + paperwork.getStockId() + ", se descarta correo de tramites.");
							            		return responseDTO;
						            		}
								            
						            	}else{
						            		LogService.logger.info("No se encontro el stock id " + paperwork.getStockId() + " en BD, se descarta correo de tramites.");
						            		return responseDTO;
						            	}
						            }else{
						            	LogService.logger.info("El stock id esta vacio, se descarta correo de tramites.");
						            	return responseDTO;
						            }
								}else if (paperwork.getPaperworkStatusId() == 998 ){
									
									LogService.logger.info("Es la opcion 998 de Status Paperwork.");
									
									templateEmail = "paperwork/customer_email/P-207.html";
									
									if (!paperwork.getCustomerEmail().isEmpty()){
										String[] firstName = userDB.getName().split(" "); 
										finalSubject  = firstName[0] + " " + finalSubject + toStart;
										dataEmail.put(Constants.KEY_EMAIL_CAR_ID, paperwork.getStockId());
										
										if(paperwork.getAfterSaleKavakoName() != null && !paperwork.getAfterSaleKavakoName().isEmpty()){
											LogService.logger.info("Hay personal postventa para el correo. Nombre: " + paperwork.getAfterSaleKavakoName());
											dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_NAME, WordUtils.capitalize(paperwork.getAfterSaleKavakoName()));
										}else{
											dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_NAME, "");
										}
										
										if(paperwork.getAfterSaleKavakoPhone()!= null && paperwork.getAfterSaleKavakoPhone() != ""){
											dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_PHONE, paperwork.getAfterSaleKavakoPhone().replaceAll(" ", ""));
										}else{
											dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_PHONE, "");
										}
										
										if(paperwork.getAfterSaleKavakoEmail() != null && !paperwork.getAfterSaleKavakoEmail().isEmpty()){
											dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_EMAIL, paperwork.getAfterSaleKavakoEmail());
										}else{
											dataEmail.put(Constants.KEY_EMAIL_AFTER_SALE_KAVAKO_EMAIL, "");
										}
									}
								}
								
								LogService.logger.info("ASUNTO: " + finalSubject);
								
								subjectData.put(Constants.KEY_SUBJECT, finalSubject);
								dataEmail.put(Constants.KEY_EMAIL_USER_NAME, userDB.getName());
								dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, paperwork.getCar());
								dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_TYPE, paperwork.getPaperworkTypeDescription());
								dataEmail.put(Constants.KEY_EMAIL_FINAL_PLATE_STATUS, paperwork.getFinalPlateStatus());
								dataEmail.put(Constants.KEY_EMAIL_SHIPPING_ADDRESS, paperwork.getShippingAddress());
								
								if (anyTemplate == true && paperwork.getEmailTemplate() != null){
									LogService.logger.info("Template de correo cliente a usar: " + paperwork.getEmailTemplate().toUpperCase());
									templateEmail = "paperwork/customer_email/"+ paperwork.getEmailTemplate().toUpperCase() +".html";
								}
								
								
								if (paperwork.getTrackingNumberUrl() != null && !paperwork.getTrackingNumberUrl().isEmpty()){
									courrierUrl = paperwork.getTrackingNumberUrl();
									dataEmail.put(Constants.KEY_EMAIL_TRACKING_URL, courrierUrl);
								}else{
									if (paperwork.getCourrier() != null && !paperwork.getCourrier().isEmpty()){
										courrierUrl = "https://www.google.com.mx/search?q="+ paperwork.getCourrier().toLowerCase() +"&oq=" + paperwork.getCourrier().toLowerCase();
										dataEmail.put(Constants.KEY_EMAIL_TRACKING_URL, courrierUrl);
									}
								}
								
								
					            //El destinatario del correo es el customer
				    			kavakEmail = paperwork.getCustomerEmail();
								
				    			// Copias de correos
					            if (paperwork.getBccEmail() != null && !paperwork.getBccEmail().isEmpty()){
					            	List<String> emailBccList = Arrays.asList(paperwork.getBccEmail().split("\\s*,\\s*"));
					            	
					            	for (String emailBcc : emailBccList){
					            		emailCc.add(emailBcc.replaceAll("`", "")); 
					            	}
					            }
					            
					            // Correos Reply To
					            if(paperwork.getReplyToEmail() != null && !paperwork.getReplyToEmail().isEmpty()){
					            	//replyTo
					            	List<String> replyToList = Arrays.asList(paperwork.getReplyToEmail().split("\\s*,\\s*"));
					            	
					            	for(String reply : replyToList){
					            		emailReplyTo.add(reply.replaceAll("`", ""));
					            	}
					            }
					            
					            if (paperwork.getAttachments() != null && paperwork.getAttachments().size() > 0){
					            	LogService.logger.debug("Este correo tiene adjuntos.");
					            	dataEmail.put(Constants.ATTACHMENT_FILES, paperwork.getAttachments());
					            }else{
					            	LogService.logger.debug("Este correo no tiene adjuntos.");
					            	dataEmail.put(Constants.ATTACHMENT_FILES, null);
					            }
					            
					            // El sender es Celeste
					            InternetAddress fromEmail = new InternetAddress("celeste@kavak.com", "Celeste");
					            
					            // Si existe plantilla de correo, se realiza el envío
					            if (!templateEmail.isEmpty()){
						            boolean emailSend = KavakUtils.sendEmail(kavakEmail, emailCc, subjectData , templateEmail, templateEmailBase, dataEmail, fromEmail, emailReplyTo);
						            LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] - sendEmail retorno " + emailSend);
						            
						            if (emailSend){
						            	sentminervaIds.add(paperwork.getId());
						            }
									
							        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
							        responseDTO.setCode(enumResult.getValue());
							        responseDTO.setStatus(enumResult.getStatus());
							        responseDTO.setData(sentminervaIds);
					            }else{
					            	LogService.logger.debug("No se definió plantilla de correo, se descarta email.");
					            	
							        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
							        responseDTO.setCode(enumResult.getValue());
							        responseDTO.setStatus(enumResult.getStatus());
							        responseDTO.setData(null);
					            }
							}
						}else{
							LogService.logger.info("No existe el usuario en minerva con correo: " + paperwork.getCustomerEmail() + ", no se enviara correo");
						}
						
					}else {
						LogService.logger.info("El cliente con correo " + paperwork.getCustomerEmail() + " no tiene ID de minerva, no se enviara correo.");
					}
				}
				
			}else{
				LogService.logger.info("Total de correos internos a enviar para tramites: 0");
			}
			
		} catch (Exception e) {
			LogService.logger.error("Error enviando correos Celeste para cliente sobre Tramites.");
			e.printStackTrace();
		}
		return responseDTO;
	}
	
	
	public ResponseDTO sendConfirmedInspection(List<ConfirmedInspectionNetsuiteDTO> confirmedInspectionList){
		ResponseDTO responseDTO = new ResponseDTO();
		
		try {
	        HashMap<String,Object> dataEmail   = new HashMap<>();
	        HashMap<String,Object> subjectData = new HashMap<>();
	        String kavakEmail                  = null;
	        String finalSubject				   = "La inspección de tu auto está Confirmada con KAVAK";
	        String subjectEmailUrl             = "Hola quiero confirmar mi inspección";
	        String bodyInspectorEmailUrl	   = null;
	        String bodyLeadEmailUrl			   = null;
	        String bodyWingmanEmailUrl		   = null;
	        List<String> emailCc               = new ArrayList<String>();
	        List<String> emailReplyTo          = new ArrayList<String>();
	        String templateEmailBase		   = "/templates/new_base_email_celeste_responsive.html";
			String templateEmail               = "";
	        List<Long> sentminervaIds          = null;
	        String dayName					   = Constants.URL_BASE_IMAGES_HEADER_CONFIRMED_INSPECTION + "default." + Constants.FILE_TYPE_PNG;

	        if(confirmedInspectionList.size() > 0){
	        	
	        	sentminervaIds = new ArrayList<>();
				
	        	for(ConfirmedInspectionNetsuiteDTO confirmedInspection : confirmedInspectionList){
					
	    	        String inspectorWhatsappMessage    = "";
	    	        String leadManagerWhatsappMessage  = "";
	    	        String wingmanWhatsappMessage	   = "";
	    	        boolean isvalideDate               = false;
	    	        String dateInSpanish               = "";
	    	        
	        		if(confirmedInspection.getCustomerMinervaId() != null && !confirmedInspection.getWingmanEmail().trim().isEmpty() && !confirmedInspection.getLeadManagerEmail().trim().isEmpty()){
	        			
	        			if(confirmedInspection.getOpportunityId() != null){
	        				finalSubject = finalSubject + " (" + confirmedInspection.getOpportunityId() + ")";
	        			}
	        			
	    				LogService.logger.info("ASUNTO: " + finalSubject);
	    				subjectData.put(Constants.KEY_SUBJECT, finalSubject);
	        			
	        			
	        			Optional<User> userDB = userRepo.findById(confirmedInspection.getCustomerMinervaId());
	        			
	        			if (userDB .isPresent()){
	        				
				            // VALIDACION DE LA FECHA DE INSPECCION (ESTE DEFINE EL BANNER QUE SE MOSTRARA EN EL CORREO)
				            if(!confirmedInspection.getInspectionDate().isEmpty()){
				            	
				            	String uncleanDate = "";
				            	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				            	SimpleDateFormat  dayNameFormat = new SimpleDateFormat("EEEE");
				            	
				            	 
				            	dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, confirmedInspection.getInspectionDate());
				            	
				            	// SE INDAGA QUE SEPARADOR TIENE LA FECHA SUMINISTRADA Y SE REEMPLAZA POR #
				            	if (confirmedInspection.getInspectionDate().indexOf("-") > 01){
				            		uncleanDate = confirmedInspection.getInspectionDate().replace("-", "#");
				            		isvalideDate = true;
				            	}else if (confirmedInspection.getInspectionDate().indexOf("/") > 0){
				            		uncleanDate = confirmedInspection.getInspectionDate().replace("/", "#");
				            		isvalideDate = true;
				            	}
				            	
				            	if (isvalideDate){
				            		
				            		try {
				            			
				            			// SE LE DA FORMATO EN INGLES A LA FECHA OBTENIDA
					            		String[] segment = uncleanDate.split("#");
				            			Date inspectionDate = formatter.parse(segment[2] + "-" + segment[1] + "-" + segment[0]);
				            			
				            			dayName = dayNameFormat.format(inspectionDate).toLowerCase();
				            			
				            			// PARA USARSE EN LOS MENSAJES WHATSAPP. FECHA EN ESPAÑOL
						            	Locale spanishLocale = new Locale("es", "ES");
						            	LocalDate localDate = LocalDate.of(Integer.valueOf(segment[2]),Integer.valueOf(segment[1]),Integer.valueOf(segment[0]));
						            	dateInSpanish = localDate.format(DateTimeFormatter.ofPattern("EEEE", spanishLocale));
						            	
									} catch (Exception e) {
										LogService.logger.error("Ocurrio un error intentando formatear la fecha " + confirmedInspection.getInspectionDate() + " para incluir el banner");
									}
				            	}
				            }
	        				
	        				
				            // INSPECTOR
				            if(!confirmedInspection.getInspectorEmail().trim().isEmpty()){
				            	String inspectorName = confirmedInspection.getInspectorEmail().trim().toLowerCase().replace(Constants.KAVAK_EMAIL_DOMAIN, "");

				            	LogService.logger.debug("Inspector a buscar: " + Constants.URL_BASE_IMAGES_KAVAKOS_SIGNATURE_MOBILE + inspectorName + "." + Constants.FILE_TYPE_PNG);
			            		
				            	dataEmail.put(Constants.KEY_EMAIL_INSPECTOR_PHOTO_MOBILE, Constants.URL_BASE_IMAGES_KAVAKOS_SIGNATURE_MOBILE + inspectorName + "."+ Constants.FILE_TYPE_PNG );
			            		dataEmail.put(Constants.KEY_EMAIL_INSPECTOR_PHOTO_DESKTOP, Constants.URL_BASE_IMAGES_KAVAKOS_SIGNATURE_DESKTOP + inspectorName + "." + Constants.FILE_TYPE_PNG );
			            		dataEmail.put(Constants.KEY_EMAIL_INSPECTOR_NAME, WordUtils.capitalize(confirmedInspection.getInspectorName().toLowerCase()));
			            		dataEmail.put(Constants.KEY_EMAIL_INSPECTOR_PHONE, confirmedInspection.getInspectorPhone().replaceAll(" ", "").replaceAll("-", ""));
			            		
			            		if(isvalideDate){
			            			String[] initDate = confirmedInspection.getInspectionTime().split(" a ");
			            			inspectorWhatsappMessage = "Hola soy " + WordUtils.capitalize(userDB.get().getName().toLowerCase()) + ". Tengo una inspección agendada con KAVAK el " + WordUtils.capitalize(dateInSpanish.toLowerCase()) + " a la " + initDate[0].toUpperCase() + ", te confirmo mi número y mi inspección. Gracias!";
			            		}else{
			            			inspectorWhatsappMessage = "Hola soy " + WordUtils.capitalize(userDB.get().getName().toLowerCase()) + ". Tengo una inspección agendada con KAVAK en los próximos días.";
			            		}

			            		bodyInspectorEmailUrl = inspectorWhatsappMessage;
			            		dataEmail.put(Constants.KEY_EMAIL_INSPECTOR_WHATSAPP_URL, KavakUtils.generateWhatsappMobileUrl(confirmedInspection.getInspectorPhone().replaceAll(" ", "").replaceAll("-", ""), inspectorWhatsappMessage));
			            		dataEmail.put(Constants.KEY_EMAIL_INSPECTOR_EMAIL_URL, KavakUtils.generateEmailUrl(confirmedInspection.getInspectorEmail(), subjectEmailUrl, bodyInspectorEmailUrl, null, null));
				            }
				            
				            
				            // LEAD MANAGER
				            if(!confirmedInspection.getLeadManagerEmail().trim().isEmpty()){
				            	String leadName = confirmedInspection.getLeadManagerEmail().trim().toLowerCase().replace(Constants.KAVAK_EMAIL_DOMAIN, "");
				            	
				            	LogService.logger.debug("Lead a buscar" + Constants.URL_BASE_IMAGES_KAVAKOS_SIGNATURE_MOBILE + leadName + "." + Constants.FILE_TYPE_PNG);
			            		dataEmail.put(Constants.KEY_EMAIL_LEAD_PHOTO_MOBILE, Constants.URL_BASE_IMAGES_KAVAKOS_SIGNATURE_MOBILE + leadName + "." + Constants.FILE_TYPE_PNG );
			            		dataEmail.put(Constants.KEY_EMAIL_LEAD_PHOTO_DESKTOP, Constants.URL_BASE_IMAGES_KAVAKOS_SIGNATURE_DESKTOP + leadName + "." + Constants.FILE_TYPE_PNG );
			            		dataEmail.put(Constants.KEY_EMAIL_LEAD_NAME, WordUtils.capitalize(confirmedInspection.getLeadManagerName().toLowerCase()));
			            		dataEmail.put(Constants.KEY_EMAIL_LEAD_PHONE, confirmedInspection.getLeadManagerPhone().replaceAll(" ", "").replaceAll("-", ""));
			            		
			            		if(isvalideDate){
			            			String[] initDate = confirmedInspection.getInspectionTime().split(" a ");
			            			leadManagerWhatsappMessage = "Hola soy " + WordUtils.capitalize(userDB.get().getName().toLowerCase()) + ". Tengo una inspección agendada con KAVAK el " + WordUtils.capitalize(dateInSpanish.toLowerCase()) + " a la " + initDate[0].toUpperCase() + ", te confirmo mi número y mi inspección. Gracias!";
			            		}else{
			            			leadManagerWhatsappMessage = "Hola soy " + WordUtils.capitalize(userDB.get().getName().toLowerCase()) + ". Tengo una inspección agendada con KAVAK en los próximos días.";
			            		}
			            		
			            		bodyLeadEmailUrl = leadManagerWhatsappMessage;
			            		dataEmail.put(Constants.KEY_EMAIL_LEAD_WHATSAPP_URL, KavakUtils.generateWhatsappMobileUrl(confirmedInspection.getLeadManagerPhone().replaceAll(" ", "").replaceAll("-", ""), leadManagerWhatsappMessage));
			            		dataEmail.put(Constants.KEY_EMAIL_LEAD_EMAIL_URL, KavakUtils.generateEmailUrl(confirmedInspection.getLeadManagerEmail(), subjectEmailUrl, bodyLeadEmailUrl, null, null));
				            }

				            
				            // WINGMAN
				            if(!confirmedInspection.getWingmanEmail().trim().isEmpty()){
				            	String wingmanName = confirmedInspection.getWingmanEmail().trim().toLowerCase().replace(Constants.KAVAK_EMAIL_DOMAIN, "");
				            	
				            	LogService.logger.debug("Wingman a buscar" + Constants.URL_BASE_IMAGES_KAVAKOS_SIGNATURE_MOBILE + wingmanName + "." + Constants.FILE_TYPE_PNG);
			            		dataEmail.put(Constants.KEY_EMAIL_WINGMAN_PHOTO_MOBILE, Constants.URL_BASE_IMAGES_KAVAKOS_SIGNATURE_MOBILE + wingmanName + "." + Constants.FILE_TYPE_PNG );
			            		dataEmail.put(Constants.KEY_EMAIL_WINGMAN_PHOTO_DESKTOP, Constants.URL_BASE_IMAGES_KAVAKOS_SIGNATURE_DESKTOP + wingmanName + "." + Constants.FILE_TYPE_PNG );
			            		dataEmail.put(Constants.KEY_EMAIL_WINGMAN_NAME, WordUtils.capitalize(confirmedInspection.getWingmanName().toLowerCase()));
			            		dataEmail.put(Constants.KEY_EMAIL_WINGMAN_PHONE, confirmedInspection.getWingmanPhone().replaceAll(" ", "").replaceAll("-", ""));
			            		
			            		if(isvalideDate){
			            			String[] initDate = confirmedInspection.getInspectionTime().split(" a ");
			            			wingmanWhatsappMessage = "Hola soy " + WordUtils.capitalize(userDB.get().getName().toLowerCase()) + ". Tengo una inspección agendada con KAVAK el " + WordUtils.capitalize(dateInSpanish.toLowerCase()) + " a la " + initDate[0].toUpperCase() + ", te confirmo mi número y mi inspección. Gracias!";
			            		}else{
			            			wingmanWhatsappMessage = "Hola soy " + WordUtils.capitalize(userDB.get().getName().toLowerCase()) + ". Tengo una inspección agendada con KAVAK en los próximos días.";
			            		}
			            		
			            		bodyWingmanEmailUrl = wingmanWhatsappMessage;
			            		dataEmail.put(Constants.KEY_EMAIL_WINGMAN_WHATSAPP_URL, KavakUtils.generateWhatsappMobileUrl(confirmedInspection.getWingmanPhone().replaceAll(" ", "").replaceAll("-", ""), wingmanWhatsappMessage));
			            		dataEmail.put(Constants.KEY_EMAIL_WINGMAN_EMAIL_URL, KavakUtils.generateEmailUrl(confirmedInspection.getWingmanEmail(), subjectEmailUrl, bodyWingmanEmailUrl, null, null));
				            }
				            
				            dataEmail.put(Constants.KEY_EMAIL_USER_ID, userDB.get().getId());
				            dataEmail.put(Constants.KEY_EMAIL_USER_NAME,  WordUtils.capitalize(userDB.get().getName().toLowerCase()));
				            dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, userDB.get().getEmail());
				            dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, confirmedInspection.getCustomerPhone());
				            dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, confirmedInspection.getInspectionTime());
				            dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, confirmedInspection.getCustomerAddress());
				            dataEmail.put(Constants.KEY_EMAIL_RETURN_VALIDITY, confirmedInspection.getValidityDate());
				            dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, confirmedInspection.getCarMake().toUpperCase() + " " + confirmedInspection.getCarModel().toUpperCase() + " " + confirmedInspection.getCarYear());
				            dataEmail.put(Constants.KEY_EMAIL_URL_HEADER_CONFIRMED_INSPECTION, Constants.URL_BASE_IMAGES_HEADER_CONFIRMED_INSPECTION + dayName + "." + Constants.FILE_TYPE_PNG);
	            			
				            if(confirmedInspection.getCarKm() != null){
				            	dataEmail.put(Constants.KEY_EMAIL_CAR_KM, KavakUtils.splitThousandFormat(confirmedInspection.getCarKm().toString()));
				            }else{
				            	dataEmail.put(Constants.KEY_EMAIL_CAR_KM, " ");
				            }
				            
				            
				            // GESTION DEL TEMPLATE DEPENDIENDO DE LAS OFERTAS RECIBIDAS
				            if(confirmedInspection.getThirtyDaysOffer() != null && !confirmedInspection.getThirtyDaysOffer().isEmpty()){
				            	
				            	if(confirmedInspection.getInstantOffer() != null && !confirmedInspection.getInstantOffer().isEmpty()){
				            		if(confirmedInspection.getConsigmentOffer() != null && !confirmedInspection.getConsigmentOffer().isEmpty()){
				            			// 3 OFERTAS
				            			templateEmail = "inspections/C-200_3offers.html";
				            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_TITLE, Constants.KEY_EMAIL_INSTANT_OFFER);
				            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getInstantOffer()));
				            			
				            			dataEmail.put(Constants.KEY_EMAIL_SECOND_OFFER_TITLE, Constants.KEY_EMAIL_THIRTY_DAYS_OFFER);
				            			dataEmail.put(Constants.KEY_EMAIL_SECOND_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getThirtyDaysOffer()));
				            			
				            			dataEmail.put(Constants.KEY_EMAIL_THIRD_OFFER_TITLE, Constants.KEY_EMAIL_CONSIGMENT_OFFER);
				            			dataEmail.put(Constants.KEY_EMAIL_THIRD_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getConsigmentOffer()));

				            		}else{
				            			// 30 DIAS E INSTANTANEA
				            			templateEmail = "inspections/C-200_2offers.html";
				            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_TITLE, Constants.KEY_EMAIL_INSTANT_OFFER);
				            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getInstantOffer()));
				            			
				            			dataEmail.put(Constants.KEY_EMAIL_SECOND_OFFER_TITLE, Constants.KEY_EMAIL_THIRTY_DAYS_OFFER);
				            			dataEmail.put(Constants.KEY_EMAIL_SECOND_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getThirtyDaysOffer()));
				            		}
				            	}else if(confirmedInspection.getConsigmentOffer() != null && !confirmedInspection.getConsigmentOffer().isEmpty()){
				            		// 30 DIAS Y CONSIGNACION
				            		templateEmail = "inspections/C-200_2offers.html";
			            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_TITLE, Constants.KEY_EMAIL_THIRTY_DAYS_OFFER);
			            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getThirtyDaysOffer()));
			            			
			            			dataEmail.put(Constants.KEY_EMAIL_SECOND_OFFER_TITLE, Constants.KEY_EMAIL_CONSIGMENT_OFFER);
			            			dataEmail.put(Constants.KEY_EMAIL_SECOND_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getConsigmentOffer()));
				            		
				            	}else{
				            		// SOLO 30 DIAS
				            		templateEmail = "inspections/C-200_1offer.html";
			            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_TITLE, Constants.KEY_EMAIL_THIRTY_DAYS_OFFER);
			            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getThirtyDaysOffer()));
			            			
				            	}
				            }else if (confirmedInspection.getInstantOffer() != null && !confirmedInspection.getInstantOffer().isEmpty()){
				            	if(confirmedInspection.getConsigmentOffer() != null && !confirmedInspection.getConsigmentOffer().isEmpty()){
				            		// INSTANTANEA Y CONSIGNACION
				            		templateEmail = "inspections/C-200_2offers.html";
			            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_TITLE, Constants.KEY_EMAIL_INSTANT_OFFER);
			            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getInstantOffer()));
				            		
			            			dataEmail.put(Constants.KEY_EMAIL_SECOND_OFFER_TITLE, Constants.KEY_EMAIL_CONSIGMENT_OFFER);
			            			dataEmail.put(Constants.KEY_EMAIL_SECOND_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getConsigmentOffer()));
				            	}else{
				            		// SOLO INSTANTANEA
				            		templateEmail = "inspections/C-200_1offer.html";
			            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_TITLE, Constants.KEY_EMAIL_INSTANT_OFFER);
			            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getInstantOffer()));
			            			
				            	}
				            }else{
				            	// SOLO INSTANTANEA
				            	templateEmail = "inspections/C-200_1offer.html";
		            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_TITLE, Constants.KEY_EMAIL_CONSIGMENT_OFFER);
		            			dataEmail.put(Constants.KEY_EMAIL_FIRST_OFFER_PRICE, "$"+KavakUtils.splitThousandFormat(confirmedInspection.getConsigmentOffer()));
				            	
				            }
				            
				            
				            //LogService.logger.debug("Template a usar: " + templateEmail);
				            
	    		            if (confirmedInspection.getAttachments() != null && confirmedInspection.getAttachments().size() > 0){
	    		            	LogService.logger.debug("Este correo tiene adjuntos.");
	    		            	dataEmail.put(Constants.ATTACHMENT_FILES, confirmedInspection.getAttachments());
	    		            }else{
	    		            	LogService.logger.debug("Este correo no tiene adjuntos.");
	    		            	dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    		            }
	    		            
				            //El destinatario del correo es el customer
			    			kavakEmail = confirmedInspection.getCustomerEmail().trim().toLowerCase();
							
			    			// Copias de correos
				            if (confirmedInspection.getBccEmail() != null && !confirmedInspection.getBccEmail().isEmpty()){
				            	List<String> emailBccList = Arrays.asList(confirmedInspection.getBccEmail().split("\\s*,\\s*"));
				            	
				            	for (String emailBcc : emailBccList){
				            		emailCc.add(emailBcc.replaceAll("`", "")); 
				            	}
				            	
				            	// Copia a ANTONY
				            	emailCc.add("antony.delgado@kavak.com");
				            }
				            
				            // Correos Reply To
				            if(confirmedInspection.getReplyToEmail() != null && !confirmedInspection.getReplyToEmail().isEmpty()){
				            	//replyTo
				            	List<String> replyToList = Arrays.asList(confirmedInspection.getReplyToEmail().split("\\s*,\\s*"));
				            	
				            	for(String reply : replyToList){
				            		emailReplyTo.add(reply.replaceAll("`", ""));
				            	}
				            }
				            
				            if(confirmedInspection.getSelectedOffer() != null){
			            		
				            	dataEmail.put(Constants.KEY_EMAIL_COLOR_SECTION_FIRST_OFFER, Constants.KEY_EMAIL_COLOR_SECTION_DEFAULT_OFFER);
			            		dataEmail.put(Constants.KEY_EMAIL_COLOR_SECTION_SECOND_OFFER, Constants.KEY_EMAIL_COLOR_SECTION_DEFAULT_OFFER);
			            		dataEmail.put(Constants.KEY_EMAIL_COLOR_SECTION_THIRD_OFFER, Constants.KEY_EMAIL_COLOR_SECTION_DEFAULT_OFFER);
			            		
				            	if (confirmedInspection.getSelectedOffer() == 1){
				            		//OFERTA 30 DIAS
				            		dataEmail.put(Constants.KEY_EMAIL_COLOR_SECTION_FIRST_OFFER, Constants.KEY_EMAIL_COLOR_SECTION_SELECTED_OFFER);
				            	}else if (confirmedInspection.getSelectedOffer() == 2 ){
				            		//OFERTA INSTANTANEA
				            		dataEmail.put(Constants.KEY_EMAIL_COLOR_SECTION_SECOND_OFFER, Constants.KEY_EMAIL_COLOR_SECTION_SELECTED_OFFER);
				            	}else if(confirmedInspection.getSelectedOffer() == 3 ){
				            		//OFERTA EN CONSIGNACION
				            		dataEmail.put(Constants.KEY_EMAIL_COLOR_SECTION_THIRD_OFFER, Constants.KEY_EMAIL_COLOR_SECTION_SELECTED_OFFER);
				            	}
				            	
				            }

				            
				            // El sender es Celeste
				            InternetAddress fromEmail = new InternetAddress("celeste@kavak.com", "Celeste");
				            
				            
				            // REGISTRO EN BD PARA ENVIAR INVITES GOOGLE CALENDAR
				            try {
				            	saveConfirmedInspection(confirmedInspection);
							} catch (Exception e) {
								LogService.logger.error("Error intentando invocar metodo de registro de inspeccion confirmada");
								e.printStackTrace();
							}
				            
				            
				            // Si existe plantilla de correo, se realiza el envío
				            if (!templateEmail.isEmpty()){
					            boolean emailSend = KavakUtils.sendEmail(kavakEmail, emailCc, subjectData , templateEmail, templateEmailBase, dataEmail, fromEmail, emailReplyTo);
					            LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] - sendEmail retorno " + emailSend);
					            
					            if (emailSend){
					            	sentminervaIds.add(confirmedInspection.getId());
					            }
								
						        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
						        responseDTO.setCode(enumResult.getValue());
						        responseDTO.setStatus(enumResult.getStatus());
						        responseDTO.setData(sentminervaIds);
				            }else{
				            	LogService.logger.debug("No se definió plantilla de correo, se descarta email.");
				            	
						        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
						        responseDTO.setCode(enumResult.getValue());
						        responseDTO.setStatus(enumResult.getStatus());
						        responseDTO.setData(null);
				            }
				            
	        			}else if (!userDB.isPresent()){
	        				LogService.logger.info("El email del cliente " + confirmedInspection.getCustomerEmail().trim() + " no existe en nuestra BD, se descarta correo");
	        			}
	        		}else if(confirmedInspection.getCustomerEmail().trim().isEmpty()){
	        			LogService.logger.info("El email del cliente esta vacio, se descarta correo.");
	        		}else if(confirmedInspection.getWingmanEmail().trim().isEmpty()){
	        			LogService.logger.info("El email del wingman esta vacio, se descarta correo.");
	        		}else{
	        			LogService.logger.info("El email del lead manager esta vacio, se descarta correo.");
	        		}
	        		
	        	}
	        	
	        }else{
	        	LogService.logger.info("Se descarta servicio Celesta para envio de inspecciones confirmadas, no hay elementos en la lista.");
	        }
	        
		} catch (Exception e) {
			LogService.logger.error("Error enviando correos Celeste de inspecciones confirmadas.");
			e.printStackTrace();
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(null);
		}
		
		return responseDTO;
	}
	
	
	
	
	public ResponseDTO saveConfirmedInspection(ConfirmedInspectionNetsuiteDTO confirmedInspection){
		ResponseDTO responseDTO    = new ResponseDTO();
		Long userId      		   = null;
		Long inspectorId           = null;
		Long leadManagerId         = null;
		
		try {
			if(confirmedInspection != null){
				
					// CUSTOMER DATA
					if(confirmedInspection.getCustomerMinervaId() != null){
						LogService.logger.debug("El id del customer es " + confirmedInspection.getCustomerMinervaId());
						
						Optional<User> userDB = userRepo.findById(confirmedInspection.getCustomerMinervaId());
						
						if(userDB.isPresent()){
							
							// IF EMAILS HAS CHANGED, UPDATE MINERVA DATA
							if (!confirmedInspection.getCustomerEmail().toLowerCase().equals(userDB.get().getEmail())){
								LogService.logger.debug("El email del customer registrado en netsuite no coincide con el registrado en minerva, se actualizara minerva.");
								
								userDB.get().setEmail(confirmedInspection.getCustomerEmail().toLowerCase());
								userDB.get().setUserName(confirmedInspection.getCustomerEmail().toLowerCase());
								
								userRepo.save(userDB.get());
							}else{
								LogService.logger.debug("El email del customer es igual al que se tiene en BD.");
							}
							userId = userDB.get().getId();
							
							UserMeta userMetaDB = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, userId);
							
							if(userMetaDB == null){
								LogService.logger.info("No existe el telefono del customer en userMeta, se procede a crear con el numero " + confirmedInspection.getCustomerPhone().replaceAll(" ", "").replaceAll("-", ""));
								
								UserMeta customerMeta = new UserMeta();
								customerMeta.setMetaKey(Constants.META_VALUE_PHONE);
								customerMeta.setMetaValue(confirmedInspection.getCustomerPhone().replaceAll(" ", "").replaceAll("-", ""));
								customerMeta.setUserId(userId);
								customerMeta.setUser(userDB.get());
								userMetaRepo.save(customerMeta);
							}else{
								LogService.logger.info("El customer con telefono ya existe en usermeta con el id " + userMetaDB.getId());
							}
							
						}else{
							LogService.logger.debug("El customer con id " + confirmedInspection.getCustomerMinervaId() + " no existe, se procede a crear en BD.");
							
							User user = new User();
							user.setName(confirmedInspection.getInspectorName());
							user.setEmail(confirmedInspection.getInspectorEmail().toLowerCase());
							user.setUserName(confirmedInspection.getInspectorEmail().toLowerCase());
							user.setRole(Constants.ADMIN_ROLE);
							user.setPassword(Constants.DEFAULT_PASSWORD);
							user.setSource(Constants.SOURCE_NETSUITE);
							user.setActive(true);
							user.setCreationDate(new Timestamp(System.currentTimeMillis()));
							user.setIpUsuario(Inet4Address.getLocalHost().getHostAddress());
							
							User newUser = userRepo.save(user);
							
							UserMeta userMeta = new UserMeta();
							userMeta.setUserId(newUser.getId());
							userMeta.setMetaKey(Constants.META_VALUE_PHONE);
							userMeta.setMetaValue(confirmedInspection.getCustomerPhone().replaceAll("-","").replaceAll(" ", ""));
							userMeta.setUser(newUser);
							userMetaRepo.save(userMeta);
							
							userId = newUser.getId();
						}
					}
						
					// INSPECTOR DATA
					if (!confirmedInspection.getInspectorEmail().trim().isEmpty()){
						LogService.logger.debug("Se procede a buscar los datos del inspector con correo " + confirmedInspection.getInspectorEmail().trim());
						
						User inspectorDB = userRepo.findByEmailAndRole(confirmedInspection.getInspectorEmail().trim(), Constants.ADMIN_ROLE);
						
						if(inspectorDB != null){
							
							// IF INSPECTOR EMAIL HAS CHANGED, UPDATE MINERVA DATA
							if (!inspectorDB.getEmail().toLowerCase().equals(confirmedInspection.getInspectorEmail().toLowerCase())){
								LogService.logger.debug("El correo del inspector encontrado en netsuite no es el mismo que en BD, se procede a actualizar la BD");
								
								inspectorDB.setEmail(confirmedInspection.getInspectorEmail().toLowerCase().toLowerCase());
								inspectorDB.setUserName(confirmedInspection.getInspectorEmail().toLowerCase().toLowerCase());
								userRepo.save(inspectorDB);
							}
							
							UserMeta inspectorMetaDB = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, inspectorDB.getId());
							
							if(inspectorMetaDB == null){
								LogService.logger.info("No existe el telefono del inspector en userMeta, se procede a crear con el numero " + confirmedInspection.getInspectorPhone().replaceAll(" ", "").replaceAll("-", ""));
								
								UserMeta inspectorMeta = new UserMeta();
								inspectorMeta.setMetaKey(Constants.META_VALUE_PHONE);
								inspectorMeta.setMetaValue(confirmedInspection.getInspectorPhone().replaceAll(" ", "").replaceAll("-", ""));
								inspectorMeta.setUserId(inspectorDB.getId());
								inspectorMeta.setUser(inspectorDB);
								userMetaRepo.save(inspectorMeta);
							}else{
								LogService.logger.info("El inspector con telefono ya existe en usermeta con el id " + inspectorMetaDB.getId());
							}
							
							
						//CREATE AN INSPECTOR (USER) IN BD	
						}else{
							LogService.logger.debug("El correo del inspector " + confirmedInspection.getInspectorEmail().trim() + " no existe en BD, se procede a registrar.");
							
							User inspector = new User();
							inspector.setName(confirmedInspection.getInspectorName());
							inspector.setEmail(confirmedInspection.getInspectorEmail().toLowerCase());
							inspector.setUserName(confirmedInspection.getInspectorEmail().toLowerCase());
							inspector.setRole(Constants.ADMIN_ROLE);
							inspector.setPassword(Constants.DEFAULT_PASSWORD);
							inspector.setSource(Constants.SOURCE_NETSUITE);
							inspector.setActive(true);
							inspector.setCreationDate(new Timestamp(System.currentTimeMillis()));
							inspector.setIpUsuario(Inet4Address.getLocalHost().getHostAddress());
							
							User newInspector = userRepo.save(inspector);
							
							UserMeta inspectorMeta = new UserMeta();
							inspectorMeta.setUserId(newInspector.getId());
							inspectorMeta.setMetaKey(Constants.META_VALUE_PHONE);
							inspectorMeta.setMetaValue(confirmedInspection.getInspectorPhone().replaceAll("-","").replaceAll(" ", ""));
							inspectorMeta.setUser(newInspector);
							userMetaRepo.save(inspectorMeta);
							
							inspectorDB = newInspector;
						}
						
						inspectorId = inspectorDB.getId();
					}
						

					//LEAD MANAGER DATA
					if(!confirmedInspection.getLeadManagerEmail().trim().isEmpty()){
						LogService.logger.debug("Se procede a buscar los datos del lead manager con correo " + confirmedInspection.getLeadManagerEmail().trim());
						
						User leadManagerDB = userRepo.findByEmailAndRole(confirmedInspection.getLeadManagerEmail().trim(), Constants.ADMIN_ROLE);
						
						if(leadManagerDB != null){
							
							// IF LEAD MANAGER EMAIL HAS CHANGED, UPDATE MINERVA DATA
							if (!leadManagerDB.getEmail().toLowerCase().trim().equals(confirmedInspection.getLeadManagerEmail().toLowerCase().trim())){
								LogService.logger.debug("El correo del lead encontrado en netsuite no es el mismo que en BD, se procede a actualizar la BD");
								
								leadManagerDB.setEmail(confirmedInspection.getLeadManagerEmail().trim().toLowerCase());
								leadManagerDB.setUserName(confirmedInspection.getLeadManagerEmail().trim().toLowerCase());
								userRepo.save(leadManagerDB);
							}
							
							UserMeta leadMetaDB = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, leadManagerDB.getId());
							
							if(leadMetaDB == null){
								LogService.logger.info("No existe el telefono del lead en userMeta, se procede a crear con el numero " + confirmedInspection.getLeadManagerPhone().replaceAll(" ", "").replaceAll("-", ""));
								
								UserMeta leadMeta = new UserMeta();
								leadMeta.setMetaKey(Constants.META_VALUE_PHONE);
								leadMeta.setMetaValue(confirmedInspection.getLeadManagerPhone().replaceAll(" ", "").replaceAll("-", ""));
								leadMeta.setUserId(leadManagerDB.getId());
								leadMeta.setUser(leadManagerDB);
								userMetaRepo.save(leadMeta);
							}else{
								LogService.logger.info("El lead con telefono ya existe en usermeta con el id " + leadMetaDB.getId());
							}
							
						//CREATE A LEAD MANAGER (USER) IN BD	
						}else{
							LogService.logger.debug("El correo del lead " + confirmedInspection.getLeadManagerEmail().toLowerCase().trim() + " no existe, se procede a crear en BD.");
							
							
							User leadManager = new User();
							leadManager.setName(confirmedInspection.getLeadManagerName());
							leadManager.setEmail(confirmedInspection.getLeadManagerEmail().toLowerCase());
							leadManager.setUserName(confirmedInspection.getLeadManagerEmail().toLowerCase());
							leadManager.setRole(Constants.ADMIN_ROLE);
							leadManager.setPassword(Constants.DEFAULT_PASSWORD);
							leadManager.setSource(Constants.SOURCE_NETSUITE);
							leadManager.setActive(true);
							leadManager.setCreationDate(new Timestamp(System.currentTimeMillis()));
							leadManager.setIpUsuario(Inet4Address.getLocalHost().getHostAddress());
							
							User newleadManager = userRepo.save(leadManager);
							
							UserMeta leadMeta = new UserMeta();
							leadMeta.setUserId(newleadManager.getId());
							leadMeta.setMetaKey(Constants.META_VALUE_PHONE);
							leadMeta.setMetaValue(confirmedInspection.getLeadManagerPhone().replaceAll("-","").replaceAll(" ", ""));
							leadMeta.setUser(newleadManager);
							userMetaRepo.save(leadMeta);
							
							leadManagerDB = newleadManager;
						}
						
						leadManagerId = leadManagerDB.getId();
					}
					
					if (userId != null && inspectorId != null && leadManagerId != null){
						
						LogService.logger.debug("El id del customer " + userId);
						LogService.logger.debug("El id del inspectorId " + inspectorId);
						LogService.logger.debug("El id del leadManagerId " + leadManagerId);
						
						ConfirmedInspections confirmedInspectionDB = new ConfirmedInspections();
						
						confirmedInspectionDB.setOfferId(confirmedInspection.getOpportunityId());
						confirmedInspectionDB.setUserId(confirmedInspection.getCustomerMinervaId());
						confirmedInspectionDB.setInspectorId(inspectorId);
						confirmedInspectionDB.setLeadManagerId(leadManagerId);
						confirmedInspectionDB.setCarYear(confirmedInspection.getCarYear());
						confirmedInspectionDB.setCarMake(confirmedInspection.getCarMake());
						confirmedInspectionDB.setCarModel(confirmedInspection.getCarModel());
						confirmedInspectionDB.setRegisterDate(new Timestamp(System.currentTimeMillis()));
						
						confirmedInspectionDB.setActive(1L);
						
						if (confirmedInspection.getThirtyDaysOffer()!= null && !confirmedInspection.getThirtyDaysOffer().isEmpty()){
							confirmedInspectionDB.setThirtyOfferMax(confirmedInspection.getThirtyDaysOffer().replaceAll("$", "").replaceAll(",", "").replaceAll(" ", "").replaceAll("[^\\d.]", ""));
						}
						
						if(confirmedInspection.getInstantOffer() != null && !confirmedInspection.getInstantOffer().isEmpty()){
							confirmedInspectionDB.setInstantOfferMax(confirmedInspection.getInstantOffer().replaceAll("$", "").replaceAll(",", "").replaceAll(" ", "").replaceAll("[^\\d.]", ""));
						}
						
						if(confirmedInspection.getConsigmentOffer()!= null && !confirmedInspection.getConsigmentOffer().isEmpty()){
							confirmedInspectionDB.setConsignmentOfferMax(confirmedInspection.getConsigmentOffer().replaceAll("$", "").replaceAll(",", "").replaceAll(" ", "").replaceAll("[^\\d.]", ""));
						}
						
						if (confirmedInspection.getInspectionDate() != null && !confirmedInspection.getInspectionDate().isEmpty()){
							String[] parts = confirmedInspection.getInspectionDate().split("/");
							String year  = parts[2];
							String month = parts[1];
							String day   = parts[0];
							
							Long monthMin = Long.valueOf(month);
							Long dayMin   = Long.valueOf(day);
							if(monthMin < 10){
								month = "0" + month;
							}
							
							if(dayMin < 10){
								day = "0"+ day;
							}
							
							confirmedInspectionDB.setInspectionDate(year+"-"+month+"-"+day);
						}
						
						if(confirmedInspection.getInspectionTime() != null && !confirmedInspection.getInspectionTime().isEmpty()){
							String[] datePart = confirmedInspection.getInspectionTime().split(" a ");
							confirmedInspectionDB.setInspectionTimeStart(datePart[0].toUpperCase());
							confirmedInspectionDB.setInspectionTimeEnd(datePart[1].toUpperCase());
						}
						
						if (confirmedInspection.getCustomerAddress()!= null && !confirmedInspection.getCustomerAddress().isEmpty()){
							confirmedInspectionDB.setInspectionAddress(confirmedInspection.getCustomerAddress());
						}
						
						
						confirmedInspectionRepo.save(confirmedInspectionDB);

				        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
				        responseDTO.setCode(enumResult.getValue());
				        responseDTO.setStatus(enumResult.getStatus());
				        
					}else if(userId == null){
						LogService.logger.warn("No se encontró el usuario en BD, se descarta regitro en BD para inspecciones confirmadas");
					}else if(inspectorId == null){
						LogService.logger.warn("No se encontró el inspector en BD, se descarta regitro en BD para inspecciones confirmadas");
					}else{
						LogService.logger.warn("No se encontró el lead manager en BD, se descarta regitro en BD para inspecciones confirmadas");
					}

				
			}else{
				LogService.logger.info("No hay inspecciones confirmadas a registrar, se descarta servicio.");
			}
		} catch (Exception e) {
			LogService.logger.error("Error guardando informacion sobre inspecciones confirmadas.");
			e.printStackTrace();
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(null);
		}
		
		return responseDTO;
	}
	
	
	
	
	
	
	
	
	
	
	/*********************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * 																SHIPPING MANAGER EMAILS
	 * 
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 */
	
	
	/**
	 * Send PaperWork Shipping Manager Email
	 * 
	 * @param List<PaperworkShippingManagerDTO> paperworkDataList
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO sendNewPaperworkShippingManagerEmail(List<PaperworkShippingManagerDTO> paperworkDataList){
		ResponseDTO responseDTO 		= new ResponseDTO();
        String templateEmail      		= "";
        String developmentPrefixSubject = "[PRUEBA] ";
        String finalSubject			    = "¡Tienes unos trámites pendientes con KAVAK!";
        String titlePaperWorkContent    = finalSubject.toUpperCase();
        List<Long> sentminervaIds       = null;
        
		try {
	        List<String> emailCc               = new ArrayList<String>();
	        String kavakEmail                  = null;
	        HashMap<String,Object> dataEmail   = new HashMap<>();
	        HashMap<String,Object> subjectData = new HashMap<>();
	        String templateNameBase 		   = "/templates/paperwork/base_shipping_manager_email.html"; 
			
	        if(!paperworkDataList.isEmpty()){
	        	
	        	sentminervaIds = new ArrayList<>();
	        	
				LogService.logger.info("Entorno de trabajo: " + Constants.PROPERTY_ENVIRONMENT);
				
				if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
					finalSubject = developmentPrefixSubject + finalSubject;
				}
	        	
				for(PaperworkShippingManagerDTO managerMail : paperworkDataList){
					
		            subjectData.put(Constants.KEY_SUBJECT, finalSubject);
		            subjectData.put(Constants.KEY_EMAIL_PREFIX, managerMail.getShippingManagerEmail());
		            
		            dataEmail.put(Constants.KEY_EMAIL_TITLE_PAPERWORK_SHIPPING_MANAGER, titlePaperWorkContent);
		            dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, managerMail.getShippingManagerName());
		            dataEmail.put(Constants.KEY_EMAIL_USER_NAME, managerMail.getShippingManagerName());
		            dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, managerMail.getCar());
		            dataEmail.put(Constants.KEY_EMAIL_STARTED_DATE, managerMail.getStartedDate());
		            dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_TYPE, managerMail.getPaperworkTypeDescription());
		            dataEmail.put(Constants.KEY_EMAIL_CURRENT_PLATE_STATUS, managerMail.getCurrentPlateStatus());
		            dataEmail.put(Constants.KEY_EMAIL_FINAL_PLATE_STATUS, managerMail.getFinalPlateStatus());
		            
	    			kavakEmail = managerMail.getShippingManagerEmail();
		            if (managerMail.getBccEmail() != ""){
		            	LogService.logger.debug("Este llamado tiene bcc: " + managerMail.getBccEmail());
		            	emailCc.add(managerMail.getBccEmail().replaceAll("`", "")); 
		            }
					
		            if(managerMail.getEmailTemplate() != null){
		            	LogService.logger.info("Template de correo interno a usar: " + managerMail.getEmailTemplate().toUpperCase());
		            	templateEmail = "paperwork/shipping_manager/"+ managerMail.getEmailTemplate().toUpperCase() +".html";
		            }
		            
		            
		            //Revisar si tiene adjuntos
		            
		            if (managerMail.getAttachments() != null && managerMail.getAttachments().size() > 0){
		            	dataEmail.put(Constants.ATTACHMENT_FILES, managerMail.getAttachments());
		            }else{
		            	dataEmail.put(Constants.ATTACHMENT_FILES, null);
		            }
		            
		            boolean emailSend = KavakUtils.sendInternalEmail(kavakEmail, emailCc, subjectData , templateEmail, templateNameBase, dataEmail);
		            LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] - sendEmail retorno " + emailSend);
		            
		            if (emailSend){
		            	sentminervaIds.add(managerMail.getId());
		            }
				}
				
		        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		        responseDTO.setCode(enumResult.getValue());
		        responseDTO.setStatus(enumResult.getStatus());
		        responseDTO.setData(sentminervaIds);
	        	
	        }else{
	        	LogService.logger.info("El objeto de tramites para enviar correo a gestor esta vacio, se descarta correo.");
	        }
			
		} catch (Exception e) {
			LogService.logger.error("Error enviando correos Celeste para gestores sobre Tramites.");
			e.printStackTrace();
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(null);
			
		}

		return responseDTO;
	}
	
	
	/**
	 * Send Delay PaperWork Shipping Manager Email
	 * 
	 * @param List<PaperworkShippingManagerDTO> paperworkDataList
	 * @return ResponseDTO responseDTO
	 */
	
	public ResponseDTO sendDelayPaperworkShippingManagerEmail(List<PaperworkShippingManagerDTO> paperworkDataList){
		ResponseDTO responseDTO 		= new ResponseDTO();
        String templateEmail      		= "";
        String developmentPrefixSubject = "[PRUEBA] ";
        String finalSubject			    = "IMPORTANTE ¡Tienes unos trámites retrasados con KAVAK!";
        String titlePaperWorkContent    = "TIENES UN TR&Aacute;MITE ATRASADO CON KAVAK";
        List<Long> sentminervaIds       = null;
        
		try {
	        List<String> emailCc               = new ArrayList<String>();
	        String kavakEmail                  = null;
	        HashMap<String,Object> dataEmail   = new HashMap<>();
	        HashMap<String,Object> subjectData = new HashMap<>();
			String templateNameBase 		   = "/templates/paperwork/base_shipping_manager_email.html"; 
			
	        if(!paperworkDataList.isEmpty()){
	        	
	        	sentminervaIds = new ArrayList<>();
	        	
				LogService.logger.info("Entorno de trabajo: " + Constants.PROPERTY_ENVIRONMENT);
				
				if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
					finalSubject = developmentPrefixSubject + finalSubject;
				}
	        	
				for(PaperworkShippingManagerDTO managerMail : paperworkDataList){
					
		            subjectData.put(Constants.KEY_SUBJECT, finalSubject);
		            subjectData.put(Constants.KEY_EMAIL_PREFIX, managerMail.getShippingManagerEmail());
		            
		            dataEmail.put(Constants.KEY_EMAIL_TITLE_PAPERWORK_SHIPPING_MANAGER, titlePaperWorkContent);
		            dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, managerMail.getShippingManagerName());
		            dataEmail.put(Constants.KEY_EMAIL_USER_NAME, managerMail.getShippingManagerName());
		            dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, managerMail.getCar());
		            dataEmail.put(Constants.KEY_EMAIL_STARTED_DATE, managerMail.getStartedDate());
		            dataEmail.put(Constants.KEY_EMAIL_PAPERWORK_TYPE, managerMail.getPaperworkTypeDescription());
		            dataEmail.put(Constants.KEY_EMAIL_CURRENT_PLATE_STATUS, managerMail.getCurrentPlateStatus());
		            dataEmail.put(Constants.KEY_EMAIL_FINAL_PLATE_STATUS, managerMail.getFinalPlateStatus());
		            
	    			kavakEmail = managerMail.getShippingManagerEmail();
		            if (managerMail.getBccEmail() != ""){
		            	LogService.logger.debug("Este llamado tiene bcc: " + managerMail.getBccEmail());
		            	emailCc.add(managerMail.getBccEmail().replaceAll("`", "")); 
		            }
					
		            if(managerMail.getEmailTemplate() != null){
		            	LogService.logger.info("Template de correo interno a usar: " + managerMail.getEmailTemplate().toUpperCase());
		            	templateEmail = "paperwork/shipping_manager/"+ managerMail.getEmailTemplate().toUpperCase() +".html";
		            }
		            
		            //Revisar si tiene adjuntos
		            
		            if (managerMail.getAttachments() != null && managerMail.getAttachments().size() > 0){
		            	dataEmail.put(Constants.ATTACHMENT_FILES, managerMail.getAttachments());
		            }else{
		            	dataEmail.put(Constants.ATTACHMENT_FILES, null);
		            }
		            
		            boolean emailSend = KavakUtils.sendInternalEmail(kavakEmail, emailCc, subjectData , templateEmail, templateNameBase, dataEmail);
		            LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] - sendEmail retorno " + emailSend);
		            
		            if (emailSend){
		            	sentminervaIds.add(managerMail.getId());
		            }
				}
				
		        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		        responseDTO.setCode(enumResult.getValue());
		        responseDTO.setStatus(enumResult.getStatus());
		        responseDTO.setData(sentminervaIds);
	        	
	        }else{
	        	LogService.logger.info("El objeto de tramites para enviar correo a gestor esta vacio, se descarta correo.");
	        }
			
		} catch (Exception e) {
			LogService.logger.error("Error enviando correos Celeste para gestores sobre Tramites.");
			e.printStackTrace();
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(null);
		}

		return responseDTO;
	}
	
	
	public ResponseDTO sendInternalEmailCustomerReview(Long userId, Long carId, Long score, String comments){
		ResponseDTO responseDTO            = new ResponseDTO();
        List<String> emailCc               = new ArrayList<String>();
        String kavakEmail                  = null;
        HashMap<String,Object> dataEmail   = new HashMap<>();
        HashMap<String,Object> subjectData = new HashMap<>();
		//String templateNameBase 		   = "/templates/paperwork/base_shipping_manager_email.html"; 
		String templateEmail      		   = "paperwork/internal_email/P-109.html";
		String finalSubject			       = "¡Has recibido un review de un cliente!";
		String stringScore				   = ""; 
		
		try {
			if (userId != null && carId != null){
				LogService.logger.info("Ejecutando servicio de correo interno sobre el review del cliente.");
				
				Optional<User> userBD = userRepo.findById(userId);
				Optional<SellCarDetail> sellCarBD = sellCarRepo.findById(carId);
				
				if(userBD.isPresent() && sellCarBD.isPresent()){
					
					switch(score.intValue()){
						case 1:
							stringScore = "DEFICIENTE";
							break;
						case 2:
							stringScore = "ACEPTABLE";
							break;
						case 3:
							stringScore = "BUENO";
							break;
						case 4:
							stringScore = "MUY BUENO";
							break;
						case 5:
							stringScore = "EXCELENTE";
							break;
						default:
							stringScore = "NO APLICA";
							break;
					}
					
		            subjectData.put(Constants.KEY_SUBJECT, finalSubject);
		            subjectData.put(Constants.KEY_EMAIL_PREFIX, "angie@kavak.com");
		            
		            dataEmail.put(Constants.KEY_EMAIL_USER_NAME, userBD.get().getName());
		            dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, userBD.get().getEmail());
		            dataEmail.put(Constants.KEY_EMAIL_USER_ID, userBD.get().getId());
		            dataEmail.put(Constants.KEY_EMAIL_CAR_ID, sellCarBD.get().getId());
		            dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, sellCarBD.get().getCarMake() + " " + sellCarBD.get().getCarModel() + " " + sellCarBD.get().getCarYear());
		            dataEmail.put(Constants.KEY_EMAIL_USER_RANKING, stringScore);
		            dataEmail.put(Constants.KEY_EMAIL_USER_COMMENTS_RANKING, KavakUtils.html2text(comments));
		            dataEmail.put(Constants.ATTACHMENT_FILES, null);
		            
	    			kavakEmail = "angie@kavak.com";

	    			emailCc.add("roger@kavak.com");
	            	emailCc.add("carlos@kavak.com");
	            	emailCc.add("ricardo@kavak.com");
	            	emailCc.add("tony@kavak.com");
	            	emailCc.add("andres@kavak.com");
	            	emailCc.add("daniel@kavak.com");
	            	emailCc.add("jonathan@kavak.com");
	            	emailCc.add("alex@kavak.com");
	            	emailCc.add("lori@kavak.com");
		            
		            boolean emailSend = KavakUtils.sendInternalEmail(kavakEmail, emailCc, subjectData , templateEmail, null, dataEmail);
		            LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] - sendEmail retorno " + emailSend);
		            
		            if (emailSend){
				        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
				        responseDTO.setCode(enumResult.getValue());
				        responseDTO.setStatus(enumResult.getStatus());
				        responseDTO.setData(null);
		            }
				}else{
					LogService.logger.info("El Id del cliente " + userId + " no existe en nuestra BD, se descarta envio de correo interno sobre su review.");
				}
			}else{
				LogService.logger.info("El Id del cliente es nulo, se descarta el envio de correo interno sobre su review.");
			}
			
		} catch (Exception e) {
			LogService.logger.error("Error enviando correo interno del review del cliente con Lead " + userId);
			e.printStackTrace();
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(null);
		}
		
		return responseDTO;
	}
	
}
