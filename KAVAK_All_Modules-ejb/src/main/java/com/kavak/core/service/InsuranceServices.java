package com.kavak.core.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.response.GetDetailInsuranceResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.InsuranceDetail;
import com.kavak.core.model.MetaValue;
import com.kavak.core.repositories.InsuranceDetailRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.util.Constants;

/**
 * <h1>Data de Carros</h1>
 *
 *
 * @author  Enrique
 * @since   2017-06-28
 */

@Stateless
public class InsuranceServices {

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private InsuranceDetailRepository insuranceDetailRepo;

    /**
     * Consulta los datos de un seguro
     * @author Enrique Marin
     * @param name nombre del seguro
     * @param type tipo de seguro
     * @return  GetDetailInsuranceResponseDTO
     */
    public ResponseDTO getInsuranceDetail(String name, String type) {
        ResponseDTO responseDTO = new ResponseDTO();
        GetDetailInsuranceResponseDTO getDetailInsuranceResponseDTO = new GetDetailInsuranceResponseDTO();
        MetaValue metaValueBD = metaValueRepo.findByGroupCategoryAndGroupNameAndOptionName(Constants.GROUP_CATEGORY_INSURANCE,name.toUpperCase(),type);

        if(metaValueBD != null){

            List<String> listBenefits = new ArrayList<>();
            List<GenericDTO> listInsuranceDetailDataDTO = new ArrayList<>();
            InsuranceDetail insuranceDetailBD = insuranceDetailRepo.findByIdInsurance(metaValueBD.getId());

            for(String benefictActual: insuranceDetailBD.getBenefits().split("\\|")){
                String benefictActualWithSpace = benefictActual.replace("<p>","").replace("</p>","");
                if(benefictActualWithSpace.substring(0,1).equals(" ")){
                    benefictActualWithSpace = benefictActualWithSpace.substring(1,benefictActualWithSpace.length());
                }

                if(benefictActualWithSpace.substring(benefictActualWithSpace.length()-1,benefictActualWithSpace.length()).equals(" ")){
                    benefictActualWithSpace = benefictActualWithSpace.substring(0,benefictActualWithSpace.length()-1);
                }

                listBenefits.add(benefictActualWithSpace);
            }

            for(String detailActual: insuranceDetailBD.getDetails().split("#")){
                List<String> listDetailActual = Arrays.asList(detailActual.split(":"));
                if(listDetailActual.size() > 1){
                    GenericDTO detailGenericDTO = new GenericDTO();
                    detailGenericDTO.setName(listDetailActual.get(0).toString());
                    detailGenericDTO.setValue(listDetailActual.get(1).toString());
                    listInsuranceDetailDataDTO.add(detailGenericDTO);
                }
            }

            getDetailInsuranceResponseDTO.setListBenefits(listBenefits);
            getDetailInsuranceResponseDTO.setListInsuranceDetailDataDTO(listInsuranceDetailDataDTO);

        }else{
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());
            return responseDTO;
        }

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(getDetailInsuranceResponseDTO);
        return responseDTO;
    }
}