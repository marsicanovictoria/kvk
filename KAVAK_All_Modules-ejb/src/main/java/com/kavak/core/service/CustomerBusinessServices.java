package com.kavak.core.service;

import com.kavak.core.dto.CustomerReviewDTO;
import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.request.PostCustomerOpportunityCommentRequestDTO;
import com.kavak.core.dto.response.PostCustomerOpportunityCommentResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.*;
import com.kavak.core.repositories.*;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Stateless
public class CustomerBusinessServices {

	@EJB(mappedName = LookUpNames.SESSION_CelesteServices)
	private CelesteServices celesteServices;
	
    @Inject
    private UserRepository userRepo;

    @Inject
    private CustomerReviewRepository customerReviewRepo;

    @Inject
    private SellCarDetailRepository sellCarDetalRepo;

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private OfferCheckpointRepository offerCheckpointRepo;

    @Inject
    private SaleCheckpointRepository saleCheckpointRepo;

    public ResponseDTO setCustomerReview(CustomerReviewDTO customerReview) {

        ResponseDTO responseDTO = new ResponseDTO();
        boolean result = true;

        try {
            LogService.logger.info("Se invoca servicio para registro de review de cliente.");

            if (customerReview.getUserId() != null && customerReview.getCarId() != null) {

                Optional<User> userBD = userRepo.findById(customerReview.getUserId());
                Optional<SellCarDetail> sellCarDetailBD = sellCarDetalRepo.findById(customerReview.getCarId());

                if (userBD.isPresent() && sellCarDetailBD.isPresent()) {

                    LogService.logger.info("Se procede a registrar Customer Review del cliente " + userBD.get().getEmail().toLowerCase());

                    CustomerReviews customerReviewBD = new CustomerReviews();
                    customerReviewBD.setUserId(userBD.get().getId());
                    customerReviewBD.setUserName(userBD.get().getName().toUpperCase());
                    customerReviewBD.setUserEmail(userBD.get().getEmail().toLowerCase());
                    customerReviewBD.setCarId(sellCarDetailBD.get().getId());
                    customerReviewBD.setReviewType(customerReview.getReviewType());
                    customerReviewBD.setReviewContent(KavakUtils.html2text(customerReview.getReviewContent()));
                    customerReviewBD.setSource(customerReview.getSource());
                    customerReviewBD.setCreationDate(new Timestamp(System.currentTimeMillis()));
                    
                    if(customerReview.getReviewScore() != null){
                    	customerReviewBD.setReviewScore(customerReview.getReviewScore());
                    }else{
                    	customerReviewBD.setReviewScore(0L);
                    }

                    customerReviewRepo.save(customerReviewBD);

                    // Internal Email about this review
                    celesteServices.sendInternalEmailCustomerReview(userBD.get().getId(), sellCarDetailBD.get().getId(), customerReview.getReviewScore(), KavakUtils.html2text(customerReview.getReviewContent()));
                    
                    EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
                    responseDTO.setCode(enumResult2.getValue());
                    responseDTO.setStatus(enumResult2.getStatus());

                } else {
                    LogService.logger.error("El user_id " + customerReview.getUserId() + " o el car_id " + customerReview.getCarId() + " no existe, se descarta insert en customer review.");
                    result = false;
                }
            } else if (customerReview.getUserId() == null && customerReview.getCarId() == null) {
                LogService.logger.error("El campo user_id y car_id vienen vacio, se descarta insert en customer review.");
                result = false;
            } else if (customerReview.getUserId() == null) {
                LogService.logger.error("El campo user_id viene vacio, se descarta insert en customer review.");
                result = false;
            } else {
                LogService.logger.error("El campo car_id viene vacio, se descarta insert en customer review.");
                result = false;
            }

            if (!result) {
                EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult2.getValue());
                responseDTO.setStatus(enumResult2.getStatus());
            }

        } catch (Exception e) {
            LogService.logger.error("Ocurrió un error insertando un registro de customer review ", e);
            e.printStackTrace();

            EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult2.getValue());
            responseDTO.setStatus(enumResult2.getStatus());
        }

        return responseDTO;
    }

    /**
     * Metodo que identifica una Oportunidad para agregarle comentario o archivo de voice
     *
     * @param postCustomerOpportunityCommentRequestDTO Request del servicio
     * @return ResponseDTO reponse generico de servicio
     * @author Oscar Montilla
     */
    public ResponseDTO postCustomerOpportunityComment(PostCustomerOpportunityCommentRequestDTO postCustomerOpportunityCommentRequestDTO) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();


        if (postCustomerOpportunityCommentRequestDTO.getOpportunityType() == null || postCustomerOpportunityCommentRequestDTO.getCheckpointId() == null
                || postCustomerOpportunityCommentRequestDTO.getOpportunityType().length() == 0) {

            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString())
                    .getRequestDTO(" " + (postCustomerOpportunityCommentRequestDTO.getOpportunityType() == null
                            || postCustomerOpportunityCommentRequestDTO.getOpportunityType().length() == 0 ? "opportunity_type, " : "")
                            + (postCustomerOpportunityCommentRequestDTO.getCheckpointId() == null ? "checkpoint_id, " : ""));
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        if ((postCustomerOpportunityCommentRequestDTO.getComment() == null || postCustomerOpportunityCommentRequestDTO.getComment().length() == 0)
                && (postCustomerOpportunityCommentRequestDTO.getVoice() == null || postCustomerOpportunityCommentRequestDTO.getVoice().length() == 0)) {

            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString())
                    .getRequestDTO(" " + (postCustomerOpportunityCommentRequestDTO.getComment() == null
                            || postCustomerOpportunityCommentRequestDTO.getComment().length() == 0 ? "comment or " : "")
                            + (postCustomerOpportunityCommentRequestDTO.getVoice() == null

                            || postCustomerOpportunityCommentRequestDTO.getVoice().length() == 0 ? "voice, " : ""));
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;
        }

        PostCustomerOpportunityCommentResponseDTO postCustomerOpportunityCommentResponseDTO = new PostCustomerOpportunityCommentResponseDTO();

        switch (postCustomerOpportunityCommentRequestDTO.getOpportunityType()) {

            case Constants.OPPORTUNITY_PURCHASE:
                LogService.logger.info("Registrando Opportunity purchase...");

                Optional<OfferCheckpoint> offerCheckpointBD = offerCheckpointRepo.findById(postCustomerOpportunityCommentRequestDTO.getCheckpointId());

                if (!offerCheckpointBD.isPresent()) {

                    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0034.toString()).getRemplaceParameterDTO("checkpoint_id");
                    listMessageDTO.add(messageDataNull);
                    responseDTO.setListMessage(listMessageDTO);
                    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                    responseDTO.setCode(enumResult.getValue());
                    responseDTO.setStatus(enumResult.getStatus());
                    responseDTO.setData(new GenericDTO());

                    return responseDTO;
                }

                if (postCustomerOpportunityCommentRequestDTO.getComment() != null) {
                    LogService.logger.info("Registrando/actualizando comentario...");
                    offerCheckpointBD.get().setCustomerComment(postCustomerOpportunityCommentRequestDTO.getComment());
                    postCustomerOpportunityCommentResponseDTO.setComment(offerCheckpointBD.get().getCustomerComment());
                }


                if (postCustomerOpportunityCommentRequestDTO.getVoice() != null) {
                    LogService.logger.info("Registrando/actualizando Nota de voz...");
                    String voiceName = Constants.NAME_VOICE_CUSTOMER + Calendar.getInstance().getTimeInMillis();

                    KavakUtils.generateFile(postCustomerOpportunityCommentRequestDTO.getVoice(), voiceName, Constants.URL_FULL_PATH_FOLDER_CUSTOMER, Constants.FILE_TYPE_M4A, false);
                    offerCheckpointBD.get().setCustomerVoice(Constants.URL_PATH_CUSTOMER + voiceName + "." + Constants.FILE_TYPE_M4A);
                    postCustomerOpportunityCommentResponseDTO.setVoiceUrl(Constants.URL_KAVAK + offerCheckpointBD.get().getCustomerVoice());

                }
                offerCheckpointBD.get().setSentNetsuite(2L);

                break;

            case Constants.OPPORTUNITY_APPOINTMENT:
                LogService.logger.info("Registrando Opportunity Appointment...");

                Optional<SaleCheckpoint> saleCheckpointBD = saleCheckpointRepo.findById(postCustomerOpportunityCommentRequestDTO.getCheckpointId());

                if (!saleCheckpointBD.isPresent()) {

                    MessageDTO messageDataaNull = messageRepo.findByCode(MessageEnum.M0034.toString()).getRemplaceParameterDTO("checkpoint_id");
                    listMessageDTO.add(messageDataaNull);
                    responseDTO.setListMessage(listMessageDTO);
                    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                    responseDTO.setCode(enumResult.getValue());
                    responseDTO.setStatus(enumResult.getStatus());
                    responseDTO.setData(new GenericDTO());

                    return responseDTO;
                }

                if (postCustomerOpportunityCommentRequestDTO.getComment() != null) {
                    LogService.logger.info("Registrando comentario...");
                    saleCheckpointBD.get().setCustomerComment(postCustomerOpportunityCommentRequestDTO.getComment());
                    postCustomerOpportunityCommentResponseDTO.setComment(saleCheckpointBD.get().getCustomerComment());
                }

                if (postCustomerOpportunityCommentRequestDTO.getVoice() != null) {
                    LogService.logger.info("Registrando/actualizando Nota de voz...");
                    String voiceName = Constants.NAME_VOICE_CUSTOMER + Calendar.getInstance().getTimeInMillis();

                    KavakUtils.generateFile(postCustomerOpportunityCommentRequestDTO.getVoice(), voiceName, Constants.URL_FULL_PATH_FOLDER_CUSTOMER, Constants.FILE_TYPE_M4A, false);
                    saleCheckpointBD.get().setCustomerVoice(Constants.URL_PATH_CUSTOMER + voiceName + "." + Constants.FILE_TYPE_M4A);
                    postCustomerOpportunityCommentResponseDTO.setVoiceUrl(Constants.URL_KAVAK + saleCheckpointBD.get().getCustomerVoice());

                }
                saleCheckpointBD.get().setSentNetsuite(2L);

                break;

            case Constants.OPPORTUNITY_BOOKING:
                LogService.logger.info("Registrando Opportunity Booking...");

                Optional<SaleCheckpoint> saleCheckpointBookingBD = saleCheckpointRepo.findById(postCustomerOpportunityCommentRequestDTO.getCheckpointId());

                if (!saleCheckpointBookingBD.isPresent()) {

                    MessageDTO messageDataaNull = messageRepo.findByCode(MessageEnum.M0034.toString()).getRemplaceParameterDTO("checkpoint_id");
                    listMessageDTO.add(messageDataaNull);
                    responseDTO.setListMessage(listMessageDTO);
                    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                    responseDTO.setCode(enumResult.getValue());
                    responseDTO.setStatus(enumResult.getStatus());
                    responseDTO.setData(new GenericDTO());

                    return responseDTO;
                }

                if (postCustomerOpportunityCommentRequestDTO.getComment() != null) {
                    LogService.logger.info("Registrando comentario...");
                    saleCheckpointBookingBD.get().setCustomerComment(postCustomerOpportunityCommentRequestDTO.getComment());
                    postCustomerOpportunityCommentResponseDTO.setComment(saleCheckpointBookingBD.get().getCustomerComment());
                }

                if (postCustomerOpportunityCommentRequestDTO.getVoice() != null) {
                    LogService.logger.info("Registrando/actualizando Nota de voz...");
                    String voiceName = Constants.NAME_VOICE_CUSTOMER + Calendar.getInstance().getTimeInMillis();

                    KavakUtils.generateFile(postCustomerOpportunityCommentRequestDTO.getVoice(), voiceName, Constants.URL_FULL_PATH_FOLDER_CUSTOMER, Constants.FILE_TYPE_M4A, false);
                    saleCheckpointBookingBD.get().setCustomerVoice(Constants.URL_PATH_CUSTOMER + voiceName + "." + Constants.FILE_TYPE_M4A);
                    postCustomerOpportunityCommentResponseDTO.setVoiceUrl(Constants.URL_KAVAK + saleCheckpointBookingBD.get().getCustomerVoice());

                }
                saleCheckpointBookingBD.get().setSentNetsuite(2L);

                break;
        }

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(postCustomerOpportunityCommentResponseDTO);

        return responseDTO;

    }


}
