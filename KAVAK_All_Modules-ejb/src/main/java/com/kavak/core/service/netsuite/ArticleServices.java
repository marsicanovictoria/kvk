package com.kavak.core.service.netsuite;

import com.kavak.core.dto.CarDataDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.CarData;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.util.LogService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Optional;


@Stateless
public class ArticleServices {

	@Inject
	private CarDataRepository carDataRepo;
	
	
	public ResponseDTO getArticleById(Long id){
		ResponseDTO responseDTO = new ResponseDTO();
		CarDataDTO carDataDTO = new CarDataDTO();
		
		try {
			Optional<CarData> carDataBD = carDataRepo.findById(id);
			carDataDTO = carDataBD.get().getDTO();
			
		} catch (Exception e) {
			LogService.logger.debug("Error al intentar consultar el artículo con ID " + id + ", " + e);
		}
		
        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(carDataDTO);
		return responseDTO;
	}
	
	
	public ResponseDTO getArticleBySku(String sku){
		ResponseDTO responseDTO = new ResponseDTO();
		CarDataDTO carDataDTO = new CarDataDTO();
		
		try {
			CarData carDataBD = carDataRepo.findBySku(sku);
			carDataDTO = carDataBD.getDTO();
			
		} catch (Exception e) {
			LogService.logger.debug("Error al intentar consultar el artículo con ID " + sku + ", " + e);
		}
		
        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(carDataDTO);
		return responseDTO;
	}
}
