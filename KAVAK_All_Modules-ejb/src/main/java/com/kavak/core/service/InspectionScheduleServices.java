package com.kavak.core.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.time.DateUtils;

import com.kavak.core.dto.InspectionScheduleBlockDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.dto.response.ScheduleBlocksDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.InspectionLocation;
import com.kavak.core.model.InspectionSchedule;
import com.kavak.core.model.InspectionScheduleBlock;
import com.kavak.core.model.MetaValue;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.InspectionScheduleBlockRepository;
import com.kavak.core.repositories.InspectionScheduleRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

@Stateless
public class InspectionScheduleServices {

    @Inject
    private InspectionScheduleRepository inspectionScheduleRepo;

    @Inject
    private InspectionLocationRepository inspectionLocationRepo;

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private InspectionScheduleBlockRepository inspectionScheduleBlockRepo;

    @Inject
    private MessageRepository messageRepo;

    public void freeInspectionSchedule() {
	LogService.logger.info("Iniciando metodo freeInspectionSchedule()...");
	MetaValue metaValue = metaValueRepo.findByAlias("TFS");
	List<InspectionSchedule> listScheduleInspection = inspectionScheduleRepo.findForLiberate(Integer.valueOf(metaValue.getOptionName()));
	if (listScheduleInspection != null && !listScheduleInspection.isEmpty()) {
	    LogService.logger.info("Liberando " + listScheduleInspection.size() + " bloque(s) de horarios de inspeccion");
	    for (InspectionSchedule inspectionSchedule : listScheduleInspection) {
		inspectionScheduleRepo.delete(inspectionSchedule);
	    }
	} else {
	    LogService.logger.info("No se encontraron bloques de horarios de inspeccion para liberar");
	}
    }

    /**
     * Libera un horario de inspeccion borrandolo de la entidad con el id De entrada
     * 
     * @author Enrique Marin
     * @param idInspectionSchedule
     * @return List<ScheduleBlocksLockedDTO> contiene los bloques de horarios disponibles y ocupados
     */
    public ResponseDTO deleteInspectionSchedule(Long idInspectionSchedule) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getReservedInspectionSchedule) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();

	ResponseDTO responseDTO = new ResponseDTO();
	if (!inspectionScheduleRepo.findById(idInspectionSchedule).isPresent()) {
	    MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0011.toString()).getDTO();
	    return KavakUtils.getResponseNoDataFound(messageNotFound);
	}
	InspectionSchedule inspectionScheduleBD = new InspectionSchedule();
	inspectionScheduleBD.setId(idInspectionSchedule);
	inspectionScheduleRepo.delete(inspectionScheduleBD);
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());

	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getReservedInspectionSchedule) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Consulta los bloques de horarios disponibles y ocupados desde una fecha dada hasta los proximos (Constants.DAYS_INSPECTIONS_BLOCK) dias
     * 
     * @author Enrique Marin
     * @param dateFrom
     *            fecha desde donde se tomara en cuenta la consulta
     * @param inspectionLocationId
     *            identificador del centro de inspeccion a consultar
     * @return List<ScheduleBlocksLockedDTO> contiene los bloques de horarios disponibles y ocupados
     */
    public ResponseDTO getScheduleBlocks(String dateFrom, Long inspectionLocationId) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getScheduleBlocks) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	ScheduleBlocksDTO scheduleBlocksDTO;
	List<ScheduleBlocksDTO> listScheduleBlocksDTO = new ArrayList<ScheduleBlocksDTO>();
	SimpleDateFormat parseFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");

	Calendar calendarDateFrom = Calendar.getInstance();
	Calendar calendarDateFromKeyMap = Calendar.getInstance();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	String keyMapParsed;
	List<InspectionScheduleBlockDTO> listInspectionScheduleBlockDTO;
	List<InspectionSchedule> listInspectionsByDateAndLocationId;
	List<InspectionSchedule> listInspectionsByDateAndIdHourBlockAndInspectionLocationId;
	Date dateFromParsed = null;
	Date dateKey;
	Date today = new Date();
	String todaySf = df.format(today);
	String dateFromSf;
	int countOfSundays = 0;
	Locale locale = new Locale("es", "MX");
	Calendar todayC = Calendar.getInstance(TimeZone.getTimeZone("America/Mexico_City"), locale);
	Long hour = (long) todayC.get(Calendar.HOUR_OF_DAY);
	List<InspectionScheduleBlock> listInspectionScheduleBlock = inspectionScheduleBlockRepo.findAll();

	try {
	    dateFromParsed = df.parse(dateFrom);
	    calendarDateFrom.setTime(dateFromParsed);

	    // Para manejar dias bloqueados por regla de negocio
	    Calendar calendarDateBlocked = Calendar.getInstance();
//	    Calendar calendarDateBlockedSunday = Calendar.getInstance();
//	    Calendar calendarDateBlockedMonday = Calendar.getInstance();
	    calendarDateBlocked.setTime(df.parse("2018-05-18"));
//	    calendarDateBlockedSunday.setTime(df.parse("2018-09-02"));
//	    calendarDateBlockedMonday.setTime(df.parse("2018-09-03"));

	    // Máximo número de inspecciones por día a domicilio
	    MetaValue maxInspectionsUserAddressPerDay = metaValueRepo.findByAlias("MAXINSPUSRADDRDAY");

	    // Máximo número de inspecciones por bloque a domicilio
	    MetaValue maxInspectionsUserAddressPerBlock = metaValueRepo.findByAlias("MAXINSPUSRADDRBLOCK");

	    // Día especial con más inspecciones a domicilio (1-Domingo, 2-Lunes, etc, 0-Ningun dia)
	    MetaValue noTopInspectionsUserAddressDay = metaValueRepo.findByAlias("NOTOPINSPUSRADDRDAY");

	    // Máximo número de inspecciones por día a domicilio, para dia especial con más inspecciones
	    MetaValue noTopInspectionsUserAddressPerDay = metaValueRepo.findByAlias("NOTOPINSPUSRADDRPERDAY");

	    // Máximo número de inspecciones por bloque a domicilio, para dia especial con más inspecciones
	    MetaValue noTopInspectionsUserAddressPerBlock = metaValueRepo.findByAlias("NOTOPINSPUSRADDRPERBLOCK");

	    // IssueID #2870 - Dia de la semana con más inspecciones por bloque a domicilio
	    Integer integerMaxInspectionsUserAddressPerDay = null;
	    Integer integerMaxInspectionsUserAddressPerBlock = null;
	    // ========================================================== >>

	    LogService.logger.info("Invocando servicio getScheduleBlocks [dateFrom=" + dateFrom + ", locationId=" + inspectionLocationId + "]");

	    // Se muestran máximo 7 días para inspección
	    for (int indexDays = 1; indexDays <= Constants.MAX_DAYS_SCHEDULE_BLOCK7 + countOfSundays; indexDays++) {
		listInspectionScheduleBlockDTO = new ArrayList<>();
		dateKey = parseFormat.parse(calendarDateFrom.getTime().toString());
		keyMapParsed = df.format(dateKey);
		calendarDateFromKeyMap.setTime(dateKey);
		dateFromSf = df.format(dateFromParsed);

		// IssueID #2870 - Dia de la semana con más inspecciones por bloque a domicilio
		// Se habilita un dia de la semana con un numero más alto de inspecciones por bloque.
		if (calendarDateFrom.get(Calendar.DAY_OF_WEEK) == Integer.valueOf(noTopInspectionsUserAddressDay.getOptionName())) {
		    integerMaxInspectionsUserAddressPerDay = Integer.valueOf(noTopInspectionsUserAddressPerDay.getOptionName());
		    integerMaxInspectionsUserAddressPerBlock = Integer.valueOf(noTopInspectionsUserAddressPerBlock.getOptionName());
		}
		// Comportamiento Normal Max inspecciones por bloque a domicilio viene de BD
		else {
		    integerMaxInspectionsUserAddressPerDay = Integer.valueOf(maxInspectionsUserAddressPerDay.getOptionName());
		    integerMaxInspectionsUserAddressPerBlock = Integer.valueOf(maxInspectionsUserAddressPerBlock.getOptionName());
		}
		// ========================================================== >>

		if (inspectionLocationId != null) {
		    // Cantidad de Inspecciones por Fecha y Direccion
		    listInspectionsByDateAndLocationId = inspectionScheduleRepo.findByInspectionDateAndInspectionLocationId(dateKey, inspectionLocationId);
		    Optional<InspectionLocation> inspectionLocationBD = inspectionLocationRepo.findById(inspectionLocationId);

		    // Si la dirección es domicilio del cliente, se valida si la cantidad de inspecciones es la máx por dia configurada en BD
		    if (inspectionLocationId == Constants.INSPECTION_LOCATION_USER_ADDRESS) {

			if (listInspectionsByDateAndLocationId != null && listInspectionsByDateAndLocationId.size() >= integerMaxInspectionsUserAddressPerDay) {
			    calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
			    continue;
			}
		    }
		    // Otros centros, valor de maximas inspecciones por dia viene de BD
		    else {
			if (listInspectionsByDateAndLocationId != null && listInspectionsByDateAndLocationId.size() >= inspectionLocationBD.get().getInspectionsPerDay()) {
			    calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
			    continue;
			}
		    }
		    // ============================================================================================================= >>>

		    // Para el día actual solo se permiten inspecciones en Lerma
		    /*if (indexDays == 1 && dateFromSf.equals(todaySf) && inspectionLocationId != Constants.INSPECTION_LOCATION_LERMA) {
			calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
			continue;
		    }*/
		    // ============================================================================================================= >>>

		    // Excluir 18/05/2018 para Lerma
		    if (inspectionLocationId.equals(Constants.INSPECTION_LOCATION_LERMA) && DateUtils.isSameDay(calendarDateFrom, calendarDateBlocked)) {
			calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
			continue;
		    }
		    // ============================================================================================================= >>>
		    // Excluir 2/09/2018 para Lerma
//		    if (inspectionLocationId.equals(Constants.INSPECTION_LOCATION_LERMA) && DateUtils.isSameDay(calendarDateFrom, calendarDateBlockedSunday)) {
//			calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
//			continue;
//		    }
		    // ============================================================================================================= >>>
		    // Excluir 3/09/2018 para Lerma
//		    if (inspectionLocationId.equals(Constants.INSPECTION_LOCATION_LERMA) && DateUtils.isSameDay(calendarDateFrom, calendarDateBlockedMonday)) {
//			calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
//			continue;
//		    }
		    // ============================================================================================================= >>>

		    
		}

		// Se excluyen los días domingos
		if (calendarDateFromKeyMap.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
		    countOfSundays++;
		} else if (indexDays == 1 && dateFromSf.equals(todaySf) && hour >= 15) {
		    calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
		    continue;
		}else {

		    for (InspectionScheduleBlock inspectionScheduleBlockActual : listInspectionScheduleBlock) {
			InspectionScheduleBlockDTO inspectionScheduleBlockDTO = inspectionScheduleBlockActual.getDTO();

			// Se debe bloquear del primer dia los bloques anteriores a la hora actual
			if (indexDays == 1 && dateFromSf.equals(todaySf)) {
			    if (inspectionScheduleBlockDTO.getIdHourBlock() == 1) {// 9-11
				if (hour >= 9) {
				    inspectionScheduleBlockDTO.setAvailable(false);
				}
			    }
			    if (inspectionScheduleBlockDTO.getIdHourBlock() == 2) {// 11-1
				if (hour >= 11) {
				    inspectionScheduleBlockDTO.setAvailable(false);
				}
			    }
			    if (inspectionScheduleBlockDTO.getIdHourBlock() == 3) {// 1-3
				if (hour >= 13) {
				    inspectionScheduleBlockDTO.setAvailable(false);
				}
			    }
			    if (inspectionScheduleBlockDTO.getIdHourBlock() == 4) {// 3-5
				if (hour >= 15) {
				    inspectionScheduleBlockDTO.setAvailable(false);
				}
			    }
			}
			// ================================================================================ >>>

			// Se debe bloquear del dia Viernes el bloque con el id 4 (hora 3 a 5)
			// if (calendarDateFromKeyMap.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY && inspectionScheduleBlockDTO.getIdHourBlock() == 4) {
			// inspectionScheduleBlockDTO.setAvailable(false);
			// }
			// ================================================================================ >>>

			if (inspectionLocationId != null) {
			    // Se determina si el bloque esta disponible para la Fecha y Direccion
			    listInspectionsByDateAndIdHourBlockAndInspectionLocationId = inspectionScheduleRepo.findByInspectionDateAndIdHourBlockAndInspectionLocationId(dateKey, inspectionScheduleBlockDTO.getIdHourBlock(), inspectionLocationId);
			    Optional<InspectionLocation> inspectionLocationBD = inspectionLocationRepo.findById(inspectionLocationId);

			    // Domicilio Max Inspección por Bloque viene de BD
			    if (inspectionLocationId == Constants.INSPECTION_LOCATION_USER_ADDRESS) {

				// Inspecciones por bloque a domicilio parametrizado en BD
				if (listInspectionsByDateAndIdHourBlockAndInspectionLocationId != null && listInspectionsByDateAndIdHourBlockAndInspectionLocationId.size() >= integerMaxInspectionsUserAddressPerBlock) {
				    inspectionScheduleBlockDTO.setAvailable(false);
				}
			    }
			    // Otros centros, valor de maximas inspecciones por dia viene de BD
			    else {
				if (listInspectionsByDateAndIdHourBlockAndInspectionLocationId != null && listInspectionsByDateAndIdHourBlockAndInspectionLocationId.size() >= inspectionLocationBD.get().getInspectionsPerBlock()) {
				    inspectionScheduleBlockDTO.setAvailable(false);
				}
			    }
			    // ============================================================================================================= >>>
			}
			listInspectionScheduleBlockDTO.add(inspectionScheduleBlockDTO);
		    }

		    scheduleBlocksDTO = new ScheduleBlocksDTO();
		    scheduleBlocksDTO.setName(keyMapParsed);
		    scheduleBlocksDTO.setListInspectionScheduleBlockDTO(listInspectionScheduleBlockDTO);
		    listScheduleBlocksDTO.add(scheduleBlocksDTO);
		}
		calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
	    }

	} catch (ParseException e1) {
	    e1.printStackTrace();
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listScheduleBlocksDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getScheduleBlocks) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

}