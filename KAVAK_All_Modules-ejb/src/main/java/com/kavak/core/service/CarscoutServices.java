package com.kavak.core.service;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.kavak.core.dto.AnswerDTO;
import com.kavak.core.dto.CarscoutQuestionsDTO;
import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.request.PostCarscoutAlertRequestDTO;
import com.kavak.core.dto.request.PostCarscoutFilterCoincidencesRequestDTO;
import com.kavak.core.dto.request.PostCarscoutQuestionsAnswersRequestDTO;
import com.kavak.core.dto.response.CarscoutQuestionsResponseDTO;
import com.kavak.core.dto.response.GetCarscoutFilterValuesResponseDTO;
import com.kavak.core.dto.response.GetCarscoutModelVersionFilterValuesResponseDTO;
import com.kavak.core.dto.response.GetCarscoutNotifyResponseDTO;
import com.kavak.core.dto.response.GetCarscoutResultsResponseDTO;
import com.kavak.core.dto.response.GetCarscoutfilterCarResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.dto.specification.CarscoutAlertSpecificationDTO;
import com.kavak.core.dto.specification.CarscoutFilterCoincidencesSpecificationDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.CarBodyType;
import com.kavak.core.model.CarColor;
import com.kavak.core.model.CarMake;
import com.kavak.core.model.CarMetaAccesories;
import com.kavak.core.model.CarModel;
import com.kavak.core.model.CarVersion;
import com.kavak.core.model.CarscoutAlert;
import com.kavak.core.model.CarscoutAlertDetail;
import com.kavak.core.model.CarscoutAlertLkpSku;
import com.kavak.core.model.CarscoutCatalogue;
import com.kavak.core.model.CarscoutCategory;
import com.kavak.core.model.CarscoutNotify;
import com.kavak.core.model.CarscoutQuestion;
import com.kavak.core.model.CarscoutQuestionAnswer;
import com.kavak.core.model.CarscoutQuestionOptions;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.User;
import com.kavak.core.repositories.CarBodyTypeRepository;
import com.kavak.core.repositories.CarColorRepository;
import com.kavak.core.repositories.CarMakeRepository;
import com.kavak.core.repositories.CarMetaAccesoriesRepository;
import com.kavak.core.repositories.CarModelRepository;
import com.kavak.core.repositories.CarVersionRepository;
import com.kavak.core.repositories.CarscoutAlertDetailRepository;
import com.kavak.core.repositories.CarscoutAlertLkpSkuRepository;
import com.kavak.core.repositories.CarscoutAlertRepository;
import com.kavak.core.repositories.CarscoutCatalogueRepository;
import com.kavak.core.repositories.CarscoutCategoryRepository;
import com.kavak.core.repositories.CarscoutNotifyRepository;
import com.kavak.core.repositories.CarscoutQuestionAnswerRepository;
import com.kavak.core.repositories.CarscoutQuestionOptionRepository;
import com.kavak.core.repositories.CarscoutQuestionRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.CarscoutSpecifications;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

@Stateless
public class CarscoutServices {

    @Inject
    private CarMetaAccesoriesRepository CarMetaAccesoriesRepo;

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private CarMakeRepository carMakeRepo;

    @Inject
    private CarBodyTypeRepository carBodyTypeRepo;

    @Inject
    private CarColorRepository carColorRepo;

    @Inject
    private CarVersionRepository carVersionRepo;

    @Inject
    private CarModelRepository carModelRepo;

    @Inject
    private CarscoutCatalogueRepository carscoutCatalogueRepo;

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private UserRepository userRepo;

    @Inject
    private CarscoutAlertRepository carscoutAlertRepo;

    @Inject
    private CarscoutNotifyRepository carscoutNotifyRepo;

    @Inject
    private CarscoutAlertLkpSkuRepository carscoutAlertLkpSkuRepo;

    @Inject
    private CarscoutCategoryRepository carscoutCategoryRepo;

    @Inject
    private CarscoutAlertDetailRepository carscoutAlertDetailRepo;

    @Inject
    private CarMetaAccesoriesRepository carMetaAccesoriesrepo;

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;

    @Inject
    private CarscoutQuestionRepository carscoutQuestionRepo;

    @Inject
    private CarscoutQuestionOptionRepository carscoutQuestionOptionRepo;

    @Inject
    private CarscoutQuestionAnswerRepository carscoutQuestionAnswerRepo;

    /**
     * Metodo que obtiene la información de los filtros de Carscout
     *
     * @return ResponseDTO Response Generico de servicio
     * @author Oscar Montilla
     */

    public ResponseDTO getCarscoutFilterValues() {

        ResponseDTO responseDTO = new ResponseDTO();

        GetCarscoutFilterValuesResponseDTO getCarscoutFilterValuesResponseDTO = new GetCarscoutFilterValuesResponseDTO();

        getCarscoutFilterValuesResponseDTO.setMinPrice(carscoutCatalogueRepo.findByMinSalePrice(Constants.ACTIVE_DATA).toString());
        getCarscoutFilterValuesResponseDTO.setMaxPrice(carscoutCatalogueRepo.findByMaxSalePrice(Constants.ACTIVE_DATA).toString());
        getCarscoutFilterValuesResponseDTO.setMaxKm(carscoutCatalogueRepo.finByMaxKm(Constants.ACTIVE_DATA).toString());
        getCarscoutFilterValuesResponseDTO.setMinKm(carscoutCatalogueRepo.finByMinKm(Constants.ACTIVE_DATA).toString());

        List<GenericDTO> listGenericDTOYears = new ArrayList<>();
        List<GenericDTO> listGenericDTODoors = new ArrayList<>();
        List<GenericDTO> listGenericDTOCylinders = new ArrayList<>();
        List<GenericDTO> listGenericDTOSeats = new ArrayList<>();
        List<GenericDTO> listGenericDTOTraccion = new ArrayList<>();
        List<GenericDTO> listGenericDTOFuel = new ArrayList<>();
        List<GenericDTO> listGenericDTOMake = new ArrayList<>();
        List<GenericDTO> listGenericDTOBodyType = new ArrayList<>();
        List<GenericDTO> listGenericDTOColor = new ArrayList<>();
        List<GenericDTO> listGenericDTOTransmissions = new ArrayList<>();

        for (CarMetaAccesories accesoriesYear : CarMetaAccesoriesRepo.findByCarscoutCategoryIdAndActiveTrue(Constants.CATEGORY_YEAR)) {
            GenericDTO years = new GenericDTO();
            years.setCategoryId(accesoriesYear.getCarscoutCategory().getId());
            years.setId(accesoriesYear.getId());
            years.setName(accesoriesYear.getDescription());
            listGenericDTOYears.add(years);

        }

        Collections.sort(listGenericDTOYears, GenericDTO.COMPARE_BY_NAME);
        getCarscoutFilterValuesResponseDTO.setGenericDTOListYear(listGenericDTOYears);

        for (CarMetaAccesories accesoriesDoors : CarMetaAccesoriesRepo.findByCarscoutCategoryIdAndActiveTrue(Constants.CATEGORY_DOORS)) {
            GenericDTO door = new GenericDTO();
            door.setCategoryId(accesoriesDoors.getCarscoutCategory().getId());
            door.setId(accesoriesDoors.getId());
            door.setName(accesoriesDoors.getDescription());
            listGenericDTODoors.add(door);

        }

        getCarscoutFilterValuesResponseDTO.setGenericDTOListDoors(listGenericDTODoors);

        for (CarMetaAccesories accesoriesCylinders : CarMetaAccesoriesRepo.findByCarscoutCategoryIdAndActiveTrue(Constants.CATEGORY_CYLINDERS)) {
            GenericDTO cylinder = new GenericDTO();
            cylinder.setCategoryId(accesoriesCylinders.getCarscoutCategory().getId());
            cylinder.setId(accesoriesCylinders.getId());
            cylinder.setName(accesoriesCylinders.getDescription());
            listGenericDTOCylinders.add(cylinder);

        }

        getCarscoutFilterValuesResponseDTO.setGenericDTOListCylinders(listGenericDTOCylinders);

        for (CarMetaAccesories accesoriesSeats : CarMetaAccesoriesRepo.findByCarscoutCategoryIdAndActiveTrue(Constants.CATEGORY_SEATS)) {
            GenericDTO seat = new GenericDTO();
            seat.setCategoryId(accesoriesSeats.getCarscoutCategory().getId());
            seat.setId(accesoriesSeats.getId());
            seat.setName(accesoriesSeats.getDescription());
            listGenericDTOSeats.add(seat);

        }

        getCarscoutFilterValuesResponseDTO.setGenericDTOListSeats(listGenericDTOSeats);

        for (CarMetaAccesories accesoriesTraccion : CarMetaAccesoriesRepo.findByCarscoutCategoryIdAndActiveTrue(Constants.CATEGORY_TRACTION)) {
            GenericDTO traccion = new GenericDTO();
            traccion.setCategoryId(accesoriesTraccion.getCarscoutCategory().getId());
            traccion.setId(accesoriesTraccion.getId());
            traccion.setName(accesoriesTraccion.getDescription());
            listGenericDTOTraccion.add(traccion);

        }

        getCarscoutFilterValuesResponseDTO.setGenericDTOListTraction(listGenericDTOTraccion);

        for (CarMetaAccesories accesoriesFuel : CarMetaAccesoriesRepo.findByCarscoutCategoryIdAndActiveTrue(Constants.CATEGORY_FUELTYPE)) {
            GenericDTO fuelType = new GenericDTO();
            fuelType.setCategoryId(accesoriesFuel.getCarscoutCategory().getId());
            fuelType.setId(accesoriesFuel.getId());
            fuelType.setName(accesoriesFuel.getDescription());
            listGenericDTOFuel.add(fuelType);

        }

        getCarscoutFilterValuesResponseDTO.setGenericDTOListFuelType(listGenericDTOFuel);

        for (CarMake accesoriesMake : carMakeRepo.findByActiveTrue()) {
            GenericDTO make = new GenericDTO();
            make.setCategoryId(Constants.CATEGORY_MAKE);
            make.setId(accesoriesMake.getId());
            make.setName(accesoriesMake.getName());
            if (accesoriesMake.getImageMakeUrl() != null) {
                make.setImageUrl(Constants.URL_KAVAK + accesoriesMake.getImageMakeUrl());
            }
            listGenericDTOMake.add(make);
        }

        getCarscoutFilterValuesResponseDTO.setGenericDTOListMake(listGenericDTOMake);

        for (CarBodyType accesoriesStyle : carBodyTypeRepo.findByActiveTrue()) {
            GenericDTO bodyType = new GenericDTO();
            bodyType.setCategoryId(Constants.CATEGORY_BODY_TYPE);
            bodyType.setId(accesoriesStyle.getId());
            bodyType.setName(accesoriesStyle.getName());
            if (accesoriesStyle.getImage() != null) {
                bodyType.setImageUrl(Constants.URL_KAVAK + accesoriesStyle.getImage());
            }
            listGenericDTOBodyType.add(bodyType);
        }

        getCarscoutFilterValuesResponseDTO.setGenericDTOListBodyType(listGenericDTOBodyType);

        for (CarColor accesoriesColor : carColorRepo.findAllByActiveTrueOrderByPosition()) {
            GenericDTO color = new GenericDTO();
            color.setCategoryId(Constants.CATEGORY_COLOR);
            color.setId(accesoriesColor.getId());
            color.setName(accesoriesColor.getName());
            if (accesoriesColor.getHexadecimal() != null) {
                color.setHexadecimal(accesoriesColor.getHexadecimal());
            } else
                color.setHexadecimal("NONE");
            listGenericDTOColor.add(color);
        }

        getCarscoutFilterValuesResponseDTO.setGenericDTOListColor(listGenericDTOColor);

        for (CarMetaAccesories accesoriesTransmissions : carMetaAccesoriesrepo.findByCarscoutCategoryIdAndActiveTrue(Constants.CATEGORY_TRASMISSIONS)) {
            GenericDTO transmissions = new GenericDTO();
            transmissions.setCategoryId(Constants.CATEGORY_TRASMISSIONS);
            transmissions.setId(accesoriesTransmissions.getId());
            transmissions.setName(accesoriesTransmissions.getDescription());

            listGenericDTOTransmissions.add(transmissions);
        }

        getCarscoutFilterValuesResponseDTO.setGenericDTOListTransmissions(listGenericDTOTransmissions);

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(getCarscoutFilterValuesResponseDTO);

        return responseDTO;

    }

    /**
     * Metodo que obtiene la información de los Modelo y versiones de Carscout
     *
     * @return ResponseDTO Response Generico de servicio
     * @author Oscar Montilla
     */

    public ResponseDTO getCarscoutModelVersionFilterValues(String search) {

        ResponseDTO responseDTO = new ResponseDTO();

        List<GetCarscoutModelVersionFilterValuesResponseDTO> FilterValuesResponseDTO = new ArrayList<>();

        for (CarVersion carVersion : carVersionRepo.findByDescriptionContainingOrderByCarModelDescriptionAsc(search)) {
            GetCarscoutModelVersionFilterValuesResponseDTO filterValues = new GetCarscoutModelVersionFilterValuesResponseDTO();
            filterValues.setModelId(carVersion.getCarModel().getId());
            filterValues.setVersionId(carVersion.getId());
            filterValues.setName(carVersion.getCarModel().getCarMake().getName() + " " + carVersion.getCarModel().getDescription() + " " + carVersion.getDescription());
            FilterValuesResponseDTO.add(filterValues);
        }

        for (CarModel carModel : carModelRepo.findByDescriptionContainingAndActiveTrueAndCarMakeActiveTrueOrderByDescriptionAsc(search)) {
            GetCarscoutModelVersionFilterValuesResponseDTO filterValues = new GetCarscoutModelVersionFilterValuesResponseDTO();
            filterValues.setModelId(carModel.getId());
            filterValues.setVersionId(null);
            filterValues.setName(carModel.getDescription());
            FilterValuesResponseDTO.add(filterValues);
        }

        Collections.sort(FilterValuesResponseDTO);
        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(FilterValuesResponseDTO);

        return responseDTO;
    }

    /**
     * Metodo que obtiene la informacion de las coincidencias CarScout
     *
     * @param postCarscoutFilterCoincidencesRequestDTO Request del servicio
     * @return ResponseDTO Response Generico de servicio
     * @author Oscar Montilla
     */
    public ResponseDTO getCarscoutFilterCoincidences(PostCarscoutFilterCoincidencesRequestDTO postCarscoutFilterCoincidencesRequestDTO) {

        ResponseDTO responseDTO = new ResponseDTO();

        List<Long> idsListYears = new ArrayList<>();
        List<Long> idsListBodytype = new LinkedList<>();
        List<Long> idsListMake = new LinkedList<>();
        List<Long> idsListVersion = new LinkedList<>();
        List<Long> idsListModel = new LinkedList<>();
        List<Long> idsListColor = new LinkedList<>();
        List<Long> idsListDoors = new LinkedList<>();
        List<Long> idsListSeats = new LinkedList<>();
        List<Long> idsListCylinders = new LinkedList<>();
        List<Long> idsListTraction = new LinkedList<>();
        List<Long> idsListFueltype = new LinkedList<>();
        List<Long> idsListTransmissions = new LinkedList<>();

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListYear() != null) {
            for (GenericDTO getCarscoutFilterValuesYear : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListYear()) {
                idsListYears.add(getCarscoutFilterValuesYear.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListBodyType() != null) {
            for (GenericDTO getCarscoutFilterBodyYpe : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListBodyType()) {
                idsListBodytype.add(getCarscoutFilterBodyYpe.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListMake() != null) {
            for (GenericDTO getCarscoutFilterValuesMake : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListMake()) {
                idsListMake.add(getCarscoutFilterValuesMake.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListColor() != null) {
            for (GenericDTO getCarscoutFilterValuesColor : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListColor()) {
                idsListColor.add(getCarscoutFilterValuesColor.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListDoors() != null) {
            for (GenericDTO getCarscoutFilterValuesDoor : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListDoors()) {
                idsListDoors.add(getCarscoutFilterValuesDoor.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListSeats() != null) {
            for (GenericDTO getCarscoutFilterValuesSeat : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListSeats()) {
                idsListSeats.add(getCarscoutFilterValuesSeat.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListCylinders() != null) {
            for (GenericDTO getCarscoutFilterValuesCylinder : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListCylinders()) {
                idsListCylinders.add(getCarscoutFilterValuesCylinder.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListTraction() != null) {
            for (GenericDTO getCarscoutFilterValuesTraction : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListTraction()) {
                idsListTraction.add(getCarscoutFilterValuesTraction.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListFuelType() != null) {
            for (GenericDTO getCarscoutFilterValuesFuelType : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListFuelType()) {
                idsListFueltype.add(getCarscoutFilterValuesFuelType.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListVersion() != null) {
            for (GenericDTO getCarscoutFilterValuesVersion : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListVersion()) {
                idsListVersion.add(getCarscoutFilterValuesVersion.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListModel() != null) {
            for (GenericDTO getCarscoutFilterValuesModel : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListModel()) {
                idsListModel.add(getCarscoutFilterValuesModel.getId());
            }
        }

        if (postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListTransmissions() != null) {
            for (GenericDTO getCarscoutFilterValuesTransmission : postCarscoutFilterCoincidencesRequestDTO.getGenericDTOListTransmissions()) {
                idsListTransmissions.add(getCarscoutFilterValuesTransmission.getId());
            }
        }

        GenericDTO genericDTO = new GenericDTO();

        CarscoutFilterCoincidencesSpecificationDTO carscoutFilterCoincidencesSpecificationDTO = new CarscoutFilterCoincidencesSpecificationDTO();

        carscoutFilterCoincidencesSpecificationDTO.setIdsListYears(idsListYears);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListBodytype(idsListBodytype);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListMake(idsListMake);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListVersion(idsListVersion);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListModel(idsListModel);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListDoors(idsListDoors);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListSeats(idsListSeats);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListCylinders(idsListCylinders);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListTraction(idsListTraction);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListFueltype(idsListFueltype);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListColor(idsListColor);
        carscoutFilterCoincidencesSpecificationDTO.setIdsListTransmissions(idsListTransmissions);
        carscoutFilterCoincidencesSpecificationDTO.setMinPrice(postCarscoutFilterCoincidencesRequestDTO.getMinPrice());
        carscoutFilterCoincidencesSpecificationDTO.setMaxPrice(postCarscoutFilterCoincidencesRequestDTO.getMaxPrice());
        carscoutFilterCoincidencesSpecificationDTO.setMaxKm(postCarscoutFilterCoincidencesRequestDTO.getMaxKm());
        carscoutFilterCoincidencesSpecificationDTO.setMinKm(postCarscoutFilterCoincidencesRequestDTO.getMinKm());

        genericDTO.setCount(carscoutCatalogueRepo.count(CarscoutSpecifications.findByPredicateCarscoutFilterCoincidences(carscoutFilterCoincidencesSpecificationDTO)));

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(genericDTO);
        return responseDTO;
    }

    /**
     * Metodo para registrar Alerta de Carscout
     *
     * @param postCarscoutAlertRequestDTO Request del servicio
     * @return ResponseDTO Response Generico de servicio
     * @author Oscar Montilla
     */
    public ResponseDTO postCarscoutAlert(PostCarscoutAlertRequestDTO postCarscoutAlertRequestDTO) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();

        if (postCarscoutAlertRequestDTO.getUserId() == null) {

            LogService.logger.debug("Esta recibiendo el usuario null..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (postCarscoutAlertRequestDTO.getUserId() == null ? "user_id, " : ""));
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);

            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListBodyType() == null && postCarscoutAlertRequestDTO.getGenericDTOListCylinders() == null && postCarscoutAlertRequestDTO.getGenericDTOListDoors() == null && postCarscoutAlertRequestDTO.getGenericDTOListFuelType() == null && postCarscoutAlertRequestDTO.getGenericDTOListMake() == null && postCarscoutAlertRequestDTO.getGenericDTOListModel() == null && postCarscoutAlertRequestDTO.getGenericDTOListSeats() == null && postCarscoutAlertRequestDTO.getGenericDTOListTraction() == null && postCarscoutAlertRequestDTO.getGenericDTOListYear() == null && postCarscoutAlertRequestDTO.getGenericDTOListVersion() == null && postCarscoutAlertRequestDTO.getMinPrice() == null && postCarscoutAlertRequestDTO.getMaxPrice() == null
                && postCarscoutAlertRequestDTO.getGenericDTOListColor() == null && postCarscoutAlertRequestDTO.getMaxKm() == null && postCarscoutAlertRequestDTO.getGenericDTOListTransmissions() == null) {

            LogService.logger.debug("Debe enviar data de coincidencias de Carscout..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0033.toString()).getDTO();
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);

            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        if (postCarscoutAlertRequestDTO.getMinPrice() != null && postCarscoutAlertRequestDTO.getMaxPrice() == null) {

            LogService.logger.debug("Debe incluir el rango de precio..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0039.toString()).getRemplaceParameterDTO("price");
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);

        }

        if (postCarscoutAlertRequestDTO.getMinPrice() == null && postCarscoutAlertRequestDTO.getMaxPrice() != null) {

            LogService.logger.debug("Debe incluir el rango de precio..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0039.toString()).getRemplaceParameterDTO("price");
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);

        }

        if (postCarscoutAlertRequestDTO.getMinKm() != null && postCarscoutAlertRequestDTO.getMaxKm() == null) {

            LogService.logger.debug("Debe incluir el rango de precio..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0039.toString()).getRemplaceParameterDTO("km");
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);

        }

        Optional<User> userBD = userRepo.findById(postCarscoutAlertRequestDTO.getUserId());

        if (!userBD.isPresent()) {

            LogService.logger.debug("El usuario no existe en Base de datos..");
            MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
            listMessageDTO.add(messageUserNotExist);
            responseDTO.setListMessage(listMessageDTO);

        }

        MetaValue metaValueBD = metaValueRepo.findByAlias(Constants.META_VALUE_ALERTS_MAX);

        if (userBD.isPresent()) {

            Integer count = carscoutAlertRepo.findbyCountAlertUser(userBD.get().getId(), Constants.ACTIVE_DATA.intValue());

            if (count >= Integer.parseInt(metaValueBD.getOptionName())) {

                LogService.logger.debug("El usuario tiene limite de Alertas Activas creadas...");
                MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0076.toString()).getRemplaceParameterDTO(metaValueBD.getOptionName());
                listMessageDTO.add(messageUserNotExist);
                responseDTO.setListMessage(listMessageDTO);

            }
        }

        if (responseDTO.getListMessage() != null) {

            LogService.logger.debug("Existe una lista de Mensaje de validacion, devolver los mensajes...");
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;
        }

        List<Long> idsListYears = new LinkedList<>();
        List<Long> idsListBodytype = new LinkedList<>();
        List<Long> idsListMake = new LinkedList<>();
        List<Long> idsListVersion = new LinkedList<>();
        List<Long> idsListModel = new LinkedList<>();
        List<Long> idsListDoors = new LinkedList<>();
        List<Long> idsListSeats = new LinkedList<>();
        List<Long> idsListCylinders = new LinkedList<>();
        List<Long> idsListTraction = new LinkedList<>();
        List<Long> idsListFueltype = new LinkedList<>();
        List<Long> idsListColor = new LinkedList<>();
        List<Long> idsListTransmissions = new LinkedList<>();

        if (postCarscoutAlertRequestDTO.getGenericDTOListYear() != null) {
            for (GenericDTO getCarscoutFilterValuesYear : postCarscoutAlertRequestDTO.getGenericDTOListYear()) {
                idsListYears.add(getCarscoutFilterValuesYear.getId());
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListBodyType() != null) {
            for (GenericDTO getCarscoutFilterBodyYpe : postCarscoutAlertRequestDTO.getGenericDTOListBodyType()) {
                    idsListBodytype.add(getCarscoutFilterBodyYpe.getId());
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListMake() != null) {
            for (GenericDTO getCarscoutFilterValuesMake : postCarscoutAlertRequestDTO.getGenericDTOListMake()) {
                idsListMake.add(getCarscoutFilterValuesMake.getId());
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListDoors() != null) {
            for (GenericDTO getCarscoutFilterValuesDoor : postCarscoutAlertRequestDTO.getGenericDTOListDoors()) {
                idsListDoors.add(getCarscoutFilterValuesDoor.getId());
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListSeats() != null) {
            for (GenericDTO getCarscoutFilterValuesSeat : postCarscoutAlertRequestDTO.getGenericDTOListSeats()) {
                idsListSeats.add(getCarscoutFilterValuesSeat.getId());
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListCylinders() != null) {
            for (GenericDTO getCarscoutFilterValuesCylinder : postCarscoutAlertRequestDTO.getGenericDTOListCylinders()) {
                idsListCylinders.add(getCarscoutFilterValuesCylinder.getId());
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListTraction() != null) {
            for (GenericDTO getCarscoutFilterValuesTraction : postCarscoutAlertRequestDTO.getGenericDTOListTraction()) {
                idsListTraction.add(getCarscoutFilterValuesTraction.getId());
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListFuelType() != null) {
            for (GenericDTO getCarscoutFilterValuesFuelType : postCarscoutAlertRequestDTO.getGenericDTOListFuelType()) {
                idsListFueltype.add(getCarscoutFilterValuesFuelType.getId());
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListVersion() != null) {
            for (GenericDTO getCarscoutFilterValuesVersion : postCarscoutAlertRequestDTO.getGenericDTOListVersion()) {
                idsListVersion.add(getCarscoutFilterValuesVersion.getId());
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListModel() != null) {
            for (GenericDTO getCarscoutFilterValuesModel : postCarscoutAlertRequestDTO.getGenericDTOListModel()) {
                if (!idsListModel.contains(getCarscoutFilterValuesModel.getId())) {
                    LogService.logger.info("El Id suministrado de Modelo no se encuentra repetido...");
                    idsListModel.add(getCarscoutFilterValuesModel.getId());
                }
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListColor() != null) {
            for (GenericDTO getCarscoutFilterValuesColor : postCarscoutAlertRequestDTO.getGenericDTOListColor()) {
                idsListColor.add(getCarscoutFilterValuesColor.getId());
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListTransmissions() != null) {
            for (GenericDTO getCarscoutFilterValuesTransmissions : postCarscoutAlertRequestDTO.getGenericDTOListTransmissions()) {
                idsListTransmissions.add(getCarscoutFilterValuesTransmissions.getId());
            }
        }

        CarscoutAlertSpecificationDTO carscoutAlertSpecificationDTO = new CarscoutAlertSpecificationDTO();

        carscoutAlertSpecificationDTO.setIdsListYears(idsListYears);
        carscoutAlertSpecificationDTO.setIdsListBodytype(idsListBodytype);
        carscoutAlertSpecificationDTO.setIdsListCylinders(idsListCylinders);
        carscoutAlertSpecificationDTO.setIdsListDoors(idsListDoors);
        carscoutAlertSpecificationDTO.setIdsListFueltype(idsListFueltype);
        carscoutAlertSpecificationDTO.setIdsListMake(idsListMake);
        carscoutAlertSpecificationDTO.setIdsListModel(idsListModel);
        carscoutAlertSpecificationDTO.setIdsListSeats(idsListSeats);
        carscoutAlertSpecificationDTO.setIdsListTraction(idsListTraction);
        carscoutAlertSpecificationDTO.setIdsListVersion(idsListVersion);
        carscoutAlertSpecificationDTO.setIdsListTransmissions(idsListTransmissions);
        if (postCarscoutAlertRequestDTO.getMinPrice() != null && postCarscoutAlertRequestDTO.getMaxPrice() != null) {
            carscoutAlertSpecificationDTO.setMinPrice((int) Double.parseDouble(postCarscoutAlertRequestDTO.getMinPrice()));
            carscoutAlertSpecificationDTO.setMaxPrice((int) Double.parseDouble(postCarscoutAlertRequestDTO.getMaxPrice()));
        }
        carscoutAlertSpecificationDTO.setMaxKm(postCarscoutAlertRequestDTO.getMaxKm());
        carscoutAlertSpecificationDTO.setMinKm(postCarscoutAlertRequestDTO.getMinKm());

        LogService.logger.debug("Estoy armando la data de CarscoutAlert..");
        CarscoutAlert carscoutAlertSave = new CarscoutAlert();
        carscoutAlertSave.setUser(userBD.get());
        carscoutAlertSave.setActiveInd(Constants.ACTIVE_DATA.intValue());
        carscoutAlertSave.setLastUpdate(new Timestamp(System.currentTimeMillis()));
        carscoutAlertSave.setCreationDate(new Timestamp(System.currentTimeMillis()));

        LogService.logger.debug("Registrando el Source donde se ejecuto el servicio..");
        if (postCarscoutAlertRequestDTO.getSource() != null) {
            carscoutAlertSave.setSource(postCarscoutAlertRequestDTO.getSource());
        } else {
            carscoutAlertSave.setSource(Constants.SOURCE_APOLO);
        }

        if (postCarscoutAlertRequestDTO.getVersionApp() != null) {
            carscoutAlertSave.setVersionApp(postCarscoutAlertRequestDTO.getVersionApp());
        }

        CarscoutAlert carscoutAlertBD = carscoutAlertRepo.save(carscoutAlertSave);

        List<CarscoutAlertLkpSku> CarscoutAlertLkpSkuListSave = new ArrayList<>();

        for (CarscoutCatalogue carscoutCatalogueBD : carscoutCatalogueRepo.findAll(CarscoutSpecifications.findByPredicateCarscoutAlert(carscoutAlertSpecificationDTO))) {

            LogService.logger.debug("armando la data de v2_carscout_alert_lkp_sku..");
            CarscoutAlertLkpSku CarscoutAlertLkpSku = new CarscoutAlertLkpSku();
            CarscoutAlertLkpSku.setCarscoutAlert(carscoutAlertBD);
            CarscoutAlertLkpSku.setCarscoutCatalogue(carscoutCatalogueBD);
            CarscoutAlertLkpSkuListSave.add(CarscoutAlertLkpSku);
        }

        carscoutAlertLkpSkuRepo.saveAll(CarscoutAlertLkpSkuListSave);

        LogService.logger.debug("armando la data de CarscoutAlertDetail..");
        List<CarscoutAlertDetail> CarscoutAlertDetailListSave = new ArrayList<>();

        if (postCarscoutAlertRequestDTO.getMinPrice() != null && postCarscoutAlertRequestDTO.getMaxPrice() != null) {

            LogService.logger.debug("Armando la data de CATEGORY_PRICE..");

            CarscoutAlertDetail price = new CarscoutAlertDetail();

            price.setCarscoutAlert(carscoutAlertBD);
            price.setCarscoutCategory(carscoutCategoryRepo.getOne(Constants.CATEGORY_PRICE));
            price.setParamId1((int) Double.parseDouble(postCarscoutAlertRequestDTO.getMinPrice()));
            price.setParamId2((int) Double.parseDouble(postCarscoutAlertRequestDTO.getMaxPrice()));
            CarscoutAlertDetailListSave.add(price);
        }
        if (postCarscoutAlertRequestDTO.getMaxKm() != null && postCarscoutAlertRequestDTO.getMinKm() == null) {

            LogService.logger.debug("Armando la data de CATEGORY_KM..");

            CarscoutAlertDetail km = new CarscoutAlertDetail();

            km.setCarscoutAlert(carscoutAlertBD);
            km.setCarscoutCategory(carscoutCategoryRepo.getOne(Constants.CATEGORY_KM));
            km.setParamId1(postCarscoutAlertRequestDTO.getMaxKm());
            CarscoutAlertDetailListSave.add(km);

        } else if (postCarscoutAlertRequestDTO.getMaxKm() != null && postCarscoutAlertRequestDTO.getMinKm() != null) {

            LogService.logger.debug("Armando la data de CATEGORY_KM RANGO..");

            CarscoutAlertDetail km = new CarscoutAlertDetail();

            km.setCarscoutAlert(carscoutAlertBD);
            km.setCarscoutCategory(carscoutCategoryRepo.getOne(Constants.CATEGORY_KM));
            km.setParamId1(postCarscoutAlertRequestDTO.getMaxKm());
            km.setParamId2(postCarscoutAlertRequestDTO.getMinKm());
            CarscoutAlertDetailListSave.add(km);

        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListMake() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_MAKE..");

            CarscoutCategory carscoutCategoryMakeBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_MAKE);

            for (Long id : idsListMake) {
                CarscoutAlertDetail make = new CarscoutAlertDetail();
                make.setCarscoutAlert(carscoutAlertBD);
                make.setCarscoutCategory(carscoutCategoryMakeBD);
                make.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(make);
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListModel() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_MODEL..");

            CarscoutCategory carscoutCategoryModelBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_MODEL);

            for (Long id : idsListModel) {
                CarscoutAlertDetail model = new CarscoutAlertDetail();
                model.setCarscoutAlert(carscoutAlertBD);
                model.setCarscoutCategory(carscoutCategoryModelBD);
                model.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(model);
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListVersion() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_VERSION..");

            CarscoutCategory carscoutCategoryVersionBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_VERSION);

            for (Long id : idsListVersion) {
                CarscoutAlertDetail version = new CarscoutAlertDetail();
                version.setCarscoutAlert(carscoutAlertBD);
                version.setCarscoutCategory(carscoutCategoryVersionBD);
                version.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(version);
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListYear() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_YEAR..");

            CarscoutCategory carscoutCategoryYearsBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_YEAR);

            for (Long id : idsListYears) {
                CarscoutAlertDetail year = new CarscoutAlertDetail();
                year.setCarscoutAlert(carscoutAlertBD);
                year.setCarscoutCategory(carscoutCategoryYearsBD);
                year.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(year);
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListBodyType() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_BODY_TYPE..");

            CarscoutCategory carscoutCategoryBodyTypeBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_BODY_TYPE);

            for (Long id : idsListBodytype) {
                CarscoutAlertDetail bodyType = new CarscoutAlertDetail();
                bodyType.setCarscoutAlert(carscoutAlertBD);
                bodyType.setCarscoutCategory(carscoutCategoryBodyTypeBD);
                bodyType.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(bodyType);
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListDoors() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_DOORS..");

            CarscoutCategory carscoutCategoryDoorsBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_DOORS);

            for (Long id : idsListDoors) {
                CarscoutAlertDetail door = new CarscoutAlertDetail();
                door.setCarscoutAlert(carscoutAlertBD);
                door.setCarscoutCategory(carscoutCategoryDoorsBD);
                door.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(door);
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListCylinders() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_CYLINDERS..");

            CarscoutCategory carscoutCategoryCylindersBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_CYLINDERS);

            for (Long id : idsListCylinders) {
                CarscoutAlertDetail cylinders = new CarscoutAlertDetail();
                cylinders.setCarscoutAlert(carscoutAlertBD);
                cylinders.setCarscoutCategory(carscoutCategoryCylindersBD);
                cylinders.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(cylinders);
            }
        }

        if (postCarscoutAlertRequestDTO.getGenericDTOListTraction() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_TRACTION..");

            CarscoutCategory carscoutCategoryTractionBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_TRACTION);

            for (Long id : idsListTraction) {
                CarscoutAlertDetail traction = new CarscoutAlertDetail();
                traction.setCarscoutAlert(carscoutAlertBD);
                traction.setCarscoutCategory(carscoutCategoryTractionBD);
                traction.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(traction);
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListFuelType() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_FUELTYPE..");

            CarscoutCategory carscoutCategoryFuelTypeBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_FUELTYPE);

            for (Long id : idsListFueltype) {
                CarscoutAlertDetail fuelType = new CarscoutAlertDetail();
                fuelType.setCarscoutAlert(carscoutAlertBD);
                fuelType.setCarscoutCategory(carscoutCategoryFuelTypeBD);
                fuelType.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(fuelType);
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListSeats() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_SEATS..");

            CarscoutCategory carscoutCategorySeatsBD = carscoutCategoryRepo.getOne(Constants.CATEGORY_SEATS);

            for (Long id : idsListSeats) {
                CarscoutAlertDetail seat = new CarscoutAlertDetail();
                seat.setCarscoutAlert(carscoutAlertBD);
                seat.setCarscoutCategory(carscoutCategorySeatsBD);
                seat.setParamId1(id.intValue());
                CarscoutAlertDetailListSave.add(seat);
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListColor() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_COLOR..");

            CarscoutCategory carscoutCategoryColor = carscoutCategoryRepo.getOne(Constants.CATEGORY_COLOR);

            for (Long id : idsListColor) {
                LogService.logger.debug("Llega el id CATEGORY_COLOR " + id);
                CarscoutAlertDetail color = new CarscoutAlertDetail();
                color.setCarscoutAlert(carscoutAlertBD);
                color.setCarscoutCategory(carscoutCategoryColor);
                color.setParamId1(id.intValue());

                CarscoutAlertDetailListSave.add(color);
            }
        }
        if (postCarscoutAlertRequestDTO.getGenericDTOListTransmissions() != null) {
            LogService.logger.debug("Armando la data de CATEGORY_TRASMISSIONS..");

            CarscoutCategory carscoutCategoryTransmissions = carscoutCategoryRepo.getOne(Constants.CATEGORY_TRASMISSIONS);

            for (Long id : idsListTransmissions) {
                LogService.logger.debug("Llega el id CATEGORY_TRASMISSIONS " + id);
                CarscoutAlertDetail transmissions = new CarscoutAlertDetail();
                transmissions.setCarscoutAlert(carscoutAlertBD);
                transmissions.setCarscoutCategory(carscoutCategoryTransmissions);
                transmissions.setParamId1(id.intValue());

                CarscoutAlertDetailListSave.add(transmissions);
            }
        }

        carscoutAlertDetailRepo.saveAll(CarscoutAlertDetailListSave);

        GenericDTO genericDTO = new GenericDTO();

        genericDTO.setAlertId(carscoutAlertBD.getId());

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(genericDTO);

        return responseDTO;

    }

    /**
     * Metodo que se utiliza para eliminar el registro de una alarma de Carscout de un usuario
     *
     * @param userId  id de usuario
     * @param alertId id de alerta
     * @return ResponseDTO Generico de servicio
     * @author Oscar Montilla
     */
    public ResponseDTO deleteCarscoutAlert(Long userId, Long alertId) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();

        if (userId == null || alertId == null) {
            LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (userId == null ? "user_id, " : "") + (alertId == null ? "alert_id, " : ""));
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;
        }

        Optional<User> userBD = userRepo.findById(userId);

        if (!userBD.isPresent()) {

            LogService.logger.info("El usuario no existe en Base de datos..");
            MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
            listMessageDTO.add(messageUserNotExist);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());
            responseDTO.setListMessage(listMessageDTO);

            return responseDTO;
        }

        Optional<CarscoutAlert> CarscoutAlertBD = carscoutAlertRepo.findById(alertId);

        if (!CarscoutAlertBD.isPresent()) {

            LogService.logger.info("El alert_id no existe en BD..");
            MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0037.toString()).getDTO();
            listMessageDTO.add(messageUserNotExist);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());
            responseDTO.setListMessage(listMessageDTO);

            return responseDTO;

        }

        CarscoutAlert CarscoutAlertUserBD = carscoutAlertRepo.findByIdAndUser(alertId, userBD.get());

        if (CarscoutAlertUserBD == null) {

            LogService.logger.info("El alert_id no corresponde al user_id indicado..");
            MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0036.toString()).getDTO();
            listMessageDTO.add(messageUserNotExist);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());
            responseDTO.setListMessage(listMessageDTO);

            return responseDTO;

        }

        CarscoutAlertBD.get().setActiveInd(Constants.INACTIVE_DATA.intValue());
        CarscoutAlertBD.get().setCancelDate(new Timestamp(System.currentTimeMillis()));
        carscoutAlertRepo.save(CarscoutAlertBD.get());

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(new GenericDTO());

        return responseDTO;

    }

    /**
     * Metodo para obtener los resultados (autos) que coinciden con los filtros de carscout aplicados.
     *
     * @param alertId Identificador de alerta
     * @param page    Numerode pagina para realizar la paginacion
     * @param items   numero de elementos a mostrar en la pagina
     * @return ResponseDTO Generico de servicio
     * @author Oscar Montilla
     */
    public ResponseDTO getCarscoutResults(Long alertId, Long page, Long items) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();

        List<GetCarscoutfilterCarResponseDTO> getCarscoutfilterCarResponseDTOList = new ArrayList<>();

        if (alertId == null) {

            LogService.logger.info("El valor de alert_id es requerido por el servicio ..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (alertId == null ? "alert_id, " : ""));
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        if ((page == null && items != null) || (page != null && items == null)) {

            LogService.logger.info("Los parametros de page y items, estan mal ..");
            MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0054.toString()).getDTO();
            listMessageDTO.add(messageNoRecords);
            EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult2.getValue());
            responseDTO.setStatus(enumResult2.getStatus());
            responseDTO.setListMessage(listMessageDTO);

            return responseDTO;
        } else if (page != null && items != null) {
            if (page == 0 || items == 0) {

                LogService.logger.info("Los parametros de page y items, estan mal ..");
                MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0054.toString()).getDTO();
                listMessageDTO.add(messageNoRecords);
                EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult2.getValue());
                responseDTO.setStatus(enumResult2.getStatus());
                responseDTO.setListMessage(listMessageDTO);

                return responseDTO;
            }
        }

        List<CarscoutCatalogue> carscoutCatalogueListBD = new ArrayList<>();

        for (CarscoutAlertLkpSku carscoutAlertLkpSkuBD : carscoutAlertLkpSkuRepo.findByCarscoutAlertId(alertId)) {

            if (carscoutAlertLkpSkuBD.getCarscoutCatalogue() != null) {
                carscoutCatalogueListBD.add(carscoutAlertLkpSkuBD.getCarscoutCatalogue());
            }

        }

        if (carscoutCatalogueListBD.isEmpty()) {
            LogService.logger.info("Los datos de la Alerta es el registro viejo por carData ..");
            carscoutCatalogueListBD = carscoutCatalogueRepo.findByCarscoutAlertLkpSkuCarscoutAlertIdAndCarMetaAccesoriesCarscoutCategoryId(alertId, Constants.CATEGORY_YEAR, Constants.ACTIVE_DATA);
        }

        Long ini = 0L;
        if (page != null && items != null && page > 0 && items > 0) {
            ini = page * items;
            ini = ini - items;
            if (carscoutCatalogueListBD.size() >= (ini + items)) {
                carscoutCatalogueListBD = carscoutCatalogueListBD.subList(ini.intValue(), (ini.intValue() + items.intValue()));
            } else if (getCarscoutfilterCarResponseDTOList.size() > ini) {
                carscoutCatalogueListBD = carscoutCatalogueListBD.subList(ini.intValue(), carscoutCatalogueListBD.size());
            } else {

                LogService.logger.info("Se exedio de paginas ..");
                MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0053.toString()).getDTO();
                listMessageDTO.add(messageNoRecords);
                EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult2.getValue());
                responseDTO.setStatus(enumResult2.getStatus());
                responseDTO.setListMessage(listMessageDTO);

                return responseDTO;
            }
        }

        if (carscoutCatalogueListBD.size() != 0) {

            Optional<CarscoutAlert> carscoutAlertBD = carscoutAlertRepo.findById(alertId);

            for (CarscoutCatalogue carscoutCatalogueBD : carscoutCatalogueListBD) {

                CarscoutNotify carscoutNotifyBD = carscoutNotifyRepo.findByCarscoutAlertIdAndCarscoutCatalogueId(alertId, carscoutCatalogueBD.getId());

                if (carscoutNotifyBD == null) {
                    CarscoutNotify carscoutNotifyNew = new CarscoutNotify();
                    carscoutNotifyNew.setCarscoutAlert(carscoutAlertBD.get());
                    carscoutNotifyNew.setCarscoutCatalogue(carscoutCatalogueBD);
                    carscoutNotifyNew.setStartShowDate(new Timestamp(System.currentTimeMillis()));
                    carscoutNotifyNew.setShowCount(1);
                    carscoutNotifyNew.setInterestedInd(Constants.NO_INTERESTED_CAR);
                    CarscoutNotify carscoutNotifyNewBD = carscoutNotifyRepo.save(carscoutNotifyNew);

                    GetCarscoutfilterCarResponseDTO getCarscoutfilterCarResponseDTO = getEstrucutreResults(carscoutCatalogueBD, alertId);
                    getCarscoutfilterCarResponseDTO.setIdCarscoutNotify(carscoutNotifyNewBD.getId());
                    getCarscoutfilterCarResponseDTOList.add(getCarscoutfilterCarResponseDTO);

                } else if (carscoutNotifyBD != null && carscoutNotifyBD.getInterestedInd() != Constants.INTERESTED_CAR) {

                    carscoutNotifyBD.setShowCount(carscoutNotifyBD.getShowCount() + 1);
                    carscoutNotifyRepo.save(carscoutNotifyBD);

                    GetCarscoutfilterCarResponseDTO getCarscoutfilterCarResponseDTO = getEstrucutreResults(carscoutCatalogueBD, alertId);
                    getCarscoutfilterCarResponseDTO.setIdCarscoutNotify(carscoutNotifyBD.getId());
                    getCarscoutfilterCarResponseDTOList.add(getCarscoutfilterCarResponseDTO);
                }
            }
        }

        Collections.sort(getCarscoutfilterCarResponseDTOList);
        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(getCarscoutfilterCarResponseDTOList);

        return responseDTO;
    }

    /**
     * Metodo que arma la estructura de los resultados de Carscout
     *
     * @param carscoutCatalogueBD Objeto de Carscout Catalogue
     * @param alertId             Identificador de alerta
     * @return GetCarscoutfilterCarResponseDTO estructura del Respose de Resultados de Carscout
     * @author Oscar Montilla
     */

    public GetCarscoutfilterCarResponseDTO getEstrucutreResults(CarscoutCatalogue carscoutCatalogueBD, Long alertId) {

        GetCarscoutfilterCarResponseDTO getCarscoutfilterCarResponseDTO = new GetCarscoutfilterCarResponseDTO();

        getCarscoutfilterCarResponseDTO.setAlertId(alertId);
        getCarscoutfilterCarResponseDTO.setCatalogueId(carscoutCatalogueBD.getId());
        getCarscoutfilterCarResponseDTO.setTitle(carscoutCatalogueBD.getCarMake().getName() + "  " + carscoutCatalogueBD.getCarModel().getDescription() + " " + carscoutCatalogueBD.getCarVersion().getDescription());
        getCarscoutfilterCarResponseDTO.setYear(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.getYearId()).get().getDescription());
        getCarscoutfilterCarResponseDTO.setStyle(carscoutCatalogueBD.getCarBodyType().getName());
        getCarscoutfilterCarResponseDTO.setStyleIcon(Constants.URL_KAVAK + carscoutCatalogueBD.getCarBodyType().getImage());
        getCarscoutfilterCarResponseDTO.setKm(carscoutCatalogueBD.getKm());
        getCarscoutfilterCarResponseDTO.setSalePrice(KavakUtils.getConvertSalePrice(carscoutCatalogueBD.getSalePrice()));
        getCarscoutfilterCarResponseDTO.setPriorityId(carscoutCatalogueBD.getPriorityId());
        getCarscoutfilterCarResponseDTO.setCarStatus(carscoutCatalogueBD.getCarStatus());

        if (carscoutCatalogueBD.getCarStatus() != null && carscoutCatalogueBD.getProbability() != null) {
            getCarscoutfilterCarResponseDTO.setCarStatus(carscoutCatalogueBD.getCarStatus());
            getCarscoutfilterCarResponseDTO.setProbability(carscoutCatalogueBD.getProbability() + "%");
        } else {
            LogService.logger.debug("El registro de BD no posee probabilidad..");
            getCarscoutfilterCarResponseDTO.setCarStatus("Inspeccion agendada");
            getCarscoutfilterCarResponseDTO.setProbability("20%");
        }

        if (carscoutCatalogueBD.getColorId() != null && carscoutCatalogueBD.getColorId() != 0) {
            LogService.logger.debug("Posee data de color..");

            Optional<CarColor> carColorBD = carColorRepo.findById(carscoutCatalogueBD.getColorId());

            if (carColorBD.isPresent()) {
                LogService.logger.debug("Existe la data del color..");

                getCarscoutfilterCarResponseDTO.setColorHexa(carColorBD.get().getHexadecimal() == null ? null : carColorBD.get().getHexadecimal());
                getCarscoutfilterCarResponseDTO.setColorName(carColorBD.get().getName());
            }
        }

        if (carscoutCatalogueBD.getCarData() != null) {

            if (carscoutCatalogueBD.getCarData().getMakeId() != null && carscoutCatalogueBD.getCarData().getMakeId() != 0) {
                LogService.logger.debug("Posee data de make..");
                getCarscoutfilterCarResponseDTO.setMake(carMakeRepo.getOne(carscoutCatalogueBD.getCarData().getMakeId()).getName());
            }

            if (carscoutCatalogueBD.getCarData().getSeatsId() != null && carscoutCatalogueBD.getCarData().getSeatsId() != 0) {
                LogService.logger.debug("Posee data de seats..");
                getCarscoutfilterCarResponseDTO.setSeats(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.getCarData().getSeatsId()).get().getDescription());
            }

            if (carscoutCatalogueBD.getCarData().getDoorsId() != null && carscoutCatalogueBD.getCarData().getDoorsId() != 0) {
                LogService.logger.debug("Posee data de doors..");
                getCarscoutfilterCarResponseDTO.setDoors(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.getCarData().getDoorsId()).get().getDescription());
            }

            if (carscoutCatalogueBD.getCarData().getCylindroId() != null && carscoutCatalogueBD.getCarData().getCylindroId() != 0) {
                LogService.logger.debug("Posee data de cylindro..");
                getCarscoutfilterCarResponseDTO.setCylinders(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.getCarData().getCylindroId()).get().getDescription());
            }

            if (carscoutCatalogueBD.getCarData().getTraccionId() != null && carscoutCatalogueBD.getCarData().getTraccionId() != 0) {
                LogService.logger.debug("Posee data de traccion..");
                getCarscoutfilterCarResponseDTO.setTraction(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.getCarData().getTraccionId()).get().getDescription());
            }

            if (carscoutCatalogueBD.getCarData().getFuelId() != null && carscoutCatalogueBD.getCarData().getFuelId() != 0) {
                LogService.logger.debug("Posee data de fuelType..");
                getCarscoutfilterCarResponseDTO.setFuelType(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.getCarData().getFuelId()).get().getDescription());
            }

            if (carscoutCatalogueBD.getCarData().getTransmissionid() != null && carscoutCatalogueBD.getCarData().getTransmissionid() != 0) {
                LogService.logger.debug("Posee data de transmission..");
                getCarscoutfilterCarResponseDTO.setTransmission(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.getCarData().getTransmissionid()).get().getDescription());
            }
        }

        if (carscoutCatalogueBD.getStockId() != null) {

            Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(carscoutCatalogueBD.getStockId());

            if (sellCarDetailBD.isPresent()) {
                LogService.logger.debug("Posee data de Stock_id, se devuelve la url..");
                getCarscoutfilterCarResponseDTO.setStockId(carscoutCatalogueBD.getStockId());
                getCarscoutfilterCarResponseDTO.setUrlStock(KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get()));
            }
        }
        return getCarscoutfilterCarResponseDTO;
    }

    /**
     * Metodo para Consultar las notificaciones de un usuario
     *
     * @param userId Identificador de Usuario
     * @return ResponseDTO Generico de servicio
     * @author Oscar Montilla
     */

    public ResponseDTO getCarscoutNotify(Long userId) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();

        if (userId == null) {

            LogService.logger.info("El valor de user_id es requerido por el servicio ..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (userId == null ? "user_id, " : ""));
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        Optional<User> userBD = userRepo.findById(userId);

        if (!userBD.isPresent()) {

            LogService.logger.info("El usuario no existe en Base de datos..");
            MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
            listMessageDTO.add(messageUserNotExist);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        List<CarscoutNotify> listcarscoutNotifyBD = carscoutNotifyRepo.findByCarscoutAlertUserIdAndInterestedInd(userId, Constants.INTERESTED_CAR);

        List<GetCarscoutNotifyResponseDTO> getCarscoutNotifyResponseDTOList = new ArrayList<>();

        if (listcarscoutNotifyBD != null) {
            LogService.logger.debug("El usuario tiene notificaciones de Interes..");
            for (CarscoutNotify carscoutNotifyBD : listcarscoutNotifyBD) {

                Optional<CarscoutCatalogue> carscoutCatalogueBD = carscoutCatalogueRepo.findById(carscoutNotifyBD.getCarscoutCatalogue().getId());

                if (carscoutCatalogueBD.isPresent()) {

                    GetCarscoutNotifyResponseDTO getCarscoutNotifyResponseDTO = new GetCarscoutNotifyResponseDTO();
                    getCarscoutNotifyResponseDTO.setIdCarscoutNotify(carscoutNotifyBD.getId());
                    getCarscoutNotifyResponseDTO.setTitle(carscoutCatalogueBD.get().getCarMake().getName() + "  " + carscoutCatalogueBD.get().getCarModel().getDescription() + "  " + carscoutCatalogueBD.get().getCarVersion().getDescription());
                    getCarscoutNotifyResponseDTO.setYear(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.get().getYearId()).get().getDescription());
                    getCarscoutNotifyResponseDTO.setStyle(carscoutCatalogueBD.get().getCarBodyType().getName());
                    getCarscoutNotifyResponseDTO.setStyleIcon(Constants.URL_KAVAK + carscoutCatalogueBD.get().getCarBodyType().getImage());
                    getCarscoutNotifyResponseDTO.setKm(carscoutCatalogueBD.get().getKm());
                    getCarscoutNotifyResponseDTO.setSalePrice(KavakUtils.getConvertSalePrice(carscoutCatalogueBD.get().getSalePrice()));
                    getCarscoutNotifyResponseDTO.setCarStatus(carscoutCatalogueBD.get().getCarStatus());
                    getCarscoutNotifyResponseDTO.setPriorityId(carscoutCatalogueBD.get().getPriorityId());

                    if (carscoutCatalogueBD.get().getCarStatus() != null && carscoutCatalogueBD.get().getProbability() != null) {
                        getCarscoutNotifyResponseDTO.setCarStatus(carscoutCatalogueBD.get().getCarStatus());
                        getCarscoutNotifyResponseDTO.setProbability(carscoutCatalogueBD.get().getProbability() + "%");
                    } else {
                        LogService.logger.debug("El registro de BD no posee probabilidad..");
                        getCarscoutNotifyResponseDTO.setCarStatus("Inspeccion agendada");
                        getCarscoutNotifyResponseDTO.setProbability("20%");
                    }

                    if (carscoutCatalogueBD.get().getColorId() != null && carscoutCatalogueBD.get().getColorId() != 0) {
                        LogService.logger.debug("Posee data de color..");

                        Optional<CarColor> carColorBD = carColorRepo.findById(carscoutCatalogueBD.get().getColorId());

                        if (carColorBD.isPresent()) {
                            LogService.logger.debug("Existe la data del color..");

                            getCarscoutNotifyResponseDTO.setColorHexa(carColorBD.get().getHexadecimal() == null ? null : carColorBD.get().getHexadecimal());
                            getCarscoutNotifyResponseDTO.setColorName(carColorBD.get().getName());
                        }
                    }

                    if (carscoutCatalogueBD.get().getCarData().getMakeId() != null && carscoutCatalogueBD.get().getCarData().getMakeId() != 0) {
                        LogService.logger.debug("Posee data de make..");
                        getCarscoutNotifyResponseDTO.setMake(carMakeRepo.getOne(carscoutCatalogueBD.get().getCarData().getMakeId()).getName());
                    }

                    if (carscoutCatalogueBD.get().getCarData().getSeatsId() != null && carscoutCatalogueBD.get().getCarData().getSeatsId() != 0) {
                        LogService.logger.debug("Posee data de seats..");
                        getCarscoutNotifyResponseDTO.setSeats(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.get().getCarData().getSeatsId()).get().getDescription());

                    }

                    if (carscoutCatalogueBD.get().getCarData().getDoorsId() != null && carscoutCatalogueBD.get().getCarData().getDoorsId() != 0) {
                        LogService.logger.debug("Posee data de doors..");
                        getCarscoutNotifyResponseDTO.setDoors(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.get().getCarData().getDoorsId()).get().getDescription());

                    }

                    if (carscoutCatalogueBD.get().getCarData().getCylindroId() != null && carscoutCatalogueBD.get().getCarData().getCylindroId() != 0) {
                        LogService.logger.debug("Posee data de cylindro..");
                        getCarscoutNotifyResponseDTO.setCylinders(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.get().getCarData().getCylindroId()).get().getDescription());

                    }

                    if (carscoutCatalogueBD.get().getCarData().getTraccionId() != null && carscoutCatalogueBD.get().getCarData().getTraccionId() != 0) {
                        LogService.logger.debug("Posee data de traccion..");
                        getCarscoutNotifyResponseDTO.setTraction(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.get().getCarData().getTraccionId()).get().getDescription());

                    }

                    if (carscoutCatalogueBD.get().getCarData().getFuelId() != null && carscoutCatalogueBD.get().getCarData().getFuelId() != 0) {
                        LogService.logger.debug("Posee data de fuelType..");
                        getCarscoutNotifyResponseDTO.setFuelType(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.get().getCarData().getFuelId()).get().getDescription());

                    }

                    if (carscoutCatalogueBD.get().getCarData().getTransmissionid() != null && carscoutCatalogueBD.get().getCarData().getTransmissionid() != 0) {
                        LogService.logger.debug("Posee data de transmission..");
                        getCarscoutNotifyResponseDTO.setTransmission(carMetaAccesoriesrepo.findById(carscoutCatalogueBD.get().getCarData().getTransmissionid()).get().getDescription());

                    }

                    if (carscoutCatalogueBD.get().getStockId() != null) {

                        Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(carscoutCatalogueBD.get().getStockId());

                        if (sellCarDetailBD.isPresent()) {
                            LogService.logger.debug("Posee data de Stock_id, se devuelve la url..");
                            getCarscoutNotifyResponseDTO.setStockId(carscoutCatalogueBD.get().getStockId());
                            getCarscoutNotifyResponseDTO.setUrlStock(KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get()));
                        }
                    }

                    getCarscoutNotifyResponseDTOList.add(getCarscoutNotifyResponseDTO);

                } else {

                    LogService.logger.info("Existen inconsistencias con la data de Catalogo..");
                }
            }

        }
        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(getCarscoutNotifyResponseDTOList);

        return responseDTO;

    }

    /**
     * Metodo que actualiza una notificacion cuando el usuario marca "Me Interesa"
     *
     * @param alertId     Identificador de CarscoutAlert
     * @param catalogueId Identificador de CarscoutCatalogue
     * @return ResponseDTO Generico de servicio
     * @author Oscar Montilla
     */

    public ResponseDTO putCarscoutNotify(Long alertId, Long catalogueId) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();

        Optional<CarscoutAlert> carscoutAlertBD = carscoutAlertRepo.findById(alertId);
        Optional<CarscoutCatalogue> carscoutCatalogueBD = carscoutCatalogueRepo.findById(catalogueId);

        if (alertId == null || catalogueId == null) {

            LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (alertId == null ? "alert_id, " : "") + (catalogueId == null ? "catalogue_id, " : ""));
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        if (!carscoutAlertBD.isPresent()) {
            LogService.logger.info("El alert_id no existe en BD..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0037.toString()).getDTO();
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);

        }

        if (!carscoutCatalogueBD.isPresent()) {
            LogService.logger.info("El catalogue_id no existe en BD..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0040.toString()).getDTO();
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);

        }

        if (responseDTO.getListMessage() != null) {
            LogService.logger.info("Existe una lista de Menssaje de validacion, devolver los mensajes..");
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        CarscoutNotify carscoutNotifyBD = carscoutNotifyRepo.findByCarscoutAlertIdAndCarscoutCatalogueId(alertId, catalogueId);

        if (carscoutNotifyBD == null) {
            LogService.logger.info("No existe un registro en v2_carscout_notify para el alert_id y catalogue_id recibidos..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0046.toString()).getDTO();
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;
        }

        carscoutNotifyBD.setInteresteDate(new Timestamp(System.currentTimeMillis()));
        carscoutNotifyBD.setInterestedInd(Constants.INTERESTED_CAR);

        GenericDTO genericDTO = new GenericDTO();

        genericDTO.setAlertId(carscoutNotifyBD.getCarscoutAlert().getId());

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(genericDTO);
        return responseDTO;
    }

    /**
     * Metodo que actualiza una notificacion cuando el usuario marca " No Me Interesa"
     *
     * @param userId           Identificador de Usuario
     * @param idCarscoutNotify Identificador de Notificacion de Carscout
     * @return ResponseDTO Generico de servicio
     * @author Oscar Montilla
     */
    public ResponseDTO deleteCarscoutNotify(Long userId, Long idCarscoutNotify) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();

        Optional<User> userBD = userRepo.findById(userId);

        if (userId == null || idCarscoutNotify == null) {

            LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (userId == null ? "user_id, " : "") + (idCarscoutNotify == null ? "id_carscout_notify, " : ""));
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        if (!userBD.isPresent()) {

            LogService.logger.info("El usuario no existe en Base de datos..");
            MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0016.toString()).getDTO();
            listMessageDTO.add(messageUserNotExist);
            responseDTO.setListMessage(listMessageDTO);
        }

        CarscoutNotify carscoutNotifyBD = carscoutNotifyRepo.findByCarscoutAlertUserIdAndId(userId, idCarscoutNotify);

        if (carscoutNotifyBD == null) {

            LogService.logger.info("El usuario no existe en Base de datos..");
            MessageDTO messageUserNotExist = messageRepo.findByCode(MessageEnum.M0045.toString()).getDTO();
            listMessageDTO.add(messageUserNotExist);
            responseDTO.setListMessage(listMessageDTO);
        }

        if (responseDTO.getListMessage() != null) {
            LogService.logger.info("Existe una lista de Menssaje de validacion, devolver los mensajes..");
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        carscoutNotifyBD.setInterestedInd(Constants.NO_INTERESTED_CAR);

        GenericDTO genericDTO = new GenericDTO();

        genericDTO.setAlertId(carscoutNotifyBD.getCarscoutAlert().getId());

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(genericDTO);
        return responseDTO;
    }

    /**
     * Metodo que obtiene las alertas registradas de carscout
     *
     * @param userId id de usuario
     * @return ResponseDTO Generico de servicio
     * @author Oscar Montilla
     */
    public ResponseDTO getCarscoutAlert(Long userId, Long alertId) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();

        String patternNumber = "$###,###,###";
        String patternNumberKM = "###,###,###";

        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        DecimalFormat decimalFormat = (DecimalFormat) nf;
        DecimalFormat decimalFormatKM = (DecimalFormat) nf;
        decimalFormat.applyPattern(patternNumber);
        decimalFormatKM.applyPattern(patternNumberKM);

        if (userId == null) {

            LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
            MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
            listMessageDTO.add(messageDataNull);
            responseDTO.setListMessage(listMessageDTO);
            EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            responseDTO.setCode(enumResult.getValue());
            responseDTO.setStatus(enumResult.getStatus());
            responseDTO.setData(new GenericDTO());

            return responseDTO;

        }

        List<GetCarscoutResultsResponseDTO> getCarscoutResultList = new ArrayList<>();
        List<CarscoutAlert> listCarscoutAlertBD = new ArrayList<CarscoutAlert>();

        if (alertId != null) {
            listCarscoutAlertBD = carscoutAlertRepo.findByUserIdAndActiveIndAndId(userId, Constants.ACTIVE_DATA.intValue(), alertId);
        } else {
            listCarscoutAlertBD = carscoutAlertRepo.findByUserIdAndActiveInd(userId, Constants.ACTIVE_DATA.intValue());
        }

        for (CarscoutAlert carscoutAlert : listCarscoutAlertBD) {

            GetCarscoutResultsResponseDTO getCarscoutResults = new GetCarscoutResultsResponseDTO();

            getCarscoutResults.setAlertId(carscoutAlert.getId());

            String groupBodyYpe = null;
            String groupMake = null;
            String groupModel = null;
            String groupVersion = null;
            String groupColor = null;
            String groupYear = null;
            String groupDoors = null;
            String groupSeats = null;
            String groupCylinders = null;
            String groupTraction = null;
            String groupFuelType = null;
            String groupTrasmission = null;
            List<GenericDTO> filters = new ArrayList<>();
            for (CarscoutAlertDetail carscoutAlertDetail : carscoutAlert.getCarscoutAlertDetail()) {

                GenericDTO filter = new GenericDTO();
                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_PRICE)) {
                    filter.setCategoryId(carscoutAlertDetail.getCarscoutCategory().getId());
                    filter.setDescription("Precio: " + "$" + decimalFormat.format(carscoutAlertDetail.getParamId1()) + " - " + "$" + decimalFormat.format(carscoutAlertDetail.getParamId2()));
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_KM)) {

                    filter.setCategoryId(carscoutAlertDetail.getCarscoutCategory().getId());
                    if (carscoutAlertDetail.getParamId2() == null) {
                        filter.setDescription("Kilometraje: " + decimalFormatKM.format(carscoutAlertDetail.getParamId1()) + " Km");
                    } else {
                        filter.setDescription("Kilometraje: " + decimalFormatKM.format(carscoutAlertDetail.getParamId2()) + " Km" + " - " + decimalFormatKM.format(carscoutAlertDetail.getParamId1()) + " Km");
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_BODY_TYPE)) {
                    Optional<CarBodyType> carBodyType = carBodyTypeRepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String bodyTypeName;

                    if (carBodyType.isPresent()) {
                        bodyTypeName = carBodyType.get().getName();
                    } else {
                        bodyTypeName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupBodyYpe == null) {
                        groupBodyYpe = bodyTypeName;
                    } else {
                        groupBodyYpe += (" | " + bodyTypeName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_MAKE)) {
                    Optional<CarMake> carMake = carMakeRepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String makeName;

                    if (carMake.isPresent()) {
                        makeName = carMake.get().getName();
                    } else {
                        makeName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupMake == null) {
                        groupMake = makeName;
                    } else {
                        groupMake += (" | " + makeName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_MODEL)) {
                    Optional<CarModel> carModel = carModelRepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String modelName;

                    if (carModel.isPresent()) {
                        modelName = carModel.get().getDescription() + " " + carModel.get().getCarMake().getName();
                    } else {
                        modelName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupModel == null) {
                        groupModel = modelName;
                    } else {
                        groupModel += (" | " + modelName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_VERSION)) {
                    Optional<CarVersion> carVersion = carVersionRepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String versionName;

                    if (carVersion.isPresent()) {
                        versionName = carVersion.get().getDescription();
                    } else {
                        versionName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupVersion == null) {
                        groupVersion = versionName;
                    } else {
                        groupVersion += (" | " + versionName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_COLOR)) {
                    Optional<CarColor> carColor = carColorRepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String colorName;

                    if (carColor.isPresent()) {
                        colorName = carColor.get().getName();
                    } else {
                        colorName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupColor == null) {
                        groupColor = colorName;
                    } else {
                        groupColor += (" | " + colorName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_YEAR)) {
                    Optional<CarMetaAccesories> carMetaAccesories = carMetaAccesoriesrepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String metaAccesoriesName;

                    if (carMetaAccesories.isPresent()) {
                        metaAccesoriesName = carMetaAccesories.get().getDescription();
                    } else {
                        metaAccesoriesName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupYear == null) {
                        groupYear = metaAccesoriesName;
                    } else {
                        groupYear += (" | " + metaAccesoriesName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_DOORS)) {
                    Optional<CarMetaAccesories> carMetaAccesories = carMetaAccesoriesrepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String metaAccesoriesName;

                    if (carMetaAccesories.isPresent()) {
                        metaAccesoriesName = carMetaAccesories.get().getDescription();
                    } else {
                        metaAccesoriesName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupDoors == null) {
                        groupDoors = metaAccesoriesName;
                    } else {
                        groupDoors += (" | " + metaAccesoriesName).toString();
                    }
                }
                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_SEATS)) {
                    Optional<CarMetaAccesories> carMetaAccesories = carMetaAccesoriesrepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String metaAccesoriesName;

                    if (carMetaAccesories.isPresent()) {
                        metaAccesoriesName = carMetaAccesories.get().getDescription();
                    } else {
                        metaAccesoriesName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupSeats == null) {
                        groupSeats = metaAccesoriesName;
                    } else {
                        groupSeats += (" | " + metaAccesoriesName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_CYLINDERS)) {
                    Optional<CarMetaAccesories> carMetaAccesories = carMetaAccesoriesrepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String metaAccesoriesName;

                    if (carMetaAccesories.isPresent()) {
                        metaAccesoriesName = carMetaAccesories.get().getDescription();
                    } else {
                        metaAccesoriesName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupCylinders == null) {
                        groupCylinders = metaAccesoriesName;
                    } else {
                        groupCylinders += (" | " + metaAccesoriesName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_TRACTION)) {
                    Optional<CarMetaAccesories> carMetaAccesories = carMetaAccesoriesrepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String metaAccesoriesName;

                    if (carMetaAccesories.isPresent()) {
                        metaAccesoriesName = carMetaAccesories.get().getDescription();
                    } else {
                        metaAccesoriesName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupTraction == null) {
                        groupTraction = metaAccesoriesName;
                    } else {
                        groupTraction += (" | " + metaAccesoriesName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_FUELTYPE)) {
                    Optional<CarMetaAccesories> carMetaAccesories = carMetaAccesoriesrepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String metaAccesoriesName;

                    if (carMetaAccesories.isPresent()) {
                        metaAccesoriesName = carMetaAccesories.get().getDescription();
                    } else {
                        metaAccesoriesName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupFuelType == null) {
                        groupFuelType = metaAccesoriesName;
                    } else {
                        groupFuelType += (" | " + metaAccesoriesName).toString();
                    }
                }

                if (carscoutAlertDetail.getCarscoutCategory().getId().equals(Constants.CATEGORY_TRASMISSIONS)) {
                    Optional<CarMetaAccesories> carMetaAccesories = carMetaAccesoriesrepo.findById(carscoutAlertDetail.getParamId1().longValue());
                    String metaAccesoriesName;

                    if (carMetaAccesories.isPresent()) {
                        metaAccesoriesName = carMetaAccesories.get().getDescription();
                    } else {
                        metaAccesoriesName = Constants.NOT_FOUND + carscoutAlertDetail.getParamId1();
                    }
                    if (groupTrasmission == null) {
                        groupTrasmission = metaAccesoriesName;
                    } else {
                        groupTrasmission += (" | " + metaAccesoriesName).toString();
                    }
                }

                if (filter.getCategoryId() != null) {
                    filters.add(filter);
                }

            }

            if (groupBodyYpe != null) {
                LogService.logger.debug("Se recupera un valor de BodyYpe");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_BODY_TYPE);
                filterFinal.setDescription("Estilo: " + groupBodyYpe);
                filters.add(filterFinal);
            }
            if (groupMake != null) {
                LogService.logger.debug("Se recupera un valor de Make");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_MAKE);
                filterFinal.setDescription("Marca: " + groupMake);
                filters.add(filterFinal);
            }
            if (groupModel != null) {
                LogService.logger.debug("Se recupera un valor de Model");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_MODEL);
                filterFinal.setDescription("Modelo: " + groupModel);
                filters.add(filterFinal);
            }

            if (groupVersion != null) {
                LogService.logger.debug("Se recupera un valor de Version");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_VERSION);
                filterFinal.setDescription("Version: " + groupVersion);
                filters.add(filterFinal);
            }
            if (groupColor != null) {
                LogService.logger.debug("Se recupera un valor de Color");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_COLOR);
                filterFinal.setDescription("Color: " + groupColor);
                filters.add(filterFinal);
            }
            if (groupYear != null) {
                LogService.logger.debug("Se recupera un valor de Year");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_YEAR);
                filterFinal.setDescription("Año: " + groupYear);
                filters.add(filterFinal);
            }
            if (groupDoors != null) {
                LogService.logger.debug("Se recupera un valor de Door");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_DOORS);
                filterFinal.setDescription("Puertas: " + groupDoors);
                filters.add(filterFinal);
            }
            if (groupCylinders != null) {
                LogService.logger.debug("Se recupera un valor de Cylinders");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_CYLINDERS);
                filterFinal.setDescription("Cilindros: " + groupCylinders);
                filters.add(filterFinal);
            }

            if (groupTraction != null) {
                LogService.logger.debug("Se recupera un valor de Traction");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_TRACTION);
                filterFinal.setDescription("Tracción: " + groupTraction);
                filters.add(filterFinal);
            }

            if (groupFuelType != null) {
                LogService.logger.debug("Se recupera un valor de FuelType");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_FUELTYPE);
                filterFinal.setDescription("Combustible: " + groupFuelType);
                filters.add(filterFinal);
            }

            if (groupSeats != null) {
                LogService.logger.debug("Se recupera un valor de Seats");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_SEATS);
                filterFinal.setDescription("Asientos: " + groupSeats);
                filters.add(filterFinal);
            }

            if (groupTrasmission != null) {
                LogService.logger.debug("Se recupera un valor de Trasmission");
                GenericDTO filterFinal = new GenericDTO();
                filterFinal.setCategoryId(Constants.CATEGORY_TRASMISSIONS);
                filterFinal.setDescription("Transmisión: " + groupTrasmission);
                filters.add(filterFinal);
            }

            Collections.sort(filters);
            getCarscoutResults.setFilters(filters);
            getCarscoutResultList.add(getCarscoutResults);

        }

        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(getCarscoutResultList);

        return responseDTO;
    }

    /**
     * Servicio: SERVICIO PARA OBTENER PREGUNTAS DE CARSCOUT
     *
     * @author Juan Tolentino
     **/
    public ResponseDTO getCarscoutQuestions(String multiple) {
        ResponseDTO responseDTO = new ResponseDTO();
        List<CarscoutQuestionsDTO> listCarscoutQuestionsDTO = new ArrayList<>();
        CarscoutQuestionsResponseDTO carscoutQuestionsResponseDTO = new CarscoutQuestionsResponseDTO();
        CarscoutQuestionsDTO carscoutQuestionsDTO = new CarscoutQuestionsDTO();
        List<CarscoutQuestionOptions> listCarscoutQuestionOptions = new ArrayList<>();
        List<GenericDTO> listOptionsDTO = new ArrayList<>();
        List<CarscoutQuestion> listCarscoutQuestionBD = new ArrayList<>();
        try {
            if (multiple.trim().toLowerCase().equals("false")) {
                listCarscoutQuestionBD = carscoutQuestionRepo.findByActiveTrueAndMultipleFalse();
            } else if (multiple.trim().toLowerCase().equals("true")) {
                listCarscoutQuestionBD = carscoutQuestionRepo.findByActiveTrueAndMultipleTrue();
            } else {
                LogService.logger.info("El parametro multiple debe tener como valor true/false");
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData(new GenericDTO());

                return responseDTO;
            }
            for (CarscoutQuestion carscoutQuestionActual : listCarscoutQuestionBD) {
                carscoutQuestionsDTO = new CarscoutQuestionsDTO();
                carscoutQuestionsDTO.setId(carscoutQuestionActual.getId());
                carscoutQuestionsDTO.setTitle(carscoutQuestionActual.getQuestion());
                listCarscoutQuestionOptions = carscoutQuestionOptionRepo.findByQuestionId(carscoutQuestionActual.getId());
                listOptionsDTO = new ArrayList<>();
                for (CarscoutQuestionOptions carscoutQuestionOptionsActual : listCarscoutQuestionOptions) {
                    // llenando el objeto options
                    GenericDTO optionDTO = new GenericDTO();
                    optionDTO.setId(carscoutQuestionOptionsActual.getId());
                    optionDTO.setName(carscoutQuestionOptionsActual.getOption());
                    listOptionsDTO.add(optionDTO);
                }
                carscoutQuestionsDTO.setOptions(listOptionsDTO);
                listCarscoutQuestionsDTO.add(carscoutQuestionsDTO);
            }
            carscoutQuestionsResponseDTO.setListCarscoutQuestionDTO(listCarscoutQuestionsDTO);
        } catch (Exception e) {
            LogService.logger.error("Error Catch de getCarscoutQuestions : " + e);
        }
        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        responseDTO.setData(carscoutQuestionsResponseDTO);
        return responseDTO;
    }

    /**
     * Servicio: SERVICIO PARA RESPUESTAS DE PREGUNTAS DE CARSCOUT
     *
     * @author Juan Tolentino
     **/
    public ResponseDTO postCarscoutQuestionAnswers(PostCarscoutQuestionsAnswersRequestDTO postCarscoutQuestionsAnswers) {
        ResponseDTO responseDTO = new ResponseDTO();
        List<MessageDTO> listMessageDTO = new ArrayList<>();
        Timestamp actualTime = new Timestamp(System.currentTimeMillis());

        try {
            if (postCarscoutQuestionsAnswers.getUserId() == null) {
                LogService.logger.info("Error los parametros del Json estan null o incompletos");
                MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + (postCarscoutQuestionsAnswers.getUserId() == null ? "user_id, " : ""));
                listMessageDTO.add(messageDataNull);
                responseDTO.setListMessage(listMessageDTO);

                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData(new GenericDTO());

                return responseDTO;
            }

            if ((postCarscoutQuestionsAnswers.getCatalogueId() == null && postCarscoutQuestionsAnswers.getAlertId() == null) ||
                    (postCarscoutQuestionsAnswers.getCatalogueId() != null && postCarscoutQuestionsAnswers.getAlertId() != null)) {

                LogService.logger.info("El servicio debe recibir alert_id o catalogue_id");
                MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString()).getRequestDTO(" " + ("alert_id or catalogue_id, "));
                listMessageDTO.add(messageDataNull);
                responseDTO.setListMessage(listMessageDTO);

                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData(new GenericDTO());

                return responseDTO;

            }

            Optional<User> userBD = userRepo.findById(postCarscoutQuestionsAnswers.getUserId());
            if (!userBD.isPresent()) {
                LogService.logger.info("Esta recibiendo el usuario no valido...");
                MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
                listMessageDTO.add(messageDataNull);
                responseDTO.setListMessage(listMessageDTO);

                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData(new GenericDTO());

                return responseDTO;
            }

            if (postCarscoutQuestionsAnswers.getAlertId() != null) {

                Optional<CarscoutAlert> carscoutAlertBD = carscoutAlertRepo.findById(postCarscoutQuestionsAnswers.getAlertId());
                if (!carscoutAlertBD.isPresent()) {
                    LogService.logger.info("Esta recibiendo el alert_id no valido...");
                    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0037.toString()).getDTO();
                    listMessageDTO.add(messageDataNull);
                    responseDTO.setListMessage(listMessageDTO);

                    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                    responseDTO.setCode(enumResult.getValue());
                    responseDTO.setStatus(enumResult.getStatus());
                    responseDTO.setData(new GenericDTO());

                    return responseDTO;
                }

                LogService.logger.info("Registro de pregunta y respuesta para una alerta...");

                if (!postCarscoutQuestionsAnswers.getAnswers().isEmpty()) {
                    for (AnswerDTO answersActual : postCarscoutQuestionsAnswers.getAnswers()) {
                        CarscoutQuestionAnswer carscoutQuestionAnswer = carscoutQuestionAnswerRepo.findQuestionAndAnswer(postCarscoutQuestionsAnswers.getUserId(), postCarscoutQuestionsAnswers.getAlertId(), answersActual.getQuestionId());
                        if (carscoutQuestionAnswer != null) {
                            carscoutQuestionAnswer.setQuestionId(answersActual.getQuestionId());
                            carscoutQuestionAnswer.setOptionId(answersActual.getOptionId());
                            carscoutQuestionAnswer.setUpdateDate(actualTime);
                            carscoutQuestionAnswerRepo.save(carscoutQuestionAnswer);
                        } else {
                            CarscoutQuestionAnswer newCarscoutQuestionAnswer = new CarscoutQuestionAnswer();
                            newCarscoutQuestionAnswer.setUserId(postCarscoutQuestionsAnswers.getUserId());
                            newCarscoutQuestionAnswer.setAlertId(postCarscoutQuestionsAnswers.getAlertId());
                            newCarscoutQuestionAnswer.setQuestionId(answersActual.getQuestionId());
                            newCarscoutQuestionAnswer.setOptionId(answersActual.getOptionId());
                            newCarscoutQuestionAnswer.setUpdateDate(actualTime);
                            carscoutQuestionAnswerRepo.save(newCarscoutQuestionAnswer);
                        }
                    }
                }
            }

            if (postCarscoutQuestionsAnswers.getCatalogueId() != null) {

                CarscoutCatalogue carscoutCatalogueBD = carscoutCatalogueRepo.findByIdAndActiveInd(postCarscoutQuestionsAnswers.getCatalogueId(), Constants.ACTIVE_DATA);
                if (carscoutCatalogueBD == null) {

                    LogService.logger.info("Esta recibiendo el catalogue_id no valido o inactivo...");
                    MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0034.toString()).getRemplaceParameterDTO("catalogue_id");
                    listMessageDTO.add(messageDataNull);
                    responseDTO.setListMessage(listMessageDTO);

                    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                    responseDTO.setCode(enumResult.getValue());
                    responseDTO.setStatus(enumResult.getStatus());
                    responseDTO.setData(new GenericDTO());

                    return responseDTO;
                }

                LogService.logger.info("Registro de pregunta y respuesta para un carro de catalogo de interes...");

                if (!postCarscoutQuestionsAnswers.getAnswers().isEmpty()) {
                    for (AnswerDTO answersActual : postCarscoutQuestionsAnswers.getAnswers()) {
                        CarscoutQuestionAnswer carscoutQuestionAnswer = carscoutQuestionAnswerRepo.
                                findByUserIdAndCatalogueIdAndQuestionId(postCarscoutQuestionsAnswers.getUserId(), postCarscoutQuestionsAnswers.getCatalogueId(), answersActual.getQuestionId());
                        if (carscoutQuestionAnswer != null) {
                            carscoutQuestionAnswer.setQuestionId(answersActual.getQuestionId());
                            carscoutQuestionAnswer.setOptionId(answersActual.getOptionId());
                            carscoutQuestionAnswer.setUpdateDate(actualTime);
                            carscoutQuestionAnswerRepo.save(carscoutQuestionAnswer);
                        } else {
                            CarscoutQuestionAnswer newCarscoutQuestionAnswer = new CarscoutQuestionAnswer();
                            newCarscoutQuestionAnswer.setUserId(postCarscoutQuestionsAnswers.getUserId());
                            newCarscoutQuestionAnswer.setCatalogueId(postCarscoutQuestionsAnswers.getCatalogueId());
                            newCarscoutQuestionAnswer.setQuestionId(answersActual.getQuestionId());
                            newCarscoutQuestionAnswer.setOptionId(answersActual.getOptionId());
                            newCarscoutQuestionAnswer.setUpdateDate(actualTime);
                            carscoutQuestionAnswerRepo.save(newCarscoutQuestionAnswer);
                        }
                    }
                }
            }

        } catch (Exception e) {
            LogService.logger.error("Error Catch de postCarscoutQuestionAnswers : " + e);
        }
        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResult.getValue());
        responseDTO.setStatus(enumResult.getStatus());
        return responseDTO;
    }

    /**
     * Metodo que inabilita las alertas que tengan mas tiempo del establecido en la aplicación
     *
     * @return Response Generico de Servicio.
     * @author Oscar Montilla
     */
    public ResponseDTO disableAlerts() {
        ResponseDTO responseDTO = new ResponseDTO();

        List<CarscoutAlert> carscoutAlertDisables = new ArrayList<>();

        for (CarscoutAlert carscoutAlertBD : carscoutAlertRepo.findByIntervalMonth(Constants.PROPERTY_CARSCOUT_MAX_ALERT_TIME_MONTHS, new Timestamp(System.currentTimeMillis()), Constants.ACTIVE_DATA)) {

            carscoutAlertBD.setActiveInd(Constants.INACTIVE_DATA.intValue());
            carscoutAlertDisables.add(carscoutAlertBD);
        }
        LogService.logger.info("Alertas de carscout Desabilitados :  " + carscoutAlertDisables.size());

        carscoutAlertRepo.saveAll(carscoutAlertDisables);
        return responseDTO;
    }

}
