package com.kavak.core.service;


import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang3.StringUtils;

import com.kavak.core.dto.AdditionalDTO;
import com.kavak.core.dto.CarCatalogueDTO;
import com.kavak.core.dto.ClientDTO;
import com.kavak.core.dto.CustomerDataDTO;
import com.kavak.core.dto.FinancingPerMonthDTO;
import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.JobDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.MetaValueAdditionalDTO;
import com.kavak.core.dto.MetaValueDomicileDTO;
import com.kavak.core.dto.MetaValueJobDataDTO;
import com.kavak.core.dto.MetaValuePropertyStatusDTO;
import com.kavak.core.dto.MetaValueReferenceDTO;
import com.kavak.core.dto.MetaValueReferenceDataDTO;
import com.kavak.core.dto.MetaValueReferenceFamilyDTO;
import com.kavak.core.dto.MetaValueReferenceLessorDTO;
import com.kavak.core.dto.MetaValueReferencePersonalDTO;
import com.kavak.core.dto.PreviousJobDTO;
import com.kavak.core.dto.ReferenceDTO;
import com.kavak.core.dto.openpay.BankTransferDetailDTO;
import com.kavak.core.dto.openpay.GenerateChargeDTO;
import com.kavak.core.dto.openpay.ResponseChargeOpenPayDTO;
import com.kavak.core.dto.request.PutCheckoutRequestDTO;
import com.kavak.core.dto.request.RegisterFinancingRequestDTO;
import com.kavak.core.dto.response.GetFinancingFormValuesResponseDTO;
import com.kavak.core.dto.response.GetPaymentTypesResponseDTO;
import com.kavak.core.dto.response.OrderResponseDTO;
import com.kavak.core.dto.response.PostRegisterFinancingResponseDTO;
import com.kavak.core.dto.response.PutCheckoutResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.CatalogueCarStatusEnum;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.CheckoutLog;
import com.kavak.core.model.FinancingData;
import com.kavak.core.model.FinancingReferece;
import com.kavak.core.model.FormOptionValue;
import com.kavak.core.model.InspectionLocation;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.CheckoutLogRepository;
import com.kavak.core.repositories.FinancingDataRepository;
import com.kavak.core.repositories.FinancingRefereceRepository;
import com.kavak.core.repositories.FormOptionValueRepository;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.service.openpay.OpenPayTransactionServices;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;

@Stateless
public class CheckoutServices  {

	@Resource
	private EJBContext context;

	@EJB(mappedName = LookUpNames.SESSION_OpenPayTransactionServices)
	private OpenPayTransactionServices openPayTransactionServices;

	@Inject
	private SellCarDetailRepository sellCarDetailRepo;

	@Inject
	private FormOptionValueRepository formOptionValueRepo;

	@Inject
	private UserRepository userRepo;

	@Inject
	private MetaValueRepository metaValueRepo;

	@Inject
	private MessageRepository messageRepo;

	@Inject
	private SaleCheckpointRepository saleCheckpointRepo;

	@Inject
	private InspectionLocationRepository inspectionLocationRepo;

	@Inject
	private UserMetaRepository userMetaRepo;

	@Inject
	private FinancingDataRepository  financingDataRepo;

	@Inject
	private FinancingRefereceRepository financingReferenceRepo;

	@Inject
	private CheckoutLogRepository checkoutLogRepo;

	/**
	 * Obtiene los valores para el formulario de financiamiento
	 * @author Enrique Marin
	 * @return List<InspectionLocationDTO>
	 *
	 */
	public ResponseDTO getFinancingFormValues() {
		LogService.logger.info(Constants.LOG_EXECUTING_START + " [getFinancingFormValues] " + Calendar.getInstance().getTime());
		ResponseDTO responseDTO = new ResponseDTO();
		GetFinancingFormValuesResponseDTO getFinancingFormValuesResponseDTO = new GetFinancingFormValuesResponseDTO();
		List<SellCarDetail> listSellCarDetailBD = sellCarDetailRepo.getCatalogueCarByCertifiedAndActiveAndValide();

		List<CarCatalogueDTO> listCarCatalogueDTO = new ArrayList<>();
		CustomerDataDTO newCustomerDataDTO = new CustomerDataDTO();
		List<GenericDTO> listGenericDTOPersonType = new ArrayList<>();
		List<GenericDTO> listGenericDTOIdentificationType = new ArrayList<>();
		List<GenericDTO> listGenericDTOEducationType = new ArrayList<>();
		List<GenericDTO> listGenericDTOMaritalStatus = new ArrayList<>();
		List<GenericDTO> listGenericDTONacionalityStatus = new ArrayList<>();
		List<GenericDTO> listGenericDTOGender = new ArrayList<>();
		List<GenericDTO> listGenericDTODependents = new ArrayList<>();
		List<GenericDTO> listGenericDTOBinary = new ArrayList<>();
		MetaValuePropertyStatusDTO metaValuePropertyStatusDTO= new MetaValuePropertyStatusDTO();
		List<GenericDTO> listMetaValuePropertyStatusDataDTO = new ArrayList<>();
		MetaValueJobDataDTO metaValueJobDataDTO = new MetaValueJobDataDTO();

		List<GenericDTO> listGenericDTOCompanyType = new ArrayList<>();
		List<GenericDTO> listInsuranceAffiliationDTO = new ArrayList<>();
		List<GenericDTO> listPositionTypeDTO = new ArrayList<>();
		List<GenericDTO> listProfessionDTO = new ArrayList<>();
		List<GenericDTO> listMonthsOld = new ArrayList<>();
		MetaValueReferenceDataDTO metaValueReferenceDataDTO = new MetaValueReferenceDataDTO();
		List<GenericDTO> listGenericDTOFamiliarReference = new ArrayList<>();
		List<GenericDTO> listGenericDTOPersonalReference = new ArrayList<>();

		MetaValueAdditionalDTO metaValueAdditionalDTO = new MetaValueAdditionalDTO();
		List<GenericDTO> listGenericDTODelay = new ArrayList<>();
		List<GenericDTO> listGenericDTOCarUse = new ArrayList<>();
		List<GenericDTO> listGenericDTOIncome = new ArrayList<>();
		//        List<GenericDTO> listGenericDTOMortgageCredit = new ArrayList<>();
		//        List<GenericDTO> listGenericDTOAutomotiveCredit = new ArrayList<>();
		//        List<GenericDTO> listGenericDTOCreditCard = new ArrayList<>();

		for(SellCarDetail sellCarDetailActual: listSellCarDetailBD){
			CarCatalogueDTO newCarCatalogueDTO = new CarCatalogueDTO();
			newCarCatalogueDTO.setId(sellCarDetailActual.getId().toString());
			newCarCatalogueDTO.setYear(sellCarDetailActual.getCarYear().toString());
			newCarCatalogueDTO.setPrice(sellCarDetailActual.getPrice().toString());
			//            newCarCatalogueDTO.setName(KavakUtils.getFriendlyCarName(sellCarDetailActual.getCarMake(),sellCarDetailActual.getCarModel(),sellCarDetailActual.getCarTrim()));
			newCarCatalogueDTO.setName("Stock ID: " + sellCarDetailActual.getId() + " | " + "car_make: "+  sellCarDetailActual.getCarMake() +
					" car_model: " + sellCarDetailActual.getCarModel()+ " car_trim: " + sellCarDetailActual.getCarTrim().split("#")[0] + " car_year: " + sellCarDetailActual.getCarYear() +
					" | KM: " + sellCarDetailActual.getCarKm() +" | Precio: " + sellCarDetailActual.getPrice() );
			listCarCatalogueDTO.add(newCarCatalogueDTO);
			//“Stock ID: ” + id + “ | ” + car_make + “ ” + car_model + “ ” car_trim + “ ” + car_year + “ | KM: ” + car_km + “ | Precio: ” + price

		}

		List<FormOptionValue> listPersonTypeBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_PERSOSN_TYPE);
		for(FormOptionValue formOptionValueActual: listPersonTypeBD){
			GenericDTO newGenericPersonTypeDTO = new GenericDTO();
			newGenericPersonTypeDTO.setValue(formOptionValueActual.getValue());
			newGenericPersonTypeDTO.setName(formOptionValueActual.getName());
			listGenericDTOPersonType.add(newGenericPersonTypeDTO);
		}
		newCustomerDataDTO.setListPersonType(listGenericDTOPersonType);

		List<FormOptionValue> listIdentificationTypeBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_IDENTIFICATION_TYPE);
		for(FormOptionValue formOptionValueActual: listIdentificationTypeBD){
			GenericDTO newGenericIdentificationTypeDTO = new GenericDTO();
			newGenericIdentificationTypeDTO.setValue(formOptionValueActual.getValue());
			newGenericIdentificationTypeDTO.setName(formOptionValueActual.getName());
			listGenericDTOIdentificationType.add(newGenericIdentificationTypeDTO);
		}
		newCustomerDataDTO.setListIdentificationType(listGenericDTOIdentificationType);

		List<FormOptionValue> listEducationTypeBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_EDUCATION_TYPE);
		for(FormOptionValue formOptionValueActual: listEducationTypeBD){
			GenericDTO newGenericEducationTypeDTO = new GenericDTO();
			newGenericEducationTypeDTO.setValue(formOptionValueActual.getValue());
			newGenericEducationTypeDTO.setName(formOptionValueActual.getName());
			listGenericDTOEducationType.add(newGenericEducationTypeDTO);
		}
		newCustomerDataDTO.setListEducationType(listGenericDTOEducationType);


		List<FormOptionValue> listMaritalStatusBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_MARITAL_STATUS);
		for(FormOptionValue formOptionValueActual: listMaritalStatusBD){
			GenericDTO newGenericMaritalDTO = new GenericDTO();
			newGenericMaritalDTO.setValue(formOptionValueActual.getValue());
			newGenericMaritalDTO.setName(formOptionValueActual.getName());
			listGenericDTOMaritalStatus.add(newGenericMaritalDTO);
		}
		newCustomerDataDTO.setListMaritalStatus(listGenericDTOMaritalStatus);

		List<FormOptionValue> listNacionalityBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_NACIONALITY_TYPE);
		for(FormOptionValue formOptionValueActual: listNacionalityBD){
			GenericDTO newGenericNacionalityDTO = new GenericDTO();
			newGenericNacionalityDTO.setValue(formOptionValueActual.getValue());
			newGenericNacionalityDTO.setName(formOptionValueActual.getName());
			listGenericDTONacionalityStatus.add(newGenericNacionalityDTO);
		}
		newCustomerDataDTO.setListNacionality(listGenericDTONacionalityStatus);

		List<FormOptionValue> listGendersBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_GENDERS);
		for(FormOptionValue formOptionValueActual: listGendersBD){
			GenericDTO newGenericGenderDTO = new GenericDTO();
			newGenericGenderDTO.setValue(formOptionValueActual.getValue());
			newGenericGenderDTO.setName(formOptionValueActual.getName());
			listGenericDTOGender.add(newGenericGenderDTO);
		}
		newCustomerDataDTO.setListGenders(listGenericDTOGender);

		List<FormOptionValue> listDependentsBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_DEPENDENTS);
		for(FormOptionValue formOptionValueActual: listDependentsBD){
			GenericDTO newGenericDependentsDTO = new GenericDTO();
			newGenericDependentsDTO.setValue(formOptionValueActual.getValue());
			newGenericDependentsDTO.setName(formOptionValueActual.getName());
			listGenericDTODependents.add(newGenericDependentsDTO);
		}
		newCustomerDataDTO.setListDependents(listGenericDTODependents);

		List<FormOptionValue> listBinaryBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_BINARY);
		for(FormOptionValue formOptionValueActual: listBinaryBD){
			GenericDTO newGenericBinaryDTO = new GenericDTO();
			newGenericBinaryDTO.setValue(formOptionValueActual.getValue());
			newGenericBinaryDTO.setName(formOptionValueActual.getName());
			listGenericDTOBinary.add(newGenericBinaryDTO);
		}
		newCustomerDataDTO.setListBancomerCustomer(listGenericDTOBinary);

		List<FormOptionValue> listPropertyStatusBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_PROPERTY_STATUS);
		for(FormOptionValue formOptionValueActual: listPropertyStatusBD){
			GenericDTO newGenericPropertyDTO = new GenericDTO();
			newGenericPropertyDTO.setValue(formOptionValueActual.getValue());
			newGenericPropertyDTO.setName(formOptionValueActual.getName());
			listMetaValuePropertyStatusDataDTO.add(newGenericPropertyDTO);
		}
		metaValuePropertyStatusDTO.setListMetaValuePropertyStatusDataDTO(listMetaValuePropertyStatusDataDTO);

		List<FormOptionValue> listCompanyTypeBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_COMPANY_TYPE);
		for(FormOptionValue formOptionValueActual: listCompanyTypeBD){
			GenericDTO newGenericDTOCompanyType = new GenericDTO();
			newGenericDTOCompanyType.setValue(formOptionValueActual.getValue());
			newGenericDTOCompanyType.setName(formOptionValueActual.getName());
			listGenericDTOCompanyType.add(newGenericDTOCompanyType);
		}
		metaValueJobDataDTO.setListCompanyTypeDTO(listGenericDTOCompanyType);

		List<FormOptionValue> listInsuranceAffiliationBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_INSURANCE_AFFILIATION);
		for(FormOptionValue formOptionValueActual: listInsuranceAffiliationBD){
			GenericDTO newGenericDTOInsuranceAffiliation = new GenericDTO();
			newGenericDTOInsuranceAffiliation.setValue(formOptionValueActual.getValue());
			newGenericDTOInsuranceAffiliation.setName(formOptionValueActual.getName());
			listInsuranceAffiliationDTO.add(newGenericDTOInsuranceAffiliation);
		}
		metaValueJobDataDTO.setListInsuranceAffiliationDTO(listGenericDTOCompanyType);

		List<FormOptionValue> listPositionTypeBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_POSITION_TYPE);
		for(FormOptionValue formOptionValueActual: listPositionTypeBD){
			GenericDTO newGenericDTOPositionType = new GenericDTO();
			newGenericDTOPositionType.setValue(formOptionValueActual.getValue());
			newGenericDTOPositionType.setName(formOptionValueActual.getName());
			listPositionTypeDTO.add(newGenericDTOPositionType);
		}
		metaValueJobDataDTO.setListPositionDTO(listPositionTypeDTO);

		List<FormOptionValue> listProfessionTypeBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_PROFESSION_TYPE);
		for(FormOptionValue formOptionValueActual: listProfessionTypeBD){
			GenericDTO newGenericDTOProfessionType = new GenericDTO();
			newGenericDTOProfessionType.setValue(formOptionValueActual.getValue());
			newGenericDTOProfessionType.setName(formOptionValueActual.getName());
			listProfessionDTO.add(newGenericDTOProfessionType);
		}
		metaValueJobDataDTO.setListProfessionDTO(listProfessionDTO);

		for(int month = 1; month < 13 ; month++){
			GenericDTO newGenericDTOMonthsOld = new GenericDTO();
			newGenericDTOMonthsOld.setValue(String.valueOf(month));
			if(String.valueOf(month).length() < 2 ){
				newGenericDTOMonthsOld.setName("0" + String.valueOf(month));
			}else{
				newGenericDTOMonthsOld.setName(String.valueOf(month));
			}
			listMonthsOld.add(newGenericDTOMonthsOld);
		}
		metaValueJobDataDTO.setListMonthsOldDTO(listMonthsOld);

		List<FormOptionValue> listFamiliarReferenceBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_FAMILIAR_REFERENCE);
		for(FormOptionValue formOptionValueActual: listFamiliarReferenceBD){
			GenericDTO newGenericDTOFamiliarReference = new GenericDTO();
			newGenericDTOFamiliarReference.setValue(formOptionValueActual.getValue());
			newGenericDTOFamiliarReference.setName(formOptionValueActual.getName());
			listGenericDTOFamiliarReference.add(newGenericDTOFamiliarReference);
		}
		ReferenceDTO familiarReference = new ReferenceDTO();
		familiarReference.setListGenericDTOReference(listGenericDTOFamiliarReference);
		metaValueReferenceDataDTO.setFamiliarReferenceDTO(familiarReference);

		List<FormOptionValue> listPersonalReferenceBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_PERSONAL_REFERENCE);
		for(FormOptionValue formOptionValueActual: listPersonalReferenceBD){
			GenericDTO newGenericDTOPersonalReference = new GenericDTO();
			newGenericDTOPersonalReference.setValue(formOptionValueActual.getValue());
			newGenericDTOPersonalReference.setName(formOptionValueActual.getName());
			listGenericDTOPersonalReference.add(newGenericDTOPersonalReference);
		}
		ReferenceDTO personalReference = new ReferenceDTO();
		personalReference.setListGenericDTOReference(listGenericDTOPersonalReference);
		metaValueReferenceDataDTO.setPersonalReferenceDTO(personalReference);

		List<FormOptionValue> listDelayBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_DELAY);
		for(FormOptionValue formOptionValueActual: listDelayBD){
			GenericDTO newGenericDTODelay = new GenericDTO();
			newGenericDTODelay.setValue(formOptionValueActual.getValue());
			newGenericDTODelay.setName(formOptionValueActual.getName());
			listGenericDTODelay.add(newGenericDTODelay);
		}
		metaValueAdditionalDTO.setListGenericDTODelay(listGenericDTODelay);

		List<FormOptionValue> listCarUseBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_CAR_USE);
		for(FormOptionValue formOptionValueActual: listCarUseBD){
			GenericDTO newGenericDTOCarUse = new GenericDTO();
			newGenericDTOCarUse.setValue(formOptionValueActual.getValue());
			newGenericDTOCarUse.setName(formOptionValueActual.getName());
			listGenericDTOCarUse.add(newGenericDTOCarUse);
		}
		metaValueAdditionalDTO.setListGenericDTOCarUse(listGenericDTOCarUse);

		List<FormOptionValue> listIncomeBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_INCOME);
		for(FormOptionValue formOptionValueActual: listIncomeBD){
			GenericDTO newGenericDTOIncome = new GenericDTO();
			newGenericDTOIncome.setValue(formOptionValueActual.getValue());
			newGenericDTOIncome.setName(formOptionValueActual.getName());
			listGenericDTOIncome.add(newGenericDTOIncome);
		}
		metaValueAdditionalDTO.setListGenericDTOIncome(listGenericDTOIncome);
		metaValueAdditionalDTO.setListGenericDTOMortgageCredit(listGenericDTOBinary);
		metaValueAdditionalDTO.setListGenericDTOAutomotiveCredit(listGenericDTOBinary);
		metaValueAdditionalDTO.setListGenericDTOCreditCard(listGenericDTOBinary);

		getFinancingFormValuesResponseDTO.setListCarCatalogueDTO(listCarCatalogueDTO);
		getFinancingFormValuesResponseDTO.setCustomerDataDTO(newCustomerDataDTO);
		getFinancingFormValuesResponseDTO.setMetaValuePropertyStatusDTO(metaValuePropertyStatusDTO);
		getFinancingFormValuesResponseDTO.setMetaValueJobDataDTO(metaValueJobDataDTO);
		getFinancingFormValuesResponseDTO.setMetaValueReferenceDataDTO(metaValueReferenceDataDTO);
		getFinancingFormValuesResponseDTO.setMetaValueAdditionalDTO(metaValueAdditionalDTO);

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(getFinancingFormValuesResponseDTO);
		LogService.logger.info(Constants.LOG_EXECUTING_END + " [getFinancingFormValues] " + Calendar.getInstance().getTime());
		return responseDTO;
	}

	/**
	 * Obtiene las formas de pago del Auto
	 * @author Enrique Marin
	 * @return List<GenericDTO> value - name
	 *
	 */
	public ResponseDTO getPaymentTypes() {
		LogService.logger.info(Constants.LOG_EXECUTING_START+ " [getPaymentTypes] " + Calendar.getInstance().getTime());
		Long starTime = Calendar.getInstance().getTimeInMillis();
		ResponseDTO responseDTO = new ResponseDTO();
		GetPaymentTypesResponseDTO getPaymentTypesResponseDTO = new GetPaymentTypesResponseDTO();
		List<FormOptionValue> listCarUseBD = formOptionValueRepo.findByCategory(Constants.CATEGORY_PAYMENT_TPE);
		List<GenericDTO> listGenericDTOPaymentTypes = new ArrayList<>();

		for(FormOptionValue formOptionValueActual: listCarUseBD){
			GenericDTO newGenericDTOPaymentType = new GenericDTO();
			newGenericDTOPaymentType.setValue(formOptionValueActual.getValue().toString());
			newGenericDTOPaymentType.setName(formOptionValueActual.getName());
			listGenericDTOPaymentTypes.add(newGenericDTOPaymentType);
		}

		getPaymentTypesResponseDTO.setListGenericDTOPaymentTypes(listGenericDTOPaymentTypes);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(getPaymentTypesResponseDTO);
		LogService.logger.info(Constants.LOG_EXECUTING_END + " [getPaymentTypes] " + (Calendar.getInstance().getTimeInMillis() - starTime));
		return responseDTO;
	}


	/**
	 *
	 * @author Enrique Marin
	 * @param putCheckoutRequestDTO
	 * @return List<GenericDTO> value - name
	 * @throws MessagingException
	 *
	 */
	public ResponseDTO putCheckout(PutCheckoutRequestDTO putCheckoutRequestDTO) throws MessagingException {
		Long starTime = Calendar.getInstance().getTimeInMillis();
		LogService.logger.info(Constants.LOG_EXECUTING_START+ " [putCheckout] ");
		ResponseDTO responseDTO = new ResponseDTO();
		PutCheckoutResponseDTO putCheckoutResponseDTO = new PutCheckoutResponseDTO();
		HashMap<String,Object> dataEmail = new  HashMap<>();
		String source;
	        Timestamp actualTimestamp = new Timestamp(System.currentTimeMillis());
		switch (putCheckoutRequestDTO.getStep()){
			case 1: //  Eligió Forma de Pago
				//Guardando en la tabla v2_checkout_log
				saveCheckoutLog(putCheckoutRequestDTO);

				SaleCheckpoint saleCheckpointBDStep1 = new SaleCheckpoint();
				Optional<User> userBD = userRepo.findById(putCheckoutRequestDTO.getUserId());
				saleCheckpointBDStep1.setUser(userBD.get());
				if(!userBD.isPresent()){
					MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
					return KavakUtils.getResponseNoDataFound(messageNotFound);
				}

				saleCheckpointBDStep1.setCustomerName(userBD.get().getDTO().getFirstName());
				saleCheckpointBDStep1.setCustomerLastName(userBD.get().getDTO().getLastName());
				saleCheckpointBDStep1.setEmail(userBD.get().getEmail());
				saleCheckpointBDStep1.setCarId(putCheckoutRequestDTO.getCarId());
				
				MetaValue metaValueCPRIBD = metaValueRepo.findByAlias("CPRI");
				saleCheckpointBDStep1.setStatus(metaValueCPRIBD.getId());
				saleCheckpointBDStep1.setRegisterDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
				saleCheckpointBDStep1.setSentNetsuite(0L);
				saleCheckpointBDStep1.setSaleOpportunityTypeId(201L);

				if(putCheckoutRequestDTO.getPaymentType() != null){
					saleCheckpointBDStep1.setTypePaymentMethodTypeId(putCheckoutRequestDTO.getPaymentType().toString());
				}else{
					saleCheckpointBDStep1.setTypePaymentMethodTypeId("144");
				}
				
				if(putCheckoutRequestDTO.getInternalUserId() != null) {
				    saleCheckpointBDStep1.setInternalUserId(Long.valueOf(putCheckoutRequestDTO.getInternalUserId()));				    
				}

				Optional<SellCarDetail> sellCarDetailBDStep1 = sellCarDetailRepo.findById(putCheckoutRequestDTO.getCarId());
				if(!sellCarDetailBDStep1.isPresent()){
					MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0009.toString()).getDTO();
					return KavakUtils.getResponseNoDataFound(messageNotFound);
				}
				saleCheckpointBDStep1.setCarYear(Long.valueOf(sellCarDetailBDStep1.get().getCarYear()));
				saleCheckpointBDStep1.setCarMake(sellCarDetailBDStep1.get().getCarMake());
				saleCheckpointBDStep1.setCarModel(sellCarDetailBDStep1.get().getCarModel());
				saleCheckpointBDStep1.setCarKm(Long.valueOf(sellCarDetailBDStep1.get().getCarKm()));
				saleCheckpointBDStep1.setSku(sellCarDetailBDStep1.get().getSku());
				if(sellCarDetailBDStep1.get().getCarTrim() != null) {
					saleCheckpointBDStep1.setCarVersion(sellCarDetailBDStep1.get().getCarTrim().split("##")[0]);
				}

				if(StringUtils.isEmpty(putCheckoutRequestDTO.getSource())) {
					source = Constants.SOURCE_IOS_APP;
				} else {
					source = putCheckoutRequestDTO.getSource();
				}
				saleCheckpointBDStep1.setSource(source);

				if(putCheckoutRequestDTO.getVersionApp() != null) {
					saleCheckpointBDStep1.setVersionApp(putCheckoutRequestDTO.getVersionApp());
				}

				List<SaleCheckpoint> listSaleCheckpointDuplicate = saleCheckpointRepo.findByUserIdAndCarIdOrderByIdAsc(putCheckoutRequestDTO.getUserId(), putCheckoutRequestDTO.getCarId());
				// Se agregar el campo control duplicado para que Netsuite lleve control de que las ofertas ya estan repetidas
				if(listSaleCheckpointDuplicate.size() == 1){
					saleCheckpointBDStep1.setDuplicatedOfferControl(saleCheckpointBDStep1.getId());
				}else{
					saleCheckpointBDStep1.setDuplicatedOfferControl(saleCheckpointRepo.save(saleCheckpointBDStep1).getId());
				}
				
				//Disable communications
				if(putCheckoutRequestDTO.isDisableCommunications() != false) {
				    saleCheckpointBDStep1.setSendEmail(true);
				    saleCheckpointBDStep1.setAppointmentEmailSent(true);
				    saleCheckpointBDStep1.setBookedCarSmsSent(true);
				    saleCheckpointBDStep1.setScheduledAppointmentSmsSent(true);
				    saleCheckpointBDStep1.setBookingTimeExpiredNotificationSent(true);
				    saleCheckpointBDStep1.setBookingTimeReminderNotificationSent(true);
				    saleCheckpointBDStep1.setBookedRegisteredNotificationSent(true);
				    saleCheckpointBDStep1.setAppointmentBookedNotificationSent(true); 
				    saleCheckpointBDStep1.setAppointmentRegisteredNotificationSent(true);
				    saleCheckpointBDStep1.setBookedEmailSent(true);
				    saleCheckpointBDStep1.setFinancingEmailSent(true);    	    
				}
				//

				putCheckoutResponseDTO.setCheckpointId(saleCheckpointRepo.save(saleCheckpointBDStep1).getId());

				//Correo
				//Ingreso Cliente Checkout Endpoints
				//
				Optional<MetaValue> metaValueBD = metaValueRepo.findById(Long.valueOf(saleCheckpointBDStep1.getTypePaymentMethodTypeId()));
				HashMap<String,Object> subjectData = new HashMap<>();
				subjectData.put(Constants.KEY_SUBJECT,"INGRESO DE CLIENTE AL CHECKOUT - Stock ID #" + sellCarDetailBDStep1.get().getId() + " " + sellCarDetailBDStep1.get().getCarYear() + " " + KavakUtils.getFriendlyCarName(sellCarDetailBDStep1.get().getDTO()));
				subjectData.put("car_year", sellCarDetailBDStep1.get().getCarYear());
				subjectData.put("car_id", sellCarDetailBDStep1.get().getId());
				subjectData.put("car_name", KavakUtils.getFriendlyCarName(sellCarDetailBDStep1.get().getDTO()));

				dataEmail.put(Constants.KEY_EMAIL_CAR_ID, sellCarDetailBDStep1.get().getId());
				dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, sellCarDetailBDStep1.get().getCarYear() + " " + KavakUtils.getFriendlyCarName(sellCarDetailBDStep1.get().getDTO()));
				dataEmail.put(Constants.KEY_EMAIL_USER_ID, userBD.get().getId() + 1000L);
				dataEmail.put(Constants.KEY_EMAIL_USER_NAME, saleCheckpointBDStep1.getCustomerName() +" "+ saleCheckpointBDStep1.getCustomerLastName());
				dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, userBD.get().getEmail());
				dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userBD.get().getDTO().getPhone());

				if(metaValueBD.get().getOptionName() == null){
					dataEmail.put(Constants.KEY_EMAIL_PAYMENT_TYPE, "No definido");
				}else{
					dataEmail.put(Constants.KEY_EMAIL_PAYMENT_TYPE, metaValueBD.get().getOptionName());
				}
				if(putCheckoutRequestDTO.getSource() == null || putCheckoutRequestDTO.getSource().trim().equals("")){
					dataEmail.put(Constants.KEY_EMAIL_SOURCE, "iOS");
				}else{
					dataEmail.put(Constants.KEY_EMAIL_SOURCE, putCheckoutRequestDTO.getSource());
				}

				String emailTo= "";

				if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)){
					emailTo= "it@kavak.com";
				}else if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)){
					emailTo= "checkout@kavak.com";
				}
				try {
					InternetAddress emailFrom = new InternetAddress("hola@kavak.com", "Hola Kavak");
					boolean sendInternalEmail = KavakUtils.sendInternalEmail(emailTo, null, subjectData,"checkout_login_alert.html", null, dataEmail, emailFrom);
					LogService.logger.info(Constants.LOG_EXECUTING_START+ " [postAppointment] - sendInternalEmail retorno " + sendInternalEmail);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
				responseDTO.setCode(enumResult.getValue());
				responseDTO.setStatus(enumResult.getStatus());
				responseDTO.setData(putCheckoutResponseDTO);
				LogService.logger.info(Constants.LOG_EXECUTING_END+ "putCheckout [Paso 1] " + (Calendar.getInstance().getTimeInMillis() -starTime) );
				return responseDTO;
			case 2: //  Eligió Dirección de Entrega
				//Guardando en la tabla v2_checkout_log
				saveCheckoutLog(putCheckoutRequestDTO);

				Optional<SaleCheckpoint> saleCheckpointBDStep2 = saleCheckpointRepo.findById(putCheckoutRequestDTO.getCheckpointId());
				if(!saleCheckpointBDStep2.isPresent()){
					MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0010.toString()).getDTO();
					return KavakUtils.getResponseNoDataFound(messageNotFound);
				}

				if(saleCheckpointBDStep2.get().getStatus() != 152L){
				    MetaValue metaValueCPDABD = metaValueRepo.findByAlias("CPDA");
				    saleCheckpointBDStep2.get().setStatus(metaValueCPDABD.getId());
				}

				if(putCheckoutRequestDTO.getDeliveryAddressId() == 0){//verificar
					//  if(putCheckoutRequestDTO.getDeliveryAddressId() == 0 && putCheckoutRequestDTO.getDeliveryAddressId() != null){//verificar
					saleCheckpointBDStep2.get().setSearchKavak("No");
					saleCheckpointBDStep2.get().setShippingAddressDescription(putCheckoutRequestDTO.getBillingAddressDTO().getStreet() + ", " + putCheckoutRequestDTO.getBillingAddressDTO().getExteriorNumber() + ", "
							+ putCheckoutRequestDTO.getBillingAddressDTO().getInteriorNumber() + ", " + putCheckoutRequestDTO.getBillingAddressDTO().getZipCode() + ".");
					MetaValue metaValueCPDEBD = metaValueRepo.findByAlias("DICLI");
					saleCheckpointBDStep2.get().setShippingAddressId(metaValueCPDEBD.getId());
					saleCheckpointBDStep2.get().setZipCode(putCheckoutRequestDTO.getBillingAddressDTO().getZipCode());
					//  if(putCheckoutRequestDTO.getShipmentCost() != null){//verificar
					saleCheckpointBDStep2.get().setCarShippingCost(putCheckoutRequestDTO.getShipmentCost().toString());
					//  }
				}else{//verificar
					//   }else if(putCheckoutRequestDTO.getDeliveryAddressId() != null){//verificar
					saleCheckpointBDStep2.get().setSearchKavak("Si");
					Optional<InspectionLocation> inspectionLocationBD = inspectionLocationRepo.findById(putCheckoutRequestDTO.getDeliveryAddressId());
					saleCheckpointBDStep2.get().setShippingAddressDescription(inspectionLocationBD.get().getAddress() + ", " + inspectionLocationBD.get().getCity() + ", " +  inspectionLocationBD.get().getZip() + ", "
							+ inspectionLocationBD.get().getState() + ".");
					MetaValue metaValueCPDKABD = metaValueRepo.findByAlias("DICAR");
					saleCheckpointBDStep2.get().setShippingAddressId(metaValueCPDKABD.getId());
					saleCheckpointBDStep2.get().setZipCode(inspectionLocationBD.get().getZip());
					saleCheckpointBDStep2.get().setCarShippingCost("0");
				}

				saleCheckpointBDStep2.get().setUpdateDate(KavakUtils.getDateOnFormat(new Date()));
				saleCheckpointBDStep2.get().setSentNetsuite(2L);

				putCheckoutResponseDTO.setCheckpointId(saleCheckpointRepo.save(saleCheckpointBDStep2.get()).getId());
				LogService.logger.info(Constants.LOG_EXECUTING_END+ "putCheckout [Paso 2] " + (Calendar.getInstance().getTimeInMillis() - starTime));break;
			case 3: //  Eligió Garantía para el Auto
				//Guardando en la tabla v2_checkout_log
				saveCheckoutLog(putCheckoutRequestDTO);

				Optional<SaleCheckpoint> saleCheckpointBDStep3 = saleCheckpointRepo.findById(putCheckoutRequestDTO.getCheckpointId());
				if(!saleCheckpointBDStep3.isPresent()){
					MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0010.toString()).getDTO();
					return KavakUtils.getResponseNoDataFound(messageNotFound);
				}
				if(saleCheckpointBDStep3.get().getStatus() != 152L){
				    MetaValue metaValueCPGABD = metaValueRepo.findByAlias("CPGA");
				    saleCheckpointBDStep3.get().setStatus(metaValueCPGABD.getId());
				}

				//   if(putCheckoutRequestDTO.getWarrantyId() != null){//verificar
				if(putCheckoutRequestDTO.getWarrantyId() == 0){
					saleCheckpointBDStep3.get().setWarrantyName("Incluída");
					saleCheckpointBDStep3.get().setWarrantyRange("0");
					saleCheckpointBDStep3.get().setWarrantyAmount(0L);
				}else {
					Optional<MetaValue> metaValueWarrantyBD = metaValueRepo.findById(putCheckoutRequestDTO.getWarrantyId());
					saleCheckpointBDStep3.get().setWarrantyName(metaValueWarrantyBD.get().getGroupName());
					saleCheckpointBDStep3.get().setWarrantyRange(metaValueWarrantyBD.get().getOptionName());
					saleCheckpointBDStep3.get().setWarrantyAmount(putCheckoutRequestDTO.getWarrantyAmount());
				}
				//      }
				saleCheckpointBDStep3.get().setUpdateDate(KavakUtils.getDateOnFormat(new Date()));
				saleCheckpointBDStep3.get().setSentNetsuite(2L);

				putCheckoutResponseDTO.setCheckpointId(saleCheckpointRepo.save(saleCheckpointBDStep3.get()).getId());
				LogService.logger.info(Constants.LOG_EXECUTING_END+ " [putCheckout] - Step 3" + (Calendar.getInstance().getTimeInMillis() - starTime) );break;
			case 4: //Eligió Seguro para el Auto
				//Guardando en la tabla v2_checkout_log
				saveCheckoutLog(putCheckoutRequestDTO);

				LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] [Paso 4] " + Calendar.getInstance().getTime());
				Optional<SaleCheckpoint> saleCheckpointBDStep4 = saleCheckpointRepo.findById(putCheckoutRequestDTO.getCheckpointId());
				if(!saleCheckpointBDStep4.isPresent()){
					MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0010.toString()).getDTO();
					return KavakUtils.getResponseNoDataFound(messageNotFound);
				}
				if(saleCheckpointBDStep4.get().getStatus() != 152L){
				    MetaValue metaValueCPSA = metaValueRepo.findByAlias("CPSA");
				    saleCheckpointBDStep4.get().setStatus(metaValueCPSA.getId());
				}
				//        if(putCheckoutRequestDTO.getInsuranceId() != null){//verificar
				if(putCheckoutRequestDTO.getInsuranceId() == 0){
					saleCheckpointBDStep4.get().setInsuranceName("No deseo seguro");
					saleCheckpointBDStep4.get().setInsuranceModality("0");
					saleCheckpointBDStep4.get().setInsuranceAmount("0");

				}else{
					Optional<MetaValue> metaValueInsuranceBD = metaValueRepo.findById(putCheckoutRequestDTO.getInsuranceId());
					saleCheckpointBDStep4.get().setInsuranceName(metaValueInsuranceBD.get().getGroupName());
					saleCheckpointBDStep4.get().setInsuranceModality( metaValueInsuranceBD.get().getOptionName());
					saleCheckpointBDStep4.get().setInsuranceAmount(putCheckoutRequestDTO.getInsuranceAmount().toString());
				}
				//          }

				saleCheckpointBDStep4.get().setUpdateDate(KavakUtils.getDateOnFormat(new Date()));
				saleCheckpointBDStep4.get().setSentNetsuite(2L);

				putCheckoutResponseDTO.setCheckpointId(saleCheckpointRepo.save(saleCheckpointBDStep4.get()).getId());
				LogService.logger.info(Constants.LOG_EXECUTING_END + " [putCheckout] [Paso 4] " + (Calendar.getInstance().getTimeInMillis() - starTime)); break;
			case 5: // Pagar Reserva del Auto
				//Guardando en la tabla v2_checkout_log
				saveCheckoutLog(putCheckoutRequestDTO);

				LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] [Paso 5] " );
				Optional<SaleCheckpoint> saleCheckpointBDStep5 = saleCheckpointRepo.findById(putCheckoutRequestDTO.getCheckpointId());
				
				if(putCheckoutRequestDTO.getPaymentType() == Constants.PAYMENT_FINANCING && putCheckoutRequestDTO.getFinancingMonths() != null){
					saleCheckpointBDStep5.get().setMonthFinancing(putCheckoutRequestDTO.getFinancingMonths().toString());
				}
				Optional<User> userBDStep5 = userRepo.findById(putCheckoutRequestDTO.getUserId());
				Optional<SellCarDetail> sellCarDetailBDStep5 = sellCarDetailRepo.findById(saleCheckpointBDStep5.get().getCarId());

				if(!saleCheckpointBDStep5.isPresent()){
					MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0010.toString()).getDTO();
					return KavakUtils.getResponseNoDataFound(messageNotFound);
				}

				if(!userBDStep5.isPresent()){
					MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
					return KavakUtils.getResponseNoDataFound(messageNotFound);
				}

				if(!sellCarDetailBDStep5.isPresent()){
					MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0009.toString()).getDTO();
					return KavakUtils.getResponseNoDataFound(messageNotFound);
				}

				//  Validacion de si el auto ya esta reservado
				if(KavakUtils.getStatusCar(sellCarDetailBDStep5.get()).equals(CatalogueCarStatusEnum.BOOKED)){
					MessageDTO messageDTO = messageRepo.findByCode(MessageEnum.M0044.toString()).getDTO();
					List<MessageDTO> listMessageDTO = new ArrayList<>();
					listMessageDTO.add(messageDTO);
					EndPointCodeResponseEnum enumIsSaleResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
					responseDTO.setCode(enumIsSaleResult.getValue());
					responseDTO.setStatus(enumIsSaleResult.getStatus());
					responseDTO.setListMessage(listMessageDTO);
					responseDTO.setData(new GenericDTO());
					LogService.logger.info(Constants.LOG_EXECUTING_END + " [putCheckout] " + (Calendar.getInstance().getTimeInMillis() - starTime));
					return responseDTO;
				}

				GenerateChargeDTO generateChargeDTO = new GenerateChargeDTO();
				generateChargeDTO.setUserId(putCheckoutRequestDTO.getUserId());
				generateChargeDTO.setBillingAddressDTO(putCheckoutRequestDTO.getBillingAddressDTO());
				generateChargeDTO.setCarId(sellCarDetailBDStep5.get().getId());
				generateChargeDTO.setDeviceSessionId(putCheckoutRequestDTO.getDeviceSessionId());
				generateChargeDTO.setPaymentTypeId(putCheckoutRequestDTO.getPaymentType());
				generateChargeDTO.setTokenId(putCheckoutRequestDTO.getTokenId());
				generateChargeDTO.setTotalPrice(putCheckoutRequestDTO.getTotalPrice());
				generateChargeDTO.setCarPrice(Long.valueOf(putCheckoutRequestDTO.getCarPrice()));
				generateChargeDTO.setEmail(userBDStep5.get().getEmail());

				// 144l Down_payment se recibe solo en pago con Financiamiento - 143
				if(putCheckoutRequestDTO.getPaymentType() == Constants.PAYMENT_FINANCING && putCheckoutRequestDTO.getDownPayment() != null){
				    generateChargeDTO.setDownPayment(putCheckoutRequestDTO.getDownPayment().longValue());
				}
				generateChargeDTO.setHasTradeIn(putCheckoutRequestDTO.isHasTradeInCar());
				Boolean successfulPayment = false;
				try {
				    ResponseChargeOpenPayDTO responseChargeOpenPayDTO =  openPayTransactionServices.generateChargesOpenPay(generateChargeDTO, userBDStep5.get().getDTO(), saleCheckpointBDStep5.get().getDTO(), sellCarDetailBDStep5.get().getDTO(), putCheckoutRequestDTO.getSource());
				    LogService.logger.info("Termino el proceso de open pay");
				    if(EndPointCodeResponseEnum.C0200.getValue().equals(responseChargeOpenPayDTO.getResponseDTO().getCode())){
					LogService.logger.info("inicio if = 200 respuesta open pay putCheckout paso 5");
					successfulPayment = true;
					String patternNumber = "$###,###,###";
					String patternNumberKM = "###,###,###";

					NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
					DecimalFormat decimalFormat = (DecimalFormat)nf;
					DecimalFormat decimalFormatKM = (DecimalFormat)nf;
					decimalFormat.applyPattern(patternNumber);
					decimalFormatKM.applyPattern(patternNumberKM);

					MetaValue metaValueBOOKINGPRICE = metaValueRepo.findByAlias(Constants.BOOKING_PRICE);

					// Garantia
					if(putCheckoutRequestDTO.getPaymentType() != 144L) {
					    putCheckoutResponseDTO.setWarranty(saleCheckpointBDStep5.get().getWarrantyName());
					}

					// Seguro
					if(putCheckoutRequestDTO.getPaymentType() == 141L) { // Solo Contado
					    putCheckoutResponseDTO.setInsurance(saleCheckpointBDStep5.get().getInsuranceName());
					}

					Long amount = Long.valueOf(putCheckoutRequestDTO.getCarPrice()) - Long.valueOf(metaValueBOOKINGPRICE.getOptionName());
					if(putCheckoutRequestDTO.getPaymentType() == 143L && putCheckoutRequestDTO.getFinancingMonths() != null){
					    putCheckoutResponseDTO.setFinancing(decimalFormat.format(amount) + " MXN / " + putCheckoutRequestDTO.getFinancingMonths() + " Meses");
					}

					putCheckoutResponseDTO.setCarName(KavakUtils.getFriendlyCarName(sellCarDetailBDStep5.get().getDTO()));
					putCheckoutResponseDTO.setKm(sellCarDetailBDStep5.get().getCarKm());
					putCheckoutResponseDTO.setCarPrice(sellCarDetailBDStep5.get().getPrice().toString());
					putCheckoutResponseDTO.setBookingPrice(decimalFormat.format(Long.valueOf(metaValueBOOKINGPRICE.getOptionName())));

					if(putCheckoutRequestDTO.getPaymentType() != 143L && responseChargeOpenPayDTO.getBankCharge() != null) {
					    BankTransferDetailDTO bankTransferDetailDTO = new BankTransferDetailDTO();
					    bankTransferDetailDTO.setReference(responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getName());
					    bankTransferDetailDTO.setClabe(responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getClabe());
					    Long transferAmount = Long.valueOf(putCheckoutRequestDTO.getCarPrice()) - Long.valueOf(metaValueBOOKINGPRICE.getOptionName());
					    bankTransferDetailDTO.setTransferAmount(transferAmount.toString());
					    bankTransferDetailDTO.setPaymentDescription(responseChargeOpenPayDTO.getBankCharge().getDescription());
					    bankTransferDetailDTO.setUrlReceipt(Constants.PROPERTY_EMAIL_URL_RECEIPT + Constants.OPENPAY_MERCHANTID + "/" + responseChargeOpenPayDTO.getBankCharge().getId());
					    putCheckoutResponseDTO.setBankTransferDetailDTO(bankTransferDetailDTO);
					}

					if(putCheckoutRequestDTO.getPaymentType() != null){
					    saleCheckpointBDStep5.get().setTypePaymentMethodTypeId(putCheckoutRequestDTO.getPaymentType().toString());
					    saleCheckpointRepo.save(saleCheckpointBDStep5.get());
					}

					OrderResponseDTO saleCarOrderResponse = new OrderResponseDTO();

					try {

					    //Objeto Order
					    if(responseChargeOpenPayDTO.getOrderResponseDTO().getId() != null && !responseChargeOpenPayDTO.getOrderResponseDTO().getId().toString().trim().isEmpty()) {
						saleCarOrderResponse.setId(responseChargeOpenPayDTO.getOrderResponseDTO().getId());
					    }
					    if(responseChargeOpenPayDTO.getOrderResponseDTO().getItemId() != null && !responseChargeOpenPayDTO.getOrderResponseDTO().getItemId().toString().trim().isEmpty()) {
						saleCarOrderResponse.setItemId(responseChargeOpenPayDTO.getOrderResponseDTO().getItemId());
					    }
					    if(responseChargeOpenPayDTO.getOrderResponseDTO().getItemName() != null && !responseChargeOpenPayDTO.getOrderResponseDTO().getItemName().toString().trim().isEmpty()) {
						saleCarOrderResponse.setItemName(responseChargeOpenPayDTO.getOrderResponseDTO().getItemName());
					    }
					    if(responseChargeOpenPayDTO.getOrderResponseDTO().getPaidAmt() != null && !responseChargeOpenPayDTO.getOrderResponseDTO().getPaidAmt().toString().trim().isEmpty()) {
						saleCarOrderResponse.setPaidAmt(responseChargeOpenPayDTO.getOrderResponseDTO().getPaidAmt());
					    }
					    if(responseChargeOpenPayDTO.getOrderResponseDTO().getTransactionId() != null && !responseChargeOpenPayDTO.getOrderResponseDTO().getTransactionId().toString().trim().isEmpty()) {
						saleCarOrderResponse.setTransactionId(responseChargeOpenPayDTO.getOrderResponseDTO().getTransactionId());
					    }
					    if(responseChargeOpenPayDTO.getOrderResponseDTO().getPendingAmt() != null && !responseChargeOpenPayDTO.getOrderResponseDTO().getPendingAmt().toString().trim().isEmpty()) {
						saleCarOrderResponse.setPendingAmt(responseChargeOpenPayDTO.getOrderResponseDTO().getPendingAmt());
					    }  
					    putCheckoutResponseDTO.setOrderResponseDTO(saleCarOrderResponse);

					} catch (Exception e) {
					    e.printStackTrace();
					}

					LogService.logger.info("fin if = 200 respuesta open pay putCheckout paso 5");
						
						
					}else{
						LogService.logger.info("Error else respuesta open pay putCheckout paso 5");
						MessageDTO newMessageDTO = responseChargeOpenPayDTO.getResponseDTO().getListMessage().get(0);
						saleCheckpointBDStep5.get().setCheckoutResult("Error realizando el pago de reserva: " + newMessageDTO.getApiMessage() + " (" + newMessageDTO.getErrorCode() + ").");
						saleCheckpointBDStep5.get().setPaymentStatus(null);
						MetaValue metaValueCPRP = metaValueRepo.findByAlias("CPEOP");
						saleCheckpointBDStep5.get().setStatus(metaValueCPRP.getId());
						saleCheckpointBDStep5.get().setUpdateDate(actualTimestamp);
						saleCheckpointRepo.save(saleCheckpointBDStep5.get());
						return responseChargeOpenPayDTO.getResponseDTO();
					}
				} catch (Exception e) {
					// TODO: handle exception
					if(!successfulPayment){
						LogService.logger.info("inicio Error Catch if(!successfulPayment) put checkpout paso 5 successfulPayment: "+successfulPayment);
						ResponseChargeOpenPayDTO responseChargeOpenPayDTO = new ResponseChargeOpenPayDTO();
						EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
						responseDTO.setCode(enumResultException.getValue());
						responseDTO.setStatus(enumResultException.getStatus());
						List<MessageDTO> listMessageDTO = new ArrayList<>();
						responseDTO.setListMessage(listMessageDTO);
						responseChargeOpenPayDTO.setResponseDTO(responseDTO);
						saleCheckpointBDStep5.get().setCheckoutResult("no se pudo completar el pago de reserva.");
						saleCheckpointBDStep5.get().setPaymentStatus(null);
						MetaValue metaValueCPRP = metaValueRepo.findByAlias("CPEOP");
						saleCheckpointBDStep5.get().setStatus(metaValueCPRP.getId());
						saleCheckpointBDStep5.get().setUpdateDate(actualTimestamp);
						saleCheckpointRepo.save(saleCheckpointBDStep5.get());
						LogService.logger.info("fin Error Catch if(!successfulPayment) put checkpout paso 5");
						return responseChargeOpenPayDTO.getResponseDTO();
					}
				}

				LogService.logger.info(Constants.LOG_EXECUTING_END + " [putCheckout] [Paso 5] " + (Calendar.getInstance().getTimeInMillis() - starTime)); break;
			default: break;
		}

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(putCheckoutResponseDTO);
		LogService.logger.info(Constants.LOG_EXECUTING_END + " [putCheckout] " + (Calendar.getInstance().getTimeInMillis() - starTime));
		return responseDTO;
	}


	/**
	 * Metodo que registra la inforamcion de Registro financiero, Actualiza el registro de financiamiento
	 * @author Oscar Montilla
	 * @param postRegisterFinancingRequestDTO Objeto que almacena la informacion de Registo Financiero de un cliente
	 * @return responseDTO Response Generico de servicio
	 */
	public ResponseDTO registerFinancingForm(RegisterFinancingRequestDTO postRegisterFinancingRequestDTO) {

		ResponseDTO responseDTO = new ResponseDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();
		ClientDTO client = postRegisterFinancingRequestDTO.getClientDTO();
		MetaValueDomicileDTO domicile = postRegisterFinancingRequestDTO.getMetaValueDomicileDTO();
		MetaValueReferenceDTO references = postRegisterFinancingRequestDTO.getMetaValueReferenceDTO();

		UserMeta saveMetaDomicile = new UserMeta();
		UserMeta saveMetaLandLinePhone = new UserMeta();
		UserMeta saveMetaPhone = new UserMeta();
		FinancingReferece saveFinancingDataFamily = new FinancingReferece();
		FinancingReferece saveFinancingDataPersonal = new FinancingReferece();
		FinancingReferece saveFinancingDataLessor = new FinancingReferece();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		PostRegisterFinancingResponseDTO postRegisterFinancingResponseDTO = new PostRegisterFinancingResponseDTO();

		FinancingData financingDataSave = new FinancingData();

		if (postRegisterFinancingRequestDTO.getClientDTO().getUserId() == null
				|| postRegisterFinancingRequestDTO.getClientDTO().getIdentificationType() == null
				|| postRegisterFinancingRequestDTO.getClientDTO().getIdNumber() == null
				|| postRegisterFinancingRequestDTO.getClientDTO().getCarId() == null
				|| postRegisterFinancingRequestDTO.getClientDTO().getIdentificationType().length() == 0
				|| postRegisterFinancingRequestDTO.getClientDTO().getIdNumber().length() == 0) {
			LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente.");
			MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString())
					.getRequestDTO(" " + (postRegisterFinancingRequestDTO.getClientDTO().getUserId() == null?"client.user_id, ":"") +
							(postRegisterFinancingRequestDTO.getClientDTO().getIdNumber() == null?"client.identification_type, ":"") + (postRegisterFinancingRequestDTO.getClientDTO().getCarId() == null?"client.car_id, ":""));
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);

			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
					.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new ArrayList<>());

			return responseDTO;

		}

		Optional<User> userBD = userRepo.findById(client.getUserId());
		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(client.getCarId());

		if (!userBD.isPresent()) {

			LogService.logger.info("El registro de usuario no existe en Base de datos..");
			MessageDTO messagenoExitUser = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
			listMessageDTO.add(messagenoExitUser);
			responseDTO.setListMessage(listMessageDTO);

		}
		if (!sellCarDetailBD.isPresent()) {

			LogService.logger.info("El registro de id_car no existe en Base de datos ..");
			MessageDTO messagenoExitSellCard = messageRepo.findByCode(MessageEnum.M0026.toString()).getDTO();
			listMessageDTO.add(messagenoExitSellCard);
			responseDTO.setListMessage(listMessageDTO);
		}

		if (responseDTO.getListMessage() != null) {
			LogService.logger.info("Existe una lista de Menssaje de validacion, devolver los mensajes..");
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
					.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new ArrayList<>());

		} else {

			FinancingData ExistFinancingData = financingDataRepo.findByUserIdAndSellCarDetailId(client.getUserId(),
					client.getCarId());

			if (ExistFinancingData == null) {

				LogService.logger.info("Es un registro nuevo se procede a agregar los datos..");
				LogService.logger.info("Se esta registrando la información de domicilio del cliente..");

				String metaValue = (domicile.getStreet() + "#" + domicile.getExteriorNumber() + "#" + (domicile.getInteriorNumber()==null?"#":domicile.getInteriorNumber()+"#") + domicile.getPostalCode());

				saveMetaDomicile.setUser(userBD.get());
				saveMetaDomicile.setMetaKey(Constants.META_VALUE_ADDRESS_FINANCING);
				saveMetaDomicile.setMetaValue(metaValue);
				UserMeta saveMetaNewDomicile = userMetaRepo.save(saveMetaDomicile);

				LogService.logger.info("Se esta registrando la información de telefono fijo del cliente..");
				saveMetaLandLinePhone.setUser(userBD.get());
				saveMetaLandLinePhone.setMetaKey(Constants.META_VALUE_LANDLINE_PHONE_FINANCING);
				saveMetaLandLinePhone.setMetaValue(client.getHomePhone());
				userMetaRepo.save(saveMetaLandLinePhone);

				LogService.logger.info("Se esta registrando la información de telefono del cliente..");
				saveMetaPhone.setUser(userBD.get());
				saveMetaPhone.setMetaKey(Constants.META_VALUE_PHONE_FINANCING);
				saveMetaPhone.setMetaValue(client.getMobilePhone());
				userMetaRepo.save(saveMetaPhone);

				if(references.getMetaValueReferenceFamilyDTO() != null) {

					LogService.logger.info("Se esta registrando la referencia familiar del cliente..");
					saveFinancingDataFamily.setName(references.getMetaValueReferenceFamilyDTO().getName());
					saveFinancingDataFamily
							.setFirstLastname(references.getMetaValueReferenceFamilyDTO().getFirstLastname());
					saveFinancingDataFamily
							.setSecondLastname(references.getMetaValueReferenceFamilyDTO().getSecondLastname());
					saveFinancingDataFamily.setPhone(references.getMetaValueReferenceFamilyDTO().getPhone());
					saveFinancingDataFamily.setAddress(references.getMetaValueReferenceFamilyDTO().getAddress());
					saveFinancingDataFamily.setRelationship(references.getMetaValueReferenceFamilyDTO().getRelationship());
					saveFinancingDataFamily.setTypeReference(Constants.META_VALUE_TYPE_REFERENCE_FAMILY);

					FinancingReferece referenceFamily = financingReferenceRepo.save(saveFinancingDataFamily);

					financingDataSave.setReferenceFamilyId(referenceFamily.getId());
				}

				if(references.getMetaValueReferencePersonalDTO() != null) {

					LogService.logger.info("Se esta registrando la referencia personal del cliente..");
					saveFinancingDataPersonal.setName(references.getMetaValueReferencePersonalDTO().getName());
					saveFinancingDataPersonal
							.setFirstLastname(references.getMetaValueReferencePersonalDTO().getFirstLastname());
					saveFinancingDataPersonal
							.setSecondLastname(references.getMetaValueReferencePersonalDTO().getSecondLastname());
					saveFinancingDataPersonal.setPhone(references.getMetaValueReferencePersonalDTO().getPhone());
					saveFinancingDataPersonal.setAddress(references.getMetaValueReferencePersonalDTO().getAddress());
					saveFinancingDataPersonal
							.setRelationship(references.getMetaValueReferencePersonalDTO().getRelationship());
					saveFinancingDataPersonal.setTypeReference(Constants.META_VALUE_TYPE_REFERENCE_PERSONAL);

					FinancingReferece referencePersonal = financingReferenceRepo.save(saveFinancingDataPersonal);

					financingDataSave.setReferencePersonalId(referencePersonal.getId());

				}

				if(references.getMetaValueReferenceLessorDTO() != null) {

					LogService.logger.info("Se esta registrando la referencia arrendador del cliente..");
					saveFinancingDataLessor.setName(references.getMetaValueReferenceLessorDTO().getName());
					saveFinancingDataLessor
							.setFirstLastname(references.getMetaValueReferenceLessorDTO().getFirstLastname());
					saveFinancingDataLessor
							.setSecondLastname(references.getMetaValueReferenceLessorDTO().getSecondLastname());
					saveFinancingDataLessor.setPhone(references.getMetaValueReferenceLessorDTO().getPhone());
					saveFinancingDataLessor.setAddress(references.getMetaValueReferenceLessorDTO().getAddress());
					saveFinancingDataLessor.setTypeReference(Constants.META_VALUE_TYPE_REFERENCE_PERSONAL);

					FinancingReferece referenceLessor = financingReferenceRepo.save(saveFinancingDataLessor);

					financingDataSave.setReferencePersonalId2(referenceLessor.getId());

				}
				financingDataSave.setUser(userBD.get());
				financingDataSave.setPersonType(postRegisterFinancingRequestDTO.getClientDTO().getPersonType());
				financingDataSave
						.setIdentificationType(postRegisterFinancingRequestDTO.getClientDTO().getIdentificationType());

				System.out.println("que valor llega de id_number =="
						+ postRegisterFinancingRequestDTO.getClientDTO().getIdNumber());
				financingDataSave.setIdNumber(postRegisterFinancingRequestDTO.getClientDTO().getIdNumber());
				financingDataSave.setElectorKey(postRegisterFinancingRequestDTO.getClientDTO().getElectorKey());

				financingDataSave.setEducationLevel(postRegisterFinancingRequestDTO.getClientDTO().getEducationLevel());
				financingDataSave
						.setMaritalStatusId(postRegisterFinancingRequestDTO.getClientDTO().getMaritalStatusId());
				financingDataSave.setNationalityId(postRegisterFinancingRequestDTO.getClientDTO().getNationalityId());
				financingDataSave.setAnddressId(saveMetaNewDomicile.getId());
				financingDataSave.setGender(postRegisterFinancingRequestDTO.getClientDTO().getGender());
				financingDataSave.setDependents(postRegisterFinancingRequestDTO.getClientDTO().getDependents());
				financingDataSave
						.setIsBancomerCustomer(postRegisterFinancingRequestDTO.getClientDTO().getIsBancomerCustomer());
				financingDataSave.setRfc(postRegisterFinancingRequestDTO.getClientDTO().getRfc());
				financingDataSave.setCreditStatus(postRegisterFinancingRequestDTO.getClientDTO().getCreditStatus());
				financingDataSave.setCheckpointId(postRegisterFinancingRequestDTO.getClientDTO().getCheckpointId());
				financingDataSave.setSellCarDetail(sellCarDetailBD.get());
				financingDataSave.setHookMount(postRegisterFinancingRequestDTO.getClientDTO().getHookAmount());
				financingDataSave
						.setFinancingAmount(postRegisterFinancingRequestDTO.getClientDTO().getFinancingAmount());
				financingDataSave
						.setAmount(postRegisterFinancingRequestDTO.getClientDTO().getFinancingPerMonth().getAmount());
				financingDataSave
						.setMonth(postRegisterFinancingRequestDTO.getClientDTO().getFinancingPerMonth().getMonth());
				financingDataSave.setCurrentDate(new Timestamp(System.currentTimeMillis()));

				if (postRegisterFinancingRequestDTO.getClientDTO().getBirthday() != null
						|| postRegisterFinancingRequestDTO.getClientDTO().getBirthday().length() != 0) {
					try {
						financingDataSave
								.setBirthday(sdf.parse(postRegisterFinancingRequestDTO.getClientDTO().getBirthday()));
					} catch (ParseException e) {

						LogService.logger.info("El formato de la fecha es invalido..");
						MessageDTO messagenoExitUser = messageRepo.findByCode(MessageEnum.M0030.toString()).getDTO();
						listMessageDTO.add(messagenoExitUser);
						responseDTO.setListMessage(listMessageDTO);

						EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
								.getByCode(EndPointCodeResponseEnum.C0400.toString());
						responseDTO.setCode(enumResult.getValue());
						responseDTO.setStatus(enumResult.getStatus());
						responseDTO.setData(new ArrayList<>());
						return responseDTO;

					}
				}

				financingDataSave.setDelay(postRegisterFinancingRequestDTO.getAdditionalDTO().getDelay());
				financingDataSave.setCarUse(postRegisterFinancingRequestDTO.getAdditionalDTO().getCarUse());
				financingDataSave.setIncome(postRegisterFinancingRequestDTO.getAdditionalDTO().getIncome());

				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getMortgageCredit().equals("Y")) {
					financingDataSave.setMortgageCredit("si");
				}
				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getMortgageCredit().equals("N")) {
					financingDataSave.setMortgageCredit("no");
				}

				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getAutomotiveCredit().equals("Y")) {
					financingDataSave.setAutomotiveCredit("si");
				}
				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getAutomotiveCredit().equals("N")){
					financingDataSave.setAutomotiveCredit("no");
				}

				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getCreditCard().equals("Y")) {
					financingDataSave.setCreditCard("si");
				}
				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getCreditCard().equals("N")) {
					financingDataSave.setCreditCard("no");
				}


				financingDataSave
						.setCreditCardLimit(postRegisterFinancingRequestDTO.getAdditionalDTO().getCreditCardLimit());
				financingDataSave
						.setCreditCardDigits(postRegisterFinancingRequestDTO.getAdditionalDTO().getCreditCardDigits());
				financingDataSave.setActivo(Constants.ACTIVE_DATA);
				financingDataSave.setSteps("00000");

				financingDataSave.setPropertyStatusId(
						postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getPropertyStatusId());

				financingDataSave
						.setYearsInHouse(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getYearsInHouse());
				financingDataSave.setColony(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getColony());
				financingDataSave
						.setDelegation(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getDelegation());
				financingDataSave.setCity(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getCity());
				financingDataSave.setState(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getState());

				financingDataSave.setStateJob(postRegisterFinancingRequestDTO.getJobDTO().getState());
				financingDataSave.setCompany(postRegisterFinancingRequestDTO.getJobDTO().getCompany());
				financingDataSave.setStreet(postRegisterFinancingRequestDTO.getJobDTO().getStreet());
				financingDataSave.setExteriorNumber(postRegisterFinancingRequestDTO.getJobDTO().getExteriorNumber());
				financingDataSave.setInteriorNumber(postRegisterFinancingRequestDTO.getJobDTO().getInteriorNumber());
				financingDataSave.setPostalCode(postRegisterFinancingRequestDTO.getJobDTO().getPostalCode());
				financingDataSave.setColonyJob(postRegisterFinancingRequestDTO.getJobDTO().getColony());
				financingDataSave.setDelegationJob(postRegisterFinancingRequestDTO.getJobDTO().getDelegation());
				financingDataSave.setCityJob(postRegisterFinancingRequestDTO.getJobDTO().getCity());
				financingDataSave.setState(postRegisterFinancingRequestDTO.getJobDTO().getState());
				financingDataSave.setPhoneJob(postRegisterFinancingRequestDTO.getJobDTO().getPhone());
				financingDataSave
						.setEconomicActivity(postRegisterFinancingRequestDTO.getJobDTO().getEconomicActivity());
				financingDataSave.setCompanyType(postRegisterFinancingRequestDTO.getJobDTO().getCompanyType());
				financingDataSave.setMontlyIncome(postRegisterFinancingRequestDTO.getJobDTO().getMontlyIncome());
				financingDataSave.setVariableIncome(postRegisterFinancingRequestDTO.getJobDTO().getVariableIncome());
				financingDataSave.setPosition(postRegisterFinancingRequestDTO.getJobDTO().getPosition());
				financingDataSave.setYearOld(postRegisterFinancingRequestDTO.getJobDTO().getYearOld());
				financingDataSave.setMonthOld(postRegisterFinancingRequestDTO.getJobDTO().getMonthOld());
				financingDataSave.setProfession(postRegisterFinancingRequestDTO.getJobDTO().getProfession());
				financingDataSave
						.setInsuranceAffiliation(postRegisterFinancingRequestDTO.getJobDTO().getInsuranceAffiliation());

				if(postRegisterFinancingRequestDTO.getPreviousJobDTO() != null) {
					financingDataSave.setCompanyPreviousJob(postRegisterFinancingRequestDTO.getPreviousJobDTO().getCompanyPreviousJob());
					financingDataSave.setYearsWorked(postRegisterFinancingRequestDTO.getPreviousJobDTO().getYearOld());
					financingDataSave.setMonthsWorked(postRegisterFinancingRequestDTO.getPreviousJobDTO().getMonthsWorked());
					financingDataSave.setCompanyPhone(postRegisterFinancingRequestDTO.getPreviousJobDTO().getCompanyPhone());
				}

				FinancingData financingDataNew = financingDataRepo.save(financingDataSave);

				postRegisterFinancingResponseDTO.setId(financingDataNew.getId());
				postRegisterFinancingResponseDTO.setUrlPdf(
						Constants.PROPERTY_FINANCING_URL_PDF + client.getUserId() + "/" + financingDataNew.getId());

				EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
						.getByCode(EndPointCodeResponseEnum.C0201.toString());
				responseDTO.setCode(enumResult.getValue());
				responseDTO.setStatus(enumResult.getStatus());
				responseDTO.setData(postRegisterFinancingResponseDTO);

			} else {

				LogService.logger.info("Es un registro que ya existe, se procede a actualizar los datos..");

				LogService.logger.info("Se esta registrando la información de domicilio del cliente..");

				String metaValue = null;

				if (domicile.getStreet() != null  && domicile.getExteriorNumber() != null && domicile.getPostalCode() != null
						&& domicile.getStreet().length() != 0  && domicile.getExteriorNumber().length() != 0 && domicile.getPostalCode().length() != 0) {

					metaValue = (domicile.getStreet() + "#" + domicile.getExteriorNumber() + "#" + (domicile.getInteriorNumber()==null?"#":domicile.getInteriorNumber()+"#") + domicile.getPostalCode());
				}
				List<UserMeta> userMetaUpdateBD = userMetaRepo.findByMetaKeyAndUserIdOrderById(Constants.META_VALUE_ADDRESS_FINANCING,ExistFinancingData.getUser().getId());

				if (userMetaUpdateBD != null && metaValue != null) {

					LogService.logger.info("Se realiza Update de direccion financiamiento..");
					userMetaUpdateBD.get(0).setMetaValue(metaValue);
					UserMeta saveMetaUpdateDomicile = userMetaRepo.save(userMetaUpdateBD.get(0));

					ExistFinancingData.setAnddressId(saveMetaUpdateDomicile.getId());

				} else if (userMetaUpdateBD == null && metaValue != null ){

					LogService.logger.info("Se realiza un registro nuevo de direccion financiamiento..");
					saveMetaDomicile.setUser(userBD.get());
					saveMetaDomicile.setMetaKey(Constants.META_VALUE_ADDRESS_FINANCING);
					saveMetaDomicile.setMetaValue(metaValue);

					UserMeta saveMetaNewDomicile = userMetaRepo.save(saveMetaDomicile);

					ExistFinancingData.setAnddressId(saveMetaNewDomicile.getId());
				}

				List<UserMeta> userMetaUpdateLinePhone = userMetaRepo.findByMetaKeyAndUserIdOrderById(Constants.META_VALUE_LANDLINE_PHONE_FINANCING,ExistFinancingData.getUser().getId());

				if (userMetaUpdateLinePhone != null && client.getHomePhone() != null){

					LogService.logger.info("Se realiza update del numero local..");
					userMetaUpdateLinePhone.get(0).setMetaValue(client.getHomePhone());
					userMetaRepo.save(userMetaUpdateLinePhone.get(0));

				}else if (userMetaUpdateLinePhone == null &&  client.getHomePhone() != null){

					LogService.logger.info("Se realiza un registro nuevo del numero local..");
					saveMetaLandLinePhone.setUser(userBD.get());
					saveMetaLandLinePhone.setMetaKey(Constants.META_VALUE_LANDLINE_PHONE_FINANCING);
					saveMetaLandLinePhone.setMetaValue(client.getMobilePhone());
					userMetaRepo.save(saveMetaLandLinePhone);
				}

				List<UserMeta> userMetaUpdatePhone = userMetaRepo.findByMetaKeyAndUserIdOrderById(Constants.META_VALUE_PHONE_FINANCING,ExistFinancingData.getUser().getId());

				if(userMetaUpdatePhone != null && client.getMobilePhone() != null){

					LogService.logger.info("Se realiza update del telefono..");
					userMetaUpdatePhone.get(0).setMetaValue(client.getMobilePhone());
					userMetaRepo.save(userMetaUpdatePhone.get(0));

				}else if (userMetaUpdatePhone == null && client.getMobilePhone() != null){

					LogService.logger.info("Se realiza un registro nuevo del telefono..");
					saveMetaPhone.setUser(userBD.get());
					saveMetaPhone.setMetaKey(Constants.META_VALUE_PHONE_FINANCING);
					saveMetaPhone.setMetaValue(client.getMobilePhone());
					userMetaRepo.save(saveMetaPhone);

				}

				if(references.getMetaValueReferenceFamilyDTO() != null) {

					LogService.logger.info("Se esta registrando la referencia familiar del cliente..");
					saveFinancingDataFamily.setName(references.getMetaValueReferenceFamilyDTO().getName());
					saveFinancingDataFamily
							.setFirstLastname(references.getMetaValueReferenceFamilyDTO().getFirstLastname());
					saveFinancingDataFamily
							.setSecondLastname(references.getMetaValueReferenceFamilyDTO().getSecondLastname());
					saveFinancingDataFamily.setPhone(references.getMetaValueReferenceFamilyDTO().getPhone());
					saveFinancingDataFamily.setAddress(references.getMetaValueReferenceFamilyDTO().getAddress());
					saveFinancingDataFamily.setRelationship(references.getMetaValueReferenceFamilyDTO().getRelationship());
					saveFinancingDataFamily.setTypeReference(Constants.META_VALUE_TYPE_REFERENCE_FAMILY);

					FinancingReferece referenceFamily = financingReferenceRepo.save(saveFinancingDataFamily);

					ExistFinancingData.setReferenceFamilyId(referenceFamily.getId());
				}

				if(references.getMetaValueReferencePersonalDTO() != null) {

					LogService.logger.info("Se esta registrando la referencia personal del cliente..");
					saveFinancingDataPersonal.setName(references.getMetaValueReferencePersonalDTO().getName());
					saveFinancingDataPersonal
							.setFirstLastname(references.getMetaValueReferencePersonalDTO().getFirstLastname());
					saveFinancingDataPersonal
							.setSecondLastname(references.getMetaValueReferencePersonalDTO().getSecondLastname());
					saveFinancingDataPersonal.setPhone(references.getMetaValueReferencePersonalDTO().getPhone());
					saveFinancingDataPersonal.setAddress(references.getMetaValueReferencePersonalDTO().getAddress());
					saveFinancingDataPersonal
							.setRelationship(references.getMetaValueReferencePersonalDTO().getRelationship());
					saveFinancingDataPersonal.setTypeReference(Constants.META_VALUE_TYPE_REFERENCE_PERSONAL);

					FinancingReferece referencePersonal = financingReferenceRepo.save(saveFinancingDataPersonal);

					ExistFinancingData.setReferencePersonalId(referencePersonal.getId());

				}

				if(references.getMetaValueReferenceLessorDTO() != null) {

					LogService.logger.info("Se esta registrando la referencia arrendador del cliente..");
					saveFinancingDataLessor.setName(references.getMetaValueReferenceLessorDTO().getName());
					saveFinancingDataLessor
							.setFirstLastname(references.getMetaValueReferenceLessorDTO().getFirstLastname());
					saveFinancingDataLessor
							.setSecondLastname(references.getMetaValueReferenceLessorDTO().getSecondLastname());
					saveFinancingDataLessor.setPhone(references.getMetaValueReferenceLessorDTO().getPhone());
					saveFinancingDataLessor.setAddress(references.getMetaValueReferenceLessorDTO().getAddress());
					saveFinancingDataLessor.setTypeReference(Constants.META_VALUE_TYPE_REFERENCE_PERSONAL);

					FinancingReferece referenceLessor = financingReferenceRepo.save(saveFinancingDataLessor);

					ExistFinancingData.setReferencePersonalId2(referenceLessor.getId());

				}

				LogService.logger.info("Se esta registrando los datos de Financiamiento del cliente..");

				ExistFinancingData.setUser(userBD.get());
				ExistFinancingData.setPersonType(postRegisterFinancingRequestDTO.getClientDTO().getPersonType());
				ExistFinancingData
						.setIdentificationType(postRegisterFinancingRequestDTO.getClientDTO().getIdentificationType());
				ExistFinancingData.setIdNumber(postRegisterFinancingRequestDTO.getClientDTO().getIdNumber());
				ExistFinancingData.setElectorKey(postRegisterFinancingRequestDTO.getClientDTO().getElectorKey());

				ExistFinancingData
						.setEducationLevel(postRegisterFinancingRequestDTO.getClientDTO().getEducationLevel());
				ExistFinancingData
						.setMaritalStatusId(postRegisterFinancingRequestDTO.getClientDTO().getMaritalStatusId());
				ExistFinancingData.setNationalityId(postRegisterFinancingRequestDTO.getClientDTO().getNationalityId());

				ExistFinancingData.setGender(postRegisterFinancingRequestDTO.getClientDTO().getGender());
				ExistFinancingData.setDependents(postRegisterFinancingRequestDTO.getClientDTO().getDependents());
				ExistFinancingData
						.setIsBancomerCustomer(postRegisterFinancingRequestDTO.getClientDTO().getIsBancomerCustomer());
				ExistFinancingData.setRfc(postRegisterFinancingRequestDTO.getClientDTO().getRfc());
				ExistFinancingData.setCreditStatus(postRegisterFinancingRequestDTO.getClientDTO().getCreditStatus());
				ExistFinancingData.setCheckpointId(postRegisterFinancingRequestDTO.getClientDTO().getCheckpointId());
				ExistFinancingData.setSellCarDetail(sellCarDetailBD.get());
				ExistFinancingData.setHookMount(postRegisterFinancingRequestDTO.getClientDTO().getHookAmount());
				ExistFinancingData
						.setFinancingAmount(postRegisterFinancingRequestDTO.getClientDTO().getFinancingAmount());
				ExistFinancingData
						.setAmount(postRegisterFinancingRequestDTO.getClientDTO().getFinancingPerMonth().getAmount());
				ExistFinancingData
						.setMonth(postRegisterFinancingRequestDTO.getClientDTO().getFinancingPerMonth().getMonth());
				ExistFinancingData.setCurrentDate(new Timestamp(System.currentTimeMillis()));

				if (postRegisterFinancingRequestDTO.getClientDTO().getBirthday() != null
						|| postRegisterFinancingRequestDTO.getClientDTO().getBirthday().length() != 0) {
					try {
						ExistFinancingData
								.setBirthday(sdf.parse(postRegisterFinancingRequestDTO.getClientDTO().getBirthday()));
					} catch (ParseException e) {

						LogService.logger.info("El formato de la fecha es invalido..");
						MessageDTO messagenoExitUser = messageRepo.findByCode(MessageEnum.M0030.toString()).getDTO();
						listMessageDTO.add(messagenoExitUser);
						responseDTO.setListMessage(listMessageDTO);

						EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
								.getByCode(EndPointCodeResponseEnum.C0400.toString());
						responseDTO.setCode(enumResult.getValue());
						responseDTO.setStatus(enumResult.getStatus());
						responseDTO.setData(new ArrayList<>());
						return responseDTO;

					}
				}

				ExistFinancingData.setDelay(postRegisterFinancingRequestDTO.getAdditionalDTO().getDelay());
				ExistFinancingData.setCarUse(postRegisterFinancingRequestDTO.getAdditionalDTO().getCarUse());
				ExistFinancingData.setIncome(postRegisterFinancingRequestDTO.getAdditionalDTO().getIncome());

				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getMortgageCredit().equals("si")) {
					ExistFinancingData.setMortgageCredit("Y");
				}
				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getMortgageCredit().equals("no")) {
					ExistFinancingData.setMortgageCredit("N");
				}

				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getAutomotiveCredit().equals("si")) {
					ExistFinancingData.setAutomotiveCredit("Y");
				}
				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getAutomotiveCredit().equals("no")) {
					ExistFinancingData.setAutomotiveCredit("N");
				}

				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getCreditCard().equals("si")) {
					ExistFinancingData.setCreditCard("Y");
				}
				if (postRegisterFinancingRequestDTO.getAdditionalDTO().getCreditCard().equals("no")) {
					ExistFinancingData.setCreditCard("N");
				}

				ExistFinancingData
						.setCreditCardLimit(postRegisterFinancingRequestDTO.getAdditionalDTO().getCreditCardLimit());
				ExistFinancingData
						.setCreditCardDigits(postRegisterFinancingRequestDTO.getAdditionalDTO().getCreditCardDigits());
				ExistFinancingData.setActivo(Constants.ACTIVE_DATA);
				ExistFinancingData.setSteps("00000");

				ExistFinancingData.setPropertyStatusId(
						postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getPropertyStatusId());

				ExistFinancingData
						.setYearsInHouse(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getYearsInHouse());
				ExistFinancingData.setColony(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getColony());
				ExistFinancingData
						.setDelegation(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getDelegation());
				ExistFinancingData.setCity(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getCity());
				ExistFinancingData.setState(postRegisterFinancingRequestDTO.getMetaValueDomicileDTO().getState());

				ExistFinancingData.setStateJob(postRegisterFinancingRequestDTO.getJobDTO().getState());
				ExistFinancingData.setCompany(postRegisterFinancingRequestDTO.getJobDTO().getCompany());
				ExistFinancingData.setStreet(postRegisterFinancingRequestDTO.getJobDTO().getStreet());
				ExistFinancingData.setExteriorNumber(postRegisterFinancingRequestDTO.getJobDTO().getExteriorNumber());
				ExistFinancingData.setInteriorNumber(postRegisterFinancingRequestDTO.getJobDTO().getInteriorNumber());
				ExistFinancingData.setPostalCode(postRegisterFinancingRequestDTO.getJobDTO().getPostalCode());
				ExistFinancingData.setColonyJob(postRegisterFinancingRequestDTO.getJobDTO().getColony());
				ExistFinancingData.setDelegationJob(postRegisterFinancingRequestDTO.getJobDTO().getDelegation());
				ExistFinancingData.setCityJob(postRegisterFinancingRequestDTO.getJobDTO().getCity());
				ExistFinancingData.setState(postRegisterFinancingRequestDTO.getJobDTO().getState());
				ExistFinancingData.setPhoneJob(postRegisterFinancingRequestDTO.getJobDTO().getPhone());
				ExistFinancingData
						.setEconomicActivity(postRegisterFinancingRequestDTO.getJobDTO().getEconomicActivity());
				ExistFinancingData.setCompanyType(postRegisterFinancingRequestDTO.getJobDTO().getCompanyType());
				ExistFinancingData.setMontlyIncome(postRegisterFinancingRequestDTO.getJobDTO().getMontlyIncome());
				ExistFinancingData.setVariableIncome(postRegisterFinancingRequestDTO.getJobDTO().getVariableIncome());
				ExistFinancingData.setPosition(postRegisterFinancingRequestDTO.getJobDTO().getPosition());
				ExistFinancingData.setYearOld(postRegisterFinancingRequestDTO.getJobDTO().getYearOld());
				ExistFinancingData.setMonthOld(postRegisterFinancingRequestDTO.getJobDTO().getMonthOld());
				ExistFinancingData.setProfession(postRegisterFinancingRequestDTO.getJobDTO().getProfession());
				ExistFinancingData
						.setInsuranceAffiliation(postRegisterFinancingRequestDTO.getJobDTO().getInsuranceAffiliation());

				if(postRegisterFinancingRequestDTO.getPreviousJobDTO() != null) {
					ExistFinancingData.setCompanyPreviousJob(
							postRegisterFinancingRequestDTO.getPreviousJobDTO().getCompanyPreviousJob());
					ExistFinancingData
							.setYearsWorked(postRegisterFinancingRequestDTO.getPreviousJobDTO().getYearOld());
					ExistFinancingData

							.setMonthsWorked(postRegisterFinancingRequestDTO.getPreviousJobDTO().getMonthsWorked());
					ExistFinancingData
							.setCompanyPhone(postRegisterFinancingRequestDTO.getPreviousJobDTO().getCompanyPhone());
				}

				FinancingData financingDataUpdate = financingDataRepo.save(ExistFinancingData);

				postRegisterFinancingResponseDTO.setId(financingDataUpdate.getId());
				postRegisterFinancingResponseDTO.setUrlPdf(
						Constants.PROPERTY_FINANCING_URL_PDF + client.getUserId() + "/" + financingDataUpdate.getId());

				EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
						.getByCode(EndPointCodeResponseEnum.C0200.toString());
				responseDTO.setCode(enumResult.getValue());
				responseDTO.setStatus(enumResult.getStatus());
				responseDTO.setData(postRegisterFinancingResponseDTO);

			}
		}

		return responseDTO;
	}

	/**
	 * Metodo que consulta la informacion de un Registro de Financiamiento
	 * @author Oscar Montilla
	 * @param financingId Identificador de registro de Financiamiento
	 * @return ResponseDTO Response generico de Servicio
	 */
	public ResponseDTO getFinancingForm(Long financingId){

		ResponseDTO responseDTO = new ResponseDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		if (financingId == null) {

			LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente...");
			MessageDTO messagenoExitUser = messageRepo.findByCode(MessageEnum.M0021.toString())
					.getRequestDTO(" " + (financingId == null?"financing_id, ":""));
			listMessageDTO.add(messagenoExitUser);
			responseDTO.setListMessage(listMessageDTO);
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
					.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new ArrayList<>());

			return responseDTO;

		}

		Optional<FinancingData> financingDataBD = financingDataRepo.findById(financingId);


		if (!financingDataBD.isPresent()){

			LogService.logger.info("El registro financing_id no existe en Base de datos...");
			MessageDTO messagenoExitUser = messageRepo.findByCode(MessageEnum.M0027.toString()).getDTO();
			listMessageDTO.add(messagenoExitUser);
			responseDTO.setListMessage(listMessageDTO);
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
					.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new ArrayList<>());

			return responseDTO;

		}

		RegisterFinancingRequestDTO RegisterFinancingRequestDTO = new RegisterFinancingRequestDTO();

		ClientDTO clientDTO = new ClientDTO();
		MetaValueDomicileDTO metaValueDomicileDTO = new MetaValueDomicileDTO();
		JobDTO jobDTO = new JobDTO();
		PreviousJobDTO previousJobDTO = new PreviousJobDTO();
		MetaValueReferenceDTO metaValueReferenceDTO = new MetaValueReferenceDTO();
		MetaValueReferenceFamilyDTO metaValueReferenceFamilyDTO = new MetaValueReferenceFamilyDTO();
		MetaValueReferencePersonalDTO metaValueReferencePersonalDTO = new MetaValueReferencePersonalDTO();
		MetaValueReferenceLessorDTO metaValueReferenceLessorDTO = new MetaValueReferenceLessorDTO();
		AdditionalDTO additionalDTO = new AdditionalDTO();

		FinancingPerMonthDTO financingPerMonthDTO = new FinancingPerMonthDTO();
		financingPerMonthDTO.setAmount(financingDataBD.get().getAmount());
		financingPerMonthDTO.setMonth(financingDataBD.get().getMonth());

		List<UserMeta> userMetaLandLine = userMetaRepo.findByMetaKeyAndUserIdOrderById(Constants.META_VALUE_LANDLINE_PHONE_FINANCING,financingDataBD.get().getUser().getId());
		List<UserMeta> userMetaPhone = userMetaRepo.findByMetaKeyAndUserIdOrderById(Constants.META_VALUE_PHONE_FINANCING,financingDataBD.get().getUser().getId());
		List<UserMeta> userMetaAddress = userMetaRepo.findByMetaKeyAndUserIdOrderById(Constants.META_VALUE_ADDRESS_FINANCING,financingDataBD.get().getUser().getId());

		if (financingDataBD.get().getReferenceFamilyId() != null) {

			Optional<FinancingReferece> referenceFamily = financingReferenceRepo.findById(financingDataBD.get().getReferenceFamilyId());

			LogService.logger.info("Se encuentra una Referencia Familiar...");
			metaValueReferenceFamilyDTO.setName(referenceFamily.get().getName());
			metaValueReferenceFamilyDTO.setFirstLastname(referenceFamily.get().getFirstLastname());
			metaValueReferenceFamilyDTO.setSecondLastname(referenceFamily.get().getSecondLastname());
			metaValueReferenceFamilyDTO.setPhone(referenceFamily.get().getPhone());
			metaValueReferenceFamilyDTO.setAddress(referenceFamily.get().getAddress());
			metaValueReferenceFamilyDTO.setRelationship(referenceFamily.get().getRelationship());

		}
		if (financingDataBD.get().getReferencePersonalId() != null) {

			Optional<FinancingReferece> referencePersonal = financingReferenceRepo.findById(financingDataBD.get().getReferencePersonalId());

			LogService.logger.info("Se encuentra una Referencia Personal...");
			metaValueReferencePersonalDTO.setName(referencePersonal.get().getName());
			metaValueReferencePersonalDTO.setFirstLastname(referencePersonal.get().getFirstLastname());
			metaValueReferencePersonalDTO.setSecondLastname(referencePersonal.get().getSecondLastname());
			metaValueReferencePersonalDTO.setPhone(referencePersonal.get().getPhone());
			metaValueReferencePersonalDTO.setAddress(referencePersonal.get().getAddress());
			metaValueReferencePersonalDTO.setRelationship(referencePersonal.get().getRelationship());
		}
		if (financingDataBD.get().getReferencePersonalId2() != null) {

			Optional<FinancingReferece> referencePersonal2 = financingReferenceRepo.findById(financingDataBD.get().getReferencePersonalId2());

			LogService.logger.info("Se encuentra una segunda Referencia Personal...");
			metaValueReferenceLessorDTO.setName(referencePersonal2.get().getName());
			metaValueReferenceLessorDTO.setFirstLastname(referencePersonal2.get().getFirstLastname());
			metaValueReferenceLessorDTO.setSecondLastname(referencePersonal2.get().getSecondLastname());
			metaValueReferenceLessorDTO.setPhone(referencePersonal2.get().getPhone());
			metaValueReferenceLessorDTO.setAddress(referencePersonal2.get().getAddress());
			metaValueReferenceLessorDTO.setRelationship(referencePersonal2.get().getRelationship());

		}

		clientDTO.setUserId(financingDataBD.get().getUser().getId());
		clientDTO.setCarId(financingDataBD.get().getSellCarDetail().getId());
		clientDTO.setCheckpointId(financingDataBD.get().getCheckpointId());
		clientDTO.setHookAmount(financingDataBD.get().getHookMount());
		clientDTO.setFinancingAmount(financingDataBD.get().getFinancingAmount());
		clientDTO.setFinancingPerMonth(financingPerMonthDTO);
		clientDTO.setCreditStatus(financingDataBD.get().getCreditStatus());
		clientDTO.setPersonType(financingDataBD.get().getPersonType());
		clientDTO.setBirthday(sdf.format(financingDataBD.get().getBirthday()));

		int countSplitName = financingDataBD.get().getUser().getName().split("(?=\\s)").length;

		if (countSplitName > 0) {

			clientDTO.setName(financingDataBD.get().getUser().getName().split("(?=\\s)")[0]);
		}
		if (countSplitName > 1) {

			clientDTO.setFirstLastname(financingDataBD.get().getUser().getName().split("(?=\\s)")[1]);
		}
		if (countSplitName > 2) {

			clientDTO.setSecondLastname(financingDataBD.get().getUser().getName().split("(?=\\s)")[2]);
		}
		clientDTO.setEmail(financingDataBD.get().getUser().getEmail());
		clientDTO.setHomePhone(userMetaLandLine.get(0).getMetaValue());
		clientDTO.setMobilePhone(userMetaPhone.get(0).getMetaValue());
		clientDTO.setIdentificationType(financingDataBD.get().getIdentificationType());
		clientDTO.setElectorKey(financingDataBD.get().getElectorKey());
		clientDTO.setIdNumber(financingDataBD.get().getIdNumber());
		clientDTO.setEducationLevel(financingDataBD.get().getEducationLevel());
		clientDTO.setMaritalStatusId(financingDataBD.get().getMaritalStatusId());
		clientDTO.setNationalityId(financingDataBD.get().getNationalityId());
		clientDTO.setGender(financingDataBD.get().getGender());
		clientDTO.setDependents(financingDataBD.get().getDependents());
		clientDTO.setIsBancomerCustomer(financingDataBD.get().getIsBancomerCustomer());
		clientDTO.setRfc(financingDataBD.get().getRfc());

		metaValueDomicileDTO.setPropertyStatusId(financingDataBD.get().getPropertyStatusId());
		metaValueDomicileDTO.setYearsInHouse(financingDataBD.get().getYearsInHouse());

		int countSplitDomicile = userMetaAddress.get(0).getMetaValue().split("#").length;

		metaValueDomicileDTO.setDelegation(financingDataBD.get().getDelegation());
		metaValueDomicileDTO.setState(financingDataBD.get().getState());
		metaValueDomicileDTO.setCity(financingDataBD.get().getCity());
		metaValueDomicileDTO.setColony(financingDataBD.get().getColony());

		if (countSplitDomicile > 0) {

			metaValueDomicileDTO.setStreet(userMetaAddress.get(0).getMetaValue().split("#")[0]);
		}
		if (countSplitDomicile > 1) {

			metaValueDomicileDTO.setExteriorNumber(userMetaAddress.get(0).getMetaValue().split("#")[1]);
		}
		if (countSplitDomicile > 2) {

			metaValueDomicileDTO.setInteriorNumber(userMetaAddress.get(0).getMetaValue().split("#")[2]);
		}
		if (countSplitDomicile > 3) {

			metaValueDomicileDTO.setPostalCode(userMetaAddress.get(0).getMetaValue().split("#")[3]);
		}
		jobDTO.setCompany(financingDataBD.get().getCompany());
		jobDTO.setCompanyType(financingDataBD.get().getCompanyType());
		jobDTO.setEconomicActivity(financingDataBD.get().getEconomicActivity());
		jobDTO.setPhone(financingDataBD.get().getPhoneJob());
		jobDTO.setStreet(financingDataBD.get().getStreet());
		jobDTO.setExteriorNumber(financingDataBD.get().getExteriorNumber());
		jobDTO.setInteriorNumber(financingDataBD.get().getInteriorNumber());
		jobDTO.setPostalCode(financingDataBD.get().getPostalCode());
		jobDTO.setDelegation(financingDataBD.get().getDelegationJob());
		jobDTO.setState(financingDataBD.get().getStateJob());
		jobDTO.setCity(financingDataBD.get().getCityJob());
		jobDTO.setColony(financingDataBD.get().getColonyJob());
		jobDTO.setMontlyIncome(financingDataBD.get().getMontlyIncome());
		jobDTO.setVariableIncome(financingDataBD.get().getVariableIncome());
		jobDTO.setInsuranceAffiliation(financingDataBD.get().getInsuranceAffiliation());
		jobDTO.setPosition(financingDataBD.get().getPosition());
		jobDTO.setProfession(financingDataBD.get().getProfession());
		jobDTO.setYearOld(financingDataBD.get().getYearOld());
		jobDTO.setMonthOld(financingDataBD.get().getMonth());

		previousJobDTO.setCompanyPreviousJob(financingDataBD.get().getCompanyPreviousJob());
		previousJobDTO.setYearOld(financingDataBD.get().getYearOld());
		previousJobDTO.setMonthsWorked(financingDataBD.get().getMonthsWorked());
		previousJobDTO.setCompanyPhone(financingDataBD.get().getCompanyPhone());

		additionalDTO.setDelay(financingDataBD.get().getDelay());
		additionalDTO.setCarUse(financingDataBD.get().getCarUse());
		additionalDTO.setIncome(financingDataBD.get().getIncome());

		if (financingDataBD.get().getMortgageCredit().equals("si")) {
			additionalDTO.setMortgageCredit("Y");
		}
		if (financingDataBD.get().getMortgageCredit().equals("no")) {
			additionalDTO.setMortgageCredit("N");
		}

		if (financingDataBD.get().getAutomotiveCredit().equals("si")) {
			additionalDTO.setAutomotiveCredit("Y");
		}
		if (financingDataBD.get().getAutomotiveCredit().equals("no")) {
			additionalDTO.setAutomotiveCredit("N");
		}

		if (financingDataBD.get().getCreditCard().equals("si")) {
			additionalDTO.setCreditCard("Y");
		}
		if (financingDataBD.get().getCreditCard().equals("no")) {
			additionalDTO.setCreditCard("N");
		}

		additionalDTO.setCreditCardLimit(financingDataBD.get().getCreditCardLimit());
		additionalDTO.setCreditCardDigits(financingDataBD.get().getCreditCardDigits());

		metaValueReferenceDTO.setMetaValueReferenceFamilyDTO(metaValueReferenceFamilyDTO);
		metaValueReferenceDTO.setMetaValueReferencePersonalDTO(metaValueReferencePersonalDTO);
		metaValueReferenceDTO.setMetaValueReferenceLessorDTO(metaValueReferenceLessorDTO);


		RegisterFinancingRequestDTO.setClientDTO(clientDTO);
		RegisterFinancingRequestDTO.setMetaValueDomicileDTO(metaValueDomicileDTO);
		RegisterFinancingRequestDTO.setJobDTO(jobDTO);
		RegisterFinancingRequestDTO.setPreviousJobDTO(previousJobDTO);
		RegisterFinancingRequestDTO.setMetaValueReferenceDTO(metaValueReferenceDTO);
		RegisterFinancingRequestDTO.setAdditionalDTO(additionalDTO);

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
				.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(RegisterFinancingRequestDTO);


		return responseDTO;
	}


	public ResponseDTO saveCheckoutLog(PutCheckoutRequestDTO putCheckoutRequestDTO){
		ResponseDTO responseDTO = new ResponseDTO();
		//Guardando en la tabla v2_checkout_log
		try {
			CheckoutLog checkoutLog = new CheckoutLog();
			if(putCheckoutRequestDTO.getUserId() != null){
				checkoutLog.setUserId(putCheckoutRequestDTO.getUserId());
			}
			if(putCheckoutRequestDTO.getStep() != null){
				checkoutLog.setStep(putCheckoutRequestDTO.getStep());
			}
			if(putCheckoutRequestDTO.getCarId() != null){
				checkoutLog.setCarId(putCheckoutRequestDTO.getCarId());
			}
			if(putCheckoutRequestDTO.getPaymentType() != null){
				checkoutLog.setPaymentType(putCheckoutRequestDTO.getPaymentType());
			}
			if(putCheckoutRequestDTO.getCheckpointId() != null){
				checkoutLog.setCheckpointId(putCheckoutRequestDTO.getCheckpointId());
			}
			if(putCheckoutRequestDTO.getDeliveryAddressId() != null){
				checkoutLog.setDeliveryAddressId(putCheckoutRequestDTO.getDeliveryAddressId());
			}
			if(putCheckoutRequestDTO.getShipmentCost() != null){
				checkoutLog.setShipmentCost(putCheckoutRequestDTO.getShipmentCost());
			}
			if(putCheckoutRequestDTO.getWarrantyId() != null){
				checkoutLog.setWarrantyId(putCheckoutRequestDTO.getWarrantyId());
			}
			if(putCheckoutRequestDTO.getWarrantyAmount() != null){
				checkoutLog.setWarrantyAmount(putCheckoutRequestDTO.getWarrantyAmount());
			}
			if(putCheckoutRequestDTO.getInsuranceAmount() != null){
				checkoutLog.setInsuranceAmount(putCheckoutRequestDTO.getInsuranceAmount());
			}
			if(putCheckoutRequestDTO.getDeviceSessionId() != null){
				checkoutLog.setDeviceSessionId(putCheckoutRequestDTO.getDeviceSessionId());
			}
			if(putCheckoutRequestDTO.getTokenId() != null){
				checkoutLog.setTokenId(putCheckoutRequestDTO.getTokenId());
			}
			if(putCheckoutRequestDTO.getCarPrice() != null){
				checkoutLog.setCarPrice(putCheckoutRequestDTO.getCarPrice());
			}
			if(putCheckoutRequestDTO.getTotalPrice() != null){
				checkoutLog.setTotalPrice(putCheckoutRequestDTO.getTotalPrice());
			}
			if(putCheckoutRequestDTO.getDownPayment() != null){
				checkoutLog.setDownPayment(putCheckoutRequestDTO.getDownPayment());
			}
			if(putCheckoutRequestDTO.getFinancingMonths() != null){
				checkoutLog.setFinancingMonths(putCheckoutRequestDTO.getFinancingMonths());
			}
			if(putCheckoutRequestDTO.getOfferCheckpointId() != null){
				checkoutLog.setOfferCheckpointId(putCheckoutRequestDTO.getOfferCheckpointId());
			}
			if(putCheckoutRequestDTO.getSource() != null){
				checkoutLog.setSource(putCheckoutRequestDTO.getSource());
			}
			if(putCheckoutRequestDTO.getVersionApp() != null){
				checkoutLog.setVersionApp(putCheckoutRequestDTO.getVersionApp());
			}

			checkoutLogRepo.save(checkoutLog);
		} catch (Exception e) {
			// TODO: handle exception
			LogService.logger.error("Error Catch step 1 guardando en la tabla v2_checkout_log");
		}
		return responseDTO;
	}

}
