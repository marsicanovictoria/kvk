package com.kavak.core.service;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.TrimsDTO;
import com.kavak.core.dto.UserCheckPointsDTO;
import com.kavak.core.dto.request.PostOfferRequestDTO;
import com.kavak.core.dto.request.PutOfferRequestDTO;
import com.kavak.core.dto.response.GetRecoveryOfferResponseStepOneDTO;
import com.kavak.core.dto.response.GetRecuperarOfertaResponseDTO;
import com.kavak.core.dto.response.OffersResponseDTO;
import com.kavak.core.dto.response.PutOfferResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.enumeration.OfferTypeEnum;
import com.kavak.core.enumeration.StatusCheckpointOfferEnum;
import com.kavak.core.enumeration.StatusDetailOfferEnum;
import com.kavak.core.model.CarData;
import com.kavak.core.model.Indication;
import com.kavak.core.model.InspectionLocation;
import com.kavak.core.model.InspectionSchedule;
import com.kavak.core.model.InspectionScheduleBlock;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.OfferCheckpoint;
import com.kavak.core.model.OfferType;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.model.UserTaskAssignmentWeight;
import com.kavak.core.model.ZipCode;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.IndicationRepository;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.InspectionScheduleBlockRepository;
import com.kavak.core.repositories.InspectionScheduleRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.OfferCheckpointRepository;
import com.kavak.core.repositories.OfferTypeRepository;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.repositories.UserTaskAssignmentWeightRepository;
import com.kavak.core.repositories.ZipCodeRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

//import Decoder.BASE64Decoder;

@Stateless
public class BuyCarServices {

    private EndPointCodeResponseEnum enumResult;
    private List<MessageDTO> listMessages;
    private boolean messageValidations;

    @Inject
    private CarDataRepository carDataRepo;

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private ZipCodeRepository costoEnvioCodigoPostalRepo;

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private OfferCheckpointRepository offerCheckpointRepo;

    @Inject
    private UserRepository userRepo;

    @Inject
    private InspectionLocationRepository inspectionLocationRepo;

    @Inject
    private IndicationRepository indicationRepo;

    @Inject
    private InspectionScheduleRepository inspectionScheduleRepo;

    @Inject
    private InspectionScheduleBlockRepository inspectionScheduleBlockRepo;

    @Inject
    private UserTaskAssignmentWeightRepository userTaskAssignmentWeightRepo;

    @Inject
    private OfferTypeRepository offerTypeRepo;
    @Inject
    private UserMetaRepository userMetaRepo;

    public ResponseDTO getCarYears() {
	ResponseDTO responseListDTO = new ResponseDTO();
	List<Integer> listCarYears = new ArrayList<>();
	listCarYears = carDataRepo.findCarYears();
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseListDTO.setCode(enumResult.getValue());
	responseListDTO.setStatus(enumResult.getStatus());
	responseListDTO.setData(listCarYears);
	return responseListDTO;
    }

    public ResponseDTO getCarMake(Long year) {
	ResponseDTO responseListDTO = new ResponseDTO();
	List<String> listCarMakesYear = new ArrayList<>();
	listCarMakesYear = carDataRepo.findCarMakesByYear(year);
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseListDTO.setCode(enumResult.getValue());
	responseListDTO.setStatus(enumResult.getStatus());
	responseListDTO.setData(listCarMakesYear);
	return responseListDTO;
    }

    public ResponseDTO getCarModel(Long year, String make) {
	ResponseDTO responseListDTO = new ResponseDTO();
	List<String> listModels = new ArrayList<>();
	listModels = carDataRepo.findModelsByYearAndMake(year, make);
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseListDTO.setCode(enumResult.getValue());
	responseListDTO.setStatus(enumResult.getStatus());
	responseListDTO.setData(listModels);
	return responseListDTO;
    }

    public ResponseDTO getCarTrims(Long year, String make, String model) {
	ResponseDTO responseDTO = new ResponseDTO();
	String separator = " | ";
	List<TrimsDTO> listTrimsDTO = new ArrayList<>();
	List<CarData> listCarData = carDataRepo.findCarTrimsByCarYearAndCarMakeAndCarModelOrderByCarTrim(year, make, model);
	List<TrimsDTO> listTrimsResponse = new ArrayList<>();

	for (CarData carDataActual : listCarData) {
	    TrimsDTO trim = new TrimsDTO();
	    trim.setSku(carDataActual.getSku());
	    trim.setTrim(carDataActual.getCarTrim());
	    StringBuilder sb = new StringBuilder();
	    if (!String.valueOf(carDataActual.getDoors()).equalsIgnoreCase("NA")) {
		sb.append("Puertas: " + carDataActual.getDoors() + separator);
	    }
	    if (!carDataActual.getTrasmissions().equalsIgnoreCase("NA")) {
		sb.append("Transmisión: " + carDataActual.getTrasmissions() + separator);
	    }
	    if (!carDataActual.getTraction().equalsIgnoreCase("NA")) {
		sb.append("Tracción: " + carDataActual.getTraction() + separator);
	    }
	    if (!carDataActual.getSeats().equalsIgnoreCase("NA")) {
		sb.append("Asientos: " + carDataActual.getSeats() + separator);
	    }
	    if (!carDataActual.getRim().equalsIgnoreCase("NA")) {
		sb.append("Rines: " + carDataActual.getRim() + separator);
	    }
	    if (!carDataActual.getTurbo().equalsIgnoreCase("NA")) {
		sb.append("Turbo: " + carDataActual.getTurbo() + separator);
	    }
	    if (!carDataActual.getCylinder().equalsIgnoreCase("NA")) {
		sb.append("Cilindros: " + carDataActual.getCylinder() + separator);
	    }
	    if (!carDataActual.getLiters().equalsIgnoreCase("NA")) {
		sb.append("Litros: " + carDataActual.getLiters() + separator);
	    }
	    if (!carDataActual.getAir().equalsIgnoreCase("NA")) {
		sb.append("Aire: " + carDataActual.getAir() + separator);
	    }
	    if (!carDataActual.getLights().equalsIgnoreCase("NA")) {
		sb.append("Luces: " + carDataActual.getLights() + separator);
	    }
	    if (!carDataActual.getHp().equalsIgnoreCase("NA")) {
		sb.append("Caballos de Fuerza: " + carDataActual.getHp() + separator);
	    }
	    if (!carDataActual.getSunroof().equalsIgnoreCase("NA")) {
		sb.append("Quemacoco: " + carDataActual.getSunroof() + separator);
	    }
	    if (!carDataActual.getNavigation().equalsIgnoreCase("NA")) {
		sb.append(carDataActual.getNavigation() + separator);
	    }
	    if (!carDataActual.getOthers().equalsIgnoreCase("NA")) {
		sb.append("Otros: " + carDataActual.getOthers());
	    }
	    trim.setFeatures(sb.toString());
	    trim.setFullVersion(carDataActual.getFullVersion());
	    listTrimsDTO.add(trim);
	    listTrimsResponse.add(trim);
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(listTrimsResponse);
	return responseDTO;
    }

    /**
     * Retorna los distintos tipos de oferta acorde a los parametros de entrada.
     *
     * @author Enrique Marin
     * @param postOfferRequestDTO
     *            del carro @Id from @SellCarDetail
     * @return OffersResponseDTO contiene la Lista de ofertas y el id de el ofertaCheckpoint registrado
     */
    public ResponseDTO postOffers(PostOfferRequestDTO postOfferRequestDTO) {
	ResponseDTO responseDTO = new ResponseDTO();
	OffersResponseDTO offersResponse = new OffersResponseDTO();
	offersResponse.setServedZone(true);
	List<GenericDTO> listGenerateOffer = new ArrayList<>();
	listMessages = new ArrayList<>();
	messageValidations = false;
	Long carKmOfferRequest = postOfferRequestDTO.getCarKm();
	CarData carDataBD = carDataRepo.findBySku(postOfferRequestDTO.getSku());
	List<OfferCheckpoint> offersByUserAndCurrenDateDB = offerCheckpointRepo.findByIdUserAndCurrentDate(postOfferRequestDTO.getIdUsuario());
	Long offersByUserLastFiveMinutes = offerCheckpointRepo.findByIdUserAndLastFiveMinutes(postOfferRequestDTO.getIdUsuario());
	MetaValue metaValueBD = metaValueRepo.findByAlias(Constants.OFFERT_OFF_PCOFFERSON);
	
//	if (postOfferRequestDTO.getVersion() == 1L && (postOfferRequestDTO.getSource() == null || postOfferRequestDTO.getSource().equals(Constants.SOURCE_APOLO))) {
//	    MessageDTO message = messageRepo.findByCode(MessageEnum.M0085.toString()).getDTO();
//	    listMessages.add(message);
//	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
//	    responseDTO.setCode(enumResult.getValue());
//	    responseDTO.setStatus(enumResult.getStatus());
//	    responseDTO.setListMessage(listMessages);
//	    responseDTO.setData(new ArrayList<>());
//	    return responseDTO;
//	}
	
	if (!metaValueBD.getActive()) {
	    MessageDTO message = messageRepo.findByCode(MessageEnum.M0084.toString()).getDTO();
	    listMessages.add(message);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(new ArrayList<>());
	    return responseDTO;
	}
	
	if (carDataBD == null) {
	    MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0004.toString()).getDTO();
	    listMessages.add(messageNoRecords);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(new ArrayList<>());
	    return responseDTO;
	}

	// Id Usuario Null
	if (postOfferRequestDTO.getIdUsuario() == null || StringUtils.isEmpty(postOfferRequestDTO.getZipCode())) {
	    MessageDTO message = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
	    listMessages.add(message);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(new ArrayList<>());
	    return responseDTO;
	}

	Optional<User> user = userRepo.findById(postOfferRequestDTO.getIdUsuario());

	// Usuario Bloqueado
	if (user.isPresent() && !user.get().isActive()) {
	    MessageDTO message = messageRepo.findByCode(MessageEnum.M0078.toString()).getDTO();
	    listMessages.add(message);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(new ArrayList<>());
	    return responseDTO;
	}

	// Máximo número de ofertas por día excedido
	if (StringUtils.isEmpty(postOfferRequestDTO.getSource()) || !postOfferRequestDTO.getSource().equals(Constants.SOURCE_PRICING)) {
	    if (offersByUserAndCurrenDateDB != null && (offersByUserAndCurrenDateDB.size() >= 10)) {
		MessageDTO message = messageRepo.findByCode(MessageEnum.M0066.toString()).getDTO();
		listMessages.add(message);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setListMessage(listMessages);
		responseDTO.setData(new ArrayList<>());
		return responseDTO;
	    }
	}

	// Máximo número de ofertas por día excedido, o muchas ofertas en pocos minutos
	if (StringUtils.isEmpty(postOfferRequestDTO.getSource()) || !postOfferRequestDTO.getSource().equals(Constants.SOURCE_PRICING)) {
	    if (offersByUserLastFiveMinutes != null && offersByUserLastFiveMinutes >= 3) {
		MessageDTO message = messageRepo.findByCode(MessageEnum.M0074.toString()).getDTO();
		listMessages.add(message);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setListMessage(listMessages);
		responseDTO.setData(new ArrayList<>());
		return responseDTO;
	    }
	}

	// ========================= >> Guardado Checkpoint Base
	MetaValue metaValueZoneVariousOffers = metaValueRepo.findByAlias(OfferTypeEnum.VARIOUSOFFERS.getAlias());
	String patternNumber = "$###,###,###";
	NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	DecimalFormat decimalFormat = (DecimalFormat) nf;
	decimalFormat.applyPattern(patternNumber);

	OfferCheckpoint newOfferCheckpoint = new OfferCheckpoint();
	newOfferCheckpoint.setIdExteriorColor(postOfferRequestDTO.getIdExteriorColor());
	newOfferCheckpoint.setMinKm(carDataBD.getMinKm());
	newOfferCheckpoint.setMaxKm(carDataBD.getMaxKm());

	newOfferCheckpoint.setUser(user.get());

	// Nombre y Apellido
	newOfferCheckpoint.setCustomerName(user.get().getDTO().getFirstName());
	newOfferCheckpoint.setCustomerLastName(user.get().getDTO().getLastName());

	newOfferCheckpoint.setEmail(user.get().getEmail());
	newOfferCheckpoint.setSku(postOfferRequestDTO.getSku());
	newOfferCheckpoint.setCarYear(carDataBD.getCarYear());
	newOfferCheckpoint.setCarMake(carDataBD.getCarMake());
	newOfferCheckpoint.setCarModel(carDataBD.getCarModel());
	newOfferCheckpoint.setCarVersion(carDataBD.getCarTrim());
	newOfferCheckpoint.setCarKm(postOfferRequestDTO.getCarKm());
	newOfferCheckpoint.setZipCode(postOfferRequestDTO.getZipCode());
	newOfferCheckpoint.setOfferPunished(carDataBD.getPunishedOffer());
	newOfferCheckpoint.setOriginalPrice30D(carDataBD.getOriginalPrice30D());
	newOfferCheckpoint.setOriginalPriceInstance(carDataBD.getOriginalPriceInstance());
	newOfferCheckpoint.setOriginalPriceConsignation(carDataBD.getOriginalPriceConsignation());
	newOfferCheckpoint.setSalePriceKavak(carDataBD.getSalePriceKavak());
	newOfferCheckpoint.setQuantitySamples(carDataBD.getQuantitySamples());
	newOfferCheckpoint.setAverageDateSamples(carDataBD.getAverageDateSamples());
	newOfferCheckpoint.setUpdateDateOffer(carDataBD.getUpdateDateOffer());
	newOfferCheckpoint.setVersionGuiaAutometrica(carDataBD.getVersionGuiaAutometrica());
	newOfferCheckpoint.setFullVersion(carDataBD.getFullVersion());
	newOfferCheckpoint.setOfferDetail(carDataBD.getOfferDetail());
	newOfferCheckpoint.setTrustedOffer(carDataBD.getTrustedOffer());
	newOfferCheckpoint.setMarketPrice(carDataBD.getMarketPrice());
	newOfferCheckpoint.setOther3(carDataBD.getOther3());
	newOfferCheckpoint.setOther4(carDataBD.getOther4());
	newOfferCheckpoint.setOther5(carDataBD.getOther5());
	newOfferCheckpoint.setOther6(carDataBD.getOther6());
	newOfferCheckpoint.setOther7(carDataBD.getOther7());
	newOfferCheckpoint.setOther8(carDataBD.getOther8());
	newOfferCheckpoint.setSegmentType(carDataBD.getSegmentType());
	newOfferCheckpoint.setPcVendeTuAuto(carDataBD.getPcVendeTuAuto());
	newOfferCheckpoint.setPcEbc(carDataBD.getPcEbc());
	newOfferCheckpoint.setPvEbc(carDataBD.getPvEbc());
	newOfferCheckpoint.setVersionEbc(carDataBD.getVersionEbc());
	newOfferCheckpoint.setAvgKm(carDataBD.getAvgKm());
	newOfferCheckpoint.setSetPriceInst(carDataBD.getSetPriceInst());
	// AWS
	newOfferCheckpoint.setAwsKarsnapOffer(carDataBD.getAwsKarsnapOffer());
	newOfferCheckpoint.setAwsLowestRange(carDataBD.getAwsLowestRange());
	newOfferCheckpoint.setAwsHeighestRange(carDataBD.getAwsHeighestRange());
	newOfferCheckpoint.setAwsInstantOffer(carDataBD.getAwsInstantOffer());
	newOfferCheckpoint.setAwsLrInstantOffer(carDataBD.getAwsLrInstantOffer());
	newOfferCheckpoint.setAwsHrInstantOffer(carDataBD.getAwsHrInstantOffer());
	newOfferCheckpoint.setAwsCncOffer(carDataBD.getAwsCncOffer());
	newOfferCheckpoint.setAwsLrCncOffer(carDataBD.getAwsLrCncOffer());
	newOfferCheckpoint.setAwsHrCncOffer(carDataBD.getAwsHrCncOffer());
	newOfferCheckpoint.setAwsPc30dOrg(carDataBD.getAwsPc30dOrg());
	newOfferCheckpoint.setAwsPcInsOrg(carDataBD.getAwsPcInsOrg());
	newOfferCheckpoint.setAwsPcCncOrg(carDataBD.getAwsPcCncOrg());
	newOfferCheckpoint.setAwsOffer(carDataBD.getAwsOffer());
	newOfferCheckpoint.setAwsPrecioVenta(carDataBD.getAwsPrecioVenta());
	newOfferCheckpoint.setRimMaterial(carDataBD.getRimMaterial());
	newOfferCheckpoint.setLights(carDataBD.getLights());
	newOfferCheckpoint.setAir(carDataBD.getAir());
	newOfferCheckpoint.setAwsFechaActOffer(carDataBD.getAwsFechaActOffer());
	//

	// Communications Disable
	if (postOfferRequestDTO.isDisableCommunications() != false) {
	    newOfferCheckpoint.setSendEmail(true);
	    newOfferCheckpoint.setSentInternalEmail(true);
	    newOfferCheckpoint.setNotAcceptedOfferEmailSent(true);
	    newOfferCheckpoint.setFollowUpEmailSent(true);
	    newOfferCheckpoint.setNewOfferEmailSent(true);
	    newOfferCheckpoint.setSelectDayPlaceCenterSmsSent(true);
	    newOfferCheckpoint.setSelectDayPlaceCustomerSmsSent(true);
	    newOfferCheckpoint.setReleaseAuditOfferSmsSent(true);
	    newOfferCheckpoint.setAcceptOfferNotContinueSmsSent(true);
	    newOfferCheckpoint.setInspectionReminderNotificationSent(true);
	}
	//

	Timestamp actualTime = new Timestamp(System.currentTimeMillis());
	newOfferCheckpoint.setOfferDate(actualTime);
	newOfferCheckpoint.setOfferRecovery(Constants.RECOVER_OFFER_NO);
	if (carDataBD.getDTO().isWishList()) {
	    newOfferCheckpoint.setWishList(1L);
	    offersResponse.setWishList(true);
	} else {
	    newOfferCheckpoint.setWishList(0L);
	    offersResponse.setWishList(false);
	}
	offersResponse.setYear(carDataBD.getCarYear());
	offersResponse.setMake(carDataBD.getCarMake());
	offersResponse.setModel(carDataBD.getCarModel());
	offersResponse.setTrim(carDataBD.getCarTrim());
	offersResponse.setColor(postOfferRequestDTO.getIdExteriorColor());
	offersResponse.setSku(carDataBD.getSku());
	offersResponse.setKm(postOfferRequestDTO.getCarKm());
	offersResponse.setZipCode(postOfferRequestDTO.getZipCode());
	String email = user.get().getEmail();
	User userBD = userRepo.findByEmailAndRole(email, Constants.CUSTOMER_ROLE);
	UserMeta phoneBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, userBD.getId());
	UserMeta addressBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_ADDRESS, userBD.getId());
	UserCheckPointsDTO userCheckpoints = new UserCheckPointsDTO();
	userCheckpoints.setEmail(email);
	userCheckpoints.setCustomerName(userBD.getName());
	userCheckpoints.setIdUser(userBD.getId());
	if (addressBD == null) {
	    userCheckpoints.setAddress("");
	} else {
	    userCheckpoints.setAddress(addressBD.getMetaValue().toString());
	}
	userCheckpoints.setPhone(phoneBD.getMetaValue().toString());
	offersResponse.setUser(userCheckpoints);

	// dataEmail.put(Constants.KEY_EMAIL_CAR_KM,
	// decimalFormatKM.format(Long.valueOf(sellCarDetailBDStep5.getCarKm())));
	if (!carDataBD.getSalePrice().equalsIgnoreCase(Constants.NO_VALUE_BD)) {
	    Double salePriceFormat = Double.valueOf(carDataBD.getSalePrice());
	    newOfferCheckpoint.setSalePrice(decimalFormat.format(salePriceFormat));
	}
	if (!carDataBD.getBuyPrice().equalsIgnoreCase(Constants.NO_VALUE_BD)) {
	    Double buyPriceFormat = Double.valueOf(carDataBD.getBuyPrice());
	    newOfferCheckpoint.setBuyPrice(decimalFormat.format(buyPriceFormat));
	}

	if (postOfferRequestDTO.getSendEmail() != null) {
	    newOfferCheckpoint.setSendEmail(true);
	}
	newOfferCheckpoint.setSentNetsuite(0L); // 0 - nuevo

	if (StringUtils.isNotEmpty(postOfferRequestDTO.getSource())) {
	    newOfferCheckpoint.setOfferSource(postOfferRequestDTO.getSource());
	} else {
	    newOfferCheckpoint.setOfferSource("NA");
	}

	if (postOfferRequestDTO.getInternalUserId() != null) {
	    newOfferCheckpoint.setInternalUserId(postOfferRequestDTO.getInternalUserId());
	}

	MetaValue metaValueStatusCDR = metaValueRepo.findByAlias(StatusCheckpointOfferEnum.CPCD.getAlias());
	newOfferCheckpoint.setIdStatus(metaValueStatusCDR.getId());
	newOfferCheckpoint.setOfferType(metaValueZoneVariousOffers.getId());
	newOfferCheckpoint.setDescriptionOfferType(metaValueZoneVariousOffers.getOptionName());

	OfferCheckpoint offerCheckpointReturnSaveBD = offerCheckpointRepo.save(newOfferCheckpoint);
	offerCheckpointReturnSaveBD.setCodeNetsuiteItem(offerCheckpointReturnSaveBD.getId());

	List<OfferCheckpoint> listOfferCheckpointCheckDuplicate = offerCheckpointRepo.findByIdUserAndCarYearAndCarMakeAndCarModelOrderByIdAsc(postOfferRequestDTO.getIdUsuario(), newOfferCheckpoint.getCarYear(), newOfferCheckpoint.getCarMake(), newOfferCheckpoint.getCarModel());
	// Se agregar el campo control duplicado para que Netsuite lleve control
	// de que las ofertas ya estan repetidas
	if (listOfferCheckpointCheckDuplicate.size() == 1) {
	    offerCheckpointReturnSaveBD.setDuplicatedOfferControl(offerCheckpointReturnSaveBD.getId());
	    offerCheckpointRepo.save(offerCheckpointReturnSaveBD);
	} else {
	    OfferCheckpoint offerCheckpointDuplicate = listOfferCheckpointCheckDuplicate.get(0);
	    // Se busca solo el primer registro por que ya viene ordenada
	    offerCheckpointReturnSaveBD.setDuplicatedOfferControl(offerCheckpointDuplicate.getId());
	    offerCheckpointRepo.save(offerCheckpointReturnSaveBD);
	}

	// ========================= >> Guardado Checkpoint Base

	/*MetaValue maxKmKavak = metaValueRepo.findByAlias("KCK");
	Long carKmMaxKavak = Long.valueOf(maxKmKavak.getOptionName());
	// Se valida que el KM del carro sea menor al permitido por KAVAK
	if (carKmOfferRequest >= carKmMaxKavak) {
	    MessageDTO messageKm = new MessageDTO();
	    messageKm = messageRepo.findByCode(MessageEnum.M0007.toString()).getDTO();
	    listMessages.add(messageKm);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(new ArrayList<>());
	    offerCheckpointReturnSaveBD.setIdStatus((metaValueRepo.findByAlias(StatusCheckpointOfferEnum.CPCD.getAlias()).getId()));
	    offerCheckpointReturnSaveBD.setStatusDetail(metaValueRepo.findByAlias("EONKM").getOptionName());
	    offerCheckpointReturnSaveBD.setOfferType(metaValueRepo.findByAlias("TONR").getId());
	    offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueRepo.findByAlias("TONR").getOptionName());
	    offerCheckpointRepo.save(offerCheckpointReturnSaveBD);
	    return responseDTO;
	}*/

	MetaValue minYearKavak = metaValueRepo.findByAlias("KCY");
	// Se valida que le anio del carro sea mayor al minimo permito por KAVAK
	if (carDataBD.getCarYear() < Long.valueOf(minYearKavak.getOptionName())) {
	    MessageDTO messageYear = new MessageDTO();
	    messageValidations = true;
	    messageYear = messageRepo.findByCode(MessageEnum.M0001.toString()).getDTO();
	    listMessages.add(messageYear);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(new ArrayList<>());
	    offerCheckpointReturnSaveBD.setIdStatus((metaValueRepo.findByAlias(StatusCheckpointOfferEnum.CPCD.getAlias()).getId()));
	    offerCheckpointReturnSaveBD.setStatusDetail(metaValueRepo.findByAlias("EONCA").getOptionName());
	    offerCheckpointReturnSaveBD.setOfferType(metaValueRepo.findByAlias("TONR").getId());
	    offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueRepo.findByAlias("TONR").getOptionName());
	    offerCheckpointRepo.save(offerCheckpointReturnSaveBD);
	    return responseDTO;
	}

	Double offer30Days = 0D;
	Double offerInstant = 0D;
	Double offerConsignation = 0D;
	Double lowerRange30Days = 0D;
	Double highRange30Days = 0D;
	Double lowerRangeInstant = 0D;
	Double highRangeInstant = 0D;
	Double lowerRangeConcession = 0D;
	Double highRangeConcession = 0D;
	Double priceFactor = 0D;
	Double finalAmount = 0D;
	Double minPrice = 0D;
	Double maxPrice = 0D;
	Long minKm = Long.valueOf(carDataBD.getMinKm());
	Long minKmStandarReward = Long.valueOf(carDataBD.getMinKmStandarReward());
	Long minKmStandarRewardFactor = Long.valueOf(carDataBD.getMinKmStandarRewardFactor());
	Long minKmTopRewardFactor = Long.valueOf(carDataBD.getMinKmTopRewardFactor());
	Long maxKm = Long.valueOf(carDataBD.getMaxKm());
	Long maxKmStandarPunishment = Long.valueOf(carDataBD.getMaxKmStandarPunishment());
	Long maxKmTopPunishmentFactor = Long.valueOf(carDataBD.getMaxKmTopPunishmentFactor());
	Long maxKmStandarPunishmentFactor = Long.valueOf(carDataBD.getMaxKmStandarPunishmentFactor());
	// String firstName = "";
	// String middleName = "";
	// String lastName = "";

	if (!carDataBD.getOffer30Days().equalsIgnoreCase("NA")) {
	    offer30Days = Double.valueOf(carDataBD.getOffer30Days());
	} else {
	    offer30Days = 0D;
	}

	// IssueID #2282 - Manejo de Habilitación de Oferta Inmediata
	offerInstant = 0D;
	MetaValue metaValueInstantOfferOn = metaValueRepo.findByAlias("INSTOFFERON");
	if (metaValueInstantOfferOn.getOptionName().equals("1")) {
	    if (!carDataBD.getOfferInstant().equalsIgnoreCase("NA")) {
		offerInstant = Double.valueOf(carDataBD.getOfferInstant());
	    }
	}

	if (!carDataBD.getOfferConsignation().equalsIgnoreCase("NA")) {
	    offerConsignation = Double.valueOf(carDataBD.getOfferConsignation());
	} else {
	    offerConsignation = 0D;
	}

	if (!carDataBD.getLowerRange30Days().equalsIgnoreCase("NA")) {
	    lowerRange30Days = Double.valueOf(carDataBD.getLowerRange30Days());
	}
	if (!carDataBD.getHighRange30Days().equalsIgnoreCase("NA")) {
	    highRange30Days = Double.valueOf(carDataBD.getHighRange30Days());
	}

	if (!carDataBD.getLowerRangeInstant().equalsIgnoreCase("NA")) {
	    lowerRangeInstant = Double.valueOf(carDataBD.getLowerRangeInstant());
	}
	if (!carDataBD.getHighRangeInstant().equalsIgnoreCase("NA")) {
	    highRangeInstant = Double.valueOf(carDataBD.getHighRangeInstant());
	}

	if (!carDataBD.getLowerRangeConsession().equalsIgnoreCase("NA")) {
	    lowerRangeConcession = Double.valueOf(carDataBD.getLowerRangeConsession());
	}
	if (!carDataBD.getHighRangeConsession().equalsIgnoreCase("NA")) {
	    highRangeConcession = Double.valueOf(carDataBD.getHighRangeConsession());
	}

	ZipCode costoEnvioCodigoPostal = costoEnvioCodigoPostalRepo.findByZipCode(Long.valueOf(postOfferRequestDTO.getZipCode()));
	if (costoEnvioCodigoPostal == null) {
	    MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0002.toString()).getDTO();
	    listMessages.add(messageNoRecords);
	    messageValidations = true;
	    offersResponse.setServedZone(false);
	} else if (!costoEnvioCodigoPostal.isServedZone()) {
	    offersResponse.setServedZone(false);
	}

	if (carKmOfferRequest < minKmStandarReward) {
	    priceFactor = Double.valueOf(minKmTopRewardFactor);
	} else if (carKmOfferRequest >= minKmStandarRewardFactor && carKmOfferRequest < minKm) {
	    priceFactor = (minKmStandarRewardFactor / Math.pow(minKmStandarReward - minKm, 2)) * Math.pow(minKm - carKmOfferRequest, 2);
	} else if (carKmOfferRequest > maxKmStandarPunishment) {
	    priceFactor = Double.valueOf(maxKmTopPunishmentFactor);
	    MessageDTO messageKmSku = messageRepo.findByCode(MessageEnum.M0003.toString()).getDTO();
	    listMessages.add(messageKmSku);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(priceFactor);
	    offerCheckpointReturnSaveBD.setIdStatus((metaValueRepo.findByAlias(StatusCheckpointOfferEnum.CPCD.getAlias()).getId()));
	    offerCheckpointReturnSaveBD.setStatusDetail(metaValueRepo.findByAlias("EONKM").getOptionName());
	    offerCheckpointReturnSaveBD.setOfferType(metaValueRepo.findByAlias("TONR").getId());
	    offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueRepo.findByAlias("TONR").getOptionName());
	    offerCheckpointRepo.save(offerCheckpointReturnSaveBD);
	    return responseDTO;
	} else if (carKmOfferRequest <= maxKmStandarPunishment && carKmOfferRequest > maxKm) {
	    priceFactor = -(maxKmStandarPunishmentFactor / Math.pow(maxKmStandarPunishment - maxKm, 2)) * Math.pow(carKmOfferRequest - maxKm, 2);
	}

	if (messageValidations) {
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	}

	if (offerConsignation != null && offerConsignation != 0D) {
	    finalAmount = Double.valueOf(offerConsignation) * (1 + priceFactor / 100);
	    if (!carDataBD.getLowerRangeConsession().equalsIgnoreCase("NA")) {
		minPrice = (finalAmount + ((finalAmount * lowerRangeConcession) / 100));
	    } else {
		minPrice = (finalAmount + ((finalAmount * 0) / 100));
	    }
	    if (!carDataBD.getHighRangeConsession().equalsIgnoreCase("NA")) {
		maxPrice = (finalAmount + ((finalAmount * highRangeConcession) / 100));
	    } else {
		maxPrice = (finalAmount + ((finalAmount * 0) / 100));
	    }
	    GenericDTO genericTypeConsessionDTO = new GenericDTO();
	    if (offersResponse.isServedZone()) {
		genericTypeConsessionDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYCONSIGMENT.getAlias()).getId());
	    } else {
		genericTypeConsessionDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYCONSIGMENT.getAliasNotServed()).getId());
	    }

	    genericTypeConsessionDTO.setTypeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYCONSIGMENT.toString()).getType());
	    genericTypeConsessionDTO.setCodeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYCONSIGMENT.toString()).getCode());
	    genericTypeConsessionDTO.setMin(Math.round(minPrice));
	    genericTypeConsessionDTO.setMax(Math.round(maxPrice));
	    offerCheckpointReturnSaveBD.setMinConsignationOffer(genericTypeConsessionDTO.getMin().toString());
	    offerCheckpointReturnSaveBD.setMaxConsignationOffer(genericTypeConsessionDTO.getMax().toString());
	    Optional<OfferType> offerTypeConsessionBD = offerTypeRepo.findById(OfferTypeEnum.BUYCONSIGMENT.getId());
	    genericTypeConsessionDTO.setDescrption(offerTypeConsessionBD.get().getDescription());
	    listGenerateOffer.add(genericTypeConsessionDTO);
	}

	if (offer30Days != null && offer30Days != 0D) {
	    finalAmount = Double.valueOf(offer30Days) * (1 + priceFactor / 100);
	    if (!carDataBD.getLowerRange30Days().equals("NA")) {
		minPrice = (finalAmount + ((finalAmount * lowerRange30Days) / 100));
	    } else {
		minPrice = (finalAmount + ((finalAmount * 0) / 100));
	    }
	    if (!carDataBD.getHighRange30Days().equals("NA")) {
		maxPrice = (finalAmount + ((finalAmount * highRange30Days) / 100));
	    } else {
		maxPrice = (finalAmount + ((finalAmount * 0) / 100));
	    }
	    GenericDTO genericTypeOffer30DaysDTO = new GenericDTO();
	    if (offersResponse.isServedZone()) {
		genericTypeOffer30DaysDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUY30DAYS.getAlias()).getId());
	    } else {
		genericTypeOffer30DaysDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUY30DAYS.getAliasNotServed()).getId());
	    }
	    genericTypeOffer30DaysDTO.setTypeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUY30DAYS.toString()).getType());
	    genericTypeOffer30DaysDTO.setCodeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUY30DAYS.toString()).getCode());
	    genericTypeOffer30DaysDTO.setMin(Math.round(minPrice));
	    genericTypeOffer30DaysDTO.setMax(Math.round(maxPrice));
	    offerCheckpointReturnSaveBD.setMin30DaysOffer(genericTypeOffer30DaysDTO.getMin().toString());
	    offerCheckpointReturnSaveBD.setMax30DaysOffer(genericTypeOffer30DaysDTO.getMax().toString());
	    Optional<OfferType> offerType30DBD = offerTypeRepo.findById(OfferTypeEnum.BUY30DAYS.getId());
	    genericTypeOffer30DaysDTO.setDescrption(offerType30DBD.get().getDescription());
	    listGenerateOffer.add(genericTypeOffer30DaysDTO);
	}

	if (offerInstant != null && offerInstant != 0D) {
	    finalAmount = Double.valueOf(offerInstant) * (1 + priceFactor / 100);
	    if (!carDataBD.getLowerRangeInstant().equalsIgnoreCase("NA")) {
		minPrice = (finalAmount + ((finalAmount * lowerRangeInstant) / 100));
	    } else {
		minPrice = (finalAmount + ((finalAmount * 0) / 100));
	    }
	    if (!carDataBD.getHighRangeInstant().equalsIgnoreCase("NA")) {
		maxPrice = (finalAmount + ((finalAmount * highRangeInstant) / 100));
	    } else {
		maxPrice = (finalAmount + ((finalAmount * 0) / 100));
	    }
	    GenericDTO genericTypeOfferInstantDTO = new GenericDTO();
	    if (offersResponse.isServedZone()) {
		genericTypeOfferInstantDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYINMEDIATE.getAlias()).getId());
	    } else {
		genericTypeOfferInstantDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYINMEDIATE.getAliasNotServed()).getId());
	    }
	    genericTypeOfferInstantDTO.setTypeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYINMEDIATE.toString()).getType());
	    genericTypeOfferInstantDTO.setCodeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYINMEDIATE.toString()).getCode());
	    genericTypeOfferInstantDTO.setMin(Math.round(minPrice));
	    genericTypeOfferInstantDTO.setMax(Math.round(maxPrice));
	    offerCheckpointReturnSaveBD.setMinInstantOffer(genericTypeOfferInstantDTO.getMin().toString());
	    offerCheckpointReturnSaveBD.setMaxInstantOffer(genericTypeOfferInstantDTO.getMax().toString());
	    Optional<OfferType> offerTypeInstantBD = offerTypeRepo.findById(OfferTypeEnum.BUYINMEDIATE.getId());
	    genericTypeOfferInstantDTO.setDescrption(offerTypeInstantBD.get().getDescription());

	    // AB Test
	    List<MetaValue> listMetaValueABTest = metaValueRepo.findByGroupNameAndActiveTrue(Constants.AB_TEST_POST_OFFER);
	    if (listMetaValueABTest.size() <= 1) {
		MetaValue metaValueABTest = metaValueRepo.findByAlias(Constants.AB_TEST_RECOVER_OFFER_CODE);
		if (metaValueABTest.getActive()) {// ESTA ACTIVO EL AB TEST
		    OfferCheckpoint offerCheckpointBD = offerCheckpointRepo.findByUserAndHiddenInstantOfferNotNullOrderDesc(postOfferRequestDTO.getIdUsuario());
		    if (offerCheckpointBD != null) {
			// SE LE MOSTRO LA OFERTA INMEDIATA
			offerCheckpointReturnSaveBD.setAbTestCode(metaValueABTest.getAlias());
			if (offerCheckpointBD.getAbTestResult() == 0L) {// SE LE ENSEÑA LA OFERTA
			    listGenerateOffer.add(genericTypeOfferInstantDTO);
			    offerCheckpointReturnSaveBD.setAbTestResult(0L);
			} else {// SE LE OCULTO LA OFERTA INMEDIATA
			    offerCheckpointReturnSaveBD.setAbTestResult(1L);
			}
		    } else {
			offerCheckpointReturnSaveBD.setAbTestCode(metaValueABTest.getAlias());
			int numeroAleatorio = (int) (Math.random() * 50 + 1);
			if (numeroAleatorio > 25) {
			    listGenerateOffer.add(genericTypeOfferInstantDTO);
			    offerCheckpointReturnSaveBD.setAbTestResult(0L);
			} else {
			    offerCheckpointReturnSaveBD.setAbTestResult(1L);
			}
		    }
		} else {// NO ESTA ACTIVO EL AB TEST
		    listGenerateOffer.add(genericTypeOfferInstantDTO);
		}
	    }
	}

	if (priceFactor != null) {
	    offerCheckpointReturnSaveBD.setFactorKmApplied(priceFactor.toString());
	}

	// Varias Ofertas
	if (listGenerateOffer.size() > 1) {
	    offerCheckpointReturnSaveBD.setOfferType(metaValueZoneVariousOffers.getId());
	    offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueZoneVariousOffers.getOptionName());

	    MetaValue metaValueStatusNotAcceptoffers = metaValueRepo.findByAlias(StatusDetailOfferEnum.NOACCEPTOFFER.getAlias());
	    offerCheckpointReturnSaveBD.setStatusDetail(metaValueStatusNotAcceptoffers.getOptionName());
	} else if (listGenerateOffer.size() == 1) { // Solo 1 Oferta
	    GenericDTO genericTypeOffer = listGenerateOffer.get(0);
	    if (!offersResponse.isServedZone()) { // Zona Not Servida
		MetaValue metaValueZoneServed = metaValueRepo.findByAlias(OfferTypeEnum.getByType(genericTypeOffer.getCodeOffer()).getAlias());
		offerCheckpointReturnSaveBD.setOfferType(metaValueZoneServed.getId());
		offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueZoneServed.getOptionName());

		MetaValue metaValueStatusNotZS = metaValueRepo.findByAlias(StatusDetailOfferEnum.NOZONESERVED.getAlias());
		offerCheckpointReturnSaveBD.setStatusDetail(metaValueStatusNotZS.getOptionName());
	    } else {

		MetaValue metaValueZoneNotServed = metaValueRepo.findByAlias(OfferTypeEnum.getByType(genericTypeOffer.getCodeOffer()).getAliasNotServed());
		offerCheckpointReturnSaveBD.setOfferType(metaValueZoneNotServed.getId());
		offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueZoneNotServed.getOptionName());

		MetaValue metaValueStatusNotZS = metaValueRepo.findByAlias(StatusDetailOfferEnum.NOACCEPTOFFER.getAlias());
		offerCheckpointReturnSaveBD.setStatusDetail(metaValueStatusNotZS.getOptionName());
	    }
	} else if (listGenerateOffer.size() == 0) {// Ninguna Oferta
	    MetaValue metaValueNoOffers = metaValueRepo.findByAlias(OfferTypeEnum.NOOFFERSBD.getAlias());
	    offerCheckpointReturnSaveBD.setOfferType(metaValueNoOffers.getId());
	    offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueNoOffers.getOptionName());

	    MetaValue metaValueStatusNotOffers = metaValueRepo.findByAlias(StatusDetailOfferEnum.NOOFFERBD.getAlias());
	    offerCheckpointReturnSaveBD.setStatusDetail(metaValueStatusNotOffers.getOptionName());
	}

	String finalHash = generateRecoverOfferURL(offerCheckpointReturnSaveBD.getId());
	offerCheckpointReturnSaveBD.setUrlRecoverOffer(finalHash);
	offersResponse.setUrlRecoverOffer(finalHash);
	// Calculo para un valor necesitado en netsuite - identificador unico
	// Calendar calendarNetsuite = Calendar.getInstance();
	// Integer numberWeek = calendarNetsuite.get(Calendar.WEEK_OF_YEAR);
	// Integer actualYear = calendarNetsuite.get(Calendar.YEAR);
	// String netsuiteItemId = newOfferCheckpoint.getOfferType().toString()
	// + numberWeek.toString() + actualYear.toString() +
	// newOfferCheckpoint.getZipCode().toString() +
	// carKmOfferRequest.toString();

	// AB Test Continuar al Seleccionar Oferta
	List<MetaValue> listMetaValueABTest = metaValueRepo.findByGroupNameAndActiveTrue(Constants.AB_TEST_POST_OFFER);
	if (listMetaValueABTest.size() <= 1) {
	    MetaValue metaValueABTest = metaValueRepo.findByAlias(Constants.AB_TEST_CONTINUE_SELECT_OFFER_CODE);
	    if (metaValueABTest.getActive()) {// ESTA ACTIVO EL AB TEST
		int numeroAleatorio = (int) (Math.random() * 50 + 1);
		if (numeroAleatorio > 25) {
		    offersResponse.setAbTestCode(Constants.AB_TEST_CONTINUE_SELECT_OFFER_CODE);
		    offersResponse.setAbTestResult(1L);
		    offerCheckpointReturnSaveBD.setAbTestCode(Constants.AB_TEST_CONTINUE_SELECT_OFFER_CODE);
		    offerCheckpointReturnSaveBD.setAbTestResult(1L);
		} else {
		    offersResponse.setAbTestCode(Constants.AB_TEST_CONTINUE_SELECT_OFFER_CODE);
		    offersResponse.setAbTestResult(0L);
		    offerCheckpointReturnSaveBD.setAbTestCode(Constants.AB_TEST_CONTINUE_SELECT_OFFER_CODE);
		    offerCheckpointReturnSaveBD.setAbTestResult(0L);
		}
	    }
	}

	offerCheckpointRepo.save(offerCheckpointReturnSaveBD);
	
	//url tracking
	if(postOfferRequestDTO.getIdCheckpoint() != null) {
	    Optional<OfferCheckpoint> OfferCheckpointBD = offerCheckpointRepo.findById(postOfferRequestDTO.getIdCheckpoint()); 
	    try {
		//url para tracking
		offersResponse.setUrlTrackingRecoverOffer(generateTrackingRecoverOfferURL(OfferCheckpointBD.get()));
	    } catch (Exception e) {
		offersResponse.setUrlTrackingRecoverOffer(Constants.NO_VALUE_BD);
	    }
	}else {
	    offersResponse.setUrlTrackingRecoverOffer(Constants.NO_VALUE_BD);
	}
	
	offersResponse.setId(offerCheckpointReturnSaveBD.getId());
	offersResponse.setListData(listGenerateOffer);
	if (listGenerateOffer.isEmpty()) {
	    enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    if (newOfferCheckpoint.getWishList() == 1L) {
		MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0005.toString()).getDTO();
		listMessages.add(messageNoRecords);
	    } else {
		MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0006.toString()).getDTO();
		// Se crea un String auto para guardar los datos del auto
		// necesarios para el mensaje,
		// ya que el mismo no esta mandando el auto, y se reemplaza en
		// el message
		String auto = carDataBD.getCarYear() + " " + KavakUtils.getFriendlyCarNameData(carDataBD);
		String userMessage = messageNoRecords.getUserMessage();
		userMessage = userMessage.replaceAll("2010 Cbo Motors Cbo", auto);
		messageNoRecords.setUserMessage(userMessage);
		listMessages.add(messageNoRecords);
	    }
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(new ArrayList<>());
	} else {
	    enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
	}
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(offersResponse);
	return responseDTO;
    }

    /**
     * Actualiza los datos de la oferta acorde con el paso en que se encuentre
     *
     * @author Enrique Marin
     * @param idOfferCheckPoint
     *            identificador de la oferta
     * @param putOfferRequestDTO
     * @return PutOfferResponseDTO
     */
    public ResponseDTO putOffers(Long idOfferCheckPoint, PutOfferRequestDTO putOfferRequestDTO) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " [putOffers] ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseDTO = new ResponseDTO();
	int stepInt = putOfferRequestDTO.getStep().intValue();
	Optional<OfferCheckpoint> offerCheckpointToUpdate = offerCheckpointRepo.findById(idOfferCheckPoint);
	PutOfferResponseDTO putOfferResponseDTO = new PutOfferResponseDTO();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat parseFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
	listMessages = new ArrayList<>();

	boolean setp5 = false;
	if (offerCheckpointToUpdate.isPresent()) {
	    switch (stepInt) {
	    case 2: // Seleccionar OFERTA
		Optional<MetaValue> metaValueTypeOffer = metaValueRepo.findById(putOfferRequestDTO.getTypeOfferId());
		offerCheckpointToUpdate.get().setOfferType(metaValueTypeOffer.get().getId());
		offerCheckpointToUpdate.get().setDescriptionOfferType(metaValueTypeOffer.get().getOptionName());
		LogService.logger.info("[putOffers] - Termino paso 2");
		break;
	    case 3: // Seleccionar Direccion

		if (putOfferRequestDTO.getIdInspectionLocation() == Constants.INSPECTION_LOCATION_USER_ADDRESS) {
		    // Tiene como entrada la direccion colocada por el usuario
		    // street + ', ' + exterior_number + ', ' + 'Numero
		    // Interior: ' + interior_number + ', ' + municipality + ',
		    // ' + colony + ', ' + postal_code + '.'
		    offerCheckpointToUpdate.get().setInspectionLocationDescription(putOfferRequestDTO.getStreet() + ", " + putOfferRequestDTO.getExterior_number() + ", " + putOfferRequestDTO.getInterior_number() + ", " + putOfferRequestDTO.getMunicipality() + ", " + putOfferRequestDTO.getColony() + ", " + putOfferRequestDTO.getZip() + ".");

		    MetaValue mevaValueDICLI = metaValueRepo.findByAlias(Constants.META_VALUE_CUSTOMER_ADDRESS);
		    offerCheckpointToUpdate.get().setInspectionLocation(mevaValueDICLI.getId());
		    offerCheckpointToUpdate.get().setInspectionCenterDescription(Constants.DIRECTION_CLIENT);
		} else {
		    // Tiene como entrada la direccion del Coliseo address +
		    // colony + city + state + postal_code;
		    Optional<InspectionLocation> inspectionLocation = inspectionLocationRepo.findById(putOfferRequestDTO.getIdInspectionLocation());
		    offerCheckpointToUpdate.get().setInspectionLocationDescription(inspectionLocation.get().getAddress() + ", " + inspectionLocation.get().getColony() + ", " + inspectionLocation.get().getCity() + ", " + inspectionLocation.get().getState() + ", " + inspectionLocation.get().getZip() + ".");

		    MetaValue mevaValueDICAR = metaValueRepo.findByAlias(Constants.META_VALUE_KAVAK_ADDRESS);
		    offerCheckpointToUpdate.get().setInspectionLocation(mevaValueDICAR.getId());
		    offerCheckpointToUpdate.get().setInspectionCenterDescription(inspectionLocation.get().getName());
		}
		LogService.logger.info("[putOffers] - Termino paso 3");
		break;
	    case 4: // Selecciona horario

		Date inspectionDate = null;
		try {
		    if (putOfferRequestDTO.getInspectionDate() != null && !StringUtils.isBlank(putOfferRequestDTO.getInspectionDate())) {
			inspectionDate = sdf.parse(putOfferRequestDTO.getInspectionDate());
		    }
		} catch (ParseException e) {
		    MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0077.toString()).getDTO();
		    listMessages.add(messageNoRecords);
		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setListMessage(listMessages);
		    responseDTO.setData(new ArrayList<>());
		    return responseDTO;
		}

		// Asignacion de EM
		UserTaskAssignmentWeight idEMBD;
		// Se busca primero si ya el cliente tuvo un EM asignado previamente que este activo
		idEMBD = userTaskAssignmentWeightRepo.findEMSaleCheckpointByQueryOfWeightAndUserId(offerCheckpointToUpdate.get().getIdUser());
		// Si no se asigna un EM según la tabla de distrubución
		if (idEMBD == null) {
		    idEMBD = userTaskAssignmentWeightRepo.findEMOfferCheckpointByQueryOfWeight();
		}

		offerCheckpointToUpdate.get().setAssignedEm(idEMBD.getId());
		if (StringUtils.isBlank(putOfferRequestDTO.getInspectionDate()) && putOfferRequestDTO.getIdHourInspectionSlot() == null && StringUtils.isBlank(putOfferRequestDTO.getHourInspectionSlotText())) {
		    MetaValue metaValueBD = metaValueRepo.findByAlias("CPCPDHI");
		    offerCheckpointToUpdate.get().setIdStatus(metaValueBD.getId());
		    offerCheckpointToUpdate.get().setStatusDetail(metaValueBD.getOptionName());
		    Calendar calendarDateFrom = Calendar.getInstance();
		    calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
		    try {
			Date dateKey = parseFormat.parse(calendarDateFrom.getTime().toString());
			String dateKeyS = sdf.format(dateKey);
			offerCheckpointToUpdate.get().setInspectionDate(dateKeyS);
		    } catch (ParseException e) {
			e.printStackTrace();
		    }
		    offerCheckpointToUpdate.get().setHourInpectionSlotText("2:00 am a 3:00 am");
		    offerCheckpointToUpdate.get().setIdHourInpectionSlot(null);
		    offerCheckpointToUpdate.get().setStartHour("2:00 AM");
		    offerCheckpointToUpdate.get().setEndHour("3:00 AM");

		} else {

		    offerCheckpointToUpdate.get().setInspectionDate(putOfferRequestDTO.getInspectionDate());
		    offerCheckpointToUpdate.get().setIdHourInpectionSlot(putOfferRequestDTO.getIdHourInspectionSlot());
		    InspectionScheduleBlock inspectionScheduleBlockBD = inspectionScheduleBlockRepo.findByIdHourBlock(putOfferRequestDTO.getIdHourInspectionSlot());
		    offerCheckpointToUpdate.get().setStartHour(inspectionScheduleBlockBD.getHourFrom());
		    offerCheckpointToUpdate.get().setEndHour(inspectionScheduleBlockBD.getHourTo());
		    offerCheckpointToUpdate.get().setHourInpectionSlotText(inspectionScheduleBlockBD.getCheckpointLabel());
		    InspectionSchedule inspectionScheduleReserved = new InspectionSchedule();
		    inspectionScheduleReserved.setInspectionLocationId(putOfferRequestDTO.getIdInspectionLocation());
		    inspectionScheduleReserved.setIdHourBlock(putOfferRequestDTO.getIdHourInspectionSlot());
		    inspectionScheduleReserved.setSelectionDate(new Timestamp(System.currentTimeMillis()));
		    inspectionScheduleReserved.setStatus(true);
		    inspectionScheduleReserved.setInspectionDate(inspectionDate);
		    inspectionScheduleRepo.save(inspectionScheduleReserved);

		}
		LogService.logger.info("[putOffers] - Termino paso 4");
		break;
	    case 5: // Enviar Documento
		setp5 = true;
		List<GenericDTO> listDataResumenGenericDTO = new ArrayList<>();
		List<GenericDTO> listDataUserGenericDTO = new ArrayList<>();
		List<GenericDTO> listIndicationsGenericDTO = new ArrayList<>();

		GenericDTO carGenericDTO = new GenericDTO();
		GenericDTO offerTypeGenericDTO = new GenericDTO();
		GenericDTO offerGenericDTO = new GenericDTO();
		GenericDTO offerExpirationGenericDTO = new GenericDTO();
		GenericDTO inspectionDateGenericDTO = new GenericDTO();
		GenericDTO inspectionHourSlotGenericDTO = new GenericDTO();
		GenericDTO addressGenericDTO = new GenericDTO();
		/** Seteo de url de recupearacion d oferta a null */
		offerCheckpointToUpdate.get().setUrlRecoverOffer(null);
		carGenericDTO.setName("Auto:");
		carGenericDTO.setContent(offerCheckpointToUpdate.get().getCarYear() + ", " + offerCheckpointToUpdate.get().getCarMake() + " - " + offerCheckpointToUpdate.get().getCarModel() + " " + offerCheckpointToUpdate.get().getCarVersion());
		listDataResumenGenericDTO.add(carGenericDTO);
		offerTypeGenericDTO.setName("Tipo de Oferta:");
		offerTypeGenericDTO.setContent(offerCheckpointToUpdate.get().getDescriptionOfferType());
		listDataResumenGenericDTO.add(offerTypeGenericDTO);
		offerGenericDTO.setName("Oferta:");
		if (offerCheckpointToUpdate.get().getDescriptionOfferType().equals(OfferTypeEnum.BUYINMEDIATE.getType())) {
		    offerGenericDTO.setContent(offerCheckpointToUpdate.get().getMinInstantOffer() + " - " + offerCheckpointToUpdate.get().getMinInstantOffer() + " (sujeto a la inspección)");
		}
		if (offerCheckpointToUpdate.get().getDescriptionOfferType().equals(OfferTypeEnum.BUY30DAYS.getType())) {
		    offerGenericDTO.setContent(offerCheckpointToUpdate.get().getMin30DaysOffer() + " - " + offerCheckpointToUpdate.get().getMax30DaysOffer() + " (sujeto a la inspección)");
		}
		if (offerCheckpointToUpdate.get().getDescriptionOfferType().equals(OfferTypeEnum.BUYCONSIGMENT.getType())) {
		    offerGenericDTO.setContent(offerCheckpointToUpdate.get().getMinConsignationOffer() + " - " + offerCheckpointToUpdate.get().getMaxConsignationOffer() + " (sujeto a la inspección)");
		}

		Calendar calendarOfferDate = Calendar.getInstance();
		calendarOfferDate.setTime(offerCheckpointToUpdate.get().getOfferDate());
		calendarOfferDate.add(Calendar.DAY_OF_MONTH, Constants.DAYS_VALIDATE_OFFER);

		listDataResumenGenericDTO.add(offerGenericDTO);
		offerExpirationGenericDTO.setName("Oferta valida hasta:");
		offerExpirationGenericDTO.setContent(sdf.format(calendarOfferDate.getTime()));
		listDataResumenGenericDTO.add(offerExpirationGenericDTO);
		addressGenericDTO.setName("Dirección de la inspeccion:");
		addressGenericDTO.setContent(offerCheckpointToUpdate.get().getInspectionLocationDescription());

		if (offerCheckpointToUpdate.get().getHourInpectionSlotText() == null || offerCheckpointToUpdate.get().getHourInpectionSlotText().trim().equals("2:00 am a 3:00 am")) {
		    inspectionDateGenericDTO.setName("Día de la inspección:");
		    inspectionDateGenericDTO.setContent("¡Pronto te Contactaremos!");
		    listDataResumenGenericDTO.add(inspectionDateGenericDTO);
		    inspectionHourSlotGenericDTO.setName("Hora de la inspección:");
		    inspectionHourSlotGenericDTO.setContent("Por Definir");
		    listDataResumenGenericDTO.add(inspectionHourSlotGenericDTO);
		} else {
		    inspectionDateGenericDTO.setName("Día de la inspección:");
		    inspectionDateGenericDTO.setContent(offerCheckpointToUpdate.get().getInspectionDate());
		    listDataResumenGenericDTO.add(inspectionDateGenericDTO);
		    inspectionHourSlotGenericDTO.setName("Hora de la inspección:");
		    inspectionHourSlotGenericDTO.setContent(offerCheckpointToUpdate.get().getHourInpectionSlotText());
		    listDataResumenGenericDTO.add(inspectionHourSlotGenericDTO);
		}
		listDataResumenGenericDTO.add(addressGenericDTO);

		GenericDTO phoneGenericDTO = new GenericDTO();
		GenericDTO emailGenericDTO = new GenericDTO();

		phoneGenericDTO.setName("Telefono:");
		phoneGenericDTO.setContent(offerCheckpointToUpdate.get().getUser().getDTO().getPhone());
		listDataUserGenericDTO.add(phoneGenericDTO);
		emailGenericDTO.setName("Email");
		emailGenericDTO.setContent(offerCheckpointToUpdate.get().getUser().getDTO().getEmail());
		listDataUserGenericDTO.add(emailGenericDTO);
		List<Indication> listIndications = indicationRepo.findAll();
		for (Indication indicationActual : listIndications) {
		    GenericDTO indicationGenericDTO = new GenericDTO();
		    indicationGenericDTO.setName(indicationActual.getNumberIndex());
		    indicationGenericDTO.setContent(indicationActual.getIndicationText());

		    listIndicationsGenericDTO.add(indicationGenericDTO);
		}

		putOfferResponseDTO.setListDataResumen(listDataResumenGenericDTO);
		putOfferResponseDTO.setListDataUser(listDataUserGenericDTO);
		putOfferResponseDTO.setListIndications(listIndicationsGenericDTO);

		String nameCirculationCard = Constants.NAME_IMAGE_CIRCULATION_CARD + Calendar.getInstance().getTimeInMillis();
		String nameCarBill = Constants.NAME_IMAGE_CAR_INVOICE + Calendar.getInstance().getTimeInMillis();
		String nameCarPhoto = Constants.NAME_IMAGE_CAR_PHOTO + Calendar.getInstance().getTimeInMillis();
		if (putOfferRequestDTO.getCirculationCard() != null) {
		    KavakUtils.generateFile(putOfferRequestDTO.getCirculationCard(), nameCirculationCard, null, Constants.FILE_TYPE_JPG, false);
		    offerCheckpointToUpdate.get().setCirculationCardUrl(Constants.URL_SHORT_PATH_FOLDER_ASSEST + nameCirculationCard + ".jpg");
		}

		if (putOfferRequestDTO.getCarBill() != null) {
		    KavakUtils.generateFile(putOfferRequestDTO.getCarBill(), nameCarBill, null, Constants.FILE_TYPE_JPG, false);
		    offerCheckpointToUpdate.get().setCarInvoiceUrl(Constants.URL_SHORT_PATH_FOLDER_ASSEST + nameCarBill + ".jpg");
		}

		if (putOfferRequestDTO.getCarPhoto() != null) {
		    KavakUtils.generateFile(putOfferRequestDTO.getCarPhoto(), nameCarPhoto, null, Constants.FILE_TYPE_JPG, false);
		    offerCheckpointToUpdate.get().setCarPhoto(Constants.URL_SHORT_PATH_FOLDER_ASSEST + nameCarPhoto + ".jpg");
		}
		LogService.logger.info("[putOffers] - Termino paso 5");
		break;
	    default: // StatusCheckpointOfferEnum.OTRO;
		break;
	    }

	    MetaValue metaValueStatus = metaValueRepo.findByAlias(StatusCheckpointOfferEnum.getByStep(putOfferRequestDTO.getStep()).getAlias());
	    offerCheckpointToUpdate.get().setIdStatus(metaValueStatus.getId());
	    offerCheckpointToUpdate.get().setStatusDetail(metaValueStatus.getOptionName());
	    offerCheckpointToUpdate.get().setSentNetsuite(2L);
	    offerCheckpointRepo.save(offerCheckpointToUpdate.get());
	    LogService.logger.info("[putOffers] - Se actuzalizo la oferta [" + offerCheckpointToUpdate.get().getId() + "]");
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());

	} else {
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0000.toString()).getDTO();
	    List<MessageDTO> listMessage = new ArrayList<>();
	    listMessage.add(messageNoRecords);
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessage);
	}
	if (setp5) {
	    responseDTO.setData(putOfferResponseDTO);
	} else {
	    responseDTO.setData(new GenericDTO());
	}
	LogService.logger.info(Constants.LOG_EXECUTING_END + " [putOffers] [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseDTO;
    }

    /**
     * Obtener y validar si la oferta esta vigente para su recuperacion....
     *
     * @author Idelfonso Sanchez Linares
     * @param Hash
     *            Hash de la oferta
     * @return responseDTO
     */
    public ResponseDTO getOfferByHash(String hash) {
	ResponseDTO responseDTO = new ResponseDTO();
	String urlRecoverOffer = Constants.URL_KAVAK + hash;
	List<OfferCheckpoint>  offerCheckpointBDList = offerCheckpointRepo.findByUrlRecoverOffer(urlRecoverOffer);
	OfferCheckpoint offerCheckpointBD = null;

	if (offerCheckpointBDList != null && offerCheckpointBDList.size() == 1) {
	    offerCheckpointBD = offerCheckpointBDList.get(0); 
	    String[] dateOffer = offerCheckpointBD.getOfferDate().toString().split("-"); // Formato de fecha de timestamp a string se sustrae anio y mes
	    String dateOffer2[] = dateOffer[2].toString().split("\\s+"); // Se sustrae el dia de la fecha
	    Calendar now = new GregorianCalendar();
	    Calendar dateOfferTime = new GregorianCalendar();
	    dateOfferTime.set(Integer.parseInt(dateOffer[0]), Integer.parseInt(dateOffer[1]) - 1, Integer.parseInt(dateOffer2[0])); // seteo los datos de la fecha de la oferta
	    int daysBetween = daysBetween(dateOfferTime.getTime(), now.getTime());
	    /**** Valido si la oferta sigue vigente ****/
	    if (daysBetween <= 7) {
		Boolean switched = false;
		if (switched) {/**** Recuperacion de oferta con los datos donde se quedo la oferta... ***/
		    OfferCheckpoint newOfferCheckpoint = new OfferCheckpoint();
		    newOfferCheckpoint.setUser(offerCheckpointBD.getUser());
		    if (offerCheckpointBD.getCustomerName() != null) {
			newOfferCheckpoint.setCustomerName(offerCheckpointBD.getCustomerName());
		    }
		    if (offerCheckpointBD.getCustomerLastName() != null) {
			newOfferCheckpoint.setCustomerLastName(offerCheckpointBD.getCustomerLastName());
		    }
		    if (offerCheckpointBD.getEmail() != null) {
			newOfferCheckpoint.setEmail(offerCheckpointBD.getEmail());
		    }
		    newOfferCheckpoint.setCarYear(offerCheckpointBD.getCarYear());
		    newOfferCheckpoint.setCarMake(offerCheckpointBD.getCarMake());
		    newOfferCheckpoint.setCarModel(offerCheckpointBD.getCarModel());
		    newOfferCheckpoint.setCarVersion(offerCheckpointBD.getCarModel());
		    newOfferCheckpoint.setCarVersion(offerCheckpointBD.getCarVersion());
		    newOfferCheckpoint.setCarKm(offerCheckpointBD.getCarKm());
		    if (offerCheckpointBD.getIdExteriorColor() != null) {
			newOfferCheckpoint.setIdExteriorColor(offerCheckpointBD.getIdExteriorColor());
		    }
		    newOfferCheckpoint.setZipCode(offerCheckpointBD.getZipCode());
		    newOfferCheckpoint.setIdStatus(offerCheckpointBD.getIdStatus());
		    newOfferCheckpoint.setOfferType(offerCheckpointBD.getOfferType());
		    if (offerCheckpointBD.getDescriptionOfferType() != null) {
			newOfferCheckpoint.setDescriptionOfferType(offerCheckpointBD.getDescriptionOfferType());
		    }
		    if (offerCheckpointBD.getOfferPunished() != null) {
			newOfferCheckpoint.setOfferPunished(offerCheckpointBD.getOfferPunished());
		    }
		    newOfferCheckpoint.setWishList(offerCheckpointBD.getWishList());
		    if (offerCheckpointBD.getMin30DaysOffer() != null) {
			newOfferCheckpoint.setMin30DaysOffer(offerCheckpointBD.getMin30DaysOffer());
		    }
		    if (offerCheckpointBD.getMax30DaysOffer() != null) {
			newOfferCheckpoint.setMax30DaysOffer(offerCheckpointBD.getMax30DaysOffer());
		    }
		    if (offerCheckpointBD.getMinInstantOffer() != null) {
			newOfferCheckpoint.setMinInstantOffer(offerCheckpointBD.getMinInstantOffer());
		    }
		    if (offerCheckpointBD.getMaxInstantOffer() != null) {
			newOfferCheckpoint.setMaxInstantOffer(offerCheckpointBD.getMaxInstantOffer());
		    }
		    if (offerCheckpointBD.getMinConsignationOffer() != null) {
			newOfferCheckpoint.setMinConsignationOffer(offerCheckpointBD.getMinConsignationOffer());
		    }
		    if (offerCheckpointBD.getMaxConsignationOffer() != null) {
			newOfferCheckpoint.setMaxConsignationOffer(offerCheckpointBD.getMaxConsignationOffer());
		    }
		    if (offerCheckpointBD.getInspectionLocation() != null) {
			newOfferCheckpoint.setInspectionLocation(offerCheckpointBD.getInspectionLocation());
		    }
		    if (offerCheckpointBD.getInspectionCenterDescription() != null) {
			newOfferCheckpoint.setInspectionCenterDescription(offerCheckpointBD.getInspectionCenterDescription());
		    }
		    if (offerCheckpointBD.getInspectionDate() != null) {
			newOfferCheckpoint.setInspectionDate(offerCheckpointBD.getInspectionDate());
		    }
		    if (offerCheckpointBD.getIdHourInpectionSlot() != null) {
			newOfferCheckpoint.setIdHourInpectionSlot(offerCheckpointBD.getIdHourInpectionSlot());
		    }
		    if (offerCheckpointBD.getCirculationCardUrl() != null) {
			newOfferCheckpoint.setCirculationCardUrl(offerCheckpointBD.getCirculationCardUrl());
		    }
		    if (offerCheckpointBD.getCarInvoiceUrl() != null) {
			newOfferCheckpoint.setCarInvoiceUrl(offerCheckpointBD.getCarInvoiceUrl());
		    }
		    if (offerCheckpointBD.getOfferDate() != null) {
			newOfferCheckpoint.setOfferDate(offerCheckpointBD.getOfferDate());
		    }
		    newOfferCheckpoint.setOfferRecovery(Constants.RECOVER_OFFER_YES);
		    if (offerCheckpointBD.getRootOffer() != null) {
			newOfferCheckpoint.setRootOffer(offerCheckpointBD.getRootOffer());
		    }
		    newOfferCheckpoint.setSku(offerCheckpointBD.getSku());
		    if (offerCheckpointBD.getInspectionLocationDescription() != null) {
			newOfferCheckpoint.setInspectionLocationDescription(offerCheckpointBD.getInspectionLocationDescription());
		    }
		    if (offerCheckpointBD.getHourInpectionSlotText() != null) {
			newOfferCheckpoint.setHourInpectionSlotText(offerCheckpointBD.getHourInpectionSlotText());
		    }
		    if (offerCheckpointBD.getStartHour() != null) {
			newOfferCheckpoint.setStartHour(offerCheckpointBD.getStartHour());
		    }
		    if (offerCheckpointBD.getEndHour() != null) {
			newOfferCheckpoint.setEndHour(offerCheckpointBD.getEndHour());
		    }

		    CarData carDataBD = carDataRepo.findBySku(offerCheckpointBD.getSku());
		    newOfferCheckpoint.setAwsKarsnapOffer(carDataBD.getAwsKarsnapOffer());
		    newOfferCheckpoint.setAwsLowestRange(carDataBD.getAwsLowestRange());
		    newOfferCheckpoint.setAwsHeighestRange(carDataBD.getAwsHeighestRange());
		    newOfferCheckpoint.setAwsInstantOffer(carDataBD.getAwsInstantOffer());
		    newOfferCheckpoint.setAwsLrInstantOffer(carDataBD.getAwsLrInstantOffer());
		    newOfferCheckpoint.setAwsHrInstantOffer(carDataBD.getAwsHrInstantOffer());
		    newOfferCheckpoint.setAwsCncOffer(carDataBD.getAwsCncOffer());
		    newOfferCheckpoint.setAwsLrCncOffer(carDataBD.getAwsLrCncOffer());
		    newOfferCheckpoint.setAwsHrCncOffer(carDataBD.getAwsHrCncOffer());
		    newOfferCheckpoint.setAwsPc30dOrg(carDataBD.getAwsPc30dOrg());
		    newOfferCheckpoint.setAwsPcInsOrg(carDataBD.getAwsPcInsOrg());
		    newOfferCheckpoint.setAwsPcCncOrg(carDataBD.getAwsPcCncOrg());
		    newOfferCheckpoint.setAwsOffer(carDataBD.getAwsOffer());
		    newOfferCheckpoint.setAwsPrecioVenta(carDataBD.getAwsPrecioVenta());
		    newOfferCheckpoint.setRimMaterial(carDataBD.getRimMaterial());
		    newOfferCheckpoint.setLights(carDataBD.getLights());
		    newOfferCheckpoint.setAir(carDataBD.getAir());
		    newOfferCheckpoint.setAwsFechaActOffer(carDataBD.getAwsFechaActOffer());


		    if (offerCheckpointBD.getStatusDetail() != null) {
			newOfferCheckpoint.setStatusDetail(offerCheckpointBD.getStatusDetail());
		    }
		    if (offerCheckpointBD.getSalePrice() != null) {
			newOfferCheckpoint.setSalePrice(offerCheckpointBD.getSalePrice());
		    }
		    if (offerCheckpointBD.getBuyPrice() != null) {
			newOfferCheckpoint.setBuyPrice(offerCheckpointBD.getBuyPrice());
		    }
		    if (offerCheckpointBD.getOriginalPrice30D() != null) {
			newOfferCheckpoint.setOriginalPrice30D(offerCheckpointBD.getOriginalPrice30D());
		    }
		    if (offerCheckpointBD.getOriginalPriceInstance() != null) {
			newOfferCheckpoint.setOriginalPriceInstance(offerCheckpointBD.getOriginalPriceInstance());
		    }
		    if (offerCheckpointBD.getOriginalPriceConsignation() != null) {
			newOfferCheckpoint.setOriginalPriceConsignation(offerCheckpointBD.getOriginalPriceConsignation());
		    }
		    if (offerCheckpointBD.getMinKm() != null) {
			newOfferCheckpoint.setMinKm(offerCheckpointBD.getMinKm());
		    }
		    if (offerCheckpointBD.getMaxKm() != null) {
			newOfferCheckpoint.setMaxKm(offerCheckpointBD.getMaxKm());
		    }
		    if (offerCheckpointBD.getFactorKmApplied() != null) {
			newOfferCheckpoint.setFactorKmApplied(offerCheckpointBD.getFactorKmApplied());
		    }
		    if (offerCheckpointBD.getSalePriceKavak() != null) {
			newOfferCheckpoint.setSalePriceKavak(offerCheckpointBD.getSalePriceKavak());
		    }
		    if (offerCheckpointBD.getMarketPrice() != null) {
			newOfferCheckpoint.setMarketPrice(offerCheckpointBD.getMarketPrice());
		    }
		    if (offerCheckpointBD.getQuantitySamples() != null) {
			newOfferCheckpoint.setQuantitySamples(offerCheckpointBD.getQuantitySamples());
		    }
		    if (offerCheckpointBD.getAverageDateSamples() != null) {
			newOfferCheckpoint.setAverageDateSamples(offerCheckpointBD.getAverageDateSamples());
		    }
		    if (offerCheckpointBD.getUpdateDateOffer() != null) {
			newOfferCheckpoint.setUpdateDateOffer(offerCheckpointBD.getUpdateDateOffer());
		    }
		    if (offerCheckpointBD.getFullVersion() != null) {
			newOfferCheckpoint.setFullVersion(offerCheckpointBD.getFullVersion());
		    }
		    if (offerCheckpointBD.getOfferDetail() != null) {
			newOfferCheckpoint.setOfferDetail(offerCheckpointBD.getOfferDetail());
		    }
		    if (offerCheckpointBD.getTrustedOffer() != null) {
			newOfferCheckpoint.setTrustedOffer(offerCheckpointBD.getTrustedOffer());
		    }
		    if (offerCheckpointBD.getOther3() != null) {
			newOfferCheckpoint.setOther3(offerCheckpointBD.getOther3());
		    }
		    if (offerCheckpointBD.getOther4() != null) {
			newOfferCheckpoint.setOther4(offerCheckpointBD.getOther4());
		    }
		    if (offerCheckpointBD.getOther5() != null) {
			newOfferCheckpoint.setOther5(offerCheckpointBD.getOther5());
		    }
		    if (offerCheckpointBD.getOther6() != null) {
			newOfferCheckpoint.setOther6(offerCheckpointBD.getOther6());
		    }
		    if (offerCheckpointBD.getOther7() != null) {
			newOfferCheckpoint.setOther7(offerCheckpointBD.getOther7());
		    }
		    if (offerCheckpointBD.getOther8() != null) {
			newOfferCheckpoint.setOther8(offerCheckpointBD.getOther8());
		    }
		    if (offerCheckpointBD.getAssignedEm() != null) {
			newOfferCheckpoint.setAssignedEm(offerCheckpointBD.getAssignedEm());
		    }
		    if (offerCheckpointBD.getOfferSource() != null) {
			newOfferCheckpoint.setOfferSource(offerCheckpointBD.getOfferSource());
		    }
		    if (offerCheckpointBD.getCarPhoto() != null) {
			newOfferCheckpoint.setCarPhoto(offerCheckpointBD.getCarPhoto());
		    }
		    if (offerCheckpointBD.getIdExteriorColor() != null) {
			newOfferCheckpoint.setIdExteriorColor(offerCheckpointBD.getIdExteriorColor());
		    }
		    if (offerCheckpointBD.getVersionGuiaAutometrica() != null) {
			newOfferCheckpoint.setVersionGuiaAutometrica(offerCheckpointBD.getVersionGuiaAutometrica());
		    }
		    newOfferCheckpoint.setSendEmail(false);
		    newOfferCheckpoint.setSentNetsuite((long) 0);
		    if (offerCheckpointBD.getCodeNetsuiteItem() != null) {
			newOfferCheckpoint.setCodeNetsuiteItem(offerCheckpointBD.getCodeNetsuiteItem());
		    }
		    if (offerCheckpointBD.getDuplicatedOfferControl() != null) {
			newOfferCheckpoint.setDuplicatedOfferControl(offerCheckpointBD.getDuplicatedOfferControl());
		    }
		    if (offerCheckpointBD.getNetsuiteOpportunityId() != null) {
			newOfferCheckpoint.setNetsuiteOpportunityId(offerCheckpointBD.getNetsuiteOpportunityId());
		    }
		    if (offerCheckpointBD.getNetsuiteOpportunityURL() != null) {
			newOfferCheckpoint.setNetsuiteOpportunityURL(offerCheckpointBD.getNetsuiteOpportunityURL());
		    }
		    if (offerCheckpointBD.getNetsuiteIdAttemps() != null) {
			newOfferCheckpoint.setNetsuiteIdAttemps(offerCheckpointBD.getNetsuiteIdAttemps());
		    }
		    if (offerCheckpointBD.getNetsuiteIdUpdateDate() != null) {
			newOfferCheckpoint.setNetsuiteIdUpdateDate(offerCheckpointBD.getNetsuiteIdUpdateDate());
		    }
		    if (offerCheckpointBD.getSegmentType() != null) {
			newOfferCheckpoint.setSegmentType(offerCheckpointBD.getSegmentType());
		    }
		    if (offerCheckpointBD.getPcVendeTuAuto() != null) {
			newOfferCheckpoint.setPcVendeTuAuto(offerCheckpointBD.getPcVendeTuAuto());
		    }
		    if (offerCheckpointBD.getPcEbc() != null) {
			newOfferCheckpoint.setPcEbc(offerCheckpointBD.getPcEbc());
		    }
		    if (offerCheckpointBD.getPvEbc() != null) {
			newOfferCheckpoint.setPvEbc(offerCheckpointBD.getPvEbc());
		    }
		    if (offerCheckpointBD.getVersionEbc() != null) {
			newOfferCheckpoint.setVersionEbc(offerCheckpointBD.getVersionEbc());
		    }
		    if (offerCheckpointBD.getAvgKm() != null) {
			newOfferCheckpoint.setAvgKm(offerCheckpointBD.getAvgKm());
		    }
		    if (offerCheckpointBD.getSetPriceInst() != null) {
			newOfferCheckpoint.setSetPriceInst(offerCheckpointBD.getSetPriceInst());
		    }
		    if (offerCheckpointBD.getInternalUserId() != null) {
			newOfferCheckpoint.setInternalUserId(offerCheckpointBD.getInternalUserId());
		    }
		    if (offerCheckpointBD.getCustomerComment() != null) {
			newOfferCheckpoint.setCustomerComment(offerCheckpointBD.getCustomerComment());
		    }
		    if (offerCheckpointBD.getCustomerVoice() != null) {
			newOfferCheckpoint.setCustomerVoice(offerCheckpointBD.getCustomerVoice());
		    }


		    OfferCheckpoint offerCheckpointUrlBD = offerCheckpointRepo.save(newOfferCheckpoint);
		    String finalHash = generateRecoverOfferURL(offerCheckpointUrlBD.getId());
		    newOfferCheckpoint.setUrlRecoverOffer(finalHash);
		    
		    offerCheckpointRepo.save(newOfferCheckpoint);
		    enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		    GetRecuperarOfertaResponseDTO recuperarOfertaResponseDTO = new GetRecuperarOfertaResponseDTO();

		    recuperarOfertaResponseDTO.setOfferCheckpointId(newOfferCheckpoint.getId());
		    recuperarOfertaResponseDTO.setOfferDate(offerCheckpointBD.getOfferDate().toString());
		    Long step = offerCheckpointBD.getIdStatus();
		    Integer stepTransform = 0;
		    if (step == 19) {
			stepTransform = 1;
		    }
		    if (step == 20) {
			stepTransform = 2;
		    }
		    if (step == 21) {
			stepTransform = 3;
		    }
		    if (step == 22) {
			stepTransform = 4;
		    }
		    recuperarOfertaResponseDTO.setStep(stepTransform);
		    recuperarOfertaResponseDTO.setYear(offerCheckpointBD.getCarYear());
		    recuperarOfertaResponseDTO.setMake(offerCheckpointBD.getCarMake());
		    recuperarOfertaResponseDTO.setModel(offerCheckpointBD.getCarModel());
		    recuperarOfertaResponseDTO.setTrim(offerCheckpointBD.getCarVersion());
		    recuperarOfertaResponseDTO.setColor(offerCheckpointBD.getIdExteriorColor());
		    recuperarOfertaResponseDTO.setSku(offerCheckpointBD.getSku());
		    recuperarOfertaResponseDTO.setKm(offerCheckpointBD.getCarKm());
		    recuperarOfertaResponseDTO.setPostalCode(offerCheckpointBD.getZipCode());
		    ZipCode costoEnvioCodigoPostal = costoEnvioCodigoPostalRepo.findByZipCode(Long.valueOf(offerCheckpointBD.getZipCode()));
		    if (costoEnvioCodigoPostal == null || !costoEnvioCodigoPostal.isServedZone()) {
			recuperarOfertaResponseDTO.setServedZone(false);
		    } else {
			recuperarOfertaResponseDTO.setServedZone(true);
		    }

		    Long statusStep = offerCheckpointBD.getIdStatus();
		    // Setear desde aqui el offerResponseDTO//
		    List<GenericDTO> listGenerateOffer = new ArrayList<>();
		    OffersResponseDTO offerResponse = new OffersResponseDTO();
		    offerResponse.setYear(offerCheckpointBD.getCarYear());
		    offerResponse.setMake(offerCheckpointBD.getCarMake());
		    offerResponse.setModel(offerCheckpointBD.getCarModel());
		    offerResponse.setTrim(offerCheckpointBD.getCarVersion());
		    offerResponse.setColor(offerCheckpointBD.getIdExteriorColor());
		    offerResponse.setSku(offerCheckpointBD.getSku());
		    offerResponse.setKm(offerCheckpointBD.getCarKm());
		    offerResponse.setZipCode(offerCheckpointBD.getZipCode());
		    String email = offerCheckpointBD.getEmail();
		    User userBD = userRepo.findByEmailAndRole(email, Constants.CUSTOMER_ROLE);
		    UserMeta phoneBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, userBD.getId());
		    UserMeta addressBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_ADDRESS, userBD.getId());
		    UserCheckPointsDTO userCheckpoints = new UserCheckPointsDTO();
		    userCheckpoints.setEmail(email);
		    userCheckpoints.setCustomerName(userBD.getName());
		    userCheckpoints.setIdUser(userBD.getId());
		    if (addressBD == null) {
			userCheckpoints.setAddress("");
		    } else {
			userCheckpoints.setAddress(addressBD.getMetaValue().toString());
		    }
		    userCheckpoints.setPhone(phoneBD.getMetaValue().toString());
		    offerResponse.setUser(userCheckpoints);
		    if (costoEnvioCodigoPostal == null || !costoEnvioCodigoPostal.isServedZone()) {
			offerResponse.setServedZone(false);
		    } else {
			offerResponse.setServedZone(true);
		    }

		    // Generacion de las ofertas a partir de los datos guardados de la oferta anterior/
		    if (offerCheckpointBD.getMinConsignationOffer() != null) {
			GenericDTO genericTypeConsessionDTO = new GenericDTO();
			if (costoEnvioCodigoPostal == null || costoEnvioCodigoPostal.isServedZone()) {
			    genericTypeConsessionDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYCONSIGMENT.getAlias()).getId());
			} else {
			    genericTypeConsessionDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYCONSIGMENT.getAliasNotServed()).getId());
			}

			genericTypeConsessionDTO.setTypeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYCONSIGMENT.toString()).getType());
			genericTypeConsessionDTO.setCodeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYCONSIGMENT.toString()).getCode());
			genericTypeConsessionDTO.setMin(offerCheckpointBD.getMinConsignationOffer());
			genericTypeConsessionDTO.setMax(offerCheckpointBD.getMaxConsignationOffer());
			Optional<OfferType> offerTypeConsessionBD = offerTypeRepo.findById(OfferTypeEnum.BUYCONSIGMENT.getId());
			genericTypeConsessionDTO.setDescrption(offerTypeConsessionBD.get().getDescription());
			listGenerateOffer.add(genericTypeConsessionDTO);
		    }
		    if (offerCheckpointBD.getMin30DaysOffer() != null) {
			GenericDTO genericTypeOffer30DaysDTO = new GenericDTO();
			if (costoEnvioCodigoPostal == null || costoEnvioCodigoPostal.isServedZone()) {
			    genericTypeOffer30DaysDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUY30DAYS.getAlias()).getId());
			} else {
			    genericTypeOffer30DaysDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUY30DAYS.getAliasNotServed()).getId());
			}
			genericTypeOffer30DaysDTO.setTypeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUY30DAYS.toString()).getType());
			genericTypeOffer30DaysDTO.setCodeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUY30DAYS.toString()).getCode());
			genericTypeOffer30DaysDTO.setMin(offerCheckpointBD.getMin30DaysOffer());
			genericTypeOffer30DaysDTO.setMax(offerCheckpointBD.getMax30DaysOffer());
			Optional<OfferType> offerType30DBD = offerTypeRepo.findById(OfferTypeEnum.BUY30DAYS.getId());
			genericTypeOffer30DaysDTO.setDescrption(offerType30DBD.get().getDescription());
			listGenerateOffer.add(genericTypeOffer30DaysDTO);

		    }
		    if (offerCheckpointBD.getMinInstantOffer() != null) {
			GenericDTO genericTypeOfferInstantDTO = new GenericDTO();
			if (costoEnvioCodigoPostal == null || costoEnvioCodigoPostal.isServedZone()) {
			    genericTypeOfferInstantDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYINMEDIATE.getAlias()).getId());
			} else {
			    genericTypeOfferInstantDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYINMEDIATE.getAliasNotServed()).getId());
			}
			genericTypeOfferInstantDTO.setTypeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYINMEDIATE.toString()).getType());
			genericTypeOfferInstantDTO.setCodeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYINMEDIATE.toString()).getCode());
			genericTypeOfferInstantDTO.setMin(offerCheckpointBD.getMinInstantOffer());
			genericTypeOfferInstantDTO.setMax(offerCheckpointBD.getMaxInstantOffer());
			Optional<OfferType> offerTypeInstantBD = offerTypeRepo.findById(OfferTypeEnum.BUYINMEDIATE.getId());
			genericTypeOfferInstantDTO.setDescrption(offerTypeInstantBD.get().getDescription());
			listGenerateOffer.add(genericTypeOfferInstantDTO);
		    }

		    offerResponse.setId(newOfferCheckpoint.getId());
		    if (offerCheckpointBD.getWishList() == 1) {
			offerResponse.setWishList(true);
		    } else {
			offerResponse.setWishList(false);
		    }

		    offerResponse.setUrlRecoverOffer(finalHash);
		    offerResponse.setListData(listGenerateOffer);
		    recuperarOfertaResponseDTO.setOffers(offerResponse);
		    if (statusStep != 19) {
			recuperarOfertaResponseDTO.setOfferSelected(offerCheckpointBD.getDescriptionOfferType());
		    }
		    // recuperarOfertaResponseDTO.setOffers(listTypeOffer);
		    // validar que los datos de la location existan//
		    if (offerCheckpointBD.getInspectionLocation() != null && offerCheckpointBD.getInspectionLocation() != 31) {
			InspectionLocation inspectionLocation = inspectionLocationRepo.findByName(offerCheckpointBD.getInspectionCenterDescription());
			recuperarOfertaResponseDTO.setInspectionDate(offerCheckpointBD.getInspectionDate());
			recuperarOfertaResponseDTO.setInspection_hour_block(offerCheckpointBD.getIdHourInpectionSlot().toString());
			recuperarOfertaResponseDTO.setInspection_location(inspectionLocation.getId().toString());
			recuperarOfertaResponseDTO.setInspectionAddress(inspectionLocation.getAddress());
			recuperarOfertaResponseDTO.setColony_loc(inspectionLocation.getColony());
			recuperarOfertaResponseDTO.setName_loc(inspectionLocation.getName());
			recuperarOfertaResponseDTO.setHour_inspection_slot_text(offerCheckpointBD.getHourInpectionSlotText());

		    }
		    if (offerCheckpointBD.getInspectionLocation() != null && offerCheckpointBD.getInspectionLocation() == 31) {
			recuperarOfertaResponseDTO.setInspectionDate(offerCheckpointBD.getInspectionDate());
			recuperarOfertaResponseDTO.setInspection_hour_block(offerCheckpointBD.getIdHourInpectionSlot().toString());
			recuperarOfertaResponseDTO.setInspection_location(Integer.toString(0));
			recuperarOfertaResponseDTO.setInspectionAddress(offerCheckpointBD.getInspectionLocationDescription());
			recuperarOfertaResponseDTO.setColony_loc(offerCheckpointBD.getZipCode());
			recuperarOfertaResponseDTO.setName_loc(offerCheckpointBD.getInspectionCenterDescription());
			recuperarOfertaResponseDTO.setHour_inspection_slot_text(offerCheckpointBD.getHourInpectionSlotText());
			String address[] = offerCheckpointBD.getInspectionLocationDescription().split(",");
			recuperarOfertaResponseDTO.setColony(address[4]);
			recuperarOfertaResponseDTO.setMunicipality(address[3]);
			recuperarOfertaResponseDTO.setStreet_customer(address[0]);
			recuperarOfertaResponseDTO.setNum_interior(address[2]);
			recuperarOfertaResponseDTO.setNum_exterior(address[1]);
		    }
		    OfferCheckpoint offerRecoverySetUrl = offerCheckpointRepo.findByIdOfferCheckpoints(offerCheckpointBD.getId());
		    //offerRecoverySetUrl.setUrlRecoverOffer(null);
		    offerCheckpointRepo.save(offerRecoverySetUrl);
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setData(recuperarOfertaResponseDTO);

		} else {
		    OfferCheckpoint newOfferCheckpoint = new OfferCheckpoint();
		    newOfferCheckpoint.setIdExteriorColor(offerCheckpointBD.getIdExteriorColor());
		    newOfferCheckpoint.setMinKm(offerCheckpointBD.getMinKm());
		    newOfferCheckpoint.setMaxKm(offerCheckpointBD.getMaxKm());
		    Optional<User> user = userRepo.findById(offerCheckpointBD.getIdUser());
		    newOfferCheckpoint.setUser(user.get());

		    // Nombre y Apellido
		    newOfferCheckpoint.setCustomerName(user.get().getDTO().getFirstName());
		    newOfferCheckpoint.setCustomerLastName(user.get().getDTO().getLastName());

		    newOfferCheckpoint.setEmail(user.get().getEmail());
		    newOfferCheckpoint.setSku(offerCheckpointBD.getSku());
		    newOfferCheckpoint.setCarYear(offerCheckpointBD.getCarYear());
		    newOfferCheckpoint.setCarMake(offerCheckpointBD.getCarMake());
		    newOfferCheckpoint.setCarModel(offerCheckpointBD.getCarModel());
		    newOfferCheckpoint.setCarVersion(offerCheckpointBD.getCarVersion());
		    newOfferCheckpoint.setCarKm(offerCheckpointBD.getCarKm());
		    newOfferCheckpoint.setZipCode(offerCheckpointBD.getZipCode());
		    newOfferCheckpoint.setOfferPunished(offerCheckpointBD.getOfferPunished());
		    newOfferCheckpoint.setOriginalPrice30D(offerCheckpointBD.getOriginalPrice30D());
		    newOfferCheckpoint.setOriginalPriceInstance(offerCheckpointBD.getOriginalPriceInstance());
		    newOfferCheckpoint.setOriginalPriceConsignation(offerCheckpointBD.getOriginalPriceConsignation());
		    newOfferCheckpoint.setSalePriceKavak(offerCheckpointBD.getSalePriceKavak());
		    newOfferCheckpoint.setQuantitySamples(offerCheckpointBD.getQuantitySamples());
		    newOfferCheckpoint.setAverageDateSamples(offerCheckpointBD.getAverageDateSamples());
		    newOfferCheckpoint.setMin30DaysOffer(offerCheckpointBD.getMin30DaysOffer());
		    newOfferCheckpoint.setMax30DaysOffer(offerCheckpointBD.getMax30DaysOffer());
		    newOfferCheckpoint.setMinConsignationOffer(offerCheckpointBD.getMinConsignationOffer());
		    newOfferCheckpoint.setMaxConsignationOffer(offerCheckpointBD.getMaxConsignationOffer());
		    newOfferCheckpoint.setMinInstantOffer(offerCheckpointBD.getMinInstantOffer());
		    newOfferCheckpoint.setMaxInstantOffer(offerCheckpointBD.getMaxInstantOffer());
		    newOfferCheckpoint.setUpdateDateOffer(offerCheckpointBD.getUpdateDateOffer());
		    newOfferCheckpoint.setVersionGuiaAutometrica(offerCheckpointBD.getVersionGuiaAutometrica());
		    newOfferCheckpoint.setFullVersion(offerCheckpointBD.getFullVersion());
		    newOfferCheckpoint.setOfferDetail(offerCheckpointBD.getOfferDetail());
		    newOfferCheckpoint.setTrustedOffer(offerCheckpointBD.getTrustedOffer());
		    newOfferCheckpoint.setMarketPrice(offerCheckpointBD.getMarketPrice());
		    newOfferCheckpoint.setOther3(offerCheckpointBD.getOther3());
		    newOfferCheckpoint.setOther4(offerCheckpointBD.getOther4());
		    newOfferCheckpoint.setOther5(offerCheckpointBD.getOther5());
		    newOfferCheckpoint.setOther6(offerCheckpointBD.getOther6());
		    newOfferCheckpoint.setOther7(offerCheckpointBD.getOther7());
		    newOfferCheckpoint.setOther8(offerCheckpointBD.getOther8());
		    newOfferCheckpoint.setSegmentType(offerCheckpointBD.getSegmentType());
		    newOfferCheckpoint.setPcVendeTuAuto(offerCheckpointBD.getPcVendeTuAuto());
		    newOfferCheckpoint.setPcEbc(offerCheckpointBD.getPcEbc());
		    newOfferCheckpoint.setPvEbc(offerCheckpointBD.getPvEbc());
		    newOfferCheckpoint.setVersionEbc(offerCheckpointBD.getVersionEbc());
		    newOfferCheckpoint.setAvgKm(offerCheckpointBD.getAvgKm());
		    newOfferCheckpoint.setSetPriceInst(offerCheckpointBD.getSetPriceInst());
		    newOfferCheckpoint.setStatusDetail(offerCheckpointBD.getStatusDetail());
		    newOfferCheckpoint.setFactorKmApplied(offerCheckpointBD.getFactorKmApplied());

		    // AWS
		    newOfferCheckpoint.setAwsKarsnapOffer(offerCheckpointBD.getAwsKarsnapOffer());
		    newOfferCheckpoint.setAwsLowestRange(offerCheckpointBD.getAwsLowestRange());
		    newOfferCheckpoint.setAwsHeighestRange(offerCheckpointBD.getAwsHeighestRange());
		    newOfferCheckpoint.setAwsInstantOffer(offerCheckpointBD.getAwsInstantOffer());
		    newOfferCheckpoint.setAwsLrInstantOffer(offerCheckpointBD.getAwsLrInstantOffer());
		    newOfferCheckpoint.setAwsHrInstantOffer(offerCheckpointBD.getAwsHrInstantOffer());
		    newOfferCheckpoint.setAwsCncOffer(offerCheckpointBD.getAwsCncOffer());
		    newOfferCheckpoint.setAwsLrCncOffer(offerCheckpointBD.getAwsLrCncOffer());
		    newOfferCheckpoint.setAwsHrCncOffer(offerCheckpointBD.getAwsHrCncOffer());
		    newOfferCheckpoint.setAwsPc30dOrg(offerCheckpointBD.getAwsPc30dOrg());
		    newOfferCheckpoint.setAwsPcInsOrg(offerCheckpointBD.getAwsPcInsOrg());
		    newOfferCheckpoint.setAwsPcCncOrg(offerCheckpointBD.getAwsPcCncOrg());
		    newOfferCheckpoint.setAwsOffer(offerCheckpointBD.getAwsOffer());
		    newOfferCheckpoint.setAwsPrecioVenta(offerCheckpointBD.getAwsPrecioVenta());
		    newOfferCheckpoint.setRimMaterial(offerCheckpointBD.getRimMaterial());
		    newOfferCheckpoint.setLights(offerCheckpointBD.getLights());
		    newOfferCheckpoint.setAir(offerCheckpointBD.getAir());
		    newOfferCheckpoint.setAwsFechaActOffer(offerCheckpointBD.getAwsFechaActOffer());
		    Timestamp actualTime = new Timestamp(System.currentTimeMillis());
		    newOfferCheckpoint.setOfferDate(actualTime);
		    newOfferCheckpoint.setOfferRecovery(Constants.RECOVER_OFFER_YES);
		    newOfferCheckpoint.setWishList(offerCheckpointBD.getWishList());
		    newOfferCheckpoint.setSalePrice(offerCheckpointBD.getSalePrice());
		    newOfferCheckpoint.setBuyPrice(offerCheckpointBD.getBuyPrice());
		    newOfferCheckpoint.setOfferSource(offerCheckpointBD.getOfferSource());
		    newOfferCheckpoint.setInternalUserId(offerCheckpointBD.getInternalUserId());
		    MetaValue metaValueStatusCDR = metaValueRepo.findByAlias(StatusCheckpointOfferEnum.CPCD.getAlias());
		    newOfferCheckpoint.setIdStatus(metaValueStatusCDR.getId());
		    newOfferCheckpoint.setOfferType(offerCheckpointBD.getOfferType());
		    newOfferCheckpoint.setDescriptionOfferType(offerCheckpointBD.getDescriptionOfferType());
		    newOfferCheckpoint.setSentNetsuite(0L);
		    newOfferCheckpoint.setDuplicatedOfferControl(offerCheckpointBD.getDuplicatedOfferControl());
		    
		    OfferCheckpoint offerCheckpointUrlBD = offerCheckpointRepo.save(newOfferCheckpoint);
		    String finalHash = generateRecoverOfferURL(offerCheckpointUrlBD.getId());
		    newOfferCheckpoint.setUrlRecoverOffer(finalHash);
		    OfferCheckpoint offerCheckpointReturnSaveBD = offerCheckpointRepo.save(newOfferCheckpoint);
		    offerCheckpointReturnSaveBD.setCodeNetsuiteItem(offerCheckpointReturnSaveBD.getId());

		    MetaValue metaValueZoneVariousOffers = metaValueRepo.findByAlias(OfferTypeEnum.VARIOUSOFFERS.getAlias());
		    enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		    GetRecoveryOfferResponseStepOneDTO recoveryOfferStepOne = new GetRecoveryOfferResponseStepOneDTO();
		    recoveryOfferStepOne.setOfferCheckpointId(offerCheckpointReturnSaveBD.getId());
		    recoveryOfferStepOne.setOfferDate(actualTime.toString());
		    recoveryOfferStepOne.setPostalCode(offerCheckpointReturnSaveBD.getZipCode());
		    recoveryOfferStepOne.setStep(1);
		    ZipCode costoEnvioCodigoPostal = costoEnvioCodigoPostalRepo.findByZipCode(Long.valueOf(offerCheckpointReturnSaveBD.getZipCode()));
		    if (costoEnvioCodigoPostal == null || !costoEnvioCodigoPostal.isServedZone()) {
			recoveryOfferStepOne.setServedZone(false);
		    } else {
			recoveryOfferStepOne.setServedZone(true);
		    }
		    List<GenericDTO> listGenerateOffer = new ArrayList<>();
		    OffersResponseDTO offerResponse = new OffersResponseDTO();
		    offerResponse.setColor(offerCheckpointReturnSaveBD.getIdExteriorColor());
		    offerResponse.setYear(offerCheckpointReturnSaveBD.getCarYear());
		    offerResponse.setKm(offerCheckpointReturnSaveBD.getCarKm());
		    offerResponse.setMake(offerCheckpointReturnSaveBD.getCarMake());
		    offerResponse.setModel(offerCheckpointReturnSaveBD.getCarModel());
		    offerResponse.setSku(offerCheckpointReturnSaveBD.getSku());
		    offerResponse.setTrim(offerCheckpointReturnSaveBD.getCarVersion());
		    offerResponse.setZipCode(offerCheckpointReturnSaveBD.getZipCode());
		    String email = offerCheckpointReturnSaveBD.getEmail();
		    User userBD = userRepo.findByEmailAndRole(email, Constants.CUSTOMER_ROLE);
		    UserMeta phoneBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_PHONE, userBD.getId());
		    UserMeta addressBD = userMetaRepo.findByMetaKeyAndUserId(Constants.META_VALUE_ADDRESS, userBD.getId());
		    UserCheckPointsDTO userCheckpoints = new UserCheckPointsDTO();
		    userCheckpoints.setEmail(email);
		    userCheckpoints.setCustomerName(userBD.getName());
		    userCheckpoints.setIdUser(userBD.getId());
		    if (addressBD == null) {
			userCheckpoints.setAddress("");
		    } else {
			userCheckpoints.setAddress(addressBD.getMetaValue().toString());
		    }
		    userCheckpoints.setPhone(phoneBD.getMetaValue().toString());
		    offerResponse.setUser(userCheckpoints);

		    if (costoEnvioCodigoPostal == null || !costoEnvioCodigoPostal.isServedZone()) {
			offerResponse.setServedZone(false);
		    } else {
			offerResponse.setServedZone(true);
		    }

		    // Generacion de las ofertas a partir de los datos guardados de la oferta anterior/
		    if (offerCheckpointReturnSaveBD.getMinConsignationOffer() != null) {
			GenericDTO genericTypeConsessionDTO = new GenericDTO();
			if (costoEnvioCodigoPostal == null || costoEnvioCodigoPostal.isServedZone()) {
			    genericTypeConsessionDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYCONSIGMENT.getAlias()).getId());
			} else {
			    genericTypeConsessionDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYCONSIGMENT.getAliasNotServed()).getId());
			}

			genericTypeConsessionDTO.setTypeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYCONSIGMENT.toString()).getType());
			genericTypeConsessionDTO.setCodeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYCONSIGMENT.toString()).getCode());
			genericTypeConsessionDTO.setMin(offerCheckpointReturnSaveBD.getMinConsignationOffer());
			genericTypeConsessionDTO.setMax(offerCheckpointReturnSaveBD.getMaxConsignationOffer());
			Optional<OfferType> offerTypeConsessionBD = offerTypeRepo.findById(OfferTypeEnum.BUYCONSIGMENT.getId());
			genericTypeConsessionDTO.setDescrption(offerTypeConsessionBD.get().getDescription());
			listGenerateOffer.add(genericTypeConsessionDTO);
		    }
		    if (offerCheckpointReturnSaveBD.getMin30DaysOffer() != null) {
			GenericDTO genericTypeOffer30DaysDTO = new GenericDTO();
			if (costoEnvioCodigoPostal == null || costoEnvioCodigoPostal.isServedZone()) {
			    genericTypeOffer30DaysDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUY30DAYS.getAlias()).getId());
			} else {
			    genericTypeOffer30DaysDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUY30DAYS.getAliasNotServed()).getId());
			}
			genericTypeOffer30DaysDTO.setTypeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUY30DAYS.toString()).getType());
			genericTypeOffer30DaysDTO.setCodeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUY30DAYS.toString()).getCode());
			genericTypeOffer30DaysDTO.setMin(offerCheckpointReturnSaveBD.getMin30DaysOffer());
			genericTypeOffer30DaysDTO.setMax(offerCheckpointReturnSaveBD.getMax30DaysOffer());
			Optional<OfferType> offerType30DBD = offerTypeRepo.findById(OfferTypeEnum.BUY30DAYS.getId());
			genericTypeOffer30DaysDTO.setDescrption(offerType30DBD.get().getDescription());
			listGenerateOffer.add(genericTypeOffer30DaysDTO);

		    }
		    if (offerCheckpointReturnSaveBD.getMinInstantOffer() != null) {
			GenericDTO genericTypeOfferInstantDTO = new GenericDTO();
			if (costoEnvioCodigoPostal == null || costoEnvioCodigoPostal.isServedZone()) {
			    genericTypeOfferInstantDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYINMEDIATE.getAlias()).getId());
			} else {
			    genericTypeOfferInstantDTO.setId(metaValueRepo.findByAlias(OfferTypeEnum.BUYINMEDIATE.getAliasNotServed()).getId());
			}
			genericTypeOfferInstantDTO.setTypeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYINMEDIATE.toString()).getType());
			genericTypeOfferInstantDTO.setCodeOffer(OfferTypeEnum.getByType(OfferTypeEnum.BUYINMEDIATE.toString()).getCode());
			genericTypeOfferInstantDTO.setMin(offerCheckpointReturnSaveBD.getMinInstantOffer());
			genericTypeOfferInstantDTO.setMax(offerCheckpointReturnSaveBD.getMaxInstantOffer());
			Optional<OfferType> offerTypeInstantBD = offerTypeRepo.findById(OfferTypeEnum.BUYINMEDIATE.getId());
			genericTypeOfferInstantDTO.setDescrption(offerTypeInstantBD.get().getDescription());
			listGenerateOffer.add(genericTypeOfferInstantDTO);
		    }

		    offerResponse.setId(newOfferCheckpoint.getId());
		    if (offerCheckpointReturnSaveBD.getWishList() == 1) {
			offerResponse.setWishList(true);
		    } else {
			offerResponse.setWishList(false);
		    }
		    if (listGenerateOffer.size() > 1) { // varias ofertas
			offerCheckpointReturnSaveBD.setOfferType(metaValueZoneVariousOffers.getId());
			offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueZoneVariousOffers.getOptionName());

			MetaValue metaValueStatusNotAcceptoffers = metaValueRepo.findByAlias(StatusDetailOfferEnum.NOACCEPTOFFER.getAlias());
			offerCheckpointReturnSaveBD.setStatusDetail(metaValueStatusNotAcceptoffers.getOptionName());
		    } else {// solo una oferta
			GenericDTO genericTypeOffer = listGenerateOffer.get(0);
			if (!offerResponse.isServedZone()) { // Zona Not Servida
			    MetaValue metaValueZoneServed = metaValueRepo.findByAlias(OfferTypeEnum.getByType(genericTypeOffer.getCodeOffer()).getAlias());
			    offerCheckpointReturnSaveBD.setOfferType(metaValueZoneServed.getId());
			    offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueZoneServed.getOptionName());

			    MetaValue metaValueStatusNotZS = metaValueRepo.findByAlias(StatusDetailOfferEnum.NOZONESERVED.getAlias());
			    offerCheckpointReturnSaveBD.setStatusDetail(metaValueStatusNotZS.getOptionName());
			} else {

			    MetaValue metaValueZoneNotServed = metaValueRepo.findByAlias(OfferTypeEnum.getByType(genericTypeOffer.getCodeOffer()).getAliasNotServed());
			    offerCheckpointReturnSaveBD.setOfferType(metaValueZoneNotServed.getId());
			    offerCheckpointReturnSaveBD.setDescriptionOfferType(metaValueZoneNotServed.getOptionName());

			    MetaValue metaValueStatusNotZS = metaValueRepo.findByAlias(StatusDetailOfferEnum.NOACCEPTOFFER.getAlias());
			    offerCheckpointReturnSaveBD.setStatusDetail(metaValueStatusNotZS.getOptionName());
			}
		    }
		    OfferCheckpoint offerRecoverySetUrl = offerCheckpointRepo.findByIdOfferCheckpoints(offerCheckpointBD.getId());
		    //offerRecoverySetUrl.setUrlRecoverOffer(null);
		    offerCheckpointRepo.save(offerRecoverySetUrl);
		    try {
			//url para tracking
			offerResponse.setUrlTrackingRecoverOffer(generateTrackingRecoverOfferURL(offerCheckpointBD));
		    } catch (Exception e) {
			offerResponse.setUrlTrackingRecoverOffer(Constants.NO_VALUE_BD);
		    }
		    offerResponse.setUrlRecoverOffer(offerCheckpointReturnSaveBD.getUrlRecoverOffer());
		    offerResponse.setListData(listGenerateOffer);
		    recoveryOfferStepOne.setOffers(offerResponse);
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setData(recoveryOfferStepOne);
		}
		/******** Genero la dupla en la base de datos con los mismo datos... ********/
		/**/
		return responseDTO;
	    } else {
		System.out.println("NUEVA OFERTA");
		PostOfferRequestDTO offerRequestDTO = new PostOfferRequestDTO();
		offerRequestDTO.setCarKm(offerCheckpointBD.getCarKm());
		offerRequestDTO.setIdExteriorColor(offerCheckpointBD.getIdExteriorColor());
		offerRequestDTO.setIdUsuario(offerCheckpointBD.getIdUser());
		offerRequestDTO.setInternalUserId(offerCheckpointBD.getIdUser());
		offerRequestDTO.setSku(offerCheckpointBD.getSku());
		offerRequestDTO.setSource("apolo");
		offerRequestDTO.setZipCode(offerCheckpointBD.getZipCode());
		offerRequestDTO.setIdCheckpoint(offerCheckpointBDList.get(0).getId());
		/**** Llamo para generar la nuneva oferta ****/

		return (ResponseDTO) postOffers(offerRequestDTO);
	    }

	} else {
	    List<MessageDTO> listMessages = new ArrayList<>();
	    MessageDTO message = messageRepo.findByCode(MessageEnum.M0081.toString()).getDTO();
	    listMessages.add(message);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(new ArrayList<>());
	    return responseDTO;

	}

    }

    public int daysBetween(Date d1, Date d2) {
	return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }
    
    public String generateRecoverOfferURL(Long offerCheckpointId) {
	String hashRecuperarOferta = null;
	 hashRecuperarOferta = DigestUtils.sha1Hex("KAV-" + offerCheckpointId + "-AVK");
	 String primerHash = hashRecuperarOferta.substring(0, 21);
	 String segundoHash = hashRecuperarOferta.substring(21);
	 String hashDefinitvo = "K_" + primerHash + "_AVA_" + segundoHash + "_K";
	 String urlRecoverOffer = "recuperar-mi-oferta_" + hashDefinitvo;
	 String finalUrl = "https://www.kavak.com/" + urlRecoverOffer;
	
	return finalUrl;
    }
    
    
    
    public String generateTrackingRecoverOfferURL(OfferCheckpoint offerCheckpoint) {
	
	String trackingUrl = "";
	
	    if(offerCheckpoint.getComment() != null && (offerCheckpoint.getComment().trim().contains(Constants.PRICING_NEW_OFFER_AUDIT)  
		    || offerCheckpoint.getComment().trim().contains(Constants.PRICING_OFFER_AUDIT)  )) {
		trackingUrl = 
			"?utm_source=Email&utm_campaign=Email-MejoramosTuOferta-"
			+offerCheckpoint.getEmail()
			+"-"+offerCheckpoint.getUser().getId()
			+"&utm_medium=organico&utm_content=Email-MejoramosTuOferta-"
			+offerCheckpoint.getEmail()
			+"-"+offerCheckpoint.getUser().getId()
			;
	    }else if(offerCheckpoint.getIdStatus() == 19L && (offerCheckpoint.getOfferSource() == null ||  offerCheckpoint.getOfferSource() != Constants.SOURCE_PRICING)) {
		trackingUrl = 
			"?utm_source=Email&utm_campaign=Email-RecuperacionOfertaEtapa1-"
			+offerCheckpoint.getEmail()
			+"-"+offerCheckpoint.getUser().getId()
			+"&utm_medium=organico&utm_content=Email-RecuperacionOfertaEtapa1-"
			+offerCheckpoint.getEmail()
			+"-"+offerCheckpoint.getUser().getId()
			;
	    }else if(offerCheckpoint.getIdStatus() == 20L && (offerCheckpoint.getOfferSource() == null ||  offerCheckpoint.getOfferSource() != Constants.SOURCE_PRICING)) {
		trackingUrl = 
			"?utm_source=Email&utm_campaign=Email-RecuperacionOfertaEtapa2-"
			+offerCheckpoint.getEmail()
			+"-"+offerCheckpoint.getUser().getId()
			+"&utm_medium=organico&utm_content=Email-RecuperacionOfertaEtapa2-"
			+offerCheckpoint.getEmail()
			+"-"+offerCheckpoint.getUser().getId()
			;
	    }else if(offerCheckpoint.getIdStatus() == 22L && (offerCheckpoint.getOfferSource() == null ||  offerCheckpoint.getOfferSource() != Constants.SOURCE_PRICING)) {
		trackingUrl = 
			"?utm_source=Email&utm_campaign=Email-RecuperacionOfertaEtapa4-"
			+offerCheckpoint.getEmail()
			+"-"+offerCheckpoint.getUser().getId()
			+"&utm_medium=organico&utm_content=Email-RecuperacionOfertaEtapa4-"
			+offerCheckpoint.getEmail()
			+"-"+offerCheckpoint.getUser().getId()
			;
	    }else {
		trackingUrl = Constants.NO_VALUE_BD;
	    }
	
	return trackingUrl;
    }
    

}
