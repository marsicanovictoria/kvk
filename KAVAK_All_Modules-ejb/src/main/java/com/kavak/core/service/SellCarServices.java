package com.kavak.core.service;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.StringUtils;

import com.github.ooxi.phparser.SerializedPhpParser;
import com.github.ooxi.phparser.SerializedPhpParserException;
import com.kavak.core.dto.AnswerDTO;
import com.kavak.core.dto.CustomerNotificationQuestionsDTO;
import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.InspectionCarFeatureDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.PutCarContentOverviewDataDTO;
import com.kavak.core.dto.SellCarDetailDTO;
import com.kavak.core.dto.UserDTO;
import com.kavak.core.dto.request.PostCarContentOverviewRequesDTO;
import com.kavak.core.dto.request.PostCarNotificationAnswersRequestDTO;
import com.kavak.core.dto.request.PostCarNotificationRequestDTO;
import com.kavak.core.dto.request.PostDimpleRequestDTO;
import com.kavak.core.dto.request.PostPhotoRequestDTO;
import com.kavak.core.dto.response.CatalogueCarResponseDTO;
import com.kavak.core.dto.response.CustomerNotificationQuestionsResponseDTO;
import com.kavak.core.dto.response.GetFiltersResponse;
import com.kavak.core.dto.response.GetWarrantyResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.CatalogueCarStatusEnum;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.CarBodyType;
import com.kavak.core.model.CarColor;
import com.kavak.core.model.CarData;
import com.kavak.core.model.CarMake;
import com.kavak.core.model.CustomerNotificationQuestion;
import com.kavak.core.model.CustomerNotificationQuestionAnswer;
import com.kavak.core.model.CustomerNotificationQuestionOptions;
import com.kavak.core.model.CustomerNotifications;
import com.kavak.core.model.DimpleType;
import com.kavak.core.model.InspectionCarContentDetail;
import com.kavak.core.model.InspectionCarFeature;
import com.kavak.core.model.InspectionCarPhoto;
import com.kavak.core.model.InspectionDimple;
import com.kavak.core.model.InspectionLocation;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.PromotionCarPrice;
import com.kavak.core.model.SaleCarOrderTransaction;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.SellCarDimple;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.CarBodyTypeRepository;
import com.kavak.core.repositories.CarColorRepository;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.CarMakeRepository;
import com.kavak.core.repositories.CustomerNotificationQuestionAnswerRepository;
import com.kavak.core.repositories.CustomerNotificationQuestionNotificationsRepository;
import com.kavak.core.repositories.CustomerNotificationQuestionOptionRepository;
import com.kavak.core.repositories.CustomerNotificationsRepository;
import com.kavak.core.repositories.DimpleTypeRepository;
import com.kavak.core.repositories.InspectionCarContentDetailRepository;
import com.kavak.core.repositories.InspectionCarFeatureRepository;
import com.kavak.core.repositories.InspectionCarPhotoRepository;
import com.kavak.core.repositories.InspectionDimpleRepository;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.PromotionCarPriceRepository;
import com.kavak.core.repositories.SaleCarOrderTransactionRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.SellCarDimpleRepository;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

import mx.openpay.client.Charge;
import mx.openpay.client.core.OpenpayAPI;
import mx.openpay.client.core.requests.transactions.ConfirmCaptureParams;
import mx.openpay.client.exceptions.OpenpayServiceException;
import mx.openpay.client.exceptions.ServiceUnavailableException;

@Stateless
public class SellCarServices { 

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private SaleCarOrderTransactionRepository saleCarOrderTransactionRepo;

    @Inject
    private UserMetaRepository userMetaRepo;

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;

    @Inject
    private CarDataRepository carDataRepo;

    @Inject
    private InspectionCarPhotoRepository inspectionCarPhotoRepo;

    @Inject
    private InspectionDimpleRepository inspectionDimpleRepo;

    @Inject
    private DimpleTypeRepository dimpleTypeRepo;

    @Inject
    private InspectionCarContentDetailRepository inspectionCarContentDetailRepo;

    @Inject
    private InspectionCarFeatureRepository inspectionCarFeatureRepo;

    @Inject
    private SellCarDimpleRepository sellCarDimpleRepo;

    @Inject
    private CarBodyTypeRepository carBodyTypeRepo;

    @Inject
    private CarColorRepository carColorRepo;

    @Inject
    private CarMakeRepository carMakeRepo;

    @Inject
    private PromotionCarPriceRepository promotionCarPriceRepo;

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private CustomerNotificationsRepository customerNotificationsRepo;

    @Inject
    private UserRepository userRepo;
    
    @Inject
    private CustomerNotificationQuestionNotificationsRepository customerNotificationQuestionNotificationsRepo;
    
    @Inject
    private CustomerNotificationQuestionOptionRepository customerNotificationQuestionOptionRepo;
    
    @Inject
    private CustomerNotificationQuestionAnswerRepository customerNotificationQuestionAnswerRepo;

    @Inject
    private SaleCheckpointRepository saleCheckpointRepo;
    
    @Inject
    private InspectionLocationRepository inspectionLocationRepo;

    
    
   
    private List<MessageDTO> listMessages;

    public void confirmSaleCarBookings() {
		LogService.logger.info("Iniciando metodo confirmSaleCarBookings()...");
		MetaValue metaValue = metaValueRepo.findByAlias("TC");
		int daysForConfirmation = Integer.valueOf(metaValue.getOptionName());
		List<SaleCarOrderTransaction> saleCarBookings = saleCarOrderTransactionRepo.findPendingBookings(daysForConfirmation);
		if (saleCarBookings != null && !saleCarBookings.isEmpty()) {
		    for (SaleCarOrderTransaction booking : saleCarBookings) {
			try {
			    LogService.logger.info("Invocando servicio de OPENPAY para confirmar reserva [orderID=" + booking.getOrderId() + ", transactionID=" + booking.getTransactionId() + "]");
			    OpenpayAPI openpayAPI = new OpenpayAPI(Constants.OPENPAY_LOCATION, Constants.OPENPAY_APIKEY, Constants.OPENPAY_MERCHANTID);
			    ConfirmCaptureParams request = new ConfirmCaptureParams();
			    request.chargeId(booking.getTransactionId());
			    request.amount(new BigDecimal(booking.getTransactionAmount()));
			    UserMeta userMeta = userMetaRepo.findUserMetaByMetaKeyAndId("op_customer_id", booking.getBuyerId());
			    Charge charge = openpayAPI.charges().confirmCapture(userMeta.getMetaValue(), request);
			    if (charge.getStatus().equalsIgnoreCase("completed")) {
				booking.setConfirmationDate(new Timestamp(charge.getOperationDate().getTime()));
				booking.setStatus(1); // 1 = Confirmada
				saleCarOrderTransactionRepo.save(booking);
				LogService.logger.info("Reserva Confirmada [orderID=" + booking.getOrderId() + ", transactionID=" + booking.getTransactionId() + "]");
			    }
			} catch (ServiceUnavailableException suError) {
			    LogService.logger.error("Error al intentar confirmar la reserva [orderID=" + booking.getOrderId() + ", transactionID=" + booking.getTransactionId() + "]. Description: " + suError.getMessage());
			} catch (OpenpayServiceException opError) {
			    if (opError.getErrorCode() == 1003) {
				LogService.logger.error("Error al intentar confirmar la reserva [orderID=" + booking.getOrderId() + ", transactionID=" + booking.getTransactionId() + "]. ErrorCode: " + opError.getErrorCode() + ", Description: " + opError.getDescription());
				SaleCarOrderTransaction saleCarOrderTransaction = saleCarOrderTransactionRepo.findByOrderId(booking.getOrderId());
				saleCarOrderTransaction.setStatus(2); // 2 = Cancelada
				saleCarOrderTransactionRepo.save(saleCarOrderTransaction);
				LogService.logger.info("Se actualiza a 'Cancelada' el estatus de la reserva [orderID:" + booking.getOrderId() + ", transactionID: " + booking.getTransactionId() + "]");
			    } else {
				LogService.logger.error("Error al intentar confirmar la reserva [orderID=" + booking.getOrderId() + ", transactionID=" + booking.getTransactionId() + "]. ErrorCode: " + opError.getErrorCode() + ", Description: " + opError.getDescription());
			    }
			}
		    }
		} else {
		    LogService.logger.info("No se encontraron pagos de reservas de autos pendientes por confirmar.");
		}
    }

    /**
     * Consulta los carros para el catalogo de ventas
     * 
     * @author Enrique Marin
     * @return List<CatalogueCarResponseDTO>
     *
     */
    @SuppressWarnings({ "unchecked", "incomplete-switch" })
    public ResponseDTO getCatalogueCars(Long page, Long items) {
		ResponseDTO responseListDTO = new ResponseDTO();
		List<CatalogueCarResponseDTO> listCatalogueResponseCars = new ArrayList<>();
		// List<CatalogueCarResponseDTO> listCatalogueResponseOrdersCars = new
		// ArrayList<>();
		// List<CatalogueCarResponseDTO> listCatalogueResponseCarsNewArrival =
		// new ArrayList<>();
		// List<CatalogueCarResponseDTO> listCatalogueResponseCarsAvailable =
		// new ArrayList<>();
		// List<CatalogueCarResponseDTO> listCatalogueResponseCarsBooked = new
		// ArrayList<>();
		List<SellCarDetail> listSellCarDetailBDEndNewArrival = new ArrayList<>();
		List<SellCarDetail> listSellCarDetailBDEndAvailable = new ArrayList<>();
		List<SellCarDetail> listSellCarDetailBDEndBooked = new ArrayList<>();
	
		String filterStatus = "";
		String passengers = "";
		List<GenericDTO> listBenefits = new ArrayList<>();
		ResponseDTO responseDTO = new ResponseDTO();
		MessageDTO messageNoRecords = null;
		listMessages = new ArrayList<>();
		Long ini = 0L;
	
		// String query = "select * from 'v2_message'";
		// Long mmgvqueveteo = carDataRepo.queryLocura(query);
		if ((page == null && items != null) || (page != null && items == null)) {
		    // error los parametros, estan mal
		    messageNoRecords = messageRepo.findByCode(MessageEnum.M0054.toString()).getDTO();
		    listMessages.add(messageNoRecords);
		    EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		    responseDTO.setCode(enumResult2.getValue());
		    responseDTO.setStatus(enumResult2.getStatus());
		    responseDTO.setListMessage(listMessages);
		    return responseDTO;
		} else if (page != null && items != null) {
		    if (page == 0 || items == 0) {
			// error los parametros, estan mal
			messageNoRecords = messageRepo.findByCode(MessageEnum.M0054.toString()).getDTO();
			listMessages.add(messageNoRecords);
			EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult2.getValue());
			responseDTO.setStatus(enumResult2.getStatus());
			responseDTO.setListMessage(listMessages);
			return responseDTO;
		    }
		}
	
		List<Long> idsListSellCarDetailBD = new LinkedList<>();
		HashMap<Long, PromotionCarPrice> mapPromotionCarPrice = new HashMap<>();
		List<SellCarDetail> listSellCarDetailBD = sellCarDetailRepo.getCatalogueCarByCertifiedAndActiveAndValide();
		List<SellCarDetail> listSellCarDetailBDEnd = new ArrayList<>();
		// Depurando la lista con status != a vendido o NA
		for (SellCarDetail sellCarDetailActual : listSellCarDetailBD) {
		    CatalogueCarStatusEnum car = KavakUtils.getStatusCar(sellCarDetailActual);
		    if ((car != CatalogueCarStatusEnum.SOLD) && (car != CatalogueCarStatusEnum.STATUS_NA)) {
			switch (car) {
			case NEW_ARRIVAL:
			    listSellCarDetailBDEndNewArrival.add(sellCarDetailActual);
			    break;
			case AVAILABLE:
			    listSellCarDetailBDEndAvailable.add(sellCarDetailActual);
			    break;
			case BOOKED:
			    listSellCarDetailBDEndBooked.add(sellCarDetailActual);
			    break;
			}
		    }
		}
		listSellCarDetailBDEnd.addAll(listSellCarDetailBDEndNewArrival);
		listSellCarDetailBDEnd.addAll(listSellCarDetailBDEndAvailable);
		listSellCarDetailBDEnd.addAll(listSellCarDetailBDEndBooked);
	
		if (page != null && items != null && page > 0 && items > 0) {
		    ini = page * items;
		    ini = ini - items;
		    if (listSellCarDetailBDEnd.size() >= (ini + items)) {
			listSellCarDetailBDEnd = listSellCarDetailBDEnd.subList(ini.intValue(), (ini.intValue() + items.intValue()));
		    } else if (listSellCarDetailBDEnd.size() > ini) {
			listSellCarDetailBDEnd = listSellCarDetailBDEnd.subList(ini.intValue(), listSellCarDetailBDEnd.size());
		    } else {
			// Mensaje de error diciendo que se exedio de paginas
			messageNoRecords = messageRepo.findByCode(MessageEnum.M0053.toString()).getDTO();
			listMessages.add(messageNoRecords);
			EndPointCodeResponseEnum enumResult2 = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult2.getValue());
			responseDTO.setStatus(enumResult2.getStatus());
			responseDTO.setListMessage(listMessages);
			return responseDTO;
		    }
		}
	
		for (SellCarDetail sellCarDetailActual : listSellCarDetailBDEnd) {
		    idsListSellCarDetailBD.add(sellCarDetailActual.getId());
		}
	
		List<PromotionCarPrice> listPromotionCarPriceBD = promotionCarPriceRepo.findByIds(idsListSellCarDetailBD);
		for (PromotionCarPrice promotionCarPriceActual : listPromotionCarPriceBD) {
		    mapPromotionCarPrice.put(promotionCarPriceActual.getIdSellCarDetail(), promotionCarPriceActual);
		}
	
		for (SellCarDetail sellCarDetailActual : listSellCarDetailBDEnd) {
		    CatalogueCarResponseDTO catalogueCarResponseDTO = new CatalogueCarResponseDTO();
		    catalogueCarResponseDTO.setId(sellCarDetailActual.getId());
		    String[] trim = sellCarDetailActual.getCarTrim().split("##");
		    catalogueCarResponseDTO.setCarName((KavakUtils.getFriendlyCarName(sellCarDetailActual.getDTO()) + " " + sellCarDetailActual.getCarPackage()).trim());
		    catalogueCarResponseDTO.setCarKm(sellCarDetailActual.getCarKm());
		    catalogueCarResponseDTO.setPrice(sellCarDetailActual.getPrice());
		    catalogueCarResponseDTO.setCarYear(sellCarDetailActual.getCarYear());
		    catalogueCarResponseDTO.setCarMake(sellCarDetailActual.getCarMake());
		    catalogueCarResponseDTO.setCarModel(sellCarDetailActual.getCarModel());
		    catalogueCarResponseDTO.setCarTrim((trim[0] + " " + sellCarDetailActual.getCarPackage()).trim());
	
		    // Para el deserializado en JAVA se usa una un parser que transforma
		    // el string a hashmap
		    String serialized = sellCarDetailActual.getSellCarMeta().getMetaValue();
		    try {
			// En el primer level del hashmap tiene como value otro hashmap
			// que nos referimos a el como hasmap de segundo level
			Map<Object, Map<Object, Object>> mapFirstLvlDeserialized = (Map<Object, Map<Object, Object>>) new SerializedPhpParser(serialized).parse();
			// Se genera un deserealizado aparte ya que en esta variable
			// esta almacenada directamente como un String
			Map<Object, Object> mapFepmDeserialized = (Map<Object, Object>) new SerializedPhpParser(serialized).parse();
	
			// Debido a que en PHP la variable esta almacenada de esta
			// manera $main['basic_overview']['transmission'] los valores
			// del primer lvl se obtienen sacando el hashmap
			// 'basic_overview'
			Map<Object, Object> mapBasicOverview = mapFirstLvlDeserialized.get(Constants.BASIC_OVERVIEW);
			Map<Object, Object> mapPricing = mapFirstLvlDeserialized.get(Constants.PRICING);
	
			if (mapFepmDeserialized.get(Constants.FEPM) != null && !mapFepmDeserialized.get(Constants.FEPM).toString().isEmpty()) {
			    catalogueCarResponseDTO.setFepm(Double.valueOf(mapFepmDeserialized.get(Constants.FEPM).toString()));
			} else {
			    catalogueCarResponseDTO.setFepm(0D);
			}
	
			// Una vez obtenido el hasmap interno o de segundo level
			// obtenemos los campos precisos que se necesitan
			catalogueCarResponseDTO.setTransmission(mapBasicOverview.get(Constants.TRANSMISION).toString());
			catalogueCarResponseDTO.setMarketPrice(Double.valueOf(mapPricing.get(Constants.MARKET_PRICE).toString()));
			catalogueCarResponseDTO.setBodyType(mapBasicOverview.get(Constants.BODY_TYPE).toString());
			catalogueCarResponseDTO.setCarColor(mapBasicOverview.get(Constants.EXT_COLOR).toString());
			if(mapBasicOverview.get(Constants.INT_COLOR) != null) {
			    catalogueCarResponseDTO.setInteriorColor(mapBasicOverview.get(Constants.INT_COLOR).toString()); 
			}else {
			    catalogueCarResponseDTO.setInteriorColor("NA");
			}

			if (mapBasicOverview.get(Constants.FUEL_COMBINED) != null) {
			    if ((mapBasicOverview.get(Constants.FUEL_COMBINED).toString().trim()).isEmpty()) {
				catalogueCarResponseDTO.setAvgFuelConsumption(mapBasicOverview.get(Constants.FUEL_COMBINED).toString());
			    } else {
				catalogueCarResponseDTO.setAvgFuelConsumption(mapBasicOverview.get(Constants.FUEL_COMBINED).toString() + " Km/Lt");
			    }
			} else {
			    catalogueCarResponseDTO.setAvgFuelConsumption(Constants.NO_VALUE_BD);
			}
	
			catalogueCarResponseDTO.setUber(sellCarDetailActual.getUberType());
			Long WarrantyMarket = 0L;
			Long paperworkMarket = 0L;
			Long inspectionMarket = 0L;
			Long detailingMarket = 0L;
			Long tenureMarket = 0L;
	
			if (mapPricing.get(Constants.WARRANTYMARKET) == null || mapPricing.get(Constants.WARRANTYMARKET).toString().isEmpty()) {
			    WarrantyMarket = 0L;
			} else {
			    WarrantyMarket = Long.valueOf(mapPricing.get(Constants.WARRANTYMARKET).toString());
			}
			if (mapPricing.get(Constants.PAPERWORKMKT) == null || mapPricing.get(Constants.PAPERWORKMKT).toString().isEmpty()) {
			    paperworkMarket = 0L;
			} else {
			    paperworkMarket = Long.valueOf(mapPricing.get(Constants.PAPERWORKMKT).toString());
			}
			if (mapPricing.get(Constants.INSPECTIONPRICEMKT) == null || mapPricing.get(Constants.INSPECTIONPRICEMKT).toString().isEmpty()) {
			    inspectionMarket = 0L;
			} else {
			    inspectionMarket = Long.valueOf(mapPricing.get(Constants.INSPECTIONPRICEMKT).toString());
			}
			if (mapPricing.get(Constants.DETAILINGMKT) == null || mapPricing.get(Constants.DETAILINGMKT).toString().isEmpty()) {
			    detailingMarket = 0L;
			} else {
			    detailingMarket = Long.valueOf(mapPricing.get(Constants.DETAILINGMKT).toString());
			}
			if (mapPricing.get(Constants.TENUREMARKET) == null || mapPricing.get(Constants.TENUREMARKET).toString().isEmpty()) {
			    tenureMarket = 0L;
			} else {
			    tenureMarket = Long.valueOf(mapPricing.get(Constants.TENUREMARKET).toString());
			}
	
			PromotionCarPrice promotionCarPrice = promotionCarPriceRepo.findBy_Id(sellCarDetailActual.getId());
			if (promotionCarPrice == null) {
			    catalogueCarResponseDTO.setSavings((catalogueCarResponseDTO.getMarketPrice() - catalogueCarResponseDTO.getPrice()) + WarrantyMarket + paperworkMarket + inspectionMarket + detailingMarket + tenureMarket);
			} else {
			    catalogueCarResponseDTO.setSavings((catalogueCarResponseDTO.getMarketPrice() - catalogueCarResponseDTO.getPrice()) + (catalogueCarResponseDTO.getPrice() - promotionCarPrice.getPrice()) + WarrantyMarket + paperworkMarket + inspectionMarket + detailingMarket + tenureMarket);
			}
				String[] splitImages = sellCarDetailActual.getImageCar().split("#");
				List<String> listImageUrl = new ArrayList<>();

				CharSequence http = "http";
				CharSequence https = "https";

				if (sellCarDetailActual.getImageCar().contains(http) || sellCarDetailActual.getImageCar().contains(https)) {
						listImageUrl.add(splitImages[0]);
					catalogueCarResponseDTO.setImageUrl(splitImages[0]);
				} else {
					catalogueCarResponseDTO.setImageUrl(Constants.URL_BASE_IMAGES_CATALOGUE_CARS + splitImages[0]);
				}

				int imageCounter = 0;
				while (imageCounter < 5 && imageCounter < splitImages.length) {
					if (splitImages[imageCounter].contains(http) || splitImages[imageCounter].contains(https)) {
							listImageUrl.add(splitImages[imageCounter]);
					} else {
						listImageUrl.add(Constants.URL_BASE_IMAGES_CATALOGUE_CARS + splitImages[imageCounter]);
					}
					imageCounter++;
				}
	
			catalogueCarResponseDTO.setListImageUrl(listImageUrl);
			CarData carDataBD = carDataRepo.findBySku(sellCarDetailActual.getSku());
	
			if (carDataBD != null) { // Validación por si el auto tiene
						 // asignado un SKU Inválido, Oscar
						 // Montilla
			    if (mapBasicOverview.get(Constants.TRACTION) == null || mapBasicOverview.get(Constants.TRACTION).toString().isEmpty()) {
				catalogueCarResponseDTO.setTraction(Constants.NO_VALUE_BD);
			    } else {
				catalogueCarResponseDTO.setTraction(mapBasicOverview.get(Constants.TRACTION).toString());
			    }
			    catalogueCarResponseDTO.setDoors(carDataBD.getDoors());
			    catalogueCarResponseDTO.setSeats(carDataBD.getSeats());
			    catalogueCarResponseDTO.setCylinder(carDataBD.getCylinder());
			    if (mapBasicOverview.get(Constants.ENGINE) == null) {
				catalogueCarResponseDTO.setEngine("NA");
			    } else {
				catalogueCarResponseDTO.setEngine(mapBasicOverview.get(Constants.ENGINE).toString().toUpperCase());
			    }
			    catalogueCarResponseDTO.setFuel(mapBasicOverview.get(Constants.FUEL_TYPE).toString());
			    catalogueCarResponseDTO.setStatus(KavakUtils.getStatusCar(sellCarDetailActual));
	
			    if (sellCarDetailActual.getDeliveryDays() != null) {
				Calendar date = Calendar.getInstance();
				date.add(Calendar.DAY_OF_YEAR, sellCarDetailActual.getDeliveryDays().intValue());
				SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy");
				Date dateD = date.getTime();
				String dateS = sfd.format(dateD);
				catalogueCarResponseDTO.setDateDelivery(dateS);
			    } else {
				catalogueCarResponseDTO.setDateDelivery("NA");
			    }
	
			    if (!mapBasicOverview.get(Constants.HORSE_POWER).toString().isEmpty()) {
				catalogueCarResponseDTO.setHorsepower(mapBasicOverview.get(Constants.HORSE_POWER).toString());
			    } else {
				catalogueCarResponseDTO.setHorsepower("NA");
			    }
			    if (mapBasicOverview.get(Constants.PASSENGERS) != null && !mapBasicOverview.get(Constants.PASSENGERS).toString().isEmpty()) {
				passengers = mapBasicOverview.get(Constants.PASSENGERS).toString();
			    } else {
				passengers = "NA";
			    }
			    // downpayment
			    Long downpaymentL = Double.valueOf(sellCarDetailActual.getPrice() * 0.25).longValue();
			    // Filter Status
			    if (KavakUtils.getStatusCar(sellCarDetailActual).toString().equals("AVAILABLE") || KavakUtils.getStatusCar(sellCarDetailActual).toString().equals("NEW_ARRIVAL")) {
				filterStatus = "DISPONIBLE";
			    } else if (KavakUtils.getStatusCar(sellCarDetailActual).toString().equals("BOOKED")) {
				filterStatus = "APARTADO";
			    }
			    catalogueCarResponseDTO.setPassengers(passengers);
			    catalogueCarResponseDTO.setDownpayment(downpaymentL);
			    catalogueCarResponseDTO.setFilterStatus(filterStatus);
	
			    // Benefits
			    GenericDTO benefits = new GenericDTO();
			    Long marketPrice = 0L;
			    Long price = 0L;
			    Locale locale = new Locale("es", "MX");
			    NumberFormat numberFormat = NumberFormat.getInstance(locale);
	
			    marketPrice = Double.valueOf(mapPricing.get(Constants.MARKET_PRICE).toString()).longValue();
			    price = sellCarDetailActual.getPrice().longValue();
	
			    benefits.setName("AHORRO VS PRECIO DEL MERCADO");
			    benefits.setValue("$" + numberFormat.format(marketPrice - price));
			    listBenefits.add(benefits);
	
			    if (promotionCarPrice != null) {
				Long promotionPrice = promotionCarPrice.getPrice();
				benefits = new GenericDTO();
				benefits.setName("DESCUENTO PROMOCIÓN");
				benefits.setValue("$" + numberFormat.format(price - promotionPrice));
				listBenefits.add(benefits);
			    }
	
			    if (inspectionMarket != null) {
				benefits = new GenericDTO();
				benefits.setName("INSPECCIÓN 240 PTOS");
				benefits.setValue("$" + numberFormat.format(inspectionMarket));
				listBenefits.add(benefits);
			    }
	
			    if (WarrantyMarket != null) {
				benefits = new GenericDTO();
				benefits.setName("3 MESES GARANTÍA");
				benefits.setValue("$" + numberFormat.format(WarrantyMarket));
				listBenefits.add(benefits);
			    }
	
			    if (tenureMarket != null) {
				benefits = new GenericDTO();
				benefits.setName("TENENCIAS");
				benefits.setValue("$" + numberFormat.format(tenureMarket));
				listBenefits.add(benefits);
			    }
	
			    if (paperworkMarket != null) {
				benefits = new GenericDTO();
				benefits.setName("TRÁMITES");
				benefits.setValue("$" + numberFormat.format(paperworkMarket));
				listBenefits.add(benefits);
			    }
	
			    if (detailingMarket != null) {
				benefits = new GenericDTO();
				benefits.setName("DETALLADO Y ENTREGA");
				benefits.setValue("$" + numberFormat.format(detailingMarket));
				listBenefits.add(benefits);
			    }
	
			    benefits = new GenericDTO();
			    benefits.setName("7 DÍAS DE PRUEBA");
			    benefits.setValue("NO TIENE PRECIO");
			    listBenefits.add(benefits);
	
			    catalogueCarResponseDTO.setBenefits(listBenefits);
			    listBenefits = new ArrayList<>();
	
			}
		    } catch (SerializedPhpParserException e) {
			e.printStackTrace();
		    }
		    // Se valida que el carro tenga promocion
		    if (catalogueCarResponseDTO.getTraction() != null) { // Validación
									 // por si el
									 // SKU del auto
									 // es inválido,
									 // Oscar
									 // Montilla
			// if ((catalogueCarResponseDTO.getStatus() !=
			// CatalogueCarStatusEnum.SOLD) &&
			// (catalogueCarResponseDTO.getStatus() !=
			// CatalogueCarStatusEnum.STATUS_NA)) {
			if (mapPromotionCarPrice.containsKey(sellCarDetailActual.getId())) {
			    catalogueCarResponseDTO.setPromotionPrice(mapPromotionCarPrice.get(sellCarDetailActual.getId()).getPrice());
			    catalogueCarResponseDTO.setPromotionName(mapPromotionCarPrice.get(sellCarDetailActual.getId()).getPromotion().getName());
			    catalogueCarResponseDTO.setPromotionColor(mapPromotionCarPrice.get(sellCarDetailActual.getId()).getPromotion().getColor());
			}
			
			if(sellCarDetailActual.getIdWarehouseLocation() != null) {
			    InspectionLocation inspectionLocation = inspectionLocationRepo.findAllActiveAndAliasNotNullAndIdWarehouseLocation(sellCarDetailActual.getIdWarehouseLocation());

			    if(inspectionLocation != null) {
				catalogueCarResponseDTO.setLocationFilter(inspectionLocation.getAlias());
			    }
			}  
			listCatalogueResponseCars.add(catalogueCarResponseDTO);
	
			// switch (catalogueCarResponseDTO.getStatus()) {
			// case NEW_ARRIVAL:
			// listCatalogueResponseCarsNewArrival.add(catalogueCarResponseDTO);
			// break;
			// case AVAILABLE:
			// listCatalogueResponseCarsAvailable.add(catalogueCarResponseDTO);
			// break;
			// case BOOKED:
			// listCatalogueResponseCarsBooked.add(catalogueCarResponseDTO);
			// break;
			// }
			// }
		    }

		}
		// listCatalogueResponseOrdersCars.addAll(listCatalogueResponseCarsNewArrival);
		// listCatalogueResponseOrdersCars.addAll(listCatalogueResponseCarsAvailable);
		// listCatalogueResponseOrdersCars.addAll(listCatalogueResponseCarsBooked);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseListDTO.setCode(enumResult.getValue());
		responseListDTO.setStatus(enumResult.getStatus());
	
		responseListDTO.setData(listCatalogueResponseCars);
	
		return responseListDTO;
    }

    /**
     * Consulta los filtros disponible para la busqueda de los carros
     * 
     * @author Enrique Marin
     *
     */
    @SuppressWarnings("unchecked")
    public ResponseDTO getFilters() {
	
	    ResponseDTO responseDTO = new ResponseDTO();
		GetFiltersResponse getFiltersResponse = new GetFiltersResponse();
	
		List<Long> listPrices = new ArrayList<>();
		List<Long> listKms = new ArrayList<>();
		List<String> listYears = new ArrayList<>();
		List<String> listBodys = new ArrayList<>();
		List<String> listMakes = new ArrayList<>();
		List<String> listColorsName = new ArrayList<>();
		List<GenericDTO> listBodysGenericDTO = new ArrayList<>();
		List<GenericDTO> listMakesGenericDTO = new ArrayList<>();
		List<GenericDTO> listCarColorGenericDTO = new ArrayList<>();
		List<Long> listIdsSellCarDetail = new ArrayList<>();
		List<String> listDoors = new ArrayList<>();
		List<String> listSeats = new ArrayList<>();
		List<String> listTransmissions = new ArrayList<>();
		List<String> listCilinder = new ArrayList<>();
		List<String> listTraction = new ArrayList<>();
		List<String> listFuels = new ArrayList<>();
		List<String> listUber = new ArrayList<>();
		List<Long> listHorsePower = new ArrayList<>();
		List<Double> listFepm = new ArrayList<>();
		String maxDownpayment = null;
		String maxMonthlyPayment = null;
		List<GenericDTO> horsepower = new ArrayList<>();
		List<String> filterStatus = new ArrayList<>();
		List<String> passengers = new ArrayList<>();
		List<String> listInspectionLocation = new ArrayList<>();
		List<String> listInspectionLocationResponse = new ArrayList<>();
	
		List<SellCarDetail> listSellCarDetailBD = sellCarDetailRepo.getCatalogueCarByCertifiedAndActiveAndValide();
		for (SellCarDetail sellCarDetailActual : listSellCarDetailBD) {
		    listIdsSellCarDetail.add(sellCarDetailActual.getId());
		    listPrices.add(sellCarDetailActual.getPrice());
		    listKms.add(Long.valueOf(sellCarDetailActual.getCarKm()));
		    if (!listYears.contains(sellCarDetailActual.getCarYear())) {
			listYears.add(sellCarDetailActual.getCarYear());
		    }
		    // Para el deserializado en JAVA se usa una un parser que transforma
		    // el string a hashmap
		    String serialized = sellCarDetailActual.getSellCarMeta().getMetaValue();
	
		    try {
			// En el primer level del hashmap tiene como value otro hashmap
			// que nos referimos a el como hasmap de segundo level
			Map<Object, Map<Object, Object>> mapFirstLvlDeserialized = (Map<Object, Map<Object, Object>>) new SerializedPhpParser(serialized).parse();
			// Debido a que en PHP la variable esta almacenada de esta
			// manera $main['basic_overview']['transmission'] los valores
			// del primer lvl se obtienen sacando el hashmap
			// 'basic_overview'
			Map<Object, Object> mapBasicOverview = mapFirstLvlDeserialized.get(Constants.BASIC_OVERVIEW);
	
			String bodyNameNoAccent = KavakUtils.getStrinNoAccent(mapBasicOverview.get(Constants.BODY_TYPE).toString().toUpperCase());
			if (bodyNameNoAccent != null && !listBodys.contains(bodyNameNoAccent) && bodyNameNoAccent.length() > 0) {
			    GenericDTO bodyTypeGenericDTO = new GenericDTO();
			    listBodys.add(mapBasicOverview.get(Constants.BODY_TYPE).toString().toUpperCase());
			    CarBodyType carBodyTypeBD = carBodyTypeRepo.findByName(mapBasicOverview.get(Constants.BODY_TYPE).toString().toUpperCase());
			    if (carBodyTypeBD != null) {
				bodyTypeGenericDTO.setName(carBodyTypeBD.getName().toUpperCase());
				listBodys.add(bodyNameNoAccent);
				bodyTypeGenericDTO.setImageMake(Constants.URL_KAVAK + carBodyTypeBD.getImage());
				listBodysGenericDTO.add(bodyTypeGenericDTO);
			    }
			}
	
			String makeNameNoAccent = KavakUtils.getStrinNoAccent(sellCarDetailActual.getCarMake().toUpperCase());
			if (!listMakes.contains(makeNameNoAccent) && makeNameNoAccent.length() > 0) {
			    CarMake carmakeBD = carMakeRepo.findByName(makeNameNoAccent);
	
			    if (carmakeBD != null) {
				GenericDTO carMakeGenericDTO = new GenericDTO();
				carMakeGenericDTO.setName(carmakeBD.getName());
				if (carmakeBD.getImageMakeUrl() != null && carmakeBD.getImageMakeUrl().length() > 0) {
				    carMakeGenericDTO.setImageMake(Constants.URL_KAVAK + carmakeBD.getImageMakeUrl());
				} else {
				    carMakeGenericDTO.setImageMake(Constants.NO_VALUE_BD);
				}
				listMakesGenericDTO.add(carMakeGenericDTO);
				listMakes.add(makeNameNoAccent);
			    }
			}
	
			String carColorName = KavakUtils.getStrinNoAccent(mapBasicOverview.get(Constants.EXT_COLOR).toString().toUpperCase());
			if (!listColorsName.contains(carColorName)) {
			    GenericDTO carColorGenericDTO = new GenericDTO();
			    CarColor carColorBD = carColorRepo.findByName(carColorName);
			    if (carColorBD != null) {
				carColorGenericDTO.setName(carColorBD.getName());
				carColorGenericDTO.setHexadecimal(carColorBD.getHexadecimal());
			    } else {
				if (!listColorsName.contains("OTRO")) {
				    carColorGenericDTO.setName("OTRO");
				    carColorGenericDTO.setHexadecimal("");
				    listColorsName.add("OTRO");
	
				} else {
				    continue;
				}
			    }
			    listColorsName.add(carColorName);
			    listCarColorGenericDTO.add(carColorGenericDTO);
			}
			if (!listTransmissions.contains(KavakUtils.getStrinNoAccent(mapBasicOverview.get(Constants.TRANSMISION).toString().trim().toUpperCase()))) {
			    listTransmissions.add(KavakUtils.getStrinNoAccent(mapBasicOverview.get(Constants.TRANSMISION).toString().toUpperCase().trim()));
			}
			String fuel = StringUtils.stripAccents(mapBasicOverview.get(Constants.FUEL_TYPE).toString().trim().toUpperCase());
			if (!listFuels.contains(fuel)) {	    
			    if (!fuel.isEmpty()) {
				listFuels.add(fuel);
			    }
			}
			if (sellCarDetailActual.getUberType() != null && !sellCarDetailActual.getUberType().trim().isEmpty()) {
			    if (!listUber.contains(sellCarDetailActual.getUberType().toUpperCase())) {
				listUber.add(sellCarDetailActual.getUberType().toUpperCase());
			    }
			}
	
			if (mapBasicOverview.get(Constants.HORSE_POWER) != null && !mapBasicOverview.get(Constants.HORSE_POWER).toString().isEmpty()) {
			    listHorsePower.add(Long.valueOf(mapBasicOverview.get(Constants.HORSE_POWER).toString()));
			}
			// Map<Object, Object> mapFepm=
			// mapFirstLvlDeserialized.get(Constants.FEPM);
			Object mapFepm = mapFirstLvlDeserialized.get(Constants.FEPM);
			if (mapFepm != null && !mapFepm.toString().toString().isEmpty()) {
			    listFepm.add(Double.valueOf(mapFepm.toString()));
			}
			if (mapBasicOverview.get(Constants.PASSENGERS) != null && !mapBasicOverview.get(Constants.PASSENGERS).toString().isEmpty()) {
			    if (!passengers.contains(mapBasicOverview.get(Constants.PASSENGERS).toString())) {
				passengers.add(mapBasicOverview.get(Constants.PASSENGERS).toString());
			    }
			}
	
		    } catch (SerializedPhpParserException e) {
			LogService.logger.info("Error al consultar SellCarMeta StockID = " + sellCarDetailActual.getId());
			e.printStackTrace();
		    } catch (Exception e) {
			LogService.logger.info("Error al consultar información auto StockID = " + sellCarDetailActual.getId());
			e.printStackTrace();
		    }
		}
	
		List<CarData> listCarData = carDataRepo.findAllById(listIdsSellCarDetail);
		for (CarData carDataActual : listCarData) {
		    if (!listDoors.contains(carDataActual.getDoors())) {
			listDoors.add(carDataActual.getDoors());
		    }
		    if (!carDataActual.getSeats().toUpperCase().equals("NA") && !listSeats.contains(carDataActual.getSeats().toUpperCase())) {
			listSeats.add(carDataActual.getSeats().toUpperCase());
		    }
		    if (!carDataActual.getCylinder().toUpperCase().equals("NA") && !listCilinder.contains(carDataActual.getCylinder().toUpperCase())) {
			listCilinder.add(carDataActual.getCylinder().toUpperCase());
		    }
		    if (!carDataActual.getTraction().toUpperCase().equals("NA") && !listTraction.contains(carDataActual.getTraction().toUpperCase())) {
			listTraction.add(carDataActual.getTraction().toUpperCase());
		    }
		}
	
		listInspectionLocation = inspectionLocationRepo.findAllActiveAndAliasNotNull();
		
		if(!listInspectionLocation.isEmpty()) {
		    for(String InspectionLocationActual : listInspectionLocation) {
			listInspectionLocationResponse.add(InspectionLocationActual);
		    }
		}
		
		Collections.sort(listPrices);
		Collections.sort(listYears);
		Collections.sort(listKms);
		Collections.sort(listDoors);
		Collections.sort(listSeats);
		Collections.sort(listCilinder);
		Collections.sort(listTransmissions);
		Collections.sort(listTraction);
		Collections.sort(listFuels);
		Collections.sort(listUber);
		Collections.sort(listHorsePower);
		Collections.sort(listFepm);
		Collections.sort(listInspectionLocationResponse);
		
	
		GenericDTO genericPricesDTO = new GenericDTO();
		GenericDTO genericKmsDTO = new GenericDTO();
		genericPricesDTO.setMin(listPrices.get(0));
		genericPricesDTO.setMax(listPrices.get(listPrices.size() - 1));
		genericKmsDTO.setMin(listKms.get(0));
		genericKmsDTO.setMax(listKms.get(listKms.size() - 1));
	
		// FILTROS
		// Filter maxDownpayment
		Long maxDownpaymentL = Double.valueOf(listPrices.get(listPrices.size() - 1) * 0.25).longValue();
		maxDownpayment = maxDownpaymentL.toString();
		GenericDTO genericFilterHorsePowerDTO = new GenericDTO();
		// Filter Status
		filterStatus.add("DISPONIBLE");
		filterStatus.add("APARTADO");
		// Filter HorsePower
		genericFilterHorsePowerDTO.setMin(Double.valueOf(listHorsePower.get(1)));
		genericFilterHorsePowerDTO.setMax(Double.valueOf(listHorsePower.get(listHorsePower.size() - 1)));
		horsepower.add(genericFilterHorsePowerDTO);
		// Filter maxMonthlyPayment
		Long maxMonthlyPaymentL = listFepm.get(listFepm.size() - 1).longValue();
		maxMonthlyPayment = maxMonthlyPaymentL.toString();
		// setter Filter
		getFiltersResponse.setFilterStatus(filterStatus);
		getFiltersResponse.setHorsepower(horsepower);
		getFiltersResponse.setMaxDownpayment(maxDownpayment);
		getFiltersResponse.setMaxMonthlyPayment(maxMonthlyPayment);
		if (passengers.isEmpty() || passengers == null) {
		    passengers.add("NA");
		    getFiltersResponse.setPassengers(passengers);
		} else {
		    Collections.sort(passengers);
		    getFiltersResponse.setPassengers(passengers);
		}
	
		// Se agregan los valores para responder
		getFiltersResponse.setPrices(genericPricesDTO);
		getFiltersResponse.setKms(genericKmsDTO);
		getFiltersResponse.setListYears(listYears);
		getFiltersResponse.setListBodysGenericDTO(listBodysGenericDTO);
		getFiltersResponse.setListMakesGenericDTO(listMakesGenericDTO);
		getFiltersResponse.setListColorsGenericDTO(listCarColorGenericDTO);
		getFiltersResponse.setListDoors(listDoors);
		getFiltersResponse.setListSeats(listSeats);
		getFiltersResponse.setListCilinder(listCilinder);
		getFiltersResponse.setListTransmissions(listTransmissions);
		getFiltersResponse.setListTraction(listTraction);
		getFiltersResponse.setListFuels(listFuels);
		getFiltersResponse.setListUber(listUber);
		getFiltersResponse.setLocations(listInspectionLocationResponse);
	
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(getFiltersResponse);
		return responseDTO;
    }

    /**
     * Agrega una imagen a una inspecccion
     * 
     * @author Enrique Marin
     * @param {@link
     * 	   {@link com.kavak.core.dto.request.PostPhotoRequestDTO}
     * @return N/A
     *
     */
    public ResponseDTO postPhoto(PostPhotoRequestDTO postPhotoRequestDTO) {
	
	    ResponseDTO responseDTO = new ResponseDTO();
		InspectionCarPhoto newInspectionCarPhoto = new InspectionCarPhoto();
	
		SellCarDetail sellCarDetalBD = sellCarDetailRepo.findByIdInspection(postPhotoRequestDTO.getIdInspection());
	
		if (sellCarDetalBD != null) {
		    if (sellCarDetalBD.getImageCar() != null && !sellCarDetalBD.getImageCar().isEmpty()) {
			sellCarDetalBD.setImageCar(sellCarDetalBD.getImageCar() + "#" + postPhotoRequestDTO.getImageName());
		    } else {
			sellCarDetalBD.setImageCar(postPhotoRequestDTO.getImageName());
		    }
		}
	
		newInspectionCarPhoto.setIdInspection(postPhotoRequestDTO.getIdInspection());
		newInspectionCarPhoto.setInspectorEmail(postPhotoRequestDTO.getInspectorEmail());
		String nameImage = Calendar.getInstance().getTimeInMillis() + "_" + postPhotoRequestDTO.getImageName() + ".jpg";
	        KavakUtils.generateFile(postPhotoRequestDTO.getImage(), nameImage, null, Constants.FILE_TYPE_JPG,true);
		newInspectionCarPhoto.setImageUrl(Constants.NAME_BASE_IMAGE_BD + nameImage);
		inspectionCarPhotoRepo.save(newInspectionCarPhoto);
	
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(new GenericDTO());
		return responseDTO;
    }

    /**
     * Registra un dimple asociado a una inspeccion
     * 
     * @author Enrique Marin
     * @param postPhotoRequestDTO
     * @return N/A
     *
     */
    public ResponseDTO postDimple(PostDimpleRequestDTO postPhotoRequestDTO) {
		ResponseDTO responseDTO = new ResponseDTO();
	
		InspectionDimple newInspectionDimple = new InspectionDimple();
		String dimpleName;
		newInspectionDimple.setIdInspection(postPhotoRequestDTO.getIdInspection());
	
		newInspectionDimple.setInspectorEmail(postPhotoRequestDTO.getInspectorEmail());
		newInspectionDimple.setIdDimpleType(postPhotoRequestDTO.getIdDimpletype());
		newInspectionDimple.setCoordinateX(postPhotoRequestDTO.getCoordinateX());
		newInspectionDimple.setCoordinateY(postPhotoRequestDTO.getCoordinateY());
		newInspectionDimple.setOriginalHeight(postPhotoRequestDTO.getDimpleHeight());
		newInspectionDimple.setOriginalWidth(postPhotoRequestDTO.getDimpleWidth());
		newInspectionDimple.setDimpleLocation(postPhotoRequestDTO.getDimpleLocation());
	
		if (postPhotoRequestDTO.getIdDimpletype() != 0) {
		    Optional<DimpleType> dimpleTypeBD = dimpleTypeRepo.findById(postPhotoRequestDTO.getIdDimpletype());
		    dimpleName = dimpleTypeBD.get().getName().replace(" ", "_");
	
		} else {
		    dimpleName = postPhotoRequestDTO.getDimpleName().replace(" ", "_");
		}
		newInspectionDimple.setDimpleName(dimpleName);
	
		String nameDimpleImage = Calendar.getInstance().getTimeInMillis() + "_" + dimpleName;
	        KavakUtils.generateFile(postPhotoRequestDTO.getDimpleImage(), nameDimpleImage, null, Constants.FILE_TYPE_JPG,false);
		newInspectionDimple.setDimpleImage(Constants.NAME_BASE_IMAGE_BD + nameDimpleImage);
		inspectionDimpleRepo.save(newInspectionDimple);
	
		SellCarDetail sellCarDetailBD = sellCarDetailRepo.findByIdInspection(postPhotoRequestDTO.getIdInspection());
		if (sellCarDetailBD != null) {
		    List<SellCarDimple> listSellCarDimpleBD = sellCarDimpleRepo.findByIdSellCarDetail(sellCarDetailBD.getId());
		    if (listSellCarDimpleBD != null && !listSellCarDimpleBD.isEmpty()) {
			sellCarDimpleRepo.deleteAll(listSellCarDimpleBD);
		    }
		    List<InspectionDimple> listInspectionDimpleBD = inspectionDimpleRepo.findByIdInspection(postPhotoRequestDTO.getIdInspection());
		    for (InspectionDimple inspectionDimpleActual : listInspectionDimpleBD) {
			SellCarDimple newSellCarDimple = new SellCarDimple();
			newSellCarDimple.setSellCarDetail(sellCarDetailBD);
			newSellCarDimple.setCoordanateX(inspectionDimpleActual.getCoordinateX());
			newSellCarDimple.setCoordanateY(inspectionDimpleActual.getCoordinateY());
			newSellCarDimple.setDimpleName(inspectionDimpleActual.getDimpleName());
			newSellCarDimple.setDimpleImageUrl(inspectionDimpleActual.getDimpleImage());
			if (inspectionDimpleActual.getDimpleLocation() == 1) {
			    newSellCarDimple.setDimpleType(Constants.EXT);
			} else {
			    newSellCarDimple.setDimpleType(Constants.INNER);
			}
			sellCarDimpleRepo.save(newSellCarDimple);
		    }
		}
	
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(new GenericDTO());
	
		return responseDTO;
    }

    /**
     * Eso registra la info base de contenido del carro, para Apolo
     * 
     * @author Enrique Marin
     * @param postCarContentOverviewRequesDTO
     * @return N/A
     *
     */
    public ResponseDTO postCarContentOverview(PostCarContentOverviewRequesDTO postCarContentOverviewRequesDTO) {
		ResponseDTO responseDTO = new ResponseDTO();
	
		for (PutCarContentOverviewDataDTO putCarContentOverviewDataDTOActual : postCarContentOverviewRequesDTO.getListPutCarContentOverviewDataDTO()) {
		    SellCarDetail sellCarDetailBD = sellCarDetailRepo.findByIdInspection(putCarContentOverviewDataDTOActual.getIdInspection());
		    if (sellCarDetailBD != null) {
			sellCarDetailBD.setWhyCarText(putCarContentOverviewDataDTOActual.getWhyCarText());
			sellCarDetailBD.setWhyCarAudio(putCarContentOverviewDataDTOActual.getWhyCarAudio());
			sellCarDetailRepo.save(sellCarDetailBD);
		    }
	
		    InspectionCarContentDetail newInspectionCarContentDetail = new InspectionCarContentDetail();
		    newInspectionCarContentDetail.setIdInspection(putCarContentOverviewDataDTOActual.getIdInspection());
		    newInspectionCarContentDetail.setInspectorEmail(putCarContentOverviewDataDTOActual.getInspectorEmail());
		    newInspectionCarContentDetail.setWhyCarText(putCarContentOverviewDataDTOActual.getWhyCarText());
		    newInspectionCarContentDetail.setWhyCarAudio(putCarContentOverviewDataDTOActual.getWhyCarAudio());
		    inspectionCarContentDetailRepo.save(newInspectionCarContentDetail);
		    for (InspectionCarFeatureDTO inspectionCarFeatureDTOActual : putCarContentOverviewDataDTOActual.getListInspectionCarFeatureDTO()) {
			InspectionCarFeature newInspectionCarFeature = new InspectionCarFeature();
			newInspectionCarFeature.setInspectorEmail(putCarContentOverviewDataDTOActual.getInspectorEmail());
			newInspectionCarFeature.setIdInspection(putCarContentOverviewDataDTOActual.getIdInspection());
			newInspectionCarFeature.setIdFeature(inspectionCarFeatureDTOActual.getIdFeature());
			newInspectionCarFeature.setFeatureLabel(inspectionCarFeatureDTOActual.getFeatureLabel());
			inspectionCarFeatureRepo.save(newInspectionCarFeature);
		    }
	
		}
	
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(new GenericDTO());
		return responseDTO;
    }

    /**
     * Lista el catalogo de garantia de todos los autos
     * 
     * @author Enrique Marin
     * @param type
     * @return
     *
     */
    public ResponseDTO getWarranty(String type) {

		ResponseDTO responseDTO = new ResponseDTO();
		List<String> listWarrantysNames = new ArrayList<>();
		GetWarrantyResponseDTO getWarrantyResponseDTO = new GetWarrantyResponseDTO();
		List<MetaValue> listMetaValueBD = metaValueRepo.findByGroupCategoryAndGroupNameAndActiveTrue(Constants.GROUP_CATEGORY_MAPFRE, Constants.GROUP_NAME_DETAIL_WARRANTY);
	
		// El endpoint debe devolver una lista de string, de los registros en
		// meta_values donde group_category = ‘MAPFRE’, group_name = ‘DETALLE
		// GARANTIA’ y activo =1.
	
		for (MetaValue metaValueActual : listMetaValueBD) {
		    listWarrantysNames.add(metaValueActual.getOptionName());
		}
	
		getWarrantyResponseDTO.setListWarrantysNames(listWarrantysNames);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(getWarrantyResponseDTO);
		return responseDTO;

    }

    /**
     * Metodo que registra la notificacíon de auto Reservado.
     * 
     * @author Oscar Montilla
     * @param postCarNotificationRequestDTO
     *            request del servicio postCarNotification
     * @return responseDTO Response Generico de servicio
     * @throws MessagingException 
     */

    public ResponseDTO postCarNotification(PostCarNotificationRequestDTO postCarNotificationRequestDTO) throws MessagingException {

		ResponseDTO responseDTO = new ResponseDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();
	
		if (postCarNotificationRequestDTO.getUserId() == null || postCarNotificationRequestDTO.getCarId() == null || postCarNotificationRequestDTO.getNotificationType() == null || postCarNotificationRequestDTO.getNotificationType().length() == 0) {
	
		    LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
				MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString())
						.getRequestDTO(" " + (postCarNotificationRequestDTO.getUserId()== null?"user_id, ":"") + (postCarNotificationRequestDTO.getCarId()== null?"car_id, ":"")
								+ (postCarNotificationRequestDTO.getNotificationType()== null?"notification_type, ":""));
		    listMessageDTO.add(messageDataNull);
		    responseDTO.setListMessage(listMessageDTO);
		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setData(new ArrayList<>());
	
		    return responseDTO;
		}
	
		Optional<User> userBD = userRepo.findById(postCarNotificationRequestDTO.getUserId());

		if (!userBD.isPresent()) {
		    LogService.logger.info("Ingresar un id valido, El usuario no existe en la base de datos..");
				MessageDTO messageNoUser = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
		    listMessageDTO.add(messageNoUser);
		    responseDTO.setListMessage(listMessageDTO);
	
		}
	
		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(postCarNotificationRequestDTO.getCarId());
	
		if (!sellCarDetailBD.isPresent()) {
		    LogService.logger.info("Ingresar un car_id valido, El CarDetail no existe en la base de datos..");
		    MessageDTO messageNoCarId = messageRepo.findByCode(MessageEnum.M0043.toString()).getDTO();
		    listMessageDTO.add(messageNoCarId);
		    responseDTO.setListMessage(listMessageDTO);
	
		}
	
		MetaValue metaValueBD = metaValueRepo.findByAlias(postCarNotificationRequestDTO.getNotificationType());
	
		if (metaValueBD == null) {
		    LogService.logger.info("Ingresar un notification_type valido, El MetaValue no existe en la base de datos..");
		    MessageDTO messageNoType = messageRepo.findByCode(MessageEnum.M0045.toString()).getDTO();
		    listMessageDTO.add(messageNoType);
		    responseDTO.setListMessage(listMessageDTO);
	
		}

		if (responseDTO.getListMessage() != null) {
		    LogService.logger.info("Existe una lista de Menssaje de validacion, devolver los mensajes..");
		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setData(new ArrayList<>());

		} else {
	
		    List<CustomerNotifications> customerNotificationsBD = customerNotificationsRepo.findByUserIdAndCarId(userBD.get().getId(), sellCarDetailBD.get().getId());
	
		    if (customerNotificationsBD == null || customerNotificationsBD.isEmpty()) {
			LogService.logger.info("No existe una notificacion, se crea la notificacion..");
			CustomerNotifications customerNotificationsNew = new CustomerNotifications();
	
			customerNotificationsNew.setUser(userBD.get());
			customerNotificationsNew.setCarId(sellCarDetailBD.get().getId());
			customerNotificationsNew.setType(metaValueBD.getId());
			if (userBD.get().getName().split("(?=\\s)").length != 0) {
			    customerNotificationsNew.setUserName(userBD.get().getName().split("(?=\\s)")[0]);
			}
			
			customerNotificationsNew.setCreationDate(new Timestamp(System.currentTimeMillis()));
			customerNotificationsNew.setUpdateDate(new Timestamp(System.currentTimeMillis()));
			customerNotificationsNew.setSendNotification(0);
			
			if(StringUtils.isNotEmpty(postCarNotificationRequestDTO.getSource())) {
			    customerNotificationsNew.setSource(postCarNotificationRequestDTO.getSource());
			} else {
			    customerNotificationsNew.setSource("ios_app");
			}
			
			if(postCarNotificationRequestDTO.getInternalUserId() != null) {
			    customerNotificationsNew.setInternalUserId(Long.valueOf(postCarNotificationRequestDTO.getInternalUserId()));
			}

			
			//Disable communications
			if(postCarNotificationRequestDTO.isDisableCommunications() != false) {
			    customerNotificationsNew.setEmailSent(true);
			    customerNotificationsNew.setCancelledCarBookedSmsSent(true);
			    customerNotificationsNew.setCarNotificationSmsSent(true);
			    customerNotificationsNew.setCarAppNotificationSent(true);
			    customerNotificationsNew.setCarNotificationActiveAppNotificationSent(true);  	    
			}
			//
			    
			customerNotificationsRepo.save(customerNotificationsNew);

		    }
		    
		    LogService.logger.info("Existe una notificacion con estas caracteristicas..");
		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());

		}
		
		
		if (postCarNotificationRequestDTO.getNotificationType().trim().equals("AUTO_RESERVADO")) {
		    sendBookedCarNotificationActivation(userBD.get().getDTO(), sellCarDetailBD.get().getDTO(), postCarNotificationRequestDTO.getSource());
		}
		

		// SE PROCEDE A GENERAR REGISTRO EN COMPRA_CHECKPPOINTS A PARTIR DE LA NOTIFICACION
		try {
			LogService.logger.info("Se procede a generar oportunidad a partir de una notificacion inbound.");
			generateInboundNotification(userBD.get().getId(), sellCarDetailBD.get().getId());
		} catch (Exception e) {
			LogService.logger.error("Error intentando generar una oportunidad a partir de una notificacion inbound" + e);
			e.printStackTrace();
		}
		
		return responseDTO;
    }

    /**
     * Metodo para consultar la Notificacion de Auto Reservado.
     * 
     * @author Oscar Montilla
     * @param userId
     *            Indentificador de usuario
     * @param carId
     *            Identificador de carro
     * @param notificationType
     *            identificador de tipo de notificacion
     * @return responseDTO Response Generico de servicio
     */

    public ResponseDTO getCarNotification(Long userId, Long carId, String notificationType) {

		ResponseDTO responseDTO = new ResponseDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();
	
		if (userId == null || carId == null || notificationType == null || notificationType.length() == 0) {
	
		    LogService.logger.info("Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
				MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString())
						.getRequestDTO(" " + (userId == null?"user_Id, ":"") + (carId == null?"car_Id, ":"") + (notificationType == null?"notification_Type, ":"") );
		    listMessageDTO.add(messageDataNull);
		    responseDTO.setListMessage(listMessageDTO);
		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setData(new ArrayList<>());
	
		    return responseDTO;
		}

		Optional<User> userBD = userRepo.findById(userId);
	
		if (!userBD.isPresent()) {
		    LogService.logger.info("Ingresar un id valido, El usuario no existe en la base de datos..");
				MessageDTO messageNoUser = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
		    listMessageDTO.add(messageNoUser);
		    responseDTO.setListMessage(listMessageDTO);
	
		}
	
		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(carId);
	
		if (!sellCarDetailBD.isPresent()) {
		    LogService.logger.info("Ingresar un car_id valido, El CarDetail no existe en la base de datos..");
		    MessageDTO messageNoCarId = messageRepo.findByCode(MessageEnum.M0043.toString()).getDTO();
		    listMessageDTO.add(messageNoCarId);
		    responseDTO.setListMessage(listMessageDTO);
	
		}

		MetaValue metaValueBD = metaValueRepo.findByAlias(notificationType);
	
		if (metaValueBD == null) {
		    LogService.logger.info("Ingresar un notification_type valido, El MetaValue no existe en la base de datos..");
		    MessageDTO messageNoType = messageRepo.findByCode(MessageEnum.M0045.toString()).getDTO();
		    listMessageDTO.add(messageNoType);
		    responseDTO.setListMessage(listMessageDTO);
	
		}
	
		if (responseDTO.getListMessage() != null) {
		    LogService.logger.info("Existe una lista de Menssaje de validacion, devolver los mensajes..");
		    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		    responseDTO.setCode(enumResult.getValue());
		    responseDTO.setStatus(enumResult.getStatus());
		    responseDTO.setData(new ArrayList<>());
	
		    return responseDTO;
		}
	
		Integer sendNotification = 0;
	
		CustomerNotifications customerNotificationsBD = customerNotificationsRepo.findByUserAndCarIdAndTypeAndSendNotification(userBD.get(), sellCarDetailBD.get().getId(), metaValueBD.getId(), sendNotification);
	
		GenericDTO genericDTO = new GenericDTO();
		if (customerNotificationsBD != null) {
		    LogService.logger.info("Existe una notificacion para el response..");
		    genericDTO.setNotificationId(customerNotificationsBD.getId());
		}

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(genericDTO);
	
		return responseDTO;
    }

    /**
     * Email: Correo Activación de Notificación de Auto Reservado
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO sendBookedCarNotificationActivation(UserDTO userDTO, SellCarDetailDTO sellCarDetailDTO, String source) throws MessagingException {
		List<String> emailCc = new ArrayList<>();
		String templateEmail = "product_booked_car_notification_activation.html";
		ResponseDTO responseDTO = new ResponseDTO();
		String email = userDTO.getEmail();
	
		if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
		    emailCc.add("it@kavak.com");
		} else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
		    emailCc.add("team@kavak.com");
		    emailCc.add("ayuda@kavak.com");
		}
	
		HashMap<String, Object> subjectData = new HashMap<>();
		subjectData.put(Constants.KEY_SUBJECT, "@PREFIX  ACTIVACIÓN DE NOTIFICACIÓN DE AUTO RESERVADO - Stock ID #" + sellCarDetailDTO.getId() + " " + sellCarDetailDTO.getCarYear() + " " + KavakUtils.getFriendlyCarName(sellCarDetailDTO));
		subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		HashMap<String, Object> dataEmail = new HashMap<>();
	
		if (userDTO.getName() != null) {
		    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, userDTO.getName());
		} else {
		    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
		}

		if (userDTO.getEmail() != null) {
		    dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, userDTO.getEmail());
		} else {
		    dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		}
	
		dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));
	
		if (sellCarDetailDTO.getCarMake() != null) {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, sellCarDetailDTO.getCarMake());
		} else {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		}
	
		if (KavakUtils.getFriendlyCarName(sellCarDetailDTO) != null) {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, KavakUtils.getFriendlyCarName(sellCarDetailDTO));
		} else {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, "No Registrado");
		}
	
		if (sellCarDetailDTO.getCarModel() != null) {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, sellCarDetailDTO.getCarModel());
		} else {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		}

		if (sellCarDetailDTO.getCarYear() != null) {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, sellCarDetailDTO.getCarYear());
		} else {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		}
	
		if (sellCarDetailDTO.getId() != null) {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_ID, sellCarDetailDTO.getId());
		} else {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_ID, "No Registrado");
		}
	
		if (userDTO.getPhone() != null) {
		    dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userDTO.getPhone());
		} else {
		    dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		}
	
		if (source != null && !source.trim().equals("")) {
		    dataEmail.put(Constants.KEY_EMAIL_SOURCE, source);
		} else {
		    dataEmail.put(Constants.KEY_EMAIL_SOURCE, "iOS");
		}
		
		dataEmail.put(Constants.ATTACHMENT_FILES, null);
	
		try {
		    InternetAddress fromEmail = new InternetAddress("hola@kavak.com", "Hola Kavak");
		    boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, null, dataEmail, fromEmail, null);
		    if (!emailSend) {
			LogService.logger.info("No se envio correo de sendBookedCarNotificationActivation al usuario " + userDTO.getId());
		    }
		} catch (UnsupportedEncodingException e) {
		    e.printStackTrace();
		}

		return responseDTO;
    }
    
    
    /**
     * Servicio: SERVICIO PARA OBTENER PREGUNTAS POR TIPO DE NOTIFICACION
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO getCarNotificationQuestionsByNotificationType(String notificationType) {
	    	
		ResponseDTO responseDTO = new ResponseDTO();
		List<CustomerNotificationQuestionsDTO> listCustomerNotificationQuestionsDTO = new ArrayList<>();
		CustomerNotificationQuestionsResponseDTO customerNotificationQuestionsResponseDTO = new CustomerNotificationQuestionsResponseDTO();
		CustomerNotificationQuestionsDTO customerNotificationQuestionsDTO = new CustomerNotificationQuestionsDTO();
		//CustomerNotificationOptionsDTO customerNotificationOptionsDTO = new CustomerNotificationOptionsDTO();
		List<CustomerNotificationQuestionOptions> listCustomerNotificationQuestionOptions = new ArrayList<>();
		List<GenericDTO> listOptionsDTO = new ArrayList<>();
		try {
		    MetaValue metaValueBD =metaValueRepo.findByAlias(notificationType);
		    if(metaValueBD != null){
			List<CustomerNotificationQuestion> listCustomerNotificationQuestionBD = customerNotificationQuestionNotificationsRepo.findByNotificationType(metaValueBD.getId());
			for(CustomerNotificationQuestion customerNotificationQuestionActual : listCustomerNotificationQuestionBD) {
			    customerNotificationQuestionsDTO = new CustomerNotificationQuestionsDTO();
			    customerNotificationQuestionsDTO.setId(customerNotificationQuestionActual.getId());
			    customerNotificationQuestionsDTO.setTitle(customerNotificationQuestionActual.getQuestion());
			    listCustomerNotificationQuestionOptions = customerNotificationQuestionOptionRepo.findByQuestionId(customerNotificationQuestionActual.getId());
			    listOptionsDTO = new ArrayList<>();
			    for(CustomerNotificationQuestionOptions customerNotificationQuestionOptionsActual : listCustomerNotificationQuestionOptions) {
				//llenando el objeto options
				GenericDTO optionDTO = new GenericDTO();
				optionDTO.setId(customerNotificationQuestionOptionsActual.getId());
				optionDTO.setName(customerNotificationQuestionOptionsActual.getOption());
				listOptionsDTO.add(optionDTO);
			    }
			    customerNotificationQuestionsDTO.setOptions(listOptionsDTO);
			    listCustomerNotificationQuestionsDTO.add(customerNotificationQuestionsDTO);
			}
			customerNotificationQuestionsResponseDTO.setListCustomerNotificationQuestionDTO(listCustomerNotificationQuestionsDTO);
		    }else {
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(customerNotificationQuestionsResponseDTO);
			return responseDTO;
		    }    
		} catch (Exception e) {
		    // TODO: handle exception
		    LogService.logger.error("Error Catch de getCarNotificationQuestionsByNotificationType : "+ e);
		}
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(customerNotificationQuestionsResponseDTO);
		return responseDTO;
    }
    
    
    /**
     * Servicio: SERVICIO PARA RESPUESTAS DE PREGUNTAS POR TIPO DE NOTIFICACION
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO postCarNotificationAnswers(PostCarNotificationAnswersRequestDTO postCarNotificationAnswersRequestDTO) {
		ResponseDTO responseDTO = new ResponseDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();
	        Timestamp actualTime = new Timestamp(System.currentTimeMillis());
	
		try {
		    if(postCarNotificationAnswersRequestDTO.getUserId() == null || postCarNotificationAnswersRequestDTO.getCarId() == null) {
			LogService.logger.info("Error los parametros del Json estan null o incompletos");
					MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0021.toString())
							.getRequestDTO(" " + (postCarNotificationAnswersRequestDTO.getUserId()== null?"user_id, ":"") + (postCarNotificationAnswersRequestDTO.getCarId()== null?"car_id, ":""));
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);
	
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
				.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());
	
			return responseDTO;
		    }
		    
		    Optional<User> userBD = userRepo.findById(postCarNotificationAnswersRequestDTO.getUserId());
		    if(!userBD.isPresent()) {
			LogService.logger.info("Esta recibiendo el usuario no valido...");
					MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0025.toString()).getDTO();
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);
	
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
				.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());
	
			return responseDTO;
		    }
	
		    Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(postCarNotificationAnswersRequestDTO.getCarId());
		    if(!sellCarDetailBD.isPresent()) {
			LogService.logger.info("Esta recibiendo el car no valido...");
			MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0009.toString()).getDTO();
			listMessageDTO.add(messageDataNull);
			responseDTO.setListMessage(listMessageDTO);
	
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum
				.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(new GenericDTO());
	
			return responseDTO;
		    }
	
		    if(!postCarNotificationAnswersRequestDTO.getAnswers().isEmpty()){
			for(AnswerDTO answersActual : postCarNotificationAnswersRequestDTO.getAnswers()) {
			    CustomerNotificationQuestionAnswer customerNotificationQuestionAnswer = customerNotificationQuestionAnswerRepo.findQuestionAndAnswer(postCarNotificationAnswersRequestDTO.getUserId(),postCarNotificationAnswersRequestDTO.getCarId(),answersActual.getQuestionId());
			    if(customerNotificationQuestionAnswer != null) {
				customerNotificationQuestionAnswer.setQuestionId(answersActual.getQuestionId());
				customerNotificationQuestionAnswer.setOptionId(answersActual.getOptionId());
				customerNotificationQuestionAnswer.setUpdateDate(actualTime);
				customerNotificationQuestionAnswerRepo.save(customerNotificationQuestionAnswer);
			    }else {
				CustomerNotificationQuestionAnswer newCustomerNotificationQuestionAnswer = new CustomerNotificationQuestionAnswer();
				newCustomerNotificationQuestionAnswer.setUserId(postCarNotificationAnswersRequestDTO.getUserId());
				newCustomerNotificationQuestionAnswer.setCarId(postCarNotificationAnswersRequestDTO.getCarId());
				newCustomerNotificationQuestionAnswer.setQuestionId(answersActual.getQuestionId());
				newCustomerNotificationQuestionAnswer.setOptionId(answersActual.getOptionId());
				newCustomerNotificationQuestionAnswer.setUpdateDate(actualTime);
				customerNotificationQuestionAnswerRepo.save(newCustomerNotificationQuestionAnswer);
			    }
	
			}
		    }  
		} catch (Exception e) {
		    LogService.logger.error("Error Catch de postCarNotificationAnswers : "+ e);
		}
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		//responseDTO.setData(customerNotificationQuestionsResponseDTO);
		return responseDTO;
    }
 
    
    /**
     * Generate a Sale Opportunity by Inbound Notification
     * 
     * @author Antony Delgado
     * @param Long userId
     * @param Long stockId
     * @return ResponseDTO responseDTO
     */
    
    public ResponseDTO generateInboundNotification(Long userId, Long stockId){
    	ResponseDTO responseDTO = new ResponseDTO();
    	
    	try {
			if(userId != null && stockId != null){
				
				Optional<User> userBD = userRepo.findById(userId);
				Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(stockId);
				
				if (userBD.isPresent() && sellCarDetailBD.isPresent()){
					
					CarData carDataBD = carDataRepo.findBySku(sellCarDetailBD.get().getSku());
					
					SaleCheckpoint saleCheckpointBD = new SaleCheckpoint();
					
					int wordsName = userBD.get().getName().isEmpty() ? 0 : userBD.get().getName().split("\\s+").length;
					String[] fragment = userBD.get().getName().split(" ");
					
					saleCheckpointBD.setCustomerName(WordUtils.capitalize(fragment[0]));
					
					if (wordsName > 1){
						saleCheckpointBD.setCustomerLastName(WordUtils.capitalize(fragment[1]));
					}
					
					saleCheckpointBD.setUserId(userBD.get().getId());
					
					saleCheckpointBD.setEmail(userBD.get().getEmail().toLowerCase());
					saleCheckpointBD.setCarId(sellCarDetailBD.get().getId());
					saleCheckpointBD.setSku(sellCarDetailBD.get().getSku());
					saleCheckpointBD.setStatus(277L);
					saleCheckpointBD.setCarYear(Long.valueOf(sellCarDetailBD.get().getCarYear()));
					saleCheckpointBD.setCarMake(sellCarDetailBD.get().getCarMake());
					saleCheckpointBD.setCarModel(sellCarDetailBD.get().getCarModel());
					saleCheckpointBD.setCarVersion(carDataBD.getCarTrim());
					saleCheckpointBD.setCarKm(Long.valueOf(sellCarDetailBD.get().getCarKm()));
					saleCheckpointBD.setZipCode(sellCarDetailBD.get().getZipCode());
					saleCheckpointBD.setCarCost(sellCarDetailBD.get().getPrice().toString());
					saleCheckpointBD.setRegisterDate(new Timestamp(System.currentTimeMillis()));
					saleCheckpointBD.setSaleOpportunityTypeId(277L);
					saleCheckpointBD.setUser(userBD.get());
					saleCheckpointBD.setSentNetsuite(0L);
					
					SaleCheckpoint saleCheckpoint = saleCheckpointRepo.save(saleCheckpointBD);
					
					if (saleCheckpoint != null){
						saleCheckpoint.setDuplicatedOfferControl(saleCheckpoint.getId());
						saleCheckpointRepo.save(saleCheckpoint);
					}
					
					EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
					responseDTO.setCode(enumResult.getValue());
					responseDTO.setStatus(enumResult.getStatus());
					
				}else if (!userBD.isPresent()){
					LogService.logger.warn("El user ID " + userId + " no existe en BD, se descarta generacion de notificacino inbound");
				}else{
					LogService.logger.warn("El Stock ID " + stockId + " no existe en BD, se descarta generacion de notificacino inbound");
				}
				
			}else if (userId == null){
				LogService.logger.error("El user ID esta vacio, se descarta generacion de notificacino inbound");
			}else{
				LogService.logger.error("El stock ID esta vacio, se descarta generacion de notificacino inbound");
			}
		} catch (Exception e) {
			LogService.logger.error("Error generando notificacion inbound : "+ e);
			e.printStackTrace();
			
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
		}
    	
    	return responseDTO;
    }
    
}