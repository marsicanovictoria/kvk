package com.kavak.core.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.kavak.core.dto.CancelledReservationLeadsDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.CarData;
import com.kavak.core.model.CustomerNotifications;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.CustomerNotificationsRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.LogService;

@Stateless
public class GenerateExcelFileServices {

	@Inject
	SellCarDetailRepository sellCarDetailRepo;
	
	@Inject
	CustomerNotificationsRepository customerNotificationRepo;
	
	@Inject
	CarDataRepository carDataRepo;
	
	/*
	 * Generate a excel file about Interests for Available Cars 
	 * 
	 */

    @SuppressWarnings("deprecation")
	public ResponseDTO generateAvailableCarsNotificationExcel(Long stockId) {
        ResponseDTO responseDTO = new ResponseDTO();
        
        String urlExcel = "/home/kavak/KAVAK_Resources/excel/temp/";
        
        //Local URL for testing
        LogService.logger.info("Entorno para generar excel: " + Constants.PROPERTY_ENVIRONMENT);
        
		if(Constants.PROPERTY_ENVIRONMENT.matches(Constants.ENVIRONMENT_DEVELOPER)){
			urlExcel = "C:/Users/Antony Casanova/Desktop/";
		}
		
		String documentName = "_notifications.xls";
        String completeExcelUrl = "";
        
        try {
        	
        	if (stockId != null){
        		
        		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(stockId);
        		
        		if (sellCarDetailBD.isPresent()){
        			
        			List<CustomerNotifications> customerNotificationBD = customerNotificationRepo.findByCarId(stockId);
        			
        			if (customerNotificationBD.size() > 0){
        				
        				ArrayList<CancelledReservationLeadsDTO> cancelledReservationList = new ArrayList<CancelledReservationLeadsDTO>();
        				
        				for(CustomerNotifications customerNotification : customerNotificationBD ){
        					
        					CancelledReservationLeadsDTO cancelledReservationLeadsDTO = new CancelledReservationLeadsDTO();
        					
        					cancelledReservationLeadsDTO.setId(customerNotification.getId());
        					cancelledReservationLeadsDTO.setCreationDate(customerNotification.getCreationDate());
        					cancelledReservationLeadsDTO.setSource(customerNotification.getSource());
        					
        					if (customerNotification.getUser() != null){
        						cancelledReservationLeadsDTO.setCustomer(customerNotification.getUser().getDTO());
        					}
        					
        					if (customerNotification.getCarId() != null){
        						Optional<CarData> carDataBD = carDataRepo.findById(customerNotification.getCarId());
        						
        						if(carDataBD.isPresent()){
        							cancelledReservationLeadsDTO.setCar(carDataBD.get().getDTO());
        						}
        					}
        					
        					cancelledReservationList.add(cancelledReservationLeadsDTO);
        				}
        				
        				if (cancelledReservationList.size() > 0 ){
        					
        					try {
        						
        						int totalRows = cancelledReservationList.size();
        						
        						Long temporalFolder = System.currentTimeMillis();
        						new File(urlExcel + temporalFolder.toString()).mkdir();
        						completeExcelUrl = urlExcel + temporalFolder.toString() + File.separator + stockId + documentName;
        						
            		            FileOutputStream fileOut = new FileOutputStream(completeExcelUrl);

            		            @SuppressWarnings("resource")
								HSSFWorkbook workbook = new HSSFWorkbook();
            		            HSSFSheet worksheet = workbook.createSheet("Stock ID " + stockId + " Notifications");
            		            HSSFRow row = worksheet.createRow((short) 0);

            		            //Define Cells and Titles
            		            
            		            HSSFCell cellA1 = row.createCell(0);
            		            cellA1.setCellValue("Lead");
            		            HSSFCellStyle styleOfCell = workbook.createCellStyle();
            		            styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
            		            cellA1.setCellStyle(styleOfCell);
            		            
            		            HSSFCell cellB1 = row.createCell(1);
            		            cellB1.setCellValue("Nombre");
            		            styleOfCell = workbook.createCellStyle();
            		            styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
            		            cellB1.setCellStyle(styleOfCell);

            		            HSSFCell cellC1 = row.createCell(2);
            		            cellC1.setCellValue("Correo");
            		            styleOfCell = workbook.createCellStyle();
            		            styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
            		            cellC1.setCellStyle(styleOfCell);

            		            HSSFCell cellD1 = row.createCell(3);
            		            cellD1.setCellValue("Teléfono");
            		            styleOfCell = workbook.createCellStyle();
            		            styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
            		            cellD1.setCellStyle(styleOfCell);
            		            
            		            HSSFCell cellE1 = row.createCell(4);
            		            cellE1.setCellValue("Fecha de Registro");
            		            styleOfCell = workbook.createCellStyle();
            		            styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
            		            cellE1.setCellStyle(styleOfCell);
            		            
            		            HSSFCell cellF1 = row.createCell(5);
            		            cellF1.setCellValue("Auto");
            		            styleOfCell = workbook.createCellStyle();
            		            styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
            		            cellF1.setCellStyle(styleOfCell);
            		            
            		            for (int r= 1; r < totalRows; r++){
                		            
            		            	String carYear    = cancelledReservationList.get(r).getCar().getCarYear().toString();
            		            	String carMake    = cancelledReservationList.get(r).getCar().getCarMake();
            		            	String carModel   = cancelledReservationList.get(r).getCar().getCarModel();
            		            	String carVersion = cancelledReservationList.get(r).getCar().getFullVersionCar();
            		            	
            		            	row = worksheet.createRow(r);
                		            row.createCell(0).setCellValue(cancelledReservationList.get(r).getCustomer().getId());
                		            row.createCell(1).setCellValue(cancelledReservationList.get(r).getCustomer().getName());
                		            row.createCell(2).setCellValue(cancelledReservationList.get(r).getCustomer().getEmail());
                		            row.createCell(3).setCellValue(cancelledReservationList.get(r).getCustomer().getPhone());
                		            
            		            	if (cancelledReservationList.get(r).getCreationDate() != null){
            		  
            		            		Date date = new Date(cancelledReservationList.get(r).getCreationDate().getTime());
            		            		
            		            		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-YYYY");
            		            		String registerDate = formatter.format(date);
            		            		row.createCell(4).setCellValue(registerDate);
            		            	}
                		            
            		            	row.createCell(5).setCellValue(carYear + " " + carMake + " " + carModel + " " + carVersion);
            		            }
            		            
            		            try {
            		                workbook.write(fileOut);
            		                fileOut.flush();
            		                fileOut.close();
            		                
            		            } catch (IOException e) {
            		                e.printStackTrace();
            		                
            		    	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
            		    	        responseDTO.setCode(enumResult.getValue());
            		    	        responseDTO.setStatus(enumResult.getStatus());
            		    	        responseDTO.setData(completeExcelUrl);
            		            }

            		        } catch (FileNotFoundException e) {
            		            e.printStackTrace();
            		        }

        				}else{
        					LogService.logger.info("No existen clientes interesados en el auto con ID " + stockId);
        				}
        				
        			}else{
        				LogService.logger.info("No existen clientes interesados en el auto con ID " + stockId);
        			}
        			
        		}else{
        			LogService.logger.info("El Stock ID " + stockId + " no existe en BD, se descarta generacion de excel.");
        		}
        	}else{
        		LogService.logger.info("La variable stockId vino vacio, se descarta generacion de excel.");
        	}
      
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(completeExcelUrl);
	        

	    }catch (Exception e){
	    	LogService.logger.error("Ocurrio un error consultando Customer Notifications para el Auto ID " + stockId + ", ", e);
	    	e.printStackTrace();
	    	
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(null);
	    }
	    
        return responseDTO;
    }
}
