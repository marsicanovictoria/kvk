package com.kavak.core.service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.auronix.calixta.GatewayException;
import com.auronix.calixta.sms.SMSGateway;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.DistanceMatrixRow;
import com.kavak.core.dto.ColonyDataDTO;
import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.PostMessageRequestDTO;
import com.kavak.core.dto.TestimonialsListItemsDTO;
import com.kavak.core.dto.request.PostContactRequestDTO;
import com.kavak.core.dto.response.ApiUrlShortenerResponseDTO;
import com.kavak.core.dto.response.ColonysByZipApiResponseDTO;
import com.kavak.core.dto.response.GetColonysResponseDTO;
import com.kavak.core.dto.response.GetInspectionCentersResponseDTO;
import com.kavak.core.dto.response.GetTestimonialsListResponse;
import com.kavak.core.dto.response.GetTestimonialsResponseDTO;
import com.kavak.core.dto.response.PressnotesResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.ColonyNotServed;
import com.kavak.core.model.ColonyZipcode;
import com.kavak.core.model.CommunicationsTray;
import com.kavak.core.model.ContactUs;
import com.kavak.core.model.InspectionLocation;
import com.kavak.core.model.InspectionSchedule;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.PageMeta;
import com.kavak.core.model.PressNotes;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.Testimonials;
import com.kavak.core.model.ZipCode;
import com.kavak.core.repositories.ColonyNotServedRepository;
import com.kavak.core.repositories.ColonyZipcodeRepository;
import com.kavak.core.repositories.CommunicationsTrayRepository;
import com.kavak.core.repositories.ContactUsRepository;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.InspectionScheduleRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.PageMetaRepository;
import com.kavak.core.repositories.PressNotesRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.TestimonialsRepository;
import com.kavak.core.repositories.ZipCodeRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.LogService;

@Stateless
public class CommonServices {

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private InspectionLocationRepository inspectionLocationRepo;

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;

    @Inject
    private ZipCodeRepository zipCodeRepo;

    @Inject
    private ColonyNotServedRepository colonyNotServedRepo;

    @Inject
    private InspectionScheduleRepository inspectionScheduleRepo;

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private ColonyZipcodeRepository colonyZipcodeRepo;
    @Inject
    @PersistenceContext(unitName = "KAVAK_BusinessCore_EJBTier")
    private EntityManager em;

    @Resource
    private SessionContext sessionContext;

    @Inject
    private CommunicationsTrayRepository communicationsTrayRepo;
    
    @Inject
    private TestimonialsRepository TestimonialsRepo;
    
    @Inject
    private PressNotesRepository PressNotesRepo;
    
    @Inject
    private ContactUsRepository contactUsRepo;
    
    @Inject
    private PageMetaRepository pageMetaRepo;
    
    

    /**
     * Consulta las colonias (Son como una especie de municipio) en Mexico
     * 
     * @author Enrique Marin
     * @param zipCode
     *            es el codigo postal del area
     * @return ColonysResponseDTO
     *
     */
    public ResponseDTO getColonies(String zipCode) {
	String url = "https://api-codigos-postales.herokuapp.com/v2/codigo_postal/" + zipCode;
	ResponseDTO responseDTO = new ResponseDTO();
	GetColonysResponseDTO getColonysResponseDTO = new GetColonysResponseDTO();
	RestTemplate restTemplate = new RestTemplate();
	ColonysByZipApiResponseDTO colonysByZipApiResponseDTO = new ColonysByZipApiResponseDTO();


		try {
			colonysByZipApiResponseDTO = restTemplate.getForObject(url, ColonysByZipApiResponseDTO.class, 200);
		} catch (Exception ex) {

            LogService.logger.info("ERROR:"+ ex);
            LogService.logger.info("La data obtenida de codigo postal es la data local de BD...");
            ColonyZipcode colonyZipcodeBD = colonyZipcodeRepo.findByZipCodeLocal(zipCode);

            if(colonyZipcodeBD != null) {
                LogService.logger.info("Existe informacion de ZipCode en nuestra BD...");
                colonysByZipApiResponseDTO.setZip(colonyZipcodeBD.getZipCode());
                colonysByZipApiResponseDTO.setMunicipality(colonyZipcodeBD.getMunicipality());
                colonysByZipApiResponseDTO.setState(colonyZipcodeBD.getState());
                colonysByZipApiResponseDTO.setListColonys(new HashSet<>(Arrays.asList(colonyZipcodeBD.getColony().split("\\|"))));
            }else{
                LogService.logger.info("No existe informacion de ZipCode en nuestra BD...");
                MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0000.toString()).getDTO();
                List<MessageDTO> listMessageDTO = new ArrayList<>();
                listMessageDTO.add(messageNoRecords);
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setListMessage(listMessageDTO);
                responseDTO.setData(new GenericDTO());
                return responseDTO;
            }
		}

	if (colonysByZipApiResponseDTO.getListColonys().isEmpty()) {
	    MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0000.toString()).getDTO();
	    List<MessageDTO> listMessageDTO = new ArrayList<>();
	    listMessageDTO.add(messageNoRecords);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessageDTO);
	    responseDTO.setData(new ArrayList<>());
	    return responseDTO;
	} else {
	    ZipCode zipCodeBD = zipCodeRepo.findByZipCode(Long.valueOf(zipCode));
	    if (zipCodeBD == null) {
		MessageDTO messageNoRecords = messageRepo.findByCode(MessageEnum.M0000.toString()).getDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();
		listMessageDTO.add(messageNoRecords);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setListMessage(listMessageDTO);
		responseDTO.setData(new ArrayList<>());
		return responseDTO;
	    } else {
		Set<ColonyDataDTO> listColonyDataDTO = new HashSet<>();
		getColonysResponseDTO.setAllowsInspection(zipCodeBD.isServedZone());
		getColonysResponseDTO.setShipmentCost(zipCodeBD.getShippingCost());
		getColonysResponseDTO.setMunicipality(colonysByZipApiResponseDTO.getMunicipality());
		getColonysResponseDTO.setState(colonysByZipApiResponseDTO.getState());
		getColonysResponseDTO.setZip(colonysByZipApiResponseDTO.getZip());
		List<ColonyNotServed> listColonyNotServedBD = colonyNotServedRepo.findByZipCode(Long.valueOf(zipCode));
		for (String nameColonyActual : colonysByZipApiResponseDTO.getListColonys()) {
		    ColonyDataDTO newColonyDataDTO = new ColonyDataDTO();
		    newColonyDataDTO.setName(nameColonyActual);
		    // listColonyNotServedBD contiene 0 elementos significa que
		    // el codigo postal tiene permitdo las inspecciones
		    if (listColonyNotServedBD.size() == 0) {
			newColonyDataDTO.setAllowInspection(true);
		    } else {
			boolean flag = false;
			for (ColonyNotServed colonyNotServedActual : listColonyNotServedBD) {
			    if(flag == false) {
				if (StringUtils.stripAccents(newColonyDataDTO.getName().trim().toLowerCase()).equalsIgnoreCase(StringUtils.stripAccents(colonyNotServedActual.getName().trim().toLowerCase()))) {
				    newColonyDataDTO.setAllowInspection(false);
				    flag = true;
				} else {
				    newColonyDataDTO.setAllowInspection(true);
				}
			    }
			}
		    }
		    if(newColonyDataDTO.isAllowInspection()) {
			listColonyDataDTO.add(newColonyDataDTO);
		    }
		}
		getColonysResponseDTO.setListColonyDataDTO(listColonyDataDTO);
	    }
	}
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	responseDTO.setData(getColonysResponseDTO);
	return responseDTO;
    }

    /**
     * Consulta los centros de inspeccion en México
     * 
     * @author Enrique Marin
     * @param userLatitude
     *            latitude actual
     * @param userLongitude
     *            longitud actual
     * @param carId
     *            identicador del carro
     * @return List<InspectionLocationDTO>
     *
     */
    public ResponseDTO getKavakCenters(String userLatitude, String userLongitude, String date, Long hourBlockId, Long carId) {
	LogService.logger.info(Constants.LOG_EXECUTING_START + " (getKavakCenters) ");
	Long starTime = Calendar.getInstance().getTimeInMillis();
	ResponseDTO responseListDTO = new ResponseDTO();
	LinkedList<GetInspectionCentersResponseDTO> listGetInspectionCentersResponseDTO = new LinkedList<>();
	List<InspectionLocation> listInspectionLocation = inspectionLocationRepo.findAllActive();
	HashMap<Long, List<Date>> mapSechduleByDate = new HashMap<>();
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");	

	Calendar calendarInspectionDate = Calendar.getInstance();
	
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseListDTO.setCode(enumResult.getValue());
	responseListDTO.setStatus(enumResult.getStatus());
        
        // Máximo número de inspecciones por día a domicilio
        MetaValue maxInspectionsUserAddressPerDay = metaValueRepo.findByAlias("MAXINSPUSRADDRDAY");
        
        // Máximo número de inspecciones por bloque a domicilio
        MetaValue maxInspectionsUserAddressPerBlock = metaValueRepo.findByAlias("MAXINSPUSRADDRBLOCK");
        
        // Día especial con más inspecciones a domicilio (1-Domingo, 2-Lunes, etc, 0-Ningun dia)
        MetaValue noTopInspectionsUserAddressDay = metaValueRepo.findByAlias("NOTOPINSPUSRADDRDAY");
        
        //  Máximo número de inspecciones por día a domicilio, para dia especial con más inspecciones
        MetaValue noTopInspectionsUserAddressPerDay = metaValueRepo.findByAlias("NOTOPINSPUSRADDRPERDAY");
        
        //  Máximo número de inspecciones por bloque a domicilio, para dia especial con más inspecciones
        MetaValue noTopInspectionsUserAddressPerBlock = metaValueRepo.findByAlias("NOTOPINSPUSRADDRPERBLOCK");

	boolean availableUserAddress = true;
	boolean availableInspectionCenter = true;
	boolean availableCurrentCenter = true;

	String[] origin = { userLatitude + "," + userLongitude };
	String[] destination = new String[listInspectionLocation.size()];
	int indexDestination = 0;
	 
	if (date != null) {

	    try {
		Date dateInspection = formatter.parse(date);
		List<InspectionSchedule> listInspectionScheduleBD = inspectionScheduleRepo.findByInspectionDateAndIdHourBlock(dateInspection, hourBlockId);
		List<InspectionSchedule> listInspectionScheduleDomicilieBD = inspectionScheduleRepo.findByInspectionDateAndInspectionLocationId(dateInspection, 0L);
		
		calendarInspectionDate.setTime(dateInspection);	
		
		// IssueID #2870 - Dia de la semana con más inspecciones por bloque a domicilio
		Integer integerMaxInspectionsUserAddressPerDay = null;
		Integer integerMaxInspectionsUserAddressPerBlock = null; 
		// Se habilita un dia de la semana con un numero más alto de inspecciones por bloque.
		if(calendarInspectionDate.get(Calendar.DAY_OF_WEEK) == Integer.valueOf(noTopInspectionsUserAddressDay.getOptionName())) {
		    integerMaxInspectionsUserAddressPerDay = Integer.valueOf(noTopInspectionsUserAddressPerDay.getOptionName());
		    integerMaxInspectionsUserAddressPerBlock = Integer.valueOf(noTopInspectionsUserAddressPerBlock.getOptionName());
		} 
		// Comportamiento Normal Max inspecciones por bloque a domicilio viene de BD
		else {
		    integerMaxInspectionsUserAddressPerDay = Integer.valueOf(maxInspectionsUserAddressPerDay.getOptionName());
		    integerMaxInspectionsUserAddressPerBlock =  Integer.valueOf(maxInspectionsUserAddressPerBlock.getOptionName());
		}
		// ========================================================== >>

		for (InspectionSchedule actualInspectionSchedule : listInspectionScheduleBD) {
		    if (mapSechduleByDate.get(actualInspectionSchedule.getInspectionLocationId()) == null) {
			List<Date> listHourBlock = new ArrayList<>();
			listHourBlock.add(actualInspectionSchedule.getInspectionDate());
			mapSechduleByDate.put(actualInspectionSchedule.getInspectionLocationId(), listHourBlock);
		    } else {
			mapSechduleByDate.get(actualInspectionSchedule.getInspectionLocationId()).add(actualInspectionSchedule.getInspectionDate());
		    }
		}

		// fechas para calcular si es el dia actual.
		//Date today = new Date();
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		//String todaySf = df.format(today);
		//Date dateS = df.parse(date);
		//String dateSf = df.format(dateS);

		// Domicilio
		LogService.logger.info("Domicilio para la fecha " + date + " tiene disponible " + listInspectionScheduleDomicilieBD.size() + " bloques");
		
		// Max Inspecciones por Dia a Domicilio viene de BD
		if (listInspectionScheduleDomicilieBD.size() >= integerMaxInspectionsUserAddressPerDay) { 
		    availableUserAddress = false;
		} else {
		    if (mapSechduleByDate.get(Constants.INSPECTION_LOCATION_USER_ADDRESS) != null) {
			// Max inspecciones por bloque a domicilio viene de BD
		    	if (mapSechduleByDate.get(Constants.INSPECTION_LOCATION_USER_ADDRESS).size() >= integerMaxInspectionsUserAddressPerBlock) {
		    		availableUserAddress = false;
		    	}
		    }
		}

		// Centros Kavak
	/*	if (dateSf.equals(todaySf)) {
		    availableUserAddress = false;
		    availableInspectionCenter = false;
		}*/

	    } catch (ParseException e) {
	    	e.printStackTrace();
	    	EndPointCodeResponseEnum enumResultR = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		responseListDTO.setCode(enumResultR.getValue());
		responseListDTO.setStatus(enumResultR.getStatus());
		responseListDTO.setData(new GenericDTO());
		return responseListDTO;
	    }
	}

	Long idWarehouseLocation = -1L;
	if (carId != null) {
	    Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(carId);
	    idWarehouseLocation = sellCarDetailBD.get().getIdWarehouseLocation();
	}
	
	if (date != null && hourBlockId != null) {
	    GetInspectionCentersResponseDTO newGetInspectionCentersResponseDTO = new GetInspectionCentersResponseDTO();
	    newGetInspectionCentersResponseDTO.setId(Constants.INSPECTION_LOCATION_USER_ADDRESS);
	    newGetInspectionCentersResponseDTO.setName("Domicilio del Cliente ");
	    newGetInspectionCentersResponseDTO.setAvailable(availableUserAddress);
	    listGetInspectionCentersResponseDTO.add(newGetInspectionCentersResponseDTO);
	}	

	for (InspectionLocation inspectionLocationActual : listInspectionLocation) {
	    availableCurrentCenter = availableInspectionCenter;
	    GetInspectionCentersResponseDTO newGetInspectionCentersResponseDTO = new GetInspectionCentersResponseDTO();
	    newGetInspectionCentersResponseDTO.setId(inspectionLocationActual.getId());
	    newGetInspectionCentersResponseDTO.setName(inspectionLocationActual.getName());
	    newGetInspectionCentersResponseDTO.setAddress(inspectionLocationActual.getAddress());
	    newGetInspectionCentersResponseDTO.setCity(inspectionLocationActual.getCity());
	    newGetInspectionCentersResponseDTO.setColony(inspectionLocationActual.getColony());
	    newGetInspectionCentersResponseDTO.setCountry(inspectionLocationActual.getCountry());
	    newGetInspectionCentersResponseDTO.setState(inspectionLocationActual.getState());
	    newGetInspectionCentersResponseDTO.setZip(inspectionLocationActual.getZip());
	    newGetInspectionCentersResponseDTO.setLatitude(inspectionLocationActual.getLatitude().toString());
	    newGetInspectionCentersResponseDTO.setLongitude(inspectionLocationActual.getLongitude().toString());

	    
	    // Ubicacion disponible para inspeccion
	    // ======================================================================================================= >>
	    if (mapSechduleByDate.get(inspectionLocationActual.getId()) != null && mapSechduleByDate.get(inspectionLocationActual.getId()).size() >= inspectionLocationActual.getInspectionsPerBlock()) {
	    	availableCurrentCenter = false;
	    }
	    newGetInspectionCentersResponseDTO.setAvailable(availableCurrentCenter);
	    	
	    // IssueID #2276 Ajuste temporal para permitir 1 sola inspección por bloque (4 por dia) incluyendo Lerma
	    /*if (inspectionLocationActual.getId() == Constants.INSPECTION_LOCATION_LERMA) { // Lerma siempre disponible, salvo por excepcion  de regla de negocio
		
		// Excluido por regla de negocio
		if(date != null && (DateUtils.isSameDay(calendarInspectionDate, calendarDateBlocked)) ) {
		    	newGetInspectionCentersResponseDTO.setAvailable(false);
		} else {
	    		newGetInspectionCentersResponseDTO.setAvailable(true);
	    	}
	    
	    }*/
	    // ======================================================================================================= >>

	    
	    // Auto disponible en esta ubicación
	    // ======================================================================================================= >>
	    if (carId != null) {
	    	if (idWarehouseLocation == inspectionLocationActual.getId()) {
	    		newGetInspectionCentersResponseDTO.setCarAtThisLocation(true);
	    	} else {
	    		newGetInspectionCentersResponseDTO.setCarAtThisLocation(false);
	    	}
	    }
	    // ======================================================================================================= >>
	    

	    // Para cálculo de distancia y km a la ubicacion con coordenadas del usuario
	    // ======================================================================================================= >>
	    if (userLatitude != null && userLongitude != null) {
	    	if (indexDestination != listInspectionLocation.size()) {
	    		destination[indexDestination] = inspectionLocationActual.getLatitude() + "," + inspectionLocationActual.getLongitude() + "|";
	    	} else {
	    		destination[indexDestination] = inspectionLocationActual.getLatitude() + "," + inspectionLocationActual.getLongitude();
	    	}

			try {
			    GeoApiContext context = new GeoApiContext().setApiKey(Constants.GOOGLE_API_KEY);
			    DistanceMatrix distanceMatrix = DistanceMatrixApi.newRequest(context).origins(origin).destinations(destination).await();
			    DistanceMatrixRow[] rows = distanceMatrix.rows;
			    for (DistanceMatrixRow distanceMatrixRowActual : rows) {
					for (DistanceMatrixElement distanceMatrixElementActual : distanceMatrixRowActual.elements) {
					    if (distanceMatrixElementActual.distance != null && distanceMatrixElementActual.duration != null) {
					    	newGetInspectionCentersResponseDTO.setKmToAddress(distanceMatrixElementActual.distance.humanReadable);
							newGetInspectionCentersResponseDTO.setTimToAddress(distanceMatrixElementActual.duration.humanReadable);
					    }
					}
			    }
			} catch (Exception e) {
			    e.printStackTrace();
			    LogService.logger.error("Ha ocurrido un error al calcular distancia y km con origen= {" + userLatitude + "," + userLongitude + "} y locationId=" + newGetInspectionCentersResponseDTO.getId());
			}

			indexDestination++;
	    }
	    // ======================================================================================================= >>

	    listGetInspectionCentersResponseDTO.add(newGetInspectionCentersResponseDTO);
	}


	responseListDTO.setData(listGetInspectionCentersResponseDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " (getKavakCenters) [" + (Calendar.getInstance().getTimeInMillis() - starTime) + "]");
	return responseListDTO;
    }

    
    public ApiUrlShortenerResponseDTO postApiUrlShortener(String url) {
	ApiUrlShortenerResponseDTO apiUrlShortenerResponseDTO = new ApiUrlShortenerResponseDTO();
	String getUrl = "https://api-ssl.bitly.com/v3/shorten?access_token="+Constants.API_KEY_URL_SHORTENER+"&longUrl="+url;
	RestTemplate restTemplate = new RestTemplate();
	
	try {
	    apiUrlShortenerResponseDTO = restTemplate.getForObject(getUrl, ApiUrlShortenerResponseDTO.class, 200);
	    if(apiUrlShortenerResponseDTO.getData().getUrl().trim().contains("undefined")) {
		apiUrlShortenerResponseDTO.getData().setUrl(url);
		LogService.logger.error("Acortar Url dio error :"+ apiUrlShortenerResponseDTO.getData().getUrl()); 
	    }
	} catch (Exception ex) {
	    LogService.logger.info("Catch Respuesta postApiUrlShortener :"+ ex); 
	}
	return apiUrlShortenerResponseDTO;
    }

	/**
	 * Metodo que obtiene de la información de contacto de Kavak
	 *
	 * @author Oscar Montilla
	 * @return ResponseDTO response generico de servicio
	 */
	public ResponseDTO getContactKavak() {
		ResponseDTO responseDTO = new ResponseDTO();

		MetaValue metaValueEmailBD = metaValueRepo.findByGroupCategoryAndGroupNameAndAliasAndActiveTrue(Constants.META_VALUE_GROUP_KAVAK,Constants.META_VALUE_CONTACT, "email");
		MetaValue metaValuePhoneBD = metaValueRepo.findByGroupCategoryAndGroupNameAndAliasAndActiveTrue(Constants.META_VALUE_GROUP_KAVAK,Constants.META_VALUE_CONTACT, "phone");

		GenericDTO genericDTO = new GenericDTO();

		genericDTO.setEmail(metaValueEmailBD.getOptionName());
		genericDTO.setPhone(metaValuePhoneBD.getOptionName());

		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(genericDTO);

		return responseDTO;
    }
	
	

	public ResponseDTO getTestimonials(Long items) {
	    ResponseDTO responseDTO = new ResponseDTO();
	    List<Testimonials> listTestimonials = new ArrayList<>();
	    GetTestimonialsResponseDTO testimonialsResponseDTO = new GetTestimonialsResponseDTO();
	    List<GetTestimonialsResponseDTO> listTestimonialsResponse = new ArrayList<>();

	    if(items != null) {
		listTestimonials = TestimonialsRepo.findAllOrderByIdDesc(items);		
	    }else {
		listTestimonials = TestimonialsRepo.findAll();
	    }

	    if(!listTestimonials.isEmpty()) {
		for(Testimonials testimonialsActual : listTestimonials) {
		    testimonialsResponseDTO = new GetTestimonialsResponseDTO();
		    testimonialsResponseDTO.setCustomerName(testimonialsActual.getUserName()); 
		    testimonialsResponseDTO.setImageUrl(testimonialsActual.getImageAddress());
		    testimonialsResponseDTO.setTestimonialsDescription(testimonialsActual.getClientDescription());
		    testimonialsResponseDTO.setYoutubeVideoId(testimonialsActual.getIdTestimanialVideo());
		    listTestimonialsResponse.add(testimonialsResponseDTO);
		}
	    }

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(listTestimonialsResponse);

	    return responseDTO;
	}
	
	
	public ResponseDTO getPressNotes() {
	    ResponseDTO responseDTO = new ResponseDTO();
	    PressnotesResponseDTO PressnotesResponse = new PressnotesResponseDTO();
	    List<PressnotesResponseDTO> listPressnotesResponse = new ArrayList<>();
	    
	    List<PressNotes> listPressNotes = PressNotesRepo.findAllByActiveTrue();
	    if(!listPressNotes.isEmpty()) {
		for(PressNotes pressNotesActual : listPressNotes) {
		    PressnotesResponse = new PressnotesResponseDTO();
		    PressnotesResponse.setTitle(pressNotesActual.getUserName());
		    PressnotesResponse.setImageUrl(pressNotesActual.getImageAddress());
		    PressnotesResponse.setExternalUrl(pressNotesActual.getUserDescription());
		    listPressnotesResponse.add(PressnotesResponse);
		}
	    }
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(listPressnotesResponse);

	    return responseDTO;
	}
	
	
	public ResponseDTO getContactSubjects() {
	    ResponseDTO responseDTO = new ResponseDTO();
	    List<MetaValue> listMetaValue = new ArrayList<>();
	    GenericDTO genericDTO = new GenericDTO();
	    List<GenericDTO> listGenericDTO = new ArrayList<>();
	    listMetaValue = metaValueRepo.findByGroupNameAndActiveTrue(Constants.META_VALUE_OPERATION_TYPE);
	    if(!listMetaValue.isEmpty()) {
		for(MetaValue metaValueActual : listMetaValue) {
		    genericDTO = new GenericDTO();
		    genericDTO.setId(metaValueActual.getId());
		    genericDTO.setName(metaValueActual.getOptionName());
		    listGenericDTO.add(genericDTO);
		}
	    }
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(listGenericDTO);

	    return responseDTO;
	}
	
	
	
	
	public ResponseDTO postContact(PostContactRequestDTO postContactRequestDTO) {
	    
	    ResponseDTO responseDTO = new ResponseDTO();
	    String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" +
		      "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
	    
	    if(postContactRequestDTO.getNames() == null || postContactRequestDTO.getNames()== "") {
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("El campo names no puede ser null o vacio");
                return responseDTO;
	    }
	    
	    if(postContactRequestDTO.getLastNames() == null || postContactRequestDTO.getLastNames() == "") {
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("El campo last_names no puede ser null o vacio");
                return responseDTO;
	    }
	    
	    if(postContactRequestDTO.getPhone() == null || postContactRequestDTO.getPhone() == "") {
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("El campo phone no puede ser null o vacio");
                return responseDTO;
	    }
	    
	    if(postContactRequestDTO.getZipCode() == null || postContactRequestDTO.getZipCode() == "") {
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("El campo zip_code no puede ser null o vacio");
                return responseDTO;
	    }
	    
	    if(postContactRequestDTO.getEmail() == null || postContactRequestDTO.getEmail() == "") {
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("El campo email no puede ser null o vacio");
                return responseDTO;
	    }
	    
	    if(postContactRequestDTO.getSubjectId() == null) {
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("El campo subject_id no puede ser null o vacio");
                return responseDTO;
	    }
	    MetaValue metaValue = new MetaValue();
	    metaValue = metaValueRepo.findByIdAndActiveTrue(Long.valueOf(postContactRequestDTO.getSubjectId()));
	    List<MetaValue> listMetaValue = new ArrayList<>();
	    listMetaValue = metaValueRepo.findByGroupNameAndActiveTrue(Constants.META_VALUE_OPERATION_TYPE);
	    if(metaValue == null || !listMetaValue.contains(metaValue)) {
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("El campo subject_id es invalido");
                return responseDTO;
	    }
	    
	    
	    if(postContactRequestDTO.getComment() == null || postContactRequestDTO.getComment() == "") {
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("El campo comments no puede ser null o vacio");
                return responseDTO;
	    }
	   
	    Pattern pattern = Pattern.compile(emailPattern);
	    Matcher matcher = pattern.matcher(postContactRequestDTO.getEmail());
	    if (!matcher.matches()) {
                EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
                responseDTO.setCode(enumResult.getValue());
                responseDTO.setStatus(enumResult.getStatus());
                responseDTO.setData("Email no valido");
                return responseDTO;
	    }
	    ContactUs contactUs = new ContactUs();
	    contactUs.setNames(postContactRequestDTO.getNames());
	    contactUs.setLastNames(postContactRequestDTO.getLastNames());
	    contactUs.setPhone(postContactRequestDTO.getPhone());
	    contactUs.setZipCode(postContactRequestDTO.getZipCode());
	    contactUs.setEmail(postContactRequestDTO.getEmail());
	    contactUs.setSubjectId(Long.valueOf(postContactRequestDTO.getSubjectId()));
	    contactUs.setComments(postContactRequestDTO.getComment());
	    contactUsRepo.save(contactUs);

	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());

	    return responseDTO;
	}
	
	
	
	
	public ResponseDTO getTestimonialsList(String type) {
	    ResponseDTO responseDTO = new ResponseDTO();
	    PageMeta pageMetaBDSellerQuestion = new PageMeta();
	    PageMeta pageMetaBDBuyerQuestion = new PageMeta();
	    PageMeta pageMetaBDSellerAnswer = new PageMeta();
	    PageMeta pageMetaBDBuyerAnswer = new PageMeta();
	    PageMeta pageMetaBDTitleBuyer = new PageMeta();
	    PageMeta pageMetaBDTitleSeller = new PageMeta();
	    GetTestimonialsListResponse testimonialsListSellerResponse = new GetTestimonialsListResponse();
	    GetTestimonialsListResponse testimonialsListBuyerResponse = new GetTestimonialsListResponse();
	    List<GetTestimonialsListResponse> listTestimonialsListResponse = new ArrayList<>();
	    List<TestimonialsListItemsDTO> listTestimonialsListItemsDTO = new ArrayList<>();
	    TestimonialsListItemsDTO testimonialsListItemsSellerDTO = new TestimonialsListItemsDTO();
	    TestimonialsListItemsDTO testimonialsListItemsBuyerDTO = new TestimonialsListItemsDTO();

	    //seller
	    pageMetaBDTitleSeller = pageMetaRepo.findByMetaKey(Constants.PAGE_VALUE_SELLER_TITLE);
	    pageMetaBDSellerQuestion = pageMetaRepo.findByMetaKey(Constants.PAGE_VALUE_SELLER_QUESTION);
	    pageMetaBDSellerAnswer = pageMetaRepo.findByMetaKey(Constants.PAGE_VALUE_SELLER_ANSWER);
	    //buyer
	    pageMetaBDTitleBuyer = pageMetaRepo.findByMetaKey(Constants.PAGE_VALUE_BUYER_TITLE);
	    pageMetaBDBuyerQuestion = pageMetaRepo.findByMetaKey(Constants.PAGE_VALUE_BUYER_QUESTION);
	    pageMetaBDBuyerAnswer = pageMetaRepo.findByMetaKey(Constants.PAGE_VALUE_BUYER_ANSWER);
	    
	    //SPLIT SELLER
	    String[] sellerQuestions = pageMetaBDSellerQuestion.getMetaValue().split("\\|\\|");
	    String[] buyerQuestions = pageMetaBDBuyerQuestion.getMetaValue().split("\\|\\|");
	    //SPLIT BUYER
	    String[] sellerAnswer = pageMetaBDSellerAnswer.getMetaValue().split("\\|\\|");
	    String[] buyerAnswer = pageMetaBDBuyerAnswer.getMetaValue().split("\\|\\|");

	    
	    if(type != null && type.equals(Constants.SELLER.toString())) {
		//llenado de question y answer de seller
		for(int i=0;i < sellerQuestions.length;i++) {
		    testimonialsListItemsSellerDTO = new TestimonialsListItemsDTO();
		    testimonialsListItemsSellerDTO.setQuestion(sellerQuestions[i].toString());
		    testimonialsListItemsSellerDTO.setAnswer(sellerAnswer[i].toString());
		    listTestimonialsListItemsDTO.add(testimonialsListItemsSellerDTO);
		}

		testimonialsListSellerResponse.setType(Constants.SELLER);
		testimonialsListSellerResponse.setTitle(pageMetaBDTitleSeller.getMetaValue());
		testimonialsListSellerResponse.setItems(listTestimonialsListItemsDTO);
		listTestimonialsListResponse.add(testimonialsListSellerResponse);
	    }else if(type != null && type.equals(Constants.BUYER.toString())) {

		//llenado de buyer y answer de answer
		for(int i=0;i < buyerQuestions.length;i++) {
		    testimonialsListItemsBuyerDTO = new TestimonialsListItemsDTO();
		    testimonialsListItemsBuyerDTO.setQuestion(buyerQuestions[i].toString());
		    testimonialsListItemsBuyerDTO.setAnswer(buyerAnswer[i].toString());
		    listTestimonialsListItemsDTO.add(testimonialsListItemsBuyerDTO);
		}
		testimonialsListBuyerResponse.setType(Constants.BUYER);
		testimonialsListBuyerResponse.setTitle(pageMetaBDTitleBuyer.getMetaValue());
		testimonialsListBuyerResponse.setItems(listTestimonialsListItemsDTO);
		listTestimonialsListResponse.add(testimonialsListBuyerResponse);
	    }else {
		//llenado de question y answer de seller
		listTestimonialsListItemsDTO = new ArrayList<>();
		for(int i=0;i < sellerQuestions.length;i++) {
		    testimonialsListItemsSellerDTO = new TestimonialsListItemsDTO();
		    testimonialsListItemsSellerDTO.setQuestion(sellerQuestions[i].toString());
		    testimonialsListItemsSellerDTO.setAnswer(sellerAnswer[i].toString());
		    listTestimonialsListItemsDTO.add(testimonialsListItemsSellerDTO);
		}
		testimonialsListSellerResponse.setType(Constants.SELLER);
		testimonialsListSellerResponse.setTitle(pageMetaBDTitleSeller.getMetaValue());
		testimonialsListSellerResponse.setItems(listTestimonialsListItemsDTO);
		listTestimonialsListResponse.add(testimonialsListSellerResponse);

		//llenado de buyer y answer de answer
		listTestimonialsListItemsDTO = new ArrayList<>();
		for(int i=0;i < buyerQuestions.length;i++) {
		    testimonialsListItemsBuyerDTO = new TestimonialsListItemsDTO();
		    testimonialsListItemsBuyerDTO.setQuestion(buyerQuestions[i].toString());
		    testimonialsListItemsBuyerDTO.setAnswer(buyerAnswer[i].toString());
		    listTestimonialsListItemsDTO.add(testimonialsListItemsBuyerDTO);
		}
		testimonialsListBuyerResponse.setType(Constants.BUYER);
		testimonialsListBuyerResponse.setTitle(pageMetaBDTitleBuyer.getMetaValue());
		testimonialsListBuyerResponse.setItems(listTestimonialsListItemsDTO);
		listTestimonialsListResponse.add(testimonialsListBuyerResponse);
	    }
	    
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(listTestimonialsListResponse);

	    return responseDTO;
	}
	
	/**
	 * Busca mensajes (SMS,VOZ,EMAIL) que esten pendientes por enviarse acorde a la config dentro de CommunicationsTray
	 *
	 * @author Enrique Marin
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseDTO putCommunicationsTraySms() {
	    ResponseDTO responseDTO = new ResponseDTO();
	    Long starTime = Calendar.getInstance().getTimeInMillis();
	    LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCommunicationsTraySms] ");
	    Timestamp actualTimestamp = new Timestamp(System.currentTimeMillis());
	    SMSGateway smsGateway = new SMSGateway();
	    Hashtable props = new Hashtable();
	    // Se consulta la tabla para verificar los numeros que falten por enviarles el mensaje
	    List<CommunicationsTray> listCommunicationsTray = communicationsTrayRepo.findByScheduledDateAndSendId();
	    Calendar calendar = Calendar.getInstance();
	    LogService.logger.info("[putCommunicationsTraySms] - encontrados [" + listCommunicationsTray.size() + "] mensajes para enviar");
	    for (CommunicationsTray communicationsTrayBDActual : listCommunicationsTray) {
		if (communicationsTrayBDActual.getType().equalsIgnoreCase(Constants.TYPE_MSG_SMS)) {
		    if (communicationsTrayBDActual.getSubType().equalsIgnoreCase(Constants.TYPE_GROUP_MSG)) {
			LogService.logger.info("[putCommunicationsTraySms] - enviando [SMS] - single al telefono [" + communicationsTrayBDActual.getPhone() + "] con el mensaje [" + communicationsTrayBDActual.getMessage() + "]");
			// por lo visto no le puedo pasar configuracions
			try {
			    Integer sendId = smsGateway.sendMessage(communicationsTrayBDActual.getPhone(), communicationsTrayBDActual.getMessage());
			    communicationsTrayBDActual.setSendDate(actualTimestamp);
			    communicationsTrayBDActual.setSendProviderId(sendId);
			    communicationsTrayBDActual.setSendId(1);
			    communicationsTrayRepo.save(communicationsTrayBDActual);
			} catch (Exception e) {
			    communicationsTrayBDActual.setSendId(1);
			    communicationsTrayRepo.save(communicationsTrayBDActual);
			    LogService.logger.info("[putCommunicationsTraySms] - enviando [SMS] - Error");  
			    e.printStackTrace();
			}
		    }

		    if (communicationsTrayBDActual.getSubType().equalsIgnoreCase("group")) {
			calendar.setTimeInMillis(communicationsTrayBDActual.getSendDate().getTime());
			LogService.logger.info(" [SMS] - group");

			props.put("fechaInicio", calendar.get(Calendar.DAY_OF_MONTH) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.YEAR) + "/" + calendar.get(Calendar.HOUR) + "/" + calendar.get(Calendar.MINUTE));
			File csvFile = new File(Constants.MESSAGE_TEMPLATE_PATH + communicationsTrayBDActual.getCsvName());

			try {
			    URL urlCsvFile = new URL("http://www.kavak.com/pricing/reports/data_sms.csv");
			    FileUtils.copyURLToFile(urlCsvFile, csvFile);
			    Integer sendId = smsGateway.sendCSVFile(Constants.MESSAGE_TEMPLATE_PATH + communicationsTrayBDActual.getCsvName(), communicationsTrayBDActual.getMessage(), props);
			    communicationsTrayBDActual.setSendProviderId(sendId);
			    communicationsTrayBDActual.setSendId(1);
			    communicationsTrayBDActual.setSendDate(actualTimestamp);
			    responseDTO.setData(communicationsTrayRepo.save(communicationsTrayBDActual).getDTO().getId());
			    FileUtils.forceDelete(csvFile);
			} catch (GatewayException e) {
			    communicationsTrayBDActual.setSendId(1);
			    communicationsTrayRepo.save(communicationsTrayBDActual);
			    e.printStackTrace();

			} catch (MalformedURLException e) {
			    e.printStackTrace();
			} catch (IOException e) {
			    e.printStackTrace();
			}
		    }
		}

		if (communicationsTrayBDActual.getType().equalsIgnoreCase("voice")) {

		}

		if (communicationsTrayBDActual.getType().equalsIgnoreCase("email")) {

		}

	    }
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    LogService.logger.info(Constants.LOG_EXECUTING_END + " [putCommunicationsTraySms] " + (Calendar.getInstance().getTimeInMillis() - starTime));
	    return responseDTO;
	}


	/**
	 * Registra un texto para ser envio como sms (Mensaje de Texto) en México
	 *
	 * @param postMessageDTO parametros para el mensaje
	 * @return CommunicationsTrayDTO
	 * @author Enrique Marin
	 */
	public ResponseDTO postMessage(PostMessageRequestDTO postMessageDTO) {
	    Long starTime = Calendar.getInstance().getTimeInMillis();
	    LogService.logger.info(Constants.LOG_EXECUTING_START + " [postMessage] ");
	    ResponseDTO responseDTO = new ResponseDTO();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Timestamp actualTimestamp = new Timestamp(System.currentTimeMillis());
	    CommunicationsTray newCommunicationsTray = new CommunicationsTray();
	    newCommunicationsTray.setPhone(StringUtils.substring(postMessageDTO.getPhone(), 0, 20));
	    newCommunicationsTray.setMessage(postMessageDTO.getMessage());
	    newCommunicationsTray.setEmail(postMessageDTO.getEmail());
	    newCommunicationsTray.setProviderName("Calixta");
	    newCommunicationsTray.setType(Constants.TYPE_MSG_SMS);
	    newCommunicationsTray.setSubType(Constants.TYPE_GROUP_MSG);
	    newCommunicationsTray.setSendId(0);
	    newCommunicationsTray.setCreateDate(actualTimestamp);

	    try {
		Date scheduleDate = sdf.parse(postMessageDTO.getScheduleDate());
		Timestamp scheduleDateTime = actualTimestamp;
		scheduleDateTime.setTime(scheduleDate.getTime());
		newCommunicationsTray.setScheduledDate(scheduleDateTime);
	    } catch (ParseException e) {
		e.printStackTrace();
	    }

	    CommunicationsTray communicationsTrayBD = communicationsTrayRepo.save(newCommunicationsTray);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setData(communicationsTrayBD.getDTO().getId());
	    LogService.logger.info(Constants.LOG_EXECUTING_END + " [postMessage] " + (Calendar.getInstance().getTimeInMillis() - starTime));
	    return responseDTO;
	}
}
