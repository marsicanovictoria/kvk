package com.kavak.core.service.netsuite;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.kavak.core.dto.CarDataDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.MinervaAndNetsuiteIdsDTO;
import com.kavak.core.dto.OfferCheckpointDTO;
import com.kavak.core.dto.RequestNetsuiteDTO;
import com.kavak.core.dto.UserDTO;
import com.kavak.core.dto.netsuite.PurchaseOpportunityResponseDTO;
import com.kavak.core.dto.response.CustomerResponseDTO;
import com.kavak.core.dto.response.NetsuiteOpportunityDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.CarColor;
import com.kavak.core.model.CarData;
import com.kavak.core.model.InspectionLocation;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.OfferCheckpoint;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.CarColorRepository;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.OfferCheckpointRepository;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

@Stateless
public class PurchaseOpportunityServices {

	@Inject
	private OfferCheckpointRepository offerCheckPointRepo;
	
	@Inject
	private UserRepository userRepo;
	
	@Inject
	private CarDataRepository carDataRepo;
	
	@Inject
	private UserMetaRepository userMetaRepo;
	
	@Inject
	private MetaValueRepository metaValueRepo;
	
	@Inject
	private InspectionLocationRepository inspectionLocationRepo;
	
	@Inject
	private MessageRepository messageRepo;
	
	@Inject
	private CarColorRepository carColorRepo;
	
	public ResponseDTO getPurchaseOpportunityById(Long id){
		ResponseDTO responseDTO = new ResponseDTO();
		OfferCheckpointDTO offerCheckpointResponseDTO = null;
		
		try {
			LogService.logger.info("Consultando los datos de la oportunidad de compra ID: " + id);
			Optional<OfferCheckpoint> offerCheckpointBD = offerCheckPointRepo.findById(id);
			
			if (offerCheckpointBD.isPresent()){
				offerCheckpointResponseDTO = offerCheckpointBD.get().getDTO();
			}
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());;
	        responseDTO.setData(offerCheckpointResponseDTO);
			
		} catch (Exception e) {
			LogService.logger.error("Error al intentar consultar Purchase Opportunity ID " +id+ ", " + e);
		}
		
		return responseDTO;
	}
	
	
	@SuppressWarnings("unused")
	public ResponseDTO getNewPurchaseOpportunities(){
        ResponseDTO responseListDTO = new ResponseDTO();
		OfferCheckpointDTO offerCheckpointDTO = null;
		int minutes       = 2;
		
		try {
			LogService.logger.info("Consultando nuevas oportunidades de compra para ser enviados a Netsuite");
			
			List<PurchaseOpportunityResponseDTO> listPurchaseOpportunityDTO = new ArrayList<>();

			MetaValue metaValueBD = metaValueRepo.findByAlias("MINUTES_PURCHASE_CHECKPOINT");
			
			if (metaValueBD != null){
				minutes =   Integer.parseInt(metaValueBD.getOptionName());
			}
			
			List<OfferCheckpoint> listPurchaseOpportunitiesBD     = offerCheckPointRepo.findBySendNetsuite();
			List<OfferCheckpoint> listResendPurchaseOpportunityBD = offerCheckPointRepo.findByResendNetsuite();
			
			if (listPurchaseOpportunitiesBD != null){
				
				LogService.logger.info("Total nuevas oportunidades de compra encontradas: " + listPurchaseOpportunitiesBD.size());
				
				for (OfferCheckpoint pucharseOpportunity : listPurchaseOpportunitiesBD){
					
					if (pucharseOpportunity != null){ 
						offerCheckpointDTO = new OfferCheckpointDTO();
						offerCheckpointDTO = pucharseOpportunity.getDTO();
					
						PurchaseOpportunityResponseDTO purchaseOpportunity = new PurchaseOpportunityResponseDTO();
						
						//LogService.logger.info("Transfiriendo de offerCheckpointDTO a PurchaseOpportunityResponseDTO la oferta ID a enviar: " + offerCheckpointDTO.getId());
						purchaseOpportunity = transferOfferCheckpointToPurchaseOpportunity(offerCheckpointDTO);
						listPurchaseOpportunityDTO.add(purchaseOpportunity);
					}
				}
			}else{
				LogService.logger.info("Total Purchases Opportunities encontradas para enviar a Netsuite: 0");
			}
			
			if ((listResendPurchaseOpportunityBD != null) && (listResendPurchaseOpportunityBD.size() > 0)){
				LogService.logger.info("Total Purchases Opportunities encontradas para reenviar a Netsuite: " + listResendPurchaseOpportunityBD.size());
				
				for (OfferCheckpoint resendPurchaseOpportunity : listResendPurchaseOpportunityBD){
					if (resendPurchaseOpportunity != null){
						offerCheckpointDTO = new OfferCheckpointDTO();
						offerCheckpointDTO = resendPurchaseOpportunity.getDTO();
						
						//LogService.logger.info("Transfiriendo de offerCheckpointDTO a PurchaseOpportunityResponseDTO la oferta ID a reenviar: " + offerCheckpointDTO.getId());
						PurchaseOpportunityResponseDTO pucharseOpportunityDTO = new PurchaseOpportunityResponseDTO();
						pucharseOpportunityDTO = transferOfferCheckpointToPurchaseOpportunity(offerCheckpointDTO);
						listPurchaseOpportunityDTO.add(pucharseOpportunityDTO);
					}
				}
			}else{
				LogService.logger.info("Total Purchases Opportunities encontradas para reenviar a Netsuite: 0");
			}

			Collections.sort(listPurchaseOpportunityDTO, (o1, o2) -> o1.getId() - o2.getId());

	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseListDTO.setCode(enumResult.getValue());
	        responseListDTO.setStatus(enumResult.getStatus());
	        responseListDTO.setData(listPurchaseOpportunityDTO);
	        responseListDTO.setNetsuiteMethod(Constants.PURCHASE_OPPORTUNITY_METHOD);
	        
			LogService.logger.info("Consultando nuevas oportunidades de compra para ser enviados a Netsuite, exitoso.");
		} catch (Exception e) {
			LogService.logger.error("Error al intentar consultar los nuevos Purchase Opportunities ");
			e.printStackTrace();
		}finally {
			LogService.logger.info("Fin de consulta de Purchase Opportunities. ");
		}
		
		return responseListDTO;
	}
	
	public ResponseDTO putSentPurchaseOpportunities(String purchaseOpportunitiesId){
		
		ResponseDTO responseDTO = null;
		
		if (!purchaseOpportunitiesId.isEmpty()){
			
			responseDTO = new ResponseDTO();
			
			purchaseOpportunitiesId = purchaseOpportunitiesId.replaceAll("\"","").replaceAll("code:200,status:OK,data:", "").replaceAll("\\{", "").replaceAll("\\}", "");

			LogService.logger.info("IDs de Purchase Opportunities a actualizar como enviado en BD " + purchaseOpportunitiesId);

			try {

				if (!purchaseOpportunitiesId.equals("null") && !purchaseOpportunitiesId.equals("%[0]%")){
					
					List<String> items = Arrays.asList(purchaseOpportunitiesId.split("\\s*,\\s*"));

					for (String idsAndStatus : items){
						
						String[] parts = idsAndStatus.split(":");
						
						Long opportunityId = Long.valueOf(parts[0]);
						Long statusOpportunityId = Long.valueOf(parts[1]);
						
						LogService.logger.info("Buscando purchase oportunidad ID " + opportunityId);
						
						Optional<OfferCheckpoint> offerCheckpointBD = offerCheckPointRepo.findById(opportunityId);
						
						if (offerCheckpointBD.isPresent()){
							
							//LogService.logger.info("offerCheckpointBD.get().getIdStatus(): " + offerCheckpointBD.get().getIdStatus() + " statusOpportunityId: " + statusOpportunityId);
							
							if (offerCheckpointBD.get().getIdStatus().longValue() == statusOpportunityId.longValue()){
								LogService.logger.info("Sale Opportunity ID " + opportunityId + " con status encontrado en BD: " + offerCheckpointBD.get().getIdStatus() + ", status que trae desde netsuite " + statusOpportunityId + ", se marca en 1");
								offerCheckpointBD.get().setSentNetsuite(1L);
							}else{
								LogService.logger.info("Sale Opportunity ID " + opportunityId + " con status encontrado en BD: " + offerCheckpointBD.get().getIdStatus() + ", status que trae desde netsuite " + statusOpportunityId + ", se marca en 2");
								offerCheckpointBD.get().setSentNetsuite(2L);
							}
							
							offerCheckpointBD.get().setNetsuiteIdAttemps(0L);
							
							offerCheckPointRepo.save(offerCheckpointBD.get());
							
							LogService.logger.info("Actualizando purchase opportunity ID " + opportunityId + " con exito" );
						}
					}
				}
				
			} catch (Exception e) {
				LogService.logger.error("Error al intentar actualizar el estatus de envío para netsuite de Purchase Opportunities, " + e);
				e.printStackTrace();
			}
		}else{
			LogService.logger.info("No hay Purchases Opportunities por actualizar como enviado, " + purchaseOpportunitiesId);	
		}
		
		return responseDTO;
	}

	/*
	public ResponseDTO putSentPurchaseOpportunities(){
		ResponseDTO responseDTO = null;
		int minutes = 2;
		
		try {
			LogService.logger.info("Actualizando estatus de envio de Netsuite (Purchase Opportunity) mayores a 15 minutos ");
			responseDTO = new ResponseDTO();
			
			MetaValue metaValueBD = metaValueRepo.findByAlias("MINUTES_PURCHASE_CHECKPOINT");
			
			if (metaValueBD != null){
				minutes =   Integer.parseInt(metaValueBD.getOptionName());
			}
			
			List<OfferCheckpoint> purchaseOpportunityBD = offerCheckPointRepo.findBySentNetsuitePastMinute(minutes);
			
			if ((purchaseOpportunityBD != null) &&(purchaseOpportunityBD.size() > 0)){
				
				LogService.logger.info("Cantidad de Purchases Opportunities por actualizar estatus de enviado: " + purchaseOpportunityBD.size() );
				
				for(OfferCheckpoint offerCheck : purchaseOpportunityBD){
					offerCheck.setSentNetsuite(true);
				}
				offerCheckPointRepo.save(purchaseOpportunityBD);
			}else{
				LogService.logger.info("Cantidad de Purchases Opportunities por actualizar estatus de enviado: 0");
			}
			
			responseDTO.setCode(Constants.OK);
			responseDTO.setStatus(EndPointsStatusResponse.OK.toString());
		} catch (Exception e) {
			LogService.logger.error("Error al intentar actualizar el estatus de envío para netsuite de Purchases Opportunities mayores a 15 minutos, " + e);
			e.printStackTrace();
		}
		
		
		return responseDTO;
	}
	*/
	
	
	public PurchaseOpportunityResponseDTO transferOfferCheckpointToPurchaseOpportunity(OfferCheckpointDTO offerCheckpointDTO){
		PurchaseOpportunityResponseDTO purchaseOpportunityDTO = null;
		
		try {
			//LogService.logger.info("Transfiriendo datos de OfferCheckpointDTO a PurchaseOpportunityResponseDTO");
			
			purchaseOpportunityDTO = new PurchaseOpportunityResponseDTO();
			Optional<User> userBD = userRepo.findById(offerCheckpointDTO.getIdUser());
			CarData carDataBD = carDataRepo.findBySku(offerCheckpointDTO.getSku());
			
			if (userBD.isPresent()){

				// Se actualizan los datos del cliente
				userBD.get().setName(KavakUtils.cleanString(userBD.get().getName()).trim());
				userBD.get().setUserName(KavakUtils.cleanString(userBD.get().getUserName().trim()));
				userBD.get().setEmail(KavakUtils.cleanString(userBD.get().getEmail().trim()));
				userBD.get().setRole(KavakUtils.cleanString(userBD.get().getRole()));

				purchaseOpportunityDTO.setCustomer(new CustomerResponseDTO());
				purchaseOpportunityDTO.getCustomer().setId(userBD.get().getId());
				purchaseOpportunityDTO.getCustomer().setName(userBD.get().getName().trim());
				purchaseOpportunityDTO.getCustomer().setUsername(userBD.get().getUserName().trim());
				purchaseOpportunityDTO.getCustomer().setEmail(userBD.get().getEmail().trim());
				purchaseOpportunityDTO.getCustomer().setRole(userBD.get().getRole());

				userRepo.save(userBD.get());

				UserMeta userPhoneBD = userMetaRepo.findUserMetaByMetaKeyAndId(Constants.META_VALUE_PHONE, userBD.get().getId());
				if (userPhoneBD != null){
					userPhoneBD.setMetaValue(KavakUtils.cleanString(userPhoneBD.getMetaValue()));
					purchaseOpportunityDTO.getCustomer().setPhone(userPhoneBD.getMetaValue());
					userMetaRepo.save(userPhoneBD);
				}
				
				UserMeta userAddressBD = userMetaRepo.findUserMetaByMetaKeyAndId(Constants.META_VALUE_ADDRESS, userBD.get().getId());
				if (userAddressBD != null){
					userAddressBD.setMetaValue(KavakUtils.cleanString(userAddressBD.getMetaValue()));
					purchaseOpportunityDTO.getCustomer().setAddressNetsuite(userAddressBD.getMetaValue());
					userMetaRepo.save(userAddressBD);
				}
			}
			
			if (carDataBD != null){
				purchaseOpportunityDTO.setCar(new CarDataDTO());
				purchaseOpportunityDTO.setCar(carDataBD.getDTO());
				purchaseOpportunityDTO.getCar().setCarMake(carDataBD.getDTO().getCarMake().trim());
				purchaseOpportunityDTO.getCar().setCarTrim(carDataBD.getDTO().getCarTrim().trim());
				
				//guarda en BD el strng sn espacion
				carDataRepo.save(carDataBD);
				

				if (!carDataBD.getBuyPrice().equals("NA")){
					purchaseOpportunityDTO.setAutometricGuidePurchasePrice(Double.parseDouble(carDataBD.getBuyPrice()));
				}
				
				if (!carDataBD.getSalePrice().equals("NA")){
					purchaseOpportunityDTO.setAutometricGuideSalePrice(Double.parseDouble(carDataBD.getSalePrice()));
				}
				
				if (!carDataBD.getVersionGuiaAutometrica().equals("NA")){
					purchaseOpportunityDTO.setGaVersion(carDataBD.getVersionGuiaAutometrica());
				}
				
				if (!carDataBD.getMarketPrice().equals("NA")){
					purchaseOpportunityDTO.setMarketPrice(Double.parseDouble(carDataBD.getMarketPrice()));
				}
				
				if (!carDataBD.getSalePriceKavak().equals("NA")){
					purchaseOpportunityDTO.setEstimateSalePrice(Double.parseDouble(carDataBD.getSalePriceKavak()));
				}
				
				if(!carDataBD.getOfferDetail().isEmpty()){
					purchaseOpportunityDTO.setOfferDetail(carDataBD.getOfferDetail().toUpperCase().trim());
				}
			}
			
			if (offerCheckpointDTO.getMax30DaysOffer() != null){
				purchaseOpportunityDTO.setMax30DaysOffer(Double.parseDouble(clearCharacters(offerCheckpointDTO.getMax30DaysOffer())));
				purchaseOpportunityDTO.setMin30DaysOffer(Double.parseDouble(clearCharacters(offerCheckpointDTO.getMin30DaysOffer())));
			}
			
			if (offerCheckpointDTO.getMaxInstantOffer() != null){
				purchaseOpportunityDTO.setMaxInstantOffer(Double.parseDouble(clearCharacters(offerCheckpointDTO.getMaxInstantOffer())));
				purchaseOpportunityDTO.setMinInstantOffer(Double.parseDouble(clearCharacters(offerCheckpointDTO.getMinInstantOffer())));
			}
			
			if (offerCheckpointDTO.getMaxConsigmentOffer() != null){ 
				purchaseOpportunityDTO.setMaxConsigmentOffer(Double.parseDouble(clearCharacters(offerCheckpointDTO.getMaxConsigmentOffer())));
				purchaseOpportunityDTO.setMinConsigmentOffer(Double.parseDouble(clearCharacters(offerCheckpointDTO.getMinConsigmentOffer())));
			}
			
			if(!offerCheckpointDTO.getOfferSource().isEmpty()){
				purchaseOpportunityDTO.setSource(offerCheckpointDTO.getOfferSource().toUpperCase().trim());
			}
			
			purchaseOpportunityDTO.setId(offerCheckpointDTO.getId().intValue());
			//purchaseOpportunityDTO.setLeadMinervaId((userBD.get().getId() + Constants.USER_ID_OFFSET));
			purchaseOpportunityDTO.setLeadMinervaId(userBD.get().getId());
			purchaseOpportunityDTO.setInspectionScheduleTime(offerCheckpointDTO.getIdHourInpectionSlot());
			purchaseOpportunityDTO.setInspectionScheduleDate(offerCheckpointDTO.getInspectionDate());
			purchaseOpportunityDTO.setInspectionScheduleTimeDesc(offerCheckpointDTO.getHourInpectionSlotText());
			
			if (offerCheckpointDTO.getInspectionLocationDescription() != null){
				purchaseOpportunityDTO.setInspectionLocationDescription(offerCheckpointDTO.getInspectionLocationDescription().replaceAll("\n", "").replaceAll("\r", "").replaceAll("\b", "").replaceAll("\t", ""));	
			}
			
			if (offerCheckpointDTO.getExteriorColorId() != null){
				CarColor CarColorBD = carColorRepo.getOne(offerCheckpointDTO.getExteriorColorId());
				
				if (CarColorBD != null){
					purchaseOpportunityDTO.setExteriorColor(CarColorBD.getName().toUpperCase());
				}
			}
			
			purchaseOpportunityDTO.setCarInvoiceUrl(offerCheckpointDTO.getCarInvoiceUrl());
			purchaseOpportunityDTO.setCirculationCardUrl(offerCheckpointDTO.getCirculationCardUrl());
			purchaseOpportunityDTO.setZipCode(Long.valueOf(offerCheckpointDTO.getZipCode()));
			purchaseOpportunityDTO.setStatusId(offerCheckpointDTO.getStatusId());
			purchaseOpportunityDTO.setStatusDetail(offerCheckpointDTO.getStatusDetail());
			purchaseOpportunityDTO.setNetsuiteItemId(offerCheckpointDTO.getCodeNetsuiteItem());
			purchaseOpportunityDTO.setCarKm(offerCheckpointDTO.getCarKm());
			purchaseOpportunityDTO.getCar().setFullVersionCar(offerCheckpointDTO.getFullVersion());
			purchaseOpportunityDTO.setOfferType(offerCheckpointDTO.getOfferType());
			purchaseOpportunityDTO.setOfferTypeDescription(offerCheckpointDTO.getOfferTypeDescription());
			purchaseOpportunityDTO.setDuplicatedOfferControl(offerCheckpointDTO.getDuplicatedOfferControl());
			purchaseOpportunityDTO.setWishlist(offerCheckpointDTO.getWishList());
			purchaseOpportunityDTO.setReliabilitySample(offerCheckpointDTO.getReliabilitySample());
			
			if (offerCheckpointDTO.getRegisterDate() != null){
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				Date registerDateUnclean = formatter.parse(offerCheckpointDTO.getRegisterDate());
				purchaseOpportunityDTO.setRegisterDate(formatter.format(registerDateUnclean));
				
				String [] registerTime = offerCheckpointDTO.getRegisterDate().split(" ");
				String time = registerTime[1];
				String [] registerTimeFinal = time.split(".0");
				
				purchaseOpportunityDTO.setRegisterTime(registerTimeFinal[0]);
			}
			
			if (offerCheckpointDTO.getUpdateDateOffer() != null){
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				Date updateDateUnclean = formatter.parse(offerCheckpointDTO.getUpdateDateOffer());
				purchaseOpportunityDTO.setUpdateDateOffer(formatter.format(updateDateUnclean));
			}
			
			if (offerCheckpointDTO.getAssignedEmId() != null){
				Optional<User> emBD = userRepo.findById(offerCheckpointDTO.getAssignedEmId());
				
				if (emBD.isPresent()){
					
					//purchaseOpportunityDTO.setExperienceManager(new UserDTO());
					//purchaseOpportunityDTO.getExperienceManager().setId(emBD.get().getId());
					//purchaseOpportunityDTO.getExperienceManager().setName(emBD.get().getName());
					//purchaseOpportunityDTO.getExperienceManager().setEmail(emBD.get().getEmail());
					//purchaseOpportunityDTO.getExperienceManager().setUsername(emBD.get().getUserName());
					//purchaseOpportunityDTO.getExperienceManager().setRole(emBD.get().getRole());

					purchaseOpportunityDTO.setWingmanInspection(new UserDTO());
					purchaseOpportunityDTO.getWingmanInspection().setId(emBD.get().getId());
					purchaseOpportunityDTO.getWingmanInspection().setName(emBD.get().getName());
					purchaseOpportunityDTO.getWingmanInspection().setEmail(emBD.get().getEmail());
					purchaseOpportunityDTO.getWingmanInspection().setUsername(emBD.get().getUserName());
					purchaseOpportunityDTO.getWingmanInspection().setRole(emBD.get().getRole());
				
				}
			}
			
			/*
			if(offerCheckpointDTO.getInternalUserId() != null){
				Optional<User> wingmanBD = userRepo.findById(offerCheckpointDTO.getInternalUserId());
				
				if(wingmanBD.isPresent()){
					purchaseOpportunityDTO.setWingmanInspection(new UserDTO());
					
					purchaseOpportunityDTO.getWingmanInspection().setId(wingmanBD.get().getId());
					purchaseOpportunityDTO.getWingmanInspection().setName(wingmanBD.get().getName());
					purchaseOpportunityDTO.getWingmanInspection().setEmail(wingmanBD.get().getEmail());
					purchaseOpportunityDTO.getWingmanInspection().setUsername(wingmanBD.get().getUserName());
					purchaseOpportunityDTO.getWingmanInspection().setRole(wingmanBD.get().getRole());
				}
			}
			*/
			
			if (offerCheckpointDTO.getInspectionCenterDescription() != "" || offerCheckpointDTO.getInspectionCenterDescription() != null){
				InspectionLocation inspectLocationBD = inspectionLocationRepo.findByName(offerCheckpointDTO.getInspectionCenterDescription());
				
				if (inspectLocationBD != null){
					purchaseOpportunityDTO.setInspectionLocationTypeId(inspectLocationBD.getId());
					purchaseOpportunityDTO.setInspectionLocationTypeDescription(inspectLocationBD.getName());
				}else{
					purchaseOpportunityDTO.setInspectionLocationTypeId(0L);
					purchaseOpportunityDTO.setInspectionLocationTypeDescription("Dirección del Cliente");
				}
				
			}else{
				LogService.logger.debug("La oportunidad " + offerCheckpointDTO.getId() + " no tiene Centro de Inspeccion Asignado");
			}
			
			// A-B TEST APPLIED
			if(offerCheckpointDTO.getAbTestResult() == null){
				purchaseOpportunityDTO.setAbTestApplied(false);
			}else{
				purchaseOpportunityDTO.setAbTestApplied(true);
				purchaseOpportunityDTO.setAbTestCode(offerCheckpointDTO.getAbTestCode().trim().toUpperCase());
				purchaseOpportunityDTO.setAbTestResult(offerCheckpointDTO.getAbTestResult());
				MetaValue metaValueDB = metaValueRepo.findByAlias(offerCheckpointDTO.getAbTestCode().trim());
				
				if(metaValueDB != null){
					purchaseOpportunityDTO.setAbTestDescription(metaValueDB.getOptionName().toUpperCase().trim());
					purchaseOpportunityDTO.setAbTestStageDescription(metaValueDB.getGroupName().toUpperCase().trim());
				}
			}
			
			
		} catch (Exception e) {
			LogService.logger.error("Error al intentar transferir DTO OffersCheckpoint to PurchaseOpportunity , " + e);
			e.printStackTrace();
		}

		return purchaseOpportunityDTO;
	}
	
	
	public ResponseDTO getPurchaseOpportunityWithoutNetsuiteId(){
		ResponseDTO responseDTO = new ResponseDTO();
		ArrayList<String> minervasIds = null;
		int quantityMinervaIds = 10; 
		
		try {
			LogService.logger.info("Consultando las oportunidades de compra sin Netsuite ID" );
			
			List<OfferCheckpoint> listofferCheckpointBD = offerCheckPointRepo.findByNetsuiteOpportunityIdEmpty();
			
			if (listofferCheckpointBD != null){
				
				minervasIds = new ArrayList<>();
				int counter = 0;
				for(OfferCheckpoint minervaIds : listofferCheckpointBD){
					if (counter < quantityMinervaIds){
						LogService.logger.debug("ID Oferta: "+ minervaIds.getId() + " Control Oferta Duplicada ID a buscar en Netsuite: " + minervaIds.getDuplicatedOfferControl());
						minervasIds.add(minervaIds.getDuplicatedOfferControl().toString());
					}else{
						break;
					}
					counter++;
				}
			}else{
				LogService.logger.info("Total de oportunidades de compra sin Netsuite ID: 0");
			}
			
			LogService.logger.debug("IDs Minerva recuperados" + minervasIds);
			
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        responseDTO.setCode(enumResult.getValue());
	        responseDTO.setStatus(enumResult.getStatus());
	        responseDTO.setData(minervasIds);
			
		} catch (Exception e) {
			LogService.logger.error("Error al intentar consultar Purchase Opportunity ID Without Netsuite ID" , e );
		}
		
		return responseDTO;
	}
	
	public ResponseDTO putNetsuiteOpportunityIdIntoMinerva(String netsuiteAndMinervaIds){
		ResponseDTO responseDTO = new ResponseDTO();
		
		LogService.logger.info("Se procede a actualizar campos netsuite_id y URL de la oportunidad en Netsuite ");
		
		
		if (!netsuiteAndMinervaIds.isEmpty()){
			try {
				netsuiteAndMinervaIds = netsuiteAndMinervaIds.replace("\"", "'").replace("{'code':200,'status':'OK','data':[{", "").replace("}]}", "").replace("},{", ",").replace("'", "").replace(",minerva_id", "#minerva_id");
				
				List<MinervaAndNetsuiteIdsDTO> minervaNetsuiteListDTO = new ArrayList<>();
				
				String[] pairs = netsuiteAndMinervaIds.split("#");
				
				for (int i=0;i<pairs.length;i++) {
				    String pair       = pairs[i];
				    String[] keyValue = pair.split(",");
				    String[] minerva  = keyValue[0].split(":");
				    String[] netsuite = keyValue[1].split(":");

				    MinervaAndNetsuiteIdsDTO netsuiteMinervaId = new MinervaAndNetsuiteIdsDTO();
				    netsuiteMinervaId.setDuplicatedOfferControl(Long.valueOf(minerva[1]));
				    netsuiteMinervaId.setNetsuiteId(Long.valueOf(netsuite[1]));
				    
				    minervaNetsuiteListDTO.add(netsuiteMinervaId);
				}

				for (MinervaAndNetsuiteIdsDTO mnIds : minervaNetsuiteListDTO){
					
					List<OfferCheckpoint> offerCheckpointBD = offerCheckPointRepo.findByDuplicatedOfferControl(mnIds.getDuplicatedOfferControl());
					
					if (offerCheckpointBD != null){
						
						for(OfferCheckpoint offerCheck : offerCheckpointBD){
							
							if(mnIds.getNetsuiteId() == 0){
								if(offerCheck.getNetsuiteIdAttemps() == null){
									offerCheck.setNetsuiteIdAttemps(0L);
								}
								
								Long attemps = offerCheck.getNetsuiteIdAttemps() + 1;
								offerCheck.setNetsuiteIdAttemps(attemps);
								LogService.logger.info("La oportunidad " + offerCheck.getId() + " no tiene aun ID netsuite, intento #" + attemps);
							}else{
								offerCheck.setNetsuiteOpportunityId(mnIds.getNetsuiteId());
								offerCheck.setNetsuiteIdUpdateDate(new Timestamp(System.currentTimeMillis()));
								offerCheck.setNetsuiteOpportunityURL(Constants.NETSUITE_OPPORTUNITY_URL + mnIds.getNetsuiteId());
								offerCheck.setNetsuiteIdAttemps(3L);
							}
							
							offerCheckPointRepo.save(offerCheck);
						}
					}
				}

				LogService.logger.info("Actualizados campos netsuite_id y URL oportunidad de Netsuite, exitoso.");
				
			} catch (Exception e) {
				LogService.logger.error("Error al intentar actualizar Netsuite Opportunity Id en la tabla oferta_checkpoints, " + e);
			}
		}
		return responseDTO;
	}
	
	
	public String clearCharacters(String dirtyValue){
		
		String clearmax30OfferLevel1 = dirtyValue.replace("$", "");
		String clearmax30OfferLevel2 = clearmax30OfferLevel1.replace(",", "");
		String clearmax30OfferLevel3 = clearmax30OfferLevel2.replace(".", "");
		
		return clearmax30OfferLevel3;
	}
	
	
	@Transactional
	public ResponseDTO postPurchaseOpportunitiesToNetsuite(List<PurchaseOpportunityResponseDTO> purchaseOpportunityDTO) {
		
		//ResponseEntity<String> response = null;  // M. Victoria
		ResponseEntity<RequestNetsuiteDTO> response = null;
		ResponseDTO responseDTO = new ResponseDTO();
		ResponseDTO body = new ResponseDTO();
		boolean result = false;
		
		for(PurchaseOpportunityResponseDTO a :  purchaseOpportunityDTO){
			LogService.logger.info("ID de oferta a enviar a netsuite " + a.getId());
		}
		
		try {
			
			if (purchaseOpportunityDTO.size() > 0){
				
				EndPointCodeResponseEnum inEnumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
				body.setCode(inEnumResult.getValue());
				body.setStatus(inEnumResult.getStatus());
				body.setNetsuiteMethod(Constants.PURCHASE_OPPORTUNITY_METHOD);
				body.setData(purchaseOpportunityDTO);
				
				RestTemplate rt = new RestTemplate();
				String uri = Constants.NETSUITE_INTEGRATION_URI;
				String plainCreds = Constants.NETSUITE_INTEGRATION_HEADER;
				//byte[] plainCredsBytes = plainCreds.getBytes();
				//byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
				//String base64Creds = new String(base64CredsBytes);
				
				HttpHeaders headers = new HttpHeaders();
				headers.add("Authorization", plainCreds);
				headers.add("Content-Type","application/json");
				
				HttpEntity<Object> request = new HttpEntity<Object>(body,headers);
				//response = rt.exchange(uri, HttpMethod.POST, request, String.class);  // M. Victoria
				response = rt.exchange(uri, HttpMethod.POST, request, RequestNetsuiteDTO.class);
				
				if (response.getStatusCode() == HttpStatus.OK){
					
					try{
						EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
						RequestNetsuiteDTO requestNetsuite = new RequestNetsuiteDTO();
						requestNetsuite.setCode(enumResult.getValue());
						requestNetsuite.setStatus(enumResult.getStatus());
						requestNetsuite.setData(response.getBody().getData());
						
						//LA RESPUESTA OBTENIDA SE ENVIA AL SERVICIO PUT PARA ACTUALIZAR SU STATUS EN BD
						ResponseDTO responsePut = PutPurchaseOpportunity(requestNetsuite);
						
						if(responsePut.getCode() == EndPointCodeResponseEnum.C0200.getValue() || responsePut.getCode() == EndPointCodeResponseEnum.C0201.getValue()){
							result = true;
						}
						
					}catch (Exception ez){
						LogService.logger.error("Ocurrio un error al llamar al metodo PutPurchaseOpportunity, " + ez);
						ez.printStackTrace();
					}
					
				}else{
					LogService.logger.info("El servicio de envio de Purchase Opportunity hacia netsuite, fallo. Body: " + response.getBody());
				}
				
			}else{
				LogService.logger.info("No hay datos en PurchaseOpportunityDTO, se descarta el llamado al servicio.");
			}
			
		} catch (Exception ex) {
			LogService.logger.error("Ocurrio un error intentando enviar Purchase Opportunity a Netsuite desde la Capa de Servicicos, " + ex);
			ex.printStackTrace();
		}
		
		
		if (result){
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(purchaseOpportunityDTO);
		}else{
			EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
			responseDTO.setCode(enumResult.getValue());
			responseDTO.setStatus(enumResult.getStatus());
			responseDTO.setData(null);
		}
		
		return responseDTO;
	}
	
	
	
	@Transactional
	public ResponseDTO PutPurchaseOpportunity(RequestNetsuiteDTO requestNetsuite) {
		
		ResponseDTO responseDTO = new ResponseDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();
		
		for (NetsuiteOpportunityDTO netsuiteOppo : requestNetsuite.getData()){
			
			LogService.logger.info("NUEVO: Id Minerva a Consultar: " + netsuiteOppo.getIdMinerva());
		
			Optional<OfferCheckpoint> offerCheckpointBD = offerCheckPointRepo.findById(netsuiteOppo.getIdMinerva());
				
			if (!offerCheckpointBD.isPresent()){
			
				LogService.logger.info("NUEVO: Uno de los datos requerido llega NULL O vacio, se realiza return inmediatamente..");
				MessageDTO messageDataNull = messageRepo.findByCode(MessageEnum.M0075.toString()).getDTO();
				listMessageDTO.add(messageDataNull);
				responseDTO.setListMessage(listMessageDTO);
				EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
				responseDTO.setCode(enumResult.getValue());
				responseDTO.setStatus(enumResult.getStatus());
				responseDTO.setData(new ArrayList<>());

				return responseDTO;
			}
		
			LogService.logger.info("NUEVO: Id Minerva a actualizar: " + netsuiteOppo.getIdMinerva() + " tiene status Netsuite " + netsuiteOppo.getIdMinervaStatus() + " y status BD " + offerCheckpointBD.get().getIdStatus());
			
			if(netsuiteOppo.getIdMinervaStatus().equals(offerCheckpointBD.get().getIdStatus())){	
				LogService.logger.info("NUEVO: La oportunidad ID " + netsuiteOppo.getIdMinerva() + " tiene el mismo status, se marca en 1");
				
				offerCheckpointBD.get().setNetsuiteOpportunityId(requestNetsuite.getData().get(0).getNetsuiteOpportunityId());
				offerCheckpointBD.get().setNetsuiteOpportunityURL(Constants.NETSUITE_OPPORTUNITY_URL+netsuiteOppo.getNetsuiteInternalId());
				offerCheckpointBD.get().setSentNetsuite(1L);
				offerCheckPointRepo.save(offerCheckpointBD.get());
				
			}else{
				LogService.logger.info("NUEVO: La oportunidad ID " + netsuiteOppo.getIdMinerva() + " tiene diferente status, se marca en 2");
				
				offerCheckpointBD.get().setNetsuiteOpportunityId(netsuiteOppo.getNetsuiteOpportunityId());
				offerCheckpointBD.get().setNetsuiteOpportunityURL(Constants.NETSUITE_OPPORTUNITY_URL + netsuiteOppo.getNetsuiteInternalId());
				offerCheckpointBD.get().setSentNetsuite(2L);
				offerCheckPointRepo.save(offerCheckpointBD.get());
			}
		}
		
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setData(requestNetsuite.getData());
		
		return responseDTO;
	}

	
	/**
	 * New Services integrate with Netsuite.
	 * 
	 * @return ResponseDTO responseListDTO
	 */

	@Transactional
	public ResponseDTO getPurchaseOpportunities(){
        ResponseDTO responseListDTO = new ResponseDTO();
		OfferCheckpointDTO offerCheckpointDTO = null;
		
		try {
			LogService.logger.info("NUEVO: Consultando nuevas oportunidades de compra para ser enviados a Netsuite");
			
			List<PurchaseOpportunityResponseDTO> listPurchaseOpportunityDTO = new ArrayList<>();

			List<OfferCheckpoint> listPurchaseOpportunitiesBD     = offerCheckPointRepo.findBySendNetsuite();
			List<OfferCheckpoint> listResendPurchaseOpportunityBD = offerCheckPointRepo.findByResendNetsuite();
			
			if (listPurchaseOpportunitiesBD != null){
				
				LogService.logger.info("NUEVO: Total nuevas oportunidades de compra encontradas: " + listPurchaseOpportunitiesBD.size());
				
				for (OfferCheckpoint pucharseOpportunity : listPurchaseOpportunitiesBD){
					
					if (pucharseOpportunity != null){ 
						offerCheckpointDTO = new OfferCheckpointDTO();
						offerCheckpointDTO = pucharseOpportunity.getDTO();
					
						PurchaseOpportunityResponseDTO purchaseOpportunity = new PurchaseOpportunityResponseDTO();
						
						//LogService.logger.info("NUEVO: Transfiriendo de offerCheckpointDTO a PurchaseOpportunityResponseDTO la oferta ID a enviar: " + offerCheckpointDTO.getId());
						purchaseOpportunity = transferOfferCheckpointToPurchaseOpportunity(offerCheckpointDTO);
						listPurchaseOpportunityDTO.add(purchaseOpportunity);
					}
				}
			}else{
				LogService.logger.info("NUEVO: Total Purchases Opportunities encontradas para enviar a Netsuite: 0");
			}
			
			if ((listResendPurchaseOpportunityBD != null) && (listResendPurchaseOpportunityBD.size() > 0)){
				LogService.logger.info("NUEVO: Total Purchases Opportunities encontradas para reenviar a Netsuite: " + listResendPurchaseOpportunityBD.size());
				
				for (OfferCheckpoint resendPurchaseOpportunity : listResendPurchaseOpportunityBD){
					if (resendPurchaseOpportunity != null){
						offerCheckpointDTO = new OfferCheckpointDTO();
						offerCheckpointDTO = resendPurchaseOpportunity.getDTO();
						
						//LogService.logger.info("NUEVO: Transfiriendo de offerCheckpointDTO a PurchaseOpportunityResponseDTO la oferta ID a reenviar: " + offerCheckpointDTO.getId());
						PurchaseOpportunityResponseDTO pucharseOpportunityDTO = new PurchaseOpportunityResponseDTO();
						pucharseOpportunityDTO = transferOfferCheckpointToPurchaseOpportunity(offerCheckpointDTO);
						listPurchaseOpportunityDTO.add(pucharseOpportunityDTO);
					}
				}
			}else{
				LogService.logger.info("NUEVO: Total Purchases Opportunities encontradas para reenviar a Netsuite: 0");
			}

			Collections.sort(listPurchaseOpportunityDTO, (o1, o2) -> o1.getId() - o2.getId());

	        ResponseDTO responsePostPurchaseToNetsuite = postPurchaseOpportunitiesToNetsuite(listPurchaseOpportunityDTO);
	        
	        LogService.logger.info("NUEVO: Resultado de responsePostPurchaseToNetsuite: " + responsePostPurchaseToNetsuite.getCode());
	        
	        
	        EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	        
	        if (responsePostPurchaseToNetsuite.getCode().equals(enumResult.getValue())){
	        	LogService.logger.info("NUEVO: Se enviaron Purchase Opportunities a Netsuite exitosamente!");
	        	
				responseListDTO.setCode(enumResult.getValue());
				responseListDTO.setStatus(enumResult.getStatus());
				responseListDTO.setData(null);
				
	        }else{
	        	LogService.logger.info("NUEVO: No se enviaron Purchase Opportunities a Netsuite!");
				EndPointCodeResponseEnum badEnumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
				responseListDTO.setCode(badEnumResult.getValue());
				responseListDTO.setStatus(badEnumResult.getStatus());
				responseListDTO.setData(null);
	        }
	        
	        
			LogService.logger.info("NUEVO: Consultando nuevas oportunidades de compra para ser enviados a Netsuite, exitoso.");
		} catch (Exception e) {
			LogService.logger.error("NUEVO: Error al intentar consultar los nuevos Purchase Opportunities ");
			e.printStackTrace();
		}finally {
			LogService.logger.info("NUEVO: Fin de consulta de Purchase Opportunities. ");
		}
		
		return responseListDTO;
	}
	
	
	
	
	
	
	
	
}
