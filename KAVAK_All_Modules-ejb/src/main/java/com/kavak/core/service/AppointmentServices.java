package com.kavak.core.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.kavak.core.dto.AppointmentScheduleBlockDTO;
import com.kavak.core.dto.AppointmentScheduleBlocksDataDTO;
import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.openpay.GenerateChargeDTO;
import com.kavak.core.dto.openpay.ResponseChargeOpenPayDTO;
import com.kavak.core.dto.request.PostAppointmentRequestDTO;
import com.kavak.core.dto.response.GetAppointmentScheduleBlocksResponseDTO;
import com.kavak.core.dto.response.PostAppointmentResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.CatalogueCarStatusEnum;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.MessageEnum;
import com.kavak.core.model.AppointmentLog;
import com.kavak.core.model.AppointmentSchedule;
import com.kavak.core.model.AppointmentScheduleBlock;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.User;
import com.kavak.core.model.UserTaskAssignmentWeight;
import com.kavak.core.repositories.AppointmentLogRepository;
import com.kavak.core.repositories.AppointmentScheduleBlockRepository;
import com.kavak.core.repositories.AppointmentScheduleRepository;
import com.kavak.core.repositories.InspectionLocationRepository;
import com.kavak.core.repositories.MessageRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.repositories.UserTaskAssignmentWeightRepository;
import com.kavak.core.service.openpay.OpenPayTransactionServices;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;

/**
 * <h1>Data de las Citas de Inspecciones</h1>
 *
 *
 * @author Enrique Marin
 * @since 2017-06-29
 */

@Stateless
public class AppointmentServices {

    private List<MessageDTO> listMessages;

    @Inject
    private AppointmentScheduleBlockRepository appointmentScheduleBlockRepo;

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;

    @Inject
    private AppointmentScheduleRepository appointmentScheduleRepo;

    @Inject
    private InspectionLocationRepository inspectionLocationRepo;

    @Inject
    private SaleCheckpointRepository saleCheckpointRepo;

    @Inject
    private UserRepository userRepo;

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private UserTaskAssignmentWeightRepository userTaskAssignmentWeightRepo;

    @Inject
    private OpenPayTransactionServices openPayTransactionServices;

    @Inject
    private MessageRepository messageRepo;

    @Inject
    private AppointmentLogRepository appointmentLogRepo;

    /**
     * Consulta los horarios de citas disponibles para los proximos MAX_DAYS_SCHEDULE_BLOCK(6) dias
     * 
     * @author Enrique Marin
     * @param dateFrom
     *            fecha a partir se buscan los appointment
     * @return
     */
    public ResponseDTO getAppointmentScheduleblocks(String dateFrom, boolean isAppointmentWithBooking) {
	ResponseDTO responseListDTO = new ResponseDTO();
	GetAppointmentScheduleBlocksResponseDTO getAppointmentScheduleBlocksResponseDTO = new GetAppointmentScheduleBlocksResponseDTO();
	HashMap<String, List<AppointmentScheduleBlockDTO>> mapAppointmentScheduleBlockBase = new HashMap<>();
	List<AppointmentScheduleBlockDTO> listAppointmentScheduleBlockDTO;
	List<AppointmentScheduleBlocksDataDTO> listAppointmentScheduleBlocksDataDTO = new ArrayList<>();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat parseFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
	String keyMapParsed = null;
	boolean dayAvailable = true;

	List<Long> listHourBlockedSaturday = new ArrayList<>(Arrays.asList(1L, 2L, 11L));
	List<Long> listHourBlockedSunday = new ArrayList<>(Arrays.asList(1L, 2L, 11L));
	List<Long> listHourBlockedDate = new ArrayList<>(Arrays.asList(1L, 2L, 3L, 10L, 11L));

	try {
	    List<AppointmentScheduleBlock> listAppointmentScheduleBlocksBD = appointmentScheduleBlockRepo.findAllByActiveTrueOrderById(); 
	    Calendar calendarDateFrom = Calendar.getInstance();
	    Calendar calendarActualTime = Calendar.getInstance();
	    calendarDateFrom.setTime(sdf.parse(dateFrom));

	    // Para manejar dias bloqueados por regla de negocio
	    Calendar calendarDateBlocked = Calendar.getInstance();
	    calendarDateBlocked.setTime(sdf.parse("2018-09-16"));

	    // Se excluye el dia actual si son más de las 5 de la tarde y/o es una visita con reserva
	    if (calendarActualTime.get(Calendar.HOUR_OF_DAY) >= 17 || isAppointmentWithBooking) {
		calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
		dayAvailable = false;
	    }
	    // Armando (MAPA A) MapaBase que contiene los MAX_DAYS_SCHEDULE_BLOCK (6) dias siguientes a validar
	    for (int indexDays = 1; indexDays <= Constants.MAX_DAYS_SCHEDULE_BLOCK; indexDays++) {
		listAppointmentScheduleBlockDTO = new ArrayList<>();
		for (AppointmentScheduleBlock appointmentScheduleBlockActual : listAppointmentScheduleBlocksBD) {
		    Date dateKey = parseFormat.parse(calendarDateFrom.getTime().toString());
		    keyMapParsed = sdf.format(dateKey);
		    AppointmentScheduleBlockDTO newAppointmentScheduleBlockDTO = new AppointmentScheduleBlockDTO();
		    newAppointmentScheduleBlockDTO.setAvailable(true);
		    if (dayAvailable) {
			if (keyMapParsed.equalsIgnoreCase(dateFrom)) {
			    // Se obtiene el 1 carater del bloque "8am a 9am" para saber la hora inicio (8)
			    Long horaInicioBlock = Long.valueOf(appointmentScheduleBlockActual.getBlock().substring(0, 1));
			    // Dado que se trabaja con hora no militar (Base 24) se calcula que la hora inicio este entre (1 - 7) y se le suma 12 para que sea mas facil validar cuales se debes bloquear
			    if (horaInicioBlock < 8) {
				horaInicioBlock = horaInicioBlock + 12;
			    }
			    if (calendarActualTime.get(Calendar.HOUR_OF_DAY) + 1 >= (horaInicioBlock)) {
				newAppointmentScheduleBlockDTO.setAvailable(false);
			    }
			}
		    }

		    if (calendarDateFrom.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			if (listHourBlockedSaturday.contains(appointmentScheduleBlockActual.getId())) {
			    newAppointmentScheduleBlockDTO.setAvailable(false);
			}
		    }

		    if (calendarDateFrom.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			if (listHourBlockedSunday.contains(appointmentScheduleBlockActual.getId())) {
			    newAppointmentScheduleBlockDTO.setAvailable(false);
			}
		    }

		    // IssueID #1826 Bloquear horario de 8-9am de Visitas (BlockID = 1)
		    if (appointmentScheduleBlockActual.getId() == 1L) {
			newAppointmentScheduleBlockDTO.setAvailable(false);
		    }
		    // ================================================================ >>

		    // IssueID #2657 Bloquear horario de 6-7pm de Visitas (BlockID = 11)
		    if (appointmentScheduleBlockActual.getId() == 11L) {
			newAppointmentScheduleBlockDTO.setAvailable(false);
		    }
		    
		    // ================================================================ >>

		    // Horario de 11am-4pm de Visitas 16/09/2018
		    if (DateUtils.isSameDay(calendarDateFrom, calendarDateBlocked)) {
			if (listHourBlockedDate.contains(appointmentScheduleBlockActual.getId())) {
			    newAppointmentScheduleBlockDTO.setAvailable(false);
			}
		    }
		    // ================================================================ >>
		    
		    newAppointmentScheduleBlockDTO.setId(appointmentScheduleBlockActual.getId());
		    newAppointmentScheduleBlockDTO.setBlock(appointmentScheduleBlockActual.getBlock());
		    listAppointmentScheduleBlockDTO.add(newAppointmentScheduleBlockDTO);
		}
		mapAppointmentScheduleBlockBase.put(keyMapParsed, listAppointmentScheduleBlockDTO);
		calendarDateFrom.add(Calendar.DAY_OF_MONTH, 1);
		AppointmentScheduleBlocksDataDTO newAppointmentScheduleBlocksDataDTO = new AppointmentScheduleBlocksDataDTO();
		newAppointmentScheduleBlocksDataDTO.setDate(keyMapParsed);
		newAppointmentScheduleBlocksDataDTO.setListAppointmentScheduleBlockDTO(listAppointmentScheduleBlockDTO);
		listAppointmentScheduleBlocksDataDTO.add(newAppointmentScheduleBlocksDataDTO);
	    }

	} catch (ParseException e) {
	    e.printStackTrace();
	}

	getAppointmentScheduleBlocksResponseDTO.setListAppointmentScheduleBlocksDataDTO(listAppointmentScheduleBlocksDataDTO);
	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	responseListDTO.setCode(enumResult.getValue());
	responseListDTO.setStatus(enumResult.getStatus());
	responseListDTO.setData(getAppointmentScheduleBlocksResponseDTO);
	return responseListDTO;
    }

    /**
     * Registra una cita dependiento del status de carro
     * 
     * @author Enrique Marin
     * @param postAppointmentRequestDTO
     * @return PostAppointmentResponseDTO en caso que el idSellCarDetail
     * @throws MessagingException
     */
    public ResponseDTO postAppointment(PostAppointmentRequestDTO postAppointmentRequestDTO) throws MessagingException {
	Long starTime = Calendar.getInstance().getTimeInMillis();
	LogService.logger.info(Constants.LOG_EXECUTING_START + " [postAppointment] ");
	ResponseDTO responseDTO = new ResponseDTO();
	Long saleCheckpointId = 0L;
	String source;
	listMessages = new ArrayList<MessageDTO>();

	// llenando tabla v2_appointment_log
	AppointmentLog newAppointmentLog = new AppointmentLog();
	if (postAppointmentRequestDTO.getIdUser() != null) {
	    newAppointmentLog.setUserId(postAppointmentRequestDTO.getIdUser());
	}
	if (postAppointmentRequestDTO.getCarId() != null) {
	    newAppointmentLog.setCarId(postAppointmentRequestDTO.getCarId());
	}
	if (postAppointmentRequestDTO.getIdLocation() != null) {
	    newAppointmentLog.setLocationId(postAppointmentRequestDTO.getIdLocation());
	}
	if (postAppointmentRequestDTO.getAppointmentDate() != null) {
	    newAppointmentLog.setAppointmentDate(postAppointmentRequestDTO.getAppointmentDate());
	}
	if (postAppointmentRequestDTO.getIdAppointmentHourBlock() != null) {
	    newAppointmentLog.setAppointmentHourBlockId(postAppointmentRequestDTO.getIdAppointmentHourBlock());
	}
	if (postAppointmentRequestDTO.getDeviceSessionId() != null) {
	    newAppointmentLog.setDeviceSessionId(postAppointmentRequestDTO.getDeviceSessionId());
	}
	if (postAppointmentRequestDTO.getTokenId() != null) {
	    newAppointmentLog.setTokenId(postAppointmentRequestDTO.getTokenId());
	}
	if (postAppointmentRequestDTO.getPaymentTypeId() != null) {
	    newAppointmentLog.setPaymenType(postAppointmentRequestDTO.getPaymentTypeId().toString());
	}
	if (postAppointmentRequestDTO.getCarPrice() != null) {
	    newAppointmentLog.setCarPrice(postAppointmentRequestDTO.getCarPrice().toString());
	}
	if (postAppointmentRequestDTO.getSource() != null) {
	    newAppointmentLog.setSource(postAppointmentRequestDTO.getSource());
	}
	appointmentLogRepo.save(newAppointmentLog);
	/////////////////////////////////////////////////////////////////////////////////////////////

	// Id Usuario Null o CarId Null
	if (postAppointmentRequestDTO.getIdUser() == null || postAppointmentRequestDTO.getCarId() == null) {
	    MessageDTO message = messageRepo.findByCode(MessageEnum.M0008.toString()).getDTO();
	    listMessages.add(message);
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0404.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    responseDTO.setListMessage(listMessages);
	    responseDTO.setData(new ArrayList<>());
	    return responseDTO;
	}

	Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(postAppointmentRequestDTO.getCarId());

	if (!sellCarDetailBD.isPresent()) {
	    MessageDTO messageNotFound = messageRepo.findByCode(MessageEnum.M0009.toString()).getDTO();
	    return KavakUtils.getResponseNoDataFound(messageNotFound);
	}

	if (StringUtils.isEmpty(postAppointmentRequestDTO.getSource())) {
	    source = Constants.SOURCE_IOS_APP;
	} else {
	    source = postAppointmentRequestDTO.getSource();
	}
	
	SaleCheckpoint newSaleCheckpoint = new SaleCheckpoint();
	newSaleCheckpoint.setPaymentPlatform("Openpay");
	Timestamp actualTime = new Timestamp(System.currentTimeMillis());
	newSaleCheckpoint.setCarId(postAppointmentRequestDTO.getCarId());
	
	if(postAppointmentRequestDTO.getInternalUserId() != null) {
	    newSaleCheckpoint.setInternalUserId(postAppointmentRequestDTO.getInternalUserId());
	}	
	
	Optional<User> userBD = userRepo.findById(postAppointmentRequestDTO.getIdUser());

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	Timestamp appointmentDate = new Timestamp(System.currentTimeMillis());

	try {
	    Date dateParseAppointment = sdf.parse(postAppointmentRequestDTO.getAppointmentDate());
	    appointmentDate.setTime(dateParseAppointment.getTime());
	} catch (ParseException e) {
	    e.printStackTrace();
	}

	if ((KavakUtils.getStatusCar(sellCarDetailBD.get()) == CatalogueCarStatusEnum.BOOKED) || (KavakUtils.getStatusCar(sellCarDetailBD.get()) == CatalogueCarStatusEnum.SOLD)) {
	    PostAppointmentResponseDTO postAppointmentResponseDTO = new PostAppointmentResponseDTO();
	    EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
	    responseDTO.setCode(enumResult.getValue());
	    responseDTO.setStatus(enumResult.getStatus());
	    postAppointmentResponseDTO.setErrorTitle("¡Auto Reservado o Vendido!");
	    postAppointmentResponseDTO.setErrorDescription("Lo sentimos, este auto ha sido vendido o se encuentra reservado. Te invitamos a seguir en www.kavak.com o te ayudamos a buscar el auto que quieres en el siguiente botón.");
	    postAppointmentResponseDTO.setButonTitle("VER CATÁLOGO");
	    postAppointmentResponseDTO.setButtonUrl("www.kavak.com/compra-de-autos");
	    responseDTO.setData(postAppointmentResponseDTO);
	    return responseDTO;
	}

	// Se esta validando que tipo de confirmacion es
	if (postAppointmentRequestDTO.getTokenId() == null) { // CASO CITA SIN RESERVA

	    LogService.logger.info(Constants.LOG_EXECUTING_START + " [postAppointment] - generando Cita sin Reserva");
	    AppointmentSchedule newAppointmentSchedule = new AppointmentSchedule();

	    newAppointmentSchedule.setIdHourBlock(postAppointmentRequestDTO.getIdAppointmentHourBlock());
	    newAppointmentSchedule.setAppoinmentDate(appointmentDate);
	    newAppointmentSchedule.setCarStatus(1);
	    newAppointmentSchedule.setSellCarDetailId(postAppointmentRequestDTO.getCarId());
	    newAppointmentSchedule.setIdUser(postAppointmentRequestDTO.getIdUser());
	    if (postAppointmentRequestDTO.getIdLocation() != null) {
		    newAppointmentSchedule.setAppointmentLocationId(postAppointmentRequestDTO.getIdLocation());
		newAppointmentSchedule.setAppointmentAddress(inspectionLocationRepo.findById(postAppointmentRequestDTO.getIdLocation()).get().getDTO().getFullAddress());
	    } else {
		    newAppointmentSchedule.setAppointmentLocationId(Constants.INSPECTION_LOCATION_LERMA);
		newAppointmentSchedule.setAppointmentAddress(inspectionLocationRepo.findById(Constants.INSPECTION_LOCATION_LERMA).get().getDTO().getFullAddress());
	    }
	    Optional<AppointmentScheduleBlock> appointmentScheduleBlockBD = appointmentScheduleBlockRepo.findById(postAppointmentRequestDTO.getIdAppointmentHourBlock());
	    newAppointmentSchedule.setStartDate(appointmentScheduleBlockBD.get().getHourFrom());
	    newAppointmentSchedule.setEndDate(appointmentScheduleBlockBD.get().getHourTo());
	    AppointmentSchedule newAppointmentScheduleDB = appointmentScheduleRepo.save(newAppointmentSchedule);

	    LogService.logger.info(Constants.LOG_EXECUTING_START + " [postAppointment] - AppointmentSchedule guardado con Exito! ");

	    newSaleCheckpoint.setUser(userBD.get());
	    newSaleCheckpoint.setCustomerName(userBD.get().getDTO().getFirstName());
	    if (userBD.get().getDTO().getLastName() != null) {
		newSaleCheckpoint.setCustomerLastName(userBD.get().getDTO().getLastName());
	    }
	    newSaleCheckpoint.setEmail(userBD.get().getEmail());
	    newSaleCheckpoint.setCarId(sellCarDetailBD.get().getId());
	    newSaleCheckpoint.setSku(sellCarDetailBD.get().getSku());
	    MetaValue metaValueBDTOPCA = metaValueRepo.findByAlias("TOPCA");
	    newSaleCheckpoint.setStatus(metaValueBDTOPCA.getId());
	    newSaleCheckpoint.setCarYear(Long.valueOf(sellCarDetailBD.get().getCarYear()));
	    newSaleCheckpoint.setCarMake(sellCarDetailBD.get().getCarMake());
	    newSaleCheckpoint.setCarModel(sellCarDetailBD.get().getCarModel());
	    newSaleCheckpoint.setCarVersion(sellCarDetailBD.get().getCarTrim().split("##")[0]);
	    newSaleCheckpoint.setCarKm(Long.valueOf(sellCarDetailBD.get().getCarKm()));
	    newSaleCheckpoint.setSaleOpportunityTypeId(200L);
	    newSaleCheckpoint.setScheduleDateId(newAppointmentScheduleDB.getId());
	    newSaleCheckpoint.setEmail(userBD.get().getEmail());
	    newSaleCheckpoint.setSentNetsuite(0L);
	    UserTaskAssignmentWeight saleBDQueryEM = userTaskAssignmentWeightRepo.findEMSaleCheckpointByQueryOfWeight();
	    newSaleCheckpoint.setAssignedEm(saleBDQueryEM.getId());
	    newSaleCheckpoint.setRegisterDate(new Timestamp(System.currentTimeMillis()));
	    newSaleCheckpoint.setSource(source);
	    
	    //Disable communications
	    if(postAppointmentRequestDTO.isDisableCommunications() != false) {
		newSaleCheckpoint.setSendEmail(true);
		newSaleCheckpoint.setAppointmentEmailSent(true);
		newSaleCheckpoint.setBookedCarSmsSent(true);
		newSaleCheckpoint.setScheduledAppointmentSmsSent(true);
		newSaleCheckpoint.setBookingTimeExpiredNotificationSent(true);
		newSaleCheckpoint.setBookingTimeReminderNotificationSent(true);
		newSaleCheckpoint.setBookedRegisteredNotificationSent(true);
		newSaleCheckpoint.setAppointmentBookedNotificationSent(true); 
		newSaleCheckpoint.setAppointmentRegisteredNotificationSent(true);
		newSaleCheckpoint.setBookedEmailSent(true);
		newSaleCheckpoint.setFinancingEmailSent(true);    	    
	    }
	    //
	    
	    SaleCheckpoint saleCheckpointReturnSaveBD = saleCheckpointRepo.save(newSaleCheckpoint);
	    saleCheckpointId = saleCheckpointReturnSaveBD.getId();
	    saleCheckpointReturnSaveBD.setDuplicatedOfferControl(saleCheckpointReturnSaveBD.getCarId());

	    List<SaleCheckpoint> listSaleCheckpointDuplicate = saleCheckpointRepo.findByUserIdAndCarIdOrderByIdAsc(postAppointmentRequestDTO.getIdUser(), postAppointmentRequestDTO.getCarId());
	    // Se agregar el campo control duplicado para que Netsuite lleve control de que las ofertas ya estan repetidas
	    if (listSaleCheckpointDuplicate.size() == 1) {
		saleCheckpointReturnSaveBD.setDuplicatedOfferControl(saleCheckpointReturnSaveBD.getId());
	    } else {
		SaleCheckpoint saleCheckpointDuplicate = listSaleCheckpointDuplicate.get(0); // Se busca solo el primer registro por que ya viene ordenada
		saleCheckpointReturnSaveBD.setDuplicatedOfferControl(saleCheckpointDuplicate.getId());
	    }
	    saleCheckpointRepo.save(saleCheckpointReturnSaveBD);
	    LogService.logger.info(Constants.LOG_EXECUTING_START + " [postAppointment] - SaleCheckpoint guardado con Exito! ");

	} else { // CASO CITA CON RESERVA

	    LogService.logger.info(Constants.LOG_EXECUTING_START + " [postAppointment] - generando Cita con Reserva");
	    GenerateChargeDTO generateChargeDTO = new GenerateChargeDTO();
	    generateChargeDTO.setUserId(postAppointmentRequestDTO.getIdUser());
	    generateChargeDTO.setCarId(postAppointmentRequestDTO.getCarId());
	    generateChargeDTO.setBillingAddressDTO(postAppointmentRequestDTO.getBillingAddressDTO());
	    generateChargeDTO.setTotalPrice(postAppointmentRequestDTO.getCarPrice().toString());
	    generateChargeDTO.setTokenId(postAppointmentRequestDTO.getTokenId());
	    generateChargeDTO.setDeviceSessionId(postAppointmentRequestDTO.getDeviceSessionId().toString());
	    generateChargeDTO.setPaymentTypeId(144L);
	    generateChargeDTO.setCarPrice(postAppointmentRequestDTO.getCarPrice());
	    generateChargeDTO.setHasTradeIn(false);
	    generateChargeDTO.setEmail(userBD.get().getEmail());
	    newSaleCheckpoint.setTypePaymentMethodTypeId("144"); // Reserva 72 horas
	    MetaValue metaValueTopCA = metaValueRepo.findByAlias("TOPCA");
	    newSaleCheckpoint.setStatus(metaValueTopCA.getId());
	    newSaleCheckpoint.setCarCost(postAppointmentRequestDTO.getCarPrice().toString());
	    MetaValue metaValueBookingPrice = metaValueRepo.findByAlias(Constants.BOOKING_PRICE);
	    Long aux = (postAppointmentRequestDTO.getCarPrice() - Long.valueOf(metaValueBookingPrice.getOptionName()));
	    newSaleCheckpoint.setTotalAmount(aux.toString());
	    newSaleCheckpoint.setCarReserve(metaValueBookingPrice.getOptionName());
	    newSaleCheckpoint.setCarYear(Long.valueOf(sellCarDetailBD.get().getCarYear()));
	    newSaleCheckpoint.setCarMake(sellCarDetailBD.get().getCarMake());
	    newSaleCheckpoint.setCarModel(sellCarDetailBD.get().getCarModel());
	    newSaleCheckpoint.setCarVersion(sellCarDetailBD.get().getCarTrim().split("##")[0]);
	    newSaleCheckpoint.setCarKm(Long.valueOf(sellCarDetailBD.get().getCarKm()));
	    newSaleCheckpoint.setCarId(sellCarDetailBD.get().getId());
	    newSaleCheckpoint.setSku(sellCarDetailBD.get().getSku());
	    newSaleCheckpoint.setRegisterDate(actualTime);
	    newSaleCheckpoint.setEmail(userBD.get().getEmail());
	    newSaleCheckpoint.setSentNetsuite(0L);
	    newSaleCheckpoint.setUser(userBD.get());
	    newSaleCheckpoint.setSource(source);
	    newSaleCheckpoint.setCustomerName(userBD.get().getDTO().getFirstName());
	    if (userBD.get().getDTO().getLastName() != null) {
		newSaleCheckpoint.setCustomerLastName(userBD.get().getDTO().getLastName());
	    }
	    SaleCheckpoint saleCheckpointBD = saleCheckpointRepo.save(newSaleCheckpoint);
	    saleCheckpointId = saleCheckpointBD.getId();

	    ResponseChargeOpenPayDTO responseChargeOpenPayDTO = openPayTransactionServices.generateChargesOpenPay(generateChargeDTO, userBD.get().getDTO(), saleCheckpointBD.getDTO(), sellCarDetailBD.get().getDTO(), postAppointmentRequestDTO.getSource());

	    if (EndPointCodeResponseEnum.C0200.getValue().equals(responseChargeOpenPayDTO.getResponseDTO().getCode())) {

		AppointmentSchedule newAppointmentSchedule = new AppointmentSchedule();
		newAppointmentSchedule.setIdHourBlock(postAppointmentRequestDTO.getIdAppointmentHourBlock());
		newAppointmentSchedule.setAppoinmentDate(appointmentDate);
		newAppointmentSchedule.setCarStatus(1);
		newAppointmentSchedule.setSellCarDetailId(postAppointmentRequestDTO.getCarId());
		newAppointmentSchedule.setIdUser(postAppointmentRequestDTO.getIdUser());
		if (postAppointmentRequestDTO.getIdLocation() != null) {
		    newAppointmentSchedule.setAppointmentLocationId(postAppointmentRequestDTO.getIdLocation());
		    newAppointmentSchedule.setAppointmentAddress(inspectionLocationRepo.findById(postAppointmentRequestDTO.getIdLocation()).get().getDTO().getFullAddress());
		} else {
		    newAppointmentSchedule.setAppointmentLocationId(Constants.INSPECTION_LOCATION_LERMA);
		    newAppointmentSchedule.setAppointmentAddress(inspectionLocationRepo.findById(Constants.INSPECTION_LOCATION_LERMA).get().getDTO().getFullAddress());
		}
		Optional<AppointmentScheduleBlock> appointmentScheduleBlockBD = appointmentScheduleBlockRepo.findById(postAppointmentRequestDTO.getIdAppointmentHourBlock());
		newAppointmentSchedule.setStartDate(appointmentScheduleBlockBD.get().getHourFrom());
		newAppointmentSchedule.setEndDate(appointmentScheduleBlockBD.get().getHourTo());
		AppointmentSchedule newAppointmentScheduleDB = appointmentScheduleRepo.save(newAppointmentSchedule);

		saleCheckpointBD.setSentNetsuite(0L);
		saleCheckpointBD.setSaleOpportunityTypeId(275L);
		saleCheckpointBD.setCheckoutResult("Pago de reserva con éxito");
		saleCheckpointBD.setPaymentStatus(1L);
		saleCheckpointBD.setScheduleDateId(newAppointmentScheduleDB.getId());
		saleCheckpointBD.setPaymentPlatformId(responseChargeOpenPayDTO.getCardCharge().getId());
		saleCheckpointBD.setTransactionNumber(responseChargeOpenPayDTO.getCardCharge().getOrderId());
		MetaValue metaValueCPRP = metaValueRepo.findByAlias("CPRP");
		saleCheckpointBD.setStatus(metaValueCPRP.getId());
		SaleCheckpoint saleCheckpointDuplicated = saleCheckpointRepo.save(saleCheckpointBD);

		List<SaleCheckpoint> listSaleCheckpointDuplicate = saleCheckpointRepo.findByUserIdAndCarIdOrderByIdAsc(postAppointmentRequestDTO.getIdUser(), postAppointmentRequestDTO.getCarId());
		// Se agregar el campo control duplicado para que Netsuite lleve control de que las ofertas ya estan repetidas
		if (listSaleCheckpointDuplicate.size() == 1) {
		    saleCheckpointBD.setDuplicatedOfferControl(saleCheckpointDuplicated.getId());
		} else {
		    SaleCheckpoint saleCheckpointDuplicate = listSaleCheckpointDuplicate.get(0); // Se busca solo el primer registro por que ya viene ordenada
		    saleCheckpointBD.setDuplicatedOfferControl(saleCheckpointDuplicate.getId());
		}

		String[] customerNameComplete = userBD.get().getName().split(" ");
		List<String> listCustomerNameComplete = Arrays.asList(customerNameComplete);
		saleCheckpointBD.setCustomerName(listCustomerNameComplete.get(0).toString());

		if (listCustomerNameComplete.size() > 1) {
		    saleCheckpointBD.setCustomerLastName(listCustomerNameComplete.get(1).toString());
		}

		saleCheckpointRepo.save(saleCheckpointBD);

	    } else {
		MessageDTO messageDTO = responseChargeOpenPayDTO.getResponseDTO().getListMessage().get(0);
		newSaleCheckpoint.setCheckoutResult("Error realizando el pago de reserva: " + messageDTO.getUserMessage() + " (" + messageDTO.getErrorCode() + ")");
		newSaleCheckpoint.setUser(userBD.get());
		newSaleCheckpoint.setCustomerName(userBD.get().getDTO().getName());
		newSaleCheckpoint.setCustomerLastName(userBD.get().getDTO().getLastName());
		newSaleCheckpoint.setEmail(userBD.get().getEmail());
		newSaleCheckpoint.setCarId(postAppointmentRequestDTO.getCarId());
		newSaleCheckpoint.setSku(sellCarDetailBD.get().getSku());
		newSaleCheckpoint.setCarYear(Long.valueOf(sellCarDetailBD.get().getCarYear()));
		newSaleCheckpoint.setCarMake(sellCarDetailBD.get().getCarMake());
		newSaleCheckpoint.setCarModel(sellCarDetailBD.get().getCarModel());
		newSaleCheckpoint.setCarVersion(sellCarDetailBD.get().getCarTrim().split("##")[0]);
		newSaleCheckpoint.setCarKm(Long.valueOf(sellCarDetailBD.get().getCarKm()));
		newSaleCheckpoint.setSaleOpportunityTypeId(275L);
		MetaValue metaValueCPRP = metaValueRepo.findByAlias("CPRP");
		saleCheckpointBD.setStatus(metaValueCPRP.getId());
		newSaleCheckpoint.setRegisterDate(actualTime);
		newSaleCheckpoint.setSentNetsuite(0L);
		newSaleCheckpoint.setSource(source);
		SaleCheckpoint saleCheckpointDuplicated = saleCheckpointRepo.save(newSaleCheckpoint);
		saleCheckpointId = saleCheckpointDuplicated.getId();

		List<SaleCheckpoint> listSaleCheckpointDuplicate = saleCheckpointRepo.findByUserIdAndCarIdOrderByIdAsc(postAppointmentRequestDTO.getIdUser(), postAppointmentRequestDTO.getCarId());
		// Se agregar el campo control duplicado para que Netsuite lleve control de que las ofertas ya estan repetidas
		if (listSaleCheckpointDuplicate.size() == 1) {
		    saleCheckpointDuplicated.setDuplicatedOfferControl(saleCheckpointDuplicated.getId());
		} else {
		    SaleCheckpoint saleCheckpointDuplicate = listSaleCheckpointDuplicate.get(0); // Se busca solo el primer registro por que ya viene ordenada
		    saleCheckpointDuplicated.setDuplicatedOfferControl(saleCheckpointDuplicate.getId());
		}
		saleCheckpointRepo.save(saleCheckpointDuplicated);
		return responseChargeOpenPayDTO.getResponseDTO();
	    }
	}

	EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0201.toString());
	responseDTO.setCode(enumResult.getValue());
	responseDTO.setStatus(enumResult.getStatus());
	GenericDTO responseDataDTO = new GenericDTO();
	responseDataDTO.setId(saleCheckpointId);
	responseDTO.setData(responseDataDTO);
	LogService.logger.info(Constants.LOG_EXECUTING_END + " [postAppointment] " + (Calendar.getInstance().getTimeInMillis() - starTime));
	return responseDTO;
    }

}