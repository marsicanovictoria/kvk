package com.kavak.core.service.openpay;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;

import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.SaleCheckpointDTO;
import com.kavak.core.dto.SellCarDetailDTO;
import com.kavak.core.dto.UserDTO;
import com.kavak.core.dto.openpay.GenerateChargeDTO;
import com.kavak.core.dto.openpay.ResponseChargeOpenPayDTO;
import com.kavak.core.dto.response.OrderResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.enumeration.OpenPayErrorCodeEnum;
import com.kavak.core.model.CustomerNotifications;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.SaleCarOrder;
import com.kavak.core.model.SaleCarOrderTransaction;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.SellCarBuyer;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.SellCarOrder;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.CustomerNotificationsRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.SaleCarOrderRepository;
import com.kavak.core.repositories.SaleCarOrderTransactionRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.SellCarBuyerRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.SellCarOrderRepository;
import com.kavak.core.repositories.UserMetaRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.service.CommonServices;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;

import mx.openpay.client.Address;
import mx.openpay.client.Card;
import mx.openpay.client.Charge;
import mx.openpay.client.Customer;
import mx.openpay.client.core.OpenpayAPI;
import mx.openpay.client.core.requests.transactions.CreateBankChargeParams;
import mx.openpay.client.core.requests.transactions.CreateCardChargeParams;
import mx.openpay.client.enums.Currency;
import mx.openpay.client.exceptions.OpenpayServiceException;
import mx.openpay.client.exceptions.ServiceUnavailableException;

//import com.kavak.core.openpay.model.Verification;

@Stateless
public class OpenPayTransactionServices {

    @Resource
    private EJBContext context;
    
    @EJB(mappedName = LookUpNames.SESSION_CommonServices)
    private CommonServices commonServices;

    @Inject
    private MetaValueRepository metaValueRepo;

    @Inject
    private SaleCheckpointRepository saleCheckpointRepo;

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;

    @Inject
    private SellCarBuyerRepository sellCarBuyerRepo;


    @Inject
    private SaleCarOrderTransactionRepository saleCarOrderTransactionRepo;

    @Inject
    private SellCarOrderRepository sellCarOrderRepo;

    @Inject
    private SaleCarOrderRepository saleCarOrderRepo;

    @Inject
    private UserRepository userRepo;

    @Inject
    private UserMetaRepository userMetaRepo;
    
    @Inject
    private CustomerNotificationsRepository customerNotificationsRepo;
    
    

//    public void doVerification(VerificationDTO verificationDTO) {
//		LogService.logger.info("Inicio doVerification " );
//		Verification verification = new Verification();
//		verification.setType(verificationDTO.getType());
//		verification.setEventDate(verificationDTO.getVerificationCode());
//		verification.setVerificationCode(verificationDTO.getVerificationCode());
//		Verification verificationBD = verificationRepository.findByVerificationCode(verification.getVerificationCode());
//		if(verificationBD == null){
//			verificationRepository.save(verification);
//		}
//		LogService.logger.info("Fin doVerification " );
//	}

    /**
     *
     * @author Enrique Marin
     * @param generateChargeDTO
     * @return ResponseChargeOpenPayDTO
     * @throws MessagingException 
     */

    //@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public ResponseChargeOpenPayDTO generateChargesOpenPay (GenerateChargeDTO generateChargeDTO,UserDTO userDTO,SaleCheckpointDTO saleCheckPointDTO,SellCarDetailDTO sellCarDetailDTO, String source) throws MessagingException{
        Long starTime = Calendar.getInstance().getTimeInMillis();
        LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - generando cargo para user [" + generateChargeDTO.getUserId() +"] para el auto ["+ generateChargeDTO.getCarId() + "]"  );
        ResponseChargeOpenPayDTO responseChargeOpenPayDTO = new ResponseChargeOpenPayDTO();
        ResponseDTO responseDTO = new ResponseDTO();

        OpenpayAPI openpayAPI = new OpenpayAPI(Constants.OPENPAY_LOCATION, Constants.OPENPAY_APIKEY, Constants.OPENPAY_MERCHANTID);
        Customer rqCustomerOpenPay = new Customer();
        Customer customerCreateResponse =  null; 
        String openpayCustomerId = null;

        Optional<User> user = userRepo.findById(generateChargeDTO.getUserId());
        UserMeta userMetaBD = userMetaRepo.findUserMetaByMetaKeyAndId("op_customer_id", generateChargeDTO.getUserId());
        Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(generateChargeDTO.getCarId());

        if(userMetaBD != null){
            openpayCustomerId = userMetaBD.getMetaValue();
        }else{
            // Registro Customer en OpenPay
            try {
                rqCustomerOpenPay.name(user.get().getDTO().getName());
                rqCustomerOpenPay.lastName(user.get().getDTO().getLastName());
                rqCustomerOpenPay.email(user.get().getDTO().getEmail());
                rqCustomerOpenPay.phoneNumber(user.get().getDTO().getPhone());
                rqCustomerOpenPay.requiresAccount(false);
                Address address = new Address();
                address.city("Ciudad de México");
                address.countryCode("MX");
                address.state("Distrito Federal");
                address.postalCode(generateChargeDTO.getBillingAddressDTO().getZipCode());
                address.line1(generateChargeDTO.getBillingAddressDTO().getStreet());
                address.line2(generateChargeDTO.getBillingAddressDTO().getExteriorNumber() + "-" + generateChargeDTO.getBillingAddressDTO().getInteriorNumber());
                address.line3("");
                rqCustomerOpenPay.address(address);
                customerCreateResponse = openpayAPI.customers().create(rqCustomerOpenPay);

                if(rqCustomerOpenPay != null){
                    UserMeta newUserMeta = new UserMeta();
                    newUserMeta.setMetaKey("op_customer_id");
                    newUserMeta.setUser(user.get());
                    newUserMeta.setMetaValue(customerCreateResponse.getId());
                    userMetaRepo.save(newUserMeta);
                    responseChargeOpenPayDTO.setCustomerCreate(customerCreateResponse);
                }

                LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - customer OpenpayCreado con Exito! " );
            } catch (OpenpayServiceException e) {
                EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
                responseDTO.setCode(enumResultException.getValue());
                responseDTO.setStatus(enumResultException.getStatus());
                List<MessageDTO> listMessageDTO = new ArrayList<>();
                MessageDTO newMessageDTO = new MessageDTO();

                OpenPayErrorCodeEnum openPayResult = OpenPayErrorCodeEnum.getByCode(e.getErrorCode());
                newMessageDTO.setErrorCode(openPayResult.getCode());
                newMessageDTO.setApiMessage(e.getDescription());
                newMessageDTO.setUserMessage(openPayResult.getMessage());

                listMessageDTO.add(newMessageDTO);
                responseDTO.setListMessage(listMessageDTO);
                LogService.logger.error("Error al intentar crear customer en openpay [" + openpayCustomerId + "]");
                LogService.logger.error("openPay Error Code: [" + openPayResult.getCode() + "]");
                LogService.logger.error("openPay Error Description: [" + e.getDescription() + "]");
//                LogService.logger.info(Constants.LOG_EXECUTING_END + " [Paso 5] " + Calendar.getInstance().getTime());
//                LogService.logger.info(Constants.LOG_EXECUTING_END + " [putCheckout] " + Calendar.getInstance().getTime());
                responseChargeOpenPayDTO.setResponseDTO(responseDTO);
                return responseChargeOpenPayDTO;
            } catch (ServiceUnavailableException e) {
                EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0600.toString());
                responseDTO.setCode(enumResultException.getValue());
                responseDTO.setStatus(enumResultException.getStatus());
                List<MessageDTO> listMessageDTO = new ArrayList<>();
                MessageDTO newMessageDTO = new MessageDTO();
                OpenPayErrorCodeEnum openPayResult = OpenPayErrorCodeEnum.getByCode(0);
                newMessageDTO.setErrorCode(openPayResult.getCode());
                newMessageDTO.setUserMessage(openPayResult.getMessage());
                listMessageDTO.add(newMessageDTO);
                responseDTO.setListMessage(listMessageDTO);
                LogService.logger.error("[Openpay  Caido]");
                LogService.logger.error("Error al intentar crear customer en openpay: [" + openpayCustomerId + "]");
                LogService.logger.error("openPay Error Code: [" + openPayResult.getCode() + "]");
                LogService.logger.error("openPay Error Description: [" + openPayResult.getMessage() + "]");
//            context.setRollbackOnly();
                responseChargeOpenPayDTO.setResponseDTO(responseDTO);
                return responseChargeOpenPayDTO;
            }
            openpayCustomerId = customerCreateResponse.getId();
        }

        // -------------------   Crear Cargo en Tarjeta
        Charge chargeCardResponse = null;
        MetaValue metaValueAmountBD = null;
        String carName = null;
        MetaValue metaValueBOOKINGPRICE = metaValueRepo.findByAlias(Constants.BOOKING_PRICE);
        Timestamp actualTimestamp = new Timestamp(System.currentTimeMillis());
        try {
            // Crear la tarjeta en Openpay
            // tokenId se genera desde el cliente con los datos de la tarjeta
            // deviceSessionId se genera desde el servidor de openpay
            Card rqCardOpenPay = new Card();
            rqCardOpenPay.tokenId(generateChargeDTO.getTokenId());
            rqCardOpenPay.setDeviceSessionId(generateChargeDTO.getDeviceSessionId());
            rqCardOpenPay = openpayAPI.cards().create(openpayCustomerId, rqCardOpenPay);

            // Crear Cargo en Tarjeta
            CreateCardChargeParams rqGenerateChargeCard = new CreateCardChargeParams();
            rqGenerateChargeCard.cardId(rqCardOpenPay.getId()); // =source_id
            metaValueAmountBD = metaValueRepo.findByAlias("BOOKING_PRICE");

            if(generateChargeDTO.getEmail().equalsIgnoreCase("german.mendoza@kavak.com")){
                rqGenerateChargeCard.amount(BigDecimal.valueOf(1L));
            }else {
                rqGenerateChargeCard.amount(BigDecimal.valueOf(Long.valueOf(metaValueAmountBD.getOptionName())));
            }
            rqGenerateChargeCard.currency(Currency.MXN);
            String description = "";
            carName = KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO());

            if(Long.valueOf(generateChargeDTO.getPaymentTypeId()).equals(Constants.PAYMENT_DISCOUNT)){
                description = "Reserva de Auto Kavak: " + carName;
            }

            if(Long.valueOf(generateChargeDTO.getPaymentTypeId()).equals(Constants.PAYMENT_FINANCING)){
                description = "Reserva con Financiamiento para Auto Kavak: " + carName;
            }

            if(Long.valueOf(generateChargeDTO.getPaymentTypeId()).equals(Constants.PAYMENT_72_HOURS)){
                description = "Reserva por 72 horas para Auto Kavak: " + carName;
            }
            rqGenerateChargeCard.description(description);
            //rqGenerateChargeCard.orderId("KAV-" + Calendar.getInstance().getTimeInMillis());
            rqGenerateChargeCard.orderId(""+Calendar.getInstance().getTimeInMillis());
            rqGenerateChargeCard.deviceSessionId(generateChargeDTO.getDeviceSessionId());

            // Generar Cargo tarjeta
            chargeCardResponse = openpayAPI.charges().create(openpayCustomerId, rqGenerateChargeCard);
            responseChargeOpenPayDTO.setCardCharge(chargeCardResponse);

            LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - Cargo a Tarjeta realizado con exito! genero la orden [" + responseChargeOpenPayDTO.getCardCharge().getOrderId() +"]"  );
            LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - Empezando carga en data transaccionales" );

            // Generar data Tablas Transaccionales
            Optional<SaleCheckpoint> saleCheckpointBD = saleCheckpointRepo.findById(saleCheckPointDTO.getId());

            SellCarOrder newSellCarOrder = new SellCarOrder();
            newSellCarOrder.setDetailId(saleCheckpointBD.get().getCarId());
            newSellCarOrder.setSellerId(sellCarDetailBD.get().getSellerId());
            if(saleCheckpointBD.get().getUserId() != null){
                newSellCarOrder.setBuyerId(saleCheckpointBD.get().getUserId());
            }else{
                newSellCarOrder.setBuyerId(saleCheckpointBD.get().getUser().getId());
            }
            newSellCarOrder.setDealDate(actualTimestamp);
            newSellCarOrder.setPrice(generateChargeDTO.getCarPrice().toString());
            newSellCarOrder.setComplete(true);
            newSellCarOrder.setDealPen(false);
            newSellCarOrder.setClosed(false);
            if(generateChargeDTO.getWarrantyId() !=null){
                newSellCarOrder.setSellCarWarrantyId(generateChargeDTO.getWarrantyId());
            }

            if(generateChargeDTO.getInsuranceId() !=null){
                newSellCarOrder.setSellCarInsuranceId(generateChargeDTO.getInsuranceId());
            }

            sellCarOrderRepo.save(newSellCarOrder);
            LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - SellCarOrder guardado con Exito! " );

            //  Creando registro en SellCarBuyer
            SellCarBuyer newSellCarBuyer = new SellCarBuyer();
            newSellCarBuyer.setDetailId(saleCheckpointBD.get().getCarId());
            if(saleCheckpointBD.get().getUserId() != null){
                newSellCarBuyer.setBuyerId(saleCheckpointBD.get().getUserId());
            }else{
                newSellCarBuyer.setBuyerId(saleCheckpointBD.get().getUser().getId());
            }
            newSellCarBuyer.setSellerId(sellCarDetailBD.get().getSellerId());
            sellCarBuyerRepo.save(newSellCarBuyer);

            LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - SellCarBuyer guardado con Exito! " );

            SaleCarOrderTransaction newSaleCarOrderTransaction = new SaleCarOrderTransaction();
            newSaleCarOrderTransaction.setTransactionId(chargeCardResponse.getId());
            newSaleCarOrderTransaction.setOrderId(chargeCardResponse.getOrderId());

            switch(Integer.valueOf(generateChargeDTO.getPaymentTypeId().toString())){
                case 141: newSaleCarOrderTransaction.setTransactionType("pagoDescuento");break;
                case 143: newSaleCarOrderTransaction.setTransactionType("pagoFinanciamiento");break;
                case 144: newSaleCarOrderTransaction.setTransactionType("pago72Horas");break;
                default:break ;
            }

            newSaleCarOrderTransaction.setRegisterDate(actualTimestamp);
            MetaValue metaValauePlatformBD =  metaValueRepo.findByAlias("OP");

            newSaleCarOrderTransaction.setPlatformId(metaValauePlatformBD.getId());
            newSaleCarOrderTransaction.setTransactionAmount(Long.valueOf(generateChargeDTO.getTotalPrice()) - Long.valueOf(metaValueBOOKINGPRICE.getOptionName()));
            if(saleCheckpointBD.get().getUserId() != null){
        	newSaleCarOrderTransaction.setBuyerId(saleCheckpointBD.get().getUserId());
            }else{
        	newSaleCarOrderTransaction.setBuyerId(saleCheckpointBD.get().getUser().getId());
            }
            newSaleCarOrderTransaction.setStatus(1);
            newSaleCarOrderTransaction.setPaymentCompleted(1);
            saleCarOrderTransactionRepo.save(newSaleCarOrderTransaction);

            LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - SaleCarOrderTransaction guardado con Exito! " );

            SaleCarOrder newSaleCarOrder = new SaleCarOrder();
//                    newSaleCarOrder.setItemId(saleCheckpointBDStep5.getCarId());
            newSaleCarOrder.setBuyerId(generateChargeDTO.getUserId());
            newSaleCarOrder.setSellCarDetail(sellCarDetailBD.get());
            newSaleCarOrder.setTransactionId("KAV-"+responseChargeOpenPayDTO.getCardCharge().getOrderId() + "-TRA");
            newSaleCarOrder.setItemName(KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()));
            newSaleCarOrder.setRegistrationDate(actualTimestamp);
            newSaleCarOrder.setTotalAmt(generateChargeDTO.getCarPrice().toString());
            newSaleCarOrder.setPaidAmt(metaValueBOOKINGPRICE.getOptionName());
            Long penditnAmt = (Long.valueOf(generateChargeDTO.getTotalPrice()) - Long.valueOf(metaValueAmountBD.getOptionName()));
            newSaleCarOrder.setPendingAmt(penditnAmt.toString());
            newSaleCarOrder.setBuyerName(KavakUtils.getFriendlyUserName(saleCheckpointBD.get().getUser().getDTO()));
            newSaleCarOrder.setPaymentMode("OPENPAY");
            newSaleCarOrder.setPaymentType("Money Transfer");
            newSaleCarOrder.setEmail(user.get().getEmail());
            newSaleCarOrder.setPhone(user.get().getDTO().getPhone());
            newSaleCarOrder.setAddress(generateChargeDTO.getBillingAddressDTO().getStreet() + " " + generateChargeDTO.getBillingAddressDTO().getExteriorNumber() + " " + generateChargeDTO.getBillingAddressDTO().getInteriorNumber());
            newSaleCarOrder.setCity("Ciudad de México");
            newSaleCarOrder.setState("Distrito Federal");
            newSaleCarOrder.setCountry("México");
            newSaleCarOrder.setZipCode(generateChargeDTO.getBillingAddressDTO().getZipCode());
            newSaleCarOrder.setActive(true);
            newSaleCarOrder.setComplete(false);
            newSaleCarOrder.setPaymentStatus("Pending");
            newSaleCarOrder.setReservationTime("72 hours");
            newSaleCarOrder.setDeliverStatus("Pendiente por Pago");
            newSaleCarOrder.setLoanDetails("NA");
            if(saleCheckpointBD.get().getSearchKavak() != null) {
                if (saleCheckpointBD.get().getSearchKavak().equalsIgnoreCase("Si")) {
                    newSaleCarOrder.setPickupType("Buscar en Kavak");
                } else {
                    newSaleCarOrder.setPickupType("Enviar a dirección");
                }
            }

            if(Long.valueOf(generateChargeDTO.getPaymentTypeId()).equals(Constants.PAYMENT_FINANCING)){
                if(generateChargeDTO.getDownPayment() != null) {
                    newSaleCarOrder.setFinancialAmount(Long.valueOf(generateChargeDTO.getTotalPrice()) - generateChargeDTO.getDownPayment());
                    newSaleCarOrder.setHookingAmount(Integer.valueOf(generateChargeDTO.getDownPayment().toString()));
                }else{
                    newSaleCarOrder.setFinancialAmount(Long.valueOf(generateChargeDTO.getTotalPrice()));
                }
                if(generateChargeDTO.getFinancingMonths()!= null) {
                    newSaleCarOrder.setFinancialMonths(generateChargeDTO.getFinancingMonths());
                }

            }else{
        	newSaleCarOrder.setFinancialAmount(null);
        	newSaleCarOrder.setFinancialMonths(null);
        	newSaleCarOrder.setHookingAmount(null);
            }

            newSaleCarOrder.setActionDate(actualTimestamp);
            newSaleCarOrder.setDaysToReturn(7);
            newSaleCarOrder.setHoldTime("72");
            SaleCarOrder saleCarOrder = saleCarOrderRepo.save(newSaleCarOrder);
            OrderResponseDTO saleCarOrderResponse = new OrderResponseDTO();

            try {

        	//Retornando ORDER
        	if(saleCarOrder.getId() != null && !saleCarOrder.getId().toString().trim().isEmpty()) {
        	    saleCarOrderResponse.setId(saleCarOrder.getId().toString());
        	}
        	if(saleCarOrder.getSellCarDetail().getId() != null && !saleCarOrder.getSellCarDetail().getId().toString().trim().isEmpty()) {
        	    saleCarOrderResponse.setItemId(saleCarOrder.getSellCarDetail().getId().toString());
        	}
        	if(saleCarOrder.getItemName() != null && !saleCarOrder.getItemName().trim().isEmpty()) {
        	    saleCarOrderResponse.setItemName(saleCarOrder.getItemName());
        	}
        	if(saleCarOrder.getPaidAmt() != null && !saleCarOrder.getPaidAmt().trim().isEmpty()) {
        	    saleCarOrderResponse.setPaidAmt(saleCarOrder.getPaidAmt());
        	}
        	if(saleCarOrder.getPendingAmt() != null && !saleCarOrder.getPendingAmt().trim().isEmpty()) {
        	    saleCarOrderResponse.setPendingAmt(saleCarOrder.getPendingAmt());
        	}
        	if(saleCarOrder.getTransactionId() != null && !saleCarOrder.getTransactionId().trim().isEmpty()) {
        	    saleCarOrderResponse.setTransactionId(saleCarOrder.getTransactionId());
        	}
        	responseChargeOpenPayDTO.setOrderResponseDTO(saleCarOrderResponse); 
            } catch (Exception e) {
        	e.printStackTrace();
            }
            
            LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - SaleCarOrder guardado con Exito! " );

            sellCarDetailBD.get().setSale(true);
            sellCarDetailBD.get().setBookingDate(actualTimestamp);
            sellCarDetailBD.get().setCancelDate(null);
            sellCarDetailBD.get().setSendNetsuite(0L);
            sellCarDetailRepo.save(sellCarDetailBD.get());

            LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - SellCarDetail guardado con Exito! " );

            MetaValue metaValueCPSA = metaValueRepo.findByAlias("CPSA");
            saleCheckpointBD.get().setStatus(metaValueCPSA.getId());
            saleCheckpointBD.get().setCheckoutResult("Pago de reserva con éxito");
            saleCheckpointBD.get().setPaymentStatus(1L);
            saleCheckpointBD.get().setPaymentPlatform("Openpay");
            saleCheckpointBD.get().setPaymentPlatformId(responseChargeOpenPayDTO.getCardCharge().getId());
            saleCheckpointBD.get().setTransactionNumber(responseChargeOpenPayDTO.getCardCharge().getOrderId());
            saleCheckpointBD.get().setCarCost(generateChargeDTO.getCarPrice().toString());
            saleCheckpointBD.get().setTotalAmount(generateChargeDTO.getTotalPrice());
	    saleCheckpointBD.get().setUpdateDate(actualTimestamp);
            saleCheckpointBD.get().setSentNetsuite(2L);
	    MetaValue metaValueCPRP = metaValueRepo.findByAlias("CPRP");
	    saleCheckpointBD.get().setStatus(metaValueCPRP.getId());

            saleCheckpointBD.get().setCarReserve(metaValueBOOKINGPRICE.getOptionName());
            saleCheckpointBD.get().setUpdateDate(actualTimestamp);

            if(generateChargeDTO.getHasTradeIn()){
                saleCheckpointBD.get().setHasTradeinCar(true);
            }else{
                saleCheckpointBD.get().setHasTradeinCar(false);
            }
            saleCheckpointBD.get().setOfferCheckPointId(generateChargeDTO.getOfferCheckpointId());
            saleCheckpointBD.get().setSentNetsuite(2L);
            saleCheckpointRepo.save(saleCheckpointBD.get());           

            LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - SaleCheckpoint guardado con Exito! " );
            
            try {

        	//Actualizar Notificaciones al Reservar Auto
        	List<CustomerNotifications> listCustomerNotifications = new ArrayList<>();
        	listCustomerNotifications = customerNotificationsRepo.findNotificationBookedCar(sellCarDetailDTO.getId());            
        	for(CustomerNotifications customerNotificationsActual : listCustomerNotifications) {
        	    customerNotificationsActual.setType(248L);
        	    customerNotificationsActual.setEmailSent(false);
        	    customerNotificationsActual.setCancelledCarBookedSmsSent(false);
        	    customerNotificationsRepo.save(customerNotificationsActual);
        	}
        	LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - CustomerNotifications guardado con Exito! " );

            } catch (Exception e) {
        	// TODO: handle exception
        	LogService.logger.error(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - CustomerNotifications error Catch = "+ e);
            }

        } catch (OpenpayServiceException e) {
            EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
            responseDTO.setCode(enumResultException.getValue());
            responseDTO.setStatus(enumResultException.getStatus());
            List<MessageDTO> listMessageDTO = new ArrayList<>();
            MessageDTO newMessageDTO = new MessageDTO();
            OpenPayErrorCodeEnum openPayResult = OpenPayErrorCodeEnum.getByCode(e.getErrorCode());
            newMessageDTO.setErrorCode(openPayResult.getCode());
            newMessageDTO.setApiMessage(e.getDescription());
            newMessageDTO.setUserMessage(openPayResult.getMessage());
            listMessageDTO.add(newMessageDTO);
            responseDTO.setListMessage(listMessageDTO);
            LogService.logger.error("Error Catch 1 al intentar generar cargo en TARJETA openpay para el customer: [" + openpayCustomerId.toString() + "]");
            LogService.logger.error("openPay Error Code: [" + openPayResult.getCode() + "]");
            LogService.logger.error("openPay Error Description: [" + e.getDescription() + "]");
            //context.setRollbackOnly();
            responseChargeOpenPayDTO.setResponseDTO(responseDTO);
            return responseChargeOpenPayDTO;

        } catch (ServiceUnavailableException e){
            EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0600.toString());
            responseDTO.setCode(enumResultException.getValue());
            responseDTO.setStatus(enumResultException.getStatus());
            List<MessageDTO> listMessageDTO = new ArrayList<>();
            MessageDTO newMessageDTO = new MessageDTO();
            OpenPayErrorCodeEnum openPayResult = OpenPayErrorCodeEnum.getByCode(0);
            newMessageDTO.setErrorCode(openPayResult.getCode());
            newMessageDTO.setUserMessage(openPayResult.getMessage());
            listMessageDTO.add(newMessageDTO);
            responseDTO.setListMessage(listMessageDTO);
            LogService.logger.error("[Openpay  Caido]");
            LogService.logger.error("Error Catch 2 al intentar generar cargo en TARJETA openpay para el customer: [" + openpayCustomerId.toString() + "]");
            LogService.logger.error("openPay Error Code: [" + openPayResult.getCode() + "]");
            LogService.logger.error("openPay Error Description: [" + openPayResult.getMessage() + "]");
//            context.setRollbackOnly();
            responseChargeOpenPayDTO.setResponseDTO(responseDTO);
            return responseChargeOpenPayDTO;
        }

        // -------------------   Crear Cargo en Banco
        try{ 

            // Generando cargo al Banco
            if((Long.valueOf(generateChargeDTO.getPaymentTypeId()).equals(Constants.PAYMENT_DISCOUNT) || Long.valueOf(generateChargeDTO.getPaymentTypeId()).equals(Constants.PAYMENT_72_HOURS)) && chargeCardResponse.getId() != null){
                Customer customer = openpayAPI.customers().get(openpayCustomerId);
                CreateBankChargeParams rqGenerateChargeBank = new CreateBankChargeParams();
                if(generateChargeDTO.getEmail().equalsIgnoreCase("german.mendoza@kavak.com")){
                    rqGenerateChargeBank.amount(BigDecimal.valueOf(1L));
                }else {
                    rqGenerateChargeBank.amount(BigDecimal.valueOf(Long.valueOf(generateChargeDTO.getTotalPrice()) - Long.valueOf(metaValueAmountBD.getOptionName())));
                }
                rqGenerateChargeBank.description("Compra de Auto Kavak: " + carName);
                rqGenerateChargeBank.customer(customer);
                rqGenerateChargeBank.orderId(chargeCardResponse.getOrderId() + "-TRA");
                Charge chargeBankResponse = openpayAPI.charges().create(rqGenerateChargeBank);
                if(chargeBankResponse != null){
                    responseChargeOpenPayDTO.setBankCharge(chargeBankResponse);
                    LogService.logger.info(Constants.LOG_EXECUTING_START+ " [generateChargesOpenPay] - cargo a Banco realizado con exito! genero la orden [" + responseChargeOpenPayDTO.getBankCharge().getOrderId() +"]"  );
                }
            }

        } catch (OpenpayServiceException e) {
            //EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0204.toString());
            //El error se va a colocar 200 para que pueda grabar los datos exitosos de la tarjeta
            EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResultException.getValue());
            responseDTO.setStatus(enumResultException.getStatus());
            List<MessageDTO> listMessageDTO = new ArrayList<>();
            MessageDTO newMessageDTO = new MessageDTO();
            OpenPayErrorCodeEnum openPayResult = OpenPayErrorCodeEnum.getByCode(e.getErrorCode());
            newMessageDTO.setErrorCode(openPayResult.getCode());
            newMessageDTO.setApiMessage(e.getDescription());
            newMessageDTO.setUserMessage(openPayResult.getMessage());
            listMessageDTO.add(newMessageDTO);
            responseDTO.setListMessage(listMessageDTO);
            LogService.logger.error("Error al intentar generar cargo en BANCO openpay para el customer: [" + openpayCustomerId.toString() + "]");
            LogService.logger.error("openPay Error Code: [" + openPayResult.getCode() + "]");
            LogService.logger.error("openPay Error Description: [" + e.getDescription() + "]");
//            context.setRollbackOnly();
            responseChargeOpenPayDTO.setResponseDTO(responseDTO);
            return responseChargeOpenPayDTO;

        } catch (ServiceUnavailableException e) {
            //EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0600.toString());
          //El error se va a colocar 200 para que pueda grabar los datos exitosos de la tarjeta
            EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
            responseDTO.setCode(enumResultException.getValue());
            responseDTO.setStatus(enumResultException.getStatus());
            List<MessageDTO> listMessageDTO = new ArrayList<>();
            MessageDTO newMessageDTO = new MessageDTO();
            OpenPayErrorCodeEnum openPayResult = OpenPayErrorCodeEnum.getByCode(0);
            newMessageDTO.setErrorCode(openPayResult.getCode());
            newMessageDTO.setUserMessage(openPayResult.getMessage());
            listMessageDTO.add(newMessageDTO);
            responseDTO.setListMessage(listMessageDTO);
            LogService.logger.error("[Openpay  Caido]");
            LogService.logger.error("Error al intentar generar cargo en BANCO openpay para el customer: [" + openpayCustomerId.toString() + "]");
            LogService.logger.error("openPay Error Code: [" + openPayResult.getCode() + "]");
            LogService.logger.error("openPay Error Description: [" + openPayResult.getMessage() + "]");
//            context.setRollbackOnly();
            responseChargeOpenPayDTO.setResponseDTO(responseDTO);
            return responseChargeOpenPayDTO;
        }


        EndPointCodeResponseEnum enumResultException = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0200.toString());
        responseDTO.setCode(enumResultException.getValue());
        responseChargeOpenPayDTO.setResponseDTO(responseDTO);
        if(responseChargeOpenPayDTO.getBankCharge() != null){
        LogService.logger.info("[generateChargesOpenPay]:  " + responseChargeOpenPayDTO.getBankCharge());
        }
        LogService.logger.info("[generateChargesOpenPay]:  " + responseChargeOpenPayDTO.getCardCharge());
        LogService.logger.info("[generateChargesOpenPay] " + (Calendar.getInstance().getTimeInMillis() - starTime));
       if(responseChargeOpenPayDTO.getBankCharge() != null){
        LogService.logger.info(" ************************************************************************************");
        LogService.logger.info(" ************************************************************************************");
        LogService.logger.info(" ****************** Respuesta OpenPay BankCharge General:   CITA CON RESERVA       **");
        LogService.logger.info(" **                 ---------------------------------------------------------      **");
        LogService.logger.info(" ** Description: " + responseChargeOpenPayDTO.getBankCharge().getDescription());
        LogService.logger.info(" ** OperationType: " + responseChargeOpenPayDTO.getBankCharge().getTransactionType());
        LogService.logger.info(" ** Method: " + responseChargeOpenPayDTO.getBankCharge().getMethod());
        LogService.logger.info(" ** OrderId: " + responseChargeOpenPayDTO.getBankCharge().getOrderId());
        LogService.logger.info(" ** CustomerId: " + responseChargeOpenPayDTO.getBankCharge().getCustomerId());
        LogService.logger.info(" **                                                                                **");
        LogService.logger.info(" *****************  Respuesta OpenPay BankCharge PymentMethod:                     **");
        LogService.logger.info(" **                 -------------------------------------------                    **");
        LogService.logger.info(" ** Type: " + responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getType());
        LogService.logger.info(" ** Bank: " + responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getBank());
        LogService.logger.info(" ** Clabe: " + responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getClabe());
        LogService.logger.info(" ** Name: " + responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getName());
        LogService.logger.info(" ** Type: " + responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getType());
        LogService.logger.info(" **                                                                                **");  
       }        
        LogService.logger.info(" ***************** Respuesta OpenPay CardCharge General:                           **");
        LogService.logger.info(" **                -------------------------------------                           **");
        LogService.logger.info(" ** Amount: "+ responseChargeOpenPayDTO.getCardCharge().getAmount());
        LogService.logger.info(" ** Id: "+ responseChargeOpenPayDTO.getCardCharge().getId());
        LogService.logger.info(" ** CreationDate: "+ responseChargeOpenPayDTO.getCardCharge().getCreationDate());
        LogService.logger.info(" ** OperationDate: "+ responseChargeOpenPayDTO.getCardCharge().getOperationDate());
        LogService.logger.info(" ** Status: "+ responseChargeOpenPayDTO.getCardCharge().getStatus());
        LogService.logger.info(" ** Description: "+ responseChargeOpenPayDTO.getCardCharge().getDescription());
        LogService.logger.info(" ** Authonization: "+ responseChargeOpenPayDTO.getCardCharge().getAuthorization());
        LogService.logger.info(" ** Method: "+ responseChargeOpenPayDTO.getCardCharge().getMethod());
        LogService.logger.info(" **                                                                                **");
        LogService.logger.info(" ************************************************************************************");
        LogService.logger.info(" ************************************************************************************");
        return responseChargeOpenPayDTO;
    }

}
