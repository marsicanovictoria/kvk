package com.kavak.core.service;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.springframework.web.client.RestTemplate;

import com.github.ooxi.phparser.SerializedPhpParser;
import com.github.ooxi.phparser.SerializedPhpParserException;
import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.response.ApiUrlShortenerResponseDTO;
import com.kavak.core.dto.response.GetCarscoutResultsResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.model.AppointmentSchedule;
import com.kavak.core.model.CarData;
import com.kavak.core.model.CarscoutAlert;
import com.kavak.core.model.ContactUs;
import com.kavak.core.model.CustomerNotifications;
import com.kavak.core.model.FinancingUser;
import com.kavak.core.model.FinancingUserIncome;
import com.kavak.core.model.FinancingUserScore;
import com.kavak.core.model.MetaValue;
import com.kavak.core.model.OfferCheckpoint;
import com.kavak.core.model.SaleCheckpoint;
import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.User;
import com.kavak.core.model.UserMeta;
import com.kavak.core.repositories.AppointmentScheduleRepository;
import com.kavak.core.repositories.CarDataRepository;
import com.kavak.core.repositories.CarscoutAlertRepository;
import com.kavak.core.repositories.ContactUsRepository;
import com.kavak.core.repositories.CustomerNotificationsRepository;
import com.kavak.core.repositories.FinancingUserIncomeRepository;
import com.kavak.core.repositories.FinancingUserRepository;
import com.kavak.core.repositories.FinancingUserScoreRepository;
import com.kavak.core.repositories.MetaValueRepository;
import com.kavak.core.repositories.OfferCheckpointRepository;
import com.kavak.core.repositories.SaleCheckpointRepository;
import com.kavak.core.repositories.SellCarDetailRepository;
import com.kavak.core.repositories.UserRepository;
import com.kavak.core.service.netsuite.SaleOpportunityServices;
import com.kavak.core.util.Constants;
import com.kavak.core.util.KavakUtils;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;

@Stateless
public class TransactionalEmailsServices {

    @Inject
    private OfferCheckpointRepository offerCheckpointRepo;

    @Inject
    private SaleCheckpointRepository saleCheckpointRepo;

    @Inject
    private CustomerNotificationsRepository customerNotificationsRepo;

    @Inject
    private SellCarDetailRepository sellCarDetailRepo;

    @Inject
    private UserRepository userRepo;
    
    @Inject
    private CarDataRepository carDataRepo;
    
    @Inject
    private AppointmentScheduleRepository appointmentScheduleRepo;
    
    @Inject
    private CarscoutAlertRepository carscoutAlertRepo;
    
    @Inject
    private MetaValueRepository metaValueRepo;
    
    @Inject
    private FinancingUserScoreRepository financingUserScoreRepo;
    
    @Inject
    private FinancingUserRepository financingUserRepo;
    
    @Inject
    private FinancingUserIncomeRepository financingUserIncomeRepo;
    
    @Inject
    private ContactUsRepository contactUsRepo;
    
    
    
        
    @EJB(mappedName = LookUpNames.SESSION_CelesteServices)
    CelesteServices celesteServices;
    
    @EJB(mappedName = LookUpNames.SESSION_SaleOpportunityServices)
    SaleOpportunityServices saleOpportunityServices;
    
    @EJB(mappedName = LookUpNames.SESSION_CarscoutServices)
    CarscoutServices carscoutServices;

    /**
     * Email: Etapa 5 Consignación- Completó registro (adjuntó TC) chekpoint 5 - 5A
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFifthStageConsignmentCompletedRegister() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage5_consignment_completed_register_atachment_tc.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFifthStageConsignmentCompletedRegister();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###"; 

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister =  Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Felicidades por Agendar una Inspección a tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 5A");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "Por Contactar");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }
		    
		    if (OfferCheckpointActual.getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, OfferCheckpointActual.getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);

		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFifthStageConsignmentCompletedRegister  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch interno correo: No se envio correo de OfferCheckpointFifthStageConsignmentCompletedRegister - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFifthStageConsignmentCompletedRegister 5A");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo correo: No se envio correo de OfferCheckpointFifthStageConsignmentCompletedRegister ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 5 Consignación- (NO) Seleccionó Horario ni dia pero si Completó registro (adjuntó TC) - 5B
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFifthStageConsignmentNOSelectedDayAndTimeCompletedRegister() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage5_consignment_noselected_dayandtime_completed_register_attached_tc.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFifthStageConsignmentNoSelectedDayAndTimeCompletedRegister();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Felicidades por Agendar una Inspección a tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 5B");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }
		    
		    if (OfferCheckpointActual.getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, OfferCheckpointActual.getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);

		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFifthStageConsignmentNOSelectedDayAndTimeCompletedRegister  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFifthStageConsignmentNOSelectedDayAndTimeCompletedRegister  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFifthStageConsignmentNOSelectedDayAndTimeCompletedRegister 5B");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo correo: No se envio correo de OfferCheckpointFifthStageConsignmentNOSelectedDayAndTimeCompletedRegister ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 5 Consignación - No WL Completó registro (adjuntó TC) chekpoint 5 - 5C
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFifthStageConsignmentNoWLCompletedRegisterAttachedTC() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage5_consignment_nowl_completed_register_attached_tc.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFifthStageConsignmentNoWLCompletedRegister();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Felicidades por Agendar una Inspección a tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 5C");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "Por Contactar");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }
		    
		    if (OfferCheckpointActual.getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, OfferCheckpointActual.getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFifthStageConsignmentNoWLCompletedRegisterAttachedTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFifthStageConsignmentNoWLCompletedRegisterAttachedTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();

		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFifthStageConsignmentNoWLCompletedRegisterAttachedTC 5C");

	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointFifthStageConsignmentNoWLCompletedRegisterAttachedTC ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 5 Consignación - No WL (NO) Seleccionó Horario ni Dia pero si Completó registro (adjuntó TC) chekpoint 5 - 5D
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFifthStageConsignmentNoWlNOSelectedDayAndTimeCompletedRegister() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage5_consignment_nowl_no_selected_dayandtime_completed_register_attached_tc.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFifthStageConsignmentNoWlNOSelectedDayAndTimeCompletedRegister();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Felicidades por Agendar una Inspección a tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 5D");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "Por Contactar");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, OfferCheckpointActual.getMaxInstantOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, OfferCheckpointActual.getMinInstantOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, OfferCheckpointActual.getMax30DaysOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, OfferCheckpointActual.getMin30DaysOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }
		    
		    if (OfferCheckpointActual.getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, OfferCheckpointActual.getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFifthStageConsignmentNoWlNOSelectedDayAndTimeCompletedRegister  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFifthStageConsignmentNoWlNOSelectedDayAndTimeCompletedRegister  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFifthStageConsignmentNoWlNOSelectedDayAndTimeCompletedRegister 5D");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointFifthStageConsignmentNoWlNOSelectedDayAndTimeCompletedRegister ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 4 Etapa 4 Consignación - Seleccionó horario y día pero no adjuntó TC chekpoint 4 - 4A
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFourthStageConsignmentNotAtachmentTC() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage4_consignment_selected_dayandtime_noattached_tc.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFourthStageConsignmentSelectedDateAndTimeNoAttachedTc();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Felicidades por Agendar una Inspección a tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 4A");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "Por Contactar");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }
		    
		    if (OfferCheckpointActual.getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, OfferCheckpointActual.getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFourthStageConsignmentNotAtachmentTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFourthStageConsignmentNotAtachmentTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFourthStageConsignmentNotAtachmentTC 4A");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointFourthStageConsignmentNotAtachmentTC");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 4 Consignación - No WL Seleccionó horario y día pero no adjuntó TC chekpoint 4 - 4B
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFourthStageConsignmentNotWlNotAtachmentTC() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage4_consignment_nowl_selected_dayandtime_noattached_tc.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFourthStageConsignmentNotWlNoAttachedTc();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Felicidades por Agendar una Inspección a tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 4B");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "Por Contactar");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }
		    
		    if (OfferCheckpointActual.getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, OfferCheckpointActual.getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);

		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFourthStageConsignmentNotWlNotAtachmentTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFourthStageConsignmentNotWlNotAtachmentTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFourthStageConsignmentNotWlNotAtachmentTC 4B");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointFourthStageConsignmentNotWlNotAtachmentTC");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 4 Consignación - No WL (NO) Seleccionó horario y día y no adjuntó TC chekpoint 4 - 4C
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFourthStageConsignmentNotWlNotDayAndTimeNotAtachmentTC() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage4_consignment_nowl_nodayandtime_noattached_tc.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFourthStageConsignmentNotWlNotDateAndTimeNotAtachmentTC();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Felicidades por Agendar una Inspección a tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 4C");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "Por Contactar");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }
		    
		    if (OfferCheckpointActual.getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, OfferCheckpointActual.getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);

		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFourthStageConsignmentNotWlNotDayAndTimeNotAtachmentTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFourthStageConsignmentNotWlNotDayAndTimeNotAtachmentTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFourthStageConsignmentNotWlNotDayAndTimeNotAtachmentTC 4C");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointFourthStageConsignmentNotWlNotDayAndTimeNotAtachmentTC ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 4 Consignación - (NO) Seleccionó horario y día Y no adjuntó TC chekpoint 4 - 4D
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFourthStageConsignmentNotDayAndTimeNotAtachmentTC() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage4_consignment_nodayandtime_noattached_tc.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFourthStageConsignmentNotDayAndTimeNotAtachmentTC();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Felicidades por Agendar una Inspección a tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 4D");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null && !OfferCheckpointActual.getHourInpectionSlotText().equals("2:00 am a 3:00 am")) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "Por Contactar");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }
		    
		    if (OfferCheckpointActual.getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, OfferCheckpointActual.getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFourthStageConsignmentNotDayAndTimeNotAtachmentTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFourthStageConsignmentNotDayAndTimeNotAtachmentTC  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFourthStageConsignmentNotDayAndTimeNotAtachmentTC 4D");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointFourthStageConsignmentNotDayAndTimeNotAtachmentTC ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 3 Consignación - No WL Indicó dirección pero no día y horario chekpoint 3 - 3A
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointThirdStageConsignmentNotWlSelectedAddressNotDayAndTime() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage3_consignment_nowl_selected_address_nodayandtime.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findThirdStageConsignmentNotWlSelectedAddressNotDayAndTime();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("giovanni.jimenez@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Te Ayudamos a Vender tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 3A");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de ThirdStageConsignmentNotWlSelectedAddressNotDayAndTime  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de ThirdStageConsignmentNotWlSelectedAddressNotDayAndTime  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - ThirdStageConsignmentNotWlSelectedAddressNotDayAndTime 3A");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de ThirdStageConsignmentNotWlSelectedAddressNotDayAndTime ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 3 Consignación - Indicó dirección pero no día y horario chekpoint 3 - 3B
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointThirdStageConsignmentSelectedAddressNoDayAndTime() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage3_consignment_selected_address_nodayandtime.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findThirdStageConsignmentSelectedAddressNotDayAndTime();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("giovanni.jimenez@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Te Ayudamos a Vender tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 3B");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de ThirdStageConsignmentSelectedAddressNoDayAndTime  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de ThirdStageConsignmentSelectedAddressNoDayAndTime  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - ThirdStageConsignmentSelectedAddressNoDayAndTime 3B");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de ThirdStageConsignmentSelectedAddressNoDayAndTime ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 2 Consignación- Aceptó oferta pero no indicó dirección ( 2 escenarios en el texto) chekpoint 2 - 2A
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointSecondStageConsignmentAcceptedOffersNotAddress() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage2_consignment_accepted_offers_notaddress.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findSecondStageConsignmentAcceptedOffersNotaddress();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("giovanni.jimenez@kavak.com");
		    }

		    CarData carDataBD = carDataRepo.findBySku(OfferCheckpointActual.getSku());
		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Te Ayudamos a Vender tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 2A");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }
		    
		    if ( carDataBD.getFullVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_FULL_VERSION, carDataBD.getFullVersion() );
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_FULL_VERSION, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);	
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointSecondStageConsignmentAcceptedOffersNotAddress  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointSecondStageConsignmentAcceptedOffersNotAddress  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointSecondStageConsignmentAcceptedOffersNotAddress 2A");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointSecondStageConsignmentAcceptedOffersNotAddress ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 2 Consignación- Aceptó oferta pero no indicó dirección ( 2 escenarios en el texto) chekpoint 2 - 2B
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointSecondStageConsignmentNotWlAcceptedOffersNotAddress() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage2_consignment_notwl_accepted_offers_notaddress.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findSecondStageConsignmentNotWlAcceptedOffersNotaddress();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("giovanni.jimenez@kavak.com");
		    }
		    CarData carDataBD = carDataRepo.findBySku(OfferCheckpointActual.getSku());
		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Te Ayudamos a Vender tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 2B");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN,  decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }
		    
		    if ( carDataBD.getFullVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_FULL_VERSION, carDataBD.getFullVersion() );
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_FULL_VERSION, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointSecondStageConsignmentNotWlAcceptedOffersNotAddress  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointSecondStageConsignmentNotWlAcceptedOffersNotAddress  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointSecondStageConsignmentNotWlAcceptedOffersNotAddress 2A");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointSecondStageConsignmentNotWlAcceptedOffersNotAddress ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 1 Seccion No cumple Requisitos chekpoint 1 - 1A
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFirstStageSectionNotRequirements() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage1_section_not_requirements.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFirstStageSectionNotRequirements();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";

	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("giovanni.jimenez@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Te Ayudamos a Vender tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 1A");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail,templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFirstStageSectionNotRequirements  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFirstStageSectionNotRequirements  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFirstStageSectionNotRequirements 1A");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointFirstStageSectionNotRequirements ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa 1 Consignación - Se registró y no aceptó oferta chekpoint 1 - 1B
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFirstStageConsignmentRegisterNotAcceptedOffer() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage1_consignment_register_not_accepted_offer.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFirstStageConsignmentRegisterNotAcceptedOffer();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";

	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("giovanni.jimenez@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Te Ayudamos a Vender tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 1B");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFirstStageConsignmentRegisterNotAcceptedOffer  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFirstStageConsignmentRegisterNotAcceptedOffer  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFirstStageConsignmentRegisterNotAcceptedOffer 1B");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointFirstStageConsignmentRegisterNotAcceptedOffer");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Etapa1 Consignacion - No WL Se registró y no aceptó oferta chekpoint 1 - 1C
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFirstStageConsignmentNotWlRegisterNotAcceptedOffer() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "stage1_consignment_notwl_register_not_accepted_offer.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findFirstStageConsignmentNotWlRegisterNotAcceptedOffer();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";
	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("giovanni.jimenez@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Te Ayudamos a Vender tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister + " ID 1C");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionDate() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, OfferCheckpointActual.getInspectionDate());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_DATE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getHourInpectionSlotText() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, OfferCheckpointActual.getHourInpectionSlotText());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_HOUR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getInspectionLocationDescription() != null) {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, OfferCheckpointActual.getInspectionLocationDescription());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSPECTION_PLACE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMaxConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			String str = OfferCheckpointActual.getMinConsignationOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMaxInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			String str = OfferCheckpointActual.getMinInstantOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMax30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			String str = OfferCheckpointActual.getMin30DaysOffer().replace("$","").replace(",", "");
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormat.format(Long.valueOf(str)));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);

		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de OfferCheckpointFirstStageConsignmentNotWlRegisterNotAcceptedOffer  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setSendEmail(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de OfferCheckpointFirstStageConsignmentNotWlRegisterNotAcceptedOffer  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }
	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - OfferCheckpointFirstStageConsignmentNotWlRegisterNotAcceptedOffer 1C");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de OfferCheckpointFirstStageConsignmentNotWlRegisterNotAcceptedOffer ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: checkoutPaymentError
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO checkoutPaymentError() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "checkout_payment_error.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findCheckoutPaymentError();
	    

	    if (listSaleCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (SaleCheckpoint SaleCheckpointActual : listSaleCheckpointBD) {
		try {
		    String email = SaleCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("checkout.payments@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX" + " " + "ERROR AL PAGAR EL APARTADO - Stock ID #" + SaleCheckpointActual.getCarId() + " " + SaleCheckpointActual.getCarMake() + " " + SaleCheckpointActual.getCarModel() + " " + SaleCheckpointActual.getCarVersion() + " " + SaleCheckpointActual.getCarYear());
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (SaleCheckpointActual.getCustomerLastName() != null) {
			if (SaleCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, SaleCheckpointActual.getCustomerName() + " " + SaleCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (SaleCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, SaleCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (SaleCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, SaleCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (SaleCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, SaleCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (SaleCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, SaleCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (SaleCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(SaleCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (SaleCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, SaleCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (SaleCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, SaleCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (SaleCheckpointActual.getCarId() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_ID, SaleCheckpointActual.getCarId());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_ID, "No Registrado");
		    }

		    if (SaleCheckpointActual.getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, SaleCheckpointActual.getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    if (SaleCheckpointActual.getCheckoutResult() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CHECKOUT_RESULT, SaleCheckpointActual.getCheckoutResult());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CHECKOUT_RESULT, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : SaleCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    if (sendEmail) {
			try {
			    InternetAddress fromEmail = new InternetAddress("ricardo.sue@kavak.com", "Ricardo Sue");
			    boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, fromEmail, null);
			    if (!emailSend) {
				LogService.logger.info("No se envio correo de checkoutPaymentError  - Id Venta [" + SaleCheckpointActual.getId() + "]");
			    }
			} catch (UnsupportedEncodingException e) {
			    e.printStackTrace();
			}

		    }
		    SaleCheckpointActual.setSendEmail(true);
		    saleCheckpointRepo.save(SaleCheckpointActual);
		} catch (Exception e) {
		    SaleCheckpointActual.setSendEmail(true);
		    saleCheckpointRepo.save(SaleCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de checkoutPaymentError - Id Venta [" + SaleCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listSaleCheckpointBD.size() + "] - checkoutPaymentError");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de checkoutPaymentError ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: availableCarsNotification
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO availableCarsNotification() throws MessagingException {
		
    	ResponseDTO responseDTO = new ResponseDTO();
		
	    // Generate excel file consolidated
	    try {
	    	LogService.logger.info("Se procede a consultar las notificaciones de reservas caidas para generar excel");
		    List<Long> listCustomerNotificationsDistinctBD = customerNotificationsRepo.findAvailableCarsNotificationDistinct();
			
	    	if (!listCustomerNotificationsDistinctBD.isEmpty()){
	    		for (Long stockId : listCustomerNotificationsDistinctBD){
	    			LogService.logger.info("Se procede a generar consolidado de availableCarsNotification (Excel) para el stock id: " + stockId);

	    			celesteServices.sendAvailableCarsNotificationsExcelEmail(stockId);
	    		}
	    	}else{
	    		LogService.logger.info("No hay registros de autos liberados para generar excel de notificaciones.");
	    	}
	    	
		} catch (Exception e) {
			LogService.logger.error("Catch Interno correo: No se pudo enviar correo consolidado de availableCarsNotification (Excel)");
			e.printStackTrace();
		}
    	
    	
    	try {
		    String metaKey = Constants.META_VALUE_PHONE;
		    List<String> emailCc = new ArrayList<>();
	        String emailTo= "";
	        List<String> emailCcCustomer = new ArrayList<String>();
	        String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";
		    String templateEmail = "available_cars_notification.html";
		    List<CustomerNotifications> listCustomerNotificationsBD = customerNotificationsRepo.findAvailableCarsNotification();
	
		    if (listCustomerNotificationsBD.isEmpty()) {
		    	return responseDTO;
		    }
	
		    String patternNumber = "$###,###,###";
		    String patternNumberKM = "###,###,###";

		    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
		    DecimalFormat decimalFormat = (DecimalFormat) nf;
		    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
		    decimalFormat.applyPattern(patternNumber);
		    decimalFormatKM.applyPattern(patternNumberKM);
	
		    for (CustomerNotifications customerNotificationsActual : listCustomerNotificationsBD) {
				try {
				    Optional<SellCarDetail> sellCarDetailBd = sellCarDetailRepo.findById(customerNotificationsActual.getCarId());
				    String email = customerNotificationsActual.getUser().getEmail();
				    
				    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
				    	emailCc.add("it@kavak.com");
				    	emailTo= "ayuda@kavak.com";
				    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
				    	emailCc.add("cars.notifications@kavak.com");
				    	emailTo= "ayuda@kavak.com";
				    }
	
				    HashMap<String, Object> subjectData = new HashMap<>();
				    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX" + customerNotificationsActual.getUser().getName() + " nuestro " + sellCarDetailBd.get().getCarModel() + " " + sellCarDetailBd.get().getCarYear() + " está disponible ¡Apúrate y Resérvalo!");
				    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
				    HashMap<String, Object> dataEmail = new HashMap<>();
		
				    if (customerNotificationsActual.getUser().getName() != null) {
				    	dataEmail.put(Constants.KEY_EMAIL_USER_NAME, customerNotificationsActual.getUser().getName());
				    } else {
				    	dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
				    }
		
				    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));
		
				    if (sellCarDetailBd.get().getCarMake() != null) {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, sellCarDetailBd.get().getCarMake());
				    } else {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
				    }
		
				    if (sellCarDetailBd.get().getCarModel() != null) {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, sellCarDetailBd.get().getCarModel());
				    } else {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
				    }
	
				    if (sellCarDetailBd.get().getCarKm() != null) {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(sellCarDetailBd.get().getCarKm())));
				    } else {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
				    }
		
				    if (customerNotificationsActual.getCarId() != null) {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_ID, customerNotificationsActual.getCarId());
				    } else {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_ID, "No Registrado");
				    }
		
				    if (customerNotificationsActual.getUser().getEmail() != null) {
				    	dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, customerNotificationsActual.getUser().getEmail());
				    } else {
				    	dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
				    }
				    
				    if (sellCarDetailBd.get().getCarYear() != null) {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, sellCarDetailBd.get().getCarYear());
				    } else {
				    	dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
				    }
			    
				    dataEmail.put(Constants.ATTACHMENT_FILES, null);
				    
				    String userPhone = null;
				    for (UserMeta userMetaActual : customerNotificationsActual.getUser().getListUserMeta()) {
						if (userMetaActual.getMetaKey().equals(metaKey)) {
						    userPhone = userMetaActual.getMetaValue();
						}
				    }
	
				    if (userPhone != null) {
				    	dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
				    } else {
				    	dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
				    }
		    
				    try {
				    	InternetAddress fromEmail = new InternetAddress("hola@kavak.com", "Hola Kavak");
		
				    	boolean emailSend = KavakUtils.sendEmail(email, emailCcCustomer, subjectData, templateEmail, templateEmailBase, dataEmail, fromEmail, null);
						if (!emailSend) {
						    LogService.logger.info("No se envio correo de availableCarsNotification al usuario " + customerNotificationsActual.getUser().getName());
						}
						
						boolean internalEmailSend = KavakUtils.sendInternalEmail(emailTo, emailCc, subjectData,"internal_available_cars_notification.html", null, dataEmail);
						if (!internalEmailSend) {
						    LogService.logger.info("No se envio correo interno de internalAvailableCarsNotification al usuario " + customerNotificationsActual.getUser().getName());
						}
					
				    } catch (UnsupportedEncodingException e) {
				    	e.printStackTrace();
				    }
			    
				    customerNotificationsActual.setEmailSent(true);
				    customerNotificationsRepo.save(customerNotificationsActual);
				    
				    //ANTONY: Desactivamos esta funcionalidad porque ya va a crearlos de manera inbound.
				    /*
				    try {
				    	LogService.logger.info("Se procede a crear el registro del user_id " + customerNotificationsActual.getUser().getId() + " con el stock id " + customerNotificationsActual.getCarId() + " de notificaciones hacia la tabla compra_checkpoints.");
				    	saleOpportunityServices.createSaleOpportunityByCustomerNotification(customerNotificationsActual.getUser().getId(), customerNotificationsActual.getCarId());
				    	
					} catch (Exception e) {
						LogService.logger.info("No se pudo crear Purchase Opportunity para la notificacion del usuario: " + customerNotificationsActual.getUser().getName());
						e.printStackTrace();
					}
					*/
				    
				} catch (Exception e) {
					customerNotificationsActual.setEmailSent(true);
					customerNotificationsRepo.save(customerNotificationsActual);
					LogService.logger.info("Catch Interno correo: No se envio correo de availableCarsNotification al usuario " + customerNotificationsActual.getUser().getName());
					e.printStackTrace();
				}
		    }
		    
		    LogService.logger.info("Total Emails Transaccionales Enviados [" + listCustomerNotificationsBD.size() + "] - availableCarsNotification");
		} catch (Exception e) {
			LogService.logger.info("Catch correo: No se envio correo de availableCarsNotification ");
			e.printStackTrace();
		}
		
		return responseDTO;
    }

    /**
     * Email: offerCheckpointFourthAndFifthStageFollowUp
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointFourthAndFifthStageFollowUp() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "offer_checkpoint_fourth_and_fifth_stage_follow_up.html";
	    String metaKey = Constants.META_VALUE_PHONE;
	    String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findOfferCheckpointFourthAndFifthStageFollowUp();

	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("tower@kavak.com");
			emailCc.add("giovanni.jimenez@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    Long userRegister = Long.valueOf(OfferCheckpointActual.getIdUser()) + 1000L;
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " Felicidades por Agendar una Inspección a tu " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion() + " " + OfferCheckpointActual.getCarYear() + "! Folio " + userRegister);
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    boolean sendEmail = true;
		    String userPhone = null;
		    for (UserMeta userMetaActual : OfferCheckpointActual.getUser().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }

		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

		    Optional<User> userInternalBd = userRepo.findById(OfferCheckpointActual.getAssignedEm());

		    if (userInternalBd.get().getEmail() == null) {
			dataEmail.put(Constants.KEY_EMAIL_INTERNAL_USER_EMAIL, "No Registrado");
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INTERNAL_USER_EMAIL, userInternalBd.get().getEmail());
		    }

		    if (userInternalBd.get().getDTO().getPhone() == null) {
			dataEmail.put(Constants.KEY_EMAIL_INTERNAL_USER_PHONE, "No Registrado");
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INTERNAL_USER_PHONE, userInternalBd.get().getDTO().getPhone());
		    }

		    if (userInternalBd.get().getName() == null) {
			dataEmail.put(Constants.KEY_EMAIL_INTERNAL_USER_NAME, "No Registrado");
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INTERNAL_USER_NAME, userInternalBd.get().getName());
		    }

		    if (sendEmail) {
			try {
			    InternetAddress fromEmail = new InternetAddress("ayuda@kavak.com", userInternalBd.get().getName());

			    boolean emailSend = KavakUtils.sendInternalEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, fromEmail);
			    if (!emailSend) {
				LogService.logger.info("No se envio correo de offerCheckpointFourthAndFifthStageFollowUp  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			    }
			} catch (UnsupportedEncodingException e) {
			    e.printStackTrace();
			}

		    }
		    OfferCheckpointActual.setFollowUpEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setFollowUpEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de offerCheckpointFourthAndFifthStageFollowUp  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - offerCheckpointFourthAndFifthStageFollowUp");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de offerCheckpointFourthAndFifthStageFollowUp ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Escenario 3 no aceptó oferta Kavak
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO offerCheckpointNotAceptedOffer() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "offer_checkpoint_not_acepted_offer.html";
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findOfferCheckpointNotAceptedOffer();
	    Long countOfferCheckpointByUser;
	    Long countOffersByUser;

	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);
	    int sentEmails = 0;

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {

		countOfferCheckpointByUser = offerCheckpointRepo.findCountOfferCheckpointNotAceptedOfferByUser(OfferCheckpointActual.getIdUser());
		countOffersByUser = offerCheckpointRepo.findCountOfferCheckpointNotOfferByUser(OfferCheckpointActual.getIdUser());
		if (countOfferCheckpointByUser == 0L && countOffersByUser == 0L) {
		    try {
			String email = OfferCheckpointActual.getEmail();
			if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			    emailCc.add("it@kavak.com");
			} else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			    emailCc.add("tower@kavak.com");
			    emailCc.add("giovanni.jimenez@kavak.com");
			}

			HashMap<String, Object> subjectData = new HashMap<>();
			subjectData.put(Constants.KEY_SUBJECT, "@PREFIX  Oferta Kavak & " + OfferCheckpointActual.getCarMake() + " " + OfferCheckpointActual.getCarModel() + " " + OfferCheckpointActual.getCarVersion());
			subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
			HashMap<String, Object> dataEmail = new HashMap<>();

			if (OfferCheckpointActual.getCustomerLastName() != null) {
			    if (OfferCheckpointActual.getCustomerName() != null) {
				dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
				dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			    } else {
				dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			    }
			} else {
			    if (OfferCheckpointActual.getCustomerName() != null) {
				dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
				dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, OfferCheckpointActual.getCustomerName());
			    } else {
				dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			    }
			}

			if (OfferCheckpointActual.getCarModel() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
			}

			if (OfferCheckpointActual.getCarMake() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
			}

			if (OfferCheckpointActual.getCarVersion() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
			}

			try {
			    InternetAddress fromEmail = new InternetAddress("andres@kavak.com", "Andres Contreras");
			    String baseEmail = "/templates/base_generic_email.html";
			    boolean emailSend = KavakUtils.sendInternalEmail(email, emailCc, subjectData, templateEmail, baseEmail, dataEmail, fromEmail);
			    if (!emailSend) {
				LogService.logger.info("No se envio correo de offerCheckpointNotAceptedOffer - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			    }
			} catch (UnsupportedEncodingException e) {
			    e.printStackTrace();
			}
		    } catch (Exception e) {
			LogService.logger.info("Catch Interno correo: No se envio correo de offerCheckpointNotAceptedOffer - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			e.printStackTrace();
		    }

		    OfferCheckpointActual.setNotAcceptedOfferEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    sentEmails++;
		}else {
		    OfferCheckpointActual.setNotAcceptedOfferEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual); 
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + sentEmails + "] - offerCheckpointNotAceptedOffer");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de offerCheckpointNotAceptedOffer ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Email de Experiencias de Venta
     * 
     * @author Juan Tolentino
     * @throws MessagingException 
     **/
    public ResponseDTO saleCustomerExperiencie() throws MessagingException {
	ResponseDTO responseDTO = new ResponseDTO();
	String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "sale_customer_experiencie.html";
	    List<SellCarDetail> listSellCarDetailBD = sellCarDetailRepo.findSaleCustomerExperiencie();

	    if (listSellCarDetailBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (SellCarDetail sellCarDetailActual : listSellCarDetailBD) {
		try {
		    Optional<User> userBd = userRepo.findById(sellCarDetailActual.getSellerId());
		    String email = userBd.get().getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("giovanni.jimenez@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ★★★  " + userBd.get().getName() + " Cuéntanos de tu Experiencia de Compra Kavak 😉 ★★★");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (userBd.get().getName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_NAME, userBd.get().getName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
		    }

		    if (sellCarDetailActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, sellCarDetailActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (sellCarDetailActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, sellCarDetailActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (sellCarDetailActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, sellCarDetailActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (sellCarDetailActual.getSellerId() != null) {
			dataEmail.put(Constants.KEY_EMAIL_SELLER_ID, sellCarDetailActual.getSellerId());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_SELLER_ID, "No Registrado");
		    }

		    if (sellCarDetailActual.getId() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_ID, sellCarDetailActual.getId());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_ID, "No Registrado");
		    }

	    dataEmail.put(Constants.ATTACHMENT_FILES, null);
	    
		    boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
		    if (!emailSend) {
			LogService.logger.info("No se envio correo de saleCustomerExperiencie al usuario " + userBd.get().getName());
		    }
		    sellCarDetailActual.setExperienceEmailSent(true);
		    sellCarDetailRepo.save(sellCarDetailActual);
		} catch (Exception e) {
		    sellCarDetailActual.setExperienceEmailSent(true);
		    sellCarDetailRepo.save(sellCarDetailActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de saleCustomerExperiencie al usuario con auto stock id = " + sellCarDetailActual.getId());
		    e.printStackTrace();
		}
	    }
	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listSellCarDetailBD.size() + "] - saleCustomerExperiencie");
	} catch (Exception e) {
	    LogService.logger.info("Catch correo: No se envio correo de saleCustomerExperiencie ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Nueva Oferta Whislist
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO newOfferWhislist() {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "new_offer_whislist.html";
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findNewOfferWhislist();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";

	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡" + OfferCheckpointActual.getCustomerName() + " ¡Kavak tiene una oferta para ti!");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMaxConsignationOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMinConsignationOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMaxInstantOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMinInstantOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMax30DaysOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMin30DaysOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }

		    boolean sendEmail = true;

		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de newOfferWhislist  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setNewOfferEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setNewOfferEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de newOfferWhislist  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - newOfferWhislist");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo correo : No se envio correo de newOfferWhislist ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Nueva Oferta Not Whislist
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO newOfferNotWhislist() {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "new_offer_not_whislist.html";
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findNewOfferNotWhislist();
	    String templateEmailBase= "/templates/new_base_email_customer_template_kavak.html";

	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("team@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX " + OfferCheckpointActual.getCustomerName() + " ¡Kavak tiene una oferta para ti!");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMaxConsignationOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMinConsignationOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMaxInstantOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMinInstantOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMax30DaysOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMin30DaysOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "No Registrado");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }

		    boolean sendEmail = true;

		    if (sendEmail) {
		    	boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
				if (!emailSend) {
				    LogService.logger.info("No se envio correo de newOfferNotWhislist  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
				}
		    }
		    OfferCheckpointActual.setNewOfferEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setNewOfferEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de newOfferNotWhislist  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - newOfferNotWhislist");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo correo : No se envio correo de newOfferNotWhislist ");
	    e.printStackTrace();
	}
	return responseDTO;
    }

    /**
     * Email: Appointment Booked (Cita con reserva)
     * 
     * @author Juan Tolentino
     **/
    @SuppressWarnings({ "unchecked" })
    public ResponseDTO appointmentBooked() {
	ResponseDTO responseDTO = new ResponseDTO();
        InternetAddress fromEmail = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat newSdfFormat = new SimpleDateFormat("dd/MM/yyyy");
        Timestamp appointmentDate = new Timestamp(System.currentTimeMillis());
        String appoinmentDateString = null;
        String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";

	try {	
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findByAppointmentBookedEmailSent();

	    for(SaleCheckpoint SaleCheckpointActual : listSaleCheckpointBD){
		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(SaleCheckpointActual.getCarId());
		Optional<User> userBD = userRepo.findById(SaleCheckpointActual.getUserId());
		HashMap<String,Object> dataEmail = new  HashMap<>();
		 Optional<AppointmentSchedule> appointmentScheduleBD = appointmentScheduleRepo.findById(SaleCheckpointActual.getScheduleDateId());
		try {            
		    Date dateParseAppointment = sdf.parse(appointmentScheduleBD.get().getAppoinmentDate().toString());
		    appoinmentDateString = newSdfFormat.format(dateParseAppointment);
		    appointmentDate.setTime(dateParseAppointment.getTime());
		} catch (ParseException e) {
		    e.printStackTrace();
		}
					
	        Calendar calendarAppoinmentDate = Calendar.getInstance();
	        calendarAppoinmentDate.setTime(appointmentDate);
	        String dayOfAppoinment = KavakUtils.getSpanishDayOfWeek(calendarAppoinmentDate.get(Calendar.DAY_OF_WEEK));
		// Armando data para envio de Email
		dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));
		dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()));
		dataEmail.put(Constants.KEY_EMAIL_CAR_KM, sellCarDetailBD.get().getCarKm());
		dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_DATE, appoinmentDateString);
		dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_HOUR, appointmentScheduleBD.get().getStartDate()+" a "+appointmentScheduleBD.get().getEndDate());
		dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_ADDRESS,  appointmentScheduleBD.get().getAppointmentAddress());
		dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, userBD.get().getEmail());
		String carUrl = KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get());
		if(!carUrl.contains(Constants.URL_HTTP)) {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_URL, KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get())); 
		}else {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_URL, sellCarDetailBD.get().getImageCar()); 
		}
		dataEmail.put(Constants.KEY_EMAIL_URL_KAVAK, Constants.URL_KAVAK + "agendar-cita_"+ sellCarDetailBD.get().getId());
		dataEmail.put(Constants.KEY_EMAIL_CAR_ID, sellCarDetailBD.get().getId());
		dataEmail.put(Constants.KEY_EMAIL_CAR_PLATE_STATE, sellCarDetailBD.get().getPlateState());
		dataEmail.put(Constants.KEY_EMAIL_CAR_VIN, sellCarDetailBD.get().getVin());
		dataEmail.put(Constants.KEY_EMAIL_USER_ID, userBD.get().getId() + 1000L);
		dataEmail.put(Constants.KEY_EMAIL_SOURCE, SaleCheckpointActual.getSource());
		String serialized = sellCarDetailBD.get().getSellCarMeta().getMetaValue();

		if(SaleCheckpointActual.getCustomerLastName() != null){
		    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, SaleCheckpointActual.getCustomerName() + " " + SaleCheckpointActual.getCustomerLastName());
		}else{
		    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, SaleCheckpointActual.getCustomerName());
		}
		
		if(userBD.get().getDTO().getPhone() == null){
		    dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado"); 
		}else{
		    dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userBD.get().getDTO().getPhone());
		}

		if(sellCarDetailBD.get().getCarKm() == null){
		    dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		}

		if(appoinmentDateString == null){
		    dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_DATE, "No Registrado");
		}

		if(appointmentScheduleBD.get().getStartDate() == null && appointmentScheduleBD.get().getEndDate() == null){
		    dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_HOUR, "No Registrado");
		}

		if(appointmentScheduleBD.get().getAppointmentAddress() == null){
		    dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_ADDRESS, "No Registrado");
		}

		if(sellCarDetailBD.get().getId() == null){
		    dataEmail.put(Constants.KEY_EMAIL_CAR_ID, "No Registrado");
		}

		if(sellCarDetailBD.get().getPlateState() == null){
		    dataEmail.put(Constants.KEY_EMAIL_CAR_PLATE_STATE, "No Registrado");
		}

		if(sellCarDetailBD.get().getVin() == null){
		    dataEmail.put(Constants.KEY_EMAIL_CAR_VIN, "No Registrado");
		}

		if(SaleCheckpointActual.getSource() == null){
		    dataEmail.put(Constants.KEY_EMAIL_SOURCE, "iOS");
		}
		

		// En el primer level del hashmap tiene como value otro hashmap que nos referimos a el como hasmap de segundo level
		Map<Object, Map<Object, Object>> mapFirstLvlDeserialized;
		try {
		    mapFirstLvlDeserialized = (Map<Object, Map<Object, Object>>) new SerializedPhpParser(serialized).parse();

		    // Debido a que en PHP la variable esta almacenada de esta manera $main['basic_overview']['transmission'] los valores del primer lvl se obtienen sacando el hashmap 'basic_overview'
		    Map<Object, Object> mapBasicOverview = mapFirstLvlDeserialized.get(Constants.BASIC_OVERVIEW);
		    Object historyReport = mapBasicOverview.get(Constants.HISTORY_REPORT);
		    //LogService.logger.info("History_Report : " + historyReport.toString() );
		    dataEmail.put(Constants.KEY_EMAIL_CAR_REPUVE, historyReport.toString());
		} catch (SerializedPhpParserException e) {
		    e.printStackTrace();
		}

		HashMap<String,Object> subjectData = new HashMap<>();
		subjectData.put(Constants.KEY_SUBJECT,  "@PREFIX " + userBD.get().getDTO().getName().split(" ")[0] + " ¡Tu visita ha sido confirmada! Te esperamos el " + dayOfAppoinment+ " " + calendarAppoinmentDate.get(Calendar.DAY_OF_MONTH));
		subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		subjectData.put("car_year", sellCarDetailBD.get().getCarYear());
		subjectData.put("car_make", sellCarDetailBD.get().getCarMake());
		subjectData.put("car_model", sellCarDetailBD.get().getCarModel());
		subjectData.put("car_trim", SaleCheckpointActual.getCarVersion());
		//Estos usuarios son los del correo customer
		String emailTo= "";
		List<String> emailCc = new ArrayList<String>();
		List<String> emailCcCustomer = new ArrayList<String>();
		if(Constants.PROPERTY_ENVIRONMENT.equals("DEVELOPER")){
		    emailTo= "it@kavak.com";
		}else if(Constants.PROPERTY_ENVIRONMENT.equals("PRODUCTION")){
		    emailTo= "ayuda@kavak.com";
		    emailCcCustomer.add("appointment@kavak.com");
		}
		try {   
		    boolean emailSend = KavakUtils.sendEmail(userBD.get().getEmail(), emailCc, subjectData,"appointment_booked_confirmation_email_template.html", templateEmailBase, dataEmail, null);
		    if (!emailSend) {
			    LogService.logger.info("No se envio correo al cliente de appointmentBooked  - Id Checkpoint [" + SaleCheckpointActual.getId() + "]");
		    }		    
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX Cita con Apartado: " + KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()) +" / "+ userBD.get().getDTO().getName().split(" ")[0] + " / "+ dayOfAppoinment + " " + calendarAppoinmentDate.get(Calendar.DAY_OF_MONTH) +" de "+ appointmentScheduleBD.get().getStartDate()+" a "+appointmentScheduleBD.get().getEndDate());
		    fromEmail = new InternetAddress("hola@kavak.com", "Hola Kavak");
		    boolean emailSendCustomerServices = KavakUtils.sendInternalEmail(emailTo, emailCcCustomer, subjectData,"appointment_booked_confirmation_email_customer_template.html", null,  dataEmail, fromEmail);
		    if (!emailSendCustomerServices) {
			LogService.logger.info("No se envio correo interno de appointmentBooked  - Id Checkpoint [" + SaleCheckpointActual.getId() + "]");
		    }		    
		    SaleCheckpointActual.setAppointmentEmailSent(true);
		    saleCheckpointRepo.save(SaleCheckpointActual);
		} catch (Exception e) {
		    SaleCheckpointActual.setAppointmentEmailSent(true);
		    saleCheckpointRepo.save(SaleCheckpointActual);
		}

	    }
	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listSaleCheckpointBD.size() + "] - appointmentBooked");
	} catch (Exception e) {
	    LogService.logger.error(Constants.LOG_EXECUTING_START+ " [postAppointment] - Error enviando correo Cita con reserva ");
	}
	return responseDTO;
    }
    
    
    /**
     * Email: Appointment Not Booked (Cita sin reserva)
     * 
     * @author Juan Tolentino
     **/
    @SuppressWarnings("unchecked")
    public ResponseDTO appointmentNotBooked() {
	ResponseDTO responseDTO = new ResponseDTO();
	InternetAddress fromEmail = null;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat newSdfFormat = new SimpleDateFormat("dd/MM/yyyy");
	Timestamp appointmentDate = new Timestamp(System.currentTimeMillis());
	String appoinmentDateString = null;
	String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";

	try {	    
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findByAppointmentEmailSent();

	    for(SaleCheckpoint SaleCheckpointActual : listSaleCheckpointBD){
		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(SaleCheckpointActual.getCarId());
		Optional<User> userBD = userRepo.findById(SaleCheckpointActual.getUserId());
		Optional<AppointmentSchedule> appointmentScheduleBD = appointmentScheduleRepo.findById(SaleCheckpointActual.getScheduleDateId());
		HashMap<String,Object> dataEmail = new  HashMap<>();
		try {            
		    Date dateParseAppointment = sdf.parse(appointmentScheduleBD.get().getAppoinmentDate().toString());
		    appoinmentDateString = newSdfFormat.format(dateParseAppointment);
		    appointmentDate.setTime(dateParseAppointment.getTime());
		} catch (ParseException e) {
		    e.printStackTrace();
		}

		Calendar calendarAppoinmentDate = Calendar.getInstance();
		calendarAppoinmentDate.setTime(appointmentDate);
	        String dayOfAppoinment = KavakUtils.getSpanishDayOfWeek(calendarAppoinmentDate.get(Calendar.DAY_OF_WEEK));
		calendarAppoinmentDate.setTime(appointmentDate);

		// Armando data para envio de Email
		dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));
		dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()));
		dataEmail.put(Constants.KEY_EMAIL_CAR_KM, sellCarDetailBD.get().getCarKm());
		dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_DATE, appoinmentDateString);
		dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_HOUR, appointmentScheduleBD.get().getStartDate()+" a "+appointmentScheduleBD.get().getEndDate());
		dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_ADDRESS, appointmentScheduleBD.get().getAppointmentAddress());
		String carUrl = KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get());
		if(!carUrl.contains(Constants.URL_HTTP)) {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_URL, KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get())); 
		}else {
		    dataEmail.put(Constants.KEY_EMAIL_CAR_URL, sellCarDetailBD.get().getImageCar()); 
		}
		dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, userBD.get().getEmail());
		dataEmail.put(Constants.KEY_EMAIL_CAR_URL, KavakUtils.getFriendlyCarUrl(sellCarDetailBD.get()));
		dataEmail.put(Constants.KEY_EMAIL_URL_KAVAK, Constants.URL_KAVAK + "agendar-cita_"+ sellCarDetailBD.get().getId());
		dataEmail.put(Constants.KEY_EMAIL_CAR_ID, sellCarDetailBD.get().getId());
		dataEmail.put(Constants.KEY_EMAIL_CAR_PLATE_STATE, sellCarDetailBD.get().getPlateState());
		dataEmail.put(Constants.KEY_EMAIL_CAR_VIN, sellCarDetailBD.get().getVin());
		dataEmail.put(Constants.KEY_EMAIL_USER_ID, userBD.get().getId() + 1000L);
		dataEmail.put(Constants.KEY_EMAIL_SOURCE, SaleCheckpointActual.getSource());
		String serialized = sellCarDetailBD.get().getSellCarMeta().getMetaValue();
		
		if(SaleCheckpointActual.getCustomerLastName() != null){
		    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, SaleCheckpointActual.getCustomerName() + " " + SaleCheckpointActual.getCustomerLastName());
		}else{
		    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, SaleCheckpointActual.getCustomerName());
		}
		
		if(userBD.get().getDTO().getPhone() == null){
		    dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado"); 
		}else{
		    dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userBD.get().getDTO().getPhone());
		}

		if(sellCarDetailBD.get().getCarKm() == null){
		    dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		}

		if(appoinmentDateString == null){
		    dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_DATE, "No Registrado");
		}

		if(appointmentScheduleBD.get().getStartDate() == null && appointmentScheduleBD.get().getEndDate() == null){
		    dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_HOUR, "No Registrado");
		}

		if(appointmentScheduleBD.get().getAppointmentAddress() == null){
		    dataEmail.put(Constants.KEY_EMAIL_APPOINTMENT_ADDRESS, "No Registrado");
		}

		if(sellCarDetailBD.get().getId() == null){
		    dataEmail.put(Constants.KEY_EMAIL_CAR_ID, "No Registrado");
		}

		if(sellCarDetailBD.get().getPlateState() == null){
		    dataEmail.put(Constants.KEY_EMAIL_CAR_PLATE_STATE, "No Registrado");
		}

		if(sellCarDetailBD.get().getVin() == null){
		    dataEmail.put(Constants.KEY_EMAIL_CAR_VIN, "No Registrado");
		}                          


		// En el primer level del hashmap tiene como value otro hashmap que nos referimos a el como hasmap de segundo level
		Map<Object, Map<Object, Object>> mapFirstLvlDeserialized;
		try {
		    mapFirstLvlDeserialized = (Map<Object, Map<Object, Object>>) new SerializedPhpParser(serialized).parse();

		    // Debido a que en PHP la variable esta almacenada de esta manera $main['basic_overview']['transmission'] los valores del primer lvl se obtienen sacando el hashmap 'basic_overview'
		    Map<Object, Object> mapBasicOverview = mapFirstLvlDeserialized.get(Constants.BASIC_OVERVIEW);
		    Object historyReport = mapBasicOverview.get(Constants.HISTORY_REPORT);
		    //LogService.logger.info("History_Report : " + historyReport.toString() );
		    dataEmail.put(Constants.KEY_EMAIL_CAR_REPUVE, historyReport.toString());
		} catch (SerializedPhpParserException e) {
		    e.printStackTrace();
		}

		// Se envia suma la fecha de appointmentDate al calendario para obtener que dia de semana es la cita

		// dateAppoinment.get(Calendar.DAY_OF_WEEK);

		HashMap<String,Object> subjectData = new HashMap<>();
		//       subjectData.put(Constants.KEY_SUBJECT, "Registro de visita para ver nuestro auto Kavak <car_year car_make car_model car_trim>");
		subjectData.put(Constants.KEY_SUBJECT,  "@PREFIX " + userBD.get().getDTO().getName().split(" ")[0] + " ¡Tu visita ha sido confirmada! Te esperamos el "+ dayOfAppoinment + " " + calendarAppoinmentDate.get(Calendar.DAY_OF_MONTH));
		subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		subjectData.put("car_year", sellCarDetailBD.get().getCarYear());
		subjectData.put("car_make", sellCarDetailBD.get().getCarMake());
		subjectData.put("car_model", sellCarDetailBD.get().getCarModel());
		subjectData.put("car_trim", SaleCheckpointActual.getCarVersion());
		String emailTo= "ayuda@kavak.com";
		List<String> emailCc = new ArrayList<String>();
		List<String> emailCcCustomer = new ArrayList<String>();
		if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)){
		    emailCcCustomer.add("it@kavak.com");
		}else if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)){
		    emailCcCustomer.add("appointment@kavak.com");
		}
		try {
		    boolean emailSend = KavakUtils.sendEmail(userBD.get().getEmail(), emailCc, subjectData,"appointment_confirmation_email_template.html", templateEmailBase, dataEmail, null);
		    if (!emailSend) {
			LogService.logger.info("No se envio correo al cliente de appointmentNotBooked  - Id Checkpoint [" + SaleCheckpointActual.getId() + "]");
		    }
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX Cita sin Apartado: " + KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()) +" / "+ userBD.get().getDTO().getName().split(" ")[0] + " / "+ dayOfAppoinment + " " + calendarAppoinmentDate.get(Calendar.DAY_OF_MONTH) +" de "+ appointmentScheduleBD.get().getStartDate()+" a "+appointmentScheduleBD.get().getEndDate());
		    fromEmail = new InternetAddress("hola@kavak.com", "Hola Kavak");
		    boolean emailSendCustomerServices = KavakUtils.sendInternalEmail(emailTo, emailCcCustomer, subjectData,"appointment_confirmation_email_customer_template.html", null, dataEmail, fromEmail);
		    if (!emailSendCustomerServices) {
			LogService.logger.info("No se envio correo interno de appointmentNotBooked  - Id Checkpoint [" + SaleCheckpointActual.getId() + "]");
		    }
		    SaleCheckpointActual.setAppointmentEmailSent(true);
		    saleCheckpointRepo.save(SaleCheckpointActual);
		} catch (Exception e) {
		    SaleCheckpointActual.setAppointmentEmailSent(true);
		    saleCheckpointRepo.save(SaleCheckpointActual);
		}
	    }
	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listSaleCheckpointBD.size() + "] - appointmentNotBooked");
	} catch (Exception e) {
	    LogService.logger.error(Constants.LOG_EXECUTING_START+ " [postAppointment] - Error enviando correo Cita sin reserva");
	}
	return responseDTO;
    }


    /**
     * Email: CarsCout App (cuando se crea una alerta desde la app)
     * 
     * @author Juan Tolentino
     **/
    @SuppressWarnings("unchecked")
    public ResponseDTO carscoutEmailSent() {
	ResponseDTO responseDTO = new ResponseDTO();
	String metaKey = Constants.META_VALUE_PHONE;
	String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";
	try {	    
	    List<CarscoutAlert> listCarscoutAlertBD = carscoutAlertRepo.findByCarscoutAlertEmailSent();	    
	    if(!listCarscoutAlertBD.isEmpty()) {
		for(CarscoutAlert carscoutAlertActual : listCarscoutAlertBD){
		    ResponseDTO filters = new ResponseDTO();
		    List<GetCarscoutResultsResponseDTO> getCarscoutResultList = new ArrayList<>();

		    filters = carscoutServices.getCarscoutAlert(carscoutAlertActual.getUser().getId(), carscoutAlertActual.getId());
		    Optional<User> userBD = userRepo.findById(carscoutAlertActual.getUser().getId());

		    getCarscoutResultList =  (List<GetCarscoutResultsResponseDTO>) filters.getData();
		    for(GetCarscoutResultsResponseDTO filterActual : getCarscoutResultList) {
			StringBuilder emailFiltersCarscout = new StringBuilder();
			for(GenericDTO filter : filterActual.getFilters()) {
			    emailFiltersCarscout.append("<b style=\"color:#151112;\">"+filter.getDescription().replace(":",":</b>")+" <br>");
			}		
			HashMap<String,Object> dataEmail = new  HashMap<>();
			dataEmail.put(Constants.KEY_EMAIL_GENERIC, emailFiltersCarscout);

			if(userBD.get().getName() != null){
			    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, userBD.get().getName());
			}

			String userPhone = null;
			for (UserMeta userMetaActual : userBD.get().getListUserMeta()) {
			    if (userMetaActual.getMetaKey().equals(metaKey)) {
				userPhone = userMetaActual.getMetaValue();
			    }
			}
			if (userPhone != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
			}

			if (userBD.get().getEmail() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, userBD.get().getEmail());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
			}		    

			HashMap<String,Object> subjectData = new HashMap<>();
			subjectData.put(Constants.KEY_SUBJECT,  "@PREFIX Alertas en BUSCAR AUTOS EN VENTA (Carscout)");
			subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
			List<String> emailCc = new ArrayList<String>();
			//List<String> emailCcCustomer = new ArrayList<String>();
			if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)){
			    emailCc.add("it@kavak.com");
			}else if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)){
			    emailCc.add("team@kavak.com");
			}
			try {
			    boolean emailSend = KavakUtils.sendEmail(userBD.get().getEmail(), emailCc, subjectData,"carscout_alert_email.html", templateEmailBase, dataEmail, null);
			    if (!emailSend) {
				LogService.logger.info("No se envio correo al cliente de carscoutAlert  - Id Checkpoint [" + carscoutAlertActual.getId() + "]");
			    }
			    carscoutAlertActual.setCustomerEmailSent(true);
			    carscoutAlertRepo.save(carscoutAlertActual);
			} catch (Exception e) {
			    carscoutAlertActual.setCustomerEmailSent(true);
			    carscoutAlertRepo.save(carscoutAlertActual);
			}
		    }
		}
	    }
	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listCarscoutAlertBD.size() + "] - CarscoutAlert");
	} catch (Exception e) {
	    LogService.logger.error(Constants.LOG_EXECUTING_START+ " [CarscoutAlert] - Error enviando correo de carscout");
	}
	return responseDTO;
    }

       
    
    /**
     * Email: Arbol Financiero Score
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO financingScoreEmailSent() {
	ResponseDTO responseDTO = new ResponseDTO();
	String metaKey = Constants.META_VALUE_PHONE;
	try {	    
	    String patternNumber = "$###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    
	    List<FinancingUserScore> listFinancingUserScoreBD = financingUserScoreRepo.findByScoreEmailSentFalse();	    
	    if(!listFinancingUserScoreBD.isEmpty()) {
		for(FinancingUserScore financingUserScoreActual : listFinancingUserScoreBD){
		    FinancingUser FinancingUserBD = financingUserRepo.findByFinancingUserId(financingUserScoreActual.getFinancingUserId()); 
		    Optional<User> userBD = userRepo.findById(FinancingUserBD.getUserId());
		    FinancingUserIncome financingUserIncomeBD = financingUserIncomeRepo.findByFinancingIdAndActiveTrue(financingUserScoreActual.getFinancingUserId());

		    StringBuilder emailFiltersCarscout = new StringBuilder();
		    if(financingUserScoreActual.getCarId() != null) {
			Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(financingUserScoreActual.getCarId());
			emailFiltersCarscout.append("<br><b style=\"color:#151112;\"> AUTO INTERÉS</b><br><br>");
			emailFiltersCarscout.append("<b style=\"color:#151112;\"> Stock id: "+sellCarDetailBD.get().getId()+"</b><br>");
			if(sellCarDetailBD.get().getCarTrim() != null) {
			    emailFiltersCarscout.append("<b style=\"color:#151112;\"> Auto: "+sellCarDetailBD.get().getCarMake() +" "+sellCarDetailBD.get().getCarModel()+" "+sellCarDetailBD.get().getCarTrim().split("##")[0]+" "+sellCarDetailBD.get().getCarYear()+"</b><br>");
			}else {
			    emailFiltersCarscout.append("<b style=\"color:#151112;\"> Auto: "+sellCarDetailBD.get().getCarMake() +" "+sellCarDetailBD.get().getCarModel()+sellCarDetailBD.get().getCarYear()+"</b><br>");
			}	    
			emailFiltersCarscout.append("<b style=\"color:#151112;\"> Precio "+sellCarDetailBD.get().getPrice()+"</b><br>");
		    }
		    HashMap<String,Object> dataEmail = new  HashMap<>();
		    dataEmail.put(Constants.KEY_EMAIL_GENERIC, emailFiltersCarscout);

		    if(userBD.get().getName() != null){
			dataEmail.put(Constants.KEY_EMAIL_USER_NAME, userBD.get().getName());
		    }

		    String userPhone = null;
		    for (UserMeta userMetaActual : userBD.get().getListUserMeta()) {
			if (userMetaActual.getMetaKey().equals(metaKey)) {
			    userPhone = userMetaActual.getMetaValue();
			}
		    }
		    if (userPhone != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, userPhone);
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, "No Registrado");
		    }

		    if (userBD.get().getEmail() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, userBD.get().getEmail());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_URL_BURO, FinancingUserBD.getUrlBuro());
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_TEXT_SCORE, financingUserScoreActual.getScoreText());
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_SCORE, financingUserScoreActual.getScore());
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_MAXIMUN_FINANCING_AMOUNT, financingUserScoreActual.getMaximumFinancingAmount());
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_PREAPPROVED_AMOUNT, financingUserScoreActual.getPreapprovedAmount());
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_MONTHLY_PAYMENT, financingUserScoreActual.getMonthlyPayment());
//		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_MAXIMUN_FINANCING_AMOUNT, decimalFormat.format(Long.valueOf(financingUserScoreActual.getMaximumFinancingAmount().replaceAll(".", ""))));
//		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_PREAPPROVED_AMOUNT, decimalFormat.format(Long.valueOf(financingUserScoreActual.getPreapprovedAmount().replaceAll(".", ""))));
//		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_MONTHLY_PAYMENT, decimalFormat.format(Long.valueOf(financingUserScoreActual.getMonthlyPayment().replaceAll(".", ""))));
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_USER_NAME, FinancingUserBD.getFullName() +" "+ FinancingUserBD.getFirstName());
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_USER_EMAIL, FinancingUserBD.getEmail());
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_USER_PHONE, FinancingUserBD.getPhone());
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING_DOWN_PAYMENT, financingUserIncomeBD.getAutomotiveDownPayment());
		     
		    
		    //falta uno Enganche dado por el cliente: (indicado por el cliente)



		    HashMap<String,Object> subjectData = new HashMap<>();
		    subjectData.put(Constants.KEY_SUBJECT,  "@PREFIX Score para Financinamiento");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    List<String> emailCcCustomer = new ArrayList<String>();
		    if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)){
			emailCcCustomer.add("it@kavak.com");
		    }else if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)){
			emailCcCustomer.add("team@kavak.com");
		    }
		    try {
			boolean emailSend = KavakUtils.sendInternalEmail(userBD.get().getEmail(), emailCcCustomer, subjectData,"financing_score_email_template.html", null, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo al cliente de Score Arbol Financiero  - Id Score [" + financingUserScoreActual.getId() + "]");
			}
			financingUserScoreActual.setScoreEmailSent(true);
			financingUserScoreRepo.save(financingUserScoreActual);
		    } catch (Exception e) {
			financingUserScoreActual.setScoreEmailSent(true);
			financingUserScoreRepo.save(financingUserScoreActual);
		    }
		}
	    }
	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listFinancingUserScoreBD.size() + "] - Score Arbol Financiero");
	} catch (Exception e) {
	    LogService.logger.error(Constants.LOG_EXECUTING_START+ " [Score Arbol Financiero] - Error enviando correo de Score Arbol Financiero");
	}
	return responseDTO;
    }

       
    
    
    
    /**
     * Consulta los centros de inspeccion en México
     * 
     * @author Juan Tolentino
     * @return
     * @throws MessagingException 
     */
    public ResponseDTO sendBookedEmail() throws MessagingException {
	String templateEmailBase = "/templates/new_base_email_customer_template_kavak.html";
	ResponseDTO responseDTO = new ResponseDTO();
	    
	try {

	    MetaValue metaValueBOOKINGPRICE = metaValueRepo.findByAlias(Constants.BOOKING_PRICE);
	    List<SaleCheckpoint> listSaleCheckpointBD = saleCheckpointRepo.findBySendBookedEmail();
	    for(SaleCheckpoint saleCheckpointActual : listSaleCheckpointBD) {
		Optional<User> userBD = userRepo.findById(saleCheckpointActual.getUserId());
		Optional<SellCarDetail> sellCarDetailBD = sellCarDetailRepo.findById(saleCheckpointActual.getCarId());

		// ENNVIO DE EMAIL POR CHECKOUT
		HashMap<String, Object> subjectData = new HashMap<>();
		subjectData.put(Constants.KEY_SUBJECT, "@PREFIX ¡Has realizado un apartado en Kavak por un auto @CAR_NAME!");
		subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		subjectData.put(Constants.KEY_EMAIL_CAR_NAME, KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()));
		String templateEmail = "";

		// Data que se envia como par (key ,valor) para las plantillas de los
		// email
		HashMap<String, Object> dataEmail = new HashMap<>();
		// Data de la plantilla base
		dataEmail.put(Constants.KEY_EMAIL_USER_NAME, KavakUtils.getFriendlyUserName(userBD.get().getDTO()));
		dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));
		dataEmail.put(Constants.KEY_EMAIL_CAR_NAME, KavakUtils.getFriendlyCarName(sellCarDetailBD.get().getDTO()));

		String patternNumber = "$###,###,###";
		String patternNumberKM = "###,###,###";

		NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
		DecimalFormat decimalFormat = (DecimalFormat) nf;
		DecimalFormat decimalFormatKM = (DecimalFormat) nf;
		decimalFormat.applyPattern(patternNumber);
		decimalFormatKM.applyPattern(patternNumberKM);

		// Data de las plantillas Internas
		dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(sellCarDetailBD.get().getCarKm())));
		dataEmail.put(Constants.KEY_EMAIL_CAR_PRICE, decimalFormat.format(sellCarDetailBD.get().getPrice()));
		dataEmail.put(Constants.KEY_EMAIL_BOOKING_PRICE, decimalFormatKM.format(Long.valueOf(metaValueBOOKINGPRICE.getOptionName())));
		dataEmail.put(Constants.KEY_EMAIL_SOURCE, saleCheckpointActual.getSource());
		// Garantia
		if (Long.valueOf(saleCheckpointActual.getTypePaymentMethodTypeId()) != 144L) {
		    if (saleCheckpointActual.getWarrantyName()==null || saleCheckpointActual.getWarrantyName().equalsIgnoreCase("Incluída")) {
			dataEmail.put(Constants.KEY_EMAIL_WARRANTY, "Incluída");
		    } else { 
			dataEmail.put(Constants.KEY_EMAIL_WARRANTY, saleCheckpointActual.getWarrantyName() + " - " + saleCheckpointActual.getWarrantyRange() + " - " + decimalFormat.format(saleCheckpointActual.getWarrantyAmount()) + " MXN");
		    }
		}

		// Seguro
		if (Long.valueOf(saleCheckpointActual.getTypePaymentMethodTypeId()) == 141L) { // Solo
		    // Contado
		    if (saleCheckpointActual.getInsuranceName()==null || saleCheckpointActual.getInsuranceName().equalsIgnoreCase("No deseo seguro")) {
			dataEmail.put(Constants.KEY_EMAIL_INSURANCE, "No deseo seguro");
		    } else { 
			dataEmail.put(Constants.KEY_EMAIL_INSURANCE, saleCheckpointActual.getInsuranceName() + " - " + saleCheckpointActual.getInsuranceModality() + " - " + decimalFormat.format(Long.valueOf(saleCheckpointActual.getInsuranceAmount())) + " MXN");
		    }
		}

		Long amount = Long.valueOf(saleCheckpointActual.getCarCost()) - Long.valueOf(metaValueBOOKINGPRICE.getOptionName());
		if (Long.valueOf(saleCheckpointActual.getTypePaymentMethodTypeId()) == 143L) {
		    dataEmail.put(Constants.KEY_EMAIL_FINANCING, decimalFormat.format(amount) + " MXN / " + saleCheckpointActual.getMonthFinancing() + " Meses");
		}

		// >>>> Pendiente comentar por que para el caso 143 no se manda esta !=
		// info
		//	if (!(Long.valueOf(saleCheckpointDTO.getPaymentMethodTypeId())).equals(Long.valueOf(143))) {
		//	    if(responseChargeOpenPayDTO.getBankCharge() != null) {
		//		dataEmail.put(Constants.KEY_EMAIL_AGREEMENT, responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getReference());
		//		dataEmail.put(Constants.KEY_EMAIL_REFERENCE, responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getName());
		//		dataEmail.put(Constants.KEY_EMAIL_CLABE, responseChargeOpenPayDTO.getBankCharge().getPaymentMethod().getClabe());
		//		dataEmail.put(Constants.KEY_EMAIL_TRANSFER_AMOUNT, amount);
		//		dataEmail.put(Constants.KEY_EMAIL_PAYMENT_DESCRIPTION, responseChargeOpenPayDTO.getBankCharge().getDescription());
		//		dataEmail.put(Constants.KEY_EMAIL_URL_RECEIPT, Constants.PROPERTY_EMAIL_URL_RECEIPT + Constants.OPENPAY_MERCHANTID + "/" + responseChargeOpenPayDTO.getBankCharge().getId());
		//	    }
		//	}

		dataEmail.put(Constants.ATTACHMENT_FILES, null);

		if (saleCheckpointActual.getSource() == null) {
		    dataEmail.put(Constants.KEY_EMAIL_SOURCE, "iOS");
		}

		switch (saleCheckpointActual.getTypePaymentMethodTypeId().toString()) {
		case "141":
		    templateEmail = "checkout_confirmation_cash_payment.html";
		    break;
		case "143":
		    templateEmail = "checkout_confirmation_financing.html";
		    break;
		case "144":
		    templateEmail = "checkout_confirmation_booking_only.html";
		    break; // Reserva 72H
		}

		List<String> emailCc = new ArrayList<String>();

		if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
		    emailCc.add("it@kavak.com");
		} else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
		    emailCc.add("team@kavak.com");
		}

		boolean emailSend = KavakUtils.sendEmail(userBD.get().getEmail(), emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
		LogService.logger.info(Constants.LOG_EXECUTING_START + " [putCheckout] - sendEmail retorno " + emailSend);

		saleCheckpointActual.setBookedEmailSent(true);
		saleCheckpointRepo.save(saleCheckpointActual);
		
	    }
	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listSaleCheckpointBD.size() + "] - BookedEmailSent");

	} catch (Exception e) {
	    e.printStackTrace();
	}

	return responseDTO;
    }
    
    
    
    public ApiUrlShortenerResponseDTO postApiUrlShortener(String url) {
	ApiUrlShortenerResponseDTO apiUrlShortenerResponseDTO = new ApiUrlShortenerResponseDTO();
	String getUrl = "https://api-ssl.bitly.com/v3/shorten?access_token="+Constants.API_KEY_URL_SHORTENER+"&longUrl="+url;
	RestTemplate restTemplate = new RestTemplate();
	
	try {
	    apiUrlShortenerResponseDTO = restTemplate.getForObject(getUrl, ApiUrlShortenerResponseDTO.class, 200);
	    if(apiUrlShortenerResponseDTO.getData().getUrl().trim().contains("undefined")) {
		apiUrlShortenerResponseDTO.getData().setUrl(url);
		LogService.logger.error("Acortar Url dio error :"+ apiUrlShortenerResponseDTO.getData().getUrl()); 
	    }
	} catch (Exception ex) {
	    LogService.logger.info("Catch Respuesta postApiUrlShortener :"+ ex); 
	}
	return apiUrlShortenerResponseDTO;
    }
    
    
    
    
    /**
     * Email: contactFormSubmitEmailSent
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO contactFormSubmitEmailSent() {
	ResponseDTO responseDTO = new ResponseDTO();

	try {	    
	    List<ContactUs> listContactUsBD = contactUsRepo.findByInternalEmailSentFalse();	    
	    if(!listContactUsBD.isEmpty()) {
		for(ContactUs contactUsActual : listContactUsBD){

		    HashMap<String, Object> dataEmail = new HashMap<>();

		    dataEmail.put(Constants.KEY_EMAIL_USER_NAME, contactUsActual.getNames());
		    dataEmail.put(Constants.KEY_EMAIL_USER_EMAIL, contactUsActual.getEmail());
		    dataEmail.put(Constants.KEY_EMAIL_USER_PHONE, contactUsActual.getPhone());
		    dataEmail.put(Constants.KEY_SUBJECT, contactUsActual.getSubjectId());
		    dataEmail.put(Constants.KEY_EMAIL_USER_ZIPCODE, contactUsActual.getZipCode());
		    dataEmail.put(Constants.KEY_EMAIL_USER_COMMENTS, contactUsActual.getComments());
		    dataEmail.put(Constants.KEY_EMAIL_ID_REGISTER, contactUsActual.getId());
		    dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, contactUsActual.getLastNames());

		    HashMap<String,Object> subjectData = new HashMap<>();
		    subjectData.put(Constants.KEY_SUBJECT,  "@PREFIX Registro de usuario en contáctanos");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    String emailTo = new String();
		    List<String> emailCcCustomer = new ArrayList<String>();
		    if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)){
			emailCcCustomer.add("it@kavak.com");
			emailTo = "angie@kavak.com";
		    }else if(Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)){
			emailCcCustomer.add("ayuda@kavak.com");
			emailCcCustomer.add("angie@kavak.com");
			emailTo = "team@kavak.com";
		    }
		    try {
			boolean emailSend = KavakUtils.sendInternalEmail(emailTo, emailCcCustomer, subjectData,"contact_form_submit.html", null, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo  - Id contactUs [" + contactUsActual.getId()+"]");
			}
			contactUsActual.setInternalEmailSent(true);
			contactUsRepo.save(contactUsActual);
		    } catch (Exception e) {
			contactUsActual.setInternalEmailSent(true);
			contactUsRepo.save(contactUsActual);
		    }
		}
	    }
	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listContactUsBD.size() + "] - contactFormSubmitEmailSent");
	} catch (Exception e) {
	    LogService.logger.error(Constants.LOG_EXECUTING_START+ " [contactFormSubmitEmailSent] - Error enviando correo de contactFormSubmitEmailSent");
	}
	return responseDTO;
    }
    
    
        
    
    /**
     * Email: Nueva Oferta Mejorada de auditoria Whislist
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO newOfferAuditWhislist() {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "new_offer_audit_whislist.html";
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findNewOfferAuditWhislist();
	    String templateEmailBase= "/templates/base_internal_email_white.html";

	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("aneyda.cervantes@kavak.com");
			emailCc.add("janette.gonzalez@kavak.com");
			emailCc.add("angela.delduca@kavak.com");
			emailCc.add("andres@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX " + OfferCheckpointActual.getCustomerName() + ", mejoramos la oferta de tu auto");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMaxConsignationOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMinConsignationOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMaxInstantOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMinInstantOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMax30DaysOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMin30DaysOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }

		    boolean sendEmail = true;

		    if (sendEmail) {
			boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
			if (!emailSend) {
			    LogService.logger.info("No se envio correo de newOfferAuditWhislist  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
			}
		    }
		    OfferCheckpointActual.setNewOfferAuditEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setNewOfferAuditEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de newOfferAuditWhislist  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - newOfferAuditWhislist");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo correo : No se envio correo de newOfferAuditWhislist ");
	    e.printStackTrace();
	}
	return responseDTO;
    }
    
    
    
    /**
     * Email: Nueva Oferta Mejorada de Auditoria Not Whislist
     * 
     * @author Juan Tolentino
     **/
    public ResponseDTO newOfferAuditNotWhislist() {
	ResponseDTO responseDTO = new ResponseDTO();
	try {
	    List<String> emailCc = new ArrayList<>();
	    String templateEmail = "new_offer_audit_not_whislist.html";
	    List<OfferCheckpoint> listOfferCheckpointBD = offerCheckpointRepo.findNewOfferAuditNotWhislist();
	    String templateEmailBase= "/templates/base_internal_email_white.html";

	    if (listOfferCheckpointBD.isEmpty()) {
		return responseDTO;
	    }

	    String patternNumber = "$###,###,###";
	    String patternNumberKM = "###,###,###";

	    NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	    DecimalFormat decimalFormat = (DecimalFormat) nf;
	    DecimalFormat decimalFormatKM = (DecimalFormat) nf;
	    decimalFormat.applyPattern(patternNumber);
	    decimalFormatKM.applyPattern(patternNumberKM);

	    for (OfferCheckpoint OfferCheckpointActual : listOfferCheckpointBD) {
		try {
		    String email = OfferCheckpointActual.getEmail();
		    if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			emailCc.add("it@kavak.com");
		    } else if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_PRODUCTION)) {
			emailCc.add("aneyda.cervantes@kavak.com");
			emailCc.add("janette.gonzalez@kavak.com");
			emailCc.add("angela.delduca@kavak.com");
			emailCc.add("andres@kavak.com");
		    }

		    HashMap<String, Object> subjectData = new HashMap<>();
		    subjectData.put(Constants.KEY_SUBJECT, "@PREFIX " + OfferCheckpointActual.getCustomerName() + ", mejoramos la oferta de tu auto");
		    subjectData.put(Constants.KEY_EMAIL_PREFIX, Constants.PROPERTY_EMAIL_PREFIX);
		    HashMap<String, Object> dataEmail = new HashMap<>();

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, OfferCheckpointActual.getCustomerName() + " " + OfferCheckpointActual.getCustomerLastName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, "No Registrado");
			}
		    } else {
			if (OfferCheckpointActual.getCustomerName() != null) {
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, OfferCheckpointActual.getCustomerName());
			} else {
			    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, "No Registrado");
			}
		    }

		    if (OfferCheckpointActual.getCustomerLastName() != null) {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, OfferCheckpointActual.getCustomerLastName());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_USER_LAST_NAME, "No Registrado");
		    }

		    dataEmail.put(Constants.KEY_EMAIL_CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));

		    if (OfferCheckpointActual.getCarMake() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, OfferCheckpointActual.getCarMake());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MAKE, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarModel() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, OfferCheckpointActual.getCarModel());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_MODEL, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarKm() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getCarKm())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_KM, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarYear() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, OfferCheckpointActual.getCarYear());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_YEAR, "No Registrado");
		    }

		    if (OfferCheckpointActual.getCarVersion() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, OfferCheckpointActual.getCarVersion());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CAR_VERSION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getMaxConsignationOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMaxConsignationOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinConsignationOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMinConsignationOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_CONSIGNMENT_OFFER_MIN, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMaxInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMaxInstantOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MAX, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMinInstantOffer() != null && (OfferCheckpointActual.getAbTestResult() == null || OfferCheckpointActual.getAbTestResult() == 0)) {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMinInstantOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_INSTANT_OFFER_MIN, "");
		    }

		    if (OfferCheckpointActual.getMax30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMax30DaysOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MAX_30_DAYS, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getMin30DaysOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, decimalFormatKM.format(Long.valueOf(OfferCheckpointActual.getMin30DaysOffer())));
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_MIN_30_DAYS, "NO APLICA");
		    }

		    if (OfferCheckpointActual.getDescriptionOfferType() != null) {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, OfferCheckpointActual.getDescriptionOfferType());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_OFFER_INSPECTION, "No Registrado");
		    }

		    if (OfferCheckpointActual.getUrlRecoverOffer() != null) {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, OfferCheckpointActual.getUrlRecoverOffer());
		    } else {
			dataEmail.put(Constants.KEY_EMAIL_URL_RECOVER_OFFER, "No Registrado");
		    }

		    boolean sendEmail = true;

		    if (sendEmail) {
		    	boolean emailSend = KavakUtils.sendEmail(email, emailCc, subjectData, templateEmail, templateEmailBase, dataEmail, null);
				if (!emailSend) {
				    LogService.logger.info("No se envio correo de newOfferAuditNotWhislist  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
				}
		    }
		    OfferCheckpointActual.setNewOfferAuditEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		} catch (Exception e) {
		    OfferCheckpointActual.setNewOfferAuditEmailSent(true);
		    offerCheckpointRepo.save(OfferCheckpointActual);
		    LogService.logger.info("Catch Interno correo: No se envio correo de newOfferAuditNotWhislist  - Id Oferta [" + OfferCheckpointActual.getId() + "]");
		    e.printStackTrace();
		}
	    }

	    LogService.logger.info("Total Emails Transaccionales Enviados [" + listOfferCheckpointBD.size() + "] - newOfferAuditNotWhislist");
	} catch (Exception e) {
	    LogService.logger.info("Catch Externo correo : No se envio correo de newOfferAuditNotWhislist ");
	    e.printStackTrace();
	}
	return responseDTO;
    }
    
    
}
