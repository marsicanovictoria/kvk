package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateDomicileApiResponseDTO {

    private FinancingUserUpdateDomicileApiDataResponseDTO data;

    
    @JsonProperty("data")
    public FinancingUserUpdateDomicileApiDataResponseDTO getData() {
        return data;
    }

    public void setData(FinancingUserUpdateDomicileApiDataResponseDTO data) {
        this.data = data;
    }

}