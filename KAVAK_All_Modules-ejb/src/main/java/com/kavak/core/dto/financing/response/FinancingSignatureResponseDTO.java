package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingSignatureResponseDTO {

	private String signature;


	@JsonProperty("signature")
	public String getSignature() {
	    return signature;
	}

	public void setSignature(String signature) {
	    this.signature = signature;
	}

}