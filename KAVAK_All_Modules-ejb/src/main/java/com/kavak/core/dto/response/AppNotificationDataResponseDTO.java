package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppNotificationDataResponseDTO {
	
	private String carId;
	private String carUrl;
	private String appointmentDate;
	private String appointmentTime;
	private String appointmentAddress;
	private String NotificationCode;

	@JsonProperty("car_id")
	public String getCarId() {
	    return carId;
	}
	public void setCarId(String carId) {
	    this.carId = carId;
	}
	
	@JsonProperty("car_url")
	public String getCarUrl() {
	    return carUrl;
	}
	public void setCarUrl(String carUrl) {
	    this.carUrl = carUrl;
	}

	@JsonProperty("appointment_date")
	public String getAppointmentDate() {
	    return appointmentDate;
	}
	public void setAppointmentDate(String appointmentDate) {
	    this.appointmentDate = appointmentDate;
	}
	
	@JsonProperty("appointment_time")
	public String getAppointmentTime() {
	    return appointmentTime;
	}
	public void setAppointmentTime(String appointmentTime) {
	    this.appointmentTime = appointmentTime;
	}
	
	@JsonProperty("appointment_address")
	public String getAppointmentAddress() {
	    return appointmentAddress;
	}
	public void setAppointmentAddress(String appointmentAddress) {
	    this.appointmentAddress = appointmentAddress;
	}

	@JsonProperty("notification_code")
	public String getNotificationCode() {
	    return NotificationCode;
	}
	public void setNotificationCode(String notificationCode) {
	    NotificationCode = notificationCode;
	}

}
