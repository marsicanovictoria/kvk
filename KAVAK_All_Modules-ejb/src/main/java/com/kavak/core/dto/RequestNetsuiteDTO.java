package com.kavak.core.dto;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.response.NetsuiteOpportunityDTO;

public class RequestNetsuiteDTO {

	private Long code;
	private String status;
	private List<NetsuiteOpportunityDTO> data;
	
	
	@JsonProperty("code")
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonProperty("data")
	public List<NetsuiteOpportunityDTO> getData() {
		return data;
	}
	public void setData(List<NetsuiteOpportunityDTO> data) {
		this.data = data;
	}
		
	
}
