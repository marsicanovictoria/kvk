package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.InspectionScheduleBlockDTO;

import java.util.List;

public class ScheduleBlocksDTO {

	private String name;
	private List<InspectionScheduleBlockDTO> listInspectionScheduleBlockDTO;
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("inpection_schedule_blocks")
	public List<InspectionScheduleBlockDTO> getListInspectionScheduleBlockDTO() {
		return listInspectionScheduleBlockDTO;
	}
	public void setListInspectionScheduleBlockDTO(List<InspectionScheduleBlockDTO> listInspectionScheduleBlockDTO) {
		this.listInspectionScheduleBlockDTO = listInspectionScheduleBlockDTO;
	}
}

