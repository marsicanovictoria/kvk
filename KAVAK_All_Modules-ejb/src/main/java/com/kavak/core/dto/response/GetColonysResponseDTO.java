package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.ColonyDataDTO;

import java.util.Set;

public class GetColonysResponseDTO {

	private String zip;
	private String municipality;
	private String state;
	private boolean allowsInspection;
	private Long shipmentCost;
	private Set<ColonyDataDTO> listColonyDataDTO;

	@JsonProperty("zip")
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	@JsonProperty("municipality")
	public String getMunicipality() {
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}
	@JsonProperty("state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@JsonProperty("allows_inspection")
	public boolean isAllowsInspection() {
		return allowsInspection;
	}
	public void setAllowsInspection(boolean allowsInspection) {
		this.allowsInspection = allowsInspection;
	}
	@JsonProperty("shipment_cost")
	public Long getShipmentCost() {
		return shipmentCost;
	}

	public void setShipmentCost(Long shipmentCost) {
		this.shipmentCost = shipmentCost;
	}
	@JsonProperty("colonies")
	public Set<ColonyDataDTO> getListColonyDataDTO() {
		return listColonyDataDTO;
	}
	public void setListColonyDataDTO(Set<ColonyDataDTO> listColonyDataDTO) {
		this.listColonyDataDTO = listColonyDataDTO;
	}
}