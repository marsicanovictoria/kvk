package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ReferenceDTO {

    List<GenericDTO> listGenericDTOReference;

    @JsonProperty("relationship")
    public List<GenericDTO> getListGenericDTOReference() {
        return listGenericDTOReference;
    }

    public void setListGenericDTOReference(List<GenericDTO> listGenericDTOReference) {
        this.listGenericDTOReference = listGenericDTOReference;
    }
}
