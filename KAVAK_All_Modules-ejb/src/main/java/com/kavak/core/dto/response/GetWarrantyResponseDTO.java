package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Enrique on 6/28/2017.
 */
public class GetWarrantyResponseDTO {

    private List<String> listWarrantysNames;

    @JsonProperty("warrantys")
    public List<String> getListWarrantysNames() {
        return listWarrantysNames;
    }

    public void setListWarrantysNames(List<String> listWarrantysNames) {
        this.listWarrantysNames = listWarrantysNames;
    }
}
