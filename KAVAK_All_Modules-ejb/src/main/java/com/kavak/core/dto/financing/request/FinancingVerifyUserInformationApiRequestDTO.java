package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.financing.FinancingUserRegisterApiDataDTO;

public class FinancingVerifyUserInformationApiRequestDTO {

    private String type;
    private FinancingUserRegisterApiDataDTO data;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    @JsonProperty("data")
    public FinancingUserRegisterApiDataDTO getData() {
	return data;
    }

    public void setData(FinancingUserRegisterApiDataDTO data) {
	this.data = data;
    }




}