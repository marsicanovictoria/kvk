package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostFinancingUserPinCodeAuthorizationResponseDTO {

    	private String status;

    	@JsonProperty("status")
	public String getStatus() {
	    return status;
	}
	public void setStatus(String status) {
	    this.status = status;
	}

}
