package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostAppNotificationTokenRequestDTO {

	private Long userId;
	private String tokenType;
	private String deviceId;
	private String tokenDevice;
	private boolean isActive;

	
	@JsonProperty("user_id")
	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}

	@JsonProperty("token_type")
	public String getTokenType() {
	    return tokenType;
	}

	public void setTokenType(String tokenType) {
	    this.tokenType = tokenType;
	}
	
	@JsonProperty("device_id")
	public String getDeviceId() {
	    return deviceId;
	}

	public void setDeviceId(String deviceId) {
	    this.deviceId = deviceId;
	}
	
	@JsonProperty("token_device")
	public String getTokenDevice() {
	    return tokenDevice;
	}

	public void setTokenDevice(String tokenDevice) {
	    this.tokenDevice = tokenDevice;
	}

	@JsonProperty("is_active")
	public boolean isActive() {
	    return isActive;
	}

	public void setActive(boolean isActive) {
	    this.isActive = isActive;
	}



















}
