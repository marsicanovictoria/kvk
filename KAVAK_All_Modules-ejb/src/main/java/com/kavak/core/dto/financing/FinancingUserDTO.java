package com.kavak.core.dto.financing;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserDTO {
	
    
    private Long id;
    private Long userId;
    private Long financingUserId;
    private String email;
    private String authenticationToken;
    private String fullName;
    private String firstName;
    private String secondName;
    private String gender;
    private String phone;
    private String rfc;
    private String birthday;
    private Timestamp buroDate;
    private Timestamp creationDate;
    private boolean active; 
    private String urlBuro;


    @JsonProperty("id")
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @JsonProperty("user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @JsonProperty("financing_user_id")
    public Long getFinancingUserId() {
        return financingUserId;
    }

    public void setFinancingUserId(Long financingUserId) {
        this.financingUserId = financingUserId;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("authentication_token")
    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    @JsonProperty("full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("second_name")
    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("rfc")
    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    @JsonProperty("birthday")
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @JsonProperty("buro_date")
    public Timestamp getBuroDate() {
        return buroDate;
    }

    public void setBuroDate(Timestamp buroDate) {
        this.buroDate = buroDate;
    }

    @JsonProperty("is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @JsonProperty("creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
    
    @JsonProperty("buro_url")
    public String getUrlBuro() {
        return urlBuro;
    }

    public void setUrlBuro(String urlBuro) {
        this.urlBuro = urlBuro;
    }


}