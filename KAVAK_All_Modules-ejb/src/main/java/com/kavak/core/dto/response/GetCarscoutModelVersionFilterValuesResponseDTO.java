package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetCarscoutModelVersionFilterValuesResponseDTO
		implements Comparable<GetCarscoutModelVersionFilterValuesResponseDTO> {

	private Long modelId;
	private Long versionId;
	private String name;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("model_id")
	public Long getModelId() {
		return modelId;
	}

	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("version_id")
	public Long getVersionId() {
		return versionId;
	}

	public void setVersionId(Long versionId) {
		this.versionId = versionId;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int compareTo(GetCarscoutModelVersionFilterValuesResponseDTO other) {
		return name.compareTo(other.name);
	}
}
