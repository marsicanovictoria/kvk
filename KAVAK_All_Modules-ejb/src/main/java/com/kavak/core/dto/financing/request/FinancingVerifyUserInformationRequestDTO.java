package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingVerifyUserInformationRequestDTO {

    	private Long userId;
	private String phone;
	private String fullName;
	private String firstSurname;
	private String secondSurname;
	private String email;
	private String rfc;
	private String gender;
	private String birthday;

	@JsonProperty("user_id")
	public Long getUserId() {
	    return userId;
	}
	public void setUserId(Long userId) {
	    this.userId = userId;
	}
	
	@JsonProperty("phone")
	public String getPhone() {
	    return phone;
	}
	public void setPhone(String phone) {
	    this.phone = phone;
	}
	
	@JsonProperty("full_name")
	public String getFullName() {
	    return fullName;
	}
	public void setFullName(String fullName) {
	    this.fullName = fullName;
	}
	
	@JsonProperty("first_surname")
	public String getFirstSurname() {
	    return firstSurname;
	}
	public void setFirstSurname(String firstSurname) {
	    this.firstSurname = firstSurname;
	}
	
	@JsonProperty("second_surname")
	public String getSecondSurname() {
	    return secondSurname;
	}
	public void setSecondSurname(String secondSurname) {
	    this.secondSurname = secondSurname;
	}
	
	@JsonProperty("email")
	public String getEmail() {
	    return email;
	}
	public void setEmail(String email) {
	    this.email = email;
	}
	
	@JsonProperty("rfc")
	public String getRfc() {
	    return rfc;
	}
	public void setRfc(String rfc) {
	    this.rfc = rfc;
	}
	
	@JsonProperty("gender")
	public String getGender() {
	    return gender;
	}
	public void setGender(String gender) {
	    this.gender = gender;
	}
	
	@JsonProperty("birthday")
	public String getBirthday() {
	    return birthday;
	}
	public void setBirthday(String birthday) {
	    this.birthday = birthday;
	}

}
