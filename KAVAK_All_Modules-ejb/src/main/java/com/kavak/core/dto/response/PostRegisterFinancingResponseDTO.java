package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostRegisterFinancingResponseDTO {

	private Long id;
	private String urlPdf;

	@JsonProperty("financing_id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("financing_form_pdf_url")
	public String getUrlPdf() {
		return urlPdf;
	}

	public void setUrlPdf(String urlPdf) {
		this.urlPdf = urlPdf;
	}

}
