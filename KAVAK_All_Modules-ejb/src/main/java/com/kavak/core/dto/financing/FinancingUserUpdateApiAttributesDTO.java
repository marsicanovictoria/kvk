package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateApiAttributesDTO {

    	private String name;
    	private String firstSurname;
    	private String secondSurname;
        private String gender;
        private String phone;
        private String mobilePhone;
        private String birthday;
        private String rfc;
        private String curp;
        

	@JsonProperty("name")
	public String getName() {
	    return name;
	}
	public void setName(String name) {
	    this.name = name;
	}
	
	@JsonProperty("first_surname")
	public String getFirstSurname() {
	    return firstSurname;
	}
	public void setFirstSurname(String firstSurname) {
	    this.firstSurname = firstSurname;
	}
	
	@JsonProperty("second_surname")
	public String getSecondSurname() {
	    return secondSurname;
	}
	public void setSecondSurname(String secondSurname) {
	    this.secondSurname = secondSurname;
	}
	
	@JsonProperty("gender")
	public String getGender() {
	    return gender;
	}
	public void setGender(String gender) {
	    this.gender = gender;
	}
	
	@JsonProperty("phone")
	public String getPhone() {
	    return phone;
	}
	public void setPhone(String phone) {
	    this.phone = phone;
	}
	
	@JsonProperty("mobile_phone")
	public String getMobilePhone() {
	    return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
	    this.mobilePhone = mobilePhone;
	}
	
	@JsonProperty("birthday")
	public String getBirthday() {
	    return birthday;
	}
	public void setBirthday(String birthday) {
	    this.birthday = birthday;
	}
	
	@JsonProperty("rfc")
	public String getRfc() {
	    return rfc;
	}
	public void setRfc(String rfc) {
	    this.rfc = rfc;
	}
	
	@JsonProperty("curp")
	public String getCurp() {
	    return curp;
	}
	public void setCurp(String curp) {
	    this.curp = curp;
	}
	

}