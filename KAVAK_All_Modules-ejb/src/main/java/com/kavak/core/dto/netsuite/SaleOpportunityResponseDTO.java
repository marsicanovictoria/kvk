package com.kavak.core.dto.netsuite;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.CarDataDTO;
import com.kavak.core.dto.UserDTO;
import com.kavak.core.dto.response.CustomerResponseDTO;

public class SaleOpportunityResponseDTO {
	private int id;
	private Long stockId;
	private Long carKm;
	private Long zipCode;
	private Long shippingAddressId;
	private String shippingAddressDescription;
	private String warrantyName;
	private String warrantyTime;
	private Long warrantyAmount;
	private String warrantyRange;
	private String insuranceName;
	private String insuranceTime;
	private String insuranceAmount;
	private String insuranceModality;
	private String paymentMethodTypeId;
	private String isFinanced;
	private String isFinancingCompleted;
	private String downPayment;
	private String amountFinancing;
	private String monthFinancing;
	private String checkoutResult;
	private String paymentPlatform;
	private String paymentPlatformId;
	private String transactionNumber;
	private Long paymentStatus;
	private Long carCost;
	private String totalAmount;
	private String carReserve;
	private String carShippingCost;
	private String registerDate;
	private String updateDate;
	private Long saleOpportunityTypeId;
	private Long visitDateId;
	private String visitDate;
	private String visitHour;
	private Long netsuiteItemId;
	private Long duplicatedSaleControl;
	private Long statusId;
	private String statusDescription;
	private String appointmentContact;
	private String appoinmentDate;
	private String comments;
	private String otherInterestedCar;
	private String watchedCar;
	private String appliedCoupon;
	private String platform;
	private String channel;
	private UserDTO wingmanCheckout;
	private Long fakeBooking;
	private CustomerResponseDTO customer;
	private CarDataDTO car;

	

	@JsonProperty("id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@JsonProperty("stock_id")
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	
	@JsonProperty("car_km")
	public Long getCarKm() {
		return carKm;
	}
	public void setCarKm(Long carKm) {
		this.carKm = carKm;
	}
	
	@JsonProperty("zip_code")
	public Long getZipCode() {
		return zipCode;
	}
	public void setZipCode(Long zipCode) {
		this.zipCode = zipCode;
	}
	
	@JsonProperty("shipping_address_id")
	public Long getShippingAddressId() {
		return shippingAddressId;
	}
	public void setShippingAddressId(Long shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}
	
	@JsonProperty("shipping_address_description")
	public String getShippingAddressDescription() {
		return shippingAddressDescription;
	}
	public void setShippingAddressDescription(String shippingAddressDescription) {
		this.shippingAddressDescription = shippingAddressDescription;
	}
	
	@JsonProperty("warranty_name")
	public String getWarrantyName() {
		return warrantyName;
	}
	public void setWarrantyName(String warrantyName) {
		this.warrantyName = warrantyName;
	}
	
	@JsonProperty("warranty_time")
	public String getWarrantyTime() {
		return warrantyTime;
	}
	public void setWarrantyTime(String warrantyTime) {
		this.warrantyTime = warrantyTime;
	}
	
	@JsonProperty("warranty_amount")
	public Long getWarrantyAmount() {
		return warrantyAmount;
	}
	public void setWarrantyAmount(Long warrantyAmount) {
		this.warrantyAmount = warrantyAmount;
	}
	
	@JsonProperty("warranty_range")
	public String getWarrantyRange() {
		return warrantyRange;
	}
	public void setWarrantyRange(String warrantyRange) {
		this.warrantyRange = warrantyRange;
	}
	
	@JsonProperty("insurance_name")
	public String getInsuranceName() {
		return insuranceName;
	}
	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}
	
	@JsonProperty("insurance_time")
	public String getInsuranceTime() {
		return insuranceTime;
	}
	public void setInsuranceTime(String insuranceTime) {
		this.insuranceTime = insuranceTime;
	}
	
	@JsonProperty("insurance_amount")
	public String getInsuranceAmount() {
		return insuranceAmount;
	}
	public void setInsuranceAmount(String insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	
	@JsonProperty("insurance_modality")
	public String getInsuranceModality() {
		return insuranceModality;
	}
	public void setInsuranceModality(String insuranceModality) {
		this.insuranceModality = insuranceModality;
	}
	
	@JsonProperty("payment_method_id")
	public String getPaymentMethodTypeId() {
		return paymentMethodTypeId;
	}
	public void setPaymentMethodTypeId(String paymentMethodTypeId) {
		this.paymentMethodTypeId = paymentMethodTypeId;
	}
	
	@JsonProperty("is_financed")
	public String getIsFinanced() {
		return isFinanced;
	}
	public void setIsFinanced(String isFinanced) {
		this.isFinanced = isFinanced;
	}
	
	@JsonProperty("is_financing_completed")
	public String getIsFinancingCompleted() {
		return isFinancingCompleted;
	}
	public void setIsFinancingCompleted(String isFinancingCompleted) {
		this.isFinancingCompleted = isFinancingCompleted;
	}
	
	@JsonProperty("downpayment")
	public String getDownPayment() {
		return downPayment;
	}
	public void setDownPayment(String downPayment) {
		this.downPayment = downPayment;
	}
	
	@JsonProperty("amount_financing")
	public String getAmountFinancing() {
		return amountFinancing;
	}
	public void setAmountFinancing(String amountFinancing) {
		this.amountFinancing = amountFinancing;
	}
	
	@JsonProperty("month_financing")
	public String getMonthFinancing() {
		return monthFinancing;
	}
	public void setMonthFinancing(String monthFinancing) {
		this.monthFinancing = monthFinancing;
	}
	
	@JsonProperty("checkout_result")
	public String getCheckoutResult() {
		return checkoutResult;
	}
	public void setCheckoutResult(String checkoutResult) {
		this.checkoutResult = checkoutResult;
	}
	
	@JsonProperty("payment_platform")
	public String getPaymentPlatform() {
		return paymentPlatform;
	}
	public void setPaymentPlatform(String paymentPlatform) {
		this.paymentPlatform = paymentPlatform;
	}
	
	@JsonProperty("payment_platform_id")
	public String getPaymentPlatformId() {
		return paymentPlatformId;
	}
	public void setPaymentPlatformId(String paymentPlatformId) {
		this.paymentPlatformId = paymentPlatformId;
	}
	
	@JsonProperty("transaction_number")
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	
	@JsonProperty("payment_status")
	public Long getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(Long paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	
	@JsonProperty("car_cost")
	public Long getCarCost() {
		return carCost;
	}
	public void setCarCost(Long carCost) {
		this.carCost = carCost;
	}
	
	@JsonProperty("total_amount")
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	@JsonProperty("car_reserve")
	public String getCarReserve() {
		return carReserve;
	}
	public void setCarReserve(String carReserve) {
		this.carReserve = carReserve;
	}
	
	@JsonProperty("car_shipping_cost")
	public String getCarShippingCost() {
		return carShippingCost;
	}
	public void setCarShippingCost(String carShippingCost) {
		this.carShippingCost = carShippingCost;
	}
	
	@JsonProperty("register_date")
	public String getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	
	@JsonProperty("update_date")
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
	@JsonProperty("sale_opportunity_type_id")
	public Long getSaleOpportunityTypeId() {
		return saleOpportunityTypeId;
	}
	public void setSaleOpportunityTypeId(Long saleOpportunityTypeId) {
		this.saleOpportunityTypeId = saleOpportunityTypeId;
	}
	
	@JsonProperty("visit_date_id")
	public Long getVisitDateId() {
		return visitDateId;
	}
	public void setVisitDateId(Long visitDateId) {
		this.visitDateId = visitDateId;
	}
	
	@JsonProperty("visit_date")
	public String getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}
	
	@JsonProperty("visit_hour")
	public String getVisitHour() {
		return visitHour;
	}
	public void setVisitHour(String visitHour) {
		this.visitHour = visitHour;
	}	
	@JsonProperty("netsuite_item_id")
	public Long getNetsuiteItemId() {
		return netsuiteItemId;
	}
	public void setNetsuiteItemId(Long netsuiteItemId) {
		this.netsuiteItemId = netsuiteItemId;
	}
	
	@JsonProperty("duplicated_sale_control")
	public Long getDuplicatedSaleControl() {
		return duplicatedSaleControl;
	}
	public void setDuplicatedSaleControl(Long duplicatedSaleControl) {
		this.duplicatedSaleControl = duplicatedSaleControl;
	}
	
	@JsonProperty("status_id")
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	@JsonProperty("status_description")
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	
	@JsonProperty("appointment_contact")
	public String getAppointmentContact() {
		return appointmentContact;
	}
	public void setAppointmentContact(String appointmentContact) {
		this.appointmentContact = appointmentContact;
	}
	
	@JsonProperty("appointment_date")
	public String getAppoinmentDate() {
		return appoinmentDate;
	}
	public void setAppoinmentDate(String appoinmentDate) {
		this.appoinmentDate = appoinmentDate;
	}
	
	@JsonProperty("comments")
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	@JsonProperty("others_interested_car")
	public String getOtherInterestedCar() {
		return otherInterestedCar;
	}
	public void setOtherInterestedCar(String otherInterestedCar) {
		this.otherInterestedCar = otherInterestedCar;
	}
	
	@JsonProperty("watched_car")
	public String getWatchedCar() {
		return watchedCar;
	}
	public void setWatchedCar(String watchedCar) {
		this.watchedCar = watchedCar;
	}
	
	@JsonProperty("applied_coupon")
	public String getAppliedCoupon() {
		return appliedCoupon;
	}
	public void setAppliedCoupon(String appliedCoupon) {
		this.appliedCoupon = appliedCoupon;
	}
	@JsonProperty("customer_data")
	public CustomerResponseDTO getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerResponseDTO customer) {
		this.customer = customer;
	}
	
	@JsonProperty("car_data")
	public CarDataDTO getCar() {
		return car;
	}
	public void setCar(CarDataDTO car) {
		this.car = car;
	}
	
	@JsonProperty("wingman_checkout")
	public UserDTO getWingmanCheckout() {
		return wingmanCheckout;
	}
	public void setWingmanCheckout(UserDTO wingmanCheckout) {
		this.wingmanCheckout = wingmanCheckout;
	}
	
	@JsonProperty("platform")
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	@JsonProperty("channel")
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	@JsonProperty("fake_booking")
	public Long getFakeBooking() {
		return fakeBooking;
	}
	public void setFakeBooking(Long fakeBooking) {
		this.fakeBooking = fakeBooking;
	}

}
