package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.MessageDTO;

import java.util.List;

public class ResponseDTO {
	
	private Long code;
	private String status;
	private List<MessageDTO> listMessage;
	private String netsuiteMethod;
	private Object data;
	
	@JsonProperty("code")
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("validations")
	public List<MessageDTO> getListMessage() {
		return listMessage;
	}
	public void setListMessage(List<MessageDTO> listMessage) {
		this.listMessage = listMessage;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("method")
    public String getNetsuiteMethod() {
		return netsuiteMethod;
	}
	public void setNetsuiteMethod(String netsuiteMethod) {
		this.netsuiteMethod = netsuiteMethod;
	}
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("data")
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}