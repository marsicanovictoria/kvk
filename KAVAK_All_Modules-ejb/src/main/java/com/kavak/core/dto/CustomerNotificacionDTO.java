package com.kavak.core.dto;

import java.security.Timestamp;

public class CustomerNotificacionDTO {
	private Long id;
	private Long customerId;
	private Long carId;
	private String username;
	private Timestamp creationDate;
	private Timestamp updateDate;
	private Long type;
	private String source;
	private Long sendNotification;
	private Long sentEmail;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public Long getType() {
		return type;
	}
	public void setType(Long type) {
		this.type = type;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Long getSendNotification() {
		return sendNotification;
	}
	public void setSendNotification(Long sendNotification) {
		this.sendNotification = sendNotification;
	}
	public Long getSentEmail() {
		return sentEmail;
	}
	public void setSentEmail(Long sentEmail) {
		this.sentEmail = sentEmail;
	}
	
	
	
	
}
