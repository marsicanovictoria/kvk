package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserAuthorizationPinCodeApiAttributesResponseDTO {

	private String url;
	private String bureauReportStatus;
	private String bureauReportResponse;
	private String bureauReportSavedAt;
	private String updatedAt;
	

	@JsonProperty("url")
	public String getUrl() {
	    return url;
	}
	public void setUrl(String url) {
	    this.url = url;
	}
	
	@JsonProperty("bureauReportStatus")
	public String getBureauReportStatus() {
	    return bureauReportStatus;
	}
	public void setBureauReportStatus(String bureauReportStatus) {
	    this.bureauReportStatus = bureauReportStatus;
	}
	
	@JsonProperty("bureauReportResponse")
	public String getBureauReportResponse() {
	    return bureauReportResponse;
	}
	public void setBureauReportResponse(String bureauReportResponse) {
	    this.bureauReportResponse = bureauReportResponse;
	}
	
	@JsonProperty("bureauReportSavedAt")
	public String getBureauReportSavedAt() {
	    return bureauReportSavedAt;
	}
	public void setBureauReportSavedAt(String bureauReportSavedAt) {
	    this.bureauReportSavedAt = bureauReportSavedAt;
	}
	
	@JsonProperty("updatedAt")
	public String getUpdatedAt() {
	    return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
	    this.updatedAt = updatedAt;
	}

}