package com.kavak.core.dto.response;

public class TypesOffersDTO {
	
	private Integer id;
	private String name;
	private String min_offer;
	private String max_offer;
	private Boolean is_selected;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the min_offer
	 */
	public String getMin_offer() {
		return min_offer;
	}
	/**
	 * @param min_offer the min_offer to set
	 */
	public void setMin_offer(String min_offer) {
		this.min_offer = min_offer;
	}
	/**
	 * @return the max_offer
	 */
	public String getMax_offer() {
		return max_offer;
	}
	/**
	 * @param max_offer the max_offer to set
	 */
	public void setMax_offer(String max_offer) {
		this.max_offer = max_offer;
	}
	/**
	 * @return the is_selected
	 */
	public Boolean getIs_selected() {
		return is_selected;
	}
	/**
	 * @param is_selected the is_selected to set
	 */
	public void setIs_selected(Boolean is_selected) {
		this.is_selected = is_selected;
	}

}
