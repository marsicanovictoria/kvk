package com.kavak.core.dto.google;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;

/**
 * Created by Enrique on 19-Jun-17.
 */
public class Elements {

    private GenericDTO genericDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("elements")
    public GenericDTO getGenericDTO() {
        return genericDTO;
    }

    public void setGenericDTO(GenericDTO genericDTO) {
        this.genericDTO = genericDTO;
    }
}
