package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeleteCarscoutAlertRequestDTO {

	private Long userId;
	private Long alertId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("alert_id")
	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

}
