package com.kavak.core.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OfferCheckpointDTO {

    private Long id;
    private Long idUser;
    private String customerName;
    private String email;
    private String customerPhone;
    private String customerAddress;
    // private Long stockId;
    private String sku;
    private String carName;
    private String carMake;
    private String carModel;
    private String carVersion;
    private Long carYear;
    private Long carKm;
    private String fullVersion;
    private String selectedOffer;
    private String offerRange30Days;
    private String offerRangeInstant;
    private String offerRangeCnc;
    private String buyPrice;
    private String marketPrice;
    private Long wishList;
    private String offerPunished;
    private String max30DaysOffer;
    private String min30DaysOffer;
    private String maxInstantOffer;
    private String minInstantOffer;
    private String maxConsigmentOffer;
    private String minConsigmentOffer;
    private String registerDate;
    private Long idHourInpectionSlot;
    private String hourInpectionSlotText;
    private String inspectionDate;
    private String circulationCardUrl;
    private String carInvoiceUrl;
    private Long inspectionLocationId;
    private String inspectionLocationDescription;
    private String inspectionCenterDescription;
    private Long StatusId;
    private String StatusDetail;
    private String zipCode;
    private Long codeNetsuiteItem;
    private Long offerType;
    private String offerTypeDescription;
    private Long duplicatedOfferControl;
    private String salePriceGa;
    private String salepriceKavak;
    private Long assignedEmId;
    private Long netsuiteOpportunityId;
    private String netsuiteOpportunityURL;
    private Timestamp netsuiteIdUpdateDate;
    private String reliabilitySample;
    private String updateDateOffer;
    private String offerSource;
    private boolean sentInternalEmail;
    private Long netsuiteIdAttemps;
    private String segmentType;
    private Long internalUserId;
    private Long exteriorColorId;
    private Long abTestResult;
    private String abTestCode;

    @JsonProperty("id")
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @JsonProperty("id_usuario")
    public Long getIdUser() {
	return idUser;
    }

    public void setIdUser(Long idUser) {
	this.idUser = idUser;
    }

    @JsonProperty("customer_name")
    public String getCustomerName() {
	return customerName;
    }

    public String getEmail() {
	return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
	this.email = email;
    }

    public void setCustomerName(String customerName) {
	this.customerName = customerName;
    }

    @JsonProperty("customer_phone")
    public String getCustomerPhone() {
	return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
	this.customerPhone = customerPhone;
    }

    @JsonProperty("address")
    public String getCustomerAddress() {
	return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
	this.customerAddress = customerAddress;
    }

    @JsonProperty("sku")
    public String getSku() {
	return sku;
    }

    public void setSku(String sku) {
	this.sku = sku;
    }

    @JsonProperty("car_name")
    public String getCarName() {
	return carName;
    }

    public void setCarName(String carName) {
	this.carName = carName;
    }

    @JsonProperty("car_make")
    public String getCarMake() {
	return carMake;
    }

    public void setCarMake(String carMake) {
	this.carMake = carMake;
    }

    @JsonProperty("car_model")
    public String getCarModel() {
	return carModel;
    }

    public void setCarModel(String carModel) {
	this.carModel = carModel;
    }

    @JsonProperty("car_version")
    public String getCarVersion() {
	return carVersion;
    }

    public void setCarVersion(String carVersion) {
	this.carVersion = carVersion;
    }

    @JsonProperty("car_year")
    public Long getCarYear() {
	return carYear;
    }

    public void setCarYear(Long carYear) {
	this.carYear = carYear;
    }

    @JsonProperty("car_km")
    public Long getCarKm() {
	return carKm;
    }

    public void setCarKm(Long carKm) {
	this.carKm = carKm;
    }

    @JsonProperty("version_features")
    public String getFullVersion() {
	return fullVersion;
    }

    public void setFullVersion(String fullVersion) {
	this.fullVersion = fullVersion;
    }

    @JsonProperty("selected_offer")
    public String getSelectedOffer() {
	return selectedOffer;
    }

    public void setSelectedOffer(String selectedOffer) {
	this.selectedOffer = selectedOffer;
    }

    @JsonProperty("30d_offer_range")
    public String getOfferRange30Days() {
	return offerRange30Days;
    }

    public void setOfferRange30Days(String offerRange30Days) {
	this.offerRange30Days = offerRange30Days;
    }

    @JsonProperty("instant_offer_range")
    public String getOfferRangeInstant() {
	return offerRangeInstant;
    }

    public void setOfferRangeInstant(String offerRangeInstant) {
	this.offerRangeInstant = offerRangeInstant;
    }

    @JsonProperty("cnc_offer_range")
    public String getOfferRangeCnc() {
	return offerRangeCnc;
    }

    public void setOfferRangeCnc(String offerRangeCnc) {
	this.offerRangeCnc = offerRangeCnc;
    }

    @JsonProperty("ga_buy_price")
    public String getBuyPrice() {
	return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
	this.buyPrice = buyPrice;
    }

    @JsonProperty("market_price")
    public String getMarketPrice() {
	return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
	this.marketPrice = marketPrice;
    }

    @JsonProperty("wish_list")
    public Long getWishList() {
	return wishList;
    }

    public void setWishList(Long wishList) {
	this.wishList = wishList;
    }

    @JsonProperty("oferta_castigada")
    public String getOfferPunished() {
	return offerPunished;
    }

    public void setOfferPunished(String offerPunished) {
	this.offerPunished = offerPunished;
    }

    @JsonProperty("max_30_days_offer")
    public String getMax30DaysOffer() {
	return max30DaysOffer;
    }

    public void setMax30DaysOffer(String max30DaysOffer) {
	this.max30DaysOffer = max30DaysOffer;
    }

    @JsonProperty("min_30_days_offer")
    public String getMin30DaysOffer() {
	return min30DaysOffer;
    }

    public void setMin30DaysOffer(String min30DaysOffer) {
	this.min30DaysOffer = min30DaysOffer;
    }

    @JsonProperty("max_instant_offer")
    public String getMaxInstantOffer() {
	return maxInstantOffer;
    }

    public void setMaxInstantOffer(String maxInstantOffer) {
	this.maxInstantOffer = maxInstantOffer;
    }

    @JsonProperty("min_instant_offer")
    public String getMinInstantOffer() {
	return minInstantOffer;
    }

    public void setMinInstantOffer(String minInstantOffer) {
	this.minInstantOffer = minInstantOffer;
    }

    @JsonProperty("max_consignment_offer")
    public String getMaxConsigmentOffer() {
	return maxConsigmentOffer;
    }

    public void setMaxConsigmentOffer(String maxConsigmentOffer) {
	this.maxConsigmentOffer = maxConsigmentOffer;
    }

    @JsonProperty("min_consignment_offer")
    public String getMinConsigmentOffer() {
	return minConsigmentOffer;
    }

    public void setMinConsigmentOffer(String minConsigmentOffer) {
	this.minConsigmentOffer = minConsigmentOffer;
    }

    @JsonProperty("register_date")
    public String getRegisterDate() {
	return registerDate;
    }

    public void setRegisterDate(String registerDate) {
	this.registerDate = registerDate;
    }

    public Long getIdHourInpectionSlot() {
	return idHourInpectionSlot;
    }

    public void setIdHourInpectionSlot(Long idHourInpectionSlot) {
	this.idHourInpectionSlot = idHourInpectionSlot;
    }

    public String getHourInpectionSlotText() {
	return hourInpectionSlotText;
    }

    public void setHourInpectionSlotText(String hourInpectionSlotText) {
	this.hourInpectionSlotText = hourInpectionSlotText;
    }

    public String getInspectionDate() {
	return inspectionDate;
    }

    public void setInspectionDate(String inspectionDate) {
	this.inspectionDate = inspectionDate;
    }

    public String getCirculationCardUrl() {
	return circulationCardUrl;
    }

    public void setCirculationCardUrl(String circulationCardUrl) {
	this.circulationCardUrl = circulationCardUrl;
    }

    public String getCarInvoiceUrl() {
	return carInvoiceUrl;
    }

    public void setCarInvoiceUrl(String carInvoiceUrl) {
	this.carInvoiceUrl = carInvoiceUrl;
    }

    @JsonProperty("inspection_location_id")
    public Long getInspectionLocationId() {
	return inspectionLocationId;
    }

    public void setInspectionLocationId(Long inspectionLocationId) {
	this.inspectionLocationId = inspectionLocationId;
    }

    @JsonProperty("inspection_location_description")
    public String getInspectionLocationDescription() {
	return inspectionLocationDescription;
    }

    public void setInspectionLocationDescription(String inspectionLocationDescription) {
	this.inspectionLocationDescription = inspectionLocationDescription;
    }

    @JsonProperty("inspection_center_description")
    public String getInspectionCenterDescription() {
	return inspectionCenterDescription;
    }

    public void setInspectionCenterDescription(String inspectionCenterDescription) {
	this.inspectionCenterDescription = inspectionCenterDescription;
    }

    public Long getStatusId() {
	return StatusId;
    }

    public void setStatusId(Long statusId) {
	StatusId = statusId;
    }

    public String getStatusDetail() {
	return StatusDetail;
    }

    @JsonProperty("status_detail")
    public void setStatusDetail(String statusDetail) {
	StatusDetail = statusDetail;
    }

    public String getZipCode() {
	return zipCode;
    }

    public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
    }

    public Long getCodeNetsuiteItem() {
	return codeNetsuiteItem;
    }

    public void setCodeNetsuiteItem(Long codeNetsuiteItem) {
	this.codeNetsuiteItem = codeNetsuiteItem;
    }

    public Long getOfferType() {
	return offerType;
    }

    public void setOfferType(Long offerType) {
	this.offerType = offerType;
    }

    public String getOfferTypeDescription() {
	return offerTypeDescription;
    }

    @JsonProperty("offer_type_description")
    public void setOfferTypeDescription(String offerTypeDescription) {
	this.offerTypeDescription = offerTypeDescription;
    }

    public Long getDuplicatedOfferControl() {
	return duplicatedOfferControl;
    }

    public void setDuplicatedOfferControl(Long duplicatedOfferControl) {
	this.duplicatedOfferControl = duplicatedOfferControl;
    }

    @JsonProperty("sale_price_ga")
    public String getSalePriceGa() {
	return salePriceGa;
    }

    public void setSalePriceGa(String salePriceGa) {
	this.salePriceGa = salePriceGa;
    }

    @JsonProperty("sale_price_kavak")
    public String getSalepriceKavak() {
	return salepriceKavak;
    }

    public void setSalepriceKavak(String salepriceKavak) {
	this.salepriceKavak = salepriceKavak;
    }

    @JsonProperty("assigned_em_id")
    public Long getAssignedEmId() {
	return assignedEmId;
    }

    public void setAssignedEmId(Long assignedEmId) {
	this.assignedEmId = assignedEmId;
    }

    @JsonProperty("netsuite_opportunity_id")
    public Long getNetsuiteOpportunityId() {
	return netsuiteOpportunityId;
    }

    public void setNetsuiteOpportunityId(Long netsuiteOpportunityId) {
	this.netsuiteOpportunityId = netsuiteOpportunityId;
    }

    @JsonProperty("netsuite_opportunity_url")
    public String getNetsuiteOpportunityURL() {
	return netsuiteOpportunityURL;
    }

    public void setNetsuiteOpportunityURL(String netsuiteOpportunityURL) {
	this.netsuiteOpportunityURL = netsuiteOpportunityURL;
    }

    @JsonProperty("netsuite_opportunity_update_date")
    public Timestamp getNetsuiteIdUpdateDate() {
	return netsuiteIdUpdateDate;
    }

    public void setNetsuiteIdUpdateDate(Timestamp netsuiteIdUpdateDate) {
	this.netsuiteIdUpdateDate = netsuiteIdUpdateDate;
    }

    @JsonProperty("reliability_sample")
    public String getReliabilitySample() {
	return reliabilitySample;
    }

    public void setReliabilitySample(String reliabilitySample) {
	this.reliabilitySample = reliabilitySample;
    }

    @JsonProperty("update_date_offer")
    public String getUpdateDateOffer() {
	return updateDateOffer;
    }

    public void setUpdateDateOffer(String updateDateOffer) {
	this.updateDateOffer = updateDateOffer;
    }

    @JsonProperty("offer_source")
    public String getOfferSource() {
	return offerSource;
    }

    public void setOfferSource(String offerSource) {
	this.offerSource = offerSource;
    }

    @JsonProperty("sent_internal_email")
    public boolean isSentInternalEmail() {
	return sentInternalEmail;
    }

    public void setSentInternalEmail(boolean sentInternalEmail) {
	this.sentInternalEmail = sentInternalEmail;
    }

    @JsonProperty("netsuite_opportunity_id_attemps")
    public Long getNetsuiteIdAttemps() {
	return netsuiteIdAttemps;
    }

    public void setNetsuiteIdAttemps(Long netsuiteIdAttemps) {
	this.netsuiteIdAttemps = netsuiteIdAttemps;
    }

    @JsonProperty("segment_type")
    public String getSegmentType() {
	return segmentType;
    }

    public void setSegmentType(String segmentType) {
	this.segmentType = segmentType;
    }

    @JsonProperty("internal_user_id")
	public Long getInternalUserId() {
		return internalUserId;
	}

	public void setInternalUserId(Long internalUserId) {
		this.internalUserId = internalUserId;
	}

	@JsonProperty("exterior_color_id")
	public Long getExteriorColorId() {
		return exteriorColorId;
	}

	public void setExteriorColorId(Long exteriorColorId) {
		this.exteriorColorId = exteriorColorId;
	}

	@JsonProperty("ab_test_result")
	public Long getAbTestResult() {
		return abTestResult;
	}

	public void setAbTestResult(Long abTestResult) {
		this.abTestResult = abTestResult;
	}

	@JsonProperty("ab_test_code")
	public String getAbTestCode() {
		return abTestCode;
	}

	public void setAbTestCode(String abTestCode) {
		this.abTestCode = abTestCode;
	}

}