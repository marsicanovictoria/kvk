package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SellCarInsuranceDTO {

    private Long id;
    private Long idSellCarDetail;
    private Long idMetaValue;
    private Long insuranceValue;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("sell_car_id")
    public Long getIdSellCarDetail() {
        return idSellCarDetail;
    }
    public void setIdSellCarDetail(Long idSellCarDetail) {
        this.idSellCarDetail = idSellCarDetail;
    }

    @JsonProperty("meta_value_id")
    public Long getIdMetaValue() {
        return idMetaValue;
    }
    public void setIdMetaValue(Long idMetaValue) {
        this.idMetaValue = idMetaValue;
    }

    @JsonProperty("insurance_value")
    public Long getInsuranceValue() {
        return insuranceValue;
    }
    public void setInsuranceValue(Long insuranceValue) {
        this.insuranceValue = insuranceValue;
    }
}
