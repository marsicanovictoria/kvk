package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.InspectionsOverviewsDataDTO;

import java.util.List;

/**
 * Created by Enrique on 23-Jun-17.
 */
public class GetInspectionsOverviewsResponseDTO {

    List<InspectionsOverviewsDataDTO>  listInspectionsOverviewsDataDTO;

    @JsonProperty("inspections_overview")
    public List<InspectionsOverviewsDataDTO> getListInspectionsOverviewsDataDTO() {
        return listInspectionsOverviewsDataDTO;
    }

    public void setListInspectionsOverviewsDataDTO(List<InspectionsOverviewsDataDTO> listInspectionsOverviewsDataDTO) {
        this.listInspectionsOverviewsDataDTO = listInspectionsOverviewsDataDTO;
    }
}
