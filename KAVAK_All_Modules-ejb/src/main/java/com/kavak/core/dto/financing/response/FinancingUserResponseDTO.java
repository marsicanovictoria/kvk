package com.kavak.core.dto.financing.response;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserResponseDTO {
	
    
    private Long id;
    private Long userId;
    private Long financingUserId;
    private String email;
    private String authenticationToken;
    private String fullName;
    private String firstName;
    private String secondName;
    private String gender;
    private String phone;
    private String rfc;
    private String birthday;
    private Timestamp buroDate;
    private String creationDate;
    private boolean active; 


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("id")
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("financing_user_id")
    public Long getFinancingUserId() {
        return financingUserId;
    }

    public void setFinancingUserId(Long financingUserId) {
        this.financingUserId = financingUserId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("authentication_token")
    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("second_name")
    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("rfc")
    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("birthday")
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("buro_date")
    public Timestamp getBuroDate() {
        return buroDate;
    }

    public void setBuroDate(Timestamp buroDate) {
        this.buroDate = buroDate;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("creation_date")
    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }


}