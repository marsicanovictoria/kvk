package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserEscoreApiAttributesResponseDTO {

	private String code;
	private String score;
	private String scoreText;
	private String verticalType;
	private String maximumFinancingAmount;
	private String preapprovedAmount;
	private Long financiersCount;
	private String minInterestRate;
	private String maxInterestRate;
	private String monthlyPayment;
	private String nextUrl;
	private String userFullName;
	private FinancingUserEscoreAttributeRelationShipsApiResponseDTO relationships;
	
	

	@JsonProperty("code")
	public String getCode() {
	    return code;
	}
	public void setCode(String code) {
	    this.code = code;
	}
	
	@JsonProperty("score")
	public String getScore() {
	    return score;
	}
	public void setScore(String score) {
	    this.score = score;
	}
	
	@JsonProperty("scoreText")
	public String getScoreText() {
	    return scoreText;
	}
	public void setScoreText(String scoreText) {
	    this.scoreText = scoreText;
	}
	
	@JsonProperty("verticalType")
	public String getVerticalType() {
	    return verticalType;
	}
	public void setVerticalType(String verticalType) {
	    this.verticalType = verticalType;
	}
	
	@JsonProperty("maximumFinancingAmount")
	public String getMaximumFinancingAmount() {
	    return maximumFinancingAmount;
	}
	public void setMaximumFinancingAmount(String maximumFinancingAmount) {
	    this.maximumFinancingAmount = maximumFinancingAmount;
	}
	
	@JsonProperty("preapprovedAmount")
	public String getPreapprovedAmount() {
	    return preapprovedAmount;
	}
	public void setPreapprovedAmount(String preapprovedAmount) {
	    this.preapprovedAmount = preapprovedAmount;
	}
	
	@JsonProperty("financiersCount")
	public Long getFinanciersCount() {
	    return financiersCount;
	}
	public void setFinanciersCount(Long financiersCount) {
	    this.financiersCount = financiersCount;
	}
	
	
	@JsonProperty("minInterestRate")
	public String getMinInterestRate() {
	    return minInterestRate;
	}
	public void setMinInterestRate(String minInterestRate) {
	    this.minInterestRate = minInterestRate;
	}
	
	@JsonProperty("maxInterestRate")
	public String getMaxInterestRate() {
	    return maxInterestRate;
	}
	public void setMaxInterestRate(String maxInterestRate) {
	    this.maxInterestRate = maxInterestRate;
	}
	
	@JsonProperty("monthlyPayment")
	public String getMonthlyPayment() {
	    return monthlyPayment;
	}
	public void setMonthlyPayment(String monthlyPayment) {
	    this.monthlyPayment = monthlyPayment;
	}
	
	@JsonProperty("nextUrl")
	public String getNextUrl() {
	    return nextUrl;
	}
	public void setNextUrl(String nextUrl) {
	    this.nextUrl = nextUrl;
	}
	
	@JsonProperty("userFullName")
	public String getUserFullName() {
	    return userFullName;
	}
	public void setUserFullName(String userFullName) {
	    this.userFullName = userFullName;
	}
	
	@JsonProperty("relationships")
	public FinancingUserEscoreAttributeRelationShipsApiResponseDTO getRelationships() {
	    return relationships;
	}
	public void setRelationships(FinancingUserEscoreAttributeRelationShipsApiResponseDTO relationships) {
	    this.relationships = relationships;
	}


}