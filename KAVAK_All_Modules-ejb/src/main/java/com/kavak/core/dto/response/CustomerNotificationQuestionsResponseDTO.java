package com.kavak.core.dto.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.CustomerNotificationQuestionsDTO;

public class CustomerNotificationQuestionsResponseDTO {

	private List<CustomerNotificationQuestionsDTO> listCustomerNotificationQuestionDTO;

	@JsonProperty("questions")
	public List<CustomerNotificationQuestionsDTO> getListCustomerNotificationQuestionDTO() {
	    return listCustomerNotificationQuestionDTO;
	}

	public void setListCustomerNotificationQuestionDTO(List<CustomerNotificationQuestionsDTO> listCustomerNotificationQuestionDTO) {
	    this.listCustomerNotificationQuestionDTO = listCustomerNotificationQuestionDTO;
	}

}
