package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdditionalDTO { 

	private String delay;
	private String carUse; // car_use": "134",
	private String income;
	private String mortgageCredit; // "mortgage_credit": "si",
	private String automotiveCredit; // "automotive_credit": "no",
	private String creditCard; // "credit_card": "si",
	private String creditCardLimit; // credit_card_limit": "20000",
	private String creditCardDigits; // credit_card_digits": "1234"

	@JsonProperty("delay")
	public String getDelay() {
		return delay;
	}

	public void setDelay(String delay) {
		this.delay = delay;
	}

	@JsonProperty("car_use")
	public String getCarUse() {
		return carUse;
	}

	public void setCarUse(String carUse) {
		this.carUse = carUse;
	}

	@JsonProperty("income")
	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	@JsonProperty("mortgage_credit")
	public String getMortgageCredit() {
		return mortgageCredit;
	}

	public void setMortgageCredit(String mortgageCredit) {
		this.mortgageCredit = mortgageCredit;
	}

	@JsonProperty("automotive_credit")
	public String getAutomotiveCredit() {
		return automotiveCredit;
	}

	public void setAutomotiveCredit(String automotiveCredit) {
		this.automotiveCredit = automotiveCredit;
	}

	@JsonProperty("credit_card")
	public String getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	@JsonProperty("credit_card_limit")
	public String getCreditCardLimit() {
		return creditCardLimit;
	}

	public void setCreditCardLimit(String creditCardLimit) {
		this.creditCardLimit = creditCardLimit;
	}

	@JsonProperty("credit_card_digits")
	public String getCreditCardDigits() {
		return creditCardDigits;
	}

	public void setCreditCardDigits(String creditCardDigits) {
		this.creditCardDigits = creditCardDigits;
	}

}
