package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InsuranceGroupDTO {

	private String name;
	private List<GenericDTO> listInsuranceGenericDTO;
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("contract_types")
	public List<GenericDTO> getListInsuranceGenericDTO() {
		return listInsuranceGenericDTO;
	}
	public void setListInsuranceGenericDTO(List<GenericDTO> listInsuranceGenericDTO) {
		this.listInsuranceGenericDTO = listInsuranceGenericDTO;
	}
}
