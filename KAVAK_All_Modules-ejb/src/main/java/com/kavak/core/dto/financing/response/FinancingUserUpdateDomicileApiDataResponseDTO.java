package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateDomicileApiDataResponseDTO {

    	private Long idFinancing;
	private String type;
	private FinancingUserUpdateDomicileApiAttributesResponseDTO attributes;

	
	@JsonProperty("id")
	public Long getIdFinancing() {
	    return idFinancing;
	}
	public void setIdFinancing(Long idFinancing) {
	    this.idFinancing = idFinancing;
	}
	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}

	@JsonProperty("attributes")
	public FinancingUserUpdateDomicileApiAttributesResponseDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserUpdateDomicileApiAttributesResponseDTO attributes) {
	    this.attributes = attributes;
	}
	

}