package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.AppointmentScheduleBlocksDataDTO;

import java.util.List;

/**
 * Created by Enrique on 29-Jun-17.
 */
public class GetAppointmentScheduleBlocksResponseDTO {

    List<AppointmentScheduleBlocksDataDTO> listAppointmentScheduleBlocksDataDTO;

    @JsonProperty("schedule_blocks")
    public List<AppointmentScheduleBlocksDataDTO> getListAppointmentScheduleBlocksDataDTO() {
        return listAppointmentScheduleBlocksDataDTO;
    }

    public void setListAppointmentScheduleBlocksDataDTO(List<AppointmentScheduleBlocksDataDTO> listAppointmentScheduleBlocksDataDTO) {
        this.listAppointmentScheduleBlocksDataDTO = listAppointmentScheduleBlocksDataDTO;
    }
}
