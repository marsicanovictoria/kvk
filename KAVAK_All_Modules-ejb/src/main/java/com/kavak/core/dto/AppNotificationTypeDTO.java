package com.kavak.core.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppNotificationTypeDTO {

	private Long id;
	private String title;
	private String description;
	private String iconUrl;
	private boolean isActive;
	private Timestamp creationDate;
	

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("title")
	public String getTitle() {
	    return title;
	}

	public void setTitle(String title) {
	    this.title = title;
	}

	@JsonProperty("description")
	public String getDescription() {
	    return description;
	}

	public void setDescription(String description) {
	    this.description = description;
	}

	@JsonProperty("icon_url")
	public String getIconUrl() {
	    return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
	    this.iconUrl = iconUrl;
	}

	@JsonProperty("creation_date")
	public Timestamp getCreationDate() {
	    return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
	    this.creationDate = creationDate;
	}

	@JsonProperty("is_active")
	public boolean isActive() {
	    return isActive;
	}

	public void setActive(boolean isActive) {
	    this.isActive = isActive;
	}









}
