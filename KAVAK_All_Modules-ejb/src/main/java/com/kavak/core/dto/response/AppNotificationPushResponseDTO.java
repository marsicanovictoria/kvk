package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppNotificationPushResponseDTO {
	
	private NotificationPushResponseDTO notification;
	private AppNotificationDataResponseDTO data;
	private String to;


	@JsonProperty("notification")
	public NotificationPushResponseDTO getNotification() {
	    return notification;
	}
	public void setNotification(NotificationPushResponseDTO notification) {
	    this.notification = notification;
	}
	
	@JsonProperty("data")
	public AppNotificationDataResponseDTO getData() {
	    return data;
	}
	public void setData(AppNotificationDataResponseDTO data) {
	    this.data = data;
	}
	
	@JsonProperty("to")
	public String getTo() {
	    return to;
	}
	public void setTo(String to) {
	    this.to = to;
	}








}
