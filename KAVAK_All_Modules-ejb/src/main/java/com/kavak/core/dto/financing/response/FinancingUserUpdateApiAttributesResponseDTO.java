package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateApiAttributesResponseDTO {

    private String name;
    private String firstSurname;
    private String secondSurname;
    private String gender;
    private String phone;
    private String mobilePhone;
    private String birthday;
    private String birthCountry;
    private String birthState;
    private String rfc;
    private String curp;
    private String employmentStatus;
    private String email;
    private String relationshipWithUser;
    private String lastStudyLevel;
    private String cellphoneModel;
    private String cellphonePayment;
    private String nickname;
    private String personalDataUseConsentment;
    private String personalDataTransferConsentment;
    private String updatedAt;

    @JsonProperty("name")
    public String getName() {
	return name;
    }
    public void setName(String name) {
	this.name = name;
    }

    @JsonProperty("firstSurname")
    public String getFirstSurname() {
	return firstSurname;
    }
    public void setFirstSurname(String firstSurname) {
	this.firstSurname = firstSurname;
    }

    @JsonProperty("secondSurname")
    public String getSecondSurname() {
	return secondSurname;
    }
    public void setSecondSurname(String secondSurname) {
	this.secondSurname = secondSurname;
    }

    @JsonProperty("gender")
    public String getGender() {
	return gender;
    }
    public void setGender(String gender) {
	this.gender = gender;
    }

    @JsonProperty("phone")
    public String getPhone() {
	return phone;
    }
    public void setPhone(String phone) {
	this.phone = phone;
    }

    @JsonProperty("mobilePhone")
    public String getMobilePhone() {
	return mobilePhone;
    }
    public void setMobilePhone(String mobilePhone) {
	this.mobilePhone = mobilePhone;
    }

    @JsonProperty("birthday")
    public String getBirthday() {
	return birthday;
    }
    public void setBirthday(String birthday) {
	this.birthday = birthday;
    }

    @JsonProperty("birthCountry")
    public String getBirthCountry() {
	return birthCountry;
    }
    public void setBirthCountry(String birthCountry) {
	this.birthCountry = birthCountry;
    }

    @JsonProperty("birthState")
    public String getBirthState() {
	return birthState;
    }
    public void setBirthState(String birthState) {
	this.birthState = birthState;
    }

    @JsonProperty("rfc")
    public String getRfc() {
	return rfc;
    }
    public void setRfc(String rfc) {
	this.rfc = rfc;
    }

    @JsonProperty("curp")
    public String getCurp() {
	return curp;
    }
    public void setCurp(String curp) {
	this.curp = curp;
    }

    @JsonProperty("employmentStatus")
    public String getEmploymentStatus() {
	return employmentStatus;
    }
    public void setEmploymentStatus(String employmentStatus) {
	this.employmentStatus = employmentStatus;
    }

    @JsonProperty("email")
    public String getEmail() {
	return email;
    }
    public void setEmail(String email) {
	this.email = email;
    }

    @JsonProperty("relationshipWithUser")
    public String getRelationshipWithUser() {
	return relationshipWithUser;
    }
    public void setRelationshipWithUser(String relationshipWithUser) {
	this.relationshipWithUser = relationshipWithUser;
    }

    @JsonProperty("lastStudyLevel")
    public String getLastStudyLevel() {
	return lastStudyLevel;
    }
    public void setLastStudyLevel(String lastStudyLevel) {
	this.lastStudyLevel = lastStudyLevel;
    }

    @JsonProperty("cellphoneModel")
    public String getCellphoneModel() {
	return cellphoneModel;
    }
    public void setCellphoneModel(String cellphoneModel) {
	this.cellphoneModel = cellphoneModel;
    }

    @JsonProperty("cellphonePayment")
    public String getCellphonePayment() {
	return cellphonePayment;
    }
    public void setCellphonePayment(String cellphonePayment) {
	this.cellphonePayment = cellphonePayment;
    }

    @JsonProperty("nickname")
    public String getNickname() {
	return nickname;
    }
    public void setNickname(String nickname) {
	this.nickname = nickname;
    }

    @JsonProperty("personalDataUseConsentment")
    public String getPersonalDataUseConsentment() {
	return personalDataUseConsentment;
    }
    public void setPersonalDataUseConsentment(String personalDataUseConsentment) {
	this.personalDataUseConsentment = personalDataUseConsentment;
    }

    @JsonProperty("personalDataTransferConsentment")
    public String getPersonalDataTransferConsentment() {
	return personalDataTransferConsentment;
    }
    public void setPersonalDataTransferConsentment(String personalDataTransferConsentment) {
	this.personalDataTransferConsentment = personalDataTransferConsentment;
    }

    @JsonProperty("updatedAt")
    public String getUpdatedAt() {
	return updatedAt;
    }
    public void setUpdatedAt(String updatedAt) {
	this.updatedAt = updatedAt;
    }



}