package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FireBaseResponseDTO {
	
	private String multicastId;
	private String succes;
	private String failure;
	private String canonicalIds;
	private ResultPushNotificationResponseDTO results;
	
	@JsonProperty("multicast_id")
	public String getMulticastId() {
	    return multicastId;
	}
	public void setMulticastId(String multicastId) {
	    this.multicastId = multicastId;
	}
	
	@JsonProperty("succes")
	public String getSucces() {
	    return succes;
	}
	public void setSucces(String succes) {
	    this.succes = succes;
	}
	
	@JsonProperty("failure")
	public String getFailure() {
	    return failure;
	}
	public void setFailure(String failure) {
	    this.failure = failure;
	}
	
	@JsonProperty("canonical_ids")
	public String getCanonicalIds() {
	    return canonicalIds;
	}
	public void setCanonicalIds(String canonicalIds) {
	    this.canonicalIds = canonicalIds;
	}
	
	@JsonProperty("results")
	public ResultPushNotificationResponseDTO getResults() {
	    return results;
	}
	public void setResults(ResultPushNotificationResponseDTO results) {
	    this.results = results;
	}
	
	
	
	



	




}