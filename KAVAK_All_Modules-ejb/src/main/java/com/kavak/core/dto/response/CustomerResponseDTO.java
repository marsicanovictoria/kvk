package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerResponseDTO {

	private Long id;
	private String name;
	private String username;
	private String email;
	private String role;
	private String phone;
	//private String address;
	private String addressNetsuite;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("full_name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@JsonProperty("role")
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/*
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	*/
	@JsonProperty("address_netsuite")
	public String getAddressNetsuite() {
		return getCleanAddress(addressNetsuite);
	}
	public void setAddressNetsuite(String addressNetsuite) {
		this.addressNetsuite = addressNetsuite;
	}
	
	public String getCleanAddress(String address){
		String newAddress = null;
		
		if (address != null){
			
			String[] split   = address.split("#");
			StringBuilder sb = new StringBuilder();
			
			for (int i = 0; i < split.length; i++) {
			    sb.append(split[i]);
			    if (i != split.length - 1) {
			        sb.append(", ");
			    }
			}
			newAddress = sb.toString();
		}
		return newAddress;
	}
}
