package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.financing.FinancingDataApiMarketPlaceRfcGeneratorDTO;

public class FinancingApiMarketPlaceRfcGeneratorDataResponseDTO {

    private Long code;
    private FinancingDataApiMarketPlaceRfcGeneratorDTO data;
    private String message;
    private String success;
    
    
    @JsonProperty("code")
    public Long getCode() {
        return code;
    }
    public void setCode(Long code) {
        this.code = code;
    }
    
    @JsonProperty("data")
    public FinancingDataApiMarketPlaceRfcGeneratorDTO getData() {
        return data;
    }
    public void setData(FinancingDataApiMarketPlaceRfcGeneratorDTO data) {
        this.data = data;
    }
    
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    
    @JsonProperty("success")
    public String getSuccess() {
        return success;
    }
    public void setSuccess(String success) {
        this.success = success;
    }

}
