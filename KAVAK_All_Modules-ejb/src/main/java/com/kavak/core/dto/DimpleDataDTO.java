package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DimpleDataDTO {

	private List<DimpleDataDetailDTO> listDimpleExtDataDetailDTO;
    private List<DimpleDataDetailDTO> listDimpleIntDataDetailDTO;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("external")
    public List<DimpleDataDetailDTO> getListDimpleExtDataDetailDTO() {
        return listDimpleExtDataDetailDTO;
    }

    public void setListDimpleExtDataDetailDTO(List<DimpleDataDetailDTO> listDimpleExtDataDetailDTO) {
        this.listDimpleExtDataDetailDTO = listDimpleExtDataDetailDTO;
    }
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("internal")
    public List<DimpleDataDetailDTO> getListDimpleIntDataDetailDTO() {
        return listDimpleIntDataDetailDTO;
    }

    public void setListDimpleIntDataDetailDTO(List<DimpleDataDetailDTO> listDimpleIntDataDetailDTO) {
        this.listDimpleIntDataDetailDTO = listDimpleIntDataDetailDTO;
    }

}
