package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Enrique on 29-Jun-17.
 */
public class AppointmentScheduleBlockDTO {

    private Long id;
    private String block;
    private boolean available;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("block")
    public String getBlock() {
        return block;
    }
    public void setBlock(String block) {
        this.block = block;
    }

    @JsonProperty("is_available")
    public boolean isAvailable() {
        return available;
    }
    public void setAvailable(boolean available) {
        this.available = available;
    }
}
