package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InspectionSchedulesBlockDTO {

	private String name;
	private List<InspectionSchedulesBlockDetailDTO> listInspectionSchedulesBlockDetailDTO;
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("inspection_schedules_block")
	public List<InspectionSchedulesBlockDetailDTO> getListInspectionSchedulesBlockDetailDTO() {
		return listInspectionSchedulesBlockDetailDTO;
	}
	public void setListInspectionSchedulesBlockDetailDTO(
			List<InspectionSchedulesBlockDetailDTO> listInspectionSchedulesBlockDetailDTO) {
		this.listInspectionSchedulesBlockDetailDTO = listInspectionSchedulesBlockDetailDTO;
	}
}
