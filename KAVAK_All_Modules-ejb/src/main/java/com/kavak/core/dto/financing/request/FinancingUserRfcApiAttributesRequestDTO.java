package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserRfcApiAttributesRequestDTO {

	private String name;
	private String firstSurname;
	private String secondSurname;
	private Long  day;
	private Long  month;
	private Long  year;
	private String rfc;

	
	@JsonProperty("name")	
	public String getName() {
	    return name;
	}
	public void setName(String name) {
	    this.name = name;
	}
	
	@JsonProperty("first_surname")	
	public String getFirstSurname() {
	    return firstSurname;
	}
	public void setFirstSurname(String firstSurname) {
	    this.firstSurname = firstSurname;
	}
	
	@JsonProperty("second_surname")	
	public String getSecondSurname() {
	    return secondSurname;
	}
	public void setSecondSurname(String secondSurname) {
	    this.secondSurname = secondSurname;
	}
	
	@JsonProperty("day")	
	public Long getDay() {
	    return day;
	}
	public void setDay(Long day) {
	    this.day = day;
	}
	
	@JsonProperty("month")	
	public Long getMonth() {
	    return month;
	}
	public void setMonth(Long month) {
	    this.month = month;
	}
	
	@JsonProperty("year")	
	public Long getYear() {
	    return year;
	}
	public void setYear(Long year) {
	    this.year = year;
	}
	
	@JsonProperty("rfc")
	public String getRfc() {
	    return rfc;
	}
	public void setRfc(String rfc) {
	    this.rfc = rfc;
	}




}