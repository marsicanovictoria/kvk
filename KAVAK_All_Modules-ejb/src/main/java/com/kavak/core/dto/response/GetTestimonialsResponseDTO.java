package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetTestimonialsResponseDTO {

    private String customerName;
    private String imageUrl;
    private String youtubeVideoId;
    private String testimonialsDescription;
    
    
    @JsonProperty("customer_name")
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    
    
    @JsonProperty("image_url")
    public String getImageUrl() {
        return imageUrl;
    }
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    
    
    @JsonProperty("youtube_video_id")
    public String getYoutubeVideoId() {
        return youtubeVideoId;
    }
    public void setYoutubeVideoId(String youtubeVideoId) {
        this.youtubeVideoId = youtubeVideoId;
    }
    
    
    @JsonProperty("testimonial_description")
    public String getTestimonialsDescription() {
        return testimonialsDescription;
    }
    public void setTestimonialsDescription(String testimonialsDescription) {
        this.testimonialsDescription = testimonialsDescription;
    }

   
}
