package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class ColonysByZipApiResponseDTO {

	private String zip;
	private String municipality;
	private String state;
	private Set<String> listColonys;

	@JsonProperty("codigo_postal")
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}

	@JsonProperty("municipio")
	public String getMunicipality() {
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	@JsonProperty("estado")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	@JsonProperty("colonias")
	public Set<String> getListColonys() {
		return listColonys;
	}
	public void setListColonys(Set<String> listColonys) {
		this.listColonys = listColonys;
	}
}