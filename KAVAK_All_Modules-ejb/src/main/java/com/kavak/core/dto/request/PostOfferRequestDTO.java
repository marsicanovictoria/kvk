package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PostOfferRequestDTO {

    private Long idUsuario;
    private Long carKm;
    private String zipCode;
    private String sku;
    private Long idExteriorColor;
    private Integer sendEmail;
    private String source;
    private Long internalUserId;
    private boolean disableCommunications; 
    private Long version;
    private Long idCheckpoint;

    @JsonProperty("user_id")
    public Long getIdUsuario() {
	return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
	this.idUsuario = idUsuario;
    }

    @JsonProperty("car_km")
    public Long getCarKm() {
	return carKm;
    }

    public void setCarKm(Long carKm) {
	this.carKm = carKm;
    }

    @JsonProperty("zip_code")
    public String getZipCode() {
	return zipCode;
    }

    public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
    }

    @JsonProperty("sku")
    public String getSku() {
	return sku;
    }

    public void setSku(String sku) {
	this.sku = sku;
    }

    @JsonProperty("exterior_color_id")
    public Long getIdExteriorColor() {
	return idExteriorColor;
    }

    public void setIdExteriorColor(Long idExteriorColor) {
	this.idExteriorColor = idExteriorColor;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("sent_email")
    public Integer getSendEmail() {
	return sendEmail;
    }

    public void setSendEmail(Integer sendEmail) {
	this.sendEmail = sendEmail;
    }

    @JsonProperty("source")
    public String getSource() {
	return source;
    }

    public void setSource(String source) {
	this.source = source;
    }

    @JsonProperty("internal_user_id")
    public Long getInternalUserId() {
	return internalUserId;
    }

    public void setInternalUserId(Long internalUserId) {
	this.internalUserId = internalUserId;
    }

    @JsonProperty("disable_communications")
    public boolean isDisableCommunications() {
        return disableCommunications;
    }

    public void setDisableCommunications(boolean disableCommunications) {
        this.disableCommunications = disableCommunications;
    }

    @JsonProperty("version")
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @JsonProperty("id")
    public Long getIdCheckpoint() {
        return idCheckpoint;
    }

    public void setIdCheckpoint(Long idCheckpoint) {
        this.idCheckpoint = idCheckpoint;
    }
}