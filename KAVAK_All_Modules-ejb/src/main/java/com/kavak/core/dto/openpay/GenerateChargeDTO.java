package com.kavak.core.dto.openpay;

import com.kavak.core.dto.BillingAddressDTO;

/**
 * Contiene los campos necesario para generar un Checkout en Openpay (User + Card + Bank)
 * Se guarda en las tablas transaccionales
 *
 */
public class GenerateChargeDTO {

    private Long userId;
    private BillingAddressDTO billingAddressDTO;
    private String tokenId;
    private String deviceSessionId;
    private Long carId;
    private Long paymentTypeId;
    private String totalPrice;
    private String email; // Solo se envia para las pruebas de German german.mendoza@kavak.com
    private Long offerCheckpointId;
    private Long carPrice;
    private Long downPayment;
    private Integer financingMonths;
    private Boolean hasTradeIn;
    private Long checkpointId;
    private Long warrantyId;
    private Long insuranceId;

    public BillingAddressDTO getBillingAddressDTO() {
        return billingAddressDTO;
    }
    public void setBillingAddressDTO(BillingAddressDTO billingAddressDTO) {
        this.billingAddressDTO = billingAddressDTO;
    }
    public String getTokenId() {
        return tokenId;
    }
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getDeviceSessionId() {
        return deviceSessionId;
    }
    public void setDeviceSessionId(String deviceSessionId) {
        this.deviceSessionId = deviceSessionId;
    }

    public Long getCarId() {
        return carId;
    }
    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public Long getPaymentTypeId() {
        return paymentTypeId;
    }
    public void setPaymentTypeId(Long paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getTotalPrice() {
        return totalPrice;
    }
    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public Long getOfferCheckpointId() {
        return offerCheckpointId;
    }
    public void setOfferCheckpointId(Long offerCheckpointId) {
        this.offerCheckpointId = offerCheckpointId;
    }

    public Long getCarPrice() {
        return carPrice;
    }
    public void setCarPrice(Long carPrice) {
        this.carPrice = carPrice;
    }

    public Long getDownPayment() {
        return downPayment;
    }
    public void setDownPayment(Long downPayment) {
        this.downPayment = downPayment;
    }

    public Integer getFinancingMonths() {
        return financingMonths;
    }
    public void setFinancingMonths(Integer financingMonths) {
        this.financingMonths = financingMonths;
    }

    public Boolean getHasTradeIn() {
        return hasTradeIn;
    }
    public void setHasTradeIn(Boolean hasTradeIn) {
        this.hasTradeIn = hasTradeIn;
    }

    public Long getCheckpointId() {
        return checkpointId;
    }
    public void setCheckpointId(Long checkpointId) {
        this.checkpointId = checkpointId;
    }

    public Long getWarrantyId() {
        return warrantyId;
    }
    public void setWarrantyId(Long warrantyId) {
        this.warrantyId = warrantyId;
    }

    public Long getInsuranceId() {
        return insuranceId;
    }
    public void setInsuranceId(Long insuranceId) {
        this.insuranceId = insuranceId;
    }

    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
