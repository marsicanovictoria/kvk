package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.ApiUrlShortenerDataDTO;

public class ApiUrlShortenerResponseDTO {

	private String statusCode;
	private String statustxt;
	private ApiUrlShortenerDataDTO data;
	
	@JsonProperty("status_code")
	public String getStatusCode() {
	    return statusCode;
	}
	public void setStatusCode(String statusCode) {
	    this.statusCode = statusCode;
	}
	
	@JsonProperty("status_txt")
	public String getStatustxt() {
	    return statustxt;
	}
	public void setStatustxt(String statustxt) {
	    this.statustxt = statustxt;
	}
	
	@JsonProperty("data")
	public ApiUrlShortenerDataDTO getData() {
	    return data;
	}
	public void setData(ApiUrlShortenerDataDTO data) {
	    this.data = data;
	}

}
