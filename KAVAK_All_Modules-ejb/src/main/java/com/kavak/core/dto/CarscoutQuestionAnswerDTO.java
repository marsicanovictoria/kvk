package com.kavak.core.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarscoutQuestionAnswerDTO {

    private Long id;
    private Long userId;
    private Long alertId;
    private Long questionId;
    private Long optionId;
    private Timestamp updateDate;

    @JsonProperty("id")
    public Long getId() {
	return id;
    }
    public void setId(Long id) {
	this.id = id;
    }

    @JsonProperty("user_id")
    public Long getUserId() {
	return userId;
    }
    public void setUserId(Long userId) {
	this.userId = userId;
    }


    @JsonProperty("question_id")
    public Long getQuestionId() {
	return questionId;
    }
    public void setQuestionId(Long questionId) {
	this.questionId = questionId;
    }

    @JsonProperty("option_id")
    public Long getOptionId() {
	return optionId;
    }
    public void setOptionId(Long optionId) {
	this.optionId = optionId;
    }

    @JsonProperty("update_date")
    public Timestamp getUpdateDate() {
	return updateDate;
    }
    public void setUpdateDate(Timestamp updateDate) {
	this.updateDate = updateDate;
    }
    
    @JsonProperty("alert_id")
    public Long getAlertId() {
        return alertId;
    }
    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }




}