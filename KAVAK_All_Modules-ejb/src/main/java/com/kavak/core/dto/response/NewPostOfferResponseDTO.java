package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.UserCheckPointsDTO;

public class NewPostOfferResponseDTO {
	
	private Long year;
	private String make;
	private String model;
	private String trim;
	private Long color;
	private String sku;
	private Long km;
	private String postalCode;
	private Boolean servedZone;
	private OffersResponseDTO offerResponseDTO;
	private UserCheckPointsDTO userPostOffer;
	
	
	@JsonProperty("year")
	public Long getYear() {
		return year;
	}
	public void setYear(Long year) {
		this.year = year;
	}
	@JsonProperty("make")
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	@JsonProperty("model")
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	@JsonProperty("trim")
	public String getTrim() {
		return trim;
	}
	public void setTrim(String trim) {
		this.trim = trim;
	}
	@JsonProperty("color")
	public Long getColor() {
		return color;
	}
	public void setColor(Long color) {
		this.color = color;
	}
	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	@JsonProperty("km")
	public Long getKm() {
		return km;
	}
	public void setKm(Long km) {
		this.km = km;
	}
	@JsonProperty("zip_code")
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	@JsonProperty("served_zone")
	public Boolean getServedZone() {
		return servedZone;
	}
	public void setServedZone(Boolean servedZone) {
		this.servedZone = servedZone;
	}
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("offers")
	public OffersResponseDTO getOfferResponseDTO() {
		return offerResponseDTO;
	}
	public void setOfferResponseDTO(OffersResponseDTO offerResponseDTO) {
		this.offerResponseDTO = offerResponseDTO;
	}
	@JsonProperty("user")
	public UserCheckPointsDTO getUserPostOffer() {
		return userPostOffer;
	}
	public void setUserPostOffer(UserCheckPointsDTO userPostOffer) {
		this.userPostOffer = userPostOffer;
	}
	
	

}
