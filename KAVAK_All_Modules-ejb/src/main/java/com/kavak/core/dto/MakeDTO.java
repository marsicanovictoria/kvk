package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MakeDTO {
	
	private Long id;
	private String name;
	private String image;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("image")
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
}