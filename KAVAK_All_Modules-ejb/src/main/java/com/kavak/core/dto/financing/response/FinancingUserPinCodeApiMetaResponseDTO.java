package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserPinCodeApiMetaResponseDTO {

	private String status;
	private String message;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("status")
	public String getStatus() {
	    return status;
	}
	public void setStatus(String status) {
	    this.status = status;
	}
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("message")
	public String getMessage() {
	    return message;
	}
	public void setMessage(String message) {
	    this.message = message;
	}

}