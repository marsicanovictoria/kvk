package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.InspectionCarFeatureDTO;
import com.kavak.core.dto.InspectionDimpleDTO;

import java.util.List;

/**
 * Created by Enrique on 16-Jun-17.
 */
public class GetCarContentOverviewResponse {

    private List<InspectionCarFeatureDTO> listInspectionCarFeatureDTO;
    private List<InspectionDimpleDTO> listInspectionDimpleDTO;

    @JsonProperty("inspection_car_features")
    public List<InspectionCarFeatureDTO> getListInspectionCarFeatureDTO() {
        return listInspectionCarFeatureDTO;
    }

    public void setListInspectionCarFeatureDTO(List<InspectionCarFeatureDTO> listInspectionCarFeatureDTO) {
        this.listInspectionCarFeatureDTO = listInspectionCarFeatureDTO;
    }
    @JsonProperty("inspection_dimples")
    public List<InspectionDimpleDTO> getListInspectionDimpleDTO() {
        return listInspectionDimpleDTO;
    }

    public void setListInspectionDimpleDTO(List<InspectionDimpleDTO> listInspectionDimpleDTO) {
        this.listInspectionDimpleDTO = listInspectionDimpleDTO;
    }
}
