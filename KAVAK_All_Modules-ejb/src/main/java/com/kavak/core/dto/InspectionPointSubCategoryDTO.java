package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InspectionPointSubCategoryDTO {
	
	private Long id;
	private String name;
	private boolean active;
	private List<InspectionPointDTO> listInspectionPointDTO;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@JsonProperty("inspection_points_sub_category")
	public List<InspectionPointDTO> getListInspectionPointDTO() {
		return listInspectionPointDTO;
	}
	public void setListInspectionPointDTO(List<InspectionPointDTO> listInspectionPointDTO) {
		this.listInspectionPointDTO = listInspectionPointDTO;
	}
	
}
