package com.kavak.core.dto.netsuite;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.CarDataDTO;
import com.kavak.core.dto.UserDTO;
import com.kavak.core.dto.response.CustomerResponseDTO;

public class PurchaseOpportunityResponseDTO {

	private int id;
	private Long leadMinervaId;
	private Long netsuiteItemId;
	private String registerDate;
	private String registerTime;
	private Double max30DaysOffer;
	private Double min30DaysOffer;
	private Double maxInstantOffer;
	private Double minInstantOffer;
	private Double maxConsigmentOffer;
	private Double minConsigmentOffer;
	private Double autometricGuidePurchasePrice;
	private Double autometricGuideSalePrice;
	private Long inspectionLocationTypeId;
	private String inspectionLocationTypeDescription;
	private String inspectionLocationDescription;
	private Long inspectionScheduleTime;
	private String inspectionScheduleTimeDesc;
	private String inspectionScheduleDate;
	private String circulationCardUrl;
	private String carInvoiceUrl;
	private Long statusId;
	private String statusDetail;
	private Long zipCode;
	private Long carKm;
	private Long offerType;
	private String offerTypeDescription;
	private Long duplicatedOfferControl;
	private String gaVersion;
	private Double estimateSalePrice;
	private Double marketPrice;
	private String exteriorColor;
	private String offerDetail;
	private String reliabilitySample;
	private String updateDateOffer; 
	private Long wishlist;
	private String source;
	private boolean abTestApplied;
    private Long abTestResult;
    private String abTestCode;
    private String abTestDescription;
	private String abTestStageDescription;
    private CarDataDTO car;
	private CustomerResponseDTO customer;
	//private UserDTO experienceManager;
	private UserDTO wingmanInspection;

	
	@JsonProperty("id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty("lead_minerva_id")
	public Long getLeadMinervaId() {
		return leadMinervaId;
	}
	public void setLeadMinervaId(Long leadMinervaId) {
		this.leadMinervaId = leadMinervaId;
	}
	@JsonProperty("netsuite_item_id")
	public Long getNetsuiteItemId() {
		return netsuiteItemId;
	}
	public void setNetsuiteItemId(Long netsuiteItemId) {
		this.netsuiteItemId = netsuiteItemId;
	}
	
	@JsonProperty("register_date")
	public String getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	
	@JsonProperty("register_time")
	public String getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}
	
	@JsonProperty("update_date")
	public String getUpdateDateOffer() {
		return updateDateOffer;
	}
	public void setUpdateDateOffer(String updateDateOffer) {
		this.updateDateOffer = updateDateOffer;
	}
	@JsonProperty("max_30_days_offer")
	public Double getMax30DaysOffer() {
		return max30DaysOffer;
	}
	public void setMax30DaysOffer(Double max30DaysOffer) {
		this.max30DaysOffer = max30DaysOffer;
	}
	
	@JsonProperty("min_30_days_offer")
	public Double getMin30DaysOffer() {
		return min30DaysOffer;
	}
	public void setMin30DaysOffer(Double min30DaysOffer) {
		this.min30DaysOffer = min30DaysOffer;
	}
	
	@JsonProperty("max_instant_offer")
	public Double getMaxInstantOffer() {
		return maxInstantOffer;
	}
	public void setMaxInstantOffer(Double maxInstantOffer) {
		this.maxInstantOffer = maxInstantOffer;
	}
	
	@JsonProperty("min_instant_offer")
	public Double getMinInstantOffer() {
		return minInstantOffer;
	}
	public void setMinInstantOffer(Double minInstantOffer) {
		this.minInstantOffer = minInstantOffer;
	}
	
	@JsonProperty("max_consignment_offer")
	public Double getMaxConsigmentOffer() {
		return maxConsigmentOffer;
	}
	public void setMaxConsigmentOffer(Double maxConsigmentOffer) {
		this.maxConsigmentOffer = maxConsigmentOffer;
	}
	
	@JsonProperty("min_consignment_offer")
	public Double getMinConsigmentOffer() {
		return minConsigmentOffer;
	}
	public void setMinConsigmentOffer(Double minConsigmentOffer) {
		this.minConsigmentOffer = minConsigmentOffer;
	}

	@JsonProperty("autometric_guide_purchase_price")
	public Double getAutometricGuidePurchasePrice() {
		return autometricGuidePurchasePrice;
	}
	public void setAutometricGuidePurchasePrice(Double autometricGuidePurchasePrice) {
		this.autometricGuidePurchasePrice = autometricGuidePurchasePrice;
	}
	
	@JsonProperty("autometric_guide_sale_price")
	public Double getAutometricGuideSalePrice() {
		return autometricGuideSalePrice;
	}
	public void setAutometricGuideSalePrice(Double autometricGuideSalePrice) {
		this.autometricGuideSalePrice = autometricGuideSalePrice;
	}
	
	@JsonProperty("inspection_location_type_id")
	public Long getInspectionLocationTypeId() {
		return inspectionLocationTypeId;
	}
	public void setInspectionLocationTypeId(Long inspectionLocationTypeId) {
		this.inspectionLocationTypeId = inspectionLocationTypeId;
	}
	
	@JsonProperty("inspection_location_type_description")
	public String getInspectionLocationTypeDescription() {
		return inspectionLocationTypeDescription;
	}
	public void setInspectionLocationTypeDescription(String inspectionLocationTypeDescription) {
		this.inspectionLocationTypeDescription = inspectionLocationTypeDescription;
	}
	@JsonProperty("inspection_location")
	public String getInspectionLocationDescription() {
		return inspectionLocationDescription;
	}
	public void setInspectionLocationDescription(String inspectionLocationDescription) {
		this.inspectionLocationDescription = inspectionLocationDescription;
	}
	
	@JsonProperty("scheduled_time")
	public Long getInspectionScheduleTime() {
		return inspectionScheduleTime;
	}
	public void setInspectionScheduleTime(Long inspectionScheduleTime) {
		this.inspectionScheduleTime = inspectionScheduleTime;
	}
	
	@JsonProperty("scheduled_time_description")
	public String getInspectionScheduleTimeDesc() {
		return inspectionScheduleTimeDesc;
	}
	public void setInspectionScheduleTimeDesc(String inspectionScheduleTimeDesc) {
		this.inspectionScheduleTimeDesc = inspectionScheduleTimeDesc;
	}
	
	@JsonProperty("scheduled_date")
	public String getInspectionScheduleDate() {
		return inspectionScheduleDate;
	}
	public void setInspectionScheduleDate(String inspectionScheduleDate) {
		this.inspectionScheduleDate = inspectionScheduleDate;
	}

	@JsonProperty("circulation_card_url")
	public String getCirculationCardUrl() {
		return circulationCardUrl;
	}
	public void setCirculationCardUrl(String circulationCardUrl) {
		this.circulationCardUrl = circulationCardUrl;
	}
	
	@JsonProperty("car_invoice_url")
	public String getCarInvoiceUrl() {
		return carInvoiceUrl;
	}
	public void setCarInvoiceUrl(String carInvoiceUrl) {
		this.carInvoiceUrl = carInvoiceUrl;
	}
	
	@JsonProperty("car_data")
	public CarDataDTO getCar() {
		return car;
	}
	public void setCar(CarDataDTO car) {
		this.car = car;
	}
	
	@JsonProperty("status_checkpoint_id")
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	
	@JsonProperty("status_checkpoint_detail")
	public String getStatusDetail() {
		return statusDetail;
	}
	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}
	
	@JsonProperty("zip_code")
	public Long getZipCode() {
		return zipCode;
	}
	public void setZipCode(Long zipCode) {
		this.zipCode = zipCode;
	}
	
	@JsonProperty("car_km")
	public Long getCarKm() {
		return carKm;
	}
	public void setCarKm(Long carKm) {
		this.carKm = carKm;
	}
	
	@JsonProperty("offer_type_id")
	public Long getOfferType() {
		return offerType;
	}
	public void setOfferType(Long offerType) {
		this.offerType = offerType;
	}
	
	@JsonProperty("offer_type_description")
	public String getOfferTypeDescription() {
		return offerTypeDescription;
	}
	public void setOfferTypeDescription(String offerTypeDescription) {
		this.offerTypeDescription = offerTypeDescription;
	}
	
	@JsonProperty("duplicated_offer_control")
	public Long getDuplicatedOfferControl() {
		return duplicatedOfferControl;
	}

	public void setDuplicatedOfferControl(Long duplicatedOfferControl) {
		this.duplicatedOfferControl = duplicatedOfferControl;
	}
	
	@JsonProperty("ga_version")
	public String getGaVersion() {
		return gaVersion;
	}
	public void setGaVersion(String gaVersion) {
		this.gaVersion = gaVersion;
	}

	@JsonProperty("estimate_sale_price")
	public Double getEstimateSalePrice() {
		return estimateSalePrice;
	}
	public void setEstimateSalePrice(Double estimateSalePrice) {
		this.estimateSalePrice = estimateSalePrice;
	}
	
	@JsonProperty("market_price")
	public Double getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}
	
	@JsonProperty("reliability_sample")
	public String getReliabilitySample() {
		return reliabilitySample;
	}
	public void setReliabilitySample(String reliabilitySample) {
		this.reliabilitySample = reliabilitySample;
	}

	@JsonProperty("customer_data")
	public CustomerResponseDTO getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerResponseDTO customer) {
		this.customer = customer;
	}
/*
	@JsonProperty("experience_manager_data")
	public UserDTO getExperienceManager() {
		return experienceManager;
	}
	public void setExperienceManager(UserDTO experienceManager) {
		this.experienceManager = experienceManager;
	}
*/
	@JsonProperty("wingman_inspection")
	public UserDTO getWingmanInspection() {
		return wingmanInspection;
	}
	public void setWingmanInspection(UserDTO wingmanInspection) {
		this.wingmanInspection = wingmanInspection;
	}
	
	@JsonProperty("exterior_color")
	public String getExteriorColor() {
		return exteriorColor;
	}
	public void setExteriorColor(String exteriorColor) {
		this.exteriorColor = exteriorColor;
	}
	
	@JsonProperty("offer_detail")
	public String getOfferDetail() {
		return offerDetail;
	}
	public void setOfferDetail(String offerDetail) {
		this.offerDetail = offerDetail;
	}
	
	@JsonProperty("wishlist")
	public Long getWishlist() {
		return wishlist;
	}
	public void setWishlist(Long wishlist) {
		this.wishlist = wishlist;
	}
	
	@JsonProperty("source")
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	@JsonProperty("is_ab_test_applied")
	public boolean isAbTestApplied() {
		return abTestApplied;
	}
	public void setAbTestApplied(boolean abTestApplied) {
		this.abTestApplied = abTestApplied;
	}
	
	@JsonProperty("ab_test_result")
	public Long getAbTestResult() {
		return abTestResult;
	}
	public void setAbTestResult(Long abTestResult) {
		this.abTestResult = abTestResult;
	}
	
	@JsonProperty("ab_test_code")
	public String getAbTestCode() {
		return abTestCode;
	}
	public void setAbTestCode(String abTestCode) {
		this.abTestCode = abTestCode;
	}
	
	@JsonProperty("ab_test_description")
	public String getAbTestDescription() {
		return abTestDescription;
	}
	public void setAbTestDescription(String abTestDescription) {
		this.abTestDescription = abTestDescription;
	}
	
	@JsonProperty("ab_test_stage_description")
	public String getAbTestStageDescription() {
		return abTestStageDescription;
	}
	public void setAbTestStageDescription(String abTestStageDescription) {
		this.abTestStageDescription = abTestStageDescription;
	}

}
