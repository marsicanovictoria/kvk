package com.kavak.core.dto;

public class SerializedDTO {

	private String why_car;
	private String fepm;
	private String booking_price;
	
	public String getWhy_car() {
		return why_car;
	}
	public void setWhy_car(String why_car) {
		this.why_car = why_car;
	}
	public String getFepm() {
		return fepm;
	}
	public void setFepm(String fepm) {
		this.fepm = fepm;
	}
	public String getBooking_price() {
		return booking_price;
	}
	public void setBooking_price(String booking_price) {
		this.booking_price = booking_price;
	}
	
}
