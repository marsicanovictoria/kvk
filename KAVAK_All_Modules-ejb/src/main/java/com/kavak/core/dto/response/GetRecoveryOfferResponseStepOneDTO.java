package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetRecoveryOfferResponseStepOneDTO {
	
	private Long offerCheckpointId;
	private String offerDate;
	private Integer step;
	private String postalCode;
	private Boolean servedZone;
	private OffersResponseDTO offers;
	
	@JsonProperty("offer_checkpoint_id")
	public Long getOfferCheckpointId() {
		return offerCheckpointId;
	}
	public void setOfferCheckpointId(Long offerCheckpointId) {
		this.offerCheckpointId = offerCheckpointId;
	}
	@JsonProperty("offerDate")
	public String getOfferDate() {
		return offerDate;
	}
	public void setOfferDate(String offerDate) {
		this.offerDate = offerDate;
	}
	@JsonProperty("step")
	public Integer getStep() {
		return step;
	}
	public void setStep(Integer step) {
		this.step = step;
	}
	@JsonProperty("postalCode")
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	@JsonProperty("served_zone")
	public Boolean getServedZone() {
		return servedZone;
	}
	public void setServedZone(Boolean servedZone) {
		this.servedZone = servedZone;
	}
	@JsonProperty("offers")
	public OffersResponseDTO getOffers() {
		return offers;
	}
	public void setOffers(OffersResponseDTO offers) {
		this.offers = offers;
	}
	

}
