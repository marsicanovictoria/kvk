package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InspectionSummaryDataDTO {

	private InspectionSummaryDTO summaryDTO;
	private List<InspectionDiscountDTO> lisInspectionDiscountDTO;
	private List<InspectionRepairDTO> listInspectionRepairDTO;
	private List<InspectionOfferDTO> listInspectionOfferDTO;


	@JsonProperty("summary")
	public InspectionSummaryDTO getSummaryDTO() {
		return summaryDTO;
	}
	public void setSummayDTO(InspectionSummaryDTO summaryDTO) {
		this.summaryDTO = summaryDTO;
	}

	@JsonProperty("discounts")
	public List<InspectionDiscountDTO> getLisInspectionDiscountDTO() {
		return lisInspectionDiscountDTO;
	}
	public void setLisInspectionDiscountDTO(List<InspectionDiscountDTO> lisInspectionDiscountDTO) {
		this.lisInspectionDiscountDTO = lisInspectionDiscountDTO;
	}

	@JsonProperty("repairs")
	public List<InspectionRepairDTO> getListInspectionRepairDTO() {
		return listInspectionRepairDTO;
	}
	public void setListInspectionRepairDTO(List<InspectionRepairDTO> listInspectionRepairDTO) {
		this.listInspectionRepairDTO = listInspectionRepairDTO;
	}
	
	@JsonProperty("offers")
	public List<InspectionOfferDTO> getListInspectionOfferDTO() {
		return listInspectionOfferDTO;
	}
	public void setListInspectionOfferDTO(List<InspectionOfferDTO> listInspectionOfferDTO) {
		this.listInspectionOfferDTO = listInspectionOfferDTO;
	}
}
