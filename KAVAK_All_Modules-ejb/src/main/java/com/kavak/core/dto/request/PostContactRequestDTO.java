package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostContactRequestDTO {

    private String names;
    private String lastNames;
    private String phone;
    private String zipCode;
    private String email;
    private Integer subjectId;
    private String comment;
    
    
    @JsonProperty("names")
    public String getNames() {
        return names;
    }
    public void setNames(String names) {
        this.names = names;
    }
    
    @JsonProperty("last_names")
    public String getLastNames() {
        return lastNames;
    }
    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }
    
    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    @JsonProperty("zip_code")
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    @JsonProperty("subject_id")
    public Integer getSubjectId() {
        return subjectId;
    }
    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }
    
    @JsonProperty("comments")
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }




}