package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class InspectionSummaryDTO {

	private Long id;
	private Long idInspection;
	private String inspectorEmail;
	private Long idOfferType;
	private Double baseOfferAmount;
	private Double totalDiscount;
	private Double finalOfferAmount;
	private boolean selected;
	private boolean inspectionApproved;
    private Long totalRepairsAmount;
    private Long totalBonusAmount;
	private String customerSignatureImage;
    private Long inspectionTypeId;
    private BigDecimal latitudeInspection;
    private BigDecimal longitudInspection;
    private Long inspectionStatus;
    private String inspectorComment;

    @JsonProperty("id")
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}

	@JsonProperty("inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}

	@JsonProperty("offer_type_id")
	public Long getIdOfferType() {
		return idOfferType;
	}
	public void setIdOfferType(Long idOfferType) {
		this.idOfferType = idOfferType;
	}

	@JsonProperty("base_offer_amount")
	public Double getBaseOfferAmount() {
		return baseOfferAmount;
	}
	public void setBaseOfferAmount(Double baseOfferAmount) {
		this.baseOfferAmount = baseOfferAmount;
	}

	@JsonProperty("total_discounts")
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	@JsonProperty("final_offer_amount")
	public Double getFinalOfferAmount() {
		return finalOfferAmount;
	}
	public void setFinalOfferAmount(Double finalOfferAmount) {
		this.finalOfferAmount = finalOfferAmount;
	}

	@JsonProperty("is_selected")
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@JsonProperty("is_inspection_approved")
	public boolean isInspectionApproved() {
		return inspectionApproved;
	}
	public void setInspectionApproved(boolean inspectionApproved) {
		this.inspectionApproved = inspectionApproved;
	}

    @JsonProperty("total_repairs_amount")
    public Long getTotalRepairsAmount() {
        return totalRepairsAmount;
    }
    public void setTotalRepairsAmount(Long totalRepairsAmount) {
        this.totalRepairsAmount = totalRepairsAmount;
    }

    @JsonProperty("total_bonus_amount")
    public Long getTotalBonusAmount() {
        return totalBonusAmount;
    }
    public void setTotalBonusAmount(Long totalBonusAmount) {
        this.totalBonusAmount = totalBonusAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("customer_signature_image")
	public String getCustomerSignatureImage() {
		return customerSignatureImage;
	}
	public void setCustomerSignatureImage(String customerSignatureImage) {
		this.customerSignatureImage = customerSignatureImage;
	}


    @JsonProperty("inspection_type_id")
    public Long getInspectionTypeId() {
        return inspectionTypeId;
    }
    public void setInspectionTypeId(Long inspectionTypeId) {
        this.inspectionTypeId = inspectionTypeId;
    }

	@JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("latitude_inspection")
    public BigDecimal getLatitudeInspection() {
        return latitudeInspection;
    }
    public void setLatitudeInspection(BigDecimal latitudeInspection) {
        this.latitudeInspection = latitudeInspection;
    }

	@JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("longitude_inspection")
    public BigDecimal getLongitudInspection() {
        return longitudInspection;
    }
    public void setLongitudInspection(BigDecimal longitudInspection) {
        this.longitudInspection = longitudInspection;
    }
    
    @JsonProperty("inspection_status")
	public Long getInspectionStatus() {
		return inspectionStatus;
	}
	public void setInspectionStatus(Long inspectionStatus) {
		this.inspectionStatus = inspectionStatus;
	}
	
	@JsonProperty("inspector_comment")
	public String getInspectorComment() {
	    return inspectorComment;
	}
	public void setInspectorComment(String inspectorComment) {
	    this.inspectorComment = inspectorComment;
	}
}
