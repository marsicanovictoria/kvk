package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Enrique on 11-Jul-17.
 */
public class PostAppointmentResponseDTO {

    private String errorTitle;
    private String errorDescription;
    private String butonTitle;
    private String buttonUrl;

    @JsonProperty("error_title")
    public String getErrorTitle() {
        return errorTitle;
    }

    public void setErrorTitle(String errorTitle) {
        this.errorTitle = errorTitle;
    }
    @JsonProperty("error_description")
    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
    @JsonProperty("button_title")
    public String getButonTitle() {
        return butonTitle;
    }

    public void setButonTitle(String butonTitle) {
        this.butonTitle = butonTitle;
    }
    @JsonProperty("button_url")
    public String getButtonUrl() {
        return buttonUrl;
    }

    public void setButtonUrl(String buttonUrl) {
        this.buttonUrl = buttonUrl;
    }

}
