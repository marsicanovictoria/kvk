package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PressnotesResponseDTO {

	private String title;
	private String imageUrl;
	private String externalUrl;
	
	@JsonProperty("title")
	public String getTitle() {
	    return title;
	}
	public void setTitle(String title) {
	    this.title = title;
	}
	
	@JsonProperty("image_url")
	public String getImageUrl() {
	    return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
	    this.imageUrl = imageUrl;
	}
	
	@JsonProperty("external_url")
	public String getExternalUrl() {
	    return externalUrl;
	}
	public void setExternalUrl(String externalUrl) {
	    this.externalUrl = externalUrl;
	}


}