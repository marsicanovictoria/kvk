package com.kavak.core.dto;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConfirmedInspectionDTO {

	private Long id;
	private Long offerId;
	private Long userId;
	private Long inspectorId;
	private Long leadManagerId;
	private String thirtyOfferMax;
	private String instantOfferMax;
	private String consignmentOfferMax;
	private String carYear;
	private String carMake;
	private String carModel;
	private String inspectionDate;
	private String inspectionTimeStart;
	private String inspectionTimeEnd;
	private String inspectionAddress;
	private Timestamp registerDate;
	private Long active;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("offer_id")
	public Long getOfferId() {
		return offerId;
	}
	public void setOfferId(Long offerId) {
		this.offerId = offerId;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@JsonProperty("inspector_id")
	public Long getInspectorId() {
		return inspectorId;
	}
	public void setInspectorId(Long inspectorId) {
		this.inspectorId = inspectorId;
	}
	
	@JsonProperty("lead_manager_id")
	public Long getLeadManagerId() {
		return leadManagerId;
	}
	public void setLeadManagerId(Long leadManagerId) {
		this.leadManagerId = leadManagerId;
	}
	
	@JsonProperty("thirty_offer_max")
	public String getThirtyOfferMax() {
		return thirtyOfferMax;
	}
	public void setThirtyOfferMax(String thirtyOfferMax) {
		this.thirtyOfferMax = thirtyOfferMax;
	}
	
	@JsonProperty("instant_offer_max")
	public String getInstantOfferMax() {
		return instantOfferMax;
	}
	public void setInstantOfferMax(String instantOfferMax) {
		this.instantOfferMax = instantOfferMax;
	}
	
	@JsonProperty("consignment_offer_max")
	public String getConsignmentOfferMax() {
		return consignmentOfferMax;
	}
	public void setConsignmentOfferMax(String consignmentOfferMax) {
		this.consignmentOfferMax = consignmentOfferMax;
	}
	
	@JsonProperty("car_year")
	public String getCarYear() {
		return carYear;
	}
	public void setCarYear(String carYear) {
		this.carYear = carYear;
	}
	
	@JsonProperty("car_make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}
	
	@JsonProperty("car_model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	
	@JsonProperty("inspection_date")
	public String getInspectionDate() {
		return inspectionDate;
	}
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}
	
	@JsonProperty("inspection_time_start")
	public String getInspectionTimeStart() {
		return inspectionTimeStart;
	}
	public void setInspectionTimeStart(String inspectionTimeStart) {
		this.inspectionTimeStart = inspectionTimeStart;
	}
	
	@JsonProperty("inspection_time_end")
	public String getInspectionTimeEnd() {
		return inspectionTimeEnd;
	}
	public void setInspectionTimeEnd(String inspectionTimeEnd) {
		this.inspectionTimeEnd = inspectionTimeEnd;
	}
	
	@JsonProperty("inspection_address")
	public String getInspectionAddress() {
		return inspectionAddress;
	}
	public void setInspectionAddress(String inspectionAddress) {
		this.inspectionAddress = inspectionAddress;
	}
	
	@JsonProperty("register_date")
	public Timestamp getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Timestamp registerDate) {
		this.registerDate = registerDate;
	}
	
	@JsonProperty("active")
	public Long getActive() {
		return active;
	}
	public void setActive(Long active) {
		this.active = active;
	}
}
