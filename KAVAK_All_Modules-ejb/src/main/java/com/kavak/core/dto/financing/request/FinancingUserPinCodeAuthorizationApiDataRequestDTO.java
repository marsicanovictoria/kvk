package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserPinCodeAuthorizationApiDataRequestDTO {

	private String type;
	private FinancingUserPinCodeAuthorizationApiAttributesRequestDTO attributes;
	

	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("attributes")
	public FinancingUserPinCodeAuthorizationApiAttributesRequestDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserPinCodeAuthorizationApiAttributesRequestDTO attributes) {
	    this.attributes = attributes;
	}

}