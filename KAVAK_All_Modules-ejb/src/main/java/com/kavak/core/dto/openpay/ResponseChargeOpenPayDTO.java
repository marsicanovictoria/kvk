package com.kavak.core.dto.openpay;

import com.kavak.core.dto.response.OrderResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import mx.openpay.client.Charge;
import mx.openpay.client.Customer;

public class ResponseChargeOpenPayDTO {

    private String transactionId;
    private Customer customerCreate;
    private Charge cardCharge;
    private Charge bankCharge;
    private ResponseDTO responseDTO;
    private OrderResponseDTO OrderResponseDTO;

    public Customer getCustomerCreate() {
        return customerCreate;
    }
    public void setCustomerCreate(Customer customerCreate) {
        this.customerCreate = customerCreate;
    }

    public String getTransactionId() {
        return transactionId;
    }
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }


    public Charge getCardCharge() {
        return cardCharge;
    }
    public void setCardCharge(Charge cardCharge) {
        this.cardCharge = cardCharge;
    }

    public Charge getBankCharge() {
        return bankCharge;
    }
    public void setBankCharge(Charge bankCharge) {
        this.bankCharge = bankCharge;
    }

    public ResponseDTO getResponseDTO() {
        return responseDTO;
    }
    public void setResponseDTO(ResponseDTO responseDTO) {
        this.responseDTO = responseDTO;
    }
    
    public OrderResponseDTO getOrderResponseDTO() {
        return OrderResponseDTO;
    }
    public void setOrderResponseDTO(OrderResponseDTO orderResponseDTO) {
        OrderResponseDTO = orderResponseDTO;
    }
}
