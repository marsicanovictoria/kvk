package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserRegisterApiDataDTO {

    	//private Long idFinancing;
	private String type;
	private FinancingUserRegisterApiAttributesDTO attributes;

//	@JsonProperty("id")
//	public Long getIdFinancing() {
//	    return idFinancing;
//	}
//	public void setIdFinancing(Long idFinancing) {
//	    this.idFinancing = idFinancing;
//	}
	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("attributes")
	public FinancingUserRegisterApiAttributesDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserRegisterApiAttributesDTO attributes) {
	    this.attributes = attributes;
	}


}