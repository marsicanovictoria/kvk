package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Enrique on 09-Jun-17.
 */
public class SectionPriceDTO {

    private String kavakPrice;
    private String marketPrice;
    private String savings;

    @JsonProperty("kavak_price")
    public String getKavakPrice() {
        return kavakPrice;
    }

    public void setKavakPrice(String kavakPrice) {
        this.kavakPrice = kavakPrice;
    }
    @JsonProperty("market_price")
    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }
    @JsonProperty("savings")
    public String getSavings() {
        return savings;
    }

    public void setSavings(String savings) {
        this.savings = savings;
    }

}
