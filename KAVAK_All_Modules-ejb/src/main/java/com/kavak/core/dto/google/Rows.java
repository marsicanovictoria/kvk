package com.kavak.core.dto.google;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Enrique on 19-Jun-17.
 */
public class Rows {

    private List<Elements> listElements;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("rows")
    public List<Elements> getListElements() {
        return listElements;
    }

    public void setListElements(List<Elements> listElements) {
        this.listElements = listElements;
    }
}
