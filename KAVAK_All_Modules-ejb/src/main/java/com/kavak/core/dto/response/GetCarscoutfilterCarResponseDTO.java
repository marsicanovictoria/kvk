package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetCarscoutfilterCarResponseDTO implements Comparable<GetCarscoutfilterCarResponseDTO> {

	private Long alertId;
	private Long catalogueId;
	private String title;
	private String year;
	private String style;
	private String styleIcon;
	private Integer km;
	private Integer salePrice;
	private Long priorityId;
	private Long idCarscoutNotify;
	private String colorHexa;
	private String colorName;
	private String make;
	private String seats;
	private String doors;
	private String cylinders;
	private String traction;
	private String fuelType;
	private String transmission;
	private Long stockId;
	private String urlStock;
	private String carStatus;
	private String probability;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("alert_id")
	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("catalogue_id")
	public Long getCatalogueId() {
		return catalogueId;
	}

	public void setCatalogueId(Long catalogueId) {
		this.catalogueId = catalogueId;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("year")
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("style")
	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("style_icon")
	public String getStyleIcon() {
		return styleIcon;
	}

	public void setStyleIcon(String styleIcon) {
		this.styleIcon = styleIcon;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("km")
	public Integer getKm() {
		return km;
	}

	public void setKm(Integer km) {
		this.km = km;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("sale_price")
	public Integer getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Integer salePrice) {
		this.salePrice = salePrice;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("priority_id")
	public Long getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("id_carscout_notify")
	public Long getIdCarscoutNotify() {
		return idCarscoutNotify;
	}

	public void setIdCarscoutNotify(Long idCarscoutNotify) {
		this.idCarscoutNotify = idCarscoutNotify;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("color")
	public String getColorHexa() {
		return colorHexa;
	}

	public void setColorHexa(String colorHexa) {
		this.colorHexa = colorHexa;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("color_name")
	public String getColorName() {
		return colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("make")
	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("seats")
	public String getSeats() {
		return seats;
	}

	public void setSeats(String seats) {
		this.seats = seats;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("doors")
	public String getDoors() {
		return doors;
	}

	public void setDoors(String doors) {
		this.doors = doors;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("cylinders")
	public String getCylinders() {
		return cylinders;
	}

	public void setCylinders(String cylinders) {
		this.cylinders = cylinders;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("traction")
	public String getTraction() {
		return traction;
	}

	public void setTraction(String traction) {
		this.traction = traction;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("fuelType")
	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("transmission")
	public String getTransmission() {
		return transmission;
	}

	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("stock_id")
	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("url_stock")
	public String getUrlStock() {
		return urlStock;
	}

	public void setUrlStock(String urlStock) {
		this.urlStock = urlStock;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("status")
	public String getCarStatus() {
		return carStatus;
	}

	public void setCarStatus(String carStatus) {
		this.carStatus = carStatus;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("probability")
	public String getProbability() {
		return probability;
	}

	public void setProbability(String probability) {
		this.probability = probability;
	}

	public int compareTo(GetCarscoutfilterCarResponseDTO other) {
		return priorityId.compareTo(other.priorityId);
	}
}







