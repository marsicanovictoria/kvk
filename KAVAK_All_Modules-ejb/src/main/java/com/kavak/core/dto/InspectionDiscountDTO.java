package com.kavak.core.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class  InspectionDiscountDTO {


	private Long id;
	private Long idInspection;
	private String inspectorEmail;
	private String discountType;
	private Long discountAmount; 
	private Boolean removed;
    private boolean isPaidByKavak;
    private String removedReason;
	private Date creationDate;
	
    @JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}
    
	@JsonProperty("inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	@JsonProperty("discount_type")
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	@JsonProperty("discount_amount")
	public Long getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Long discountAmount) {
		this.discountAmount = discountAmount;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("is_removed")
	public Boolean isRemoved() {
		return removed;
	}
	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	@JsonProperty("removed_reason")
	public String getRemovedReason() {
		return removedReason;
	}
	public void setRemovedReason(String removedReason) {
		this.removedReason = removedReason;
	}

    @JsonProperty("is_paid_by_kavak")
    public boolean isPaidByKavak() {
        return isPaidByKavak;
    }
    public void setPaidByKavak(boolean paidByKavak) {
        isPaidByKavak = paidByKavak;
    }
    
	@JsonProperty("creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
