package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AppNotificationLogResponseDTO {

    private Long id;
    private Long userId;
    private Long typeId;
    private Long tokenId;
    private String description;
    private String creationDate;
    private String title;
    private String iconUrl;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("id")
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("user_id")
    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    @JsonProperty("type_id")
    public Long getTypeId() {
	return typeId;
    }

    public void setTypeId(Long typeId) {
	this.typeId = typeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("token_id")
    public Long getTokenId() {
	return tokenId;
    }

    public void setTokenId(Long tokenId) {
	this.tokenId = tokenId;
    }

    @JsonProperty("description")
    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    @JsonProperty("date")
    public String getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(String creationDate) {
	this.creationDate = creationDate;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("title")
    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    @JsonProperty("icon_url")
    public String getIconUrl() {
	return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
	this.iconUrl = iconUrl;
    }

}
