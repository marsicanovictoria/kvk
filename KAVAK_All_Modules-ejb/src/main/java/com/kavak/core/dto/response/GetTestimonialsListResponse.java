package com.kavak.core.dto.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.TestimonialsListItemsDTO;

public class GetTestimonialsListResponse {
	
	private String type;
	private String title;
	private List<TestimonialsListItemsDTO> items;
	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("title")
	public String getTitle() {
	    return title;
	}
	public void setTitle(String title) {
	    this.title = title;
	}
	
	@JsonProperty("items")
	public List<TestimonialsListItemsDTO> getItems() {
	    return items;
	}
	public void setItems(List<TestimonialsListItemsDTO> items) {
	    this.items = items;
	}


}