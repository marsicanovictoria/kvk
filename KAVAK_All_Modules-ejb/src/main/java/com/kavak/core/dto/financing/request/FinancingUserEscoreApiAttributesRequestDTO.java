package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserEscoreApiAttributesRequestDTO {

	private String currentApplyingVertical;
	private String automotiveCarPrice;
	private String automotiveDownPayment;
	private String automotiveCarManufacturer;
	private String automotiveCarModel;
	private String automotiveCarVersion;
	private String automotiveCarVersionYear;
	private String automotiveCarUseType;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("current_applying_vertical")
	public String getCurrentApplyingVertical() {
	    return currentApplyingVertical;
	}
	public void setCurrentApplyingVertical(String currentApplyingVertical) {
	    this.currentApplyingVertical = currentApplyingVertical;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("automotive_car_price")
	public String getAutomotiveCarPrice() {
	    return automotiveCarPrice;
	}
	public void setAutomotiveCarPrice(String automotiveCarPrice) {
	    this.automotiveCarPrice = automotiveCarPrice;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("automotive_down_payment")
	public String getAutomotiveDownPayment() {
	    return automotiveDownPayment;
	}
	public void setAutomotiveDownPayment(String automotiveDownPayment) {
	    this.automotiveDownPayment = automotiveDownPayment;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("automotive_car_manufacturer")
	public String getAutomotiveCarManufacturer() {
	    return automotiveCarManufacturer;
	}
	public void setAutomotiveCarManufacturer(String automotiveCarManufacturer) {
	    this.automotiveCarManufacturer = automotiveCarManufacturer;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("automotive_car_model")
	public String getAutomotiveCarModel() {
	    return automotiveCarModel;
	}
	public void setAutomotiveCarModel(String automotiveCarModel) {
	    this.automotiveCarModel = automotiveCarModel;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("automotive_car_version")
	public String getAutomotiveCarVersion() {
	    return automotiveCarVersion;
	}
	public void setAutomotiveCarVersion(String automotiveCarVersion) {
	    this.automotiveCarVersion = automotiveCarVersion;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("automotive_car_version_year")
	public String getAutomotiveCarVersionYear() {
	    return automotiveCarVersionYear;
	}
	public void setAutomotiveCarVersionYear(String automotiveCarVersionYear) {
	    this.automotiveCarVersionYear = automotiveCarVersionYear;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("automotive_car_use_type")
	public String getAutomotiveCarUseType() {
	    return automotiveCarUseType;
	}
	public void setAutomotiveCarUseType(String automotiveCarUseType) {
	    this.automotiveCarUseType = automotiveCarUseType;
	}

}