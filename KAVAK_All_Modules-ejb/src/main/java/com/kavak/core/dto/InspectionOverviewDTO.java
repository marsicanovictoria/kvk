package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InspectionOverviewDTO {
	
	private InspectionOverviewDataDTO inspectionOverviewDataDTO;
	private List<InspectionDiscountDTO> listInspectionDiscountDTO;
	
	@JsonProperty("overview_data")
	public InspectionOverviewDataDTO getInspectionOverviewDataDTO() {
		return inspectionOverviewDataDTO;
	}
	public void setInspectionOverviewDataDTO(InspectionOverviewDataDTO inspectionOverviewDataDTO) {
		this.inspectionOverviewDataDTO = inspectionOverviewDataDTO;
	}
	@JsonProperty("discounts")
	public List<InspectionDiscountDTO> getListInspectionDiscountDTO() {
		return listInspectionDiscountDTO;
	}
	public void setListInspectionDiscountDTO(List<InspectionDiscountDTO> listInspectionDiscountDTO) {
		this.listInspectionDiscountDTO = listInspectionDiscountDTO;
	}

}
