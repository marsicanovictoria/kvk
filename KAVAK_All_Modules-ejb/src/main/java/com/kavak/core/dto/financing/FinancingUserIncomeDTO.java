package com.kavak.core.dto.financing;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserIncomeDTO {
	
    
    private Long id;
    private Long financingId;
    private Long netIncomeVerified;
    private String incomeProfile;
    private String automotiveCarUseType;
    private Long automotiveCarPrice;
    private Long automotiveDownPayment;
//    private String codeArf;
//    private String escore;
//    private String escoreText;
//    private String disqualified;
//    private String escoreRazon;
//    private String financingAmount;
//    private String maximumFinancingAmount;
    private Timestamp creationDate;
    private boolean active;
    private Long positionAge;
    
    
    @JsonProperty("id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    @JsonProperty("net_income_verified")
    public Long getNetIncomeVerified() {
        return netIncomeVerified;
    }
    public void setNetIncomeVerified(Long netIncomeVerified) {
        this.netIncomeVerified = netIncomeVerified;
    }
    
    @JsonProperty("income_profile")
    public String getIncomeProfile() {
        return incomeProfile;
    }
    public void setIncomeProfile(String incomeProfile) {
        this.incomeProfile = incomeProfile;
    }
    
    @JsonProperty("automotive_car_use_type")
    public String getAutomotiveCarUseType() {
        return automotiveCarUseType;
    }
    public void setAutomotiveCarUseType(String automotiveCarUseType) {
        this.automotiveCarUseType = automotiveCarUseType;
    }
    
    @JsonProperty("automotive_car_price")
    public Long getAutomotiveCarPrice() {
        return automotiveCarPrice;
    }
    public void setAutomotiveCarPrice(Long automotiveCarPrice) {
        this.automotiveCarPrice = automotiveCarPrice;
    }
    
    @JsonProperty("automotive_down_payment")
    public Long getAutomotiveDownPayment() {
        return automotiveDownPayment;
    }
    public void setAutomotiveDownPayment(Long automotiveDownPayment) {
        this.automotiveDownPayment = automotiveDownPayment;
    }

    
//    @JsonProperty("code_arf")
//    public String getCodeArf() {
//        return codeArf;
//    }
//    public void setCodeArf(String codeArf) {
//        this.codeArf = codeArf;
//    }
//    
//    @JsonProperty("escore")
//    public String getEscore() {
//        return escore;
//    }
//    public void setEscore(String escore) {
//        this.escore = escore;
//    }
//    
//    @JsonProperty("escore_text")
//    public String getEscoreText() {
//        return escoreText;
//    }
//    public void setEscoreText(String escoreText) {
//        this.escoreText = escoreText;
//    }
//    
//    @JsonProperty("disqualified")
//    public String getDisqualified() {
//        return disqualified;
//    }
//    public void setDisqualified(String disqualified) {
//        this.disqualified = disqualified;
//    }
//    
//    @JsonProperty("escore_razon")
//    public String getEscoreRazon() {
//        return escoreRazon;
//    }
//    public void setEscoreRazon(String escoreRazon) {
//        this.escoreRazon = escoreRazon;
//    }
//    
//    @JsonProperty("financing_amount")
//    public String getFinancingAmount() {
//        return financingAmount;
//    }
//    public void setFinancingAmount(String financingAmount) {
//        this.financingAmount = financingAmount;
//    }
//    
//    @JsonProperty("maximum_financing_amount")
//    public String getMaximumFinancingAmount() {
//        return maximumFinancingAmount;
//    }
//    public void setMaximumFinancingAmount(String maximumFinancingAmount) {
//        this.maximumFinancingAmount = maximumFinancingAmount;
//    }
    
    @JsonProperty("is_active")
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }
    
    @JsonProperty("creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
    
    @JsonProperty("financing_id")
    public Long getFinancingId() {
        return financingId;
    }
    public void setFinancingId(Long financingId) {
        this.financingId = financingId;
    }
    
    @JsonProperty("position_age")
    public Long getPositionAge() {
        return positionAge;
    }
    public void setPositionAge(Long positionAge) {
        this.positionAge = positionAge;
    }



}