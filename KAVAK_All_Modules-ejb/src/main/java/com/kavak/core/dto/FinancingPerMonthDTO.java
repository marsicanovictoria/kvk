package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingPerMonthDTO {

	private String month;
	private String amount;

	@JsonProperty("month")
	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	@JsonProperty("amount")
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
