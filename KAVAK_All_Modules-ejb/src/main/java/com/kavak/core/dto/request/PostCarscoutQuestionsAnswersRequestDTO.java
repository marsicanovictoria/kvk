package com.kavak.core.dto.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.AnswerDTO;

public class PostCarscoutQuestionsAnswersRequestDTO {

    private Long userId;
    private Long alertId;
    private List<AnswerDTO> answers;
    private Long catalogueId;


    @JsonProperty("user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @JsonProperty("answers")
    public List<AnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDTO> answers) {
        this.answers = answers;
    }

    @JsonProperty("alert_id")
    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    public Long getAlertId() {
        return alertId;
    }

    @JsonProperty("catalogue_id")
    public Long getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(Long catalogueId) {
        this.catalogueId = catalogueId;
    }
}
