package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostFinancingUserPinCodeAuthorizationRequestDTO {

    	private String pinCode;

    	@JsonProperty("pin_code")
	public String getPinCode() {
	    return pinCode;
	}
	public void setPinCode(String pinCode) {
	    this.pinCode = pinCode;
	}
}
