package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class RepairSubCategoryDTO {
	
	private Long id;
	private String name;
	private List<RepairTypeDTO> listRepairTypeDTO;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("sub_category_repair_type")
	public List<RepairTypeDTO> getListRepairTypeDTO() {
		return listRepairTypeDTO;
	}
	public void setListRepairTypeDTO(List<RepairTypeDTO> listRepairTypeDTO) {
		this.listRepairTypeDTO = listRepairTypeDTO;
	}
}