package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SellCarMetaDTO {
	
	private Long id;
	private Long sellCarId;
	private String metaValue;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("sell_car_id")
	public Long getSellCarId() {
		return sellCarId;
	}
	public void setSellCarId(Long sellCarId) {
		this.sellCarId = sellCarId;
	}
	@JsonProperty("meta_value")
	public String getMetaValue() {
		return metaValue;
	}
	public void setMetaValue(String metaValue) {
		this.metaValue = metaValue;
	}
}