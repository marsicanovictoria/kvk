package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JobDTO {

	private String company;
	private String companyType; // company_type
	private String economicActivity; // economic_activity
	private String phone;
	private String street;
	private String exteriorNumber; // exterior_number
	private String interiorNumber; // interior_number
	private String postalCode;
	private String delegation;
	private String state;
	private String city;
	private String colony;
	private String montlyIncome; // montly_income
	private String variableIncome; // variable_income
	private Long insuranceAffiliation; // insurance_affiliation
	private Long position;
	private Long profession;
	private String yearOld; // year_old"
	private String monthOld; // month_old

	@JsonProperty("company")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@JsonProperty("company_type")
	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	@JsonProperty("economic_activity")
	public String getEconomicActivity() {
		return economicActivity;
	}

	public void setEconomicActivity(String economicActivity) {
		this.economicActivity = economicActivity;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonProperty("street")
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@JsonProperty("exterior_number")
	public String getExteriorNumber() {
		return exteriorNumber;
	}

	public void setExteriorNumber(String exteriorNumber) {
		this.exteriorNumber = exteriorNumber;
	}

	@JsonProperty("interior_number")
	public String getInteriorNumber() {
		return interiorNumber;
	}

	public void setInteriorNumber(String interiorNumber) {
		this.interiorNumber = interiorNumber;
	}

	@JsonProperty("postal_code")
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@JsonProperty("delegation")
	public String getDelegation() {
		return delegation;
	}

	public void setDelegation(String delegation) {
		this.delegation = delegation;
	}

	@JsonProperty("state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@JsonProperty("city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@JsonProperty("colony")
	public String getColony() {
		return colony;
	}

	public void setColony(String colony) {
		this.colony = colony;
	}

	@JsonProperty("montly_income")
	public String getMontlyIncome() {
		return montlyIncome;
	}

	public void setMontlyIncome(String montlyIncome) {
		this.montlyIncome = montlyIncome;
	}

	@JsonProperty("variable_income")
	public String getVariableIncome() {
		return variableIncome;
	}

	public void setVariableIncome(String variableIncome) {
		this.variableIncome = variableIncome;
	}

	@JsonProperty("insurance_affiliation")
	public Long getInsuranceAffiliation() {
		return insuranceAffiliation;
	}

	public void setInsuranceAffiliation(Long insuranceAffiliation) {
		this.insuranceAffiliation = insuranceAffiliation;
	}

	@JsonProperty("position")
	public Long getPosition() {
		return position;
	}

	public void setPosition(Long position) {
		this.position = position;
	}

	@JsonProperty("profession")
	public Long getProfession() {
		return profession;
	}

	public void setProfession(Long profession) {
		this.profession = profession;
	}

	@JsonProperty("year_old")
	public String getYearOld() {
		return yearOld;
	}

	public void setYearOld(String yearOld) {
		this.yearOld = yearOld;
	}

	@JsonProperty("month_old")
	public String getMonthOld() {
		return monthOld;
	}

	public void setMonthOld(String monthOld) {
		this.monthOld = monthOld;
	}

}
