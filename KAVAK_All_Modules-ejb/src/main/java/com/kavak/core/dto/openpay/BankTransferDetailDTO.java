package com.kavak.core.dto.openpay;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BankTransferDetailDTO {

    private String reference;
    private String clabe;
    private String transferAmount;
    private String paymentDescription;
    private String urlReceipt;



    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("reference")
    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("clabe")
    public String getClabe() {
        return clabe;
    }
    public void setClabe(String clabe) {
        this.clabe = clabe;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("transfer_amount")
    public String getTransferAmount() {
        return transferAmount;
    }
    public void setTransferAmount(String transferAmount) {
        this.transferAmount = transferAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("payment_description")
    public String getPaymentDescription() {
        return paymentDescription;
    }
    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("url_receipt")
    public String getUrlReceipt() {
        return urlReceipt;
    }
    public void setUrlReceipt(String urlReceipt) {
        this.urlReceipt = urlReceipt;
    }
}
