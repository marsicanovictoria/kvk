package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ColonyDataDTO {

    private String name;
    private boolean allowInspection;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty("allows_inspection")
    public boolean isAllowInspection() {
        return allowInspection;
    }

    public void setAllowInspection(boolean allowInspection) {
        this.allowInspection = allowInspection;
    }
}
