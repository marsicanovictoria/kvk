package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingApiMarketPlaceRfcGeneratorResponseDTO {

    private FinancingApiMarketPlaceRfcGeneratorDataResponseDTO response;

    
    @JsonProperty("response")
    public FinancingApiMarketPlaceRfcGeneratorDataResponseDTO getResponse() {
        return response;
    }

    public void setResponse(FinancingApiMarketPlaceRfcGeneratorDataResponseDTO response) {
        this.response = response;
    }

}