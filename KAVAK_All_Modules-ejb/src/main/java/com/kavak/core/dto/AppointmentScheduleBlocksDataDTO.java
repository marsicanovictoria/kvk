package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Enrique on 29-Jun-17.
 */
public class AppointmentScheduleBlocksDataDTO {

    private String date;
    private List<AppointmentScheduleBlockDTO> listAppointmentScheduleBlockDTO;

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    @JsonProperty("appointment_blocks")
    public List<AppointmentScheduleBlockDTO> getListAppointmentScheduleBlockDTO() {
        return listAppointmentScheduleBlockDTO;
    }

    public void setListAppointmentScheduleBlockDTO(List<AppointmentScheduleBlockDTO> listAppointmentScheduleBlockDTO) {
        this.listAppointmentScheduleBlockDTO = listAppointmentScheduleBlockDTO;
    }
}
