package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MetaValuePropertyStatusDTO {

    private List<GenericDTO> listMetaValuePropertyStatusDataDTO;

    @JsonProperty("property_status")
    public List<GenericDTO> getListMetaValuePropertyStatusDataDTO() {
        return listMetaValuePropertyStatusDataDTO;
    }

    public void setListMetaValuePropertyStatusDataDTO(List<GenericDTO> listMetaValuePropertyStatusDataDTO) {
        this.listMetaValuePropertyStatusDataDTO = listMetaValuePropertyStatusDataDTO;
    }
}
