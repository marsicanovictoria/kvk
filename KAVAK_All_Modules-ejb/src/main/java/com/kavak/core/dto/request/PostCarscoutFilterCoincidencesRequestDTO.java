package com.kavak.core.dto.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;

public class PostCarscoutFilterCoincidencesRequestDTO {

    private Integer minPrice;
    private Integer maxPrice;
    private Integer maxKm;
    private Integer minKm;
    private List<GenericDTO> genericDTOListYear;
    private List<GenericDTO> genericDTOListBodyType;
    private List<GenericDTO> genericDTOListMake;
    private List<GenericDTO> genericDTOListColor;
    private List<GenericDTO> genericDTOListDoors;
    private List<GenericDTO> genericDTOListSeats;
    private List<GenericDTO> genericDTOListCylinders;
    private List<GenericDTO> genericDTOListTraction;
    private List<GenericDTO> genericDTOListFuelType;
    private List<GenericDTO> genericDTOListModel;
    private List<GenericDTO> genericDTOListVersion;
    private List<GenericDTO> genericDTOListTransmissions;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("min_price")
    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("max_price")
    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("max_km")
    public Integer getMaxKm() {
        return maxKm;
    }

    public void setMaxKm(Integer maxKm) {
        this.maxKm = maxKm;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("min_km")
    public Integer getMinKm() {
        return minKm;
    }

    public void setMinKm(Integer minKm) {
        this.minKm = minKm;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("years")
    public List<GenericDTO> getGenericDTOListYear() {
        return genericDTOListYear;
    }

    public void setGenericDTOListYear(List<GenericDTO> genericDTOListYear) {
        this.genericDTOListYear = genericDTOListYear;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("bodytypes")
    public List<GenericDTO> getGenericDTOListBodyType() {
        return genericDTOListBodyType;
    }

    public void setGenericDTOListBodyType(List<GenericDTO> genericDTOListBodyType) {
        this.genericDTOListBodyType = genericDTOListBodyType;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("makes")
    public List<GenericDTO> getGenericDTOListMake() {
        return genericDTOListMake;
    }

    public void setGenericDTOListMake(List<GenericDTO> genericDTOListMake) {
        this.genericDTOListMake = genericDTOListMake;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("colors")
    public List<GenericDTO> getGenericDTOListColor() {
        return genericDTOListColor;
    }

    public void setGenericDTOListColor(List<GenericDTO> genericDTOListColor) {
        this.genericDTOListColor = genericDTOListColor;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("doors")
    public List<GenericDTO> getGenericDTOListDoors() {
        return genericDTOListDoors;
    }

    public void setGenericDTOListDoors(List<GenericDTO> genericDTOListDoors) {
        this.genericDTOListDoors = genericDTOListDoors;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("seats")
    public List<GenericDTO> getGenericDTOListSeats() {
        return genericDTOListSeats;
    }

    public void setGenericDTOListSeats(List<GenericDTO> genericDTOListSeats) {
        this.genericDTOListSeats = genericDTOListSeats;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("cylinders")
    public List<GenericDTO> getGenericDTOListCylinders() {
        return genericDTOListCylinders;
    }

    public void setGenericDTOListCylinders(List<GenericDTO> genericDTOListCylinders) {
        this.genericDTOListCylinders = genericDTOListCylinders;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("tractions")
    public List<GenericDTO> getGenericDTOListTraction() {
        return genericDTOListTraction;
    }

    public void setGenericDTOListTraction(List<GenericDTO> genericDTOListTraction) {
        this.genericDTOListTraction = genericDTOListTraction;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("fueltypes")
    public List<GenericDTO> getGenericDTOListFuelType() {
        return genericDTOListFuelType;
    }

    public void setGenericDTOListFuelType(List<GenericDTO> genericDTOListFuelType) {
        this.genericDTOListFuelType = genericDTOListFuelType;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("models")
    public List<GenericDTO> getGenericDTOListModel() {
        return genericDTOListModel;
    }

    public void setGenericDTOListModel(List<GenericDTO> genericDTOListModel) {
        this.genericDTOListModel = genericDTOListModel;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("versions")
    public List<GenericDTO> getGenericDTOListVersion() {
        return genericDTOListVersion;
    }

    public void setGenericDTOListVersion(List<GenericDTO> genericDTOListVersion) {
        this.genericDTOListVersion = genericDTOListVersion;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("transmissions")
    public List<GenericDTO> getGenericDTOListTransmissions() {
        return genericDTOListTransmissions;
    }

    public void setGenericDTOListTransmissions(List<GenericDTO> genericDTOListTransmissions) {
        this.genericDTOListTransmissions = genericDTOListTransmissions;
    }
}
