package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.CarInformationDTO;

import java.util.List;

public class CarResponseDTO {
	
	private List<String> listProductPageEnumNames;
	private CarInformationDTO carInformationDTO;
	private String urlProduct;

	@JsonProperty("sections")
	public List<String> getListProductPageEnumNames() {
		return listProductPageEnumNames;
	}

	public void setListProductPageEnumNames(List<String> listProductPageEnumNames) {
		this.listProductPageEnumNames = listProductPageEnumNames;
	}
	@JsonProperty("summary")
	public CarInformationDTO getCarInformationDTO() {
		return carInformationDTO;
	}

	public void setCarInformationDTO(CarInformationDTO carInformationDTO) {
		this.carInformationDTO = carInformationDTO;
	}

	@JsonProperty("product_url")
	public String getUrlProduct() {
		return urlProduct;
	}

	public void setUrlProduct(String urlProduct) {
		this.urlProduct = urlProduct;
	}
}
