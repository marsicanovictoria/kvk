package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IndicationDTO {
	
	private Long id;
	private String numberIndex;
	private String indicationText;
	private boolean active;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("name")
	public String getNumberIndex() {
		return numberIndex;
	}
	public void setNumberIndex(String numberIndex) {
		this.numberIndex = numberIndex;
	}
	@JsonProperty("content")
	public String getIndicationText() {
		return indicationText;
	}
	public void setIndicationText(String indicationText) {
		this.indicationText = indicationText;
	}
	@JsonProperty("is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
}
