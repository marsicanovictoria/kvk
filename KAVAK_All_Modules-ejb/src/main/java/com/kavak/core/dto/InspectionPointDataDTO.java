package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Enrique on 27-Jun-17.
 */
public class InspectionPointDataDTO {

    private String nameCategory;
    private List<GenericDTO> listInpectionPointDataGenericDTO;

    @JsonProperty("categories")
    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }
    @JsonProperty("ìnspection_points")
    public List<GenericDTO> getListInpectionPointDataGenericDTO() {
        return listInpectionPointDataGenericDTO;
    }

    public void setListInpectionPointDataGenericDTO(List<GenericDTO> listInpectionPointDataGenericDTO) {
        this.listInpectionPointDataGenericDTO = listInpectionPointDataGenericDTO;
    }
}
