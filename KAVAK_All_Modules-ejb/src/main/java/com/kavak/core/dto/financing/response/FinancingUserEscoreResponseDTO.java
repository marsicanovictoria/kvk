package com.kavak.core.dto.financing.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserEscoreResponseDTO {

	private String scoreText;
	private String score;
	private String financingAmount;
	private String maximumFinancingAmount;
	private String downpayment;
	private String netIncomeVerified;
	private List<FinancingUserEscoreProfileResponseDTO> listFinancingUserEscoreProfileResponseDTO;
	
	@JsonProperty("score_text")
	public String getScoreText() {
	    return scoreText;
	}
	public void setScoreText(String scoreText) {
	    this.scoreText = scoreText;
	}
	
	@JsonProperty("score")
	public String getScore() {
	    return score;
	}
	public void setScore(String score) {
	    this.score = score;
	}
	
	@JsonProperty("financing_amount")
	public String getFinancingAmount() {
	    return financingAmount;
	}
	public void setfinancingAmount(String financingAmount) {
	    this.financingAmount = financingAmount;
	}
	
	@JsonProperty("maximum_financing_amount")
	public String getMaximumFinancingAmount() {
	    return maximumFinancingAmount;
	}
	public void setMaximumFinancingAmount(String maximumFinancingAmount) {
	    this.maximumFinancingAmount = maximumFinancingAmount;
	}
	
	@JsonProperty("downpayment")
	public String getDownpayment() {
	    return downpayment;
	}
	public void setDownpayment(String downpayment) {
	    this.downpayment = downpayment;
	}
	
	@JsonProperty("net_income_verified")
	public String getNetIncomeVerified() {
	    return netIncomeVerified;
	}
	public void setNetIncomeVerified(String netIncomeVerified) {
	    this.netIncomeVerified = netIncomeVerified;
	}
	
	@JsonProperty("profiles")
	public List<FinancingUserEscoreProfileResponseDTO> getListFinancingUserEscoreProfileResponseDTO() {
	    return listFinancingUserEscoreProfileResponseDTO;
	}
	public void setListFinancingUserEscoreProfileResponseDTO(List<FinancingUserEscoreProfileResponseDTO> listFinancingUserEscoreProfileResponseDTO) {
	    this.listFinancingUserEscoreProfileResponseDTO = listFinancingUserEscoreProfileResponseDTO;
	}


}