package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserEscoreAttributearbolScoreResultApiResponseDTO {

	private String data;

	

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("data")
	public String getData() {
	    return data;
	}

	public void setData(String data) {
	    this.data = data;
	}
}