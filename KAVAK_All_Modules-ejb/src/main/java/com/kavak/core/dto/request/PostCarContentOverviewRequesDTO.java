package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.PutCarContentOverviewDataDTO;

import java.util.List;

public class PostCarContentOverviewRequesDTO {

	private List<PutCarContentOverviewDataDTO> listPutCarContentOverviewDataDTO;

	@JsonProperty("inpections")
	public List<PutCarContentOverviewDataDTO> getListPutCarContentOverviewDataDTO() {
		return listPutCarContentOverviewDataDTO;
	}

	public void setListPutCarContentOverviewDataDTO(List<PutCarContentOverviewDataDTO> listPutCarContentOverviewDataDTO) {
		this.listPutCarContentOverviewDataDTO = listPutCarContentOverviewDataDTO;
	}
}
