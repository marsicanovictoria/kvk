package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.model.SellCarDetail;

import java.sql.Timestamp;

public class SaleCarOrderDTO {
	
	private Long id; 
	private Long itemId;
	private Long buyerId;
	private String transactionId;
	private String itemName;
	private Timestamp registrationDate;
	private String totalAmt;
	private String paidAmt;
	private String pendingAmt;
	private String buyerName;
	private String paymentMode;
	private String paymentType;
	private String email;
	private String phone;
	private String address;
	private String city;
	private String state;
	private String country;
	private String zipCode;
	private boolean active;
	private boolean complete;
	private boolean canceled;
	private String holdTime;
	private String paymentStatus;
	private String reservationTime;
	private String deliverStatus;
	private Integer daysToReturn;
	private String loanDetails;
	private String pickupType;
	private Timestamp actionDate;
	private Long financialAmount;
	private Integer financialMonths;
	private Integer hookingAmount;
	private SellCarDetail sellCarDetail;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("item_id")
	public Long getItemId() {
		return itemId;
	}
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
	@JsonProperty("buyer_id")
	public Long getBuyerId() {
		return buyerId;
	}
	public void setBuyerId(Long buyerId) {
		this.buyerId = buyerId;
	}
	@JsonProperty("transaction_id")
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	@JsonProperty("item_name")
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@JsonProperty("registration_date")
	public Timestamp getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}
	@JsonProperty("total_amt")
	public String getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(String totalAmt) {
		this.totalAmt = totalAmt;
	}
	public String getPaidAmt() {
		return paidAmt;
	}
	public void setPaidAmt(String paidAmt) {
		this.paidAmt = paidAmt;
	}
	public String getPendingAmt() {
		return pendingAmt;
	}
	public void setPendingAmt(String pendingAmt) {
		this.pendingAmt = pendingAmt;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isComplete() {
		return complete;
	}
	public void setComplete(boolean complete) {
		this.complete = complete;
	}
	public boolean isCanceled() {
		return canceled;
	}
	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
	public String getHoldTime() {
		return holdTime;
	}
	public void setHoldTime(String holdTime) {
		this.holdTime = holdTime;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getReservationTime() {
		return reservationTime;
	}
	public void setReservationTime(String reservationTime) {
		this.reservationTime = reservationTime;
	}
	public String getDeliverStatus() {
		return deliverStatus;
	}
	public void setDeliverStatus(String deliverStatus) {
		this.deliverStatus = deliverStatus;
	}
	public Integer getDaysToReturn() {
		return daysToReturn;
	}
	public void setDaysToReturn(Integer daysToReturn) {
		this.daysToReturn = daysToReturn;
	}
	public String getLoanDetails() {
		return loanDetails;
	}
	public void setLoanDetails(String loanDetails) {
		this.loanDetails = loanDetails;
	}
	public String getPickupType() {
		return pickupType;
	}
	public void setPickupType(String pickupType) {
		this.pickupType = pickupType;
	}
	public Timestamp getActionDate() {
		return actionDate;
	}
	public void setActionDate(Timestamp actionDate) {
		this.actionDate = actionDate;
	}
	public Long getFinancialAmount() {
		return financialAmount;
	}
	public void setFinancialAmount(Long financialAmount) {
		this.financialAmount = financialAmount;
	}
	public Integer getFinancialMonths() {
		return financialMonths;
	}
	public void setFinancialMonths(Integer financialMonths) {
		this.financialMonths = financialMonths;
	}
	public Integer getHookingAmount() {
		return hookingAmount;
	}
	public void setHookingAmount(Integer hookingAmount) {
		this.hookingAmount = hookingAmount;
	}
	public SellCarDetail getSellCarDetail() {
		return sellCarDetail;
	}
	public void setSellCarDetail(SellCarDetail sellCarDetail) {
		this.sellCarDetail = sellCarDetail;
	}
	
	
	

}
