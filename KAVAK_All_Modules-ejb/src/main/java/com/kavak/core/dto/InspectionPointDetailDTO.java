package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InspectionPointDetailDTO {
	
	private Long idInspection;
	private Long idInspectionPoint;
	private Long inspectionPointAnswer;
	private Long idRepairPiece;
	private String idRepairLocation;
	private String inspectorEmail;
	private String additionalInfoDetail;
    private Long inspectionTypeId;
	private List<InspectionRepairDTO> listInspectionRepairDTO;
	private List<InspectionDimpleDTO> listInspectionDimpleDTO;
	
	@JsonProperty("id_inspection")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}

	@JsonProperty("id_inspection_point")
	public Long getIdInspectionPoint() {
		return idInspectionPoint;
	}
	public void setIdInspectionPoint(Long idInspectionPoint) {
		this.idInspectionPoint = idInspectionPoint;
	}

	@JsonProperty("id_inspection_answer")
	public Long getInspectionPointAnswer() {
		return inspectionPointAnswer;
	}
	public void setInspectionPointAnswer(Long inspectionPointAnswer) {
		this.inspectionPointAnswer = inspectionPointAnswer;
	}

	@JsonProperty("id_repair_piece")
	public Long getIdRepairPiece() {
		return idRepairPiece;
	}
	public void setIdRepairPiece(Long idRepairPiece) {
		this.idRepairPiece = idRepairPiece;
	}

	@JsonProperty("id_repair_location")
	public String getIdRepairLocation() {
		return idRepairLocation;
	}
	public void setIdRepairLocation(String idRepairLocation) {
		this.idRepairLocation = idRepairLocation;
	}

	@JsonProperty("inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}

	@JsonProperty("additional_info_detail")
	public String getAdditionalInfoDetail() {
		return additionalInfoDetail;
	}
	public void setAdditionalInfoDetail(String additionalInfoDetail) {
		this.additionalInfoDetail = additionalInfoDetail;
	}

	@JsonProperty("repairs")
	public List<InspectionRepairDTO> getListInspectionRepairDTO() {
		return listInspectionRepairDTO;
	}
	public void setListInspectionRepairDTO(List<InspectionRepairDTO> listInspectionRepairDTO) {
		this.listInspectionRepairDTO = listInspectionRepairDTO;
	}

	@JsonProperty("dimples")
	public List<InspectionDimpleDTO> getListInspectionDimpleDTO() {
		return listInspectionDimpleDTO;
	}
	public void setListInspectionDimpleDTO(List<InspectionDimpleDTO> listInspectionDimpleDTO) {
		this.listInspectionDimpleDTO = listInspectionDimpleDTO;
	}

    @JsonProperty("inspection_type_id")
    public Long getInspectionTypeId() {
        return inspectionTypeId;
    }
    public void setInspectionTypeId(Long inspectionTypeId) {
        this.inspectionTypeId = inspectionTypeId;
    }

}
//=======
//package com.kavak.core.dto;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//
//public class InspectionPointDetailDTO {
//
//	private String idCategory;
//	private String idSubCategory;
//	private String idInspectionPoint;
//	private Long inspectionAnswer;
//	private Long idDimple;
//	private String coordinatesDimplesX;
//	private String CoordinatesDimplesY;
//	private Long dimpleHeight;
//	private Long dimpleWidth;
//	private Byte[] dimpleImage;
//	private Long idRepairPiece;
//	private String idRepairLocation;
//	private String idRepairType;
//	private Byte[] photoInspection;
//
//	@JsonProperty("id_category")
//	public String getIdCategory() {
//		return idCategory;
//	}
//	public void setIdCategory(String idCategory) {
//		this.idCategory = idCategory;
//	}
//	@JsonProperty("id_sub_category")
//	public String getIdSubCategory() {
//		return idSubCategory;
//	}
//	public void setIdSubCategory(String idSubCategory) {
//		this.idSubCategory = idSubCategory;
//	}
//	@JsonProperty("id_inspection_point")
//	public String getIdInspectionPoint() {
//		return idInspectionPoint;
//	}
//	public void setIdInspectionPoint(String idInspectionPoint) {
//		this.idInspectionPoint = idInspectionPoint;
//	}
//	@JsonProperty("id_inspection_answer")
//	public Long getInspectionAnswer() {
//		return inspectionAnswer;
//	}
//	public void setInspectionAnswer(Long inspectionAnswer) {
//		this.inspectionAnswer = inspectionAnswer;
//	}
//	@JsonProperty("id_dimple")
//	public Long getIdDimple() {
//		return idDimple;
//	}
//	public void setIdDimple(Long idDimple) {
//		this.idDimple = idDimple;
//	}
//	@JsonProperty("id_coordinate_dimple_x")
//	public String getCoordinatesDimplesX() {
//		return coordinatesDimplesX;
//	}
//	public void setCoordinatesDimplesX(String coordinatesDimplesX) {
//		this.coordinatesDimplesX = coordinatesDimplesX;
//	}
//	@JsonProperty("id_coordinate_dimple_y")
//	public String getCoordinatesDimplesY() {
//		return CoordinatesDimplesY;
//	}
//	public void setCoordinatesDimplesY(String coordinatesDimplesY) {
//		CoordinatesDimplesY = coordinatesDimplesY;
//	}
//	@JsonProperty("dimple_height")
//	public Long getDimpleHeight() {
//		return dimpleHeight;
//	}
//	public void setDimpleHeight(Long dimpleHeight) {
//		this.dimpleHeight = dimpleHeight;
//	}
//	@JsonProperty("dimple_width")
//	public Long getDimpleWidth() {
//		return dimpleWidth;
//	}
//	public void setDimpleWidth(Long dimpleWidth) {
//		this.dimpleWidth = dimpleWidth;
//	}
//	@JsonProperty("dimple_image")
//	public Byte[] getDimpleImage() {
//		return dimpleImage;
//	}
//	public void setDimpleImage(Byte[] dimpleImage) {
//		this.dimpleImage = dimpleImage;
//	}
//	@JsonProperty("id_repair_piece")
//	public Long getIdRepairPiece() {
//		return idRepairPiece;
//	}
//	public void setIdRepairPiece(Long idRepairPiece) {
//		this.idRepairPiece = idRepairPiece;
//	}
//	@JsonProperty("id_repair_location")
//	public String getIdRepairLocation() {
//		return idRepairLocation;
//	}
//	public void setIdRepairLocation(String idRepairLocation) {
//		this.idRepairLocation = idRepairLocation;
//	}
//	@JsonProperty("id_repair_type")
//	public String getIdRepairType() {
//		return idRepairType;
//	}
//	public void setIdRepairType(String idRepairType) {
//		this.idRepairType = idRepairType;
//	}
//	@JsonProperty("photo_inspection")
//	public Byte[] getPhotoInspection() {
//		return photoInspection;
//	}
//	public void setPhotoInspection(Byte[] photoInspection) {
//		this.photoInspection = photoInspection;
//	}
//}

