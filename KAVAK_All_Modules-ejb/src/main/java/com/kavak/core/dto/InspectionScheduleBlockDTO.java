package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InspectionScheduleBlockDTO {
	
	private Long id;
	private Long idHourBlock;
	private String block;
    private String checkpointLabel;
    private boolean available;
	private boolean active;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("block_hour_id")
	public Long getIdHourBlock() {
		return idHourBlock;
	}
	public void setIdHourBlock(Long idHourBlock) {
		this.idHourBlock = idHourBlock;
	}

    @JsonProperty("block")
    public String getBlock() {
        return block;
    }
    public void setBlock(String block) {
        this.block = block;
    }

    @JsonProperty("checkpoint_label")
    public String getCheckpointLabel() {
        return checkpointLabel;
    }
    public void setCheckpointLabel(String checkpointLabel) {
        this.checkpointLabel = checkpointLabel;
    }

	@JsonProperty("is_available")
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}

	@JsonProperty("is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
}

