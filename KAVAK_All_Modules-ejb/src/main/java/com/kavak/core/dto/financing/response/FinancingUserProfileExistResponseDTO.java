package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserProfileExistResponseDTO {

	private String exist;

	@JsonProperty("exist")
	public String getExist() {
	    return exist;
	}

	public void setExist(String exist) {
	    this.exist = exist;
	}


}