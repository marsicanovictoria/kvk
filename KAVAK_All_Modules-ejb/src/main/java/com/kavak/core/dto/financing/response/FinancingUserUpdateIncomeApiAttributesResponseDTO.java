package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateIncomeApiAttributesResponseDTO {

    private String netIncomeVerified;
    private String incomePaymentFrequency;
    private String incomePaymentType;
    private String incomeProfile;
    
    @JsonProperty("netIncomeVerified")
    public String getNetIncomeVerified() {
        return netIncomeVerified;
    }
    public void setNetIncomeVerified(String netIncomeVerified) {
        this.netIncomeVerified = netIncomeVerified;
    }
    
    @JsonProperty("incomePaymentFrequency")
    public String getIncomePaymentFrequency() {
        return incomePaymentFrequency;
    }
    public void setIncomePaymentFrequency(String incomePaymentFrequency) {
        this.incomePaymentFrequency = incomePaymentFrequency;
    }
    
    @JsonProperty("incomePaymentType")
    public String getIncomePaymentType() {
        return incomePaymentType;
    }
    public void setIncomePaymentType(String incomePaymentType) {
        this.incomePaymentType = incomePaymentType;
    }
    
    @JsonProperty("incomeProfile")
    public String getIncomeProfile() {
        return incomeProfile;
    }
    public void setIncomeProfile(String incomeProfile) {
        this.incomeProfile = incomeProfile;
    }


}