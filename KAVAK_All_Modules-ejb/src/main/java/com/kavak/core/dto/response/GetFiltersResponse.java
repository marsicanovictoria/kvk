package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;

import java.util.ArrayList;
import java.util.List;

public class GetFiltersResponse {
	
    private GenericDTO prices;
    private GenericDTO kms;
    private List<String> listYears;
    private List<GenericDTO> listBodysGenericDTO = new ArrayList<>();
    private List<GenericDTO> listMakesGenericDTO;
    private List<GenericDTO> listColorsGenericDTO;
    private List<String> listDoors;
    private List<String> listSeats;
    private List<String> listTransmissions;
    private List<String> listCilinder;
    private List<String> listTraction;
    private List<String> listFuels;
    private List<String> listUber;
    private String maxDownpayment;
    private String maxMonthlyPayment;
    private List<GenericDTO> horsepower;
    private List<String> filterStatus;
    private List<String> passengers;
    private List<String> locations;
	
	@JsonProperty("prices")
	public GenericDTO getPrices() {
		return prices;
	}
	public void setPrices(GenericDTO prices) {
		this.prices = prices;
	}
	@JsonProperty("kms")
	public GenericDTO getKms() {
		return kms;
	}
	public void setKms(GenericDTO kms) {
		this.kms = kms;
	}
	@JsonProperty("years")
	public List<String> getListYears() {
		return listYears;
	}
	public void setListYears(List<String> listYears) {
		this.listYears = listYears;
	}
	@JsonProperty("body_types")
	public List<GenericDTO> getListBodysGenericDTO() {
		return listBodysGenericDTO;
	}
	public void setListBodysGenericDTO(List<GenericDTO> listBodysGenericDTO) {
		this.listBodysGenericDTO = listBodysGenericDTO;
	}
	@JsonProperty("makes")
	public List<GenericDTO> getListMakesGenericDTO() {
		return listMakesGenericDTO;
	}
	public void setListMakesGenericDTO(List<GenericDTO> listMakesGenericDTO) {
		this.listMakesGenericDTO = listMakesGenericDTO;
	}
	@JsonProperty("colors")
	public List<GenericDTO> getListColorsGenericDTO() {
		return listColorsGenericDTO;
	}
	public void setListColorsGenericDTO(List<GenericDTO> listColorsGenericDTO) {
		this.listColorsGenericDTO = listColorsGenericDTO;
	}
	@JsonProperty("doors")
	public List<String> getListDoors() {
		return listDoors;
	}
	public void setListDoors(List<String> listDoors) {
		this.listDoors = listDoors;
	}
	@JsonProperty("seats")
	public List<String> getListSeats() {
		return listSeats;
	}
	public void setListSeats(List<String> listSeats) {
		this.listSeats = listSeats;
	}
	@JsonProperty("transmitions")
	public List<String> getListTransmissions() {
		return listTransmissions;
	}
	public void setListTransmissions(List<String> listTransmissions) {
		this.listTransmissions = listTransmissions;
	}
	@JsonProperty("cilinders")
	public List<String> getListCilinder() {
		return listCilinder;
	}
	public void setListCilinder(List<String> listCilinder) {
		this.listCilinder = listCilinder;
	}
	@JsonProperty("tractions")
	public List<String> getListTraction() {
		return listTraction;
	}
	public void setListTraction(List<String> listTraction) {
		this.listTraction = listTraction;
	}
	@JsonProperty("fuels")
	public List<String> getListFuels() {
		return listFuels;
	}
	public void setListFuels(List<String> listFuels) {
		this.listFuels = listFuels;
	}
	@JsonProperty("ubers")
	public List<String> getListUber() {
		return listUber;
	}
	public void setListUber(List<String> listUber) {
		this.listUber = listUber;
	}
	
	@JsonProperty("max_downpayment")
	public String getMaxDownpayment() {
		return maxDownpayment;
	}
	public void setMaxDownpayment(String maxDownpayment) {
		this.maxDownpayment = maxDownpayment;
	}
	
	@JsonProperty("max_monthly_payment")
	public String getMaxMonthlyPayment() {
		return maxMonthlyPayment;
	}
	public void setMaxMonthlyPayment(String maxMonthlyPayment) {
		this.maxMonthlyPayment = maxMonthlyPayment;
	}
	
	@JsonProperty("horsepower")
	public List<GenericDTO> getHorsepower() {
		return horsepower;
	}
	public void setHorsepower(List<GenericDTO> horsepower) {
		this.horsepower = horsepower;
	}
	
	@JsonProperty("filter_status")
	public List<String> getFilterStatus() {
		return filterStatus;
	}
	public void setFilterStatus(List<String> filterStatus) {
		this.filterStatus = filterStatus;
	}
	
	@JsonProperty("passengers")
	public List<String> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<String> passengers) {
		this.passengers = passengers;
	}

	@JsonProperty("locations")
	public List<String> getLocations() {
	    return locations;
	}
	public void setLocations(List<String> locations) {
	    this.locations = locations;
	}



}