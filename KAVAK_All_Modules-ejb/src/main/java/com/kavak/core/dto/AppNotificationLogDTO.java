package com.kavak.core.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppNotificationLogDTO {

	private Long id;
	private Long userId;
	private Long typeId;
	private Long tokenId;
	private String description;
	private Timestamp creationDate;

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}

	@JsonProperty("type_id")
	public Long getTypeId() {
	    return typeId;
	}

	public void setTypeId(Long typeId) {
	    this.typeId = typeId;
	}
	
	@JsonProperty("token_id")
	public Long getTokenId() {
	    return tokenId;
	}

	public void setTokenId(Long tokenId) {
	    this.tokenId = tokenId;
	}

	@JsonProperty("description")
	public String getDescription() {
	    return description;
	}

	public void setDescription(String description) {
	    this.description = description;
	}
	
	@JsonProperty("creation_date")
	public Timestamp getCreationDate() {
	    return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
	    this.creationDate = creationDate;
	}
























}
