package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrimsDTO {
	
	private String sku;
	private String trim;
	private String features;
	private String fullVersion;
	
	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	@JsonProperty("trim")
	public String getTrim() {
		return trim;
	}
	public void setTrim(String trim) {
		this.trim = trim;
	}
	@JsonProperty("features")
	public String getFeatures() {
		return features;
	}
	public void setFeatures(String features) {
		this.features = features;
	}
	@JsonProperty("full_version")
	public String getFullVersion() {
		return fullVersion;
	}
	public void setFullVersion(String fullVersion) {
		this.fullVersion = fullVersion;
	}

}
