package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AnswerDTO {

	private Long questionId;
	private Long optionId;

	
	@JsonProperty("question_id")
	public Long getQuestionId() {
	    return questionId;
	}
	public void setQuestionId(Long questionId) {
	    this.questionId = questionId;
	}
	
	@JsonProperty("option_id")
	public Long getOptionId() {
	    return optionId;
	}
	public void setOptionId(Long optionId) {
	    this.optionId = optionId;
	}

}