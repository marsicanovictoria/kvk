package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateDomicileApiAttributesDTO {

    	private String street;
    	private String externalNumber;
    	private String internalNumber;
        private String suburb;  //COLONY
        private String cityCouncil;  //DELEGATION
        private String state;
        private String zipCode;
        private Long residentYears;
        private String livingType;
        
        
	@JsonProperty("street")
	public String getStreet() {
	    return street;
	}
	public void setStreet(String street) {
	    this.street = street;
	}
	
	@JsonProperty("external_number")
	public String getExternalNumber() {
	    return externalNumber;
	}
	public void setExternalNumber(String externalNumber) {
	    this.externalNumber = externalNumber;
	}
	
	@JsonProperty("internal_number")
	public String getInternalNumber() {
	    return internalNumber;
	}
	public void setInternalNumber(String internalNumber) {
	    this.internalNumber = internalNumber;
	}
	
	@JsonProperty("suburb")
	public String getSuburb() {
	    return suburb;
	}
	public void setSuburb(String suburb) {
	    this.suburb = suburb;
	}
	
	@JsonProperty("city_council")
	public String getCityCouncil() {
	    return cityCouncil;
	}
	public void setCityCouncil(String cityCouncil) {
	    this.cityCouncil = cityCouncil;
	}
	
	@JsonProperty("state")
	public String getState() {
	    return state;
	}
	public void setState(String state) {
	    this.state = state;
	}
	
	@JsonProperty("zip_code")
	public String getZipCode() {
	    return zipCode;
	}
	public void setZipCode(String zipCode) {
	    this.zipCode = zipCode;
	}
	
	@JsonProperty("resident_years")
	public Long getResidentYears() {
	    return residentYears;
	}
	public void setResidentYears(Long residentYears) {
	    this.residentYears = residentYears;
	}
	
	@JsonProperty("living_type")
	public String getLivingType() {
	    return livingType;
	}
	public void setLivingType(String livingType) {
	    this.livingType = livingType;
	}


        

	

}