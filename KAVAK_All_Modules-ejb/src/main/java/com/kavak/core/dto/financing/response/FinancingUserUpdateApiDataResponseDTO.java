package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateApiDataResponseDTO {

    	private Long idFinancing;
	private String type;
	private FinancingUserUpdateApiAttributesResponseDTO attributes;

	
	@JsonProperty("id")
	public Long getIdFinancing() {
	    return idFinancing;
	}
	public void setIdFinancing(Long idFinancing) {
	    this.idFinancing = idFinancing;
	}
	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("attributes")
	public FinancingUserUpdateApiAttributesResponseDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserUpdateApiAttributesResponseDTO attributes) {
	    this.attributes = attributes;
	}

}