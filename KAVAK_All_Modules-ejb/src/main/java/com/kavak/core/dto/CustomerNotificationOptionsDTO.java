package com.kavak.core.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerNotificationOptionsDTO {
	
	private List<GenericDTO> options;

	@JsonProperty("options")
	public List<GenericDTO> getOptions() {
	    return options;
	}

	public void setOptions(List<GenericDTO> options) {
	    this.options = options;
	}

}