package com.kavak.core.dto.netsuite;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MakeAndModelDTO {
	private Long id;
	private String carMake;
	private String carModel;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}
	
	@JsonProperty("model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	
}
