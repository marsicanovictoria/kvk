package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.financing.request.FinancingUserRfcApiDataRequestDTO;

public class FinancingUserRfcApiBodyResponseDTO {

    	private FinancingUserRfcApiDataRequestDTO data;

    	
    	@JsonProperty("data")
	public FinancingUserRfcApiDataRequestDTO getData() {
	    return data;
	}
	public void setData(FinancingUserRfcApiDataRequestDTO data) {
	    this.data = data;
	}

}
