package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetaValueReferenceLessorDTO {

	private String name;
	private String firstLastname;
	private String secondLastname;
	private String phone;
	private String address;
	private Long relationship;

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("first_lastname")
	public String getFirstLastname() {
		return firstLastname;
	}

	public void setFirstLastname(String firstLastname) {
		this.firstLastname = firstLastname;
	}

	@JsonProperty("second_lastname")
	public String getSecondLastname() {
		return secondLastname;
	}

	public void setSecondLastname(String secondLastname) {
		this.secondLastname = secondLastname;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("relationship")
	public Long getRelationship() {
		return relationship;
	}

	public void setRelationship(Long relationship) {
		this.relationship = relationship;
	}

}
