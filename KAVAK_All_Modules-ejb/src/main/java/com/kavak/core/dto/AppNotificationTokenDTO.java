package com.kavak.core.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppNotificationTokenDTO {

	private Long id;
	private Long userId;
	private String tokenType;
	private String deviceId;
	private String tokenDevice;
	private boolean isActive;
	private Timestamp creationDate;

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}

	@JsonProperty("token_type")
	public String getTokenType() {
	    return tokenType;
	}

	public void setTokenType(String tokenType) {
	    this.tokenType = tokenType;
	}
	
	@JsonProperty("device_id")
	public String getDeviceId() {
	    return deviceId;
	}

	public void setDeviceId(String deviceId) {
	    this.deviceId = deviceId;
	}
	
	@JsonProperty("token_device")
	public String getTokenDevice() {
	    return tokenDevice;
	}

	public void setTokenDevice(String tokenDevice) {
	    this.tokenDevice = tokenDevice;
	}
	
	@JsonProperty("creation_date")
	public Timestamp getCreationDate() {
	    return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
	    this.creationDate = creationDate;
	}

	@JsonProperty("is_active")
	public boolean isActive() {
	    return isActive;
	}

	public void setActive(boolean isActive) {
	    this.isActive = isActive;
	}

















}
