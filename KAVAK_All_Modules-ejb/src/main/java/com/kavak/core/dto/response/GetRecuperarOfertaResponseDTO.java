package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;


public class GetRecuperarOfertaResponseDTO {
	
	private Long offerCheckpointId;
	private String offerDate;
	private Integer step;
	private Long year;
	private String make;
	private String model;
	private String trim;
	private Long color;
	private String sku;
	private Long km;
	private String postalCode;
	private Boolean servedZone;
	private OffersResponseDTO offers;
	private String inspectionDate;
	private String inspection_hour_block;
	private String inspection_location;
	private String inspectionAddress;
	private String hour_inspection_slot_text;
	private String colony_loc;
	private String name_loc;
	private String street_customer;
	private String colony;
	private String municipality;
	private String num_exterior;
	private String num_interior;
	private String offerSelected;
	
	
	
	
	@JsonProperty("offer_checkpoint_id")
	public Long getOfferCheckpointId() {
		return offerCheckpointId;
	}
	public void setOfferCheckpointId(Long offerCheckpointId) {
		this.offerCheckpointId = offerCheckpointId;
	}
	@JsonProperty("offerDate")
	public String getOfferDate() {
		return offerDate;
	}
	public void setOfferDate(String offerDate) {
		this.offerDate = offerDate;
	}
	@JsonProperty("step")
	public Integer getStep() {
		return step;
	}
	public void setStep(Integer step) {
		this.step = step;
	}
	@JsonProperty("year")
	public Long getYear() {
		return year;
	}
	public void setYear(Long year) {
		this.year = year;
	}
	@JsonProperty("make")
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	@JsonProperty("model")
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	@JsonProperty("trim")
	public String getTrim() {
		return trim;
	}
	public void setTrim(String trim) {
		this.trim = trim;
	}
	@JsonProperty("color")
	public Long getColor() {
		return color;
	}
	public void setColor(Long color) {
		this.color = color;
	}
	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	@JsonProperty("km")
	public Long getKm() {
		return km;
	}
	public void setKm(Long km) {
		this.km = km;
	}
	@JsonProperty("postal_code")
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	@JsonProperty("served_zone")
	public Boolean getServedZone() {
		return servedZone;
	}
	public void setServedZone(Boolean servedZone) {
		this.servedZone = servedZone;
	}
	@JsonProperty("offers")
	public OffersResponseDTO getOffers() {
		return offers;
	}
	public void setOffers(OffersResponseDTO offers) {
		this.offers = offers;
	}
	@JsonProperty("inspection_date")
	public String getInspectionDate() {
		return inspectionDate;
	}
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}
	@JsonProperty("inspection_hour_block")
	public String getInspection_hour_block() {
		return inspection_hour_block;
	}
	public void setInspection_hour_block(String inspection_hour_block) {
		this.inspection_hour_block = inspection_hour_block;
	}
	@JsonProperty("inspection_location")
	public String getInspection_location() {
		return inspection_location;
	}
	public void setInspection_location(String inspection_location) {
		this.inspection_location = inspection_location;
	}
	@JsonProperty("inspection_address")
	public String getInspectionAddress() {
		return inspectionAddress;
	}
	public void setInspectionAddress(String inspectionAddress) {
		this.inspectionAddress = inspectionAddress;
	}
	@JsonProperty("hour_inspection_slot_text")
	public String getHour_inspection_slot_text() {
		return hour_inspection_slot_text;
	}
	public void setHour_inspection_slot_text(String hour_inspection_slot_text) {
		this.hour_inspection_slot_text = hour_inspection_slot_text;
	}
	@JsonProperty("colony_loc")
	public String getColony_loc() {
		return colony_loc;
	}
	public void setColony_loc(String colony_loc) {
		this.colony_loc = colony_loc;
	}
	@JsonProperty("name_loc")
	public String getName_loc() {
		return name_loc;
	}
	public void setName_loc(String name_loc) {
		this.name_loc = name_loc;
	}
	@JsonProperty("street_customer")
	public String getStreet_customer() {
		return street_customer;
	}
	public void setStreet_customer(String street_customer) {
		this.street_customer = street_customer;
	}
	@JsonProperty("colony")
	public String getColony() {
		return colony;
	}
	public void setColony(String colony) {
		this.colony = colony;
	}
	@JsonProperty("municipality")
	public String getMunicipality() {
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}
	@JsonProperty("num_exterior")
	public String getNum_exterior() {
		return num_exterior;
	}
	public void setNum_exterior(String num_exterior) {
		this.num_exterior = num_exterior;
	}
	@JsonProperty("num_interior")
	public String getNum_interior() {
		return num_interior;
	}
	public void setNum_interior(String num_interior) {
		this.num_interior = num_interior;
	}
	@JsonProperty("offer_selected")
	public String getOfferSelected() {
		return offerSelected;
	}
	public void setOfferSelected(String offerSelected) {
		this.offerSelected = offerSelected;
	}
	
	
	
	
	
	
	

}
