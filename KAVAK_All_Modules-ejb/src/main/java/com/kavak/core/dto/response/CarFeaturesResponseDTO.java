package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;

import java.util.List;

public class CarFeaturesResponseDTO {

	private List<GenericDTO> listDetails;
	private List<GenericDTO> listPerformances;
	private List<GenericDTO> listFuelConsumption;
	private List<String> listEntertainment;
	private List<String> listExterior;
	private List<String> listInterior;
	private List<String> listMechanics;
	private List<String> listSafety;

	@JsonProperty("details")
	public List<GenericDTO> getListDetails() {
		return listDetails;
	}

	public void setListDetails(List<GenericDTO> listDetails) {
		this.listDetails = listDetails;
	}
	@JsonProperty("performance")
	public List<GenericDTO> getlistPerformances() {
		return listPerformances;
	}

	public void setListPerformances(List<GenericDTO> listPerformances) {
		this.listPerformances = listPerformances;
	}
	@JsonProperty("fuel_consumption")
	public List<GenericDTO> getListFuelConsumption() {
		return listFuelConsumption;
	}

	public void setListFuelConsumption(List<GenericDTO> listFuelConsumption) {
		this.listFuelConsumption = listFuelConsumption;
	}
	@JsonProperty("entertainment")
	public List<String> getListEntertainment() {
		return listEntertainment;
	}

	public void setListEntertainment(List<String> listEntertainment) {
		this.listEntertainment = listEntertainment;
	}
	@JsonProperty("exterior")
	public List<String> getListExterior() {
		return listExterior;
	}

	public void setListExterior(List<String> listExterior) {
		this.listExterior = listExterior;
	}
	@JsonProperty("interior")
	public List<String> getListInterior() {
		return listInterior;
	}

	public void setListInterior(List<String> listInterior) {
		this.listInterior = listInterior;
	}
	@JsonProperty("mechanical")
	public List<String> getListMechanics() {
		return listMechanics;
	}

	public void setListMechanics(List<String> listMechanics) {
		this.listMechanics = listMechanics;
	}
	@JsonProperty("safety")
	public List<String> getlistSafety() {
		return listSafety;
	}

	public void setListSafety(List<String> listSafety) {
		this.listSafety = listSafety;
	}
	
}
