package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserCheckPointsDTO {

	 	private Long idUser;
	    private String customerName;
	    private String email;
	    private String address;
	    private String phone;
	    
	    @JsonProperty("id_user")
		public Long getIdUser() {
			return idUser;
		}
		public void setIdUser(Long idUser) {
			this.idUser = idUser;
		}
		@JsonProperty("customer_name")
		public String getCustomerName() {
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		@JsonProperty("email")
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		@JsonProperty("address")
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		@JsonProperty("phone")
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
	
	
}
