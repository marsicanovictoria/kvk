package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetWhyLoveResponseDTO {

	private String idInspectionVideo;
	private String whyCarText;
	private String oldOwnerTestimonial;
	private String idTestimanialVideo;
	
	@JsonProperty("inspection_video_id")
	public String getIdInspectionVideo() {
		return idInspectionVideo;
	}
	public void setIdInspectionVideo(String idInspectionVideo) {
		this.idInspectionVideo = idInspectionVideo;
	}
	@JsonProperty("why_kavak")
	public String getWhyCarText() {
		return whyCarText;
	}
	public void setWhyCarText(String whyCarText) {
		this.whyCarText = whyCarText;
	}
	@JsonProperty("old_owner_testimonial")
	public String getOldOwnerTestimonial() {
		return oldOwnerTestimonial;
	}
	public void setOldOwnerTestimonial(String oldOwnerTestimonial) {
		this.oldOwnerTestimonial = oldOwnerTestimonial;
	}
	@JsonProperty("testimanial_video_id")
	public String getIdTestimanialVideo() {
		return idTestimanialVideo;
	}
	public void setIdTestimanialVideo(String idTestimanialVideo) {
		this.idTestimanialVideo = idTestimanialVideo;
	}
}
