package com.kavak.core.dto;

import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.model.AppNotificationType;

public class AppNotificationSettingDTO {

	private Long id;
	private Long userId;
	private Long typeId;
	private boolean isActive;
	private Timestamp creationDate;
	private List<AppNotificationType> listAppNotificationType;
//	private String descriptionNotificationType;
//	private String titleNotificationType;

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}

	@JsonProperty("type_id")
	public Long getTypeId() {
	    return typeId;
	}

	public void setTypeId(Long typeId) {
	    this.typeId = typeId;
	}

	@JsonProperty("creation_date")
	public Timestamp getCreationDate() {
	    return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
	    this.creationDate = creationDate;
	}

	@JsonProperty("is_active")
	public boolean isActive() {
	    return isActive;
	}

	public void setActive(boolean isActive) {
	    this.isActive = isActive;
	}

	public List<AppNotificationType> getListAppNotificationType() {
	    return listAppNotificationType;
	}

	public void setListAppNotificationType(List<AppNotificationType> listAppNotificationType) {
	    this.listAppNotificationType = listAppNotificationType;
	}

//	@JsonProperty("description")
//	public String getDescriptionNotificationType() {
//	    return descriptionNotificationType;
//	}
//
//	public void setDescriptionNotificationType(String descriptionNotificationType) {
//	    this.descriptionNotificationType = descriptionNotificationType;
//	}
//
//	@JsonProperty("title")
//	public String getTitleNotificationType() {
//	    return titleNotificationType;
//	}
//
//	public void setTitleNotificationType(String titleNotificationType) {
//	    this.titleNotificationType = titleNotificationType;
//	}









}
