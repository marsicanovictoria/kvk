package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MetaValueAdditionalDTO {

    private List<GenericDTO> listGenericDTODelay;
    private List<GenericDTO> listGenericDTOCarUse;
    private List<GenericDTO> listGenericDTOIncome;
    private List<GenericDTO> listGenericDTOMortgageCredit;
    private List<GenericDTO> listGenericDTOAutomotiveCredit;
    private List<GenericDTO> listGenericDTOCreditCard;

    @JsonProperty("delay")
    public List<GenericDTO> getListGenericDTODelay() {
        return listGenericDTODelay;
    }
    public void setListGenericDTODelay(List<GenericDTO> listGenericDTODelay) {
        this.listGenericDTODelay = listGenericDTODelay;
    }

    @JsonProperty("car_use")
    public List<GenericDTO> getListGenericDTOCarUse() {
        return listGenericDTOCarUse;
    }
    public void setListGenericDTOCarUse(List<GenericDTO> listGenericDTOCarUse) {
        this.listGenericDTOCarUse = listGenericDTOCarUse;
    }

    @JsonProperty("income")
    public List<GenericDTO> getListGenericDTOIncome() {
        return listGenericDTOIncome;
    }
    public void setListGenericDTOIncome(List<GenericDTO> listGenericDTOIncome) {
        this.listGenericDTOIncome = listGenericDTOIncome;
    }

    @JsonProperty("mortgage_credit")
    public List<GenericDTO> getListGenericDTOMortgageCredit() {
        return listGenericDTOMortgageCredit;
    }
    public void setListGenericDTOMortgageCredit(List<GenericDTO> listGenericDTOMortgageCredit) {
        this.listGenericDTOMortgageCredit = listGenericDTOMortgageCredit;
    }

    @JsonProperty("automotive_credit")
    public List<GenericDTO> getListGenericDTOAutomotiveCredit() {
        return listGenericDTOAutomotiveCredit;
    }
    public void setListGenericDTOAutomotiveCredit(List<GenericDTO> listGenericDTOAutomotiveCredit) {
        this.listGenericDTOAutomotiveCredit = listGenericDTOAutomotiveCredit;
    }

    @JsonProperty("credit_card")
    public List<GenericDTO> getListGenericDTOCreditCard() {
        return listGenericDTOCreditCard;
    }
    public void setListGenericDTOCreditCard(List<GenericDTO> listGenericDTOCreditCard) {
        this.listGenericDTOCreditCard = listGenericDTOCreditCard;
    }
}
