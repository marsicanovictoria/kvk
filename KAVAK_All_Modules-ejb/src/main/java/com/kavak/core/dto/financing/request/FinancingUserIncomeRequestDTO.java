package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserIncomeRequestDTO {

    	private String netIncomeVerified;
    	private String incomeProfile;
	private String automotiveCarUseType;
    	private String automotiveCarPrice;
    	private String automotiveDownpayment;
    	private String positionAge;

    	
    	@JsonProperty("net_income_verified")
	public String getNetIncomeVerified() {
	    return netIncomeVerified;
	}


	public void setNetIncomeVerified(String netIncomeVerified) {
	    this.netIncomeVerified = netIncomeVerified;
	}

    	@JsonProperty("income_profile")
	public String getIncomeProfile() {
	    return incomeProfile;
	}


	public void setIncomeProfile(String incomeProfile) {
	    this.incomeProfile = incomeProfile;
	}

    	@JsonProperty("automotive_car_use_type")
	public String getAutomotiveCarUseType() {
	    return automotiveCarUseType;
	}


	public void setAutomotiveCarUseType(String automotiveCarUseType) {
	    this.automotiveCarUseType = automotiveCarUseType;
	}

    	@JsonProperty("automotive_car_price")
	public String getAutomotiveCarPrice() {
	    return automotiveCarPrice;
	}


	public void setAutomotiveCarPrice(String automotiveCarPrice) {
	    this.automotiveCarPrice = automotiveCarPrice;
	}

    	@JsonProperty("automotive_downpayment")
	public String getAutomotiveDownpayment() {
	    return automotiveDownpayment;
	}


	public void setAutomotiveDownpayment(String automotiveDownpayment) {
	    this.automotiveDownpayment = automotiveDownpayment;
	}


    	@JsonProperty("position_age")
	public String getPositionAge() {
	    return positionAge;
	}
	public void setPositionAge(String positionAge) {
	    this.positionAge = positionAge;
	}

    	
    	



}
