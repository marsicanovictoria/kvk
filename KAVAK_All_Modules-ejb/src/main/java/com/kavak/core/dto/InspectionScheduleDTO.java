package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class InspectionScheduleDTO {
	
	private Long id;
	private Date inspectionDate;
	private Long idHourBlock;
	private boolean status;
	private Date selectionDate;
	private Long inspectionLocationId;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("inspection_date")
	public Date getInspectionDate() {
		return inspectionDate;
	}
	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}
	
	@JsonProperty("hour_block_id")
	public Long getIdHourBlock() {
		return idHourBlock;
	}
	public void setIdHourBlock(Long idHourBlock) {
		this.idHourBlock = idHourBlock;
	}
	
	@JsonProperty("status")
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@JsonProperty("date_when_selected")
	public Date getSelectionDate() {
		return selectionDate;
	}
	public void setSelectionDate(Date selectionDate) {
		this.selectionDate = selectionDate;
	}
	@JsonProperty("inspection_location_id")
	public Long getInspectionLocationId() {
		return inspectionLocationId;
	}
	public void setInspectionLocationId(Long inspectionLocationId) {
		this.inspectionLocationId = inspectionLocationId;
	}
}
