package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CarFeatureCategoryDTO {
	
	private Long id;
	private String name;
//	private boolean active; 
	private List<CarFeatureItemDTO> listCarFeatureItemDTO;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
//	@JsonProperty("is_active")
//	public boolean isActive() {
//		return active;
//	}
//
//	public void setActive(boolean active) {
//		this.active = active;
//	}
	@JsonProperty("features")
	public List<CarFeatureItemDTO> getListCarFeatureItemDTO() {
		return listCarFeatureItemDTO;
	}

	public void setListCarFeatureItemDTO(List<CarFeatureItemDTO> listCarFeatureItemDTO) {
		this.listCarFeatureItemDTO = listCarFeatureItemDTO;
	}
}
