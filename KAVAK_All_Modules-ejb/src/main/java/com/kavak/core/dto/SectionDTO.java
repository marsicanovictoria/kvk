package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SectionDTO {

	private String sectionName;
	private String sectionDescrption;
	private String image;
	private SectionPriceDTO priceDTO;

	@JsonProperty("section_name")
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	@JsonProperty("section_description")
	public String getSectionDescrption() {
		return sectionDescrption;
	}
	public void setSectionDescrption(String sectionDescrption) {
		this.sectionDescrption = sectionDescrption;
	}
	@JsonProperty("image")
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
    @JsonProperty("price")
    public SectionPriceDTO getPriceDTO() {
        return priceDTO;
    }
    public void setPriceDTO(SectionPriceDTO priceDTO) {
        this.priceDTO = priceDTO;
    }

}