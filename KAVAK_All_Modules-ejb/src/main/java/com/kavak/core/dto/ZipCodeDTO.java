package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZipCodeDTO {
	
	private Long shippingCost;
	
	@JsonProperty("shipping_cost")
	public Long getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(Long shippingCost) {
		this.shippingCost = shippingCost;
	}
}

