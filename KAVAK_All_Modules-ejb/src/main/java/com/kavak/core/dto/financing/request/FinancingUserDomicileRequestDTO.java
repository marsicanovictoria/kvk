package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserDomicileRequestDTO {

    	private String street;
    	private String internalNumber;
	private String externalNumber;
    	private String colony;
    	private String delegation;
    	private String state;
    	private String zipCode;
    	private String residentYears;
    	private String livingType;
    	
	
    	@JsonProperty("street")
	public String getStreet() {
	    return street;
	}
	public void setStreet(String street) {
	    this.street = street;
	}
	
    	@JsonProperty("external_number")
	public String getExternalNumber() {
	    return externalNumber;
	}
	public void setExternalNumber(String externalNumber) {
	    this.externalNumber = externalNumber;
	}
	
    	@JsonProperty("internal_number")
    	public String getInternalNumber() {
	    return internalNumber;
	}
	public void setInternalNumber(String internalNumber) {
	    this.internalNumber = internalNumber;
	}
	
    	@JsonProperty("colony")
	public String getColony() {
	    return colony;
	}
	public void setColony(String colony) {
	    this.colony = colony;
	}
	
    	@JsonProperty("delegation")
	public String getDelegation() {
	    return delegation;
	}
	public void setDelegation(String delegation) {
	    this.delegation = delegation;
	}
	
    	@JsonProperty("state")
	public String getState() {
	    return state;
	}
	public void setState(String state) {
	    this.state = state;
	}
	
    	@JsonProperty("zip_code")
	public String getZipCode() {
	    return zipCode;
	}
	public void setZipCode(String zipCode) {
	    this.zipCode = zipCode;
	}
	
    	@JsonProperty("resident_years")
	public String getResidentYears() {
	    return residentYears;
	}
	public void setResidentYears(String residentYears) {
	    this.residentYears = residentYears;
	}
	
    	@JsonProperty("living_type")
	public String getLivingType() {
	    return livingType;
	}
	public void setLivingType(String livingType) {
	    this.livingType = livingType;
	}
    	
    	



}
