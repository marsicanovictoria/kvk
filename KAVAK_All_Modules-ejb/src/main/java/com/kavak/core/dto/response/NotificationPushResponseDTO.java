package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificationPushResponseDTO {
	
	private String title;
	private String body;

	
	@JsonProperty("title")
	public String getTitle() {
	    return title;
	}
	public void setTitle(String title) {
	    this.title = title;
	}
	
	@JsonProperty("body")
	public String getBody() {
	    return body;
	}
	public void setBody(String body) {
	    this.body = body;
	}
	
}
