package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.UserCheckPointsDTO;

import java.util.List;

public class OffersResponseDTO {

	private Long id;
	private boolean servedZone;
	private boolean wishList;
	private String abTestCode;
	private Long abTestResult;
	private String urlRecoverOffer;
	private String urlTrackingRecoverOffer;
	private List<?> listData;
	private Long year;
	private String make;
	private String model;
	private String trim;
	private Long color;
	private String sku;
	private Long km;
	private String zipCode;
	private  UserCheckPointsDTO user;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("offer_check_point_id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("served_zone")
	public boolean isServedZone() {
		return servedZone;
	}
	public void setServedZone(boolean servedZone) {
		this.servedZone = servedZone;
	}

	@JsonProperty("wish_list")
	public boolean isWishList() {
		return wishList;
	}
	public void setWishList(boolean wishList) {
		this.wishList = wishList;
	}
	@JsonProperty("year")
	public Long getYear() {
		return year;
	}
	public void setYear(Long year) {
		this.year = year;
	}
	@JsonProperty("make")
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	@JsonProperty("model")
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	@JsonProperty("trim")
	public String getTrim() {
		return trim;
	}
	public void setTrim(String trim) {
		this.trim = trim;
	}
	@JsonProperty("color")
	public Long getColor() {
		return color;
	}
	public void setColor(Long color) {
		this.color = color;
	}
	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	@JsonProperty("km")
	public Long getKm() {
		return km;
	}
	public void setKm(Long km) {
		this.km = km;
	}
	@JsonProperty("postal_code")
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	@JsonProperty("user")
	public UserCheckPointsDTO getUser() {
		return user;
	}
	public void setUser(UserCheckPointsDTO user) {
		this.user = user;
	}
	@JsonProperty("url_recuperar_oferta")
	public String getUrlRecoverOffer() {
		return urlRecoverOffer;
	}
	public void setUrlRecoverOffer(String urlRecoverOffer) {
		this.urlRecoverOffer = urlRecoverOffer;
	}

	@JsonProperty("offer_check_points")
	public List<?> getListData() {
		return listData;
	}
	public void setListData(List<?> listData) {
		this.listData = listData;
	}
	
	@JsonProperty("ab_test_code")
	public String getAbTestCode() {
	    return abTestCode;
	}
	public void setAbTestCode(String abTestCode) {
	    this.abTestCode = abTestCode;
	}
	
	@JsonProperty("ab_test_result")
	public Long getAbTestResult() {
	    return abTestResult;
	}
	public void setAbTestResult(Long abTestResult) {
	    this.abTestResult = abTestResult;
	}

	@JsonProperty("url_tracking")
	public String getUrlTrackingRecoverOffer() {
	    return urlTrackingRecoverOffer;
	}
	public void setUrlTrackingRecoverOffer(String urlTrackingRecoverOffer) {
	    this.urlTrackingRecoverOffer = urlTrackingRecoverOffer;
	}
}