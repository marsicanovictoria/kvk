package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;

import java.util.List;

public class GetCarscoutResultsResponseDTO {

    private Long alertId;
    private List<GenericDTO> filters;

    @JsonProperty("alert_id")
    public Long getAlertId() {
        return alertId;
    }

    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    @JsonProperty("filters")
    public List<GenericDTO> getFilters() {
        return filters;
    }

    public void setFilters(List<GenericDTO> filters) {
        this.filters = filters;
    }

}
