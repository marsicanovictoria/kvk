package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PutUserResponseDTO {

    private Long userId;
    private String email;
    private String name;
    private String phone;
    private String address;
    private String street;
    private String exteriorNumber;
    private String interiorNumber;
    private String postalCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("exterior_number")
    public String getExteriorNumber() {
        return exteriorNumber;
    }

    public void setExteriorNumber(String exteriorNumber) {
        this.exteriorNumber = exteriorNumber;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("interior_number")
    public String getInteriorNumber() {
        return interiorNumber;
    }

    public void setInteriorNumber(String interiorNumber) {
        this.interiorNumber = interiorNumber;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("postal")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
