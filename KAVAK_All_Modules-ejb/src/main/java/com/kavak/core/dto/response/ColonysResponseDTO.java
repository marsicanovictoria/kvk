package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ColonysResponseDTO {

	private String zip;
	private String municipality;
	private String state;
	private List<String> listColonys;

	@JsonProperty("zip")
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	@JsonProperty("municipality")
	public String getMunicipality() {
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}
	@JsonProperty("state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@JsonProperty("colonys")
	public List<String> getListColonys() {
		return listColonys;
	}
	public void setListColonys(List<String> listColonys) {
		this.listColonys = listColonys;
	}
}