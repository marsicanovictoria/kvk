package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingPinCodeErrorResponseDTO {

	private String status;
	private String code;
	private String title;
	

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("status")
	public String getStatus() {
	    return status;
	}
	public void setStatus(String status) {
	    this.status = status;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("code")
	public String getCode() {
	    return code;
	}
	public void setCode(String code) {
	    this.code = code;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("title")
	public String getTitle() {
	    return title;
	}
	public void setTitle(String title) {
	    this.title = title;
	}



}