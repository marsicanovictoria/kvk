package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MetaValueJobDataDTO {

    private List<GenericDTO> listCompanyTypeDTO;
    private List<GenericDTO> listInsuranceAffiliationDTO;
    private List<GenericDTO> listPositionDTO;
    private List<GenericDTO> listProfessionDTO;
    private List<GenericDTO> listMonthsOldDTO;

    @JsonProperty("company_type")
    public List<GenericDTO> getListCompanyTypeDTO() {
        return listCompanyTypeDTO;
    }
    public void setListCompanyTypeDTO(List<GenericDTO> listCompanyTypeDTO) {
        this.listCompanyTypeDTO = listCompanyTypeDTO;
    }

    @JsonProperty("insurance_affiliation")
    public List<GenericDTO> getListInsuranceAffiliationDTO() {
        return listInsuranceAffiliationDTO;
    }
    public void setListInsuranceAffiliationDTO(List<GenericDTO> listInsuranceAffiliationDTO) {
        this.listInsuranceAffiliationDTO = listInsuranceAffiliationDTO;
    }

    @JsonProperty("position")
    public List<GenericDTO> getListPositionDTO() {
        return listPositionDTO;
    }
    public void setListPositionDTO(List<GenericDTO> listPositionDTO) {
        this.listPositionDTO = listPositionDTO;
    }

    @JsonProperty("profession")
    public List<GenericDTO> getListProfessionDTO() {
        return listProfessionDTO;
    }
    public void setListProfessionDTO(List<GenericDTO> listProfessionDTO) {
        this.listProfessionDTO = listProfessionDTO;
    }

    @JsonProperty("months_old")
    public List<GenericDTO> getListMonthsOldDTO() {
        return listMonthsOldDTO;
    }

    public void setListMonthsOldDTO(List<GenericDTO> listMonthsOldDTO) {
        this.listMonthsOldDTO = listMonthsOldDTO;
    }
}
