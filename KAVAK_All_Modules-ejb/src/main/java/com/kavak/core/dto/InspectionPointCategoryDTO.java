package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InspectionPointCategoryDTO {

	private Long id;
	private String name;
	private boolean active;
	private List<InspectionPointSubCategoryDTO> listInspectionPointSubCategoryDTO;
	private List<InspectionPointDTO> listInspectionPointDTO;



    //	private boolean photoRequired;
//	private boolean naButtonEnabled;
//	private boolean standartButtonEnabled;
//	private boolean repairButtonEnabled;
//	private boolean reprovedButtonEnabled;
//	private boolean showAdditionalInfo;
//	private String showAdditionalInfoKey;
//	private boolean captureAdditionalInfo;
//	private boolean generateDiscount;
//	private boolean generateSummaryComment;
//	private boolean generateEmailAlert;

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	@JsonProperty("subcategories")
	public List<InspectionPointSubCategoryDTO> getlistInspectionPointSubCategoryDTO() {
		return listInspectionPointSubCategoryDTO;
	}
	public void setListInspectionPointSubCategoryDTO(List<InspectionPointSubCategoryDTO> listInspectionPointSubCategoryDTO) {
		this.listInspectionPointSubCategoryDTO = listInspectionPointSubCategoryDTO;
	}

	@JsonProperty("inspection_points_category")
	public List<InspectionPointDTO> getlistInspectionPointDTO() {
		return listInspectionPointDTO;
	}
	public void setListInspectionPointDTO(List<InspectionPointDTO> listInspectionPointDTO) {
		this.listInspectionPointDTO = listInspectionPointDTO;
	}

//	@JsonProperty("is_photo_required")
//	public boolean isPhotoRequired() {
//		return photoRequired;
//	}
//
//	public void setPhotoRequired(boolean photoRequired) {
//		this.photoRequired = photoRequired;
//	}
//
//	@JsonProperty("is_na_button_enabled")
//	public boolean isNaButtonEnabled() {
//		return naButtonEnabled;
//	}
//
//	public void setNaButtonEnabled(boolean naButtonEnabled) {
//		this.naButtonEnabled = naButtonEnabled;
//	}
//
//	@JsonProperty("is_standard_button_enabled")
//	public boolean isStandartButtonEnabled() {
//		return standartButtonEnabled;
//	}
//
//	public void setStandartButtonEnabled(boolean standartButtonEnabled) {
//		this.standartButtonEnabled = standartButtonEnabled;
//	}
//
//	@JsonProperty("is_repair_button_enabled")
//	public boolean isRepairButtonEnabled() {
//		return repairButtonEnabled;
//	}
//
//	public void setRepairButtonEnabled(boolean repairButtonEnabled) {
//		this.repairButtonEnabled = repairButtonEnabled;
//	}
//
//	@JsonProperty("is_reproved_button_enabled")
//	public boolean isReprovedButtonEnabled() {
//		return reprovedButtonEnabled;
//	}
//
//	public void setReprovedButtonEnabled(boolean reprovedButtonEnabled) {
//		this.reprovedButtonEnabled = reprovedButtonEnabled;
//	}
//
//	@JsonProperty("is_show_additional_info")
//	public boolean isShowAdditionalInfo() {
//		return showAdditionalInfo;
//	}
//
//	public void setShowAdditionalInfo(boolean showAdditionalInfo) {
//		this.showAdditionalInfo = showAdditionalInfo;
//	}
//
//	@JsonProperty("show_additional_info_key")
//	public String getShowAdditionalInfoKey() {
//		return showAdditionalInfoKey;
//	}
//
//	public void setShowAdditionalInfoKey(String showAdditionalInfoKey) {
//		this.showAdditionalInfoKey = showAdditionalInfoKey;
//	}
//
//	@JsonProperty("is_capture_additional_info")
//	public boolean isCaptureAdditionalInfo() {
//		return captureAdditionalInfo;
//	}
//
//	public void setCaptureAdditionalInfo(boolean captureAdditionalInfo) {
//		this.captureAdditionalInfo = captureAdditionalInfo;
//	}
//
//
//
//	@JsonProperty("is_generates_discount")
//	public boolean isGenerateDiscount() {
//		return generateDiscount;
//	}
//
//	public void setGenerateDiscount(boolean generateDiscount) {
//		this.generateDiscount = generateDiscount;
//	}
//
//	@JsonProperty("is_generates_summary_comment")
//	public boolean isGenerateSummaryComment() {
//		return generateSummaryComment;
//	}
//
//	public void setGenerateSummaryComment(boolean generateSummaryComment) {
//		this.generateSummaryComment = generateSummaryComment;
//	}
//
//	@JsonProperty("is_generates_email_alert")
//	public boolean isGenerateEmailAlert() {
//		return generateEmailAlert;
//	}
//
//	public void setGenerateEmailAlert(boolean generateEmailAlert) {
//		this.generateEmailAlert = generateEmailAlert;
//	}
}
