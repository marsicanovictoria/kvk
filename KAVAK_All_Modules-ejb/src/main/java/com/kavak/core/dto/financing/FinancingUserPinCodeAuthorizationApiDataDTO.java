package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserPinCodeAuthorizationApiDataDTO {


	private String type;
	private FinancingUserPinCodeAuthorizationApiAttributesDTO attributes;
	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}

	
	@JsonProperty("attributes")
	public FinancingUserPinCodeAuthorizationApiAttributesDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserPinCodeAuthorizationApiAttributesDTO attributes) {
	    this.attributes = attributes;
	}


}