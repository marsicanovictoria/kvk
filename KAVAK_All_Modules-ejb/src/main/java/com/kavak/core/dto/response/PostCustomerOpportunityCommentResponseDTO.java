package com.kavak.core.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PostCustomerOpportunityCommentResponseDTO {

    private String voiceUrl;
    private String comment;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("voice_url")
    public String getVoiceUrl() {
        return voiceUrl;
    }

    public void setVoiceUrl(String voiceUrl) {
        this.voiceUrl = voiceUrl;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
