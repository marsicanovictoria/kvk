package com.kavak.core.dto.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;

public class PostCarscoutAlertRequestDTO {

	private Long userId;
	private String minPrice;
	private String maxPrice;
	private Integer maxKm;
	private Integer minKm;
	private List<GenericDTO> genericDTOListYear;
	private List<GenericDTO> genericDTOListBodyType;
	private List<GenericDTO> genericDTOListMake;
	private List<GenericDTO> genericDTOListDoors;
	private List<GenericDTO> genericDTOListSeats;
	private List<GenericDTO> genericDTOListCylinders;
	private List<GenericDTO> genericDTOListTraction;
	private List<GenericDTO> genericDTOListFuelType;
	private List<GenericDTO> genericDTOListModel;
	private List<GenericDTO> genericDTOListVersion;
	private List<GenericDTO> genericDTOListColor;
	private List<GenericDTO> genericDTOListTransmissions;
	private String source;
	private String versionApp;


	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@JsonProperty("min_price")
	public String getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}

	@JsonProperty("max_price")
	public String getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}

	@JsonProperty("years")
	public List<GenericDTO> getGenericDTOListYear() {
		return genericDTOListYear;
	}

	public void setGenericDTOListYear(List<GenericDTO> genericDTOListYear) {
		this.genericDTOListYear = genericDTOListYear;
	}

	@JsonProperty("bodytypes")
	public List<GenericDTO> getGenericDTOListBodyType() {
		return genericDTOListBodyType;
	}

	public void setGenericDTOListBodyType(List<GenericDTO> genericDTOListBodyType) {
		this.genericDTOListBodyType = genericDTOListBodyType;
	}

	@JsonProperty("makes")
	public List<GenericDTO> getGenericDTOListMake() {
		return genericDTOListMake;
	}

	public void setGenericDTOListMake(List<GenericDTO> genericDTOListMake) {
		this.genericDTOListMake = genericDTOListMake;
	}

	@JsonProperty("doors")
	public List<GenericDTO> getGenericDTOListDoors() {
		return genericDTOListDoors;
	}

	public void setGenericDTOListDoors(List<GenericDTO> genericDTOListDoors) {
		this.genericDTOListDoors = genericDTOListDoors;
	}

	@JsonProperty("seats")
	public List<GenericDTO> getGenericDTOListSeats() {
		return genericDTOListSeats;
	}

	public void setGenericDTOListSeats(List<GenericDTO> genericDTOListSeats) {
		this.genericDTOListSeats = genericDTOListSeats;
	}

	@JsonProperty("cylinders")
	public List<GenericDTO> getGenericDTOListCylinders() {
		return genericDTOListCylinders;
	}

	public void setGenericDTOListCylinders(List<GenericDTO> genericDTOListCylinders) {
		this.genericDTOListCylinders = genericDTOListCylinders;
	}

	@JsonProperty("tractions")
	public List<GenericDTO> getGenericDTOListTraction() {
		return genericDTOListTraction;
	}

	public void setGenericDTOListTraction(List<GenericDTO> genericDTOListTraction) {
		this.genericDTOListTraction = genericDTOListTraction;
	}

	@JsonProperty("fueltypes")
	public List<GenericDTO> getGenericDTOListFuelType() {
		return genericDTOListFuelType;
	}

	public void setGenericDTOListFuelType(List<GenericDTO> genericDTOListFuelType) {
		this.genericDTOListFuelType = genericDTOListFuelType;
	}

	@JsonProperty("models")
	public List<GenericDTO> getGenericDTOListModel() {
		return genericDTOListModel;
	}

	public void setGenericDTOListModel(List<GenericDTO> genericDTOListModel) {
		this.genericDTOListModel = genericDTOListModel;
	}

	@JsonProperty("versions")
	public List<GenericDTO> getGenericDTOListVersion() {
		return genericDTOListVersion;
	}

	public void setGenericDTOListVersion(List<GenericDTO> genericDTOListVersion) {
		this.genericDTOListVersion = genericDTOListVersion;
	}

	@JsonProperty("max_km")
	public Integer getMaxKm() {
		return maxKm;
	}

	public void setMaxKm(Integer maxKm) {
		this.maxKm = maxKm;
	}

	@JsonProperty("min_km")
	public Integer getMinKm() {
		return minKm;
	}

	public void setMinKm(Integer minKm) {
		this.minKm = minKm;
	}

	@JsonProperty("colors")
	public List<GenericDTO> getGenericDTOListColor() {
		return genericDTOListColor;
	}

	public void setGenericDTOListColor(List<GenericDTO> genericDTOListColor) {
		this.genericDTOListColor = genericDTOListColor;
	}

	@JsonProperty("transmissions")
	public List<GenericDTO> getGenericDTOListTransmissions() {
		return genericDTOListTransmissions;
	}

	public void setGenericDTOListTransmissions(List<GenericDTO> genericDTOListTransmissions) {
		this.genericDTOListTransmissions = genericDTOListTransmissions;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("source")
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("version_app")
	public String getVersionApp() {
		return versionApp;
	}

	public void setVersionApp(String versionApp) {
		this.versionApp = versionApp;
	}
}
