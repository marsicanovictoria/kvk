package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingVerifyUserInformationRegisterApiResponseDTO {

    private FinancingUserRegisterApiDataResponseDTO data;


    @JsonProperty("data")
    public FinancingUserRegisterApiDataResponseDTO getData() {
        return data;
    }

    public void setData(FinancingUserRegisterApiDataResponseDTO data) {
        this.data = data;
    }




}