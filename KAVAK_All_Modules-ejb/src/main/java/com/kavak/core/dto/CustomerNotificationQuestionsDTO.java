package com.kavak.core.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerNotificationQuestionsDTO {
	
	private Long id;
	private String title;
	private List<GenericDTO> options;
	//private CustomerNotificationOptionsDTO customerNotificationOptions; 


	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public List<GenericDTO> getOptions() {
	    return options;
	}
	
	@JsonProperty("title")
	public String getTitle() {
	    return title;
	}
	public void setTitle(String title) {
	    this.title = title;
	}
	
	@JsonProperty("options")
	public void setOptions(List<GenericDTO> options) {
	    this.options = options;
	}
	public void setId(Long id) {
		this.id = id;
	}



}