package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserProfileDTO {

    	private Long id;
    	private String content;
    	private String name;

	@JsonProperty("id")
        public Long getId() {
    	return id;
        }
        public void setId(Long id) {
    	this.id = id;
        }
        
	@JsonProperty("content")
    	public String getContent() {
    	    return content;
    	}
    	public void setContent(String content) {
	    this.content = content;
	}
	
	@JsonProperty("name")
	public String getName() {
	    return name;
	}
	public void setName(String name) {
	    this.name = name;
	}


}