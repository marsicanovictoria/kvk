package com.kavak.core.dto.netsuite;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConfirmedInspectionNetsuiteDTO {
	
	private Long id;
	private Long opportunityId;
	private String inspectionDate;
	private String inspectionTime;
	private Long customerMinervaId;
	private String customerEmail;
	private String customerPhone;
	private String customerAddress;
	private String inspectorEmail;
	private String inspectorName;
	private String inspectorPhone;
	private String leadManagerName;
	private String leadManagerEmail;
	private String leadManagerPhone;
	private String carYear;
	private String carMake;
	private String carModel;
	private Long carKm;
	private String consigmentOffer;
	private String thirtyDaysOffer;
	private String instantOffer;
	private String validityDate;
	private String wingmanName;
	private String wingmanEmail;
	private String wingmanPhone;
	private ArrayList<String> attachments;
	private String emailTemplate;
	private String bccEmail;
	private String replyToEmail;
	private Long selectedOffer;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("opportunity_id")
	public Long getOpportunityId() {
		return opportunityId;
	}
	public void setOpportunityId(Long opportunityId) {
		this.opportunityId = opportunityId;
	}
	@JsonProperty("inspection_date")
	public String getInspectionDate() {
		return inspectionDate;
	}
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}
	
	@JsonProperty("inspection_time")
	public String getInspectionTime() {
		return inspectionTime;
	}
	public void setInspectionTime(String inspectionTime) {
		this.inspectionTime = inspectionTime;
	}
	
	@JsonProperty("customer_minerva_id")	
	public Long getCustomerMinervaId() {
		return customerMinervaId;
	}
	public void setCustomerMinervaId(Long customerMinervaId) {
		this.customerMinervaId = customerMinervaId;
	}
	
	@JsonProperty("customer_email")
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	@JsonProperty("customer_phone")
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	
	@JsonProperty("customer_address")
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	
	@JsonProperty("inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	
	@JsonProperty("inspector_name")
	public String getInspectorName() {
		return inspectorName;
	}
	public void setInspectorName(String inspectorName) {
		this.inspectorName = inspectorName;
	}
	@JsonProperty("inspector_phone")
	public String getInspectorPhone() {
		return inspectorPhone;
	}
	
	public void setInspectorPhone(String inspectorPhone) {
		this.inspectorPhone = inspectorPhone;
	}
	
	@JsonProperty("lead_manager_email")
	public String getLeadManagerEmail() {
		return leadManagerEmail;
	}
	public void setLeadManagerEmail(String leadManagerEmail) {
		this.leadManagerEmail = leadManagerEmail;
	}
	
	@JsonProperty("lead_manager_name")
	public String getLeadManagerName() {
		return leadManagerName;
	}
	
	public void setLeadManagerName(String leadManagerName) {
		this.leadManagerName = leadManagerName;
	}
	
	@JsonProperty("lead_manager_phone")
	public String getLeadManagerPhone() {
		return leadManagerPhone;
	}
	
	public void setLeadManagerPhone(String leadManagerPhone) {
		this.leadManagerPhone = leadManagerPhone;
	}
	
	@JsonProperty("car_year")
	public String getCarYear() {
		return carYear;
	}
	public void setCarYear(String carYear) {
		this.carYear = carYear;
	}
	
	@JsonProperty("car_make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}
	
	@JsonProperty("car_model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	
	@JsonProperty("car_km")
	public Long getCarKm() {
		return carKm;
	}
	public void setCarKm(Long carKm) {
		this.carKm = carKm;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("consignment_offer")
	public String getConsigmentOffer() {
		return consigmentOffer;
	}
	public void setConsigmentOffer(String consigmentOffer) {
		this.consigmentOffer = consigmentOffer;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("thirty_days_offer")
	public String getThirtyDaysOffer() {
		return thirtyDaysOffer;
	}
	public void setThirtyDaysOffer(String thirtyDaysOffer) {
		this.thirtyDaysOffer = thirtyDaysOffer;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("instant_offer")
	public String getInstantOffer() {
		return instantOffer;
	}
	public void setInstantOffer(String instantOffer) {
		this.instantOffer = instantOffer;
	}
	
	@JsonProperty("validity_date")
	public String getValidityDate() {
		return validityDate;
	}
	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}
	
	@JsonProperty("wingman_name")
	public String getWingmanName() {
		return wingmanName;
	}
	public void setWingmanName(String wingmanName) {
		this.wingmanName = wingmanName;
	}
	@JsonProperty("wingman_email")
	public String getWingmanEmail() {
		return wingmanEmail;
	}
	public void setWingmanEmail(String wingmanEmail) {
		this.wingmanEmail = wingmanEmail;
	}
	
	@JsonProperty("wingman_phone")
	public String getWingmanPhone() {
		return wingmanPhone;
	}
	public void setWingmanPhone(String wingmanPhone) {
		this.wingmanPhone = wingmanPhone;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("attachments")
	public ArrayList<String> getAttachments() {
		return attachments;
	}
	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("email_template")
	public String getEmailTemplate() {
		return emailTemplate;
	}
	public void setEmailTemplate(String emailTemplate) {
		this.emailTemplate = emailTemplate;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("bcc_email")
	public String getBccEmail() {
		return bccEmail;
	}
	public void setBccEmail(String bccEmail) {
		this.bccEmail = bccEmail;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("reply_to_email")
	public String getReplyToEmail() {
		return replyToEmail;
	}
	public void setReplyToEmail(String replyToEmail) {
		this.replyToEmail = replyToEmail;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("selected_offer")
	public Long getSelectedOffer() {
		return selectedOffer;
	}
	public void setSelectedOffer(Long selectedOffer) {
		this.selectedOffer = selectedOffer;
	}
	
}
