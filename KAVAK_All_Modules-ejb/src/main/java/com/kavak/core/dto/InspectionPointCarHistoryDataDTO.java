package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Enrique on 27-Jun-17.
 */
public class InspectionPointCarHistoryDataDTO {

    private String name;
    private boolean checked;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty("is_checked")
    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
