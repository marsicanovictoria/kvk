package com.kavak.core.dto.specification;

import java.util.List;

public class CarscoutAlertSpecificationDTO {

    private List<Long> idsListYears;
    private List<Long> idsListBodytype;
    private List<Long> idsListMake;
    private List<Long> idsListVersion;
    private List<Long> idsListModel;
    private List<Long> idsListDoors;
    private List<Long> idsListSeats;
    private List<Long> idsListCylinders;
    private List<Long> idsListTraction;
    private List<Long> idsListFueltype;
    private List<Long> idsListTransmissions;
    private Integer minPrice;
    private Integer maxPrice;
    private Integer maxKm;
    private Integer minKm;

    public List<Long> getIdsListYears() {
        return idsListYears;
    }

    public void setIdsListYears(List<Long> idsListYears) {
        this.idsListYears = idsListYears;
    }

    public List<Long> getIdsListBodytype() {
        return idsListBodytype;
    }

    public void setIdsListBodytype(List<Long> idsListBodytype) {
        this.idsListBodytype = idsListBodytype;
    }

    public List<Long> getIdsListMake() {
        return idsListMake;
    }

    public void setIdsListMake(List<Long> idsListMake) {
        this.idsListMake = idsListMake;
    }

    public List<Long> getIdsListVersion() {
        return idsListVersion;
    }

    public void setIdsListVersion(List<Long> idsListVersion) {
        this.idsListVersion = idsListVersion;
    }

    public List<Long> getIdsListModel() {
        return idsListModel;
    }

    public void setIdsListModel(List<Long> idsListModel) {
        this.idsListModel = idsListModel;
    }

    public List<Long> getIdsListDoors() {
        return idsListDoors;
    }

    public void setIdsListDoors(List<Long> idsListDoors) {
        this.idsListDoors = idsListDoors;
    }

    public List<Long> getIdsListSeats() {
        return idsListSeats;
    }

    public void setIdsListSeats(List<Long> idsListSeats) {
        this.idsListSeats = idsListSeats;
    }

    public List<Long> getIdsListCylinders() {
        return idsListCylinders;
    }

    public void setIdsListCylinders(List<Long> idsListCylinders) {
        this.idsListCylinders = idsListCylinders;
    }

    public List<Long> getIdsListTraction() {
        return idsListTraction;
    }

    public void setIdsListTraction(List<Long> idsListTraction) {
        this.idsListTraction = idsListTraction;
    }

    public List<Long> getIdsListFueltype() {
        return idsListFueltype;
    }

    public void setIdsListFueltype(List<Long> idsListFueltype) {
        this.idsListFueltype = idsListFueltype;
    }

    public List<Long> getIdsListTransmissions() {
        return idsListTransmissions;
    }

    public void setIdsListTransmissions(List<Long> idsListTransmissions) {
        this.idsListTransmissions = idsListTransmissions;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Integer getMaxKm() {
        return maxKm;
    }

    public void setMaxKm(Integer maxKm) {
        this.maxKm = maxKm;
    }

    public Integer getMinKm() {
        return minKm;
    }

    public void setMinKm(Integer minKm) {
        this.minKm = minKm;
    }
}
