package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PutCarContentOverviewDataDTO {
	
	private Long idInspection;
	private String inspectorEmail;
	private String whyCarText;
	private String whyCarAudio;
	private List<InspectionCarFeatureDTO> listInspectionCarFeatureDTO;
	
	@JsonProperty("id_inspection")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}
	@JsonProperty("inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	@JsonProperty("why_kavak")
	public String getWhyCarText() {
		return whyCarText;
	}
	public void setWhycarText(String whyCarText) {
		this.whyCarText = whyCarText;
	}
	@JsonProperty("why_kavak_audio")
	public String getWhyCarAudio() {
		return whyCarAudio;
	}
	public void setWhyCarAudio(String whyCarAudio) {
		this.whyCarAudio = whyCarAudio;
	}
	@JsonProperty("features")
	public List<InspectionCarFeatureDTO> getListInspectionCarFeatureDTO() {
		return listInspectionCarFeatureDTO;
	}
	public void setListInspectionCarFeatureDTO(List<InspectionCarFeatureDTO> listInspectionCarFeatureDTO) {
		this.listInspectionCarFeatureDTO = listInspectionCarFeatureDTO;
	}
	
}
