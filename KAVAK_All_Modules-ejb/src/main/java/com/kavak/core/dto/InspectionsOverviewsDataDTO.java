package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Enrique on 23-Jun-17.
 */
public class InspectionsOverviewsDataDTO {

    private Long idInspection;
    private boolean inspectionInitiated;
    private OfferCheckpointDTO offerCheckpointDTO;

    @JsonProperty("inspection_id")
    public Long getIdInspection() {
        return idInspection;
    }
    public void setIdInspection(Long idInspection) {
        this.idInspection = idInspection;
    }

    @JsonProperty("inspection_initiated")
    public boolean isInspectionInitiated() {
        return inspectionInitiated;
    }
    public void setInspectionInitiated(boolean inspectionInitiated) {
        this.inspectionInitiated = inspectionInitiated;
    }

    @JsonProperty("offer_data")
    public OfferCheckpointDTO getOfferCheckpointDTO() {
        return offerCheckpointDTO;
    }
    public void setOfferCheckpointDTO(OfferCheckpointDTO offerCheckpointDTO) {
        this.offerCheckpointDTO = offerCheckpointDTO;
    }
}
