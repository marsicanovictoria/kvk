package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.financing.FinancingGenericDTO;

public class FinancingVerifyUserInformationResponseDTO {
    
    private String idFinancing;
    private String email;
    private String authenticationToken;
    private String fullName;
    private String firstSurname;
    private String secondSurname;
    private FinancingGenericDTO gender;
    private String phone;
    private String rfc;
    private String birthday;
    private String street;
    private String externalNumber;
    private String internalNumber;
    private String colony;
    private String delegation;
    private String state;
    private String zipCode;
    private String residentYears;
    private FinancingGenericDTO livingType;
    private String netIncomeVerified;
    private FinancingGenericDTO incomeProfile;
    private String positionAge;
    private String showBuro;
    private FinancingGenericDTO automotiveCarUseType;
    
    
    @JsonProperty("id_financing")
    public String getIdFinancing() {
        return idFinancing;
    }
    public void setIdFinancing(String idFinancing) {
        this.idFinancing = idFinancing;
    }
    
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    @JsonProperty("authenticatio_token")
    public String getAuthenticationToken() {
        return authenticationToken;
    }
    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }
    
    @JsonProperty("full_name")
    public String getFullName() {
        return fullName;
    }
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    @JsonProperty("first_surname")
    public String getFirstSurname() {
        return firstSurname;
    }
    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }
    
    @JsonProperty("second_surname")
    public String getSecondSurname() {
        return secondSurname;
    }
    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }
    
    @JsonProperty("gender")
    public FinancingGenericDTO getGender() {
        return gender;
    }
    public void setGender(FinancingGenericDTO gender) {
        this.gender = gender;
    }
    
    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    @JsonProperty("rfc")
    public String getRfc() {
        return rfc;
    }
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
    
    @JsonProperty("birthday")
    public String getBirthday() {
        return birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    
    @JsonProperty("street")
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    
    @JsonProperty("external_number")
    public String getExternalNumber() {
        return externalNumber;
    }
    public void setExternalNumber(String externalNumber) {
        this.externalNumber = externalNumber;
    }
    
    @JsonProperty("internal_number")
    public String getInternalNumber() {
        return internalNumber;
    }
    public void setInternalNumber(String internalNumber) {
        this.internalNumber = internalNumber;
    }
    
    @JsonProperty("colony")
    public String getColony() {
        return colony;
    }
    public void setColony(String colony) {
        this.colony = colony;
    }
    
    @JsonProperty("delegation")
    public String getDelegation() {
        return delegation;
    }
    public void setDelegation(String delegation) {
        this.delegation = delegation;
    }
    
    @JsonProperty("state")
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    
    @JsonProperty("zip_code")
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    
    @JsonProperty("resident_years")
    public String getResidentYears() {
        return residentYears;
    }
    public void setResidentYears(String residentYears) {
        this.residentYears = residentYears;
    }
    
    @JsonProperty("living_type")
    public FinancingGenericDTO getLivingType() {
        return livingType;
    }
    public void setLivingType(FinancingGenericDTO livingType) {
        this.livingType = livingType;
    }
    
    @JsonProperty("net_income_verified")
    public String getNetIncomeVerified() {
        return netIncomeVerified;
    }
    public void setNetIncomeVerified(String netIncomeVerified) {
        this.netIncomeVerified = netIncomeVerified;
    }
    
    @JsonProperty("income_profile")
    public FinancingGenericDTO getIncomeProfile() {
        return incomeProfile;
    }
    public void setIncomeProfile(FinancingGenericDTO incomeProfile) {
        this.incomeProfile = incomeProfile;
    }
    
    @JsonProperty("position_age")
    public String getPositionAge() {
        return positionAge;
    }
    public void setPositionAge(String positionAge) {
        this.positionAge = positionAge;
    }
    
    @JsonProperty("show_buro")
    public String getShowBuro() {
        return showBuro;
    }
    public void setShowBuro(String showBuro) {
        this.showBuro = showBuro;
    }

    
    @JsonProperty("automotive_car_use_type")
    public FinancingGenericDTO getAutomotiveCarUseType() {
        return automotiveCarUseType;
    }
    public void setAutomotiveCarUseType(FinancingGenericDTO automotiveCarUseType) {
        this.automotiveCarUseType = automotiveCarUseType;
    }









	

}