package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.enumeration.MessageEnum;

public class MessageDTO {

	private MessageEnum code;
    private Integer errorCode;
	private String apiMessage;
	private String userMessage;
	private String basicRequirements;

    @JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("code")
	public MessageEnum getCode() {
		return code;
	}
	public void setCode(MessageEnum code) {
		this.code = code;
	}

    @JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("error_code")
	public Integer getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("api_message")
	public String getApiMessage() {
		return apiMessage;
	}
	public void setApiMessage(String apiMessage) {
		this.apiMessage = apiMessage;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("user_message")
	public String getUserMessage() {
		return userMessage;
	}
	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("basic_requirements")
	public String getBasicRequirements() {
		return basicRequirements;
	}
	public void setBasicRequirements(String basicRequirements) {
		this.basicRequirements = basicRequirements;
	}
}