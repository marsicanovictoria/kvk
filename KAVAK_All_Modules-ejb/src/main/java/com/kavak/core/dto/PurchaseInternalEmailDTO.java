package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurchaseInternalEmailDTO {

	private Long stage;
	private Long minervaId;
	private Long netsuiteOpOpportunityId;
	private String netsuiteOpprotunityUrl;
	private String customerName;
	private String customerEmail;
	private String customerPhone;
	private Long carYear;
	private String carMake;
	private String carModel;
	private String carFullVersion;
	private String customerZipCode;
	private String thirtyDaysOfferMax;
	private String thirtyDaysOfferMin;
	private String instantOfferMax;
	private String instantOfferMin;
	private String consigmentOfferMax;
	private String consigmentOfferMin;
	private boolean acceptedOffer;
	private Long acceptedOfferType;
	private String bccEmail;
	private String currentStatus;
	private String pendingStatus;
	private String appoinmentSchedule;
	private String addressAppointmentSchedule;
	private String addressAppointmentScheduleType;
	private String circulationCardUrl;
	private String carInvoiceUrl;
	private String isWishlist;
	private Long OfferQuantity;
	private String segmentType;
	
	@JsonProperty("stage")
	public Long getStage() {
		return stage;
	}
	public void setStage(Long stage) {
		this.stage = stage;
	}
	
	@JsonProperty("minerva_id")
	public Long getMinervaId() {
		return minervaId;
	}
	public void setMinervaId(Long minervaId) {
		this.minervaId = minervaId;
	}
	@JsonProperty("netsuite_opportunity_id")
	public Long getNetsuiteOpOpportunityId() {
		return netsuiteOpOpportunityId;
	}
	public void setNetsuiteOpOpportunityId(Long netsuiteOpOpportunityId) {
		this.netsuiteOpOpportunityId = netsuiteOpOpportunityId;
	}
	
	@JsonProperty("netsuite_opportunity_url")
	public String getNetsuiteOpprotunityUrl() {
		return netsuiteOpprotunityUrl;
	}
	public void setNetsuiteOpprotunityUrl(String netsuiteOpprotunityUrl) {
		this.netsuiteOpprotunityUrl = netsuiteOpprotunityUrl;
	}
	@JsonProperty("customer_name")	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@JsonProperty("customer_email")	
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	@JsonProperty("customer_phone")	
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	
	@JsonProperty("car_year")	
	public Long getCarYear() {
		return carYear;
	}
	public void setCarYear(Long carYear) {
		this.carYear = carYear;
	}
	
	@JsonProperty("car_make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}
	
	@JsonProperty("car_model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	
	@JsonProperty("car_version")
	public String getCarFullVersion() {
		return carFullVersion;
	}
	public void setCarFullVersion(String carFullVersion) {
		this.carFullVersion = carFullVersion;
	}
	@JsonProperty("customer_zipcode")	
	public String getCustomerZipCode() {
		return customerZipCode;
	}
	public void setCustomerZipCode(String customerZipCode) {
		this.customerZipCode = customerZipCode;
	}
	
	@JsonProperty("thirty_days_offer_max")	
	public String getThirtyDaysOfferMax() {
		return thirtyDaysOfferMax;
	}
	public void setThirtyDaysOfferMax(String thirtyDaysOfferMax) {
		this.thirtyDaysOfferMax = thirtyDaysOfferMax;
	}
	
	@JsonProperty("thirty_days_offer_min")
	public String getThirtyDaysOfferMin() {
		return thirtyDaysOfferMin;
	}
	public void setThirtyDaysOfferMin(String thirtyDaysOfferMin) {
		this.thirtyDaysOfferMin = thirtyDaysOfferMin;
	}
	
	@JsonProperty("instant_offer_max")
	public String getInstantOfferMax() {
		return instantOfferMax;
	}
	public void setInstantOfferMax(String instantOfferMax) {
		this.instantOfferMax = instantOfferMax;
	}
	
	@JsonProperty("instant_offer_min")
	public String getInstantOfferMin() {
		return instantOfferMin;
	}
	public void setInstantOfferMin(String instantOfferMin) {
		this.instantOfferMin = instantOfferMin;
	}
	
	@JsonProperty("consignment_offer_max")
	public String getConsigmentOfferMax() {
		return consigmentOfferMax;
	}
	public void setConsigmentOfferMax(String consigmentOfferMax) {
		this.consigmentOfferMax = consigmentOfferMax;
	}
	
	@JsonProperty("consignment_offer_min")
	public String getConsigmentOfferMin() {
		return consigmentOfferMin;
	}
	public void setConsigmentOfferMin(String consigmentOfferMin) {
		this.consigmentOfferMin = consigmentOfferMin;
	}
	
	@JsonProperty("is_accepted_offer")
	public boolean isAcceptedOffer() {
		return acceptedOffer;
	}
	public void setAcceptedOffer(boolean acceptedOffer) {
		this.acceptedOffer = acceptedOffer;
	}
	
	@JsonProperty("accepted_offer_type")
	public Long getAcceptedOfferType() {
		return acceptedOfferType;
	}
	public void setAcceptedOfferType(Long acceptedOfferType) {
		this.acceptedOfferType = acceptedOfferType;
	}
	
	@JsonProperty("bcc")
	public String getBccEmail() {
		return bccEmail;
	}
	public void setBccEmail(String bccEmail) {
		this.bccEmail = bccEmail;
	}
	
	@JsonProperty("current_status_description")
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	@JsonProperty("pending_status_description")
	public String getPendingStatus() {
		return pendingStatus;
	}
	public void setPendingStatus(String pendingStatus) {
		this.pendingStatus = pendingStatus;
	}
	
	@JsonProperty("appointment_status")
	public String getAppoinmentSchedule() {
		return appoinmentSchedule;
	}
	public void setAppoinmentSchedule(String appoinmentSchedule) {
		this.appoinmentSchedule = appoinmentSchedule;
	}
	
	@JsonProperty("address_appointment_schedule")
	public String getAddressAppointmentSchedule() {
		return addressAppointmentSchedule;
	}
	public void setAddressAppointmentSchedule(String addressAppointmentSchedule) {
		this.addressAppointmentSchedule = addressAppointmentSchedule;
	}
	
	@JsonProperty("address_appointment_schedule_type")
	public String getAddressAppointmentScheduleType() {
		return addressAppointmentScheduleType;
	}
	public void setAddressAppointmentScheduleType(String addressAppointmentScheduleType) {
		this.addressAppointmentScheduleType = addressAppointmentScheduleType;
	}
	
	@JsonProperty("circulation_card_url")
	public String getCirculationCardUrl() {
		return circulationCardUrl;
	}
	public void setCirculationCardUrl(String circulationCardUrl) {
		this.circulationCardUrl = circulationCardUrl;
	}
	
	@JsonProperty("car_invoice_url")
	public String getCarInvoiceUrl() {
		return carInvoiceUrl;
	}
	public void setCarInvoiceUrl(String carInvoiceUrl) {
		this.carInvoiceUrl = carInvoiceUrl;
	}
	
	@JsonProperty("is_wishlist")
	public String getIsWishlist() {
		return isWishlist;
	}
	public void setIsWishlist(String isWishlist) {
		this.isWishlist = isWishlist;
	}
	
	@JsonProperty("offer_quantity")
	public Long getOfferQuantity() {
		return OfferQuantity;
	}
	public void setOfferQuantity(Long offerQuantity) {
		OfferQuantity = offerQuantity;
	}
	
	@JsonProperty("segment_type")
	public String getSegmentType() {
		return segmentType;
	}
	public void setSegmentType(String segmentType) {
		this.segmentType = segmentType;
	}	
}
