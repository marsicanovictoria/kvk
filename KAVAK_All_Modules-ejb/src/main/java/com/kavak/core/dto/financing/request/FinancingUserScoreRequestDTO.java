package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserScoreRequestDTO {

//	private String automotiveCarPrice;
//	private String automotiveCarManufacturer;
//	private String automotiveCarModel;
//	private String automotiveCarVersion;
//	private String automotiveCarVersionYear;
	private Long carId;

	@JsonProperty("stock_id")
	public Long getCarId() {
	    return carId;
	}

	public void setCarId(Long carId) {
	    this.carId = carId;
	}


//	@JsonProperty("automotive_car_price")
//	public String getAutomotiveCarPrice() {
//	    return automotiveCarPrice;
//	}
//	public void setAutomotiveCarPrice(String automotiveCarPrice) {
//	    this.automotiveCarPrice = automotiveCarPrice;
//	}
//	
//	@JsonProperty("automotive_car_manufacturer")
//	public String getAutomotiveCarManufacturer() {
//	    return automotiveCarManufacturer;
//	}
//	public void setAutomotiveCarManufacturer(String automotiveCarManufacturer) {
//	    this.automotiveCarManufacturer = automotiveCarManufacturer;
//	}
//	
//	@JsonProperty("automotive_car_model")
//	public String getAutomotiveCarModel() {
//	    return automotiveCarModel;
//	}
//	public void setAutomotiveCarModel(String automotiveCarModel) {
//	    this.automotiveCarModel = automotiveCarModel;
//	}
//	
//	@JsonProperty("automotive_car_version")
//	public String getAutomotiveCarVersion() {
//	    return automotiveCarVersion;
//	}
//	public void setAutomotiveCarVersion(String automotiveCarVersion) {
//	    this.automotiveCarVersion = automotiveCarVersion;
//	}
//	
//	@JsonProperty("automotive_car_version_year")
//	public String getAutomotiveCarVersionYear() {
//	    return automotiveCarVersionYear;
//	}
//	public void setAutomotiveCarVersionYear(String automotiveCarVersionYear) {
//	    this.automotiveCarVersionYear = automotiveCarVersionYear;
//	}



}