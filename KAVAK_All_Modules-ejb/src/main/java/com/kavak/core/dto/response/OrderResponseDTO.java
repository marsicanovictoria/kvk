package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderResponseDTO {
	
	private String id;
	private String itemId;
	private String transactionId;
	private String paidAmt;
	private String itemName;
	private String pendingAmt;
	

	@JsonProperty("id")
	public String getId() {
	    return id;
	}
	public void setId(String id) {
	    this.id = id;
	}
	

	@JsonProperty("item_id")
	public String getItemId() {
	    return itemId;
	}
	public void setItemId(String itemId) {
	    this.itemId = itemId;
	}
	
	@JsonProperty("transaction_id")
	public String getTransactionId() {
	    return transactionId;
	}
	public void setTransactionId(String transactionId) {
	    this.transactionId = transactionId;
	}
	
	@JsonProperty("paid_amt")
	public String getPaidAmt() {
	    return paidAmt;
	}
	public void setPaidAmt(String paidAmt) {
	    this.paidAmt = paidAmt;
	}
	
	@JsonProperty("item_name")
	public String getItemName() {
	    return itemName;
	}
	public void setItemName(String itemName) {
	    this.itemName = itemName;
	}
	
	@JsonProperty("pending_amt")
	public String getPendingAmt() {
	    return pendingAmt;
	}
	public void setPendingAmt(String pendingAmt) {
	    this.pendingAmt = pendingAmt;
	}

}
