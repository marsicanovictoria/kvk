package com.kavak.core.dto.financing.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserPinCodeApiDataResponseDTO {

	private String data;
	private FinancingUserPinCodeApiMetaResponseDTO meta;
	private List<FinancingPinCodeErrorResponseDTO> errors;
	

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("data")
	public String getData() {
	    return data;
	}
	public void setData(String data) {
	    this.data = data;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("meta")
	public FinancingUserPinCodeApiMetaResponseDTO getMeta() {
	    return meta;
	}
	public void setMeta(FinancingUserPinCodeApiMetaResponseDTO meta) {
	    this.meta = meta;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("errors")
	public List<FinancingPinCodeErrorResponseDTO> getErrors() {
	    return errors;
	}
	public void setErrors(List<FinancingPinCodeErrorResponseDTO> errors) {
	    this.errors = errors;
	}

}