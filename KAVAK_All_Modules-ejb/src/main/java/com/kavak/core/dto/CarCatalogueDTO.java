package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarCatalogueDTO {

    private String id;
    private String year;
    private String price;
    private String name;

    @JsonProperty("id")
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("year")
    public String getYear() {
        return year;
    }
    public void setYear(String year) {
        this.year = year;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
