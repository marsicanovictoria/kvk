package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ColonyNotServedDTO {

    private Long id;
    private Long zipCode;
    private String name;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @JsonProperty("zip_code")
    public Long getZipCode() {
        return zipCode;
    }

    public void setZipCode(Long zipCode) {
        this.zipCode = zipCode;
    }
    @JsonProperty("name_colony")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
