package com.kavak.core.dto.financing;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserDomicileDTO {
	
    
    private Long id;
    private Long userId;
    private Long financingId;
    private String street;
    private String externalNumber;
    private String colony;
    private String delegation;
    private String state;
    private String zipCode;
    private String residentYears;
    private String livingType;
    private Timestamp creationDate;
    private boolean active; 


    @JsonProperty("id")
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @JsonProperty("user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("external_number")
    public String getExternalNumber() {
        return externalNumber;
    }

    public void setExternalNumber(String externalNumber) {
        this.externalNumber = externalNumber;
    }

    @JsonProperty("colony")
    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    @JsonProperty("delegation")
    public String getDelegation() {
        return delegation;
    }

    public void setDelegation(String delegation) {
        this.delegation = delegation;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("zip_code")
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @JsonProperty("resident_years")
    public String getResidentYears() {
        return residentYears;
    }

    public void setResidentYears(String residentYears) {
        this.residentYears = residentYears;
    }

    @JsonProperty("living_type")
    public String getLivingType() {
        return livingType;
    }

    public void setLivingType(String livingType) {
        this.livingType = livingType;
    }


    @JsonProperty("is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @JsonProperty("creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @JsonProperty("financing_id")
    public Long getFinancingId() {
        return financingId;
    }

    public void setFinancingId(Long financingId) {
        this.financingId = financingId;
    }







}