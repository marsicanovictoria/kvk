package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostMessageRequestDTO {

    private String phone;
    private String message;
    private String email;
    private String scheduleDate;

//    private String csvName;
//    private String type;
//    private String subType;
//    private String starDate;
//    private String sendDate;

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("schedule_date")
    public String getScheduleDate() {
        return scheduleDate;
    }
    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

//    @JsonProperty("cv_name")
//    public String getCsvName() {
//        return csvName;
//    }
//    public void setCsvName(String csvName) {
//        this.csvName = csvName;
//    }
//
//    @JsonProperty("type")
//    public String getType() {
//        return type;
//    }
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    @JsonProperty("sub_type")
//    public String getSubType() {
//        return subType;
//    }
//    public void setSubType(String subType) {
//        this.subType = subType;
//    }
//
//    @JsonProperty("start_date")
//    public String getStarDate() {
//        return starDate;
//    }
//    public void setStarDate(String starDate) {
//        this.starDate = starDate;
//    }
//
//    @JsonProperty("send_date")
//    public String getSendDate() {
//        return sendDate;
//    }
//    public void setSendDate(String sendDate) {
//        this.sendDate = sendDate;
//    }

}
