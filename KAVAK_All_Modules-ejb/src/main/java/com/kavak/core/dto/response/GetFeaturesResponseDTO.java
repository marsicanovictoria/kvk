package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.CarFeatureCategoryDTO;

import java.util.List;

public class GetFeaturesResponseDTO {

	private List<CarFeatureCategoryDTO> listCarFeatureCategoryDTO;

	@JsonProperty("categories")
	public List<CarFeatureCategoryDTO> getListCarFeatureCategoryDTO() {
		return listCarFeatureCategoryDTO;
	}

	public void setListCarFeatureCategoryDTO(List<CarFeatureCategoryDTO> listCarFeatureCategoryDTO) {
		this.listCarFeatureCategoryDTO = listCarFeatureCategoryDTO;
	}
}
