package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BillingAddressDTO {

    private String street;
    private String exteriorNumber;
    private String interiorNumber;
    private String zipCode;

    @JsonProperty("street")
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("exterior_numer")
    public String getExteriorNumber() {
        return exteriorNumber;
    }
    public void setExteriorNumber(String exteriorNumber) {
        this.exteriorNumber = exteriorNumber;
    }

    @JsonProperty("interior_number")
    public String getInteriorNumber() {
        return interiorNumber;
    }
    public void setInteriorNumber(String interiorNumber) {
        this.interiorNumber = interiorNumber;
    }

    @JsonProperty("zip_code")
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

}
