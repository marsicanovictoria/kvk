package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostInspectionScheduleResponseDTO {

	private Long id;

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}