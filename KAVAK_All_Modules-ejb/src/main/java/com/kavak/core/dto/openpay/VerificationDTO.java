package com.kavak.core.dto.openpay;

import java.io.Serializable;

public class VerificationDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long idVerification;
	private String type;
	private String eventDate;
	private String verificationCode;
	
	public Long getIdVerification() {
		return idVerification;
	}
	public void setIdVerification(Long idVerification) {
		this.idVerification = idVerification;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
}

