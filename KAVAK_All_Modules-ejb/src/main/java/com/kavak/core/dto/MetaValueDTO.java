package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetaValueDTO {
	
	private Long id;
	private String groupCategory;
	private String groupName;
	private String optionName;
	private String alias;
	private boolean active;
	private OfferCheckpointDTO inspectionOverviewDTO;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("group_category")
	public String getGroupCategory() {
		return groupCategory;
	}
	public void setGroupCategory(String groupCategory) {
		this.groupCategory = groupCategory;
	}
	@JsonProperty("group_name")
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	@JsonProperty("option_name")
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	@JsonProperty("alias")
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	@JsonProperty("active")
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@JsonProperty("inspection_overview")
	public OfferCheckpointDTO getInspectionOverviewDTO() {
		return inspectionOverviewDTO;
	}
	public void setInspectionOverviewDTO(OfferCheckpointDTO inspectionOverviewDTO) {
		this.inspectionOverviewDTO = inspectionOverviewDTO;
	}

}