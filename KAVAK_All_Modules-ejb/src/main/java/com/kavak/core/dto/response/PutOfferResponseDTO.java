package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;

import java.util.List;

public class PutOfferResponseDTO {
	
	List<GenericDTO> listDataResumen;
	List<GenericDTO> listDataUser;
	List<GenericDTO> listIndications;
	
	@JsonProperty("resumen")
	public List<GenericDTO> getListDataResumen() {
		return listDataResumen;
	}
	public void setListDataResumen(List<GenericDTO> listDataResumen) {
		this.listDataResumen = listDataResumen;
	}

	@JsonProperty("user")
	public List<GenericDTO> getListDataUser() {
		return listDataUser;
	}
	public void setListDataUser(List<GenericDTO> listDataUser) {
		this.listDataUser = listDataUser;
	}

	@JsonProperty("indications")
	public List<GenericDTO> getListIndications() {
		return listIndications;
	}
	public void setListIndications(List<GenericDTO> listIndications) {
		this.listIndications = listIndications;
	}

}

