package com.kavak.core.dto.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.InspectionDiscountDTO;
import com.kavak.core.dto.InspectionRepairDTO;
import com.kavak.core.dto.InspectionSummaryDTO;

public class InspectionNetsuiteDataResponseDTO {

	private Long id;
	private Long minervaOfferId;
	private String creationDate;
	//private Boolean approvedInspection;
	private String inpectorComments;
	private boolean isCanceled;
	private String isCanceledReason;
	private boolean isRescheduled;
	private String isRescheduledReason;
	private List<InspectionSummaryDTO> inspectionSummaryList;
	private List<InspectionRepairDTO> inspectionRepairList;
	private List<InspectionDiscountDTO> inspectionDiscountList;
	
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("minerva_offer_id")
	public Long getMinervaOfferId() {
		return minervaOfferId;
	}
	public void setMinervaOfferId(Long minervaOfferId) {
		this.minervaOfferId = minervaOfferId;
	}

	@JsonProperty("creation_date")
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	/*
	@JsonProperty("approved_inspection")
	public Boolean getApprovedInspection() {
		return approvedInspection;
	}
	public void setApprovedInspection(Boolean approvedInspection) {
		this.approvedInspection = approvedInspection;
	}*/
	
	@JsonProperty("inspector_comments")
	public String getInpectorComments() {
		return inpectorComments;
	}
	
	public void setInpectorComments(String inpectorComments) {
		this.inpectorComments = inpectorComments;
	}
	
	@JsonProperty("is_canceled")
	public boolean isCanceled() {
		return isCanceled;
	}
	
	public void setCanceled(boolean isCanceled) {
		this.isCanceled = isCanceled;
	}
	
	@JsonProperty("is_canceled_reason")
	public String getIsCanceledReason() {
		return isCanceledReason;
	}
	
	public void setIsCanceledReason(String isCanceledReason) {
		this.isCanceledReason = isCanceledReason;
	}
	
	@JsonProperty("is_rescheduled")
	public boolean isRescheduled() {
		return isRescheduled;
	}
	
	public void setRescheduled(boolean isRescheduled) {
		this.isRescheduled = isRescheduled;
	}
	
	@JsonProperty("is_rescheduled_reason")
	public String getIsRescheduledReason() {
		return isRescheduledReason;
	}
	
	public void setIsRescheduledReason(String isRescheduledReason) {
		this.isRescheduledReason = isRescheduledReason;
	}
	
	@JsonProperty("inspection_summary")
	public List<InspectionSummaryDTO> getInspectionSummaryList() {
		return inspectionSummaryList;
	}
	
	public void setInspectionSummaryList(List<InspectionSummaryDTO> inspectionSummaryList) {
		this.inspectionSummaryList = inspectionSummaryList;
	}
	
	@JsonProperty("inspection_repair_details")
	public List<InspectionRepairDTO> getInspectionRepairList() {
		return inspectionRepairList;
	}
	
	public void setInspectionRepairList(List<InspectionRepairDTO> inspectionRepairList) {
		this.inspectionRepairList = inspectionRepairList;
	}
	
	@JsonProperty("inspection_discount_details")
	public List<InspectionDiscountDTO> getInspectionDiscountList() {
		return inspectionDiscountList;
	}
	public void setInspectionDiscountList(List<InspectionDiscountDTO> inspectionDiscountList) {
		this.inspectionDiscountList = inspectionDiscountList;
	}
	
}
