package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.*;

/**
 * Clase DTO para procesar la data de Un Registro de Financiamiento de un
 * cliente.
 * 
 * @author Oscar Montilla
 *
 */
public class RegisterFinancingRequestDTO {

	private MetaValueDomicileDTO metaValueDomicileDTO;
	private ClientDTO clientDTO;
	private MetaValueReferenceDTO metaValueReferenceDTO;
	private JobDTO jobDTO;
	private PreviousJobDTO previousJobDTO;
	private AdditionalDTO additionalDTO;

	@JsonProperty("domicile")
	public MetaValueDomicileDTO getMetaValueDomicileDTO() {
		return metaValueDomicileDTO;
	}

	public void setMetaValueDomicileDTO(MetaValueDomicileDTO metaValueDomicileDTO) {
		this.metaValueDomicileDTO = metaValueDomicileDTO;
	}

	@JsonProperty("client")
	public ClientDTO getClientDTO() {
		return clientDTO;
	}

	public void setClientDTO(ClientDTO clientDTO) {
		this.clientDTO = clientDTO;
	}

	@JsonProperty("reference")
	public MetaValueReferenceDTO getMetaValueReferenceDTO() {
		return metaValueReferenceDTO;
	}

	public void setMetaValueReferenceDTO(MetaValueReferenceDTO metaValueReferenceDTO) {
		this.metaValueReferenceDTO = metaValueReferenceDTO;
	}

	@JsonProperty("job")
	public JobDTO getJobDTO() {
		return jobDTO;
	}

	public void setJobDTO(JobDTO jobDTO) {
		this.jobDTO = jobDTO;
	}

	@JsonProperty("previous_job")
	public PreviousJobDTO getPreviousJobDTO() {
		return previousJobDTO;
	}

	public void setPreviousJobDTO(PreviousJobDTO previousJobDTO) {
		this.previousJobDTO = previousJobDTO;
	}

	@JsonProperty("additional")
	public AdditionalDTO getAdditionalDTO() {
		return additionalDTO;
	}

	public void setAdditionalDTO(AdditionalDTO additionalDTO) {
		this.additionalDTO = additionalDTO;
	}

}
