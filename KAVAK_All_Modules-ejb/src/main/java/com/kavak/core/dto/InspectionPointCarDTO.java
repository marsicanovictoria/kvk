package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InspectionPointCarDTO {

    private Long id;
    private Long idInspection;
    private Long idInspectioPointItem;
    private String inspectorEmail;
    private Long idInspectionPointResult;
    private String additionalInfoDetail;
    private Long inspectionTypeId;
    private List<InspectionRepairDTO> listInspectionRepairDTO;
    private List<InspectionDimpleDTO> listInspectionDimpleDTO;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("inspection_id")
    public Long getIdInspection() {
        return idInspection;
    }
    public void setIdInspection(Long idInspection) {
        this.idInspection = idInspection;
    }

    @JsonProperty("inspection_point_item_id")
    public Long getIdInspectioPointItem() {
        return idInspectioPointItem;
    }
    public void setIdInspectioPointItem(Long idInspectioPointItem) {
        this.idInspectioPointItem = idInspectioPointItem;
    }

    @JsonProperty("inspector_email_id")
    public String getInspectorEmail() {
        return inspectorEmail;
    }
    public void setInspectorEmail(String inspectorEmail) {
        this.inspectorEmail = inspectorEmail;
    }

    @JsonProperty("inspection_point_result")
    public Long getIdInspectionPointResult() {
        return idInspectionPointResult;
    }
    public void setIdInspectionPointResult(Long idInspectionPointResult) {
        this.idInspectionPointResult = idInspectionPointResult;
    }

    @JsonProperty("additional_info_detail")
    public String getAdditionalInfoDetail() {
        return additionalInfoDetail;
    }
    public void setAdditionalInfoDetail(String additionalInfoDetail) {
        this.additionalInfoDetail = additionalInfoDetail;
    }

    @JsonProperty("inspection_type_id")
    public Long getInspectionTypeId() {
        return inspectionTypeId;
    }
    public void setInspectionTypeId(Long inspectionTypeId) {
        this.inspectionTypeId = inspectionTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("repairs")
    public List<InspectionRepairDTO> getListInspectionRepairDTO() {
        return listInspectionRepairDTO;
    }
    public void setListInspectionRepairDTO(List<InspectionRepairDTO> listInspectionRepairDTO) {
        this.listInspectionRepairDTO = listInspectionRepairDTO;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("dimples")
    public List<InspectionDimpleDTO> getListInspectionDimpleDTO() {
        return listInspectionDimpleDTO;
    }
    public void setListInspectionDimpleDTO(List<InspectionDimpleDTO> listInspectionDimpleDTO) {
        this.listInspectionDimpleDTO = listInspectionDimpleDTO;
    }
}
