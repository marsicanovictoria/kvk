package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GoogleResponseDTO {

	private String sub;
	private String name;
	private String email;
	private String familyName;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("sub")
	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("given_name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("family_name")
	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
}
