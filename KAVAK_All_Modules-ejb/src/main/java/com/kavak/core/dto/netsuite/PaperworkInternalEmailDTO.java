package com.kavak.core.dto.netsuite;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaperworkInternalEmailDTO {
	private Long id;
	private Long stockId;
	private String car;
	private Long carKm;
	private Long paperworkStatusId;
	private String paperworkStatusDescription;
	private Long customerMinervaId;
	private String customerEmail;
	private String currentPlateStatus;
	private String finalPlateStatus;
	private Long paperworkTypeId;
	private String paperworkTypeDescription;
	private String shippingAddress;
	private String shippingManager;
	private String courrier;
	private String trackingNumber;
	private String trackingNumberUrl;
	private String personWhoReceive;
	private String tentativeDeliveryDate;
	private String bccEmail;
	private String startedDate;
	private String netsuiteURL;
	private String deliveryDate;
	private String validityReturn;
	private String emailTemplate;
	private ArrayList<String> attachments;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("stock_id")
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	@JsonProperty("car_name")
	public String getCar() {
		return car;
	}
	public void setCar(String car) {
		this.car = car;
	}
	
	@JsonProperty("car_km")
	public Long getCarKm() {
		return carKm;
	}
	public void setCarKm(Long carKm) {
		this.carKm = carKm;
	}
	
	@JsonProperty("paperwork_status_id")
	public Long getPaperworkStatusId() {
		return paperworkStatusId;
	}
	public void setPaperworkStatusId(Long paperworkStatusId) {
		this.paperworkStatusId = paperworkStatusId;
	}
	
	@JsonProperty("paperwork_status_description")
	public String getPaperworkStatusDescription() {
		return paperworkStatusDescription;
	}
	public void setPaperworkStatusDescription(String paperworkStatusDescription) {
		this.paperworkStatusDescription = paperworkStatusDescription;
	}
	
	@JsonProperty("customer_minerva_id")
	public Long getCustomerMinervaId() {
		return customerMinervaId;
	}
	public void setCustomerMinervaId(Long customerMinervaId) {
		this.customerMinervaId = customerMinervaId;
	}
	
	@JsonProperty("customer_email")
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	@JsonProperty("current_plate_status")
	public String getCurrentPlateStatus() {
		return currentPlateStatus;
	}
	public void setCurrentPlateStatus(String currentPlateStatus) {
		this.currentPlateStatus = currentPlateStatus;
	}
	
	@JsonProperty("final_plate_status")
	public String getFinalPlateStatus() {
		return finalPlateStatus;
	}
	public void setFinalPlateStatus(String finalPlateStatus) {
		this.finalPlateStatus = finalPlateStatus;
	}
	
	@JsonProperty("paperwork_type_id")
	public Long getPaperworkTypeId() {
		return paperworkTypeId;
	}
	public void setPaperworkTypeId(Long paperworkTypeId) {
		this.paperworkTypeId = paperworkTypeId;
	}
	
	@JsonProperty("paperwork_type_description")
	public String getPaperworkTypeDescription() {
		return paperworkTypeDescription;
	}
	public void setPaperworkTypeDescription(String paperworkTypeDescription) {
		this.paperworkTypeDescription = paperworkTypeDescription;
	}
	
	@JsonProperty("shipping_address")
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	
	@JsonProperty("manager_shipping")
	public String getShippingManager() {
		return shippingManager;
	}
	public void setShippingManager(String shippingManager) {
		this.shippingManager = shippingManager;
	}
	
	@JsonProperty("courrier")
	public String getCourrier() {
		return courrier;
	}
	public void setCourrier(String courrier) {
		this.courrier = courrier;
	}
	
	@JsonProperty("tracking_number")
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	
	@JsonProperty("tracking_number_url")
	public String getTrackingNumberUrl() {
		return trackingNumberUrl;
	}
	public void setTrackingNumberUrl(String trackingNumberUrl) {
		this.trackingNumberUrl = trackingNumberUrl;
	}
	@JsonProperty("person_who_receive")
	public String getPersonWhoReceive() {
		return personWhoReceive;
	}
	public void setPersonWhoReceive(String personWhoReceive) {
		this.personWhoReceive = personWhoReceive;
	}
	@JsonProperty("tentative_delivery_date")
	public String getTentativeDeliveryDate() {
		return tentativeDeliveryDate;
	}
	public void setTentativeDeliveryDate(String tentativeDeliveryDate) {
		this.tentativeDeliveryDate = tentativeDeliveryDate;
	}
	
	@JsonProperty("bcc_email")
	public String getBccEmail() {
		return bccEmail;
	}
	public void setBccEmail(String bccEmail) {
		this.bccEmail = bccEmail;
	}
	
	@JsonProperty("started_date")
	public String getStartedDate() {
		return startedDate;
	}
	public void setStartedDate(String startedDate) {
		this.startedDate = startedDate;
	}
	
	@JsonProperty("netsuite_url")
	public String getNetsuiteURL() {
		return netsuiteURL;
	}
	public void setNetsuiteUrl(String netsuiteURL) {
		this.netsuiteURL = netsuiteURL;
	}
	
	@JsonProperty("delivery_date")
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	@JsonProperty("validity_return")
	public String getValidityReturn() {
		return validityReturn;
	}
	public void setValidityReturn(String validityReturn) {
		this.validityReturn = validityReturn;
	}
	
	@JsonProperty("email_template")
	public String getEmailTemplate() {
		return emailTemplate;
	}
	public void setEmailTemplate(String emailTemplate) {
		this.emailTemplate = emailTemplate;
	}
	@JsonProperty("attachments")
	public ArrayList<String> getAttachments() {
		return attachments;
	}
	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}
}
