package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InspectionCarPhotoTypeDTO {
	
	private Long id;
	private String name;
	private String image_reference;
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("image_reference")
	public String getImage_reference() {
		return image_reference;
	}

	public void setImage_reference(String image_reference) {
		this.image_reference = image_reference;
	}
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
