package com.kavak.core.dto.netsuite;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PaperworkShippingManagerDTO {
	private Long id;
	private Long stockId;
	private String car;
	private Long paperworkTypeId;
	private String paperworkTypeDescription;
	private String currentPlateStatus;
	private String finalPlateStatus;
	private String startedDate;
	private String shippingManagerName;
	private String shippingManagerEmail;
	private String emailTemplate;
	private String bccEmail;
	private ArrayList<String> attachments;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("stock_id")
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	
	@JsonProperty("car_name")
	public String getCar() {
		return car;
	}
	public void setCar(String car) {
		this.car = car;
	}
	
	@JsonProperty("paperwork_type_id")
	public Long getPaperworkTypeId() {
		return paperworkTypeId;
	}
	public void setPaperworkTypeId(Long paperworkTypeId) {
		this.paperworkTypeId = paperworkTypeId;
	}
	
	@JsonProperty("paperwork_type_description")
	public String getPaperworkTypeDescription() {
		return paperworkTypeDescription;
	}
	public void setPaperworkTypeDescription(String paperworkTypeDescription) {
		this.paperworkTypeDescription = paperworkTypeDescription;
	}
	
	@JsonProperty("current_plate_status")
	public String getCurrentPlateStatus() {
		return currentPlateStatus;
	}
	public void setCurrentPlateStatus(String currentPlateStatus) {
		this.currentPlateStatus = currentPlateStatus;
	}
	
	@JsonProperty("final_plate_status")
	public String getFinalPlateStatus() {
		return finalPlateStatus;
	}
	public void setFinalPlateStatus(String finalPlateStatus) {
		this.finalPlateStatus = finalPlateStatus;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("started_date")
	public String getStartedDate() {
		return startedDate;
	}
	public void setStartedDate(String startedDate) {
		this.startedDate = startedDate;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("shipping_manager_name")
	public String getShippingManagerName() {
		return shippingManagerName;
	}
	public void setShippingManagerName(String shippingManagerName) {
		this.shippingManagerName = shippingManagerName;
	}
	
	@JsonProperty("shipping_manager_email")
	public String getShippingManagerEmail() {
		return shippingManagerEmail;
	}
	public void setShippingManagerEmail(String shippingManagerEmail) {
		this.shippingManagerEmail = shippingManagerEmail;
	}
	
	@JsonProperty("email_template")
	public String getEmailTemplate() {
		return emailTemplate;
	}
	public void setEmailTemplate(String emailTemplate) {
		this.emailTemplate = emailTemplate;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("bcc_email")
	public String getBccEmail() {
		return bccEmail;
	}
	public void setBccEmail(String bccEmail) {
		this.bccEmail = bccEmail;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("attachments")
	public ArrayList<String> getAttachments() {
		return attachments;
	}
	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}
	
}
