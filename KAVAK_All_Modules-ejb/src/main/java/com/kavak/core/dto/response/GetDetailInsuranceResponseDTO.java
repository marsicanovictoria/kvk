package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;

import java.util.List;

/**
 * Created by Enrique on 6/28/2017.
 */
public class GetDetailInsuranceResponseDTO {

    private List<String> listBenefits;
    private List<GenericDTO> listInsuranceDetailDataDTO;

    @JsonProperty("benefits")
    public List<String> getListBenefits() {
        return listBenefits;
    }
    public void setListBenefits(List<String> listBenefits) {
        this.listBenefits = listBenefits;
    }

    @JsonProperty("summary")
    public List<GenericDTO> getListInsuranceDetailDataDTO() {
        return listInsuranceDetailDataDTO;
    }
    public void setListInsuranceDetailDataDTO(List<GenericDTO> listInsuranceDetailDataDTO) {
        this.listInsuranceDetailDataDTO = listInsuranceDetailDataDTO;
    }
}
