package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Enrique on 19-Jun-17.
 */
public class CarColorDTO {

    private Long id;
    private String name;
    private String hexadecimal;
//    private boolean active;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty("hexadecimal")
    public String getHexadecimal() {
        return hexadecimal;
    }

    public void setHexadecimal(String hexadecimal) {
        this.hexadecimal = hexadecimal;
    }
//    @JsonProperty("is_active")
//    public boolean isActive() {
//        return active;
//    }
//
//    public void setActive(boolean active) {
//        this.active = active;
//    }
}
