package com.kavak.core.dto;

import java.sql.Timestamp;

import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerReviewDTO {
	private Long id;
	private Long userId;
	private Long carId;
	private String reviewType;
	private Long reviewScore;
	private String reviewContent;
	private String source;
	private Timestamp creationDate;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@JsonProperty("car_id")
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	
	@JsonProperty("review_type")
	public String getReviewType() {
		return reviewType;
	}
	public void setReviewType(String reviewType) {
		this.reviewType = reviewType;
	}
	
	@JsonProperty("review_score")
	public Long getReviewScore() {
		return reviewScore;
	}
	public void setReviewScore(Long reviewScore) {
		this.reviewScore = reviewScore;
	}
	
	@SafeHtml
	@JsonProperty("review_content")
	public String getReviewContent() {
		return reviewContent;
	}
	public void setReviewContent(String reviewContent) {
		this.reviewContent = reviewContent;
	}
	
	@JsonProperty("source")
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	@JsonProperty("creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	
}
