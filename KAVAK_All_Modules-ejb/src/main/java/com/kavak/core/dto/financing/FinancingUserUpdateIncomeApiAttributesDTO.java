package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateIncomeApiAttributesDTO {

    	private String netIncomeVerified;
    	private String incomeProfile;
    	private String positionAge;
               
	@JsonProperty("net_income_verified")
	public String getNetIncomeVerified() {
	    return netIncomeVerified;
	}
	public void setNetIncomeVerified(String netIncomeVerified) {
	    this.netIncomeVerified = netIncomeVerified;
	}
	
	@JsonProperty("income_profile")
	public String getIncomeProfile() {
	    return incomeProfile;
	}
	public void setIncomeProfile(String incomeProfile) {
	    this.incomeProfile = incomeProfile;
	}
	
	@JsonProperty("position_age")
	public String getPositionAge() {
	    return positionAge;
	}
	public void setPositionAge(String positionAge) {
	    this.positionAge = positionAge;
	}


        

	

}