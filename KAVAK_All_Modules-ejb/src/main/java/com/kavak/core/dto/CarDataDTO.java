package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarDataDTO {
	
	private Long idCarData;
	private String sku;
	private Long carYear;
	private String carMake;
	private String carModel;
	private String carTrim;
	private String doors;
	private String trasmissions;
	private String seats;
	private String rim;
	private String turbo;
	private String hp;
	private String sunroof;
	private String cylinder;
	private String traction;
	private String offer30Days;
	private String others;
	private boolean wishList;
	private String offerPunished;
	private String fullVersionCar;
	private String segmentType;
	
	@JsonProperty("id")
	public Long getIdCarData() {
		return idCarData;
	}
	public void setIdCarData(Long idCarData) {
		this.idCarData = idCarData;
	}
	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	@JsonProperty("year")
	public Long getCarYear() {
		return carYear;
	}
	public void setCarYear(Long carYear) {
		this.carYear = carYear;
	}
	@JsonProperty("make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}
	@JsonProperty("model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	@JsonProperty("trim")
	public String getCarTrim() {
		return carTrim;
	}
	public void setCarTrim(String carTrim) {
		this.carTrim = carTrim;
	}
	@JsonProperty("doors")
	public String getDoors() {
		return doors;
	}
	public void setDoors(String doors) {
		this.doors = doors;
	}
	@JsonProperty("transmision")
	public String getTrasmissions() {
		return trasmissions;
	}
	public void setTrasmissions(String trasmissions) {
		this.trasmissions = trasmissions;
	}
	@JsonProperty("seats")
	public String getSeats() {
		return seats;
	}
	public void setSeats(String seats) {
		this.seats = seats;
	}
	@JsonProperty("rim")
	public String getRim() {
		return rim;
	}
	public void setRim(String rim) {
		this.rim = rim;
	}
	@JsonProperty("turbo")
	public String getTurbo() {
		return turbo;
	}
	public void setTurbo(String turbo) {
		this.turbo = turbo;
	}
	@JsonProperty("hp")
	public String getHp() {
		return hp;
	}
	public void setHp(String hp) {
		this.hp = hp;
	}
	@JsonProperty("sunroof")
	public String getSunroof() {
		return sunroof;
	}
	public void setSunroof(String sunroof) {
		this.sunroof = sunroof;
	}
	@JsonProperty("cylinder")
	public String getCylinder() {
		return cylinder;
	}
	public void setCylinder(String cylinder) {
		this.cylinder = cylinder;
	}
	@JsonProperty("traccion")
	public String getTraction() {
		return traction;
	}
	public void setTraction(String traction) {
		this.traction = traction;
	}
	@JsonProperty("offer_30_days")
	public String getOffer30Days() {
		return offer30Days;
	}
	public void setOffer30Days(String offer30Days) {
		this.offer30Days = offer30Days;
	}
	@JsonProperty("others")
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	@JsonProperty("wish_list")
	public boolean isWishList() {
		return wishList;
	}
	public void setWishList(boolean wishList) {
		this.wishList = wishList;
	}
	@JsonProperty("oferta_castigada")
	public String getOfferPunished() {
		return offerPunished;
	}
	public void setOfferPunished(String offerPunished) {
		this.offerPunished = offerPunished;
	}
	
	@JsonProperty("full_version_car") 
	public String getFullVersionCar() {
		return fullVersionCar;
	}
	public void setFullVersionCar(String fullVersionCar) {
		this.fullVersionCar = fullVersionCar;
	}
	
	@JsonProperty("segment_type") 
	public String getSegmentType() {
		return segmentType;
	}
	public void setSegmentType(String segmentType) {
		this.segmentType = segmentType;
	}
}