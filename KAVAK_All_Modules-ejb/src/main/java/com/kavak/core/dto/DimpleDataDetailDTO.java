package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DimpleDataDetailDTO {

	private String url;
	private String detailText;
	private CoordinateDimpleDTO coordinateDimpleDTO;
	
	@JsonProperty("image_dimple_url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@JsonProperty("msg")
	public String getDetailText() {
		return detailText;
	}
	public void setDetailText(String detailText) {
		this.detailText = detailText;
	}
	@JsonProperty("position")
	public CoordinateDimpleDTO getCoordinateDimpleDTO() {
		return coordinateDimpleDTO;
	}
	public void setCoordinateDimpleDTO(CoordinateDimpleDTO coordinateDimpleDTO) {
		this.coordinateDimpleDTO = coordinateDimpleDTO;
	}
}
