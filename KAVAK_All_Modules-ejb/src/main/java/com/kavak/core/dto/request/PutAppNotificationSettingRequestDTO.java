package com.kavak.core.dto.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PutAppNotificationSettingRequestDTO {

	private Long userId;
	private Long typeId;
	private boolean isActive;
	private List<AppNotificationTypeRequestDTO> listAppNotificationTypeRequestDTO;

	
	@JsonProperty("user_id")
	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}

	@JsonProperty("type_id")
	public Long getTypeId() {
	    return typeId;
	}

	public void setTypeId(Long typeId) {
	    this.typeId = typeId;
	}

	@JsonProperty("is_active")
	public boolean isActive() {
	    return isActive;
	}

	public void setActive(boolean isActive) {
	    this.isActive = isActive;
	}

	@JsonProperty("notifications")
	public List<AppNotificationTypeRequestDTO> getListAppNotificationTypeRequestDTO() {
	    return listAppNotificationTypeRequestDTO;
	}

	public void setListAppNotificationTypeRequestDTO(List<AppNotificationTypeRequestDTO> listAppNotificationTypeRequestDTO) {
	    this.listAppNotificationTypeRequestDTO = listAppNotificationTypeRequestDTO;
	}

}
