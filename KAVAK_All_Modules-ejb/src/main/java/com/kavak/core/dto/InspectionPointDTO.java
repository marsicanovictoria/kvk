package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InspectionPointDTO {
	
	private Long id;
	private String name;
	private boolean feature;
	private Long idFeature;
	private boolean reprovesInspection;
	private boolean active;
	private String availableRepairs;
	private boolean photoRequired;
	private boolean naButtonEnabled;
	private boolean standartButtonEnabled;
	private boolean repairButtonEnabled;
	private boolean reprovedButtonEnabled;
	private boolean showAdditionalInfo;
	private String showAdditionalInfoKey;
	private boolean captureAdditionalInfo;
	private String captureAdditionalInfoKey;
	private boolean generateDiscount;
	private boolean generateSummaryComment;
	private boolean generateEmailAlert;
    private Long discountTypeId;
    private Long inspectionPointTypeId;
    private boolean mandatory;
    private List<InspectionPointCarDTO> listInspectionPointCarDTO;

    @JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("is_feature")
	public boolean isFeature() {
		return feature;
	}
	public void setFeature(boolean feature) {
		this.feature = feature;
	}

	@JsonProperty("feature_id")
	public Long getIdFeature() {
		return idFeature;
	}
	public void setIdFeature(Long idFeature) {
		this.idFeature = idFeature;
	}

	@JsonProperty("is_reproves_inspection")
	public boolean isReprovesInspection() {
		return reprovesInspection;
	}
	public void setReprovesInspection(boolean reprovesInspection) {
		this.reprovesInspection = reprovesInspection;
	}

	@JsonProperty("is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

    @JsonProperty("available_repairs")
    public String getAvailableRepairs() {
        return availableRepairs;
    }
    public void setAvailableRepairs(String availableRepairs) {
        this.availableRepairs = availableRepairs;
    }

	@JsonProperty("is_photo_required")
	public boolean isPhotoRequired() {
		return photoRequired;
	}

	public void setPhotoRequired(boolean photoRequired) {
		this.photoRequired = photoRequired;
	}

	@JsonProperty("is_na_button_enabled")
	public boolean isNaButtonEnabled() {
		return naButtonEnabled;
	}

	public void setNaButtonEnabled(boolean naButtonEnabled) {
		this.naButtonEnabled = naButtonEnabled;
	}

    @JsonProperty("is_standard_button_enabled")
	public boolean isStandartButtonEnabled() {
		return standartButtonEnabled;
	}

	public void setStandartButtonEnabled(boolean standartButtonEnabled) {
		this.standartButtonEnabled = standartButtonEnabled;
	}

    @JsonProperty("is_repair_button_enabled")
	public boolean isRepairButtonEnabled() {
		return repairButtonEnabled;
	}

	public void setRepairButtonEnabled(boolean repairButtonEnabled) {
		this.repairButtonEnabled = repairButtonEnabled;
	}

    @JsonProperty("is_reproved_button_enabled")
	public boolean isReprovedButtonEnabled() {
		return reprovedButtonEnabled;
	}

	public void setReprovedButtonEnabled(boolean reprovedButtonEnabled) {
		this.reprovedButtonEnabled = reprovedButtonEnabled;
	}

    @JsonProperty("is_show_additional_info")
	public boolean isShowAdditionalInfo() {
		return showAdditionalInfo;
	}

	public void setShowAdditionalInfo(boolean showAdditionalInfo) {
		this.showAdditionalInfo = showAdditionalInfo;
	}

    @JsonProperty("show_additional_info_key")
	public String getShowAdditionalInfoKey() {
		return showAdditionalInfoKey;
	}

	public void setShowAdditionalInfoKey(String showAdditionalInfoKey) {
		this.showAdditionalInfoKey = showAdditionalInfoKey;
	}

    @JsonProperty("is_capture_additional_info")
	public boolean isCaptureAdditionalInfo() {
		return captureAdditionalInfo;
	}
	public void setCaptureAdditionalInfo(boolean captureAdditionalInfo) {
		this.captureAdditionalInfo = captureAdditionalInfo;
	}

    @JsonProperty("capture_additional_info_key")
	public String getCaptureAdditionalInfoKey() {
		return captureAdditionalInfoKey;
	}
	public void setCaptureAdditionalInfoKey(String captureAdditionalInfoKey) {
		this.captureAdditionalInfoKey = captureAdditionalInfoKey;
	}

    @JsonProperty("is_generates_discount")
	public boolean isGenerateDiscount() {
		return generateDiscount;
	}
	public void setGenerateDiscount(boolean generateDiscount) {
		this.generateDiscount = generateDiscount;
	}

    @JsonProperty("is_generates_summary_comment")
	public boolean isGenerateSummaryComment() {
		return generateSummaryComment;
	}
	public void setGenerateSummaryComment(boolean generateSummaryComment) {
		this.generateSummaryComment = generateSummaryComment;
	}

    @JsonProperty("is_generates_email_alert")
	public boolean isGenerateEmailAlert() {
		return generateEmailAlert;
	}
	public void setGenerateEmailAlert(boolean generateEmailAlert) {
		this.generateEmailAlert = generateEmailAlert;
	}

    @JsonProperty("discount_type_id")
    public Long getDiscountTypeId() {
        return discountTypeId;
    }
    public void setDiscountTypeId(Long discountTypeId) {
        this.discountTypeId = discountTypeId;
    }

    @JsonProperty("inspection_point_type_id")
    public Long getInspectionPointTypeId() {
        return inspectionPointTypeId;
    }
    public void setInspectionPointTypeId(Long inspectionPointTypeId) {
        this.inspectionPointTypeId = inspectionPointTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("is_mandatory")
    public boolean isMandatory() {
        return mandatory;
    }
    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("inspection_point_car")
    public List<InspectionPointCarDTO> getListInspectionPointCarDTO() {
        return listInspectionPointCarDTO;
    }
    public void setListInspectionPointCarDTO(List<InspectionPointCarDTO> listInspectionPointCarDTO) {
        this.listInspectionPointCarDTO = listInspectionPointCarDTO;
    }

}
