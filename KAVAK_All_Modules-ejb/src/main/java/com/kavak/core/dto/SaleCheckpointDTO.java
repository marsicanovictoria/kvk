package com.kavak.core.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;


public class SaleCheckpointDTO {

	private Long id;
	private Long idUser;
	private String customerName;
	private String customerLastName;
	private String email;
	private Long carId;
	private String sku;
	private Long status;
	private Long carYear;
	private String carMake;
	private String carModel;
	private String carVersion;
	private Long carKm;
	private String zipCode;
	private Long shippingAddressId;
	private String shippingAddressDescription;
	private String warrantyName;
	private String warrantyTime;
	private Long warrantyAmount;
	private String warrantyRange;
	private String insuranceName;
	private String insuranceTime;
	private String insuranceAmount;
	private String insuranceModality;
	private String paymentMethodTypeId;
	private String isFinanced;
	private String isFinancingCompleted;
	private String downPayment;
	private String amountFinancing;
	private String monthFinancing;
	private String checkoutResult;
	private String paymentPlatform;
	private String paymentPlatformId;
	private String transactionNumber;
	private Long paymentStatus;
	private String carCost;
	private String totalAmount;
	private String carReserve;
	private String carShippingCost;
	private String registerDate;
	private String updateDate;
	private Long saleOpportunityTypeId;
	private Long scheduleDateId;
	//private String netsuiteItemId;
	private Long duplicatedOfferControl;
	private Long assignedEm;
	private String appointmentContact;
	private String appointmentDate;
	private String comments;
	private String otherInterestedCar;
	private String watchedCar;
    private Long netsuiteOpportunityId;
    private String netsuiteOpportunityURL;
    private Timestamp netsuiteIdUpdateDate;
    private  String appliedCoupon;
	private Long internalUserId;
	private Long fakeBooking;
	private String source;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("user_id")
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	
	@JsonProperty("user_name")
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@JsonProperty("user_last_name")
	public String getCustomerLastName() {
		return customerLastName;
	}
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	
	@JsonProperty("user_email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@JsonProperty("car_id")
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	
	@JsonProperty("car_sku")
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	
	@JsonProperty("status")
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	
	@JsonProperty("car_year")
	public Long getCarYear() {
		return carYear;
	}
	public void setCarYear(Long carYear) {
		this.carYear = carYear;
	}
	
	@JsonProperty("car_make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}
	
	@JsonProperty("car_model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	
	@JsonProperty("car_version")
	public String getCarVersion() {
		return carVersion;
	}
	public void setCarVersion(String carVersion) {
		this.carVersion = carVersion;
	}
	
	@JsonProperty("car_km")
	public Long getCarKm() {
		return carKm;
	}
	public void setCarKm(Long carKm) {
		this.carKm = carKm;
	}
	
	@JsonProperty("zip_code")
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	@JsonProperty("shipping_address_id")
	public Long getShippingAddressId() {
		return shippingAddressId;
	}
	public void setShippingAddressId(Long shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}
	
	@JsonProperty("shipping_address_description")
	public String getShippingAddressDescription() {
		return shippingAddressDescription;
	}
	public void setShippingAddressDescription(String shippingAddressDescription) {
		this.shippingAddressDescription = shippingAddressDescription;
	}
	
	@JsonProperty("warranty_name")
	public String getWarrantyName() {
		return warrantyName;
	}
	public void setWarrantyName(String warrantyName) {
		this.warrantyName = warrantyName;
	}
	
	@JsonProperty("warranty_time")
	public String getWarrantyTime() {
		return warrantyTime;
	}
	public void setWarrantyTime(String warrantyTime) {
		this.warrantyTime = warrantyTime;
	}
	
	@JsonProperty("warranty_amount")
	public Long getWarrantyAmount() {
		return warrantyAmount;
	}
	public void setWarrantyAmount(Long warrantyAmount) {
		this.warrantyAmount = warrantyAmount;
	}
	
	@JsonProperty("warranty_range")
	public String getWarrantyRange() {
		return warrantyRange;
	}
	public void setWarrantyRange(String warrantyRange) {
		this.warrantyRange = warrantyRange;
	}
	
	@JsonProperty("insurance_name")
	public String getInsuranceName() {
		return insuranceName;
	}
	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}
	
	@JsonProperty("insurance_time")
	public String getInsuranceTime() {
		return insuranceTime;
	}
	public void setInsuranceTime(String insuranceTime) {
		this.insuranceTime = insuranceTime;
	}
	
	@JsonProperty("insurance_amount")
	public String getInsuranceAmount() {
		return insuranceAmount;
	}
	public void setInsuranceAmount(String insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	
	@JsonProperty("insurance_modality")
	public String getInsuranceModality() {
		return insuranceModality;
	}
	public void setInsuranceModality(String insuranceModality) {
		this.insuranceModality = insuranceModality;
	}
	
	@JsonProperty("payment_method_type_id")
	public String getPaymentMethodTypeId() {
		return paymentMethodTypeId;
	}
	public void setPaymentMethodTypeId(String paymentMethodTypeId) {
		this.paymentMethodTypeId = paymentMethodTypeId;
	}
	
	@JsonProperty("is_financed")
	public String getIsFinanced() {
		return isFinanced;
	}
	public void setIsFinanced(String isFinanced) {
		this.isFinanced = isFinanced;
	}
	
	@JsonProperty("is_financing_completed")
	public String getIsFinancingCompleted() {
		return isFinancingCompleted;
	}
	public void setIsFinancingCompleted(String isFinancingCompleted) {
		this.isFinancingCompleted = isFinancingCompleted;
	}
	
	@JsonProperty("downpayment")
	public String getDownPayment() {
		return downPayment;
	}
	public void setDownPayment(String downPayment) {
		this.downPayment = downPayment;
	}
	
	@JsonProperty("amount_financing")
	public String getAmountFinancing() {
		return amountFinancing;
	}
	public void setAmountFinancing(String amountFinancing) {
		this.amountFinancing = amountFinancing;
	}
	
	@JsonProperty("month_financing")
	public String getMonthFinancing() {
		return monthFinancing;
	}
	public void setMonthFinancing(String monthFinancing) {
		this.monthFinancing = monthFinancing;
	}
	
	@JsonProperty("checkout_result")
	public String getCheckoutResult() {
		return checkoutResult;
	}
	public void setCheckoutResult(String checkoutResult) {
		this.checkoutResult = checkoutResult;
	}
	
	@JsonProperty("payment_platform")
	public String getPaymentPlatform() {
		return paymentPlatform;
	}
	public void setPaymentPlatform(String paymentPlatform) {
		this.paymentPlatform = paymentPlatform;
	}
	
	@JsonProperty("payment_platform_id")
	public String getPaymentPlatformId() {
		return paymentPlatformId;
	}
	public void setPaymentPlatformId(String paymentPlatformId) {
		this.paymentPlatformId = paymentPlatformId;
	}
	
	@JsonProperty("transaction_number")
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	
	@JsonProperty("payment_status")
	public Long getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(Long paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	
	@JsonProperty("car_cost")
	public String getCarCost() {
		return carCost;
	}
	public void setCarCost(String carCost) {
		this.carCost = carCost;
	}
	
	@JsonProperty("total_amount")
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	@JsonProperty("car_reserve")
	public String getCarReserve() {
		return carReserve;
	}
	public void setCarReserve(String carReserve) {
		this.carReserve = carReserve;
	}
	
	@JsonProperty("shipping_cost")
	public String getCarShippingCost() {
		return carShippingCost;
	}
	public void setCarShippingCost(String carShippingCost) {
		this.carShippingCost = carShippingCost;
	}
	
	@JsonProperty("register_date")
	public String getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	
	@JsonProperty("update_date")
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
	@JsonProperty("sale_opportunity_type_id")
	public Long getSaleOpportunityTypeId() {
		return saleOpportunityTypeId;
	}
	public void setSaleOpportunityTypeId(Long saleOpportunityTypeId) {
		this.saleOpportunityTypeId = saleOpportunityTypeId;
	}
	
	@JsonProperty("schedule_date_id")
	public Long getScheduleDateId() {
		return scheduleDateId;
	}
	public void setScheduleDateId(Long scheduleDateId) {
		this.scheduleDateId = scheduleDateId;
	}
	/*
	@JsonProperty("netsuite_item_id")
	public String getNetsuiteItemId() {
		return netsuiteItemId;
	}
	public void setNetsuiteItemId(String netsuiteItemId) {
		this.netsuiteItemId = netsuiteItemId;
	}
	*/
	@JsonProperty("duplicated_offer_control")
	public Long getDuplicatedOfferControl() {
		return duplicatedOfferControl;
	}
	public void setDuplicatedOfferControl(Long duplicatedOfferControl) {
		this.duplicatedOfferControl = duplicatedOfferControl;
	}

	@JsonProperty("assigned_em_id")
	public Long getAssignedEm() {
		return assignedEm;
	}
	public void setAssignedEm(Long assignedEm) {
		this.assignedEm = assignedEm;
	}
	@JsonProperty("appointment_contact")
	public String getAppointmentContact() {
		return appointmentContact;
	}
	public void setAppointmentContact(String appointmentContact) {
		this.appointmentContact = appointmentContact;
	}
	
	@JsonProperty("appointment_date")
	public String getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	
	@JsonProperty("comments")
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	@JsonProperty("others_interested_car")
	public String getOtherInterestedCar() {
		return otherInterestedCar;
	}
	public void setOtherInterestedCar(String otherInterestedCar) {
		this.otherInterestedCar = otherInterestedCar;
	}
	
	@JsonProperty("watched_car")
	public String getWatchedCar() {
		return watchedCar;
	}
	public void setWatchedCar(String watchedCar) {
		this.watchedCar = watchedCar;
	}
	
	@JsonProperty("netsuite_opportunity_id")
	public Long getNetsuiteOpportunityId() {
		return netsuiteOpportunityId;
	}
	public void setNetsuiteOpportunityId(Long netsuiteOpportunityId) {
		this.netsuiteOpportunityId = netsuiteOpportunityId;
	}
	
	@JsonProperty("netsuite_opportunity_url")
	public String getNetsuiteOpportunityURL() {
		return netsuiteOpportunityURL;
	}
	public void setNetsuiteOpportunityURL(String netsuiteOpportunityURL) {
		this.netsuiteOpportunityURL = netsuiteOpportunityURL;
	}
	
	@JsonProperty("netsuite_opportunity_update_date")
	public Timestamp getNetsuiteIdUpdateDate() {
		return netsuiteIdUpdateDate;
	}
	public void setNetsuiteIdUpdateDate(Timestamp netsuiteIdUpdateDate) {
		this.netsuiteIdUpdateDate = netsuiteIdUpdateDate;
	}
	
	@JsonProperty("applied_coupon")
	public String getAppliedCoupon() {
		return appliedCoupon;
	}
	public void setAppliedCoupon(String appliedCoupon) {
		this.appliedCoupon = appliedCoupon;
	}
	
	@JsonProperty("internal_user_id")
	public Long getInternalUserId() {
		return internalUserId;
	}
	public void setInternalUserId(Long internalUserId) {
		this.internalUserId = internalUserId;
	}
	
	@JsonProperty("fake_booking")
	public Long getFakeBooking() {
		return fakeBooking;
	}
	public void setFakeBooking(Long fakeBooking) {
		this.fakeBooking = fakeBooking;
	}
	
	@JsonProperty("source")
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
}
