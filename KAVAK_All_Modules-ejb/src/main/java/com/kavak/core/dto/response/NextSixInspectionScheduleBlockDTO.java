package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.InspectionScheduleBlockDTO;

import java.util.List;

public class NextSixInspectionScheduleBlockDTO {
	
	private String date;
	private List<InspectionScheduleBlockDTO> listInspectionScheduleBlockDTO;
	
	@JsonProperty("date")
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@JsonProperty("inspection_schedule_hour_blocks")
	public List<InspectionScheduleBlockDTO> getListInspectionScheduleBlockDTO() {
		return listInspectionScheduleBlockDTO;
	}
	public void setListInspectionScheduleBlockDTO(List<InspectionScheduleBlockDTO> listInspectionScheduleBlockDTO) {
		this.listInspectionScheduleBlockDTO = listInspectionScheduleBlockDTO;
	}
}

