package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserEscoreApiBodyResponseDTO {

    	private FinancingUserEscoreApiDataResponseDTO data;

    	
    	@JsonProperty("data")
	public FinancingUserEscoreApiDataResponseDTO getData() {
	    return data;
	}
	public void setData(FinancingUserEscoreApiDataResponseDTO data) {
	    this.data = data;
	}

}
