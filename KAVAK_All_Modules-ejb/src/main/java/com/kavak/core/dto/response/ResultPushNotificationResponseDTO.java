package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultPushNotificationResponseDTO {
	
    
	private String messageId;

	
	@JsonProperty("message_id")
	public String getMessageId() {
	    return messageId;
	}

	public void setMessageId(String messageId) {
	    this.messageId = messageId;
	}

	




}