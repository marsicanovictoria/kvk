package com.kavak.core.dto.netsuite;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.enumeration.CatalogueCarStatusEnum;

public class InventoryDTO {

	private Long id;
	private Long leadMinervaId;
	private Long minervaOfferId;
	private String carKm;
	private Long price;
	private String carYear;
	private String carMake;
	private String carModel;
	private String carTrim;
	private String sku;
	private boolean certified;
	private boolean valide;
	private boolean active;
	private boolean sale;
	private String carColor;
	private String uberType;
	private String transmission;
    private Long isWishlist;
	private Long netsuiteItemId;
	private CatalogueCarStatusEnum status;
	private boolean downpayment;
	private String postDate;
	private String bookingDate;
	private String preloadDate;
	private String cancelDate;
	private String saleDate;
	private String deliveredDate;
	private Long deliveryDays;
	private Long fakeBooking;

	

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("lead_minerva_id")
	public Long getLeadMinervaId() {
		return leadMinervaId;
	}
	public void setLeadMinervaId(Long leadMinervaId) {
		this.leadMinervaId = leadMinervaId;
	}

	@JsonProperty("minerva_offer_id")
	public Long getMinervaOfferId() {
		return minervaOfferId;
	}
	public void setMinervaOfferId(Long minervaOfferId) {
		this.minervaOfferId = minervaOfferId;
	}

	@JsonProperty("km")
	public String getCarKm() {
		return carKm;
	}
	public void setCarKm(String carKm) {
		this.carKm = carKm;
	}

	@JsonProperty("price")
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	@JsonProperty("year")
	public String getCarYear() {
		return carYear;
	}
	public void setCarYear(String carYear) {
		this.carYear = carYear;
	}

	@JsonProperty("make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}

	@JsonProperty("model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	@JsonProperty("full_version")
	public String getCarTrim() {
		return carTrim;
	}
	public void setCarTrim(String carTrim) {
		this.carTrim = carTrim;
	}

	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}

	@JsonProperty("certified")
	public boolean isCertified() {
		return certified;
	}
	public void setCertified(boolean certified) {
		this.certified = certified;
	}

	@JsonProperty("valide")
	public boolean isValide() {
		return valide;
	}
	public void setValide(boolean valide) {
		this.valide = valide;
	}

	@JsonProperty("active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@JsonProperty("sale")
	public boolean isSale() {
		return sale;
	}
	public void setSale(boolean sale) {
		this.sale = sale;
	}

	@JsonProperty("color")
	public String getCarColor() {
		return carColor;
	}
	public void setCarColor(String carColor) {
		this.carColor = carColor;
	}
	@JsonProperty("uber_type")
	public String getUberType() {
		return uberType;
	}
	public void setUberType(String uberType) {
		this.uberType = uberType;
	}

	@JsonProperty("transmission")
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

    @JsonProperty("is_wishlist")
    public Long isWishlist() {
		return isWishlist;
	}
	public void setWishlist(Long isWishlist) {
		this.isWishlist = isWishlist;
	}
	@JsonProperty("netsuite_item_id")
	public Long getNetsuiteItemId() {
		return netsuiteItemId;
	}
	public void setNetsuiteItemId(Long netsuiteItemId) {
		this.netsuiteItemId = netsuiteItemId;
	}
	
    @JsonProperty("status")
	public CatalogueCarStatusEnum getStatus() {
		return status;
	}
	public void setStatus(CatalogueCarStatusEnum status) {
		this.status = status;
	}
	  
	@JsonProperty("is_downpayment")
	public boolean isDownpayment() {
		return downpayment;
	}
	public void setDownpayment(boolean downpayment) {
		this.downpayment = downpayment;
	}
	
	@JsonProperty("post_date")
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	
	@JsonProperty("booking_date")
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	
	@JsonProperty("preload_date")
	public String getPreloadDate() {
		return preloadDate;
	}
	public void setPreloadDate(String preloadDate) {
		this.preloadDate = preloadDate;
	}
	
	@JsonProperty("cancel_date")
	public String getCancelDate() {
		return cancelDate;
	}
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	
	@JsonProperty("sale_date")
	public String getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}
	@JsonProperty("delivered_date")
	public String getDeliveredDate() {
		return deliveredDate;
	}
	public void setDeliveredDate(String deliveredDate) {
		this.deliveredDate = deliveredDate;
	}
	
	@JsonProperty("delivery_days")
	public Long getDeliveryDays() {
		return deliveryDays;
	}
	public void setDeliveryDays(Long deliveryDays) {
		this.deliveryDays = deliveryDays;
	}
	
	@JsonProperty("fake_booking")
	public Long getFakeBooking() {
		return fakeBooking;
	}
	public void setFakeBooking(Long fakeBooking) {
		this.fakeBooking = fakeBooking;
	}
}
