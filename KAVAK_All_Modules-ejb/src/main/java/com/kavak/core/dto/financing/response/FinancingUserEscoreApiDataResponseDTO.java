package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserEscoreApiDataResponseDTO {

	private String id;
	private String type;
	private FinancingUserEscoreApiAttributesResponseDTO attributes;
	

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("id")
	public String getId() {
	    return id;
	}
	public void setId(String id) {
	    this.id = id;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("attributes")
	public FinancingUserEscoreApiAttributesResponseDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserEscoreApiAttributesResponseDTO attributes) {
	    this.attributes = attributes;
	}

	

}