package com.kavak.core.dto.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.CarscoutQuestionsDTO;

public class CarscoutQuestionsResponseDTO {

	private List<CarscoutQuestionsDTO> listCarscoutQuestionDTO;

	@JsonProperty("questions")
	public List<CarscoutQuestionsDTO> getListCarscoutQuestionDTO() {
	    return listCarscoutQuestionDTO;
	}

	public void setListCarscoutQuestionDTO(List<CarscoutQuestionsDTO> listCarscoutQuestionDTO) {
	    this.listCarscoutQuestionDTO = listCarscoutQuestionDTO;
	}



}
