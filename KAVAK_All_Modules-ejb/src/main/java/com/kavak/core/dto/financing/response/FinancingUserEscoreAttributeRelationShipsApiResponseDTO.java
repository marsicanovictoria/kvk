package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserEscoreAttributeRelationShipsApiResponseDTO {

	private FinancingUserEscoreAttributearbolScoreResultApiResponseDTO arbolScoreResult;

	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonProperty("arbolScoreResult")
	public FinancingUserEscoreAttributearbolScoreResultApiResponseDTO getArbolScoreResult() {
	    return arbolScoreResult;
	}

	public void setArbolScoreResult(FinancingUserEscoreAttributearbolScoreResultApiResponseDTO arbolScoreResult) {
	    this.arbolScoreResult = arbolScoreResult;
	}


}