package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiUrlShortenerDataDTO {

    private String url;
    private String hash;
    private String globalHash;
    private String longUrl;
    private String newHash;
    
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    @JsonProperty("hash")
    public String getHash() {
        return hash;
    }
    public void setHash(String hash) {
        this.hash = hash;
    }
    
    @JsonProperty("global_hash")
    public String getGlobalHash() {
        return globalHash;
    }
    public void setGlobalHash(String globalHash) {
        this.globalHash = globalHash;
    }
    
    @JsonProperty("long_url")
    public String getLongUrl() {
        return longUrl;
    }
    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }
    
    @JsonProperty("new_hash")
    public String getNewHash() {
        return newHash;
    }
    public void setNewHash(String newHash) {
        this.newHash = newHash;
    }
    

}
