package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

public class UserRequestDTO {

	private Long id;
	private String name;
	private String username;
	private String email;
	private String password;
	private String role;
	private boolean active;
	private Timestamp creationDate;
	private String ipUsuario;
	private String phone;
	private String address;
	private String googlePlusId;
	private String facebookUserId;
	private String socialNetworkToken;
	private Integer socialNetwork;
	private Boolean isRegisterWithoutPassword;
	private Long version;
	private String street;
	private String exteriorNumber;
	private String interiorNumber;
	private String postalCode;
	private String source;


	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("role")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("is_active")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("ip_usuario")
	public String getIpUsuario() {
		return ipUsuario;
	}

	public void setIpUsuario(String ipUsuario) {
		this.ipUsuario = ipUsuario;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("google_plus_user_id")
	public String getGooglePlusId() {
		return googlePlusId;
	}

	public void setGooglePlusId(String googlePlusId) {
		this.googlePlusId = googlePlusId;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("facebook_user_id")
	public String getFacebookUserId() {
		return facebookUserId;
	}

	public void setFacebookUserId(String facebookUserId) {
		this.facebookUserId = facebookUserId;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("social_network_token")
	public String getSocialNetworkToken() {
		return socialNetworkToken;
	}

	public void setSocialNetworkToken(String socialNetworkToken) {
		this.socialNetworkToken = socialNetworkToken;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("social_network")
	public Integer getSocialNetwork() {
		return socialNetwork;
	}

	public void setSocialNetwork(Integer socialNetwork) {
		this.socialNetwork = socialNetwork;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("is_register_without_password")
	public Boolean getIsRegisterWithoutPassword() {
		return isRegisterWithoutPassword;
	}

	public void setRegisterWithoutPassword(Boolean registerWithoutPassword) {
		isRegisterWithoutPassword = registerWithoutPassword;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("version")
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("street")
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("exterior_number")
	public String getExteriorNumber() {
		return exteriorNumber;
	}

	public void setExteriorNumber(String exteriorNumber) {
		this.exteriorNumber = exteriorNumber;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
		@JsonProperty("interior_number")
	public String getInteriorNumber() {
		return interiorNumber;
	}

	public void setInteriorNumber(String interiorNumber) {
		this.interiorNumber = interiorNumber;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("postal")
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@JsonProperty("source")
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
}
