package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.financing.FinancingUserUpdateIncomeApiDataDTO;

public class FinancingUserUpdateIncomeApiBodyRequestDTO {

    	private FinancingUserUpdateIncomeApiDataDTO data;


    	@JsonProperty("data")
	public FinancingUserUpdateIncomeApiDataDTO getData() {
	    return data;
	}

	public void setData(FinancingUserUpdateIncomeApiDataDTO data) {
	    this.data = data;
	}


}
