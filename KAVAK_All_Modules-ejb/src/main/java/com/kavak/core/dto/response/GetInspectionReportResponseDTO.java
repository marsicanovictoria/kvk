package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.DimpleDataDTO;
import com.kavak.core.dto.InspectionPointCarHistoryDataDTO;
import com.kavak.core.dto.InspectionPointDataDTO;

import java.util.List;


public class GetInspectionReportResponseDTO {


    private List<InspectionPointDataDTO> listInspectionPointDataDTO;
	private List<InspectionPointCarHistoryDataDTO> listInspectionPointCarHistoryDataDTO;
	private DimpleDataDTO dimplesData;

	@JsonProperty("inspection_report")
	public List<InspectionPointDataDTO> getListInspectionPointDataDTO() {
		return listInspectionPointDataDTO;
	}

	public void setListInspectionPointDataDTO(List<InspectionPointDataDTO> listInspectionPointDataDTO) {
		this.listInspectionPointDataDTO = listInspectionPointDataDTO;
	}
	@JsonProperty("vehicle_history")
	public List<InspectionPointCarHistoryDataDTO> gbetListInspectionPointCarHistoryDataDTO() {
		return listInspectionPointCarHistoryDataDTO;
	}

	public void setListInspectionPointCarHistoryDataDTO(List<InspectionPointCarHistoryDataDTO> listInspectionPointCarHistoryDataDTO) {
		this.listInspectionPointCarHistoryDataDTO = listInspectionPointCarHistoryDataDTO;
	}
	@JsonProperty("dimples")
    public DimpleDataDTO getDimplesData() {
        return dimplesData;
    }

    public void setDimplesData(DimpleDataDTO dimplesData) {
        this.dimplesData = dimplesData;
    }
}
