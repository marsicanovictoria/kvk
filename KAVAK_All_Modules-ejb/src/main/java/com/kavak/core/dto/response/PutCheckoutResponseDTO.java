package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.openpay.BankTransferDetailDTO;

public class PutCheckoutResponseDTO {

    private Long checkpointId;
    private String carName;
    private String km;
    private String carPrice;
    private String bookingPrice;
    private String warranty;
    private String insurance;
    private String financing;
    private BankTransferDetailDTO bankTransferDetailDTO;
    private OrderResponseDTO OrderResponseDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("checkpoint_id")
    public Long getCheckpointId() {
        return checkpointId;
    }
    public void setCheckpointId(Long checkpointId) {
        this.checkpointId = checkpointId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("car_name")
    public String getCarName() {
        return carName;
    }
    public void setCarName(String carName) {
        this.carName = carName;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("km")
    public String getKm() {
        return km;
    }
    public void setKm(String km) {
        this.km = km;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("car_price")
    public String getCarPrice() {
        return carPrice;
    }
    public void setCarPrice(String carPrice) {
        this.carPrice = carPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("booking_price")
    public String getBookingPrice() {
        return bookingPrice;
    }
    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("warranty")
    public String getWarranty() {
        return warranty;
    }
    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("insurance")
    public String getInsurance() {
        return insurance;
    }
    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("financing")
    public String getFinancing() {
        return financing;
    }
    public void setFinancing(String financing) {
        this.financing = financing;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("bank_transfer_details")
    public BankTransferDetailDTO getBankTransferDetailDTO() {
        return bankTransferDetailDTO;
    }
    public void setBankTransferDetailDTO(BankTransferDetailDTO bankTransferDetailDTO) {
        this.bankTransferDetailDTO = bankTransferDetailDTO;
    }
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("order")
    public OrderResponseDTO getOrderResponseDTO() {
        return OrderResponseDTO;
    }
    public void setOrderResponseDTO(OrderResponseDTO orderResponseDTO) {
        OrderResponseDTO = orderResponseDTO;
    }
}