package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Enrique on 19-Jun-17.
 */
public class GetInspectionCentersResponseDTO {

    private Long id;
    private String name;
    private String address;
    private String country;
    private String state;
    private String city;
    private String colony;
    private String zip;
    private String kmToAddress;
    private String latitude;
    private String longitude;
    private String timeToAddress;
    private Boolean carAtThisLocation;
    private boolean isAvailable;

    @JsonProperty("id")
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("address")
    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("country")
    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("state")
    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("city")
    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("colony")
    public String getColony() {
	return colony;
    }

    public void setColony(String colony) {
	this.colony = colony;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("zip")
    public String getZip() {
	return zip;
    }

    public void setZip(String zip) {
	this.zip = zip;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("km_to_address")
    public String getKmToAddress() {
	return kmToAddress;
    }

    public void setKmToAddress(String kmToAddress) {
	this.kmToAddress = kmToAddress;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("latitude")
    public String getLatitude() {
	return latitude;
    }

    public void setLatitude(String latitude) {
	this.latitude = latitude;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("longitude")
    public String getLongitude() {
	return longitude;
    }

    public void setLongitude(String longitude) {
	this.longitude = longitude;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("time_to_address")
    public String getTimeToAddress() {
	return timeToAddress;
    }

    public void setTimToAddress(String timeToAddress) {
	this.timeToAddress = timeToAddress;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("car_at_this_location")
    public Boolean isCarAtThisLocation() {
	return carAtThisLocation;
    }

    public void setCarAtThisLocation(Boolean carAtThisLocation) {
	this.carAtThisLocation = carAtThisLocation;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("is_available")
    public boolean isAvailable() {
	return isAvailable;
    }

    public void setAvailable(boolean available) {
	isAvailable = available;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("full_address")
    public String getFullAddres() {
	if (getAddress() != null && getColony() != null && getZip() != null && getCity() != null && getState() != null && getCountry() != null) {
	    return getAddress() + ". " + getColony() + ". " + getZip() + ". " + getCity() + ". " + getState() + "." + getCountry() + ".";
	} else {
	    return null;
	}
    }

}
