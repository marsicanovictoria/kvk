package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetFinancingUserRfcResponseDTO {

    private String rfc;

    
    @JsonProperty("rfc")
    public String getRfc() {
        return rfc;
    }
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

}
