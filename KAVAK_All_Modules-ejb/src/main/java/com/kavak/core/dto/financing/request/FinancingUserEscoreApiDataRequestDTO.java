package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserEscoreApiDataRequestDTO {

	private String type;
	private FinancingUserEscoreApiAttributesRequestDTO attributes;
	

	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("attributes")
	public FinancingUserEscoreApiAttributesRequestDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserEscoreApiAttributesRequestDTO attributes) {
	    this.attributes = attributes;
	}

}