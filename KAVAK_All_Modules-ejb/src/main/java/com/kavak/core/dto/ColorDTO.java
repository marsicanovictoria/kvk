package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ColorDTO {

	private Long id;
	private String name;
	private String hexadecimal;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("hexadecimal")
	public String getHexadecimal() {
		return hexadecimal;
	}
	public void setHexadecimal(String hexadecimal) {
		this.hexadecimal = hexadecimal;
	}
}