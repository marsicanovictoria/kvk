package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateIncomeApiDataDTO {


	private String type;
	private FinancingUserUpdateIncomeApiAttributesDTO attributes;	

	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("attributes")
	public FinancingUserUpdateIncomeApiAttributesDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserUpdateIncomeApiAttributesDTO attributes) {
	    this.attributes = attributes;
	}

}