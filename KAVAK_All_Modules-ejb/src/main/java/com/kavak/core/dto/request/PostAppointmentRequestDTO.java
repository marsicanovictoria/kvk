package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.BillingAddressDTO;

/**
 * Created by Enrique on 11-Jul-17.
 */
public class PostAppointmentRequestDTO {

    private Long carId;
    private Long idUser;
    private Long idLocation;
    private String appointmentDate;
    private Long idAppointmentHourBlock;
    private String deviceSessionId;
    private String tokenId;
    private Long paymentTypeId;
    private Long carPrice;
    private BillingAddressDTO billingAddressDTO;
    private String source;
    private Long internalUserId;
    private boolean disableCommunications;

    @JsonProperty("car_id")
    public Long getCarId() {
        return carId;
    }
    public void setCarId(Long carId) {
        this.carId = carId;
    }

    @JsonProperty("user_id")
    public Long getIdUser() {
        return idUser;
    }
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    @JsonProperty("location_id")
    public Long getIdLocation() {
        return idLocation;
    }
    public void setIdLocation(Long idLocation) {
        this.idLocation = idLocation;
    }

    @JsonProperty("appointment_date")
    public String getAppointmentDate() {
        return appointmentDate;
    }
    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    @JsonProperty("appointment_hour_block_id")
    public Long getIdAppointmentHourBlock() {
        return idAppointmentHourBlock;
    }
    public void setIdAppointmentHourBlock(Long idAppointmentHourBlock) {
        this.idAppointmentHourBlock = idAppointmentHourBlock;
    }

    @JsonProperty("device_session_id")
    public String getDeviceSessionId() {
        return deviceSessionId;
    }
    public void setDeviceSessionId(String deviceSessionId) {
        this.deviceSessionId = deviceSessionId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("token_id")
    public String getTokenId() {
        return tokenId;
    }
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("payment_type")
    public Long getPaymentTypeId() {
        return paymentTypeId;
    }
    public void setPaymentTypeId(Long paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("car_price")
    public Long getCarPrice() {
        return carPrice;
    }
    public void setCarPrice(Long carPrice) {
        this.carPrice = carPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("billing_address")
    public BillingAddressDTO getBillingAddressDTO() {
        return billingAddressDTO;
    }
    public void setBillingAddressDTO(BillingAddressDTO billingAddressDTO) {
        this.billingAddressDTO = billingAddressDTO;
    }
    
    @JsonProperty("source")
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }
    
    @JsonProperty("internal_user_id")
    public Long getInternalUserId() {
	return internalUserId;
    }
    public void setInternalUserId(Long internalUserId) {
	this.internalUserId = internalUserId;
    }
    
    @JsonProperty("disable_communications")
    public boolean isDisableCommunications() {
        return disableCommunications;
    }
    public void setDisableCommunications(boolean disableCommunications) {
        this.disableCommunications = disableCommunications;
    }
}
