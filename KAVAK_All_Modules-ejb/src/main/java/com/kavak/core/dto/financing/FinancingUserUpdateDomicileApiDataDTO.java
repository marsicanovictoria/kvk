package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateDomicileApiDataDTO {


	private String type;
	private FinancingUserUpdateDomicileApiAttributesDTO attributes;

	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}

	
	@JsonProperty("attributes")
	public FinancingUserUpdateDomicileApiAttributesDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserUpdateDomicileApiAttributesDTO attributes) {
	    this.attributes = attributes;
	}


}