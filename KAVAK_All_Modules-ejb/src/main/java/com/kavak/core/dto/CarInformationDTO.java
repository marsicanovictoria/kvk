package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.enumeration.CatalogueCarStatusEnum;

import java.util.List;

public class CarInformationDTO {

	private Long id;
	private GenericDTO location;
	private String name;
	private String year;
	private String km;
	private String bodyType;
	private String transmission;
	private String shippingCost;
	private Long price;
	private Double marketPrice;
	private Double savings;
	private Double fepm;
	private boolean allowsAppointments;
	private List<String> listImages;
	private CatalogueCarStatusEnum status;
	private List<GenericDTO> listSellCarWarranty;
	private List<InsuranceGroupDTO> listSellCarInsurance;
	private List<GenericDTO> listSellCarDimple;
	private Long promotionPrice;
	private String promotionName;
	private String promotionColor;

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("car_year")
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}

	@JsonProperty("km")
	public String getKm() {
		return km;
	}
	public void setKm(String km) {
		this.km = km;
	}

	@JsonProperty("body_type")
	public String getBodyType() {
		return bodyType;
	}
	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	@JsonProperty("transmission")
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	@JsonProperty("warrantys")
	public List<GenericDTO> getListSellCarWarranty() {
		return listSellCarWarranty;
	}
	public void setListSellCarWarranty(List<GenericDTO> listSellCarWarranty) {
		this.listSellCarWarranty = listSellCarWarranty;
	}

	@JsonProperty("shipping_cost")
	public String getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(String shippingCost) {
		this.shippingCost = shippingCost;
	}

	@JsonProperty("price")
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}

	@JsonProperty("market_price")
	public Double getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}

	@JsonProperty("savings")
	public Double getSavings() {
		return savings;
	}
	public void setSavings(Double savings) {
		this.savings = savings;
	}

	@JsonProperty("fepm")
	public Double getFepm() {
		return fepm;
	}
	public void setFepm(Double fepm) {
		this.fepm = fepm;
	}

	@JsonProperty("media")
	public List<String> getListImages() {
		return listImages;
	}
	public void setListImages(List<String> listImages) {
		this.listImages = listImages;
	}

	@JsonProperty("status")
	public CatalogueCarStatusEnum getStatus() {
		return status;
	}
	public void setStatus(CatalogueCarStatusEnum status) {
		this.status = status;
	}

	@JsonProperty("sell_car_insurance")
	public List<InsuranceGroupDTO> getListSellCarInsurance() {
		return listSellCarInsurance;
	}
	public void setlistSellCarInsurance(List<InsuranceGroupDTO> listSellCarInsurance) {
		this.listSellCarInsurance = listSellCarInsurance;
	}

	@JsonProperty("sell_car_dimple")
	public List<GenericDTO> getListSellCarDimple() {
		return listSellCarDimple;
	}
	public void setListSellCarDimple(List<GenericDTO> listSellCarDimple) {
		this.listSellCarDimple = listSellCarDimple;
	}

	@JsonProperty("allows_appointments")
	public boolean isAllowsAppointments() {
		return allowsAppointments;
	}
	public void setAllowsAppointments(boolean allowsAppointments) {
		this.allowsAppointments = allowsAppointments;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("promotion_price")
	public Long getPromotionPrice() {
		return promotionPrice;
	}

	public void setPromotionPrice(Long promotionPrice) {
		this.promotionPrice = promotionPrice;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("promotion_name")
	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("promotion_color")
	public String getPromotionColor() {
		return promotionColor;
	}

	public void setPromotionColor(String promotionColor) {
		this.promotionColor = promotionColor;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("location")
	public GenericDTO getLocation() {
	    return location;
	}
	public void setLocation(GenericDTO location) {
	    this.location = location;
	}
	
}
