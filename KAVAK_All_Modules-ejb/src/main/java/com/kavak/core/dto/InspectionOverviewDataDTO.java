package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InspectionOverviewDataDTO {

	private Long idOfferCheckpoint;
	private String inspectorEmail;
	private String inspectionDate;
	private Long idHourBlock;
    private String hourBlockText;
	private boolean canceled;
    private String canceledReason;
    private boolean rescheduled;
    private String rescheduledReason;
    private String inspectionAddress;
    private String nameExperimentManager;
    private String nameTowerContact;
    private Long inspectionTypeId;
    private String comments;

	@JsonProperty("offer_checkpoint_id")
	public Long getIdOfferCheckpoint() {
		return idOfferCheckpoint;
	}
	public void setIdOfferCheckpoint(Long idOfferCheckpoint) {
		this.idOfferCheckpoint = idOfferCheckpoint;
	}

	@JsonProperty("inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}

	@JsonProperty("inspection_date")
	public String getInspectionDate() {
		return inspectionDate;
	}
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

    @JsonProperty("block_hour_id")
    public Long getIdHourBlock() {
        return idHourBlock;
    }
    public void setIdHourBlock(Long idHourBlock) {
        this.idHourBlock = idHourBlock;
    }

    @JsonProperty("block_hour_text")
    public String getHourBlockText() {
        return hourBlockText;
    }
    public void setHourBlockText(String hourBlockText) {
        this.hourBlockText = hourBlockText;
    }

	@JsonProperty("inspection_address")
	public String getInspectionAddress() {
		return inspectionAddress;
	}
	public void setInspectionAddress(String inspectionAddress) {
		this.inspectionAddress = inspectionAddress;
	}

	@JsonProperty("em_contact")
	public String getNameExperimentManager() {
		return nameExperimentManager;
	}
	public void setNameExperimentManager(String nameExperimentManager) {
		this.nameExperimentManager = nameExperimentManager;
	}

	@JsonProperty("tower_contact")
	public String getNameTowerContact() {
		return nameTowerContact;
	}
	public void setNameTowerContact(String nameTowerContact) {
		this.nameTowerContact = nameTowerContact;
	}

	@JsonProperty("comments")
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}

    @JsonProperty("is_canceled")
    public boolean isCanceled() {
        return canceled;
    }
    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    @JsonProperty("is_canceled_reason")
    public String getCanceledReason() {
        return canceledReason;
    }
    public void setCanceledReason(String canceledReason) {
        this.canceledReason = canceledReason;
    }

    @JsonProperty("is_rescheduled")
    public boolean isRescheduled() {
        return rescheduled;
    }
    public void setRescheduled(boolean rescheduled) {
        this.rescheduled = rescheduled;
    }

    @JsonProperty("is_rescheduled_reason")
    public String getRescheduledReason() {
        return rescheduledReason;
    }
    public void setRescheduledReason(String rescheduledReason) {
        this.rescheduledReason = rescheduledReason;
    }

    @JsonProperty("inspection_type_id")
    public Long getInspectionTypeId() {
        return inspectionTypeId;
    }
    public void setInspectionTypeId(Long inspectionTypeId) {
        this.inspectionTypeId = inspectionTypeId;
    }

}
