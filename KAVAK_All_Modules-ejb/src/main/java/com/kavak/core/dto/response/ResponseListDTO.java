//package com.kavak.core.dto.response;
//
//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.annotation.JsonProperty;
//import com.kavak.core.dto.MessageDTO;
//
//import java.util.List;
//
//public class ResponseListDTO {
//
//	private Long code;
//	private String status;
//	private List<MessageDTO> listValidations;
//	private List<?> data;
//
//	@JsonProperty("code")
//	public Long getCode() {
//		return code;
//	}
//	public void setCode(Long code) {
//		this.code = code;
//	}
//	@JsonProperty("status")
//	public String getStatus() {
//		return status;
//	}
//	public void setStatus(String status) {
//		this.status = status;
//	}
//	@JsonInclude(JsonInclude.Include.NON_EMPTY)
//	@JsonProperty("validations")
//	public List<MessageDTO> getListValidations() {
//		return listValidations;
//	}
//	public void setListValidations(List<MessageDTO> listValidations) {
//		this.listValidations = listValidations;
//	}
//	@JsonProperty("data")
//	public List<?> getData() {
//		return data;
//	}
//	public void setData(List<?> data) {
//		this.data = data;
//	}
//}