package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingDataApiMarketPlaceRfcGeneratorDTO {
	
	private String nombreProcesado;
	private String rfc;

	
	@JsonProperty("nombre_procesado")
	public String getNombreProcesado() {
	    return nombreProcesado;
	}
	public void setNombreProcesado(String nombreProcesado) {
	    this.nombreProcesado = nombreProcesado;
	}
	
	@JsonProperty("rfc")
	public String getRfc() {
	    return rfc;
	}
	public void setRfc(String rfc) {
	    this.rfc = rfc;
	}

}