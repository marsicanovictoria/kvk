package com.kavak.core.dto.netsuite;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PaperworkCustomerEmailDTO {

	private Long id;
	private Long stockId;
	private String car;
	private Long carKm;
	private Long paperworkStatusId;
	private String paperworkStatusDescription;
	private Long customerMinervaId;
	private String customerEmail;
	private String currentPlateStatus;
	private String finalPlateStatus;
	private Long paperworkTypeId;
	private String paperworkTypeDescription;
	private String shippingAddress;
	private String shippingManager;
	private String courrier;
	private String trackingNumber;
	private String trackingNumberUrl;
	private String personWhoReceive;
	private String tentativeDeliveryDate;
	private String bccEmail;
	private String replyToEmail;
	private String validityReturn;
	private String emailTemplate;
	private String afterSaleKavakoName;
	private String afterSaleKavakoPhone;
	private String afterSaleKavakoEmail;
	private String paperworkKavakoName;
	private String paperworkKavakoPhone;
	private String paperworkKavakoEmail;
	private ArrayList<String> attachments;
	
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("stock_id")
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	
	@JsonProperty("car_name")
	public String getCar() {
		return car;
	}
	public void setCar(String car) {
		this.car = car;
	}
	
	@JsonProperty("car_km")
	public Long getCarKm() {
		return carKm;
	}
	public void setCarKm(Long carKm) {
		this.carKm = carKm;
	}
	
	@JsonProperty("paperwork_status_id")
	public Long getPaperworkStatusId() {
		return paperworkStatusId;
	}
	public void setPaperworkStatusId(Long paperworkStatusId) {
		this.paperworkStatusId = paperworkStatusId;
	}
	
	@JsonProperty("paperwork_status_description")
	public String getPaperworkStatusDescription() {
		return paperworkStatusDescription;
	}
	public void setPaperworkStatusDescription(String paperworkStatusDescription) {
		this.paperworkStatusDescription = paperworkStatusDescription;
	}
	
	@JsonProperty("customer_minerva_id")
	public Long getCustomerMinervaId() {
		return customerMinervaId;
	}
	public void setCustomerMinervaId(Long customerMinervaId) {
		this.customerMinervaId = customerMinervaId;
	}
	
	@JsonProperty("customer_email")
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	@JsonProperty("current_plate_status")
	public String getCurrentPlateStatus() {
		return currentPlateStatus;
	}
	public void setCurrentPlateStatus(String currentPlateStatus) {
		this.currentPlateStatus = currentPlateStatus;
	}
	
	@JsonProperty("final_plate_status")
	public String getFinalPlateStatus() {
		return finalPlateStatus;
	}
	public void setFinalPlateStatus(String finalPlateStatus) {
		this.finalPlateStatus = finalPlateStatus;
	}
	
	@JsonProperty("paperwork_type_id")
	public Long getPaperworkTypeId() {
		return paperworkTypeId;
	}
	public void setPaperworkTypeId(Long paperworkTypeId) {
		this.paperworkTypeId = paperworkTypeId;
	}
	
	@JsonProperty("paperwork_type_description")
	public String getPaperworkTypeDescription() {
		return paperworkTypeDescription;
	}
	public void setPaperworkTypeDescription(String paperworkTypeDescription) {
		this.paperworkTypeDescription = paperworkTypeDescription;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("shipping_address")
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("manager_shipping")
	public String getShippingManager() {
		return shippingManager;
	}
	public void setShippingManager(String shippingManager) {
		this.shippingManager = shippingManager;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("courrier")
	public String getCourrier() {
		return courrier;
	}
	public void setCourrier(String courrier) {
		this.courrier = courrier;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("tracking_number")
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("tracking_number_url")
	public String getTrackingNumberUrl() {
		return trackingNumberUrl;
	}
	public void setTrackingNumberUrl(String trackingNumberUrl) {
		this.trackingNumberUrl = trackingNumberUrl;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("person_who_receive")
	public String getPersonWhoReceive() {
		return personWhoReceive;
	}
	public void setPersonWhoReceive(String personWhoReceive) {
		this.personWhoReceive = personWhoReceive;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("tentative_delivery_date")
	public String getTentativeDeliveryDate() {
		return tentativeDeliveryDate;
	}
	public void setTentativeDeliveryDate(String tentativeDeliveryDate) {
		this.tentativeDeliveryDate = tentativeDeliveryDate;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("bcc_email")
	public String getBccEmail() {
		return bccEmail;
	}
	public void setBccEmail(String bccEmail) {
		this.bccEmail = bccEmail;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("reply_to_email")
	public String getReplyToEmail() {
		return replyToEmail;
	}
	public void setReplyToEmail(String replyToEmail) {
		this.replyToEmail = replyToEmail;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("validity_return")
	public String getValidityReturn() {
		return validityReturn;
	}
	public void setValidityReturn(String validityReturn) {
		this.validityReturn = validityReturn;
	}
	
	@JsonProperty("email_template")
	public String getEmailTemplate() {
		return emailTemplate;
	}
	public void setEmailTemplate(String emailTemplate) {
		this.emailTemplate = emailTemplate;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("after_sale_kavako_email")
	public String getAfterSaleKavakoEmail() {
		return afterSaleKavakoEmail;
	}
	public void setAfterSaleKavakoEmail(String afterSaleKavakoEmail) {
		this.afterSaleKavakoEmail = afterSaleKavakoEmail;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("after_sale_kavako_name")
	public String getAfterSaleKavakoName() {
		return afterSaleKavakoName;
	}
	public void setAfterSaleKavakoName(String afterSaleKavakoName) {
		this.afterSaleKavakoName = afterSaleKavakoName;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("after_sale_kavako_phone")
	public String getAfterSaleKavakoPhone() {
		return afterSaleKavakoPhone;
	}
	public void setAfterSaleKavakoPhone(String afterSaleKavakoPhone) {
		this.afterSaleKavakoPhone = afterSaleKavakoPhone;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("paperwork_kavako_name")
	public String getPaperworkKavakoName() {
		return paperworkKavakoName;
	}
	public void setPaperworkKavakoName(String paperworkKavakoName) {
		this.paperworkKavakoName = paperworkKavakoName;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("paperwork_kavako_phone")
	public String getPaperworkKavakoPhone() {
		return paperworkKavakoPhone;
	}
	public void setPaperworkKavakoPhone(String paperworkKavakoPhone) {
		this.paperworkKavakoPhone = paperworkKavakoPhone;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("paperwork_kavako_email")
	public String getPaperworkKavakoEmail() {
		return paperworkKavakoEmail;
	}
	public void setPaperworkKavakoEmail(String paperworkKavakoEmail) {
		this.paperworkKavakoEmail = paperworkKavakoEmail;
	}
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("attachments")
	public ArrayList<String> getAttachments() {
		return attachments;
	}
	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}
}
