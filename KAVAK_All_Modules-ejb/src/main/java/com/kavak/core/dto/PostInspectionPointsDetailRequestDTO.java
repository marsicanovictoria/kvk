package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

public class PostInspectionPointsDetailRequestDTO {
	
	private Collection<InspectionPointDetailDTO> listInspectionPointDetailDTO;

	@JsonProperty("inspection_details")
	public Collection<InspectionPointDetailDTO> getListInspectionPointDetailDTO() {
		return listInspectionPointDetailDTO;
	}
	public void setListInspectionPointDetailDTO(Collection<InspectionPointDetailDTO> listInspectionPointDetailDTO) {
		this.listInspectionPointDetailDTO = listInspectionPointDetailDTO;
	}
}

