package com.kavak.core.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InspectionOfferDTO {

	private Long id;
	private Long inspectionId;
	private Long inspectionSummaryId;
	private Long offerTypeId;
	private Double offerAmount;
	private String inspectorEmail;
	private Timestamp creationDate;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("inspection_id")
	public Long getInspectionId() {
		return inspectionId;
	}
	public void setInspectionId(Long inspectionId) {
		this.inspectionId = inspectionId;
	}
	
	@JsonProperty("offer_type_id")
	public Long getOfferTypeId() {
		return offerTypeId;
	}
	public void setOfferTypeId(Long offerTypeId) {
		this.offerTypeId = offerTypeId;
	}
	
	@JsonProperty("offer_amount")
	public Double getOfferAmount() {
		return offerAmount;
	}
	public void setOfferAmount(Double offerAmount) {
		this.offerAmount = offerAmount;
	}
	
	@JsonProperty("inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	
	@JsonProperty("creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	@JsonProperty("inspection_summary_id")
	public Long getInspectionSummaryId() {
		return inspectionSummaryId;
	}
	public void setInspectionSummaryId(Long inspectionSummaryId) {
		this.inspectionSummaryId = inspectionSummaryId;
	}

	
	

	
	
	
	
	
	
	
	
	




}
