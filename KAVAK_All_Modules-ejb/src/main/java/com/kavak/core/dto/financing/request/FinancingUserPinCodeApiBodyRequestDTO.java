package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserPinCodeApiBodyRequestDTO {

    	private String status;

    	@JsonProperty("status")
	public String getStatus() {
	    return status;
	}

	public void setStatus(String status) {
	    this.status = status;
	}
}
