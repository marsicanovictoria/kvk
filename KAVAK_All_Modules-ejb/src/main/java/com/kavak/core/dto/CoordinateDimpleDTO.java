package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CoordinateDimpleDTO {

	private String top;
	private String left;
	
	@JsonProperty("top")
	public String getTop() {
		return top;
	}
	public void setTop(String top) {
		this.top = top;
	}

	@JsonProperty("left")
	public String getLeft() {
		return left;
	}
	public void setLeft(String left) {
		this.left = left;
	}

}
