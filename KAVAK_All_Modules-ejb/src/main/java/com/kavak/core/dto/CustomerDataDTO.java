package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CustomerDataDTO {

    private List<GenericDTO> listPersonType;
    private List<GenericDTO> listIdentificationType;
    private List<GenericDTO> listEducationType;
    private List<GenericDTO> listMaritalStatus;
    private List<GenericDTO> listNacionality;
    private List<GenericDTO> listGenders;
    private List<GenericDTO> listDependents;
    private List<GenericDTO> listBancomerCustomer;
    //private MetaValuePropertyStatusDTO metaValuePropertyStatusDTO;

    @JsonProperty("person_type")
    public List<GenericDTO> getListPersonType() {
        return listPersonType;
    }
    public void setListPersonType(List<GenericDTO> listPersonType) {
        this.listPersonType = listPersonType;
    }

    @JsonProperty("identification_type")
    public List<GenericDTO> getListIdentificationType() {
        return listIdentificationType;
    }
    public void setListIdentificationType(List<GenericDTO> listIdentificationType) {
        this.listIdentificationType = listIdentificationType;
    }

    @JsonProperty("education_level")
    public List<GenericDTO> getListEducationType() {
        return listEducationType;
    }
    public void setListEducationType(List<GenericDTO> listEducationType) {
        this.listEducationType = listEducationType;
    }

    @JsonProperty("marital_status")
    public List<GenericDTO> getListMaritalStatus() {
        return listMaritalStatus;
    }
    public void setListMaritalStatus(List<GenericDTO> listMaritalStatus) {
        this.listMaritalStatus = listMaritalStatus;
    }

    @JsonProperty("nacionality")
    public List<GenericDTO> getListNacionality() {
        return listNacionality;
    }
    public void setListNacionality(List<GenericDTO> listNacionality) {
        this.listNacionality = listNacionality;
    }

    @JsonProperty("gender")
    public List<GenericDTO> getListGenders() {
        return listGenders;
    }
    public void setListGenders(List<GenericDTO> listGenders) {
        this.listGenders = listGenders;
    }

    @JsonProperty("dependents")
    public List<GenericDTO> getListDependents() {
        return listDependents;
    }
    public void setListDependents(List<GenericDTO> listDependents) {
        this.listDependents = listDependents;
    }

    @JsonProperty("is_bancomer_customer")
    public List<GenericDTO> getListBancomerCustomer() {
        return listBancomerCustomer;
    }
    public void setListBancomerCustomer(List<GenericDTO> listBancomerCustomer) {
        this.listBancomerCustomer = listBancomerCustomer;
    }

//    @JsonProperty("domicile")
//    public MetaValuePropertyStatusDTO getMetaValuePropertyStatusDTO() {
//        return metaValuePropertyStatusDTO;
//    }
//    public void setMetaValuePropertyStatusDTO(MetaValuePropertyStatusDTO metaValuePropertyStatusDTO) {
//        this.metaValuePropertyStatusDTO = metaValuePropertyStatusDTO;
//    }



}
