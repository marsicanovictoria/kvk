package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.*;

import java.util.List;

public class GetFinancingFormValuesResponseDTO {

    private List<CarCatalogueDTO> listCarCatalogueDTO;
    private CustomerDataDTO customerDataDTO;
    private MetaValuePropertyStatusDTO metaValuePropertyStatusDTO;
    private MetaValueJobDataDTO metaValueJobDataDTO;
    private MetaValueReferenceDataDTO metaValueReferenceDataDTO;
    private MetaValueAdditionalDTO metaValueAdditionalDTO;

    @JsonProperty("cars")
    public List<CarCatalogueDTO> getListCarCatalogueDTO() {
        return listCarCatalogueDTO;
    }
    public void setListCarCatalogueDTO(List<CarCatalogueDTO> listCarCatalogueDTO) {
        this.listCarCatalogueDTO = listCarCatalogueDTO;
    }

    @JsonProperty("customer")
    public CustomerDataDTO getCustomerDataDTO() {
        return customerDataDTO;
    }
    public void setCustomerDataDTO(CustomerDataDTO customerDataDTO) {
        this.customerDataDTO = customerDataDTO;
    }

    @JsonProperty("domicile")
    public MetaValuePropertyStatusDTO getMetaValuePropertyStatusDTO() {
        return metaValuePropertyStatusDTO;
    }
    public void setMetaValuePropertyStatusDTO(MetaValuePropertyStatusDTO metaValuePropertyStatusDTO) {
        this.metaValuePropertyStatusDTO = metaValuePropertyStatusDTO;
    }

    @JsonProperty("job")
    public MetaValueJobDataDTO getMetaValueJobDataDTO() {
        return metaValueJobDataDTO;
    }
    public void setMetaValueJobDataDTO(MetaValueJobDataDTO metaValueJobDataDTO) {
        this.metaValueJobDataDTO = metaValueJobDataDTO;
    }

    @JsonProperty("reference")
    public MetaValueReferenceDataDTO getMetaValueReferenceDataDTO() {
        return metaValueReferenceDataDTO;
    }
    public void setMetaValueReferenceDataDTO(MetaValueReferenceDataDTO metaValueReferenceDataDTO) {
        this.metaValueReferenceDataDTO = metaValueReferenceDataDTO;
    }

    @JsonProperty("additional")
    public MetaValueAdditionalDTO getMetaValueAdditionalDTO() {
        return metaValueAdditionalDTO;
    }
    public void setMetaValueAdditionalDTO(MetaValueAdditionalDTO metaValueAdditionalDTO) {
        this.metaValueAdditionalDTO = metaValueAdditionalDTO;
    }

}
