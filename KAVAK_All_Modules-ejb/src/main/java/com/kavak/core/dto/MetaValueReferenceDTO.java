package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetaValueReferenceDTO {

	private MetaValueReferenceFamilyDTO metaValueReferenceFamilyDTO;
	private MetaValueReferencePersonalDTO metaValueReferencePersonalDTO;
	private MetaValueReferenceLessorDTO metaValueReferenceLessorDTO;

	@JsonProperty("familiar_reference")
	public MetaValueReferenceFamilyDTO getMetaValueReferenceFamilyDTO() {
		return metaValueReferenceFamilyDTO;
	}

	public void setMetaValueReferenceFamilyDTO(MetaValueReferenceFamilyDTO metaValueReferenceFamilyDTO) {
		this.metaValueReferenceFamilyDTO = metaValueReferenceFamilyDTO;
	}

	@JsonProperty("personal_reference")
	public MetaValueReferencePersonalDTO getMetaValueReferencePersonalDTO() {
		return metaValueReferencePersonalDTO;
	}

	public void setMetaValueReferencePersonalDTO(MetaValueReferencePersonalDTO metaValueReferencePersonalDTO) {
		this.metaValueReferencePersonalDTO = metaValueReferencePersonalDTO;
	}

	@JsonProperty("lessor_reference")
	public MetaValueReferenceLessorDTO getMetaValueReferenceLessorDTO() {
		return metaValueReferenceLessorDTO;
	}

	public void setMetaValueReferenceLessorDTO(MetaValueReferenceLessorDTO metaValueReferenceLessorDTO) {
		this.metaValueReferenceLessorDTO = metaValueReferenceLessorDTO;
	}

}
