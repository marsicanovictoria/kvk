package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MinervaAndNetsuiteIdsDTO {
	private Long minervaId;
	private Long netsuiteId;
	private Long duplicatedOfferControl;
	
	@JsonProperty("minerva_id")
	public Long getMinervaId() {
		return minervaId;
	}
	public void setMinervaId(Long minervaId) {
		this.minervaId = minervaId;
	}
	
	@JsonProperty("netsuite_id")
	public Long getNetsuiteId() {
		return netsuiteId;
	}
	public void setNetsuiteId(Long netsuiteId) {
		this.netsuiteId = netsuiteId;
	}
	
	@JsonProperty("duplicated_offer_control")
	public Long getDuplicatedOfferControl() {
		return duplicatedOfferControl;
	}
	public void setDuplicatedOfferControl(Long duplicatedOfferControl) {
		this.duplicatedOfferControl = duplicatedOfferControl;
	}
	
}
