package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NetsuiteOpportunityDTO {
    
	private Long idMinerva;
    private Long idMinervaStatus;
    private Long netsuiteInternalId;
    private Long netsuiteOpportunityId;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("minerva_id")
	public Long getIdMinerva() {
		return idMinerva;
	}
	public void setIdMinerva(Long idMinerva) {
		this.idMinerva = idMinerva;
	}
	
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("status_minerva_id")
	public Long getIdMinervaStatus() {
		return idMinervaStatus;
	}
	public void setIdMinervaStatus(Long idMinervaStatus) {
		this.idMinervaStatus = idMinervaStatus;
	}
	
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("netsuite_internal_id")
	public Long getNetsuiteInternalId() {
		return netsuiteInternalId;
	}
	public void setNetsuiteInternalId(Long netsuiteInternalId) {
		this.netsuiteInternalId = netsuiteInternalId;
	}
	
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("netsuite_opportunity_id")
	public Long getNetsuiteOpportunityId() {
		return netsuiteOpportunityId;
	}
	public void setNetsuiteOpportunityId(Long netsuiteOpportunityId) {
		this.netsuiteOpportunityId = netsuiteOpportunityId;
	}
    
    
    
}
