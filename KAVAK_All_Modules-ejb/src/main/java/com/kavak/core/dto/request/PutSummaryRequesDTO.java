package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.InspectionSummaryDataDTO;

import java.util.List;

public class PutSummaryRequesDTO {
	
	List<InspectionSummaryDataDTO> listInspectionSummaryDTO;

	@JsonProperty("inspection_summary")
	public List<InspectionSummaryDataDTO> getListInspectionSummaryDTO() {
		return listInspectionSummaryDTO;
	}

	public void setListInspectionSummaryDTO(List<InspectionSummaryDataDTO> listInspectionSummaryDTO) {
		this.listInspectionSummaryDTO = listInspectionSummaryDTO;
	}
}
