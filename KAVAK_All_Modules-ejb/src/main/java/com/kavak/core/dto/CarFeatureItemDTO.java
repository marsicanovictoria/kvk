package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarFeatureItemDTO {

	private Long id;
//	private Long idCategory;
	private String name;
//	private String iconImage;
//	private boolean active;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
//	@JsonProperty("category_id")
//	public Long getIdCategory() {
//		return idCategory;
//	}
//	public void setIdCategory(Long idCategory) {
//		this.idCategory = idCategory;
//	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
//	@JsonProperty("icon_image")
//	public String getIconImage() {
//		return iconImage;
//	}
//	public void setIconImage(String iconImage) {
//		this.iconImage = iconImage;
//	}
//	@JsonProperty("is_active")
//	public boolean isActive() {
//		return active;
//	}
//	public void setActive(boolean active) {
//		this.active = active;
//	}
}
