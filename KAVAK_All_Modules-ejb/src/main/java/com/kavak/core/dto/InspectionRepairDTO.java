package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InspectionRepairDTO {
	
	private Long id;
	private Long inspctionId;
	private Long idInspectionRepairCategory;
	private String inspectionRepairCategoryDescription;
	private Long idInspectionRepairSubCategory;
	private String inspectionRepairSubCategoryDescription;
	private Long idInspectionRepairItem;
	private String inspectionRepairItemDescription;
	private Long idInspectionPointCar; 
	private String inspectionPointCarDescription;
	private Double repairAmount;
	private boolean paidByKavak;
    private String repairImage;
    private String inspectorEmail;
    private String repairOtherText; 

	@JsonProperty("id")
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("inspection_id")
	public Long getInspctionId() {
		return inspctionId;
	}
	public void setInspctionId(Long inspctionId) {
		this.inspctionId = inspctionId;
	}

	@JsonProperty("repair_category_id")
	public Long getIdInspectionRepairCategory() {
		return idInspectionRepairCategory;
	}
	public void setIdInspectionRepairCategory(Long idInspectionRepairCategory) {
		this.idInspectionRepairCategory = idInspectionRepairCategory;
	}

    @JsonProperty("repair_category_description")
	public String getInspectionRepairCategoryDescription() {
		return inspectionRepairCategoryDescription;
	}
	public void setInspectionRepairCategoryDescription(String inspectionRepairCategoryDescription) {
		this.inspectionRepairCategoryDescription = inspectionRepairCategoryDescription;
	}
	
	@JsonProperty("repair_subcategory_id")
	public Long getIdInspectionRepairSubCategory() {
		return idInspectionRepairSubCategory;
	}
	public void setIdInspectionRepairSubCategory(Long idInspectionRepairSubCategory) {
		this.idInspectionRepairSubCategory = idInspectionRepairSubCategory;
	}

	@JsonProperty("repair_subcategory_description")
	public String getInspectionRepairSubCategoryDescription() {
		return inspectionRepairSubCategoryDescription;
	}
	public void setInspectionRepairSubCategoryDescription(String inspectionRepairSubCategoryDescription) {
		this.inspectionRepairSubCategoryDescription = inspectionRepairSubCategoryDescription;
	}
	
	@JsonProperty("repair_type_id")
	public Long getIdInspectionRepairItem() {
		return idInspectionRepairItem;
	}
	public void setIdInspectionRepairItem(Long idInspectionRepairItem) {
		this.idInspectionRepairItem = idInspectionRepairItem;
	}

	@JsonProperty("repair_type_description")
	public String getInspectionRepairItemDescription() {
		return inspectionRepairItemDescription;
	}
	public void setInspectionRepairItemDescription(String inspectionRepairItemDescription) {
		this.inspectionRepairItemDescription = inspectionRepairItemDescription;
	}

	@JsonProperty("repair_amount")
	public Double getRepairAmount() {
		return repairAmount;
	}
	public void setRepairAmount(Double repairAmount) {
		this.repairAmount = repairAmount;
	}

    @JsonProperty("is_paid_by_kavak")
    public boolean isPaidByKavak() {
        return paidByKavak;
    }
    public void setPaidByKavak(boolean paidByKavak) {
        this.paidByKavak = paidByKavak;
    }

	@JsonProperty("inspection_point_car_id")
	public Long getIdInspectionPointCar() {
		return idInspectionPointCar;
	}
	public void setIdInspectionPointCar(Long idInspectionPointCar) {
		this.idInspectionPointCar = idInspectionPointCar;
	}

	@JsonProperty("inspection_point_car_description")
    public String getInspectionPointCarDescription() {
		return inspectionPointCarDescription;
	}
	public void setInspectionPointCarDescription(String inspectionPointCarDescription) {
		this.inspectionPointCarDescription = inspectionPointCarDescription;
	}

	@JsonProperty("photo")
    public String getRepairImage() {
        return repairImage;
    }
    public void setRepairImage(String repairImage) {
        this.repairImage = repairImage;
    }

    @JsonProperty("inspector_email")
    public String getInspectorEmail() {
        return inspectorEmail;
    }
    public void setInspectorEmail(String inspectorEmail) {
        this.inspectorEmail = inspectorEmail;
    }
    
    @JsonProperty("repair_other_text")
	public String getRepairOtherText() {
		return repairOtherText;
	}
	public void setRepairOtherText(String repairOtherText) {
		this.repairOtherText = repairOtherText;
	}
}
