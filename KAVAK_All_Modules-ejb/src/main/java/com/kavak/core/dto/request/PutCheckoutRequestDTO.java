package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.BillingAddressDTO;

public class PutCheckoutRequestDTO {

    private Integer step;      
    private Long userId;     
    private Long carId;      //-----------
    private Long paymentType;    
    private Long checkpointId;   
    private Long deliveryAddressId;    //------ 
    private Long shipmentCost;    //------
    private Long warrantyId;     //-------
    private Long warrantyAmount;   //------
    private Long insuranceId;     //------
    private Long insuranceAmount;  //-------
    private String deviceSessionId;    
    private String tokenId;    
    private String carPrice;     
    private String totalPrice;    
    private Integer downPayment;     
    private Integer financingMonths;   
    private BillingAddressDTO billingAddressDTO; 
    private boolean hasTradeInCar;     //-----
    private Long offerCheckpointId;     //------
    private String source;
    private String versionApp;
    private String internalUserId;
    private boolean disableCommunications; 

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("step")
    public Integer getStep() {
        return step;
    }
    public void setStep(Integer step) {
        this.step = step;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("user_id")
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }


    @JsonProperty("car_id")
    public Long getCarId() {
        return carId;
    }
    public void setCarId(Long carId) {
        this.carId = carId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("payment_type_id")
    public Long getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(Long paymentType) {
        this.paymentType = paymentType;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("checkpoint_id")
    public Long getCheckpointId() {
        return checkpointId;
    }
    public void setCheckpointId(Long checkpointId) {
        this.checkpointId = checkpointId;
    }

    @JsonProperty("delivery_address_id")
    public Long getDeliveryAddressId() {
        return deliveryAddressId;
    }
    public void setDeliveryAddressId(Long deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    @JsonProperty("shipment_cost")
    public Long getShipmentCost() {
        return shipmentCost;
    }
    public void setShipmentCost(Long shipmentCost) {
        this.shipmentCost = shipmentCost;
    }

    @JsonProperty("warranty_id")
    public Long getWarrantyId() {
        return warrantyId;
    }
    public void setWarrantyId(Long warrantyId) {
        this.warrantyId = warrantyId;
    }

    @JsonProperty("warranty_amount")
    public Long getWarrantyAmount() {
        return warrantyAmount;
    }
    public void setWarrantyAmount(Long warrantyAmount) {
        this.warrantyAmount = warrantyAmount;
    }

    @JsonProperty("insurance_id")
    public Long getInsuranceId() {
        return insuranceId;
    }
    public void setInsuranceId(Long insuranceId) {
        this.insuranceId = insuranceId;
    }

    @JsonProperty("insurance_amount")
    public Long getInsuranceAmount() {
        return insuranceAmount;
    }
    public void setInsuranceAmount(Long insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("device_session_id")
    public String getDeviceSessionId() {
        return deviceSessionId;
    }
    public void setDeviceSessionId(String deviceSessionId) {
        this.deviceSessionId = deviceSessionId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("token_id")
    public String getTokenId() {
        return tokenId;
    }
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("car_price")
    public String getCarPrice() {
        return carPrice;
    }
    public void setCarPrice(String carPrice) {
        this.carPrice = carPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("total_price")
    public String getTotalPrice() {
        return totalPrice;
    }
    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("down_payment")
    public Integer getDownPayment() {
        return downPayment;
    }
    public void setDownPayment(Integer downPayment) {
        this.downPayment = downPayment;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("billing_address")
    public BillingAddressDTO getBillingAddressDTO() {
        return billingAddressDTO;
    }
    public void setBillingAddressDTO(BillingAddressDTO billingAddressDTO) {
        this.billingAddressDTO = billingAddressDTO;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("financing_months")
    public Integer getFinancingMonths() {
        return financingMonths;
    }
    public void setFinancingMonths(Integer financingMonths) {
        this.financingMonths = financingMonths;
    }

    @JsonProperty("has_tradein_car")
    public boolean isHasTradeInCar() {
        return hasTradeInCar;
    }
    public void setHasTradeInCar(boolean hasTradeInCar) {
        this.hasTradeInCar = hasTradeInCar;
    }

    @JsonProperty("offer_checkpoint_id")
    public Long getOfferCheckpointId() {
        return offerCheckpointId;
    }
    public void setOfferCheckpointId(Long offerCheckpointId) {
        this.offerCheckpointId = offerCheckpointId;
    }
    
    @JsonProperty("source")
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("version_app")
    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }
    
    @JsonProperty("internal_user_id")
    public String getInternalUserId() {
        return internalUserId;
    }
    public void setInternalUserId(String internalUserId) {
        this.internalUserId = internalUserId;
    }
    
    @JsonProperty("disable_communications")
    public boolean isDisableCommunications() {
        return disableCommunications;
    }
    public void setDisableCommunications(boolean disableCommunications) {
        this.disableCommunications = disableCommunications;
    }
}
