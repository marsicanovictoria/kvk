package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InspectionCarFeatureDTO {

	private Long idFeature;
	private String featureLabel;
	
	@JsonProperty("feature_id")
	public Long getIdFeature() {
		return idFeature;
	}
	public void setIdFeature(Long idFeature) {
		this.idFeature = idFeature;
	}
	@JsonProperty("feature_label")
	public String getFeatureLabel() {
		return featureLabel;
	}
	public void setFeatureLabel(String labelFeature) {
		this.featureLabel = labelFeature;
	}
}
