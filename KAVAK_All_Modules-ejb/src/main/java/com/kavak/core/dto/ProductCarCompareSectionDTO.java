package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

/**
 * Created by Enrique on 09-Jun-17.
 */
public class ProductCarCompareSectionDTO {

    private String name;
    private String description;
    private String image;
    private String kavakPrice;
    private String marketPrice;
    private String savings;
    private Timestamp creationDate;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    @JsonProperty("kavakPrice")
    public String getKavakPrice() {
        return kavakPrice;
    }

    public void setKavakPrice(String kavakPrice) {
        this.kavakPrice = kavakPrice;
    }
    @JsonProperty("marketPrice")
    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }
    @JsonProperty("savings")
    public String getSavings() {
        return savings;
    }

    public void setSavings(String savings) {
        this.savings = savings;
    }
    @JsonProperty("creationDate")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
}
