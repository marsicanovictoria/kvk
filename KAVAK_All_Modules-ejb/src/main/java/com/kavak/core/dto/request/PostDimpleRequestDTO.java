package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostDimpleRequestDTO {
	
	private Long idInspection;
	private String inspectorEmail;
	private Long idDimpletype;
	private String dimpleName;
	private String coordinateX;
	private String coordinateY;
	private Long dimpleHeight;
	private Long dimpleWidth;
	private String dimpleImage;
	private int dimpleLocation;
	
	@JsonProperty("inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}

	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}
	@JsonProperty("inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}

	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	@JsonProperty("dimple_type_id")
	public Long getIdDimpletype() {
		return idDimpletype;
	}
	public void setIdDimpletype(Long idDimpletype) {
		this.idDimpletype = idDimpletype;
	}

	@JsonProperty("coordinate_dimple_x")
	public String getCoordinateX() {
		return coordinateX;
	}
	public void setCoordinateX(String coordinateX) {
		this.coordinateX = coordinateX;
	}

	@JsonProperty("coordinate_dimple_y")
	public String getCoordinateY() {
		return coordinateY;
	}
	public void setCoordinateY(String coordinateY) {
		this.coordinateY = coordinateY;
	}

	@JsonProperty("dimple_height")
	public Long getDimpleHeight() {
		return dimpleHeight;
	}
	public void setDimpleHeight(Long dimpleHeight) {
		this.dimpleHeight = dimpleHeight;
	}

	@JsonProperty("dimple_width")
	public Long getDimpleWidth() {
		return dimpleWidth;
	}

	public void setDimpleWidth(Long dimpleWidth) {
		this.dimpleWidth = dimpleWidth;
	}
	@JsonProperty("dimple_image")
	public String getDimpleImage() {
		return dimpleImage;
	}

	public void setDimpleImage(String dimpleImage) {
		this.dimpleImage = dimpleImage;
	}
	@JsonProperty("dimple_location")
	public int getDimpleLocation() {
		return dimpleLocation;
	}

	public void setDimpleLocation(int dimpleLocation) {
		this.dimpleLocation = dimpleLocation;
	}
	@JsonProperty("dimple_name")
	public String getDimpleName() {
		return dimpleName;
	}

	public void setDimpleName(String dimpleName) {
		this.dimpleName = dimpleName;
	}
	
}
