package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserAuthorizationPinCodeApiDataResponseDTO {

    	private Long idFinancing;
	private String type;
	private FinancingUserAuthorizationPinCodeApiAttributesResponseDTO attributes;

	
	@JsonProperty("id")
	public Long getIdFinancing() {
	    return idFinancing;
	}
	public void setIdFinancing(Long idFinancing) {
	    this.idFinancing = idFinancing;
	}
	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("attributes")
	public FinancingUserAuthorizationPinCodeApiAttributesResponseDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserAuthorizationPinCodeApiAttributesResponseDTO attributes) {
	    this.attributes = attributes;
	}


}