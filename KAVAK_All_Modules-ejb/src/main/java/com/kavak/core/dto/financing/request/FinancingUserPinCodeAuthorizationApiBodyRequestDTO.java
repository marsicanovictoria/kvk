package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserPinCodeAuthorizationApiBodyRequestDTO {

    	private FinancingUserPinCodeAuthorizationApiDataRequestDTO data;

    	@JsonProperty("data")
	public FinancingUserPinCodeAuthorizationApiDataRequestDTO getData() {
	    return data;
	}
	public void setData(FinancingUserPinCodeAuthorizationApiDataRequestDTO data) {
	    this.data = data;
	}

}
