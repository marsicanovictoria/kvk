package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetaValueDomicileDTO {

	
	private Long propertyStatusId; 
	private String yearsInHouse; 
	private String postalCode; 
	private String delegation;
	private String state;
	private String city;
	private String colony;
	private String street;
	private String exteriorNumber; 
	private String interiorNumber; 
	
    @JsonProperty("property_status")
	public Long getPropertyStatusId() {
		return propertyStatusId;
	}
	public void setPropertyStatusId(Long propertyStatusId) {
		this.propertyStatusId = propertyStatusId;
	}
    @JsonProperty("years_in_house")
	public String getYearsInHouse() {
		return yearsInHouse;
	}
	public void setYearsInHouse(String yearsInHouse) {
		this.yearsInHouse = yearsInHouse;
	}
    @JsonProperty("postal_code")
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
    @JsonProperty("delegation")
	public String getDelegation() {
		return delegation;
	}
	public void setDelegation(String delegation) {
		this.delegation = delegation;
	}
    @JsonProperty("state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
    @JsonProperty("city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
    @JsonProperty("colony")
	public String getColony() {
		return colony;
	}
	public void setColony(String colony) {
		this.colony = colony;
	}
    @JsonProperty("street")
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
    @JsonProperty("exterior_number")
	public String getExteriorNumber() {
		return exteriorNumber;
	}
	public void setExteriorNumber(String exteriorNumber) {
		this.exteriorNumber = exteriorNumber;
	}
    @JsonProperty("interior_number")
	public String getInteriorNumber() {
		return interiorNumber;
	}
	public void setInteriorNumber(String interiorNumber) {
		this.interiorNumber = interiorNumber;
	}
	
}
