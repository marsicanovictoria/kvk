package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.enumeration.CatalogueCarStatusEnum;
import com.kavak.core.model.SellCarDimple;
import com.kavak.core.model.SellCarInsurance;
import com.kavak.core.model.SellCarWarranty;

import java.util.List;


public class SellCarDetailDTO {
	
	private Long id;
	private String carKm;
	private Long price;
	private String carYear;
	private String carMake;
	private String carModel;
	private String carTrim;
	private String sku;
	private boolean certified;
	private boolean valide;
	private boolean active;
	private boolean sale;
	private SellCarMetaDTO sellCarMetaDTO;
	private List<SaleCarOrderDTO> saleCarOrdersDTO;
	private String carColor;
	private String uberType;
	private List<String> listImagesCar;
	private List<SellCarWarranty> listSellCarWarranty;
	private List<SellCarInsurance> listSellCarInsurance;
	private List<SellCarDimple> listSellCarDimple;
	private String transmission;
	private String carPackage;
    private String whyCarText;
    private String whyCarAudio;
    private Long isWishlist;
	private Long netsuiteItemId;
	private CatalogueCarStatusEnum status;
	private Long sendNetsuite;
	private Long deliveryDate;
	private String plateState;
	private String shortUrl;
	private Long userId;
	private Long fakeBooking;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("km")
	public String getCarKm() {
		return carKm;
	}
	public void setCarKm(String carKm) {
		this.carKm = carKm;
	}
	@JsonProperty("price")
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	@JsonProperty("year")
	public String getCarYear() {
		return carYear;
	}
	public void setCarYear(String carYear) {
		this.carYear = carYear;
	}
	@JsonProperty("package")
	public String getCarPackage() {
		return carPackage;
	}
	public void setCarPackage(String carPackage) {
		this.carPackage = carPackage;
	}
	@JsonProperty("make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}
	@JsonProperty("model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	@JsonProperty("trim")
	public String getCarTrim() {
		return carTrim;
	}
	public void setCarTrim(String carTrim) {
		this.carTrim = carTrim;
	}
	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	@JsonProperty("certified")
	public boolean isCertified() {
		return certified;
	}
	public void setCertified(boolean certified) {
		this.certified = certified;
	}
	@JsonProperty("valide")
	public boolean isValide() {
		return valide;
	}
	public void setValide(boolean valide) {
		this.valide = valide;
	}
	@JsonProperty("active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@JsonProperty("sale")
	public boolean isSale() {
		return sale;
	}
	public void setSale(boolean sale) {
		this.sale = sale;
	}
	@JsonProperty("car_meta")
	public SellCarMetaDTO getSellCarMetaDTO() {
		return sellCarMetaDTO;
	}
	public void setSellCarMetaDTO(SellCarMetaDTO sellCarMetaDTO) {
		this.sellCarMetaDTO = sellCarMetaDTO;
	}
	@JsonProperty("sale_car_order")
	public List<SaleCarOrderDTO> getSaleCarOrdersDTO() {
		return saleCarOrdersDTO;
	}
	public void setSaleCarOrdersDTO(List<SaleCarOrderDTO> saleCarOrdersDTO) {
		this.saleCarOrdersDTO = saleCarOrdersDTO;
	}
	@JsonProperty("color")
	public String getCarColor() {
		return carColor;
	}
	public void setCarColor(String carColor) {
		this.carColor = carColor;
	}
	@JsonProperty("uber_type")
	public String getUberType() {
		return uberType;
	}
	public void setUberType(String uberType) {
		this.uberType = uberType;
	}
	@JsonProperty("media")
	public List<String> getListImagesCar() {
		return listImagesCar;
	}
	public void setListImagesCar(List<String> listImagesCar) {
		this.listImagesCar = listImagesCar;
	}
	@JsonProperty("warranty")
	public List<SellCarWarranty> getListSellCarWarranty() {
		return listSellCarWarranty;
	}
	public void setListSellCarWarranty(List<SellCarWarranty> listSellCarWarranty) {
		this.listSellCarWarranty = listSellCarWarranty;
	}
	@JsonProperty("insurance")
	public List<SellCarInsurance> getListSellCarInsurance() {
		return listSellCarInsurance;
	}
	public void setListSellCarInsurance(List<SellCarInsurance> listSellCarInsurance) {
		this.listSellCarInsurance = listSellCarInsurance;
	}
	@JsonProperty("car_dimple")
	public List<SellCarDimple> getListSellCarDimple() {
		return listSellCarDimple;
	}
	public void setListSellCarDimple(List<SellCarDimple> listSellCarDimple) {
		this.listSellCarDimple = listSellCarDimple;
	}

	@JsonProperty("transmission")
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

    @JsonProperty("why_car")
    public String getWhyCarText() {
        return whyCarText;
    }
    public void setWhyCarText(String whyCarText) {
        this.whyCarText = whyCarText;
    }

    @JsonProperty("why_car_audio")
    public String getWhyCarAudio() {
        return whyCarAudio;
    }
    public void setWhyCarAudio(String whyCarAudio) {
        this.whyCarAudio = whyCarAudio;
    }

    @JsonProperty("is_wishlist")
    public Long isWishlist() {
		return isWishlist;
	}
	public void setWishlist(Long isWishlist) {
		this.isWishlist = isWishlist;
	}
	@JsonProperty("netsuite_item_id")
	public Long getNetsuiteItemId() {
		return netsuiteItemId;
	}
	public void setNetsuiteItemId(Long netsuiteItemId) {
		this.netsuiteItemId = netsuiteItemId;
	}
	  
    @JsonProperty("status")
	public CatalogueCarStatusEnum getStatus() {
		return status;
	}
	public void setStatus(CatalogueCarStatusEnum status) {
		this.status = status;
	}
	
	@JsonProperty("send_netsuite")
	public Long getSendNetsuite() {
		return sendNetsuite;
	}
	public void setSendNetsuite(Long sendNetsuite) {
		this.sendNetsuite = sendNetsuite;
	}
	
	@JsonProperty("delivery_days")
	public Long getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Long deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	@JsonProperty("estado_placas")
	public String getPlateState() {
		return plateState;
	}
	public void setPlateState(String plateState) {
		this.plateState = plateState;
	}
	
	@JsonProperty("short_url")
	public String getShortUrl() {
	    return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
	    this.shortUrl = shortUrl;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@JsonProperty("fake_booking")
	public Long getFakeBooking() {
		return fakeBooking;
	}
	public void setFakeBooking(Long fakeBooking) {
		this.fakeBooking = fakeBooking;
	}

}