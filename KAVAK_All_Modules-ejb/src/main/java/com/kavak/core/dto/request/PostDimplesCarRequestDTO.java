package com.kavak.core.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.DimplesDTO;

import java.util.List;

public class PostDimplesCarRequestDTO {

    private String moment;
    private List<DimplesDTO> dimplesDTOS;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("moment")
    public String getMoment() {
        return moment;
    }

    public void setMoment(String moment) {
        this.moment = moment;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("dimples")
    public List<DimplesDTO> getDimplesDTOS() {
        return dimplesDTOS;
    }

    public void setDimplesDTOS(List<DimplesDTO> dimplesDTOS) {
        this.dimplesDTOS = dimplesDTOS;
    }
}
