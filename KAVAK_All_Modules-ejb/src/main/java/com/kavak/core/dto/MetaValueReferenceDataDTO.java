package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetaValueReferenceDataDTO {

    private ReferenceDTO familiarReferenceDTO;
    private ReferenceDTO personalReferenceDTO;

    @JsonProperty("familiar_reference")
    public ReferenceDTO getFamiliarReferenceDTO() {
        return familiarReferenceDTO;
    }
    public void setFamiliarReferenceDTO(ReferenceDTO familiarReferenceDTO) {
        this.familiarReferenceDTO = familiarReferenceDTO;
    }

    @JsonProperty("personal_reference")
    public ReferenceDTO getPersonalReferenceDTO() {
        return personalReferenceDTO;
    }
    public void setPersonalReferenceDTO(ReferenceDTO personalReferenceDTO) {
        this.personalReferenceDTO = personalReferenceDTO;
    }
}
