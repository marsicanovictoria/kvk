package com.kavak.core.dto.financing.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserRfcApiDataRequestDTO {

	private String type;
	private FinancingUserRfcApiAttributesRequestDTO attributes;

	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("attributes")
	public FinancingUserRfcApiAttributesRequestDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserRfcApiAttributesRequestDTO attributes) {
	    this.attributes = attributes;
	}

}