package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingPostFinancingUserPinCodeResponseDTO {

	private String status;


	@JsonProperty("status")
	public String getStatus() {
	    return status;
	}

	public void setStatus(String status) {
	    this.status = status;
	}


}