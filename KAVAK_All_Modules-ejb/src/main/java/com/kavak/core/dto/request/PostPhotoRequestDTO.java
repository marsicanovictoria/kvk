package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostPhotoRequestDTO {

	private Long idInspection;
	private String inspectorEmail;
	private String imageName;
	private String image;

	@JsonProperty("inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}
	@JsonProperty("inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	@JsonProperty("image_name")
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	@JsonProperty("image")
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
}
