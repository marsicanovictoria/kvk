package com.kavak.core.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PostCustomerOpportunityCommentRequestDTO {

    private String opportunityType;
    private Long checkpointId;
    private String comment;
    private String voice;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("opportunity_type")
    public String getOpportunityType() {
        return opportunityType;
    }

    public void setOpportunityType(String opportunityType) {
        this.opportunityType = opportunityType;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("checkpoint_id")
    public Long getCheckpointId() {
        return checkpointId;
    }

    public void setCheckpointId(Long checkpointId) {
        this.checkpointId = checkpointId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("voice")
    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }
}
