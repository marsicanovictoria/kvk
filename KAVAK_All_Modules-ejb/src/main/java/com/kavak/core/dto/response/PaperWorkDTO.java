package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaperWorkDTO {

	private Long id;
	private String car;
	private Long paperworkStatusId;
	private String paperworkStatusDescription;
	private String deliveryDate;
	private String netsuiteURL;
	private String currentPlateStatus;
	private String finalPlateStatus;
	private Long paperworkTypeId;
	private String paperworkTypeDescription;
	private String shippingAddress;
	private String managerShipping;
	private String tentativeDeliveryDate;
	private String startedDate;
	private String emailNumberCode;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("car_name")
	public String getCar() {
		return car;
	}
	public void setCar(String car) {
		this.car = car;
	}
	
	@JsonProperty("paperwork_status_id")
	public Long getPaperworkStatusId() {
		return paperworkStatusId;
	}
	public void setPaperworkStatusId(Long paperworkStatusId) {
		this.paperworkStatusId = paperworkStatusId;
	}
	
	@JsonProperty("paperwork_status_description")
	public String getPaperworkStatusDescription() {
		return paperworkStatusDescription;
	}
	public void setPaperworkStatusDescription(String paperworkStatusDescription) {
		this.paperworkStatusDescription = paperworkStatusDescription;
	}
	@JsonProperty("delivery_date")
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	@JsonProperty("netsuite_url")
	public String getNetsuiteURL() {
		return netsuiteURL;
	}
	public void setNetsuiteURL(String netsuiteURL) {
		this.netsuiteURL = netsuiteURL;
	}
	
	@JsonProperty("current_plate_status")
	public String getCurrentPlateStatus() {
		return currentPlateStatus;
	}
	public void setCurrentPlateStatus(String currentPlateStatus) {
		this.currentPlateStatus = currentPlateStatus;
	}
	
	@JsonProperty("final_plate_status")
	public String getFinalPlateStatus() {
		return finalPlateStatus;
	}
	public void setFinalPlateStatus(String finalPlateStatus) {
		this.finalPlateStatus = finalPlateStatus;
	}
	
	@JsonProperty("paperwork_type_id")
	public Long getPaperworkTypeId() {
		return paperworkTypeId;
	}
	public void setPaperworkTypeId(Long paperworkTypeId) {
		this.paperworkTypeId = paperworkTypeId;
	}
	@JsonProperty("paperwork_type_description")
	public String getPaperworkTypeDescription() {
		return paperworkTypeDescription;
	}
	public void setPaperworkType(String paperworkTypeDescription) {
		this.paperworkTypeDescription = paperworkTypeDescription;
	}
	
	@JsonProperty("shipping_address")
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	
	@JsonProperty("manager_shipping")
	public String getManagerShipping() {
		return managerShipping;
	}
	public void setManagerShipping(String managerShipping) {
		this.managerShipping = managerShipping;
	}
	
	@JsonProperty("tentative_delivery_date")
	public String getTentativeDeliveryDate() {
		return tentativeDeliveryDate;
	}
	public void setTentativeDeliveryDate(String tentativeDeliveryDate) {
		this.tentativeDeliveryDate = tentativeDeliveryDate;
	}
	
	@JsonProperty("started_date")
	public String getStartedDate() {
		return startedDate;
	}
	public void setStartedDate(String startedDate) {
		this.startedDate = startedDate;
	}
	@JsonProperty("email_number_code")
	public String getEmailNumberCode() {
		return emailNumberCode;
	}
	public void setEmailNumberCode(String emailNumberCode) {
		this.emailNumberCode = emailNumberCode;
	}
	
}
