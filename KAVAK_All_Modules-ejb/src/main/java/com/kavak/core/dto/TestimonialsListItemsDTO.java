package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TestimonialsListItemsDTO {

	private String question;
	private String answer;
	
	@JsonProperty("question")
	public String getQuestion() {
	    return question;
	}
	public void setQuestion(String question) {
	    this.question = question;
	}
	
	@JsonProperty("answer")
	public String getAnswer() {
	    return answer;
	}
	public void setAnswer(String answer) {
	    this.answer = answer;
	}

	
}
