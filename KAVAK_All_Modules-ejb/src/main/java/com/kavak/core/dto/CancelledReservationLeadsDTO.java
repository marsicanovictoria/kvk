package com.kavak.core.dto;


import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CancelledReservationLeadsDTO {

	private Long id;
	private Timestamp creationDate;
	private String source;
	private UserDTO customer;
	private CarDataDTO car;
	
    @JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
    @JsonProperty("creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
    @JsonProperty("source")
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
    @JsonProperty("customer")
	public UserDTO getCustomer() {
		return customer;
	}
	public void setCustomer(UserDTO customer) {
		this.customer = customer;
	}
	
    @JsonProperty("car")
	public CarDataDTO getCar() {
		return car;
	}
	public void setCar(CarDataDTO car) {
		this.car = car;
	}
	
	
	
}
