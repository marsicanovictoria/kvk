package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostInspectionOverviewResponseDTO {
	
	private Long idInspection;

	@JsonProperty("inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}

	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}
}
