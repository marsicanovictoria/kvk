package com.kavak.core.dto.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;
import com.kavak.core.enumeration.CatalogueCarStatusEnum;

public class CatalogueCarResponseDTO {
	
	private Long id;
	private String carName;
	private String carMake;
	private String carModel;
	private String carTrim;
	private String carKm;
	private String carYear;
	private String transmission;
	private String traction;
	private String bodyType;
	private String carColor;
	private String doors;
	private String seats;
	private String passengers;
	private String cylinder;
	private String engine;
	private String fuel;
	private String horsepower;
	private String uber;
	private Long price;
	private Double marketPrice;
	private Double savings;
	private Double fepm;
	private Long downpayment;
	private String imageUrl;
	private List<String> listImageUrl;
	private String avgFuelConsumption;
	private CatalogueCarStatusEnum status;
	private String filterStatus;
	private Long promotionPrice;
	private String promotionName;
	private String promotionColor;
	private String dateDelivery;
	private List<GenericDTO> benefits;
	private String interiorColor;
	private String locationFilter;

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("car_name")
	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}

	@JsonProperty("km")
	public String getCarKm() {
		return carKm;
	}
	public void setCarKm(String carKm) {
		this.carKm = carKm;
	}

	@JsonProperty("car_year")
	public String getCarYear() {
		return carYear;
	}
	public void setCarYear(String carYear) {
		this.carYear = carYear;
	}

	@JsonProperty("transmission")
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	@JsonProperty("price")
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}

	@JsonProperty("market_price")
	public Double getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}

	@JsonProperty("savings")
	public Double getSavings() {
		return savings;
	}
	public void setSavings(Double savings) {
		this.savings = savings;
	}

	@JsonProperty("fepm")
	public Double getFepm() {
		return fepm;
	}
	public void setFepm(Double fepm) {
		this.fepm = fepm;
	}

	@JsonProperty("image_url")
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@JsonProperty("status")
	public CatalogueCarStatusEnum getStatus() {
		return status;
	}
	public void setStatus(CatalogueCarStatusEnum status) {
		this.status = status;
	}

	@JsonProperty("car_make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}

	@JsonProperty("car_model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	@JsonProperty("car_trim")
	public String getCarTrim() {
		return carTrim;
	}
	public void setCarTrim(String carTrim) {
		this.carTrim = carTrim;
	}

	@JsonProperty("traction")
	public String getTraction() {
		return traction;
	}
	public void setTraction(String traction) {
		this.traction = traction;
	}

	@JsonProperty("body_type")
	public String getBodyType() {
		return bodyType;
	}
	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	@JsonProperty("ext_color")
	public String getCarColor() {
		return carColor;
	}
	public void setCarColor(String carColor) {
		this.carColor = carColor;
	}

	@JsonProperty("interior_color")
	public String getInteriorColor() {
	    return interiorColor;
	}
	public void setInteriorColor(String interiorColor) {
	    this.interiorColor = interiorColor;
	}

	@JsonProperty("doors")
	public String getDoors() {
		return doors;
	}
	public void setDoors(String doors) {
		this.doors = doors;
	}

	@JsonProperty("seats")
	public String getSeats() {
		return seats;
	}
	public void setSeats(String seats) {
		this.seats = seats;
	}

	@JsonProperty("cylinder")
	public String getCylinder() {
		return cylinder;
	}
	public void setCylinder(String cylinder) {
		this.cylinder = cylinder;
	}
	
	@JsonProperty("engine")
	public String getEngine() {
	    return engine;
	}
	public void setEngine(String engine) {
	    this.engine = engine;
	}

	@JsonProperty("fuel_type")
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}

	@JsonProperty("uber_type")
	public String getUber() {
		return uber;
	}
	public void setUber(String uber) {
		this.uber = uber;
	}

	@JsonProperty("avg_fuel_consumption")
	public String getAvgFuelConsumption() {
		return avgFuelConsumption;
	}
	public void setAvgFuelConsumption(String avgFuelConsumption) {
		this.avgFuelConsumption = avgFuelConsumption;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("promotion_price")
	public Long getPromotionPrice() {
		return promotionPrice;
	}
	public void setPromotionPrice(Long promotionPrice) {
		this.promotionPrice = promotionPrice;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("promotion_name")
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("promotion_color")
	public String getPromotionColor() {
		return promotionColor;
	}
	public void setPromotionColor(String promotionColor) {
		this.promotionColor = promotionColor;
	}
	
	@JsonProperty("date_delivery")
	public String getDateDelivery() {
		return dateDelivery;
	}
	public void setDateDelivery(String dateDelivery) {
		this.dateDelivery = dateDelivery;
	}

	
	@JsonProperty("horsepower")
	public String getHorsepower() {
		return horsepower;
	}
	public void setHorsepower(String horsepower) {
		this.horsepower = horsepower;
	}
	
	@JsonProperty("image_url_list")
	public List<String> getListImageUrl() {
	    return listImageUrl;
	}
	public void setListImageUrl(List<String> listImageUrl) {
	    this.listImageUrl = listImageUrl;
	}
	
	@JsonProperty("passengers")
	public String getPassengers() {
	    return passengers;
	}
	public void setPassengers(String passengers) {
	    this.passengers = passengers;
	}
	
	@JsonProperty("downpayment")
	public Long getDownpayment() {
	    return downpayment;
	}
	public void setDownpayment(Long downpayment) {
	    this.downpayment = downpayment;
	}
	
	@JsonProperty("filter_status")
	public String getFilterStatus() {
	    return filterStatus;
	}
	public void setFilterStatus(String filterStatus) {
	    this.filterStatus = filterStatus;
	}

	@JsonProperty("benefits")
	public List<GenericDTO> getBenefits() {
	    return benefits;
	}
	public void setBenefits(List<GenericDTO> benefits) {
	    this.benefits = benefits;
	}

	@JsonProperty("location_filter")
	public String getLocationFilter() {
	    return locationFilter;
	}
	public void setLocationFilter(String locationFilter) {
	    this.locationFilter = locationFilter;
	}
	
	





}