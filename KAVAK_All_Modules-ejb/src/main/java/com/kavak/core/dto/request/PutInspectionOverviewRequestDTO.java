package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.InspectionOverviewDTO;

import java.util.List;

public class PutInspectionOverviewRequestDTO {

	private List<InspectionOverviewDTO> listInspectionOverviewDTO;

	@JsonProperty("inspection_overviews")
	public List<InspectionOverviewDTO> getListInspectionOverviewDTO() {
		return listInspectionOverviewDTO;
	}

	public void setListInspectionOverviewDTO(List<InspectionOverviewDTO> listInspectionOverviewDTO) {
		this.listInspectionOverviewDTO = listInspectionOverviewDTO;
	}
}
