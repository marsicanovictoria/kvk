package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientDTO {

	private Long userId;
	private Long carId;
	private Long checkpointId;
	private String hookAmount;
	private String financingAmount;
	private FinancingPerMonthDTO financingPerMonth;
	private Long creditStatus; 
	private Long personType; 
	private String birthday;
	private String name;
	private String firstLastname; 
	private String secondLastname; 
	private String email;
	private String mobilePhone; 
	private String homePhone; 
	private String identificationType; 
	private String electorKey; 
	private String idNumber; 
	private Long educationLevel; 
	private Long maritalStatusId; 
	private Long nationalityId;
	private String gender;
	private String dependents;
	private String isBancomerCustomer;
	private String rfc;

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@JsonProperty("car_id")
	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	@JsonProperty("checkpoint_id")
	public Long getCheckpointId() {
		return checkpointId;
	}

	public void setCheckpointId(Long checkpointId) {
		this.checkpointId = checkpointId;
	}

	@JsonProperty("hook_amount")
	public String getHookAmount() {
		return hookAmount;
	}

	public void setHookAmount(String hookAmount) {
		this.hookAmount = hookAmount;
	}

	@JsonProperty("financing_amount")
	public String getFinancingAmount() {
		return financingAmount;
	}

	public void setFinancingAmount(String financingAmount) {
		this.financingAmount = financingAmount;
	}

	@JsonProperty("financing_per_month")
	public FinancingPerMonthDTO getFinancingPerMonth() {
		return financingPerMonth;
	}

	public void setFinancingPerMonth(FinancingPerMonthDTO financingPerMonth) {
		this.financingPerMonth = financingPerMonth;
	}

	@JsonProperty("credit_status")
	public Long getCreditStatus() {
		return creditStatus;
	}

	public void setCreditStatus(Long creditStatus) {
		this.creditStatus = creditStatus;
	}

	@JsonProperty("person_type")
	public Long getPersonType() {
		return personType;
	}

	public void setPersonType(Long personType) {
		this.personType = personType;
	}

	@JsonProperty("birthday")
	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("first_lastname")
	public String getFirstLastname() {
		return firstLastname;
	}

	public void setFirstLastname(String firstLastname) {
		this.firstLastname = firstLastname;
	}

	@JsonProperty("second_lastname")
	public String getSecondLastname() {
		return secondLastname;
	}

	public void setSecondLastname(String secondLastname) {
		this.secondLastname = secondLastname;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("mobile_phone")
	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	@JsonProperty("home_phone")
	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	@JsonProperty("identification_type")
	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	@JsonProperty("elector_key")
	public String getElectorKey() {
		return electorKey;
	}

	public void setElectorKey(String electorKey) {
		this.electorKey = electorKey;
	}

	@JsonProperty("id_number")
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	@JsonProperty("education_level")
	public Long getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(Long educationLevel) {
		this.educationLevel = educationLevel;
	}

	@JsonProperty("marital_status")
	public Long getMaritalStatusId() {
		return maritalStatusId;
	}

	public void setMaritalStatusId(Long maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}

	@JsonProperty("nationality")
	public Long getNationalityId() {
		return nationalityId;
	}

	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}

	@JsonProperty("gender")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@JsonProperty("dependents")
	public String getDependents() {
		return dependents;
	}


	public void setDependents(String dependents) {
		this.dependents = dependents;
	}

	@JsonProperty("is_bancomer_customer")
	public String getIsBancomerCustomer() {
		return isBancomerCustomer;
	}

	public void setIsBancomerCustomer(String isBancomerCustomer) {
		this.isBancomerCustomer = isBancomerCustomer;
	}

	@JsonProperty("rfc")
	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

}
