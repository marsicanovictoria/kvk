package com.kavak.core.dto.financing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateApiDataDTO {

    	//private Long idFinancing;
	private String type;
	private FinancingUserUpdateApiAttributesDTO attributes;

//	@JsonProperty("id")
//	public Long getIdFinancing() {
//	    return idFinancing;
//	}
//	public void setIdFinancing(Long idFinancing) {
//	    this.idFinancing = idFinancing;
//	}
	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("attributes")
	public FinancingUserUpdateApiAttributesDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserUpdateApiAttributesDTO attributes) {
	    this.attributes = attributes;
	}


}