package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserRegisterApiDataResponseDTO {

    	private Long idFinancing;
    	//private String idFinancing;
	private String type;
	private FinancingUserRegisterApiAttributesResponseDTO attributes;

	
	@JsonProperty("id")
	public Long getIdFinancing() {
	    return idFinancing;
	}
	public void setIdFinancing(Long idFinancing) {
	    this.idFinancing = idFinancing;
	}
//	@JsonProperty("type")
//	public String getIdFinancing() {
//	    return idFinancing;
//	}
//	public void setIdFinancing(String idFinancing) {
//	    this.idFinancing = idFinancing;
//	}
	
	@JsonProperty("type")
	public String getType() {
	    return type;
	}
	public void setType(String type) {
	    this.type = type;
	}
	
	@JsonProperty("attributes")
	public FinancingUserRegisterApiAttributesResponseDTO getAttributes() {
	    return attributes;
	}
	public void setAttributes(FinancingUserRegisterApiAttributesResponseDTO attributes) {
	    this.attributes = attributes;
	}

}