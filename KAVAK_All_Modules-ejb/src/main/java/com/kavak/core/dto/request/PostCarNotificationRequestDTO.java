package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostCarNotificationRequestDTO {

	private Long userId;
	private Long carId;
	private String notificationType;
	private String source;
	private String internalUserId;
	private boolean disableCommunications;

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@JsonProperty("car_id")
	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	@JsonProperty("notification_type")
	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	@JsonProperty("source")
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@JsonProperty("internal_user_id")
	public String getInternalUserId() {
	    return internalUserId;
	}

	public void setInternalUserId(String internalUserId) {
	    this.internalUserId = internalUserId;
	}

	@JsonProperty("disable_communications")
	public boolean isDisableCommunications() {
	    return disableCommunications;
	}

	public void setDisableCommunications(boolean disableCommunications) {
	    this.disableCommunications = disableCommunications;
	}

}
