package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.GenericDTO;

import java.util.ArrayList;
import java.util.List;

public class GetPaymentTypesResponseDTO {

    List<GenericDTO> listGenericDTOPaymentTypes = new ArrayList<>();

    @JsonProperty("payment_types")
    public List<GenericDTO> getListGenericDTOPaymentTypes() {
        return listGenericDTOPaymentTypes;
    }

    public void setListGenericDTOPaymentTypes(List<GenericDTO> listGenericDTOPaymentTypes) {
        this.listGenericDTOPaymentTypes = listGenericDTOPaymentTypes;
    }
}
