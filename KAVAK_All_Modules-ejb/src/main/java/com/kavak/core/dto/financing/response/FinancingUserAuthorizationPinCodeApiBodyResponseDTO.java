package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserAuthorizationPinCodeApiBodyResponseDTO {

    	private FinancingUserAuthorizationPinCodeApiDataResponseDTO data;

    	
    	@JsonProperty("data")
	public FinancingUserAuthorizationPinCodeApiDataResponseDTO getData() {
	    return data;
	}

	public void setData(FinancingUserAuthorizationPinCodeApiDataResponseDTO data) {
	    this.data = data;
	}

}
