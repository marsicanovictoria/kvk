package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingVerifyUserUpdatePersonalDataApiResponseDTO {

    private FinancingUserUpdateApiDataResponseDTO data;

    
    @JsonProperty("data")
    public FinancingUserUpdateApiDataResponseDTO getData() {
        return data;
    }

    public void setData(FinancingUserUpdateApiDataResponseDTO data) {
        this.data = data;
    }

}