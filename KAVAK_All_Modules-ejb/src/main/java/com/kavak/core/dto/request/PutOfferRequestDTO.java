package com.kavak.core.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PutOfferRequestDTO {
	
	private Long step;
	private Long typeOfferId;
	private String street;
	private String exterior_number;
	private String interior_number;
	private String zip;
	private String municipality;
	private String colony;
	private Long idInspectionLocation;
	private String inspectionDate;
	private Long idHourInspectionSlot;
	private String hourInspectionSlotText;
	private String circulationCard;
	private String carBill;
	private String carPhoto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("type_offer_id")
	public Long getTypeOfferId() {
		return typeOfferId;
	}
	public void setTypeOfferId(Long typeOfferId) {
		this.typeOfferId = typeOfferId;
	}

	@JsonProperty("step")
	public Long getStep() {
		return step;
	}
	public void setStep(Long step) {
		this.step = step;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("street")
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("exterior_number")
	public String getExterior_number() {
		return exterior_number;
	}
	public void setExterior_number(String exterior_number) {
		this.exterior_number = exterior_number;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("interior_number")
	public String getInterior_number() {
		return interior_number;
	}
	public void setInterior_number(String interior_number) {
		this.interior_number = interior_number;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("postal_code")
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("municipality")
	public String getMunicipality() {
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("colony")
	public String getColony() {
		return colony;
	}
	public void setColony(String colony) {
		this.colony = colony;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("inspection_location_id")
	public Long getIdInspectionLocation() {
		return idInspectionLocation;
	}
	public void setIdInspectionLocation(Long idInspectionLocation) {
		this.idInspectionLocation = idInspectionLocation;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("inspection_date")
	public String getInspectionDate() {
		return inspectionDate;
	}
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("hour_inspection_slot_id")
	public Long getIdHourInspectionSlot() {
		return idHourInspectionSlot;
	}
	public void setIdHourInspectionSlot(Long idHourInspectionSlot) {
		this.idHourInspectionSlot = idHourInspectionSlot;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("hour_inspection_slot_text")
	public String getHourInspectionSlotText() {
		return hourInspectionSlotText;
	}
	public void setHourInspectionSlotText(String hourInspectionSlotText) {
		this.hourInspectionSlotText = hourInspectionSlotText;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("circulation_card")
	public String getCirculationCard() {
		return circulationCard;
	}
	public void setCirculationCard(String circulationCard) {
		this.circulationCard = circulationCard;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("car_bill")
	public String getCarBill() {
		return carBill;
	}
	public void setCarBill(String carBill) {
		this.carBill = carBill;
	}

    @JsonProperty("car_photo")
    public String getCarPhoto() {
        return carPhoto;
    }
    public void setCarPhoto(String carPhoto) {
        this.carPhoto = carPhoto;
    }
}

