package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.SectionDTO;

import java.util.List;

public class GetWhyKavakResponseDTO {

    private List<SectionDTO> listSectionDTO;
    private Long totalSavings;

    @JsonProperty("sections")
    public List<SectionDTO> getListSectionDTO() {
        return listSectionDTO;
    }

    public void setListSectionDTO(List<SectionDTO> listSectionDTO) {
        this.listSectionDTO = listSectionDTO;
    }
    @JsonProperty("total_savings")
    public Long getTotalSavings() {
        return totalSavings;
    }
    public void setTotalSavings(Long totalSavings) {
        this.totalSavings = totalSavings;
    }
}
