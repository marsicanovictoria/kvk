package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

public class UserDTO {

	private Long id;
	private String name;
	private String username;
    private String lastName;
    private String firstName;
	private String email;
	private String password;
	private String role;
	private boolean active;
	private Timestamp creationDate;
	private String ipUsuario;
	private String phone;
	private String address;
	private String googlePlusId;
	private String facebookUserId;
	private String socialNetworkToken;
	// private List<Inspection> inspections;

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonProperty("role")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@JsonProperty("is_active")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@JsonProperty("creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@JsonProperty("ip_usuario")
	public String getIpUsuario() {
		return ipUsuario;
	}

	public void setIpUsuario(String ipUsuario) {
		this.ipUsuario = ipUsuario;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("google_plus_user_id")
	public String getgooglePlusId() {
		return googlePlusId;
	}

	public void setgooglePlusId(String googlePlusId) {
		this.googlePlusId = googlePlusId;
	}

	@JsonProperty("facebook_user_id")
	public String getFacebookUserId() {
		return facebookUserId;
	}

	public void setFacebookUserId(String facebookUserId) {
		this.facebookUserId = facebookUserId;
	}

	@JsonProperty("social_network_token")
	public String getSocialNetworkToken() {
		return socialNetworkToken;
	}

	public void setSocialNetworkToken(String socialNetworkToken) {
		this.socialNetworkToken = socialNetworkToken;
	}

	@JsonProperty("first_name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	// @JsonProperty("inspections")
	// public List<Inspection> getInspections() {
	// return inspections;
	// }
	// public void setInspections(List<Inspection> inspections) {
	// this.inspections = inspections;
	// }
}