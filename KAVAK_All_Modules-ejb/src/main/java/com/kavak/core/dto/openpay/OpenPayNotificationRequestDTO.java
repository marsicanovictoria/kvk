package com.kavak.core.dto.openpay;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OpenPayNotificationRequestDTO {

	private String type;
	private String evenDate;
	private String verificationCode;
//	private OpenPayTransaction transactionOpenPay;
	
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	@JsonProperty("event_date")
	public String getEvenDate() {
		return evenDate;
	}
	
	public void setEvenDate(String evenDate) {
		this.evenDate = evenDate;
	}
	@JsonProperty("verification_code")
	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

//	@JsonProperty("transaction")
//	public OpenPayTransaction getTransactionOpenPay() {
//		return transactionOpenPay;
//	}
//
//	public void setTransactionOpenPay(OpenPayTransaction transactionOpenPay) {
//		this.transactionOpenPay = transactionOpenPay;
//	}
	


}