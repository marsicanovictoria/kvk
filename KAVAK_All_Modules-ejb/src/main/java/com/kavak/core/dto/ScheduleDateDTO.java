package com.kavak.core.dto;

public class ScheduleDateDTO {

	private Long id;
	private String visitDate;
	private Long hourSlot;
	private String hourSlotDescription;
	private Long status;
	private String selectedDate;
	private Long carId;
	private Long userId;
	private String visitAddress;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}
	public Long getHourSlot() {
		return hourSlot;
	}
	public void setHourSlot(Long hourSlot) {
		this.hourSlot = hourSlot;
	}
	public String getHourSlotDescription() {
		
		if (getHourSlot() != null){
			
			long hourSlotLong = getHourSlot();
			int hourSlot = (int) hourSlotLong;
			
	        switch (hourSlot) {
	        case 1:  hourSlotDescription = "8am a 9am";
	                 break;
	        case 2:  hourSlotDescription = "9am a 10am";
	                 break;
	        case 3:  hourSlotDescription = "10am a 11am";
	                 break;
	        case 4:  hourSlotDescription = "11am a 12pm";
	                 break;
	        case 5:  hourSlotDescription = "12pm a 1pm";
	                 break;
	        case 6:  hourSlotDescription = "1pm a 2pm";
	                 break;
	        case 7:  hourSlotDescription = "2pm a 3pm";
	                 break;
	        case 8:  hourSlotDescription = "3pm a 4pm";
	                 break;
	        case 9:  hourSlotDescription = "4pm a 5pm";
	                 break;
	        case 10: hourSlotDescription = "5pm a 6pm";
	                 break;
	        case 11: hourSlotDescription = "6pm a 7pm";
	                 break;
	        default: hourSlotDescription = "8am a 9am";
	                 break;
	        }
		}
		
		return hourSlotDescription;
	}
	
	public void setHourSlotDescription(String hourSlotDescription) {
		this.hourSlotDescription = hourSlotDescription;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	public String getSelectedDate() {
		return selectedDate;
	}
	public void setSelectedDate(String selectedDate) {
		this.selectedDate = selectedDate;
	}
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getVisitAddress() {
		return visitAddress;
	}
	public void setVisitAddress(String visitAddress) {
		this.visitAddress = visitAddress;
	}
	
	
	
	
	
}
