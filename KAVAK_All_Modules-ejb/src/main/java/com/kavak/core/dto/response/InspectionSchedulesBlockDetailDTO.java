package com.kavak.core.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InspectionSchedulesBlockDetailDTO {
	
	private Long id;
	private Long hourBlock;
	private boolean status;
	private String block;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("available")
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@JsonProperty("block")
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	@JsonProperty("hour_block")
	public Long getHourBlock() {
		return hourBlock;
	}
	public void setHourBlock(Long hourBlock) {
		this.hourBlock = hourBlock;
	}
}
