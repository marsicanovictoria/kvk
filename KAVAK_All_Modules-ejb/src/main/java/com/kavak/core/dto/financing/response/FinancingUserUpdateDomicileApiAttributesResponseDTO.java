package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateDomicileApiAttributesResponseDTO {

    private String suburb;
    private String internalNumber;
    private String externalNumber;
    private String cityCouncil;
    private String zipCode;
    private String state;
    private String residentYears;
    private String livingType;
    private String updatedAt;
    
    @JsonProperty("suburb")
    public String getSuburb() {
        return suburb;
    }
    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }
    
    @JsonProperty("internalNumber")
    public String getInternalNumber() {
        return internalNumber;
    }
    public void setInternalNumber(String internalNumber) {
        this.internalNumber = internalNumber;
    }
    
    @JsonProperty("externalNumber")
    public String getExternalNumber() {
        return externalNumber;
    }
    public void setExternalNumber(String externalNumber) {
        this.externalNumber = externalNumber;
    }
    
    @JsonProperty("cityCouncil")
    public String getCityCouncil() {
        return cityCouncil;
    }
    public void setCityCouncil(String cityCouncil) {
        this.cityCouncil = cityCouncil;
    }
    
    @JsonProperty("zipCode")
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    
    @JsonProperty("state")
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    
    @JsonProperty("residentYears")
    public String getResidentYears() {
        return residentYears;
    }
    public void setResidentYears(String residentYears) {
        this.residentYears = residentYears;
    }
    
    @JsonProperty("livingType")
    public String getLivingType() {
        return livingType;
    }
    public void setLivingType(String livingType) {
        this.livingType = livingType;
    }
    
    @JsonProperty("updatedAt")
    public String getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}