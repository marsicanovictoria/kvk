package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RepairTypeDTO {
	
	private Long id;
	private String name;
	private Long repairAmount;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("repair_amount")
	public Long getRepairAmount() {
		return repairAmount;
	}
	public void setRepairAmount(Long repairAmount) {
		this.repairAmount = repairAmount;
	}
}