package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserEscoreProfileResponseDTO {

	private String id;
	private String name;
	private String content;
	private String interestRate;
	private String downpayment;
	private String active;
	
	@JsonProperty("id")
	public String getId() {
	    return id;
	}
	public void setId(String id) {
	    this.id = id;
	}
	
	@JsonProperty("name")
	public String getName() {
	    return name;
	}
	public void setName(String name) {
	    this.name = name;
	}
	
	@JsonProperty("content")
	public String getContent() {
	    return content;
	}
	public void setContent(String content) {
	    this.content = content;
	}
	
	@JsonProperty("interest_rate")
	public String getInterestRate() {
	    return interestRate;
	}
	public void setInterestRate(String interestRate) {
	    this.interestRate = interestRate;
	}
	
	@JsonProperty("downpayment")
	public String getDownpayment() {
	    return downpayment;
	}
	public void setDownpayment(String downpayment) {
	    this.downpayment = downpayment;
	}
	
	@JsonProperty("is_active")
	public String getActive() {
	    return active;
	}
	public void setActive(String active) {
	    this.active = active;
	}

}