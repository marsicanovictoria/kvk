package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InspectionDimpleDTO {

    private Long idDimple;
    private Long inspectionId;
    private String inspectorEmail;
    //	private Long dimpleHeight;
//	private Long dimpleWidth;
//	private Double coordinateDimpleX;
//	private Double coordinateDimpleY;
//	private String photoInspection;
    private String dimpleImageLocation;
    private int dimpleLocation;
    private Long idInspectionPointCar;
    private Long idDimpleType;
    private String dimpleName;
    private String coordinateX;
    private String coordinateY;
    private Long originalHeight;
    private Long originalWidth;
    private String dimpleImage;

    @JsonProperty("dimple_id")
    public Long getIdDimple() {
        return idDimple;
    }
    public void setIdDimple(Long idDimple) {
        this.idDimple = idDimple;
    }

    @JsonProperty("inspection_id")
    public Long getInspectionId() {
        return inspectionId;
    }
    public void setInspectionId(Long inspectionId) {
        this.inspectionId = inspectionId;
    }

    @JsonProperty("inspector_email")
    public String getInspectorEmail() {
        return inspectorEmail;
    }
    public void setInspectorEmail(String inspectorEmail) {
        this.inspectorEmail = inspectorEmail;
    }


    //	@JsonProperty("dimple_height")
//	public Long getDimpleHeight() {
//		return dimpleHeight;
//	}
//	public void setDimpleHeight(Long dimpleHeight) {
//		this.dimpleHeight = dimpleHeight;
//	}
//	@JsonProperty("dimple_width")
//	public Long getDimpleWidth() {
//		return dimpleWidth;
//	}
//	public void setDimpleWidth(Long dimpleWidth) {
//		this.dimpleWidth = dimpleWidth;
//	}
//	@JsonProperty("id_coordinate_dimple_x")
//	public Double getCoordinateDimpleX() {
//		return coordinateDimpleX;
//	}
//	public void setCoordinateDimpleX(Double coordinateDimpleX) {
//		this.coordinateDimpleX = coordinateDimpleX;
//	}
//	@JsonProperty("id_coordinate_dimple_y")
//	public Double getCoordinateDimpleY() {
//		return coordinateDimpleY;
//	}
//	public void setCoordinateDimpleY(Double coordinateDimpleY) {
//		this.coordinateDimpleY = coordinateDimpleY;
//	}
//	@JsonProperty("photo")
//	public String getPhotoInspection() {
//		return photoInspection;
//	}
//	public void setPhotoInspection(String photoInspection) {
//		this.photoInspection = photoInspection;
//	}
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("dimple_image_location")
    public String getDimpleImageLocation() {
        return dimpleImageLocation;
    }

    public void setDimpleImageLocation(String dimpleImageLocation) {
        this.dimpleImageLocation = dimpleImageLocation;
    }

    @JsonProperty("dimple_location")
    public int getDimpleLocation() {
        return dimpleLocation;
    }
    public void setDimpleLocation(int dimpleLocation) {
        this.dimpleLocation = dimpleLocation;
    }

    @JsonProperty("inspection_point_car_id")
    public Long getIdInspectionPointCar() {
        return idInspectionPointCar;
    }
    public void setIdInspectionPointCar(Long idInspectionPointCar) {
        this.idInspectionPointCar = idInspectionPointCar;
    }

    @JsonProperty("dimple_type_id")
    public Long getIdDimpleType() {
        return idDimpleType;
    }
    public void setIdDimpleType(Long idDimpleType) {
        this.idDimpleType = idDimpleType;
    }

    @JsonProperty("dimple_name")
    public String getDimpleName() {
        return dimpleName;
    }
    public void setDimpleName(String dimpleName) {
        this.dimpleName = dimpleName;
    }

    @JsonProperty("coordinate_x")
    public String getCoordinateX() {
        return coordinateX;
    }
    public void setCoordinateX(String coordinateX) {
        this.coordinateX = coordinateX;
    }

    @JsonProperty("coordinate_y")
    public String getCoordinateY() {
        return coordinateY;
    }
    public void setCoordinateY(String coordinateY) {
        this.coordinateY = coordinateY;
    }

    @JsonProperty("dimple_image")
    public String getDimpleImage() {
        return dimpleImage;
    }
    public void setDimpleImage(String dimpleImage) {
        this.dimpleImage = dimpleImage;
    }

    @JsonProperty("original_height")
    public Long getOriginalHeight() {
        return originalHeight;
    }
    public void setOriginalHeight(Long originalHeight) {
        this.originalHeight = originalHeight;
    }

    @JsonProperty("original_width")
    public Long getOriginalWidth() {
        return originalWidth;
    }
    public void setOriginalWidth(Long originalWidth) {
        this.originalWidth = originalWidth;
    }
}
