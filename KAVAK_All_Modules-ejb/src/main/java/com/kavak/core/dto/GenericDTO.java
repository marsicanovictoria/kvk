package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Comparator;
import java.util.List;

public class GenericDTO implements Comparable<GenericDTO> {

    private Long id;
    private String name;
    private Object content;
    private String typeOffer;
    private String codeOffer;
    private Object min;
    private Object max;
    private Object imageMake;
    private Object hexadecimal;
    private String coverageWarranty;
    private Object amount;
    private String url;
    private String message;
    private String value;
    private String text;
    private Boolean checked;
    private String descrption;
    private Long categoryId;
    private String imageUrl;
    private Long notificationId;
    private Long count;
    private Long alertId;
    private Object alerts;
    private String description;
    private String phone;
    private String email;
    private List<String> urlImages;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("min")
    public Object getMin() {
        return min;
    }

    public void setMin(Object min) {
        this.min = min;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("max")
    public Object getMax() {
        return max;
    }

    public void setMax(Object max) {
        this.max = max;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("image")
    public Object getImageMake() {
        return imageMake;
    }

    public void setImageMake(Object imageMake) {
        this.imageMake = imageMake;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("hexadecimal")
    public Object getHexadecimal() {
        return hexadecimal;
    }

    public void setHexadecimal(Object hexadecimal) {
        this.hexadecimal = hexadecimal;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("type")
    public String getTypeOffer() {
        return typeOffer;
    }

    public void setTypeOffer(String typeOffer) {
        this.typeOffer = typeOffer;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("code_offer")
    public String getCodeOffer() {
        return codeOffer;
    }

    public void setCodeOffer(String codeOffer) {
        this.codeOffer = codeOffer;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("content")
    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("coverage")
    public String getCoverageWarranty() {
        return coverageWarranty;
    }

    public void setCoverageWarranty(String coverageWarranty) {
        this.coverageWarranty = coverageWarranty;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("amount")
    public Object getAmount() {
        return amount;
    }

    public void setAmount(Object amount) {
        this.amount = amount;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("is_checked")
    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("descrption")
    public String getDescrption() {
        return descrption;
    }

    public void setDescrption(String descrption) {
        this.descrption = descrption;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("category_id")
    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("notification_id")
    public Long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("count")
    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("alert_id")
    public Long getAlertId() {
        return alertId;
    }

    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("alerts")
    public Object getAlerts() {
        return alerts;
    }

    public void setAlerts(Object alerts) {
        this.alerts = alerts;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int compareTo(GenericDTO other) {
        return categoryId.compareTo(other.categoryId);
    }

    public static Comparator<GenericDTO> COMPARE_BY_NAME = new Comparator<GenericDTO>() {
        public int compare(GenericDTO one, GenericDTO other) {
            return other.name.compareTo(one.name);
        }
    };

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("url_images")
    public List<String> getUrlImages() {
        return urlImages;
    }

    public void setUrlImages(List<String> urlImages) {
        this.urlImages = urlImages;
    }
}