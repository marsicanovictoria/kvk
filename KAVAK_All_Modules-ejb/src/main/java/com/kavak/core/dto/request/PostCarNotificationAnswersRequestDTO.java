package com.kavak.core.dto.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kavak.core.dto.AnswerDTO;

public class PostCarNotificationAnswersRequestDTO {

	private Long userId;
	private Long carId;
	private List<AnswerDTO> answers;
	

	@JsonProperty("user_id")
	public Long getUserId() {
	    return userId;
	}
	public void setUserId(Long userId) {
	    this.userId = userId;
	}
	
	@JsonProperty("car_id")
	public Long getCarId() {
	    return carId;
	}
	public void setCarId(Long carId) {
	    this.carId = carId;
	}
	
	@JsonProperty("answers")
	public List<AnswerDTO> getAnswers() {
	    return answers;
	}
	public void setAnswers(List<AnswerDTO> answers) {
	    this.answers = answers;
	}
	










}
