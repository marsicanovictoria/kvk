package com.kavak.core.dto.financing.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancingUserUpdateIncomeApiResponseDTO {

    private FinancingUserUpdateIncomeApiDataResponseDTO data;

    
    @JsonProperty("data")
    public FinancingUserUpdateIncomeApiDataResponseDTO getData() {
        return data;
    }

    public void setData(FinancingUserUpdateIncomeApiDataResponseDTO data) {
        this.data = data;
    }

}