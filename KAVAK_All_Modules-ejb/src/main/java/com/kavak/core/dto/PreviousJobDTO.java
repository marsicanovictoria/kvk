package com.kavak.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PreviousJobDTO {

	private String companyPreviousJob; // company
	private String yearOld; // "years_worked": "4",
	private String monthsWorked; // months_worked": "3",
	private String companyPhone; // company_phone": "12321321"

	@JsonProperty("company")
	public String getCompanyPreviousJob() {
		return companyPreviousJob;
	}

	public void setCompanyPreviousJob(String companyPreviousJob) {
		this.companyPreviousJob = companyPreviousJob;
	}

	@JsonProperty("years_worked")
	public String getYearOld() {
		return yearOld;
	}

	public void setYearOld(String yearOld) {
		this.yearOld = yearOld;
	}

	@JsonProperty("months_worked")
	public String getMonthsWorked() {
		return monthsWorked;
	}

	public void setMonthsWorked(String monthsWorked) {
		this.monthsWorked = monthsWorked;
	}

	@JsonProperty("company_phone")
	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

}
