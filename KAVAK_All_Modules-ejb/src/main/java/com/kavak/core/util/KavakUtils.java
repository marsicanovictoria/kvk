package com.kavak.core.util;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.ImageIcon;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.client.RestTemplate;

import com.kavak.core.dto.MessageDTO;
import com.kavak.core.dto.SellCarDetailDTO;
import com.kavak.core.dto.UserDTO;
import com.kavak.core.dto.response.FacebookResponseDTO;
import com.kavak.core.dto.response.GoogleResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.dto.response.UserRequestDTO;
import com.kavak.core.enumeration.CatalogueCarStatusEnum;
import com.kavak.core.enumeration.EndPointCodeResponseEnum;
import com.kavak.core.model.CarData;
import com.kavak.core.model.SaleCarOrder;
import com.kavak.core.model.SellCarDetail;

public class KavakUtils {

    private static KavakUtils kavakUtilsInstance = null;
    private static JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
    private static DatatypeConverter datatypeConverter;
    private static final int BUFFER_SIZE = 4096;

    // Existe solo para fingir inicializacion
    protected KavakUtils() {
    }

    /**
     * Implementando el patron Singleton en KavakUtils
     * 
     * @author Enrique Marin
     */
    public static KavakUtils getInstance() {
		if (kavakUtilsInstance == null) {
		    kavakUtilsInstance = new KavakUtils();
		}
		return kavakUtilsInstance;
    }

    /**
     * Ensambla en nombre del carro
     * 
     * @author Enrique Marin
     * @param sellcarDetail
     * @return String nombre del carro armado correctamente
     */
    public static String getFriendlyCarName(SellCarDetailDTO sellcarDetail) {
		String carName, carVersion;
		String[] modelSplit = sellcarDetail.getCarModel().split(" ");
		carVersion = sellcarDetail.getCarTrim().split("##")[0];
	
		if ((modelSplit[0].trim()).equalsIgnoreCase(sellcarDetail.getCarMake().trim())) {
		    carName = WordUtils.capitalize(sellcarDetail.getCarModel().trim()) + " " + WordUtils.capitalize(carVersion.trim());
		} else {
		    carName = sellcarDetail.getCarMake() + " " + WordUtils.capitalize(sellcarDetail.getCarModel().trim()) + " " + WordUtils.capitalize(carVersion.trim());
		}
		return carName;
    }

    /**
     * Ensambla en nombre del carro
     * 
     * @author Juan Tolentino
     * @param carData
     * @return String nombre del carro armado correctamente
     */
    public static String getFriendlyCarNameData(CarData carData) {
    	String carName, carVersion;
		String[] modelSplit = carData.getCarModel().split(" ");
		String model = carData.getCarModel();
		carVersion = carData.getCarTrim();
	
		if ((modelSplit[0].trim()).equalsIgnoreCase(carData.getCarMake().trim()) || model.equalsIgnoreCase(carData.getCarMake().trim())) {
		    if (model.equalsIgnoreCase(carData.getCarTrim().trim())) {
			carName = WordUtils.capitalize(carData.getCarModel().trim());
		    } else {
			carName = WordUtils.capitalize(carData.getCarModel().trim()) + " " + WordUtils.capitalize(carVersion.trim());
		    }
		} else {
		    if (model.equalsIgnoreCase(carData.getCarTrim().trim())) {
			carName = carData.getCarMake() + " " + WordUtils.capitalize(carData.getCarModel().trim());
		    } else {
			carName = carData.getCarMake() + " " + WordUtils.capitalize(carData.getCarModel().trim()) + " " + WordUtils.capitalize(carVersion.trim());
		    }
		}
		return carName;
    }

    /**
     * Genera una imagen usando el base64 recibido en imgStr y lo envia a la ruta URL_FULL_PATH_FOLDER_ASSEST
     * 
     * @author Enrique Marin
     * @param fileString
     *            base64 de la imagen a procesar
     * @param fileName
     *            nombre de la imagen
     * @param resizeImage
     *            indicador de que hace falta crear la imagen en varios formatos
     * @return boolean indicador si se genero la imagen o no
     */
    @SuppressWarnings("static-access")
    public static boolean generateFile(String fileString, String fileName, String filePath, String fileType, boolean resizeImage) {

	try {
	    byte[] fileBytes = null;
	    switch (fileType.toLowerCase()) {
	    
	    case Constants.FILE_TYPE_JPG:
			String base64Jpg = fileString.split(",")[1];
			fileBytes = datatypeConverter.parseBase64Binary(base64Jpg);
			if (filePath != null) {
			    try {
				Files.write(Paths.get(filePath + fileName + "." + Constants.FILE_TYPE_JPG), fileBytes);
			    } catch (IOException e) {
				e.printStackTrace();
			    }
			} else {
			    try {
				Files.write(Paths.get(Constants.URL_FULL_PATH_FOLDER_ASSEST + fileName + "." + Constants.FILE_TYPE_JPG), fileBytes);
			    } catch (IOException e) {
				e.printStackTrace();
			    }
			}
			break;
	    case Constants.FILE_TYPE_PDF:
			String base64Pdf = fileString.split(",")[1];
			try {
			    fileBytes = datatypeConverter.parseBase64Binary(base64Pdf);
			    Path path = Paths.get(filePath + fileName + "." + Constants.FILE_TYPE_PDF);
			    Files.write(path, fileBytes);
			} catch (IOException e) {
			    e.printStackTrace();
			}
			break;
	    case Constants.FILE_TYPE_PNG:
			String base64Png = fileString.split(",")[1];
			fileBytes = datatypeConverter.parseBase64Binary(base64Png);
			try {
			    Files.write(Paths.get(filePath + fileName + "." + Constants.FILE_TYPE_PNG), fileBytes);
			} catch (IOException e) {
			    e.printStackTrace();
			}
			break;
	    case Constants.FILE_TYPE_M4A:
			fileBytes = datatypeConverter.parseBase64Binary(fileString);
			try {
			    Files.write(Paths.get(filePath + fileName + "." + Constants.FILE_TYPE_M4A), fileBytes);
			} catch (IOException e) {
			    e.printStackTrace();
			}
			break;
	    default:
	    	break;
	    }

	    // FileOutputStream fop = new FileOutputStream(outputfile);

	    // Files.write(fileBytes);
	    // fop.flush();
	    // fop.close();

	    if (resizeImage) {
		// Slider
		File sliderOutputfile = new File(Constants.URL_FULL_PATH_FOLDER_ASSEST + Constants.SLIDER + fileName);
		BufferedImage sliderImg = scaleImage(1920, 1100, Constants.URL_FULL_PATH_FOLDER_ASSEST + fileName);
		ImageIO.write(sliderImg, "jpg", sliderOutputfile);

		// Grid
		File gridOutputfile = new File(Constants.URL_FULL_PATH_FOLDER_ASSEST + Constants.GRID + fileName);
		BufferedImage gridImg = scaleImage(540, 310, Constants.URL_FULL_PATH_FOLDER_ASSEST + fileName);
		ImageIO.write(gridImg, "jpg", gridOutputfile);

		// List
		File listOutputfile = new File(Constants.URL_FULL_PATH_FOLDER_ASSEST + Constants.LIST + fileName);
		BufferedImage listImg = scaleImage(1080, 620, Constants.URL_FULL_PATH_FOLDER_ASSEST + fileName);
		ImageIO.write(listImg, "jpg", listOutputfile);

		// Thumb
		File thumbOutputfile = new File(Constants.URL_FULL_PATH_FOLDER_ASSEST + Constants.THUMB + fileName);
		BufferedImage thumbImg = scaleImage(240, 160, Constants.URL_FULL_PATH_FOLDER_ASSEST + fileName);
		ImageIO.write(thumbImg, "jpg", thumbOutputfile);
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    return false;
	}
	return true;
    }

    public static BufferedImage scaleImage(int WIDTH, int HEIGHT, String filename) {
		BufferedImage bi = null;
		try {
		    ImageIcon ii = new ImageIcon(filename);// path to image
		    bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		    Graphics2D g2d = (Graphics2D) bi.createGraphics();
		    g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
		    g2d.drawImage(ii.getImage(), 0, 0, WIDTH, HEIGHT, null);
		} catch (Exception e) {
		    e.printStackTrace();
		    return null;
		}
		return bi;
    }

    /**
     * Transforma un valor String ("$123,456") a el valor numerico sin decimales (123456)
     * 
     * @author Enrique Marin
     * @param indianValue
     *            Valor con el formato creato por los indus
     * @return Long numero sin caracteres
     */
    public static Long getRealNumber(String indianValue) {
		String noDollar = indianValue.substring(1, indianValue.length());
		Long fixNumber = Long.valueOf(noDollar.replace(",", ""));
		return fixNumber;
    }

    /**
     * Elimina los acentos de un String
     * 
     * @author Enrique Marin
     * @param stringAccent
     *            Valor con el formato creato por los indus
     * @return Long numero sin caracteres
     */
    public static String getStrinNoAccent(String stringAccent) {
    	String normalized = Normalizer.normalize(stringAccent, Normalizer.Form.NFD);
		String valueNoAccent = normalized.replaceAll("[^\\p{ASCII}]", "");
		return valueNoAccent;
    }

    /**
     * Valida el status del carro acorde con las condiciones del negocio 11/07/2017
     * 
     * @author Enrique Marin
     * @param sellCarDetail
     *            datos del carro a verificar
     * @return CatalogueCarStatusEnum status del Carro
     */
    public static CatalogueCarStatusEnum getStatusCar(SellCarDetail sellCarDetail) {
		// PreCargado >> is_valide = 0, is_sale = 0, is_complete = 0,
		// is_new_arrival = 0
		// New Arrival >> is_valide = 1, is_sale = 0, is_complete = 0,
		// is_new_arrival = 1
	
		if (!sellCarDetail.isValide()) {
		    return CatalogueCarStatusEnum.PRELOADED;
		} else if (!sellCarDetail.isSale()) {
		    if (sellCarDetail.getIsNewArrival().equalsIgnoreCase("yes")) {
		    	return CatalogueCarStatusEnum.NEW_ARRIVAL;
		    } else {
		    	return CatalogueCarStatusEnum.AVAILABLE;
		    }
		} else if (sellCarDetail.getListSaleCarOrders() != null) {
		    boolean saleCarOrderComplete = false;
		    for (SaleCarOrder saleCarOrderActual : sellCarDetail.getListSaleCarOrders()) {
		    	if (saleCarOrderActual.isComplete() && !saleCarOrderActual.isCanceled()) {
		    		saleCarOrderComplete = true;
		    	}
		    }
		    
		    if (saleCarOrderComplete) {
		    	return CatalogueCarStatusEnum.SOLD;
		    } else {
		    	return CatalogueCarStatusEnum.BOOKED;
		    }
		}

		return CatalogueCarStatusEnum.STATUS_NA;
    }

    /**
     * Metodo para enviar un email acorde a 2 plantillas 1 - base_email_template_kavak(Plantilla Base) 2 - templateName(Plantilla con la data acorde al email)
     * 
     * @author Enrique Marin
     * @param emailTo
     *            email del destinatario
     * @param subjectData
     *            data necesaria para generar el subject
     * @param templateName
     *            plantilla especifica al email necesitado
     * @param dataEmail
     *            la info de la keys(@CAR_NAME) de la plantilla
     * @return Sended indicando si se envio el email o no
     * @throws MessagingException
     */

    public static boolean sendEmail(String emailTo, List<String> emailCc, HashMap<String, Object> subjectData, String templateName, String templateNameBase, HashMap<String, Object> dataEmail, InternetAddress emailFrom, List<String> emailReplyTo) throws MessagingException {

		boolean sended = false;
	
		// Se configuran las propiedades del correo
		Properties props = javaMailSenderImpl.getJavaMailProperties();
		props.setProperty("mail.transport.protocol", Constants.PROPERTY_EMAIL_PROTOCOL);
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.debug", "false");
		props.setProperty("mail.smtp.starttls.enable", "true");
		// props.setProperty("mail.mime.charset", "UTF-8");
		props.setProperty("mail.pop3.connectionpooltimeout", Constants.PROPERTY_EMAIL_TIMEOUT);
		props.setProperty("mail.pop3.connectiontimeout", Constants.PROPERTY_EMAIL_TIMEOUT);
		props.setProperty("mail.pop3.timeout", Constants.PROPERTY_EMAIL_TIMEOUT);
	
		javaMailSenderImpl.setUsername(Constants.PROPERTY_EMAIL_USERNAME);
		javaMailSenderImpl.setPassword(Constants.PROPERTY_EMAIL_PASSWORD);
		javaMailSenderImpl.setHost(Constants.PROPERTY_EMAIL_HOST);
		javaMailSenderImpl.setPort(Constants.PROPERTY_EMAIL_PORT);
		javaMailSenderImpl.setDefaultEncoding("UTF-8");
		javaMailSenderImpl.setJavaMailProperties(props);

		// Se genera un correo con la propiedades ya planteadas
		MimeMessage email = javaMailSenderImpl.createMimeMessage();
		MimeMessageHelper helperEmail = new MimeMessageHelper(email, true, "UTF-8");
	
		ArrayList<String> tempFiles = null;
		String downloadedFile = "";
		String completeUrl = "";
	
		try {
		    helperEmail.setTo(emailTo);
		    if (!emailCc.isEmpty()) {
		    	for (String cc : emailCc) {
			 	  helperEmail.addBcc(cc);
		    	}
		    }
	
		    if (emailReplyTo != null && !emailReplyTo.isEmpty()) {
		    	for (String reply : emailReplyTo) {
		    		helperEmail.setReplyTo(reply);
		    	}
		    }

		    LogService.logger.debug("KavakUtils > Template a utilizar en este correo: " + templateName);
	
		    InputStream htmlBasic = getInstance().getClass().getResourceAsStream("/templates/base_email_template_kavak.html");
		    InputStream htmlIntern = getInstance().getClass().getResourceAsStream("/templates/" + templateName);
	
		    // Sobreescribimos la base del template en caso de que se haya enviado una en el método.
		    if (templateNameBase != null && !templateNameBase.isEmpty()) {
		    	LogService.logger.debug("Se esta definiendo la plantilla base del correo publico: " + templateNameBase);
		    	htmlBasic = getInstance().getClass().getResourceAsStream(templateNameBase);
		    }

		    // Create a temporal directory
		    if (dataEmail.get(Constants.ATTACHMENT_FILES) != null) {
	
		    	String rootUrl = "/home/kavak/KAVAK_Resources/temp/";
	
				if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
				    rootUrl = "/Users/antonycasanova/Desktop/temp/";
				}
	
				Long temporalFolder = System.currentTimeMillis();
				completeUrl = rootUrl + temporalFolder.toString();
		    }
		    
		    try {

			String stringHtmlBasic = readFromInputStream(htmlBasic);
				String stringHtmlIntern = readFromInputStream(htmlIntern);
		
				stringHtmlBasic = stringHtmlBasic.replace(Constants.KEY_EMAIL_MESSAGE, stringHtmlIntern);
				if (dataEmail != null) {
				    for (String keyActual : dataEmail.keySet()) {
				    	if (dataEmail.get(keyActual) != null) {
				    		stringHtmlBasic = stringHtmlBasic.replace(keyActual, dataEmail.get(keyActual).toString());
				    	}
				    }
				}
	
				// Mapea de data dentro del Subject
				String subject = subjectData.get(Constants.KEY_SUBJECT).toString();
				if (subjectData.keySet().size() > 1) {
				    for (String keyDataSubjectActual : subjectData.keySet()) {
					subject = subject.replace(keyDataSubjectActual, subjectData.get(keyDataSubjectActual).toString());
				    }
				}
	
				// Check if Object has attachment files
				if (dataEmail.get(Constants.ATTACHMENT_FILES) != null) {
		
				    FileSystemResource file = null;
				    tempFiles = new ArrayList<>();
				    @SuppressWarnings("unchecked")
				    ArrayList<String> attachmentList = (ArrayList<String>) dataEmail.get(Constants.ATTACHMENT_FILES);
		
				    for (String attachment : attachmentList) {
		
				    	File attachmentFile = new File(attachment);
		
				    	if (attachmentFile.exists() && !attachmentFile.isDirectory()) {
		
				    		file = new FileSystemResource(new File(attachment));
				    		helperEmail.addAttachment(file.getFilename(), file);
		
				    		// If file is into a temp folder, it must be add to "tempFiles" in order to delete it later...
				    		if (dataEmail.get(Constants.ATTACHMENT_FILES_TEMP_FOLDER) != null) {
				    			tempFiles.add(dataEmail.get(Constants.ATTACHMENT_FILES).toString().replace("[", "").replace("]", ""));
				    		}
	
				    	} else {
		
				    		// It is a external file, it must be local downloaded in order to attachment to email
				    		new File(completeUrl).mkdir();
				    		// LogService.logger.info("URL para descarga temporal: " + completeUrl + File.separator);
				    		downloadedFile = downloadFile(attachment, completeUrl + File.separator);
				    		// LogService.logger.info("Ruta y Archivo descargado: " + downloadedFile);
		
				    		if (!downloadedFile.isEmpty()) {
				    			file = new FileSystemResource(new File(downloadedFile));
				    			LogService.logger.info("attachment externo: " + downloadedFile);
				    			helperEmail.addAttachment(file.getFilename(), file);
				    		}
		
				    		tempFiles.add(downloadedFile);
				    	}
				    }
	
				} else {
					//LogService.logger.info("Desde el servicio sendInternalEmail este mensaje NO tiene adjuntos");
				}

				email.setSubject(subject, StandardCharsets.UTF_8.toString());
				helperEmail.setText(stringHtmlBasic, true);
			
				if (emailFrom != null) {
				    helperEmail.setFrom(emailFrom);
				} else {
				    helperEmail.setFrom(new InternetAddress("ayuda@kavak.com", "Hola Kavak"));
				}
	
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
	
		    javaMailSenderImpl.send(helperEmail.getMimeMessage());
		    sended = true;
	
		    // It must delete all temporal files and temporal directory
		    if (dataEmail.get(Constants.ATTACHMENT_FILES) != null) {
		    	if (sended == true) {
		    		if (!tempFiles.isEmpty()) {
	
		    			// Delete temporal files
		    			for (String file : tempFiles) {
		    				LogService.logger.info("Eliminando archivo temporal " + file);
		    				Path pathForDelete = Paths.get(file);
		    				deleteFile(pathForDelete);
		    			}
		    		}

		    		if (dataEmail.get(Constants.ATTACHMENT_FILES_TEMP_FOLDER) != null) {
		    			completeUrl = dataEmail.get(Constants.ATTACHMENT_FILES_TEMP_FOLDER).toString();
		    		}
	
		    		// Delete temporal directory
		    		LogService.logger.info("Eliminando carpeta temporal " + completeUrl);
		    		Path pathForDelete = Paths.get(completeUrl);
		    		deleteFile(pathForDelete);
		    	}
		    }

		} catch (MessagingException e) {
			e.printStackTrace();
			sended = false;
		}

		return sended;
    }

    public static boolean sendEmail(String emailTo, List<String> emailCc, HashMap<String, Object> subjectData, String templateName, String templateNameBase, HashMap<String, Object> dataEmail, List<String> emailReplyTo) throws MessagingException {
    	return sendEmail(emailTo, emailCc, subjectData, templateName, templateNameBase, dataEmail, null, emailReplyTo);
    }

    // Metodo interno de sendEmail para codificar a UTF-8 los html
    private static String readFromInputStream(InputStream inputStream) throws IOException {
		StringBuilder resultStringBuilder = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
		    String line;
		    while ((line = br.readLine()) != null) {
			resultStringBuilder.append(line).append("\n");
		    }
		}
		return resultStringBuilder.toString();
    }

    /**
     * Metodo que recibe el nombre de la imagen como esta en BD - select image From sell_car_detail y lo tranforma a la url entendible por el usuario
     * 
     * @author Enrique Marin
     * @param image
     *            nombre de la imagen
     * @return urlImage
     */
    public static String getFriendlyCarImage(String image) {
		String urlImage = Constants.URL_KAVAK + Constants.NAME_BASE_IMAGE_BD + image.split("#")[0];
		return urlImage;
    }

    /**
     * Retorna la url de la imagen del carro
     * 
     * @author Enrique Marin
     * @param car
     *
     * @return urlImage
     */
    public static String getFriendlyCarUrl(SellCarDetail car) {
		String carName = StringUtils.lowerCase(getFriendlyCarName(car.getDTO())).replace(" ", "_");
		String carUrl = Constants.URL_KAVAK + carName + "-compra-de-autos-" + car.getId();
		return carUrl;
    }

    /**
     * Metodo para darle formato a la fecha registro
     * 
     * @author Enrique Marin
     * @param dateIn
     *            fecha a darle formato
     * @return urlImage
     */
    public static Timestamp getDateOnFormat(Date dateIn) {
		Date parsedDate = null;
		try {
		    String strDate = sdf.format(dateIn);
		    parsedDate = sdf.parse(strDate);
	
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		return (new Timestamp(parsedDate.getTime()));
    }

    /**
     * Metodo que arma la respuesta para el caso de no data encontrada
     * 
     * @author Enrique Marin
     * @param messageDTO
     *            mensaje que indica especificamente que data no se encontro
     * @return ResponseDTO
     */
    public static ResponseDTO getResponseNoDataFound(MessageDTO messageDTO) {
		ResponseDTO responseDTO = new ResponseDTO();
		List<MessageDTO> listMessageDTO = new ArrayList<>();
		listMessageDTO.add(messageDTO);
		EndPointCodeResponseEnum enumResult = EndPointCodeResponseEnum.getByCode(EndPointCodeResponseEnum.C0400.toString());
		responseDTO.setCode(enumResult.getValue());
		responseDTO.setStatus(enumResult.getStatus());
		responseDTO.setListMessage(listMessageDTO);
		responseDTO.setData(new ArrayList<>());
		return responseDTO;
    }

    /**
     * Metodo para obtener el nombre completo
     * 
     * @author Enrique Marin
     * @param user
     * @return ResponseDTO
     */

    public static String getFriendlyUserName(UserDTO user) {
		String[] splitName = user.getName().split(" ");
		List<String> listNameComplete = Arrays.asList(splitName);
		String correctName = listNameComplete.get(0);

		switch (listNameComplete.size()) {
			case 1:
			    return correctName;
			case 2:
			    return correctName + " " + listNameComplete.get(1);
			case 3:
			    return correctName + " " + listNameComplete.get(1) + " " + listNameComplete.get(2);
			case 4:
			    return correctName + " " + listNameComplete.get(1) + " " + listNameComplete.get(2) + listNameComplete.get(3);
		}
		return correctName;
    }

    /**
     * Metodo para validar la estrusctura de un email
     * 
     * @author Oscar Montilla
     * @param email
     * @return Boolean (True= formato incorrecto, False= Formato correcto)
     */
    public static boolean validateEmail(String email) {

		if (email == null) {
		    return false;
		} else {
		    return (email.matches(Constants.PATTERN_EMAIL));
		}
    }

    /**
     * Metodo para validar una direccion IP
     * 
     * @author Oscar Montilla
     * @param ip
     * @return Boolean (True= formato incorrecto, False= Formato correcto)
     */

    public static boolean validIP(String ip) {
		try {
		    if (ip == null || ip.isEmpty()) {
		    	return false;
		    }
	
		    String[] parts = ip.split("\\.");
		    if (parts.length != 4) {
		    	return true;
		    }
	
		    for (String s : parts) {
				int i = Integer.parseInt(s);
				if ((i < 0) || (i > 255)) {
				    return true;
				}
		    }
		    if (ip.endsWith(".")) {
		    	return true;
		    }
	
		    return false;
		} catch (NumberFormatException nfe) {
		    return true;
		}
    }

    /**
     * Metodoque valida el token propornociado por la aplibación web para obtener los datos de usuario
     * 
     * @author Oscar Montilla
     * @param token
     *            para validar información de google+
     * @return UserRequesDTO Objeto con los datos de session de Google+
     */
    public static GoogleResponseDTO authGoogleLogin(String token) {
		GoogleResponseDTO googleResponseDTO = new GoogleResponseDTO();
	
		RestTemplate restTemplate = new RestTemplate();
	
		try {
		    ResponseEntity<GoogleResponseDTO> responseEntity = restTemplate.getForEntity("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + token, GoogleResponseDTO.class);
	
		    googleResponseDTO.setSub(responseEntity.getBody().getSub());
		    googleResponseDTO.setName(responseEntity.getBody().getName());
		    googleResponseDTO.setFamilyName(responseEntity.getBody().getFamilyName());
		    googleResponseDTO.setEmail(responseEntity.getBody().getEmail());
	
		} catch (Exception ex) {
		    return null;
		}
	
		return googleResponseDTO;
    }

    /**
     * Metodoque valida el token propornociado por la aplibación web para obtener los datos de usuario
     * 
     * @author Oscar Montilla
     * @param token
     *            para validar información de Facebook
     * @return facebookDTO Objeto con los datos de session de Facebook
     */
    public static FacebookResponseDTO authFacebookLogin(String token) {

	FacebookResponseDTO facebookResponseDTO = new FacebookResponseDTO();

	try {
	    String fields = "fields=id,name,email";
	    RestTemplate restTemplate = new RestTemplate();
	    facebookResponseDTO = restTemplate.getForObject("https://graph.facebook.com/v2.10/me?access_token=" + token + "&" + fields, FacebookResponseDTO.class);

	} catch (Exception ex) {
	    return null;
	}
	return facebookResponseDTO;
    }

    /**
     * Metodo para tranformar el Hash generado por BCrypt en un formato que Php lo pueda manipular, la tranformación se utiliza para consultar como para registrar una contraseña
     * 
     * @author Oscar Montilla
     * @param hash
     *            valor generado por BCrypt
     * @param condition
     *            condicional, 1 = tranformar Hash java a PHP, 2 = tranformar hash PHP a java solo si es necesario
     * @return hashConvert hash tranformado
     */
    public static String changeSaltInHash(String hash, Integer condition) {

	String hashConvert = null;

	if (condition == 1) {

	    hashConvert = hash.replaceFirst("2a", "2y");
	}

	if (condition == 2) {

	    if (hash.contains("$2y$")) {

		hashConvert = hash.replaceFirst("2y", "2a");

	    } else {

		hashConvert = hash;
	    }

	}

	return hashConvert;
    }

    public static String getSpanishDayOfWeek(Integer day) {
	
		switch (day) {
			case 2:
			    return Constants.MONDAY;
			case 3:
			    return Constants.TUESDAY;
			case 4:
			    return Constants.WEDNESDAY;
			case 5:
			    return Constants.THURSDAY;
			case 6:
			    return Constants.FRIDAY;
			case 7:
			    return Constants.SATURDAY;
			case 1:
			    return Constants.SUNDAY;
			default:
			    break;
			}
		return "";
    }

    /**
     * Operacion que retorna el detallado mensaje de error de la BD en caso q falle alguna trasaccion manejada fuera del ambiente de Spring / JPA
     *
     * @author Enrique Marin
     */
    public static MessageDTO getErrorCode(RuntimeException e) {
		MessageDTO messageSQLDTO = new MessageDTO();
		Throwable throwable = e;
		while (throwable != null && !(throwable instanceof SQLException)) {
		    throwable = throwable.getCause();
		}
		if (throwable instanceof SQLException) {
		    SQLException sqlex = (SQLException) throwable;
		    messageSQLDTO.setErrorCode(Integer.valueOf(sqlex.getErrorCode()));
		    messageSQLDTO.setApiMessage(sqlex.getMessage());
		    // errorSQLDTO.setMessage(sqlex.get);
		}
		return messageSQLDTO;
    }

    /**
     * Metodo que genera una constraseña dependiento de la longitud deseada
     * 
     * @author Oscar Montilla
     * @param length
     *            Longitud que se le desea asignar a la Contraseña
     * @return password constraseña generada
     */
    public static String generatePassword(int length) {

		SecureRandom random = new SecureRandom();
	
		String password = "";
		for (int i = 0; i < length; i++) {
		    int index = random.nextInt(Constants.DICTIONARY.length());
		    password += Constants.DICTIONARY.charAt(index);
		}
		return password;
    }

    /**
     * Metodo para enviar un email acorde a 2 plantillas 1 - base_email_template_kavak(Plantilla Base) 2 - templateName(Plantilla con la data acorde al email)
     * 
     * @author Antony Delgado
     * @param emailTo
     *            email del destinatario
     * @param subjectData
     *            data necesaria para generar el subject
     * @param templateName
     *            plantilla especifica al email necesitado
     * @param dataEmail
     *            la info de la keys(@CAR_NAME) de la plantilla
     * @return Sended indicando si se envio el email o no
     * @throws MessagingException
     */

    public static boolean sendInternalEmail(String emailTo, List<String> emailCc, HashMap<String, Object> subjectData, String templateName, String templateNameBase, HashMap<String, Object> dataEmail, InternetAddress emailFrom) throws MessagingException {
	boolean sended = false;

		// Se configuran las propiedades del correo
		Properties props = javaMailSenderImpl.getJavaMailProperties();
		props.setProperty("mail.transport.protocol", Constants.PROPERTY_EMAIL_PROTOCOL);
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.debug", "false");
		props.setProperty("mail.smtp.starttls.enable", "true");
		// props.setProperty("mail.mime.charset", "UTF-8");
		props.setProperty("mail.pop3.connectionpooltimeout", Constants.PROPERTY_EMAIL_TIMEOUT);
		props.setProperty("mail.pop3.connectiontimeout", Constants.PROPERTY_EMAIL_TIMEOUT);
		props.setProperty("mail.pop3.timeout", Constants.PROPERTY_EMAIL_TIMEOUT);
	
		javaMailSenderImpl.setUsername(Constants.PROPERTY_EMAIL_USERNAME);
		javaMailSenderImpl.setPassword(Constants.PROPERTY_EMAIL_PASSWORD);
		javaMailSenderImpl.setHost(Constants.PROPERTY_EMAIL_HOST);
		javaMailSenderImpl.setPort(Constants.PROPERTY_EMAIL_PORT);
		javaMailSenderImpl.setDefaultEncoding("UTF-8");
		javaMailSenderImpl.setJavaMailProperties(props);
	
		if (dataEmail.get(Constants.KEY_EMAIL_TEAM_KAVAK) == null) {
		    dataEmail.put(Constants.KEY_EMAIL_TEAM_KAVAK, "Team Kavak");
		}

		// Se genera un correo con la propiedades ya planteadas
		MimeMessage email = javaMailSenderImpl.createMimeMessage();
		// MimeMessageHelper helperEmail = new MimeMessageHelper(email);
		MimeMessageHelper helperEmail = new MimeMessageHelper(email, true, "UTF-8");
	
		ArrayList<String> tempFiles = null;
		String downloadedFile = "";
		String completeUrl = "";

		try {
	
		    LogService.logger.info("Correo interno TO " + emailTo);
		    LogService.logger.info("Correo interno FROM " + javaMailSenderImpl.getUsername());
	
		    helperEmail.setTo(emailTo);
		    if (emailCc != null && !emailCc.isEmpty()) {
				for (String cc : emailCc) {
				    helperEmail.addBcc(cc);
				}
		    }
	
		    InputStream htmlBasic = getInstance().getClass().getResourceAsStream("/templates/base_internal_email.html");
		    InputStream htmlIntern = getInstance().getClass().getResourceAsStream("/templates/" + templateName);

	    // Sobreescribimos la base del template en caso de que se haya enviado una en el método.
	    if (templateNameBase != null && !templateNameBase.isEmpty()) {
			LogService.logger.info("Se esta definiendo la plantilla base del correo interno: " + templateNameBase);
			htmlBasic = getInstance().getClass().getResourceAsStream(templateNameBase);
	    }

	    if (dataEmail.get(Constants.ATTACHMENT_FILES) != null) {

	    	String rootUrl = "/home/kavak/KAVAK_Resources/temp/";

			if (Constants.PROPERTY_ENVIRONMENT.equals(Constants.ENVIRONMENT_DEVELOPER)) {
			    rootUrl = "/Users/antonycasanova/Desktop/temp/";
			}

			Long temporalFolder = System.currentTimeMillis();
			completeUrl = rootUrl + temporalFolder.toString();
	    }

	    try {
			String stringHtmlBasic = readFromInputStream(htmlBasic);
			String stringHtmlIntern = readFromInputStream(htmlIntern);
			stringHtmlBasic = stringHtmlBasic.replace(Constants.KEY_EMAIL_MESSAGE, stringHtmlIntern);

			if (dataEmail != null) {
			    for (String keyActual : dataEmail.keySet()) {
					if (dataEmail.get(keyActual) != null) {
					    stringHtmlBasic = stringHtmlBasic.replace(keyActual, dataEmail.get(keyActual).toString());
					}
			    }
			}

			// Mapea de data dentro del Subject
			String subject = subjectData.get(Constants.KEY_SUBJECT).toString();
			if (subjectData.keySet().size() > 1) {
			    for (String keyDataSubjectActual : subjectData.keySet()) {
			    	subject = subject.replace(keyDataSubjectActual, subjectData.get(keyDataSubjectActual).toString());
			    }
			}

		// Check if Object has attachment files
		if (dataEmail.get(Constants.ATTACHMENT_FILES) != null) {

		    FileSystemResource file = null;
		    tempFiles = new ArrayList<>();
		    @SuppressWarnings("unchecked")
		    ArrayList<String> attachmentList = (ArrayList<String>) dataEmail.get(Constants.ATTACHMENT_FILES);

		    for (String attachment : attachmentList) {

				File attachmentFile = new File(attachment);
	
				if (attachmentFile.exists() && !attachmentFile.isDirectory()) {
	
				    file = new FileSystemResource(new File(attachment));
				    helperEmail.addAttachment(file.getFilename(), file);
	
				    // If file is into a temp folder, it must be add to "tempFiles" in order to delete it later...
				    if (dataEmail.get(Constants.ATTACHMENT_FILES_TEMP_FOLDER) != null) {
					tempFiles.add(dataEmail.get(Constants.ATTACHMENT_FILES).toString().replace("[", "").replace("]", ""));
				    }
	
				} else {
	
				    // It is a external file, it must be local downloaded in order to attachment to email
				    new File(completeUrl).mkdir();
				    downloadedFile = downloadFile(attachment, completeUrl + File.separator);
	
				    if (!downloadedFile.isEmpty()) {
					file = new FileSystemResource(new File(downloadedFile));
					LogService.logger.info("attachment externo: " + downloadedFile);
					helperEmail.addAttachment(file.getFilename(), file);
				    }
	
				    tempFiles.add(downloadedFile);
				}
		    }

		} else {
		    //LogService.logger.info("Desde el servicio sendInternalEmail este mensaje NO tiene adjuntos");
		}

		email.setSubject(subject, StandardCharsets.UTF_8.toString());
		helperEmail.setText(stringHtmlBasic, true);
		
		if (emailFrom != null) {
		    helperEmail.setFrom(emailFrom);
		} else {
		    helperEmail.setFrom(new InternetAddress("celeste@kavak.com", "Celeste"));
		}
	    } catch (IOException e) {
		e.printStackTrace();
	    }

	    javaMailSenderImpl.send(helperEmail.getMimeMessage());
	    sended = true;

	    // It must delete all temporal files and temporal directory
	    if (dataEmail.get(Constants.ATTACHMENT_FILES) != null) {
			if (sended == true) {
			    if (!tempFiles.isEmpty()) {
	
				// Delete temporal files
				for (String file : tempFiles) {
				    LogService.logger.info("Eliminando archivo temporal " + file);
				    Path pathForDelete = Paths.get(file);
				    deleteFile(pathForDelete);
				}
			    }
	
			    if (dataEmail.get(Constants.ATTACHMENT_FILES_TEMP_FOLDER) != null) {
				completeUrl = dataEmail.get(Constants.ATTACHMENT_FILES_TEMP_FOLDER).toString();
			    }
	
			    // Delete temporal directory
			    LogService.logger.info("Eliminando carpeta temporal " + completeUrl);
			    Path pathForDelete = Paths.get(completeUrl);
			    deleteFile(pathForDelete);
			}
	    }

	} catch (MessagingException e) {
	    e.printStackTrace();
	    sended = false;
	}

	return sended;
    }

    public static boolean sendInternalEmail(String emailTo, List<String> emailCc, HashMap<String, Object> subjectData, String templateName, String templateNameBase, HashMap<String, Object> dataEmail) throws MessagingException {
	return sendInternalEmail(emailTo, emailCc, subjectData, templateName, templateNameBase, dataEmail, null);
    }

    /**
     * Metodo que valida la información recibida en el servicio PutUser
     *
     * @author Oscar Montilla
     * @param userRequestDTO
     *            request del servicio
     * @return Boolean
     */
    public static Boolean validateInputPutUser(UserRequestDTO userRequestDTO) {
	
		if (userRequestDTO.getId() == null) {
		    return false;
		}
	
		if (userRequestDTO.getName() == null && userRequestDTO.getPhone() == null && userRequestDTO.getStreet() == null && userRequestDTO.getExteriorNumber() == null && userRequestDTO.getInteriorNumber() == null && userRequestDTO.getPostalCode() == null && userRequestDTO.getPassword() == null) {
		    return false;
		}
	
		if (userRequestDTO.getStreet() != null && userRequestDTO.getExteriorNumber() == null && userRequestDTO.getPostalCode() == null) {
		    return false;
		}
	
		if (userRequestDTO.getStreet() != null && userRequestDTO.getExteriorNumber() != null && userRequestDTO.getPostalCode() == null) {
		    return false;
		}
	
		if (userRequestDTO.getStreet() != null && userRequestDTO.getExteriorNumber() != null && userRequestDTO.getInteriorNumber() != null && userRequestDTO.getPostalCode() == null) {
		    return false;
		}
	
		if (userRequestDTO.getStreet() != null && userRequestDTO.getExteriorNumber() == null && userRequestDTO.getInteriorNumber() != null && userRequestDTO.getPostalCode() != null) {
		    return false;
		}
	
		if (userRequestDTO.getExteriorNumber() != null && userRequestDTO.getStreet() == null && userRequestDTO.getPostalCode() == null) {
		    return false;
		}
	
		if (userRequestDTO.getExteriorNumber() != null && userRequestDTO.getStreet() != null && userRequestDTO.getPostalCode() == null) {
		    return false;
		}
	
		if (userRequestDTO.getInteriorNumber() != null && userRequestDTO.getExteriorNumber() == null && userRequestDTO.getStreet() == null && userRequestDTO.getPostalCode() == null) {
		    return false;
		}
	
		if (userRequestDTO.getInteriorNumber() != null && userRequestDTO.getExteriorNumber() != null && userRequestDTO.getStreet() == null && userRequestDTO.getPostalCode() == null) {
		    return false;
		}
	
		if (userRequestDTO.getInteriorNumber() != null && userRequestDTO.getExteriorNumber() != null && userRequestDTO.getStreet() != null && userRequestDTO.getPostalCode() == null) {
		    return false;
		}
	
		if (userRequestDTO.getPostalCode() != null && userRequestDTO.getExteriorNumber() == null && userRequestDTO.getStreet() == null) {
		    return false;
		}
	
		if (userRequestDTO.getPostalCode() != null && userRequestDTO.getExteriorNumber() != null && userRequestDTO.getStreet() == null) {
		    return false;
		}
	
		if (userRequestDTO.getPostalCode() != null && userRequestDTO.getExteriorNumber() != null && userRequestDTO.getInteriorNumber() != null && userRequestDTO.getStreet() == null) {
		    return false;
		}
	
		if (userRequestDTO.getPostalCode() != null && userRequestDTO.getExteriorNumber() == null && userRequestDTO.getStreet() != null) {
		    return false;
		}
	
		return true;
    }

    /**
     * Metodo que limpia los espacio y saltos de linea de un String
     *
     * @author Maria Marsicano
     * @param toClean
     *            String a limpiar
     * @return stringOut String Limpio
     */
    public static String cleanString(String toClean) {
		String stringOut;
	
		stringOut = toClean.replace("\n", "").replace("\t", "").replace("\r", "");
	
		return stringOut;
    }

    /**
     * Downloads a file from a URL (Netsuite propose)
     * 
     * @param fileURL
     *            HTTP URL of the file to be downloaded
     * @param saveDir
     *            path of the directory to save the file
     * @throws IOException
     */
    public static String downloadFile(String fileURL, String saveDir) throws IOException {
		URL url = new URL(fileURL);
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		int responseCode = httpConn.getResponseCode();
		String fileName = "";
	
		// always check HTTP response code first
		if (responseCode == HttpURLConnection.HTTP_OK) {
		    String disposition = httpConn.getHeaderField("Content-Disposition");
		    String contentType = httpConn.getContentType();
		    // int contentLength = httpConn.getContentLength();
	
		    if (disposition != null) {
			// extracts file name from header field
			int index = disposition.indexOf("filename=");
			if (index > 0) {
			    fileName = disposition.substring(index + 10, disposition.length() - 1);
			}
		    } else {
			// extracts file name from URL
			fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1, fileURL.length());
		    }
	
		    LogService.logger.info("Content-Type = " + contentType);
		    LogService.logger.info("fileName = " + fileName);
	
		    fileName = fileName.replaceAll("%20", "_");
	
		    // opens input stream from the HTTP connection
		    InputStream inputStream = httpConn.getInputStream();
	
		    String saveFilePath = saveDir + fileName;
	
		    // opens an output stream to save into file
		    FileOutputStream outputStream = new FileOutputStream(saveFilePath);
	
		    int bytesRead = -1;
		    byte[] buffer = new byte[BUFFER_SIZE];
		    while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		    }
	
		    outputStream.close();
		    inputStream.close();
	
		    if (contentType.equals("application/pdf")) {
			fileName = renameFileExtension(saveDir + fileName, "pdf");
		    } else if (contentType.equals("text/xml") || contentType.equals("text/xml; charset=UTF-8")) {
			fileName = renameFileExtension(saveDir + fileName, "xml");
		    } else if (contentType.equals("image/png")) {
			fileName = renameFileExtension(saveDir + fileName, "png");
		    } else if (contentType.equals("image/jpg")) {
			fileName = renameFileExtension(saveDir + fileName, "jpg");
		    } else if (contentType.equals("image/jpeg")) {
			fileName = renameFileExtension(saveDir + fileName, "jpeg");
		    } else if (contentType.equals("image/gif")) {
			fileName = renameFileExtension(saveDir + fileName, "gif");
		    } else if (contentType.equals("audio/mpeg3") || contentType.equals("audio/x-mpeg-3") || contentType.equals("video/mpeg") || contentType.equals("video/x-mpeg")) {
			fileName = renameFileExtension(saveDir + fileName, "mp3");
		    } else if (contentType.equals("text/html")) {
			fileName = renameFileExtension(saveDir + fileName, "html");
		    } else if (contentType.equals("text/plain")) {
			fileName = renameFileExtension(saveDir + fileName, "txt");
		    } else if (contentType.equals("application/msword") || contentType.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
			fileName = renameFileExtension(saveDir + fileName, ".docx");
		    } else if (contentType.equals("application/vnd.ms-excel") || contentType.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
			fileName = renameFileExtension(saveDir + fileName, "xlsx");
		    } else if (contentType.equals("application/vnd.ms-powerpoint") || contentType.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
			fileName = renameFileExtension(saveDir + fileName, "pptx");
		    }
	
		} else {
		    LogService.logger.info("No file to download. Server replied HTTP code: " + responseCode);
		}
		httpConn.disconnect();
	
		return fileName;
    }

    /*
     * Delete a Folder (this folder must be empty)
     * 
     * @params Path pathToFile
     */

    public static void deleteFile(Path pathToFile) {
		try {
		    Files.delete(pathToFile);
		} catch (NoSuchFileException x) {
		    LogService.logger.error("No existe el archivo o directorio: " + pathToFile);
	
		} catch (DirectoryNotEmptyException x) {
		    System.err.format("%s not empty%n", pathToFile);
	
		    LogService.logger.error("Not empty: " + pathToFile);
		} catch (IOException x) {
		    // File permission problems are caught here.
		    LogService.logger.error("Error por permisologia eliminando archivo o directorio: " + x);
		}
    }

    /*
     * Rename a extension file (For Netsuite Download files)
     * 
     * @param String source: file name with initial extension
     * 
     * @param String newExtension: new extension for replacing into source
     */

    public static String renameFileExtension(String source, String newExtension) {
		String target;
		String currentExtension = getFileExtension(source);
	
		if (currentExtension.equals("")) {
		    target = source + "." + newExtension;
		} else {
		    target = source.replaceFirst(Pattern.quote("." + currentExtension) + "$", Matcher.quoteReplacement("." + newExtension));
	
		}
	
		new File(source).renameTo(new File(target));
	
		return target;
    }

    public static String getFileExtension(String f) {
		String ext = "";
		int i = f.lastIndexOf('.');
		if (i > 0 && i < f.length() - 1) {
		    ext = f.substring(i + 1);
		}
		return ext;
    }

    /**
     * Metodo redondea el precio suministrado
     * 
     * @param salePrice
     *            precio de venta
     * @author Oscar Montilla
     * @return resutl precio reondeado
     */
    public static Integer getConvertSalePrice(Integer salePrice) {

    	Integer resutl;
	
		Integer priceAux = (salePrice / 10000) * 10000;
	
		if ((salePrice - priceAux) < 5000) {
		    resutl = priceAux + 4999;
		} else {
		    resutl = priceAux + 9999;
		}
	
		return resutl;
    }

    /**
     * Remove any HTML tag from String
     * 
     * @author Antony Delgado
     * @param String html
     * @return Clean String
     */

    public static String html2text(String html) {
    	return Jsoup.parse(html).text();
    }
    
    
    /**
     * Validate if file exist in server
     * 
     * @author Antony Delgado
     * @param String html
     * @return Clean String
     */
    
    
    public static boolean isFileExists(String filePath){
    	boolean result = false;
    	
    	LogService.logger.info("Se procede a buscar el archivo " + filePath);
    	
    	File f = new File(filePath);
    	
    	if(f.listFiles() != null) { 
    	    result = true;
    	}else if (!f.isFile()){
    		LogService.logger.warn(filePath + " no es un archivo.");
    	}
    	
    	return result;
    }
    
    
    /**
     * Generate an URL for calling Whatsapp App with a initial message
     * 
     * NOTE: return phoneNumber format: +52123456789
     * 
     * @author Antony Delgado
     * @param String phoneNumber
     * @param String message
     * @return String finalUrl
     */
    
    public static String generateWhatsappMobileUrl(String phoneNumber, String message){
    	String finalUrl = "";
    	
    	try {
    		LogService.logger.debug("Se procede a generar URL para Whatsapp Web");
    		
    		if(!phoneNumber.isEmpty() && !message.isEmpty()){
    	
    			phoneNumber = phoneNumber.replaceAll(" ", "");
    			
    			if(phoneNumber.indexOf("+52") == -1){
    				LogService.logger.debug("El numero de celular " + phoneNumber + " NO tiene el formato correcto, se procede a dar formato.");
    				phoneNumber = Constants.KAVAK_CODE_MX_PHONE + phoneNumber;
    			}
    			
    			String clearMessage = URLEncoder.encode(message, "UTF-8");
    			
    			LogService.logger.debug("El numero de celular definitivo: " + phoneNumber);
				finalUrl = Constants.WHATSAPP_MOBIL_API.replace(Constants.WHATSAPP_NUMBER_SECTION, phoneNumber).replace(Constants.WHATSAPP_MESSAGE_SECTION, clearMessage.replace("+", "%20"));
				
    		}else if(phoneNumber.isEmpty()){
    			LogService.logger.debug("El numero de celular esta vacio, se descarta llamado a GenerateWhatsappMobileUrl");
    		}else{
    			LogService.logger.debug("El mensaje esta vacio, se descarta llamado a GnerateWhatsappMobileUrl para el numero " + phoneNumber);
    		}
		} catch (Exception e) {
			LogService.logger.error("Ocurrio un error intentando generar URL para Whatsapp Mobile, " + e);
			e.printStackTrace();
		}

    	return finalUrl;
    	
    }
    
    
    
    /**
     * Generate an URL for calling Whatsapp Web with a initial message
     * 
     * NOTE: return a phoneNumber format: +52123456789
     * 
     * @author Antony Delgado
     * @param String phoneNumber
     * @param String message
     * @return String finalUrl
     */
    
    public static String generateWhatsappDesktopUrl(String phoneNumber, String message){
    	String finalUrl = "";
    	
    	try {
    		LogService.logger.debug("Se procede a generar URL para Whatsapp Web");
    		
    		if(!phoneNumber.isEmpty() && !message.isEmpty()){
    	
    			phoneNumber = phoneNumber.replaceAll(" ", "");
    			
    			if(phoneNumber.indexOf("+52") == -1){
    				LogService.logger.debug("El numero de celular " + phoneNumber + " NO tiene el formato correcto, se procede a dar formato.");
    				phoneNumber = Constants.KAVAK_CODE_MX_PHONE + phoneNumber;
    			}
    			
    			String clearMessage = URLEncoder.encode(message, "UTF-8");
    			
    			LogService.logger.debug("El numero de celular definitivo: " + phoneNumber);
				finalUrl = Constants.WHATSAPP_DESKTOP_API.replace(Constants.WHATSAPP_NUMBER_SECTION, phoneNumber).replace(Constants.WHATSAPP_MESSAGE_SECTION, clearMessage.replace("+", "%20"));
    			
    		}else if(phoneNumber.isEmpty()){
    			LogService.logger.debug("El numero de celular esta vacio, se descarta llamado a GnerateWhatsappDesktopUrl");
    		}else{
    			LogService.logger.debug("El mensaje esta vacio, se descarta llamado a GnerateWhatsappDesktopUrl para el numero " + phoneNumber);
    		}
		} catch (Exception e) {
			LogService.logger.error("Ocurrio un error intentando generar URL para Whatsapp Web, " + e);
			e.printStackTrace();
		}

    	return finalUrl;
    }
    
    /**
     * Generate an URL for calling Whatsapp Web with a initial message
     * 
     * NOTE: String url email to. e.g: mailto:antony.delgado@kavak.com?subject=Hello&body=This%20is%20a%20test
     * 
     * @author Antony Delgado
     * @param String email
     * @param String subject
     * @param String body
     * @param String cc
     * @param String bcc
     * @return
     */
    
    public static String generateEmailUrl(String email, String subject, String body, String cc, String bcc){
    	String finalUrl = null;
    	String mailTo   = "mailto:";
    	try {
    		if(email != null && !email.trim().isEmpty()){
    			LogService.logger.debug("Se procede a generar URL para enlaces que invocan correos.");
    			
    			finalUrl = mailTo + email.trim().toLowerCase();
    			
    			if(subject != null && !subject.isEmpty()){
    				finalUrl = finalUrl + "?subject=" + WordUtils.capitalize(subject);
    			}
    			
				if( body != null && !body.isEmpty()){
					finalUrl = finalUrl + "&body=" + body;
				}
				
				if(cc != null && !cc.isEmpty()){
					finalUrl = finalUrl + "&cc=" + cc.trim();
				}
				
    			if( bcc != null && !bcc.isEmpty()){
    				finalUrl = finalUrl + "&bcc=" + bcc.trim();
    			}
    			
    			//String clearMessage = URLEncoder.encode(finalUrl, "UTF-8");
    			finalUrl = finalUrl.replace(" ", "%20");
    			
    			LogService.logger.debug("URL final para enlaces invocando email: " + finalUrl);
				
    		}else{
    			LogService.logger.warn("No hay correo para generar url de emails, se descarta servicio.");
    		}
		} catch (Exception e) {
			LogService.logger.error("Ocurrio un error intentando generar URL para correo, " + e);
			e.printStackTrace();
		}
    	
    	return finalUrl;
    }
    
    
    
    /**
     * Clean Price en set Mexican Peso format
     *
     * @author Antony Delgado
     * @param String price (eg 123446.00)
     * @return String result (eg $123,446)
     */
    
    public static String splitThousandFormat(String price){
    	String result = "";
    	
    	try {
    		String clearPrice = price.replaceAll("[^\\d.]", "");
    		
    		Long noFormatPrice = (long)Double.parseDouble(clearPrice);
    		result = String.format("%,d", noFormatPrice);
    		
		} catch (Exception e) {
			LogService.logger.error("Ocurrio un error limpiar precio de auto, " + e);
			e.printStackTrace();
		}
    	
    	
    	return result;
    }
    
    
    /**
     * Remove acute accents in String
     *
     * @author Antony Delgado
     * @param String text (eg. árbol ``)
     * @return String result (eg arbol)
     */
    
    public static String removeAccents(String text) 
    {
    	text = Normalizer.normalize(text, Normalizer.Form.NFD);
        text = text.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return text;
    }

	/**
	 * Metodo que vlida una url, identifica si tiene dildes o acentos en el codigo para codificarlo y validar la url
	 *
	 * @param url Url Suministrada
	 * @return Boolean
	 * @author Oscar Montilla
	 */
	public static Boolean URLvalidador(String url) {
		try {
			BufferedImage image = ImageIO.read(new URL(url.replace(" ", "%20")
					.replace("ò", "%C3%B2")
					.replace("ó", "%C3%B3")
					.replace("ù", "%C3%B9")
					.replace("ú", "%C3%BA")
					.replace("à", "%C3%A0")
					.replace("á", "%C3%A1")
					.replace("è", "%C3%A8")
					.replace("é", "%C3%A9")
					.replace("ì", "%C3%AC")
					.replace("í", "%C3%AD")
					.replace("Ñ", "%C3%91")
					.replace("ñ", "%C3%B1")));
			if (image == null) {
				return false;
			} else {
				return true;
}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.err.println("URL error with image");
			return false;
		} catch (IOException e) {
			System.err.println("IO error with image");
			return false;
		}
	}
}