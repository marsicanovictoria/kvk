package com.kavak.core.util;

public interface LookUpNames {

    // String JDBC_KAVAK = "java:jboss/datasources/KavakDS";

    String SESSION_SellCarServices = "java:app/KAVAK_BusinessCore_EJBTier/SellCarServices!com.kavak.core.service.SellCarServices";

    String SESSION_OpenPayTransactionServices = "java:app/KAVAK_BusinessCore_EJBTier/OpenPayTransactionServices!com.kavak.core.service.openpay.OpenPayTransactionServices";

    String SESSION_InspectionScheduleServices = "java:app/KAVAK_BusinessCore_EJBTier/InspectionScheduleServices!com.kavak.core.service.InspectionScheduleServices";

    String SESSION_BuyCarServices = "java:app/KAVAK_BusinessCore_EJBTier/BuyCarServices!com.kavak.core.service.BuyCarServices";

    String SESSION_InspectionServices = "java:app/KAVAK_BusinessCore_EJBTier/InspectionServices!com.kavak.core.service.InspectionServices";

    String SESSION_CustomerServices = "java:app/KAVAK_BusinessCore_EJBTier/CustomerServices!com.kavak.core.service.netsuite.CustomerServices";

    String SESSION_PurchaseOpportunityServices = "java:app/KAVAK_BusinessCore_EJBTier/PurchaseOpportunityServices!com.kavak.core.service.netsuite.PurchaseOpportunityServices";

    String SESSION_ArticleServices = "java:app/KAVAK_BusinessCore_EJBTier/ArticleServices!com.kavak.core.service.netsuite.ArticleServices";

    String SESSION_CommonServices = "java:app/KAVAK_BusinessCore_EJBTier/CommonServices!com.kavak.core.service.CommonServices";

    String SESSION_ProductServices = "java:app/KAVAK_BusinessCore_EJBTier/ProductServices!com.kavak.core.service.ProductServices";

    String SESSION_InsuranceServices = "java:app/KAVAK_BusinessCore_EJBTier/InsuranceServices!com.kavak.core.service.InsuranceServices";

    String SESSION_AppointmentServices = "java:app/KAVAK_BusinessCore_EJBTier/AppointmentServices!com.kavak.core.service.AppointmentServices";

    String SESSION_SaleOpportunityServices = "java:app/KAVAK_BusinessCore_EJBTier/SaleOpportunityServices!com.kavak.core.service.netsuite.SaleOpportunityServices";

    String SESSION_InventoryServices = "java:app/KAVAK_BusinessCore_EJBTier/InventoryServices!com.kavak.core.service.netsuite.InventoryServices";

    String SESSION_CheckoutServices = "java:app/KAVAK_BusinessCore_EJBTier/CheckoutServices!com.kavak.core.service.CheckoutServices";

    String SESSION_OAServices = "java:app/KAVAK_BusinessCore_EJBTier/OAServices!com.kavak.core.service.oa.OAServices";

    String SESSION_InspectionsNetsuite = "java:app/KAVAK_BusinessCore_EJBTier/InspectionsServices!com.kavak.core.service.netsuite.InspectionsServices";

    String SESSION_UserServices = "java:app/KAVAK_BusinessCore_EJBTier/UserServices!com.kavak.core.service.UserServices";

    String SESSION_CarscoutServices = "java:app/KAVAK_BusinessCore_EJBTier/CarscoutServices!com.kavak.core.service.CarscoutServices";

    String SESSION_LocationKavakCarServices = "java:app/KAVAK_BusinessCore_EJBTier/LocationKavakCarServices!com.kavak.core.service.netsuite.LocationKavakCarServices";

    String SESSION_StagePurchaseOpportunities = "java:app/KAVAK_BusinessCore_EJBTier/StagePurchaseServices!com.kavak.core.service.StagePurchaseServices";

    String SESSION_TransactionalEmailsServices = "java:app/KAVAK_BusinessCore_EJBTier/TransactionalEmailsServices!com.kavak.core.service.TransactionalEmailsServices";
	
    String SESSION_AppNotificationServices = "java:app/KAVAK_BusinessCore_EJBTier/AppNotificationServices!com.kavak.core.service.AppNotificationServices";

    String SESSION_BPMServices = "java:app/KAVAK_BusinessCore_EJBTier/BPMServices!com.kavak.core.service.bpm.BPMServices";
    
    String SESSION_CustomerBusinessServices = "java:app/KAVAK_BusinessCore_EJBTier/CustomerBusinessServices!com.kavak.core.service.CustomerBusinessServices";
    
    String SESSION_GenerateExcelFileServices = "java:app/KAVAK_BusinessCore_EJBTier/GenerateExcelFileServices!com.kavak.core.service.GenerateExcelFileServices";
    
    String SESSION_TransactionalSmsServices = "java:app/KAVAK_BusinessCore_EJBTier/TransactionalSmsServices!com.kavak.core.service.TransactionalSmsServices";

    String SESSION_CelesteServices = "java:app/KAVAK_BusinessCore_EJBTier/CelesteServices!com.kavak.core.service.CelesteServices";
    
    String SESSION_TransactionalPersonalEmailsServices = "java:app/KAVAK_BusinessCore_EJBTier/TransactionalPersonalEmailsServices!com.kavak.core.service.TransactionalPersonalEmailsServices";

    String SESSION_FinancingServices = "java:app/KAVAK_BusinessCore_EJBTier/FinancingServices!com.kavak.core.service.FinancingServices";
    
}