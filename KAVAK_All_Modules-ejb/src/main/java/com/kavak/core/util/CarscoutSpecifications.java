package com.kavak.core.util;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.kavak.core.dto.specification.CarscoutAlertSpecificationDTO;
import com.kavak.core.dto.specification.CarscoutFilterCoincidencesSpecificationDTO;
import com.kavak.core.model.CarscoutCatalogue;

public class CarscoutSpecifications {

    /**
     * Metodo que obtiene el predicado para el query del servicio de
     * Coincidencias Carscout
     *
     * @param carscoutFilterCoincidencesSpecificationDTO Objeto de valores para verificar en el predicado
     * @return predicates Condiciones preparadas para ser incorporadas en el query de consulta
     * @author Oscar Montilla
     */
    public static Specification<CarscoutCatalogue> findByPredicateCarscoutFilterCoincidences(CarscoutFilterCoincidencesSpecificationDTO carscoutFilterCoincidencesSpecificationDTO) {
        return new Specification<CarscoutCatalogue>() {

            private static final long serialVersionUID = -5375712015799108109L;

            @Override
            public Predicate toPredicate(Root<CarscoutCatalogue> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                Collection<Predicate> predicates = new ArrayList<>();

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListYears().isEmpty()) {

                    predicates.add(root.join("carData").get("yearId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListYears()));
                }

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListBodytype().isEmpty()) {

                    predicates.add(root.join("carData").get("styleId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListBodytype()));
                }

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListMake().isEmpty()) {

                    predicates.add(root.join("carData").get("makeId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListMake()));
                }

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListVersion().isEmpty()) {

                    predicates.add(root.join("carData").get("versionId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListVersion()));
                }

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListModel().isEmpty()) {

                    predicates.add(root.join("carData").get("modelId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListModel()));
                }

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListDoors().isEmpty()) {

                    predicates.add(root.join("carData").get("doorsId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListDoors()));
                }

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListSeats().isEmpty()) {

                    predicates.add(root.join("carData").get("seatsId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListSeats()));
                }

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListCylinders().isEmpty()) {

                    predicates.add(root.join("carData").get("cylindroId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListCylinders()));
                }

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListTraction().isEmpty()) {

                    predicates.add(root.join("carData").get("traccionId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListTraction()));
                }

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListFueltype().isEmpty()) {

                    predicates.add(root.join("carData").get("fuelId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListFueltype()));
                }

//				if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListColor().isEmpty()) {
//
//					predicates.add(root.get("colorId").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListColor()));
//				}

                if (!carscoutFilterCoincidencesSpecificationDTO.getIdsListTransmissions().isEmpty()) {

                    predicates.add(root.join("carData").get("transmissionid").in(carscoutFilterCoincidencesSpecificationDTO.getIdsListTransmissions()));
                }

                if (carscoutFilterCoincidencesSpecificationDTO.getMinPrice() != null && carscoutFilterCoincidencesSpecificationDTO.getMaxPrice() != null) {

                    predicates.add(cb.between(root.get("salePrice"), carscoutFilterCoincidencesSpecificationDTO.getMinPrice(), carscoutFilterCoincidencesSpecificationDTO.getMaxPrice()));
                }

                if (carscoutFilterCoincidencesSpecificationDTO.getMaxKm() != null && carscoutFilterCoincidencesSpecificationDTO.getMinKm() == null) {

                    predicates.add(cb.le(root.get("km"), carscoutFilterCoincidencesSpecificationDTO.getMaxKm()));

                } else if (carscoutFilterCoincidencesSpecificationDTO.getMaxKm() != null && carscoutFilterCoincidencesSpecificationDTO.getMinKm() != null) {

                    predicates.add(cb.between(root.get("km"), carscoutFilterCoincidencesSpecificationDTO.getMinKm(), carscoutFilterCoincidencesSpecificationDTO.getMaxKm()));
                }

                predicates.add(root.get("activeInd").in(Constants.ACTIVE_DATA));

                return cb.and(predicates.toArray(new Predicate[predicates.size()]));

            }

        };

    }

    /**
     * Metodo que obtiene el predicado para el query del servicio de registrar
     * alertas de Carscout
     *
     * @param carscoutAlertSpecificationDTO Objeto de valores para verificar en el predicado
     * @return predicates Condiciones preparadas para ser incorporadas en el query de consulta
     * @author Oscar Montilla
     */
    public static Specification<CarscoutCatalogue> findByPredicateCarscoutAlert(CarscoutAlertSpecificationDTO carscoutAlertSpecificationDTO) {
        return new Specification<CarscoutCatalogue>() {

            private static final long serialVersionUID = -4497783990774623762L;

            @Override
            public Predicate toPredicate(Root<CarscoutCatalogue> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                Collection<Predicate> predicates = new ArrayList<>();

                if (!carscoutAlertSpecificationDTO.getIdsListYears().isEmpty()) {

                    predicates.add(root.join("carData").get("yearId").in(carscoutAlertSpecificationDTO.getIdsListYears()));
                }

                if (!carscoutAlertSpecificationDTO.getIdsListBodytype().isEmpty()) {

                    predicates.add(root.join("carData").get("styleId").in(carscoutAlertSpecificationDTO.getIdsListBodytype()));
                }

                if (!carscoutAlertSpecificationDTO.getIdsListMake().isEmpty()) {

                    predicates.add(root.join("carData").get("makeId").in(carscoutAlertSpecificationDTO.getIdsListMake()));
                }

                if (!carscoutAlertSpecificationDTO.getIdsListVersion().isEmpty()) {

                    predicates.add(root.join("carData").get("versionId").in(carscoutAlertSpecificationDTO.getIdsListVersion()));
                }

                if (!carscoutAlertSpecificationDTO.getIdsListModel().isEmpty()) {

                    predicates.add(root.join("carData").get("modelId").in(carscoutAlertSpecificationDTO.getIdsListModel()));
                }

                if (!carscoutAlertSpecificationDTO.getIdsListDoors().isEmpty()) {

                    predicates.add(root.join("carData").get("doorsId").in(carscoutAlertSpecificationDTO.getIdsListDoors()));
                }

                if (!carscoutAlertSpecificationDTO.getIdsListSeats().isEmpty()) {

                    predicates.add(root.join("carData").get("seatsId").in(carscoutAlertSpecificationDTO.getIdsListSeats()));
                }

                if (!carscoutAlertSpecificationDTO.getIdsListCylinders().isEmpty()) {

                    predicates.add(root.join("carData").get("cylindroId").in(carscoutAlertSpecificationDTO.getIdsListCylinders()));
                }

                if (!carscoutAlertSpecificationDTO.getIdsListTraction().isEmpty()) {

                    predicates.add(root.join("carData").get("traccionId").in(carscoutAlertSpecificationDTO.getIdsListTraction()));
                }

                if (!carscoutAlertSpecificationDTO.getIdsListFueltype().isEmpty()) {

                    predicates.add(root.join("carData").get("fuelId").in(carscoutAlertSpecificationDTO.getIdsListFueltype()));
                }

//				if (!carscoutAlertSpecificationDTO.getIdsListColor().isEmpty()) {
//
//					predicates.add(root.get("colorId").in(carscoutAlertSpecificationDTO.getIdsListColor()));
//				}

                if (!carscoutAlertSpecificationDTO.getIdsListTransmissions().isEmpty()) {

                    predicates.add(root.join("carData").get("transmissionid").in(carscoutAlertSpecificationDTO.getIdsListTransmissions()));
                }

                if (carscoutAlertSpecificationDTO.getMinPrice() != null && carscoutAlertSpecificationDTO.getMaxPrice() != null) {

                    predicates.add(cb.between(root.get("salePrice"), carscoutAlertSpecificationDTO.getMinPrice(), carscoutAlertSpecificationDTO.getMaxPrice()));
                }

                if (carscoutAlertSpecificationDTO.getMaxKm() != null && carscoutAlertSpecificationDTO.getMinKm() == null) {

                    predicates.add(cb.le(root.get("km"), carscoutAlertSpecificationDTO.getMaxKm()));

                } else if (carscoutAlertSpecificationDTO.getMaxKm() != null && carscoutAlertSpecificationDTO.getMinKm() != null) {

                    predicates.add(cb.between(root.get("km"), carscoutAlertSpecificationDTO.getMinKm(), carscoutAlertSpecificationDTO.getMaxKm()));
                }

                predicates.add(root.get("activeInd").in(Constants.ACTIVE_DATA));

                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

    }

}
