package com.kavak.core.util;

import org.apache.commons.configuration.SystemConfiguration;

public class Constants {
    /*
     * Parametro para obtener los parametros definidos en el standalone-full de
     * wildfly
     */
    static SystemConfiguration systemConfiguration = new SystemConfiguration();

    /*******************************
     * CONSTANTES QUE SE OBTIENEN DEL WILDFLY
     **********************************/
    /* Parametros para la configuracion de OPENPAY */
    public static final String OPENPAY_LOCATION = systemConfiguration.getString("com.kavak.core.openpay.location");
    public static final String OPENPAY_APIKEY = systemConfiguration.getString("com.kavak.core.openpay.apiKey");
    public static final String OPENPAY_MERCHANTID = systemConfiguration.getString("com.kavak.core.openpay.merchantId");

    /* Parametros para el envio de correo por gmail */
    public static final String PROPERTY_EMAIL_USERNAME = systemConfiguration.getString("com.kavak.core.email.username");
    public static final String PROPERTY_EMAIL_PASSWORD = systemConfiguration.getString("com.kavak.core.email.password");
    public static final String PROPERTY_EMAIL_HOST = systemConfiguration.getString("com.kavak.core.email.host");
    public static final Integer PROPERTY_EMAIL_PORT = systemConfiguration.getInt("com.kavak.core.email.port");
    public static final String PROPERTY_EMAIL_PROTOCOL = systemConfiguration.getString("com.kavak.core.email.protocol");
    public static final String PROPERTY_EMAIL_TIMEOUT = systemConfiguration.getString("com.kavak.core.email.timeout");
    public static final String MESSAGE_TEMPLATE_PATH = systemConfiguration.getString("com.kavak.core.message.fullpath");
    public static final String PROPERTY_EMAIL_PREFIX = systemConfiguration.getString("com.kavak.core.email.subject.prefix");
    public static final String PROPERTY_ITKAVAK = systemConfiguration.getString("com.kavak.core.email.bcc");

    public static final String PROPERTY_EMAIL_URL_RECEIPT = systemConfiguration.getString("com.kavak.core.openpay.receipt_url");
    public static final String PROPERTY_FINANCING_URL_PDF = systemConfiguration.getString("com.kavak.core.checkout.financing_url_pdf");
    public static final String PROPERTY_HOST_URL = systemConfiguration.getString("com.kavak.core.host.url");
    public static final String PROPERTY_ENVIRONMENT = systemConfiguration.getString("com.kavak.core.environment");
    public static final String PROPERTY_CATALOGUE_NEWCARS = systemConfiguration.getString("com.kavak.core.catalogue.newcars");

    public static final Integer PROPERTY_CARSCOUT_MAX_ALERT_TIME_MONTHS = systemConfiguration.getInt("com.kavak.core.carscout.maxAlertTimeMonths");
    /*******************************
     * AMBIENTES WILDFLY
     **********************************************/
    public static final String ENVIRONMENT_PRODUCTION = "PRODUCTION";
    public static final String ENVIRONMENT_DEVELOPER = "DEVELOPER";
    
    /*******************************
     * CONSTANTES MENSAJES EN EL LOG
     **********************************************/
    // public static final String LOG_INVOKE = "Invocando Enpoint";
    public static final String LOG_EXECUTING_START = "Iniciando Servicio";
    public static final String LOG_EXECUTING_END = "Terminando Servicio";
    // public static final String LOG_FINISH = "Termino Enpoint ";

    /*******************************
     * CONSTANTES PARA POST EN OPENPAY
     ******************************************/
    /*
     * Parametros para la configuracion del POST que envia OPENPAY para
     * identificar el tipo de operacion
     */
    public static final String VERIFICATION = "verification";
    public static final String CHARGESUCCEEDED = "charge.succeeded";

    /*******************************
     * CONSTANTES PARA GET DISTANCEMATRIX DE GOOGLE
     *********************************/
    public static final String GOOGLE_API_KEY = systemConfiguration.getString("com.kavak.core.google.distanceMatrix.apiKey");

    /****************************
     * CONSTANTES PARA El DESEREALIZADO DE PHP A JAVA
     **********************************/
    /* Deserializado de primer lvl */
    public static final String FEPM = "fepm";
    public static final String BASIC_OVERVIEW = "basic_overview";
    public static final String BOOKING_PRICE = "booking_price";
    public static final String INSPECTOR_DETAILS = "inspector_details";

    /* Deserializado de segundo lvl */
    public static final String STOCK = "stock";
    public static final String PRICING = "pricing";
    public static final String TRANSMISION = "transmission";
    public static final String MARKET_PRICE = "market_prrice";
    public static final String FUEL_TYPE = "fueltype";
    public static final String ENGINE = "engine";
    public static final String HORSE_POWER = "horsepower";
    public static final String HISTORY_REPORT = "history_report";
    public static final String BODY_TYPE = "body_type";
    public static final String EXT_COLOR = "ext_color";
    public static final String INT_COLOR = "int_color";
    public static final String DOORS = "doores";
    public static final String KEYS = "keys";
    public static final String PREV_OWN = "prev_own";
    public static final String FUEL_CITY = "fcity";
    public static final String FUEL_HIGHWAY = "fhighway";
    public static final String FUEL_COMBINED = "fcombined";
    public static final String ENTERTAINMENT = "entertainment";
    public static final String EXTERIOR = "exterior";
    public static final String INTERIOR = "interior";
    public static final String MECHANICAL = "mechanical";
    public static final String SAFETY = "safety";
    public static final String NAME = "name";
    public static final String ADDRESS = "address";
    public static final String EMAIL = "mail";
    public static final String IMAGE = "image";
    public static final String TRACTION = "traccion";
    public static final String CYLINDER = "engine";
    public static final String DIMPLES_CONTENT = "dimples_content";
    public static final String OTHER = "other";
    public static final String INCPECTIONKS = "incpectionks";
    public static final String INSPECTIONPRICEMKT = "incpectionmarket";
    public static final String PAPERWORKKVK = "fourth_section_carsnap";
    public static final String PAPERWORKMKT = "fourth_section_market";
    public static final String DETAILINGKVK = "detailing_kavak";
    public static final String DETAILINGMKT = "detailing_market";
    public static final String FEATURE_LIST = "feature_list";
    public static final String WARRANTYKS = "warrantyks";
    public static final String WARRANTYMARKET = "warrantymarket";
    public static final String PASSENGERS = "passengers";
    public static final String TENUREMARKET = "tenure_market";
    public static final String RAPI_URL = "rapi_url";
    public static final String DOWNLOAD = "DESCARGAR";
    public static final String AUTO_LOCATION = "UBICACIÓN DEL AUTO";
    

    // CARROS
    // public static final String URL_BASE_IMAGES_DIMPLES =
    // "http://www.kavak.com/"; //DIMPLES Repetido
    public static final String NAME_BASE_IMAGE_BD = "/assets/uploads/"; // Generico
    public static final String NAME_BASE_IMAGE_CUSTOMER_SIGNATURE = "inspections/"; // Firma
										    // del
										    // usuario
										    // para
										    // el
										    // Summary

    /****************************
     * URL BASE PARA ARCHIVOS FISICOS EN EL SERVIDOR
     *************************************/
    public static final String URL_FULL_PATH_FOLDER_ASSEST = systemConfiguration.getString("com.kavak.core.url.FullPath");
    public static final String URL_SHORT_PATH_FOLDER_ASSEST = systemConfiguration.getString("com.kavak.core.url.assets.uploads");
    public static final String URL_FULL_PATH_FOLDER_CUSTOMER= systemConfiguration.getString("com.kavak.core.url.customer.FullPath");
    public static final String URL_PATH_CUSTOMER = systemConfiguration.getString("com.kavak.core.url.assets.uploads.customer");

    /****************************
     * NOMBRE DE LA IMAGEN DEL DOCUMENTO
     *************************************/
    public static final String NAME_IMAGE_CIRCULATION_CARD = "circulation_card_";
    public static final String NAME_IMAGE_CAR_INVOICE = "car_invoice_";
    public static final String NAME_IMAGE_CAR_PHOTO = "car_photo_";
    public static final String NAME_VOICE_CUSTOMER = "voice";

    /***************************
     * URL BASE PARA LA FICHA TECNICA
     *************************************/
    public static final String URL_BASE_DATA_SHEET = "https://www.kavak.com/reports/datasheet/";

    // public static final String UR_BASE_CIRCULATION_CARD =
    // "http://www.kavak.com/assets/uploads/tarjeta-circulacion/";
    // public static final String UR_BASE_CAR_BILL =
    // "http://www.kavak.com/assets/uploads/factura-auto/";
    // public static final String UR_BASE_IMG_CAR_PHOTO =
    // "http://www.kavak.com/assets/uploads/car_photo/";

    /****************************
     * STATUS PARA CONSULTA CARROS EN EL CATALOGO
     *************************************/
    /*
     * Cuando se consulta en Catalogo de carros con catalogue/cars este retorna
     * un estatus en los carros
     */
    public static final String AVAILABLE = "Disponible";
    public static final String RESERVED = "Reservado";
    public static final String SOLD = "Vendido";

    /****************************
     * VALORES PARA PARA META_VALE
     ****************************************************/
    public static final String META_VALUE_PHONE = "dealer_phn";
    public static final String META_VALUE_ADDRESS = "dealer_addr";
    public static final String META_VALUE_WHY_KAVAK = "PorqueKavak";
    public static final String META_VALUE_ADDRESS_FINANCING = "direccion_financiamiento";
    public static final String META_VALUE_LANDLINE_PHONE_FINANCING = "telefono_fijo_financiamiento";
    public static final String META_VALUE_PHONE_FINANCING = "telefono_celular_financiamiento";
    public static final String META_VALUE_TYPE_REFERENCE_PERSONAL = "P";
    public static final String META_VALUE_TYPE_REFERENCE_FAMILY = "F";
    public static final String META_VALUE_KAVAK_ADDRESS = "DICAR";
    public static final String META_VALUE_CUSTOMER_ADDRESS = "DICLI";
    public static final String META_VALUE_CONTACT = "contact";
    public static final String META_VALUE_GROUP_KAVAK = "KAVAK";
    public static final String META_VALUE_ALERTS_MAX = "ALERT_MAX";

    /********************************
     * ENDPOINTS RESPONSE CODE
     *****************************************/
    public static final Long OK = 200L; // 2XX Success
    public static final Long SERVER_ERROR = 500L;// 5XX Server Error
    // 3XX Redirection
    // 4XX Client Error

    /********************************
     * VALORES GLOBALES IMPLEMENTADAS PARA NETSUITE
     *********************************/
    public static final String URL_KAVAK = "https://www.kavak.com/";
    public static final String CUSTOMER_ROLE = "seller";
    public static final String ADMIN_ROLE = "admin";
    public static final String DEFAULT_PASSWORD = "$2y$12$pcNQ6oiLRwNMdZ1lvawRwu3pP9piZrVFEvPjPBOV140IqBKDH2BWm";
    public static final String NETSUITE_OPPORTUNITY_URL = "https://system.na2.netsuite.com/app/accounting/transactions/opprtnty.nl?id=";
    public static final String PURCHASE_OPPORTUNITY_METHOD = "purchaseOpportunity";
    public static final String SALE_OPPORTUNITY_METHOD = "saleOpportunity";
    public static final String INVENTORY_METHOD = "inventory";
    public static final String NETSUITE_INTEGRATION_AUTH_USER = "marsicano.victoria@gmail.com";
    public static final String NETSUITE_INTEGRATION_AUTH_PASSWORD = "M4r1@V1ct0r14";
    public static final String NETSUITE_INTEGRATION_URI = "https://4570554.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=167&deploy=1";
    public static final String NETSUITE_INTEGRATION_HEADER = "NLAuth nlauth_account=4570554, nlauth_email="+ NETSUITE_INTEGRATION_AUTH_USER + ", nlauth_signature=" + NETSUITE_INTEGRATION_AUTH_PASSWORD + ", nlauth_role=18";
    public static final String KAVAK_EMAIL_CAR_LOGO = URL_KAVAK + "assets/kavak/email/car_logo/";
    public static final String KAVAK_CODE_MX_PHONE = "+52";
    public static final String URL_HTTP = "http";
    
    /********************************
     * VALORES GLOBALES IMPLEMENTADAS PARA NETSUITE
     *********************************/
    public static final int MAX_DAYS_SCHEDULE_BLOCK = 6; // Cantidad de dias que se toman para buscar los horarios de inpecciones
    public static final int MAX_DAYS_SCHEDULE_BLOCK7 = 7; // Cantidad de dias que se toman para buscar los horarios de inpecciones

    /********************************
     * VALORRES PARA LOS DIAS DE VALIDES DE LA OFERTA
     *****************************/
    public static final int DAYS_VALIDATE_OFFER = 7;

    /***************************
     * VALORRES PARA LA DIRECCION DE LOS CENTROS DE INSPECCION
     *************************/
    public static final Long INSPECTION_LOCATION_USER_ADDRESS = 0L;
    public static final Long INSPECTION_LOCATION_TORRE_WEWORK = 1L;
    public static final Long INSPECTION_LOCATION_LERMA = 2L;
    public static final Long INSPECTION_LOCATION_PONIENTE = 3L;
    public static final Long INSPECTION_LOCATION_PEDREGAL = 4L;

    /***************************
     * CUANDO EN LA BD TIENE UN CAMPO EN BLANCO O NULL
     *********************************/
    public static final String NO_VALUE_BD = "NA";

    /************
     * INDICADOR QUE EL INSPECTOR NO TOMO LA FOTO AL MOMENTO DE LA INSPECCION
     *************************/
    public static final String NO_PHOTO_INSPECTOR = "NO_INSPECTOR";

    /************
     * VALORES PARA LOS DIFERENTES SIZE DE LA IMAGEN POSTPHOTO
     *****************************************/
    public static final String SLIDER = "slider_";
    public static final String THUMB = "thumb_";
    public static final String GRID = "grid_";
    public static final String LIST = "list_";

    /************
     * VALORES CATEGORIAS DE LOS CAR_FEATURES InspectionCarFeatureItem
     **********************************/
    public static final Long CATEGORY_ENTERTAINMENT = 1L;
    public static final Long CATEGORY_EXTERIOR = 2L;
    public static final Long CATEGORY_INTERIOR = 3L;
    public static final Long CATEGORY_MECHANICAL = 4L;
    public static final Long CATEGORY_SAFETY = 5L;
    public static final Long CATEGORY_OTHERS = 6L;

    /************
     * VALORES DIMPLE_LOCATION EN SellCarDimple
     **********************************************************/
    public static final String EXT = "ext";
    public static final String INNER = "inner";

    /************
     * VALORES KAVAKPRICE EN ProductCarCompareSection
     **************************************************/
    public static final String AVGPRICE = "PRECIO PROMEDIO";
    public static final String INSPECT240POINTS = "INSPECCIÓN DE 240 PTS";
    public static final String WARRANTYKAVAK = "GARANTÍA KAVAK";
    public static final String NOHAVEPRICE = "NO TIENE PRECIO";

    /************
     * VALORES DE GROUPCATEGORY EN MAETAVLUE PAARA INSURANCE AND WARRANTYS
     *******************************/
    public static final String GROUP_CATEGORY_INSURANCE = "SEGUROS";
    public static final String GROUP_CATEGORY_MAPFRE = "MAPFRE";
    public static final String GROUP_NAME_DETAIL_WARRANTY = "DETALLE GARANTIA";

    /************
     * VALORES KEYS PARA PLATILLAS EMAILS
     *****************************************************************/
    public static final String KEY_EMAIL_USER_NAME = "@USER_NAME";
    public static final String KEY_EMAIL_MESSAGE = "@MESSAGE";
    public static final String KEY_EMAIL_CURRENT_YEAR = "@CURRENT_YEAR";
    public static final String KEY_EMAIL_CAR_NAME = "@CAR_NAME";
    public static final String KEY_EMAIL_CAR_KM = "@CAR_KM";
    public static final String KEY_EMAIL_CAR_PRICE = "@CAR_PRICE";
    public static final String KEY_EMAIL_WARRANTY = "@WARRANTY";
    public static final String KEY_EMAIL_APPOINTMENT_DATE = "@APPOINTMENT_DATE";
    public static final String KEY_EMAIL_APPOINTMENT_HOUR = "@APPOINTMENT_HOUR";
    // public static final String KEY_EMAIL_APPOINTMENT_BLOCK =
    // "@APPOINTMENT_BLOCK";
    public static final String KEY_EMAIL_APPOINTMENT_ADDRESS = "@APPOINTMENT_ADDRESS";
    public static final String KEY_EMAIL_CAR_IMAGE = "@CAR_IMAGE";
    public static final String KEY_EMAIL_USER_EMAIL = "@USER_EMAIL";
    public static final String KEY_EMAIL_USER_PHONE = "@USER_PHONE";
    public static final String KEY_EMAIL_CAR_URL = "@CAR_URL";
    public static final String KEY_EMAIL_PREFIX = "@PREFIX";
    public static final String KEY_SUBJECT = "subject";
    public static final String KEY_EMAIL_INSURANCE = "@INSURANCE";
    public static final String KEY_EMAIL_FINANCING = "@FINANCING";
    public static final String KEY_EMAIL_AGREEMENT = "@AGREEMENT";
    public static final String KEY_EMAIL_REFERENCE = "@REFERENCE";
    public static final String KEY_EMAIL_CLABE = "@CLABE";
    public static final String KEY_EMAIL_TRANSFER_AMOUNT = "@TRANSFER_AMOUNT";
    public static final String KEY_EMAIL_PAYMENT_DESCRIPTION = "@PAYMENT_DESCRIPTION";
    public static final String KEY_EMAIL_URL_RECEIPT = "@URL_RECEIPT";
    public static final String KEY_EMAIL_BOOKING_PRICE = "@BOOKING_PRICE";
    public static final String KEY_EMAIL_URL_KAVAK = "@URL_KAVAK";
    public static final String KEY_EMAIL_CAR_ID = "@CAR_ID";
    public static final String KEY_EMAIL_CAR_PLATE_STATE = "@CAR_PLATE_STATE";
    public static final String KEY_EMAIL_CAR_VIN = "@CAR_VIN";
    public static final String KEY_EMAIL_CAR_REPUVE = "@CAR_REPUVE";
    public static final String KEY_EMAIL_USER_ID = "@USER_ID";
    public static final String KEY_EMAIL_USER_ZIPCODE = "@USER_ZIPCODE";
    public static final String KEY_EMAIL_USER_IS_ACCEPTED_OFFER = "@USER_IS_ACCEPTED_OFFER";
    public static final String KEY_EMAIL_USER_ACCEPTED_OFFER_TYPE = "@USER_ACCEPTED_OFFER_TYPE";
    public static final String KEY_EMAIL_THIRTY_OFFER_MAX = "@THIRTY_OFFER_MAX";
    public static final String KEY_EMAIL_THIRTY_OFFER_MIN = "@THIRTY_OFFER_MIN";
    public static final String KEY_EMAIL_INSTANT_OFFER_MAX = "@INSTANT_OFFER_MAX";
    public static final String KEY_EMAIL_INSTANT_OFFER_MIN = "@INSTANT_OFFER_MIN";
    public static final String KEY_EMAIL_CONSIGNMENT_OFFER_MAX = "@CONSIGNMENT_OFFER_MAX";
    public static final String KEY_EMAIL_CONSIGNMENT_OFFER_MIN = "@CONSIGNMENT_OFFER_MIN";
    public static final String KEY_EMAIL_USER_PASSWORD = "@USER_PASSWORD";
    public static final String KEY_EMAIL_LOGIN_URL = "@URL_LOGIN";
    public static final String KEY_EMAIL_NETSUITE_OPPORTUNITY_URL = "@NETSUITE_OPPORTUNITY_URL";
    public static final String KEY_EMAIL_PENDING_STATUS = "@PENDING_STATUS";
    public static final String KEY_EMAIL_ADDRESS_DESC = "@APPOINTMENT_ADDRESS_DESC";
    public static final String KEY_EMAIL_ADDRESS_TYPE = "@APPOINTMENT_ADDRESS_TYPE";
    public static final String KEY_EMAIL_APPOINTMENT_SCHEDULE = "@APPOINTMENT_SCHEDULE";
    public static final String KEY_EMAIL_URL_CAR_INVOICE = "@URL_CAR_INVOICE";
    public static final String KEY_EMAIL_URL_CIRCULATION_CARD = "@URL_CIRCULATION_CARD";
    public static final String KEY_EMAIL_OFFERS_QUANTITY = "@OFFERS_QUANTITY";
    public static final String KEY_EMAIL_WISHLIST = "@WISHLIST";
    public static final String KEY_EMAIL_SOURCE = "@SOURCE";
    public static final String KEY_EMAIL_USER_LAST_NAME = "@USER_LAST_NAME";
    public static final String KEY_EMAIL_CAR_MAKE = "@CAR_MAKE";
    public static final String KEY_EMAIL_CAR_MODEL = "@CAR_MODEL";
    public static final String KEY_EMAIL_CAR_YEAR = "@CAR_YEAR";
    public static final String KEY_EMAIL_CAR_VERSION = "@CAR_VERSION";
    public static final String KEY_EMAIL_INSPECTION_DATE = "@INSPECTION_DATE";
    public static final String KEY_EMAIL_INSPECTION_HOUR = "@INSPECTION_HOUR";
    public static final String KEY_EMAIL_INSPECTION_PLACE = "@INSPECTION_PLACE";
    public static final String KEY_EMAIL_OFFER_MAX_30_DAYS = "@OFFER_MAX_30_DAYS";
    public static final String KEY_EMAIL_OFFER_MIN_30_DAYS = "@OFFER_MIN_30_DAYS";
    public static final String KEY_EMAIL_OFFER_INSPECTION = "@OFFER_INSPECTION";
    public static final String KEY_EMAIL_URL_RECOVER_OFFER = "@URL_RECOVER_OFFER";
    public static final String KEY_EMAIL_PAYMENT_TYPE = "@PAYMENT_TYPE";
    public static final String KEY_EMAIL_CHECKOUT_RESULT = "@CHECKOUT_RESULT";
    public static final String KEY_EMAIL_INTERNAL_USER_NAME = "@INTERNAL_USER_NAME";
    public static final String KEY_EMAIL_INTERNAL_USER_PHONE = "@INTERNAL_USER_PHONE";
    public static final String KEY_EMAIL_INTERNAL_USER_EMAIL = "@INTERNAL_USER_EMAIL";
    public static final String KEY_EMAIL_TEAM_KAVAK = "@TEAM_KAVAK";
    public static final String KEY_EMAIL_SELLER_ID = "@SELLER_ID";
    
    public static final String KEY_EMAIL_DELIVERY_DATE = "@KEY_EMAIL_DELIVERY_DATE";
    public static final String KEY_EMAIL_CURRENT_PLATE_STATUS = "@KEY_EMAIL_CURRENT_PLATE_STATUS";
    public static final String KEY_EMAIL_FINAL_PLATE_STATUS = "@KEY_EMAIL_FINAL_PLATE_STATUS";
    public static final String KEY_EMAIL_URL_NETSUITE_PAPERWORK = "@KEY_EMAIL_URL_NETSUITE_PAPERWORK";
    public static final String KEY_EMAIL_PAPERWORK_TYPE = "@KEY_EMAIL_PAPERWORK_TYPE";
    public static final String KEY_EMAIL_SHIPPING_ADDRESS = "@KEY_EMAIL_SHIPPING_ADDRESS";
    public static final String KEY_EMAIL_SHIPPING_MANAGER = "@KEY_EMAIL_MANAGER_SHIPPING";
    public static final String KEY_EMAIL_SHIPPING_COURRIER = "@KEY_EMAIL_SHIPPING_COURRIER";
    public static final String KEY_EMAIL_PERSON_WHO_RECEIVE = "@KEY_EMAIL_PERSON_WHO_RECEIVE";
    public static final String KEY_EMAIL_STARTED_DATE = "@KEY_EMAIL_STARTED_DATE";
    public static final String KEY_EMAIL_TENTATIVE_DATE = "@KEY_EMAIL_TENTATIVE_DATE";
    public static final String KEY_EMAIL_TRACKING_NUMBER = "@KEY_EMAIL_TRACKING_NUMBER";
    public static final String KEY_EMAIL_TRACKING_URL = "@KEY_EMAIL_TRACKING_URL";
    public static final String KEY_EMAIL_CANCELLED_RESERVATIONS_LEADS = "@KEY_EMAIL_CANCELLED_RESERVATIONS_LEADS";
    public static final String KEY_EMAIL_RETURN_VALIDITY = "@KEY_EMAIL_RETURN_VALIDITY";
    public static final String KEY_EMAIL_EMAIL_TEMPLATE = "@KEY_EMAIL_EMAIL_TEMPLATE";
    public static final String KEY_EMAIL_TITLE_PAPERWORK_SHIPPING_MANAGER = "@TITLE_PAPERWORK_SHIPPING_MANAGER";
    public static final String KEY_EMAIL_CUSTOMER_REVIEW_SERVICE_URL = "@KEY_EMAIL_CUSTOMER_REVIEW_SERVICE_URL";
    public static final String KEY_EMAIL_CUSTOMER_REVIEW_SEO_URL = "@KEY_EMAIL_CUSTOMER_REVIEW_SEO_URL";
    public static final String KEY_EMAIL_CAR_FULL_VERSION = "@CAR_FULL_VERSION";
    public static final String KEY_EMAIL_AFTER_SALE_KAVAKO_NAME = "@KEY_EMAIL_AFTER_SALE_KAVAKO_NAME";
    public static final String KEY_EMAIL_AFTER_SALE_KAVAKO_PHONE = "@KEY_EMAIL_AFTER_SALE_KAVAKO_PHONE";
    public static final String KEY_EMAIL_AFTER_SALE_KAVAKO_EMAIL = "@KEY_EMAIL_AFTER_SALE_KAVAKO_EMAIL";
    public static final String KEY_EMAIL_AFTER_SALE_KAVAKO_PHOTO = "@KEY_EMAIL_AFTER_SALE_KAVAKO_PHOTO";
    public static final String KEY_EMAIL_PAPERWORK_KAVAKO_NAME = "@KEY_EMAIL_PAPERWORK_KAVAKO_NAME";
    public static final String KEY_EMAIL_PAPERWORK_KAVAKO_PHONE= "@KEY_EMAIL_PAPERWORK_KAVAKO_PHONE";
    public static final String KEY_EMAIL_PAPERWORK_KAVAKO_EMAIL= "@KEY_EMAIL_PAPERWORK_KAVAKO_EMAIL";
    public static final String KEY_EMAIL_PAPERWORK_KAVAKO_PHOTO= "@KEY_EMAIL_PAPERWORK_KAVAKO_PHOTO";
    public static final String KEY_EMAIL_GENERIC = "@GENERIC";
    public static final String KEY_EMAIL_CAR_LOGO = "@KEY_EMAIL_CAR_LOGO";
    public static final String KAVAK_EMAIL_DOMAIN = "@kavak.com";
    public static final String KEY_EMAIL_USER_RANKING = "@KEY_EMAIL_USER_RANKING";
    public static final String KEY_EMAIL_USER_COMMENTS_RANKING = "@KEY_EMAIL_USER_COMMENTS_RANKING";
    public static final String KEY_EMAIL_URL_KAVAKO_PHOTO_MOBILE = "@URL_KAVAKO_PHOTO_MOBILE";
    public static final String KEY_EMAIL_URL_HEADER_CONFIRMED_INSPECTION = "@URL_HEADER_CONFIRMED_INSPECTION";
    public static final String KEY_EMAIL_INSPECTOR_PHOTO_DESKTOP = "@INSPECTOR_PHOTO_DESKTOP";
    public static final String KEY_EMAIL_INSPECTOR_PHOTO_MOBILE = "@INSPECTOR_PHOTO_MOBILE";
    public static final String KEY_EMAIL_INSPECTOR_NAME = "@INSPECTOR_NAME";
    public static final String KEY_EMAIL_INSPECTOR_PHONE = "@INSPECTOR_PHONE";
    public static final String KEY_EMAIL_INSPECTOR_EMAIL_ADDRESS = "@LEAD_EMAIL_ADDRESS";
    public static final String KEY_EMAIL_INSPECTOR_EMAIL_URL = "@INSPECTOR_EMAIL_URL";
    public static final String KEY_EMAIL_INSPECTOR_WHATSAPP_URL = "@INSPECTOR_WHATSAPP_URL";
    public static final String KEY_EMAIL_LEAD_PHOTO_DESKTOP = "@LEAD_PHOTO_DESKTOP";
    public static final String KEY_EMAIL_LEAD_PHOTO_MOBILE = "@LEAD_PHOTO_MOBILE";
    public static final String KEY_EMAIL_LEAD_NAME = "@LEAD_NAME";
    public static final String KEY_EMAIL_LEAD_PHONE = "@LEAD_PHONE";
    public static final String KEY_EMAIL_LEAD_EMAIL_ADDRESS = "@LEAD_EMAIL_ADDRESS";
    public static final String KEY_EMAIL_LEAD_EMAIL_URL = "@LEAD_EMAIL_URL";
    public static final String KEY_EMAIL_LEAD_WHATSAPP_URL = "@LEAD_WHATSAPP_URL";
    public static final String KEY_EMAIL_WINGMAN_PHOTO_DESKTOP = "@WINGMAN_PHOTO_DESKTOP";
    public static final String KEY_EMAIL_WINGMAN_PHOTO_MOBILE = "@WINGMAN_PHOTO_MOBILE";
    public static final String KEY_EMAIL_WINGMAN_NAME = "@WINGMAN_NAME";
    public static final String KEY_EMAIL_WINGMAN_PHONE = "@WINGMAN_PHONE";
    public static final String KEY_EMAIL_WINGMAN_EMAIL_ADDRESS = "@WINGMAN_EMAIL_ADDRESS";
    public static final String KEY_EMAIL_WINGMAN_EMAIL_URL = "@WINGMAN_EMAIL_URL";
    public static final String KEY_EMAIL_WINGMAN_WHATSAPP_URL = "@WINGMAN_WHATSAPP_URL";
    public static final String KEY_EMAIL_FIRST_OFFER_TITLE = "@FIRST_OFFER_TEXT";
    public static final String KEY_EMAIL_FIRST_OFFER_PRICE = "@FIRST_OFFER_PRICE";
    public static final String KEY_EMAIL_SECOND_OFFER_TITLE = "@SECOND_OFFER_TEXT";
    public static final String KEY_EMAIL_SECOND_OFFER_PRICE = "@SECOND_OFFER_PRICE";
    public static final String KEY_EMAIL_THIRD_OFFER_TITLE = "@THIRD_OFFER_TEXT";
    public static final String KEY_EMAIL_THIRD_OFFER_PRICE = "@THIRD_OFFER_PRICE";
    public static final String KEY_EMAIL_THIRTY_DAYS_OFFER = "OFERTA A 30 DÍAS";
    public static final String KEY_EMAIL_INSTANT_OFFER = "COMPRA INMEDIATA";
    public static final String KEY_EMAIL_CONSIGMENT_OFFER = "OFERTA CONSIGNACIÓN";
    public static final String KEY_EMAIL_COLOR_SECTION_FIRST_OFFER = "@COLOR_SECTION_FIRST_OFFER";
    public static final String KEY_EMAIL_COLOR_SECTION_SECOND_OFFER = "@COLOR_SECTION_SECOND_OFFER";
    public static final String KEY_EMAIL_COLOR_SECTION_THIRD_OFFER = "@COLOR_SECTION_THIRD_OFFER";
    public static final String KEY_EMAIL_COLOR_SECTION_DEFAULT_OFFER = "#FFFFFF";
    public static final String KEY_EMAIL_COLOR_SECTION_SELECTED_OFFER = "F6F6F6";
        public static final String KEY_EMAIL_FINANCING_URL_BURO = "@KEY_EMAIL_FINANCING_URL_BURO";
    public static final String KEY_EMAIL_FINANCING_TEXT_SCORE = "@KEY_EMAIL_FINANCING_TEXT_SCORE";
    public static final String KEY_EMAIL_FINANCING_SCORE = "@KEY_EMAIL_FINANCING_SCORE";
    public static final String KEY_EMAIL_FINANCING_MAXIMUN_FINANCING_AMOUNT = "@KEY_EMAIL_FINANCING_MAXIMUN_FINANCING_AMOUNT";
    public static final String KEY_EMAIL_FINANCING_PREAPPROVED_AMOUNT = "@KEY_EMAIL_FINANCING_PREAPPROVED_AMOUNT";
    public static final String KEY_EMAIL_FINANCING_DOWN_PAYMENT = "@KEY_EMAIL_FINANCING_DOWN_PAYMENT";
    public static final String KEY_EMAIL_FINANCING_MONTHLY_PAYMENT = "@KEY_EMAIL_FINANCING_MONTHLY_PAYMENT";
    public static final String KEY_EMAIL_FINANCING_USER_NAME = "@KEY_EMAIL_FINANCING_USER_NAME";
    public static final String KEY_EMAIL_FINANCING_USER_EMAIL = "@KEY_EMAIL_FINANCING_USER_EMAIL";
    public static final String KEY_EMAIL_FINANCING_USER_PHONE = "@KEY_EMAIL_FINANCING_USER_PHONE";
    public static final String KEY_EMAIL_USER_COMMENTS = "@KEY_EMAIL_USER_COMMENTS";
    public static final String KEY_EMAIL_ID_REGISTER = "@KEY_EMAIL_ID_REGISTER";
        public static final String KEY_EMAIL_URL_RECOVER_PASSWORD = "@URL_RECOVER_PASSWORD";
    
    
    /************
     * VALORES PARA LEAD DE MINERVA (ES DECIR EL ID DE USUARIO + 1000)
     **********************************/

    public static final long USER_ID_OFFSET = 1000L;

    // /************ URL BASe PARA LAS PLANTILLAS DE LOS EMAILS
    // *******************************************************/
    // public static final String URL_EMAIL_TEMPLATES =
    // "templates/template_prueba.html";

    /************
     * VALORES PARA BSUSQUEDA EN LA TABLA FORMOPTIONVALUE
     *************************************************/
    public static final String CATEGORY_PERSOSN_TYPE = "TIPO_PERSONA";
    public static final String CATEGORY_IDENTIFICATION_TYPE = "TIPO_IDENTIFICACION";
    public static final String CATEGORY_EDUCATION_TYPE = "TIPO_EDUCACION";
    public static final String CATEGORY_MARITAL_STATUS = "ESTADO_CIVIL";
    public static final String CATEGORY_NACIONALITY_TYPE = "TIPO_NACIONALIDAD";
    public static final String CATEGORY_GENDERS = "SEXO";
    public static final String CATEGORY_DEPENDENTS = "DEPENDIENTES";
    public static final String CATEGORY_BINARY = "SI/NO";
    public static final String CATEGORY_PROPERTY_STATUS = "SITUACION_VIVIENDA";
    public static final String CATEGORY_COMPANY_TYPE = "TIPO_EMPRESA";
    public static final String CATEGORY_INSURANCE_AFFILIATION = "AFILIACION_SEGURO";
    public static final String CATEGORY_POSITION_TYPE = "TIPO_CARGO";
    public static final String CATEGORY_PROFESSION_TYPE = "TIPO_PROFESION";
    public static final String CATEGORY_FAMILIAR_REFERENCE = "PARENTESCO_FAMILIAR";
    public static final String CATEGORY_PERSONAL_REFERENCE = "PARENTESCO_PERSONAL";
    public static final String CATEGORY_DELAY = "FINANCIAMIENTO_COMPROBAR_RETRASO";
    public static final String CATEGORY_CAR_USE = "FINANCIAMIENTO_USO_AUTO";
    public static final String CATEGORY_INCOME = "FINANCIAMIENTO_COMPROBAR_INGRESO";
    public static final String CATEGORY_PAYMENT_TPE = "FORMA_PAGO_AUTO";

    /************
     * VALORES PARA VALIDAR UN EMAIL
     *********************************************************************/
    public static final String PATTERN_EMAIL = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

    /************
     * VALORES PARA TIPO DE RED SOCIAL
     *******************************************************************/
    public static final Integer SOCIAL_NETWOK_GOOGLE = 1;
    public static final Integer SOCIAL_NETWOK_FACEBOOK = 2;

    /************
     * VALORES PARA ESTATUS DE USUARIO
     *******************************************************************/
    public static final boolean USER_ACTIVE = true;
    public static final boolean USER_INACTIVE = false;

    /************
     * VALORES CARSCOUT
     *******************************************************************/
    public static final Long CATEGORY_PRICE = 1L;
    public static final Long CATEGORY_KM = 2L;
    public static final Long CATEGORY_YEAR = 3L;
    public static final Long CATEGORY_BODY_TYPE = 4L;
    public static final Long CATEGORY_MAKE = 5L;
    public static final Long CATEGORY_MODEL = 6L;
    public static final Long CATEGORY_VERSION = 7L;
    public static final Long CATEGORY_COLOR = 8L;
    public static final Long CATEGORY_DOORS = 9L;
    public static final Long CATEGORY_SEATS = 10L;
    public static final Long CATEGORY_CYLINDERS = 11L;
    public static final Long CATEGORY_TRACTION = 12L;
    public static final Long CATEGORY_FUELTYPE = 13L;
    public static final Long CATEGORY_TRASMISSIONS = 14L;

    /************
     * BONITA DATA
     **************************************************************************************/
    public static final String BONITA_BPM_USER = systemConfiguration.getString("com.kavak.core.bpm.username");
    public static final String BONITA_BPM_PASSWORD = systemConfiguration.getString("com.kavak.core.bpm.password");
    public static final String BONITA_BPM_URL = systemConfiguration.getString("com.kavak.core.bpm.url.bonita");

    /***********************
     * CONSTANTES PARA INDICAR EL ORIGEN DE LAS OFERTAS
     *****************************/

    // Se colocan sus values en minúscula ya que es una implementación existente
    // y se encuentran registros en minuscula.
    public static final String SOURCE_IOS_APP  = "ios_app";
    public static final String SOURCE_ANDROID_APP = "android_app";
    public static final String SOURCE_APOLO    = "apolo";
    public static final String SOURCE_MINERVA  = "minerva";
    public static final String SOURCE_PRICING  = "pricing";
    public static final String SOURCE_NETSUITE = "netsuite";

    /*******************************
     * Constantes para OfertaCheckpoint
     *************************************/
    public static final String DIRECTION_CLIENT = "Direccion Cliente";

    /********************************
     * CONSTANTES PARA LOS DIAS DE LA SEMANA EN ESPAÑOL
     *********************/
    public static final String MONDAY = "Lunes";
    public static final String TUESDAY = "Martes";
    public static final String WEDNESDAY = "Miercoles";
    public static final String THURSDAY = "Jueves";
    public static final String FRIDAY = "Viernes";
    public static final String SATURDAY = "Sabado";
    public static final String SUNDAY = "Domingo";

    /********************************
     * CONSTANTES REGISTRO DE ME GUSTA, NO ME GUSTA
     *****************************************/
    public static final Long INTERESTED_CAR = 1L;
    public static final Long NO_INTERESTED_CAR = 0L;

    /********************************
     * CONSTANTES REGISTRO ACTIVOS O INACTIVOS EN BD
     *****************************************/
    public static final Long ACTIVE_DATA = 1L;
    public static final Long INACTIVE_DATA = 0L;

    /*****************************
     * TIPOS DE MENSAJES PARA ENVIO A CLIENTES DESDE BONITA
     **************************/
    public static final String TYPE_MSG_SMS = "SMS";

    /***************************
     * TIPO DE GRUPO DE ENVIOS
     **********************************************************/
    public static final String TYPE_GROUP_MSG = "SINGLE";

    /********************************
     * CONSTANTES REGISTRO DE DICCIONARIO PARA PASSWORD
     *****************************************/
    public static final String DICTIONARY = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    /***************************
     * CONSTANTES LOS TIPOS DE DOC GESTION SOLICITUDES
     **************************/
    public static final String FILE_TYPE_PDF = "pdf";
    public static final String FILE_TYPE_PNG = "png";
    public static final String FILE_TYPE_JPG = "jpg";
    public static final String FILE_TYPE_M4A = "m4a";

    /***************************
     * CONSTANTES PARA LAS VARIBLES DE PATH EN APPCOMPLAINCE
     ****************************/
    public static final String OPORTUNITY_SALE = "VENTA";
    public static final String OPORTUNITY_BUY = "COMPRA";
    public static final String DOCUMENT_OF_CLIENT = "Client";
    public static final String DOCUMENT_OF_OPPORTUNITY = "Opportunity";

    /********************************
     * CONSTANTES EN LA TABLA V2_PRODUCT_CAR_COMPARE_SECTION
     *****************************************/
    public static final String TRAMITES = "TRÁMITES";

    /********************************
     * CONSTANTE PARA CUANDO NO CONSIGUE EL PARAMETRO
     *****************************************/
    public static final String NOT_FOUND = "NOT FOUND:";

    /********************************
     * CONSTANTE PARA APPCOM FieldType
     *****************************************/
    public static final String LIST_TYPE = "List";

    /********************************
     * CONSTANTE PARA APPCOM Status Request
     *****************************************/
    public static final Long PENDING = 1L;
    public static final Long CONSIGNEDTOVALIDATE = 2L;
    public static final Long VALIDATEDCORRECT = 3L;
    public static final Long WRONGDOCUMENT = 4L;
    public static final Long DOESNOTHAVE = 5L;

    /********************************
     * CONSTANTE PARA APPCOM Question
     *****************************************/
    public static final Long MAX_COUNT_VALIDATION = 4L;
    
    /*****************************************
     * CONSTANTE PARA transaccional Open Pay
     *****************************************/
    public static final Long PAYMENT_DISCOUNT = 141L;
    public static final Long PAYMENT_FINANCING = 143L;
    public static final Long PAYMENT_72_HOURS = 144L;

    /*******************************
     *  ATTACHMENTS FOR EMAILS
     * ***************************************/
    
    public static final String ATTACHMENT_FILES = "@ATTACHMENT_FILES";
    public static final String ATTACHMENT_FILES_TEMP_FOLDER = "@ATTACHMENT_FILES_TEMP_FOLDER";
    
    /*******************************
     * CONSTANTES PARA POST ApiUrlShortener 
     *********************************/
    public static final String API_KEY_URL_SHORTENER = systemConfiguration.getString("com.kavak.core.UrlShortener.apiKey");
    
    /*******************************
     * CONSTANTES PARA URL CORTAS
     *********************************/
    //URL del catalogo   www.kavak.com/compra-de-autos
    public static final String URL_SHORTENER_CATALOGUE = "https://goo.gl/9r1q3m"; 
    
    /********************************
     * CONSTANTE PARA NOTIFICACIONES PUSH
     *****************************************/
    public static final String KEY_PUSH_TOKEN = "@TOKEN";
    
    /**************************************************************
     * CONSTANTE PARA CODIGOS DE REFERENCIA DE NOTIFICACIONES PUSH
     **************************************************************/
    public static final String KEY_PUSH_CODE_V204   = "V-204";
    public static final String KEY_PUSH_CODE_V201_1 = "V-201.1";
    public static final String KEY_PUSH_CODE_V201_2 = "V-201.2";
    public static final String KEY_PUSH_CODE_V201_3 = "V-201.3";
    public static final String KEY_PUSH_CODE_V207   = "V-207";
    public static final String KEY_PUSH_CODE_V201   = "V-201";
    public static final String KEY_PUSH_CODE_V206   = "V-206";
    
    /*******************************
     * CONSTANTES PARA PushNotification FireBase 
     *********************************/
    public static final String PUSH_FIREBASE_API_KEY = systemConfiguration.getString("com.kavak.core.appNotification.fireBase.apiKey");
    public static final String PUSH_FIREBASE_URL_SEND = systemConfiguration.getString("com.kavak.core.appNotification.fireBase.urlSend");

    /*******************************
     * CONSTANTES PARA IDENTIFICAR OPPORTUNITY
     *********************************/

    public static final String OPPORTUNITY_PURCHASE = "purchase";
    public static final String OPPORTUNITY_APPOINTMENT = "appointment";
    public static final String OPPORTUNITY_BOOKING = "booking";

    /****************************
     * URL BASE PARA IMAGENES
     **********************************************************/
    
    public static final String URL_BASE_IMAGES_CATALOGUE_CARS = "https://www.kavak.com/assets/uploads/list_";   // CATALOGO
    public static final String URL_BASE_IMAGES_PRODUCT_GALERY = "https://www.kavak.com/assets/uploads/slider_"; // GALERIA
    public static final String URL_BASE_IMAGES_KAVAKOS = URL_KAVAK +  "assets/kavak/email/kavakos/";            // KAVAKOS (SOLO FOTOS)
    public static final String URL_BASE_IMAGES_HEADER_CONFIRMED_INSPECTION = URL_KAVAK + "assets/kavak/email/header/confirmed_inspections/";
    public static final String URL_BASE_IMAGES_KAVAKOS_SIGNATURE_MOBILE = URL_BASE_IMAGES_KAVAKOS + "signature/mobile/";
    public static final String URL_BASE_IMAGES_KAVAKOS_SIGNATURE_DESKTOP = URL_BASE_IMAGES_KAVAKOS + "signature/desktop/";
	public static final String URL_BASE_IMAGES_KAVAKOS_DEFAULT = URL_KAVAK + "assets/kavak/email/kavakos/woman-avatar1.png";
    public static final String URL_BASE_IMAGES_INSPECTOR_DEFAULT = URL_KAVAK + "assets/kavak/email/inspectors/inspector.png";
	public static final String URL_BASE_IMAGES_LEAD_MANAGERS_DEFAULT = URL_KAVAK + "assets/kavak/email/lead_managers/lead.png";
	public static final String URL_BASE_IMAGES_WINGMAN_DEFAULT = URL_KAVAK + "assets/kavak/email/wingmen/wingman.png";
	
    /****************************
     * EMPRESAS DE ENVIO
     **********************************************************/
	
	public static final String FEDEX = "FEDEX";
	public static final String UPS = "UPS";
	public static final String AIRBORNE = "AIRBORNE";
	public static final String DHL = "DHL";
	
	public static final String BASE_COURRIER_TRACKING_NUMBER = "XXXXXXXXXX";
	public static final String BASE_COURRIER_NAME = "ZZZZZZZZZZ";
	public static final String URL_BASE_FEDEX_TRACKING_SEARCH = "https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber="+BASE_COURRIER_TRACKING_NUMBER+"&cntry_code=mx&locale=es_MX";
	public static final String URL_BASE_DHL_TRACKING_SEARCH = "http://www.dhl.com.mx/es/express/rastreo.html?AWB="+BASE_COURRIER_TRACKING_NUMBER+"&brand=DHL";
	public static final String URL_BASE_GOOGLE_SEARCH = "https://www.google.com.mx/search?q="+ BASE_COURRIER_NAME +"&oq=" + BASE_COURRIER_NAME;
	
    /****************************
     * EMPRESAS DE ENVIO
     **********************************************************/
	
	public static final String WHATSAPP_NUMBER_SECTION = "YYYYYYYYYY";
	public static final String WHATSAPP_MESSAGE_SECTION = "ZZZZZZZZZZ";
	public static final String WHATSAPP_MOBIL_API = "https://api.whatsapp.com/send?phone=" + WHATSAPP_NUMBER_SECTION + "&text=" + WHATSAPP_MESSAGE_SECTION;
	public static final String WHATSAPP_DESKTOP_API = "https://web.whatsapp.com/send?phone=" + WHATSAPP_NUMBER_SECTION + "&text=" + WHATSAPP_MESSAGE_SECTION;
	
	    /*******************************
     * CONSTANTES PARA Financing Arbol Financiero 
     *********************************/
    public static final String FINANCING_LIVING_TYPES = "LIVING_TYPES";
    public static final String FINANCING_INCOME_PROFILE = "INCOME_PROFILE";
    public static final String FINANCING_AUTOMOTIVE_CAR_USE_TYPES = "AUTOMOTIVE_CAR_USE_TYPES";
    public static final String FINANCING_MAXIMUM_VERIFY_USER_INFORMATION = "FMVUI";
    public static final String FINANCING_API_MARKET_RFC_GENERATOR_API_KEY = systemConfiguration.getString("com.kavak.core.financing.ApiMarketPlaceRfcGenerator");
    public static final String FINANCING_API_KEY = systemConfiguration.getString("com.kavak.core.financing.apiKey");
    public static final String FINANCING_API_URL = systemConfiguration.getString("com.kavak.core.financing.apiUrl");
    public static final String FINANCING_SECRET_KEY = systemConfiguration.getString("com.kavak.core.financing.secretKey");
    public static final String FINANCING_META_VALUE_ALIAS_FSIRM = "FSIRM";
    public static final String FINANCING_META_VALUE_ALIAS_FSIRB = "FSIRB";
    public static final String FINANCING_META_VALUE_ALIAS_FSIRE = "FSIRE";
    public static final String FINANCING_META_VALUE_ALIAS_FSDM =  "FSDM";
    public static final String FINANCING_META_VALUE_ALIAS_FSDB = "FSDB";
    public static final String FINANCING_META_VALUE_ALIAS_FSDE = "FSDE";
    public static final String FINANCING_META_VALUE_ALIAS_FSNM = "FSNM";
    public static final String FINANCING_META_VALUE_ALIAS_FSNB = "FSNB";
    public static final String FINANCING_META_VALUE_ALIAS_FSNE = "FSNE";
    public static final String FINANCING_META_VALUE_ALIAS_OTF = "OTF";
    public static final String FINANCING_META_VALUE_ALIAS_CPRSF = "CPRSF";
    
    	/*******************
	 * 
	 * CONSTANTES Offers
	 */
	
	public static final String RECOVER_OFFER_YES = "SI";
	public static final String RECOVER_OFFER_NO = "NO";
	
    
    /***********************************************************************
     * AB Test
     * 
     ***********************************************************************/
    public static final String AB_TEST_RECOVER_OFFER_CODE = "PCAB001";
    public static final String AB_TEST_CONTINUE_SELECT_OFFER_CODE = "PCAB002";
    public static final String AB_TEST_POST_OFFER = "AB Test Recuperar Oferta";
    
     
         /***********************************************************************
     * Migracion Apolo
     * 
     ***********************************************************************/
    public static final String META_VALUE_OPERATION_TYPE = "OPERATION_TYPE";
    public static final String PAGE_VALUE_SELLER_TITLE = "faq_sllr_heading";
    public static final String PAGE_VALUE_SELLER_ANSWER = "faq_slr_ansr";
    public static final String PAGE_VALUE_SELLER_QUESTION = "faq_slr_que";
    public static final String PAGE_VALUE_BUYER_TITLE = "faq_buyr_heading";
    public static final String PAGE_VALUE_BUYER_ANSWER = "faq_byr_ans";
    public static final String PAGE_VALUE_BUYER_QUESTION = "faq_byr-que";
    public static final String SELLER = "seller";
    public static final String BUYER = "buyer";
    
    
    //***********************************************************************
    public static final String OFFERT_OFF_PCOFFERSON = "PCOFFERSON";   
    public static final String PRICING_NEW_OFFER_AUDIT = "REOM0";
    public static final String PRICING_OFFER_AUDIT = "REAU0";

}