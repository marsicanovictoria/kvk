package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "v2_app_notification_setting")
public class AppNotificationSetting {

	private Long id;
	private boolean active;
	private Timestamp creationDate;
	private AppNotificationType appNotificationType;
	private User user;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "type_id", referencedColumnName = "id")
	public AppNotificationType getAppNotificationType() {
	    return appNotificationType;
	}

	public void setAppNotificationType(AppNotificationType appNotificationType) {
	    this.appNotificationType = appNotificationType;
	}

	@Column(name = "creation_date")
	public Timestamp getCreationDate() {
	    return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
	    this.creationDate = creationDate;
	}

	@Column(name = "is_active")
	public boolean isActive() {
	    return active;
	}

	public void setActive(boolean isActive) {
	    this.active = isActive;
	}

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
