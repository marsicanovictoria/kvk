package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Enrique on 6/28/2017.
 */
@Entity
@Table(name ="insurance_details")
public class InsuranceDetail {

    private Long id;
    private Long idInsurance;
    private String benefits;
    private String details;

    @Id
    @Column(name= "id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name= "insurance_id")
    public Long getIdInsurance() {
        return idInsurance;
    }
    public void setIdInsurance(Long idInsurance) {
        this.idInsurance = idInsurance;
    }

    @Column(name= "benefits")
    public String getBenefits() {
        return benefits;
    }
    public void setBenefits(String benefits) {
        this.benefits = benefits;
    }

    @Column(name= "details")
    public String getDetails() {
        return details;
    }
    public void setDetails(String details) {
        this.details = details;
    }

}
