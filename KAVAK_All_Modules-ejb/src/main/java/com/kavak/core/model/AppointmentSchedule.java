package com.kavak.core.model;

import javax.persistence.*;

import java.sql.Timestamp;

/**
 * Created by Enrique on 11-Jul-17.
 */
@Entity
@Table(name="horarios_citas")
public class AppointmentSchedule {

    private Long id;
    private Timestamp appoinmentDate;
    private Long idHourBlock;
    private int carStatus;
    private Timestamp selectionDate;
    private Long sellCarDetailId;
    private Long idUser;
    private String appointmentAddress;
    private String startDate;
    private String endDate;
    private Long appointmentLocationId;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(name = "fecha_cita")
    public Timestamp getAppoinmentDate() {
        return appoinmentDate;
    }

    public void setAppoinmentDate(Timestamp appoinmentDate) {
        this.appoinmentDate = appoinmentDate;
    }
    @Column(name = "bloque_hora")
    public Long getIdHourBlock() {
        return idHourBlock;
    }

    public void setIdHourBlock(Long idHourBlock) {
        this.idHourBlock = idHourBlock;
    }
    @Column(name = "estatus")
    public int getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(int carStatus) {
        this.carStatus = carStatus;
    }
    @Column(name = "fecha_seleccion")
    public Timestamp getSelectionDate() {
        return selectionDate;
    }

    public void setSelectionDate(Timestamp selectionDate) {
        this.selectionDate = selectionDate;
    }
    @Column(name = "id_auto")
    public Long getSellCarDetailId() {
        return sellCarDetailId;
    }
    public void setSellCarDetailId(Long sellCarDetailId) {
        this.sellCarDetailId = sellCarDetailId;
    }

    @Column(name = "id_usuario")
    public Long getIdUser() {
        return idUser;
    }
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    @Column(name = "direccion_cita")
    public String getAppointmentAddress() {
        return appointmentAddress;
    }
    public void setAppointmentAddress(String appointmentAddress) {
        this.appointmentAddress = appointmentAddress;
    }

    @Column(name = "hora_inicio")
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @Column(name = "hora_fin")
    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
   
    @Column(name = "appointment_location_id")
    public Long getAppointmentLocationId() {
        return appointmentLocationId;
    }

    public void setAppointmentLocationId(Long appointmentLocationId) {
        this.appointmentLocationId = appointmentLocationId;
    }

}
