package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="testimonials")
public class Testimonials {

	private Long id;
	private String clientDescription;
	private String idTestimanialVideo;
	private Long idSellCarDetail;
	private String userName;
	private String imageAddress;

	@Id
	@Column(name="id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="client_description")
	public String getClientDescription() {
		return clientDescription;
	}

	public void setClientDescription(String clientDescription) {
		this.clientDescription = clientDescription;
	}
	@Column(name="carro_id")
	public Long getIdSellCarDetail() {
		return idSellCarDetail;
	}

	public void setIdSellCarDetail(Long idSellCarDetail) {
		this.idSellCarDetail = idSellCarDetail;
	}
	@Column(name="video")
	public String getIdTestimanialVideo() {
		return idTestimanialVideo;
	}

	public void setIdTestimanialVideo(String idTestimanialVideo) {
		this.idTestimanialVideo = idTestimanialVideo;
	}
	
	@Column(name="client_name")
	public String getUserName() {
	    return userName;
	}

	public void setUserName(String userName) {
	    this.userName = userName;
	}

	@Column(name="image_address")
	public String getImageAddress() {
	    return imageAddress;
	}

	public void setImageAddress(String imageAddress) {
	    this.imageAddress = imageAddress;
	}

	

}
