package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "v2_carscout_alert_lkp_sku")
public class CarscoutAlertLkpSku {

	private Long id;
	private Long skudId;
	private CarscoutAlert carscoutAlert;
	private CarscoutCatalogue carscoutCatalogue;

	@Id
	@Column(name = "alert_sku_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "sku_id")
	public Long getSkudId() {
		return skudId;
	}

	public void setSkudId(Long skudId) {
		this.skudId = skudId;
	}

	@ManyToOne
	@JoinColumn(name = "alert_id")
	public CarscoutAlert getCarscoutAlert() {
		return carscoutAlert;
	}

	public void setCarscoutAlert(CarscoutAlert carscoutAlert) {
		this.carscoutAlert = carscoutAlert;
	}

	@ManyToOne
	@JoinColumn(name = "catalogue_id")
	public CarscoutCatalogue getCarscoutCatalogue() {
		return carscoutCatalogue;
	}

	public void setCarscoutCatalogue(CarscoutCatalogue carscoutCatalogue) {
		this.carscoutCatalogue = carscoutCatalogue;
	}
}
