package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.financing.FinancingUserDTO;

@Entity
@Table(name = "v2_financing_user")
public class FinancingUser {

    private Long id;
    private Long userId;
    private Long financingUserId;
    private String email;
    private String authenticationToken;
    private String fullName;
    private String firstName;
    private String secondName;
    private String gender;
    private String phone;
    private String rfc;
    private String birthday;
    private Timestamp buroDate;
    private Timestamp creationDate;;
    private boolean active;
    private String urlBuro;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Column(name = "financing_user_id")
    public Long getFinancingUserId() {
        return financingUserId;
    }

    public void setFinancingUserId(Long financingUserId) {
        this.financingUserId = financingUserId;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "authentication_token")
    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "second_name")
    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Column(name = "gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "rfc")
    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    @Column(name = "birthday")
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Column(name = "buro_date")
    public Timestamp getBuroDate() {
        return buroDate;
    }

    public void setBuroDate(Timestamp buroDate) {
        this.buroDate = buroDate;
    }

    @Column(name = "is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    @Column(name = "creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
    
    @Column(name = "buro_url")
    public String getUrlBuro() {
        return urlBuro;
    }

    public void setUrlBuro(String urlBuro) {
        this.urlBuro = urlBuro;
    }


    @Transient
    public FinancingUserDTO getDTO() {
	FinancingUserDTO financingUserDTO = new FinancingUserDTO();
	financingUserDTO.setId(this.id);
	financingUserDTO.setUserId(this.userId);
	financingUserDTO.setFinancingUserId(this.financingUserId);
	financingUserDTO.setEmail(this.email);
	financingUserDTO.setAuthenticationToken(this.authenticationToken);
	financingUserDTO.setFullName(this.fullName);
	financingUserDTO.setFirstName(this.firstName);
	financingUserDTO.setSecondName(this.secondName);
	financingUserDTO.setGender(this.gender);
	financingUserDTO.setPhone(this.phone);
	financingUserDTO.setRfc(this.rfc);
	financingUserDTO.setBirthday(this.birthday);
	financingUserDTO.setBuroDate(this.buroDate);
	financingUserDTO.setActive(this.active);
	financingUserDTO.setCreationDate(this.creationDate);

	return financingUserDTO;
    }



}