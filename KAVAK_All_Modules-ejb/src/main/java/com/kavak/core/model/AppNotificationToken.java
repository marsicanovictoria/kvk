package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "v2_app_notification_token")
public class AppNotificationToken {

	private Long id;
	private Long userId; 
	private String tokenType;
	private String deviceId;
	private String tokenDevice;
	private boolean isActive;
	private Timestamp creationDate;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "user_id")
	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}

	@Column(name = "token_type")
	public String getTokenType() {
	    return tokenType;
	}

	public void setTokenType(String tokenType) {
	    this.tokenType = tokenType;
	}
	
	@Column(name = "device_id")
	public String getDeviceId() {
	    return deviceId;
	}

	public void setDeviceId(String deviceId) {
	    this.deviceId = deviceId;
	}
	
	@Column(name = "token_device")
	public String getTokenDevice() {
	    return tokenDevice;
	}

	public void setTokenDevice(String tokenDevice) {
	    this.tokenDevice = tokenDevice;
	}
	
	@Column(name = "creation_date")
	public Timestamp getCreationDate() {
	    return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
	    this.creationDate = creationDate;
	}

	@Column(name = "is_active")
	public boolean isActive() {
	    return isActive;
	}

	public void setActive(boolean isActive) {
	    this.isActive = isActive;
	}

}
