package com.kavak.core.model;

import com.kavak.core.dto.InspectionDimpleDTO;
import com.kavak.core.dto.InspectionPointCarDTO;
import com.kavak.core.dto.InspectionRepairDTO;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "v2_inspection_point_car")
public class InspectionPointCar {
	
	private Long id;
	private Long idInspection;
	private Long idInspectioPointItem;
	private String inspectorEmail;
	private Long idInspectionPointResult;
	private Timestamp creationDate;
	private String additionalInfoDetail;
	private Long inspectionTypeId;
    private List<InspectionRepair> listInspectionRepair;
    private List<InspectionDimple> listInspectionDimple;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}

    @Column(name="inspection_point_item_id")
	public Long getIdInspectioPointItem() {
		return idInspectioPointItem;
	}
	public void setIdInspectioPointItem(Long idInspectioPointItem) {
		this.idInspectioPointItem = idInspectioPointItem;
	}

	@Column(name="inspection_point_result")
	public Long getIdInspectionPointResult() {
		return idInspectionPointResult;
	}
	public void setIdInspectionPointResult(Long idInspectionPointResult) {
		this.idInspectionPointResult = idInspectionPointResult;
	}

	@Column(name="creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name="inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}

	@Column(name="additional_info_detail")
	public String getAdditionalInfoDetail() {
		return additionalInfoDetail;
	}
	public void setAdditionalInfoDetail(String additionalInfoDetail) {
		this.additionalInfoDetail = additionalInfoDetail;
	}

	@Column(name="inspection_type_id")
	public Long getInspectionTypeId() {
		return inspectionTypeId;
	}
	public void setInspectionTypeId(Long inspectionTypeId) {
		this.inspectionTypeId = inspectionTypeId;
	}

    @OneToMany(mappedBy = "inspectionPointCar")
    public List<InspectionRepair> getListInspectionRepair() {
        return listInspectionRepair;
    }
    public void setListInspectionRepair(List<InspectionRepair> listInspectionRepair) {
        this.listInspectionRepair = listInspectionRepair;
    }

	@OneToMany(mappedBy = "inspectionPointCar")
	public List<InspectionDimple> getListInspectionDimple() {
		return listInspectionDimple;
	}
	public void setListInspectionDimple(List<InspectionDimple> listInspectionDimple) {
		this.listInspectionDimple = listInspectionDimple;
	}

	@Transient
    public InspectionPointCarDTO getDTO(){
        InspectionPointCarDTO newInspectionPointCarDTO = new InspectionPointCarDTO();
        newInspectionPointCarDTO.setId(this.id);
        newInspectionPointCarDTO.setIdInspection(this.idInspection);
        newInspectionPointCarDTO.setIdInspectioPointItem(this.idInspectioPointItem);
        newInspectionPointCarDTO.setInspectorEmail(this.inspectorEmail);
        newInspectionPointCarDTO.setIdInspectionPointResult(this.idInspectionPointResult);
        newInspectionPointCarDTO.setAdditionalInfoDetail(this.additionalInfoDetail);
        newInspectionPointCarDTO.setInspectionTypeId(this.inspectionTypeId);
        if(this.listInspectionRepair != null) {
            List<InspectionRepairDTO> listInspectionRepairDTO = new ArrayList<>();
            for(InspectionRepair inspectionRepairActual: this.listInspectionRepair){
                listInspectionRepairDTO.add(inspectionRepairActual.getDTO());
            }
            newInspectionPointCarDTO.setListInspectionRepairDTO(listInspectionRepairDTO);
        }
        if(this.listInspectionDimple != null) {
            List<InspectionDimpleDTO> listInspectionDimpleDTO = new ArrayList<>();
            for(InspectionDimple inspectionDimpleActual: this.listInspectionDimple){
                listInspectionDimpleDTO.add(inspectionDimpleActual.getDTO());
            }
            newInspectionPointCarDTO.setListInspectionDimpleDTO(listInspectionDimpleDTO);
        }
        return newInspectionPointCarDTO;
    }
}
