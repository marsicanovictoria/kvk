package com.kavak.core.model;

import javax.persistence.*;

/**
 * Created by Enrique on 05-Jun-17.
 */
@Entity
@Table(name= "puntos_inspeccion_auto")
public class ApoloInspectionPointCar {

    private Long id;
    private Long idSellCarDetail; // nombre para la columna
    private Long idApoloInspectionPointsItem;
    private boolean status;
    private Long idUser;
    private ApoloInspectionPointsItem apoloInspectionPointsItem;

    @Id
    @Column(name ="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name ="auto_id")
    public Long getIdSellCarDetail() {
        return idSellCarDetail;
    }
    public void setIdSellCarDetail(Long idSellCarDetail) {
        this.idSellCarDetail = idSellCarDetail;
    }

    @Column(name ="puntos_inspeccion_items_id", insertable= false, updatable=false)
    public Long getIdApoloInspectionPointsItem() {
        return idApoloInspectionPointsItem;
    }

    public void setIdApoloInspectionPointsItem(Long idApoloInspectionPointsItem) {
        this.idApoloInspectionPointsItem = idApoloInspectionPointsItem;
    }

    @Column(name ="estatus_punto_inspeccion")
    public boolean isStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }

    @Column(name ="usuario_id")
    public Long getIdUser() {
        return idUser;
    }
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    @ManyToOne
    @JoinColumn(name="puntos_inspeccion_items_id")
    public ApoloInspectionPointsItem getApoloInspectionPointsItem() {
        return apoloInspectionPointsItem;
    }
    public void setApoloInspectionPointsItem(ApoloInspectionPointsItem apoloInspectionPointsItem) {
        this.apoloInspectionPointsItem = apoloInspectionPointsItem;
    }


}
