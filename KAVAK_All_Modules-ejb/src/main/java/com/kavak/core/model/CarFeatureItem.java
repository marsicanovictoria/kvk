package com.kavak.core.model;

import com.kavak.core.dto.CarFeatureItemDTO;

import javax.persistence.*;

@Entity
@Table(name="v2_car_feature_item")
public class CarFeatureItem {

	private Long id;
	private Long idCategory;
	private String name;
	private String iconImage;
	private boolean active;
	private CarFeatureCategory carFeatureCategory;
	
	@Id
	@Column(name= "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name= "category_id",insertable = false, updatable = false)
	public Long getIdCategory() {
		return idCategory;
	}
	public void setIdCategory(Long idCategory) {
		this.idCategory = idCategory;
	}
	@Column(name= "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name= "icon_image")
	public String getIconImage() {
		return iconImage;
	}
	public void setIconImage(String iconImage) {
		this.iconImage = iconImage;
	}
	@Column(name= "active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@ManyToOne
	@JoinColumn(name="category_id")
	public CarFeatureCategory getCarFeatureCategory() {
		return carFeatureCategory;
	}
	public void setCarFeatureCategory(CarFeatureCategory carFeatureCategory) {
		this.carFeatureCategory = carFeatureCategory;
	}
	
	@Transient
	public CarFeatureItemDTO getDTO(){
		CarFeatureItemDTO carFeatureItemDTO = new CarFeatureItemDTO();
		carFeatureItemDTO.setId(this.id);
		carFeatureItemDTO.setName(this.name);
		return carFeatureItemDTO;
	}
	
}
