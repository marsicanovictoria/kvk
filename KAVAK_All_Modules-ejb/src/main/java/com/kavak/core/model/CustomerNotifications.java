package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "v2_customer_notifications")
public class CustomerNotifications {

	private Long id;
	private User user;
	private Long carId;
	private String userName;
	private Timestamp creationDate;
	private Timestamp updateDate;
	//private MetaValue metaValue;
	private Integer sendNotification;
	private boolean emailSent;
	private boolean carAppNotificationSent;
	private boolean carNotificationActiveAppNotificationSent;	
	private Long type;
	private String source;
	private boolean carNotificationSmsSent;
	private boolean cancelledCarBookedSmsSent;
	private Long internalUserId;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "car_id")
	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "update_date")
	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name = "send_notification")
	public Integer getSendNotification() {
		return sendNotification;
	}

	public void setSendNotification(Integer sendNotification) {
		this.sendNotification = sendNotification;
	}

	@Column(name = "email_sent")
	public boolean isEmailSent() {
		return emailSent;
	}

	public void setEmailSent(boolean emailSent) {
		this.emailSent = emailSent;
	}

	@Column(name = "type")
	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	@Column(name = "source")
	public String getSource() {
	    return source;
	}

	public void setSource(String source) {
	    this.source = source;
	}

	@Column(name = "car_notification_sms_sent")
	public boolean isCarNotificationSmsSent() {
	    return carNotificationSmsSent;
	}

	public void setCarNotificationSmsSent(boolean carNotificationSmsSent) {
	    this.carNotificationSmsSent = carNotificationSmsSent;
	}

	@Column(name = "cancelled_car_booked_sms_sent")
	public boolean isCancelledCarBookedSmsSent() {
	    return cancelledCarBookedSmsSent;
	}

	public void setCancelledCarBookedSmsSent(boolean cancelledCarBookedSmsSent) {
	    this.cancelledCarBookedSmsSent = cancelledCarBookedSmsSent;
	}

	@Column(name = "car_app_notification_sent")
	public boolean isCarAppNotificationSent() {
	    return carAppNotificationSent;
	}

	public void setCarAppNotificationSent(boolean carAppNotificationSent) {
	    this.carAppNotificationSent = carAppNotificationSent;
	}


	@Column(name = "car_notification_active_app_notification_sent")
	public boolean isCarNotificationActiveAppNotificationSent() {
	    return carNotificationActiveAppNotificationSent;
	}

	public void setCarNotificationActiveAppNotificationSent(boolean carNotificationActiveAppNotificationSent) {
	    this.carNotificationActiveAppNotificationSent = carNotificationActiveAppNotificationSent;
	}

	@Column(name = "internal_user_id")
	public Long getInternalUserId() {
	    return internalUserId;
	}

	public void setInternalUserId(Long internalUserId) {
	    this.internalUserId = internalUserId;
	}



}
