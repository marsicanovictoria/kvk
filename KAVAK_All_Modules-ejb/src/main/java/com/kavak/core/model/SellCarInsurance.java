package com.kavak.core.model;


import com.kavak.core.dto.SellCarInsuranceDTO;

import javax.persistence.*;

@Entity
@Table(name = "sell_car_insurance")
public class SellCarInsurance {

	private Long id;
	private Long idSellCarDetail;
	private Long idMetaValue;
	private Long insuranceValue;
	private SellCarDetail sellCarDetail;// Campo solo para la relacion ManyTone con @SellCarDetail
	
	@Id
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name = "sell_car_id",insertable= false, updatable=false)
	public Long getIdSellCarDetail() {
		return idSellCarDetail;
	}
	public void setIdSellCarDetail(Long idSellCarDetail) {
		this.idSellCarDetail = idSellCarDetail;
	}
	@Column(name = "meta_values_id")
	public Long getIdMetaValue() {
		return idMetaValue;
	}
	public void setIdMetaValue(Long idMetaValue) {
		this.idMetaValue = idMetaValue;
	}
	@Column(name = "value")
	public Long getInsuranceValue() {
		return insuranceValue;
	}
	public void setInsuranceValue(Long insuranceValue) {
		this.insuranceValue = insuranceValue;
	}
	@ManyToOne
	@JoinColumn(name="sell_car_id")
	public SellCarDetail getSellCarDetail() {
		return sellCarDetail;
	}
	public void setSellCarDetail(SellCarDetail sellCarDetail) {
		this.sellCarDetail = sellCarDetail;
	}
	
	@Transient
	public SellCarInsuranceDTO getDTO(){
		SellCarInsuranceDTO sellCarInsuranceDTO = new SellCarInsuranceDTO();
		sellCarInsuranceDTO.setId(this.id);
		sellCarInsuranceDTO.setIdSellCarDetail(this.idSellCarDetail);
		sellCarInsuranceDTO.setIdMetaValue(this.idMetaValue);
		sellCarInsuranceDTO.setInsuranceValue(this.insuranceValue);
		return sellCarInsuranceDTO;
	}
}
