package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "v2_user_task_assignment_weight")
public class UserTaskAssignmentWeight {

    private Long id;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
       this.id = id;
    }
}
