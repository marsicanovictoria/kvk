package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "v2_inspection_type")
public class InspectionType {

    private Long id;
    private String name;
    private boolean active;
    private Timestamp creatioDate;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name="is_active")
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }
    @Column(name="creation_date")
    public Timestamp getCreatioDate() {
        return creatioDate;
    }
    public void setCreatioDate(Timestamp creatioDate) {
        this.creatioDate = creatioDate;
    }
}
