//package com.kavak.core.openpay.model;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//import mx.openpay.client.Card;
//
//public class OpenPayTransaction {
//
//	private Double amount;
//	private String authorization;
//	private String method;
//	private String transactionType;
//	private String operationType;
//	private Card card;
//	private String status;
//	private String id;
//	private String creationDate;
//	private String description;
//	private String errorMessage;
//	private String orderId;
//
//	@JsonProperty("amount")
//	public Double getAmount() {
//		return amount;
//	}
//	public void setAmount(Double amount) {
//		this.amount = amount;
//	}
//	@JsonProperty("authorization")
//	public String getAuthorization() {
//		return authorization;
//	}
//	public void setAuthorization(String authorization) {
//		this.authorization = authorization;
//	}
//	@JsonProperty("method")
//	public String getMethod() {
//		return method;
//	}
//	public void setMethod(String method) {
//		this.method = method;
//	}
//	@JsonProperty("transaction_type")
//	public String getTransactionType() {
//		return transactionType;
//	}
//	public void setTransactionType(String transactionType) {
//		this.transactionType = transactionType;
//	}
//	@JsonProperty("operation_type")
//	public String getOperationType() {
//		return operationType;
//	}
//	public void setOperationType(String operationType) {
//		this.operationType = operationType;
//	}
//	@JsonProperty("card")
//	public Card getCard() {
//		return card;
//	}
//	public void setCard(Card card) {
//		this.card = card;
//	}
//	@JsonProperty("status")
//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
//	@JsonProperty("id")
//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//	@JsonProperty("transaction")
//	public String getCreationDate() {
//		return creationDate;
//	}
//
//	public void setCreationDate(String creationDate) {
//		this.creationDate = creationDate;
//	}
//	@JsonProperty("description")
//	public String getDescription() {
//		return description;
//	}
//
//	public void setDescription(String description) {
//		this.description = description;
//	}
//	@JsonProperty("error_message")
//	public String getErrorMessage() {
//		return errorMessage;
//	}
//
//	public void setErrorMessage(String errorMessage) {
//		this.errorMessage = errorMessage;
//	}
//	@JsonProperty("order_id")
//	public String getOrderId() {
//		return orderId;
//	}
//
//	public void setOrderId(String orderId) {
//		this.orderId = orderId;
//	}
//
//}
