package com.kavak.core.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="orders_transactions")
public class SaleCarOrderTransaction {
	
	private Long id;
	private String transactionId;
	private String orderId;
	private Long platformId;
	private String transactionType;
	private int status; //'0- No confirmada, 1- Confirmada, 2- Cancelada',
	private int paymentCompleted;
	private Timestamp confirmationDate;
	private Timestamp paymentDate;
	private Timestamp registerDate;
	private Long transactionAmount;
	private Long buyerId;
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="transaction_id")
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Column(name="order_id")
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name="platform_id")
	public Long getPlatformId() {
		return platformId;
	}
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}

	@Column(name="transaction_type")
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	@Column(name="status")
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name="is_payment_completed")
	public int isPaymentCompleted() {
		return paymentCompleted;
	}
	public void setPaymentCompleted(int paymentCompleted) {
		this.paymentCompleted = paymentCompleted;
	}

	@Column(name="confirmation_date")
	public Timestamp getConfirmationDate() {
		return confirmationDate;
	}
	public void setConfirmationDate(Timestamp confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	@Column(name="payment_date")
	public Timestamp getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Timestamp paymentDate) {
		this.paymentDate = paymentDate;
	}

	@Column(name="register_date")
	public Timestamp getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Timestamp registerDate) {
		this.registerDate = registerDate;
	}

	@Column(name="transaction_amount")
	public Long getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(Long transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	@Column(name="buyer_id")
	public Long getBuyerId() {
		return buyerId;
	}
	public void setBuyerId(Long buyerId) {
		this.buyerId = buyerId;
	}
}
