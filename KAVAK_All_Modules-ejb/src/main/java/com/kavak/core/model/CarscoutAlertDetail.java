package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "v2_carscout_alert_detail")
public class CarscoutAlertDetail {

	private Long id;
	private CarscoutAlert carscoutAlert;
	private CarscoutCategory carscoutCategory;
	private Integer paramId1;
	private Integer paramId2;

	@Id
	@Column(name = "alert_detail_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "alert_id")
	public CarscoutAlert getCarscoutAlert() {
		return carscoutAlert;
	}

	public void setCarscoutAlert(CarscoutAlert carscoutAlert) {
		this.carscoutAlert = carscoutAlert;
	}

	@ManyToOne
	@JoinColumn(name = "category_id")
	public CarscoutCategory getCarscoutCategory() {
		return carscoutCategory;
	}

	public void setCarscoutCategory(CarscoutCategory carscoutCategory) {
		this.carscoutCategory = carscoutCategory;
	}

	@Column(name = "param_id_1")
	public Integer getParamId1() {
		return paramId1;
	}

	public void setParamId1(Integer paramId1) {
		this.paramId1 = paramId1;
	}

	@Column(name = "param_id_2")
	public Integer getParamId2() {
		return paramId2;
	}

	public void setParamId2(Integer paramId2) {
		this.paramId2 = paramId2;
	}

}
