package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.CustomerNotificationQuestionAnswerDTO;

@Entity
@Table(name = "v2_customer_notification_question_answer")
public class CustomerNotificationQuestionAnswer {

    private Long id;
    private Long userId;
    private Long carId;
    private Long questionId;
    private Long optionId;
    private Timestamp updateDate;


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Column(name = "car_id")
    public Long getCarId() {
        return carId;
    }
    public void setCarId(Long carId) {
        this.carId = carId;
    }

    @Column(name = "question_id")
    public Long getQuestionId() {
        return questionId;
    }
    
    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }


    @Column(name = "option_id")
    public Long getOptionId() {
        return optionId;
    }

    public void setOptionId(Long optionId) {
        this.optionId = optionId;
    }

    @Column(name = "update_date")
    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    @Transient
    public CustomerNotificationQuestionAnswerDTO getDTO(){
	CustomerNotificationQuestionAnswerDTO customerNotificationQuestionAnswerDTO= new CustomerNotificationQuestionAnswerDTO();
	customerNotificationQuestionAnswerDTO.setId(this.id);
	customerNotificationQuestionAnswerDTO.setUserId(this.userId);
	customerNotificationQuestionAnswerDTO.setCarId(this.carId);
	customerNotificationQuestionAnswerDTO.setQuestionId(this.getQuestionId());
	customerNotificationQuestionAnswerDTO.setOptionId(this.getOptionId());
        return customerNotificationQuestionAnswerDTO;
    }
}
