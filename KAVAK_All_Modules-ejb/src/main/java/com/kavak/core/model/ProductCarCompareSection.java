package com.kavak.core.model;

import com.kavak.core.dto.ProductCarCompareSectionDTO;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Enrique on 09-Jun-17.
 */
@Entity
@Table(name = "v2_product_car_compare_section")
public class ProductCarCompareSection {

    private Long id;
    private String name;
    private String description;
    private String image;
    private String kavakPrice;
    private String marketPrice;
    private String savings;
    private Timestamp creationDate;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Column(name="description")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    @Column(name="image")
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    @Column(name="kavak_price")
    public String getKavakPrice() {
        return kavakPrice;
    }
    public void setKavakPrice(String kavakPrice) {
        this.kavakPrice = kavakPrice;
    }
    @Column(name="market_price")
    public String getMarketPrice() {
        return marketPrice;
    }
    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }
    @Column(name="savings")
    public String getSavings() {
        return savings;
    }
    public void setSavings(String savings) {
        this.savings = savings;
    }
    @Column(name="creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Transient
    public ProductCarCompareSectionDTO getDTO(){
        ProductCarCompareSectionDTO productCarCompareSectionDTO =  new ProductCarCompareSectionDTO();
        productCarCompareSectionDTO.setName(this.name);
        productCarCompareSectionDTO.setDescription(this.description);
        productCarCompareSectionDTO.setImage(this.image);
        productCarCompareSectionDTO.setKavakPrice(this.kavakPrice);
        productCarCompareSectionDTO.setMarketPrice(this.marketPrice);
        productCarCompareSectionDTO.setSavings(this.savings);
        return productCarCompareSectionDTO;
    }
}
