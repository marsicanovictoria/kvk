package com.kavak.core.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="v2_promotion")
public class Promotion {

    private Long id;
    private Long type; //  1 - Compra  2 - Venta
    private String name;
    private String color;
    private Date starDate;
    private Date endDate;
    private boolean active;
    private Date creationDate;
    private List<PromotionCarPrice> listPromotionCarPrice;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="type")
    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }
    @Column(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="promotion_color")
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Column(name="start_date")
    public Date getStarDate() {
        return starDate;
    }

    public void setStarDate(Date starDate) {
        this.starDate = starDate;
    }

    @Column(name="end_date")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column(name="is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name="creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @OneToMany(mappedBy = "promotion")
    public List<PromotionCarPrice> getListPromotionCarPrice() {
        return listPromotionCarPrice;
    }

    public void setListPromotionCarPrice(List<PromotionCarPrice> listPromotionCarPrice) {
        this.listPromotionCarPrice = listPromotionCarPrice;
    }

}
