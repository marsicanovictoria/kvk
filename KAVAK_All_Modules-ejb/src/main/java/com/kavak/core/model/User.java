package com.kavak.core.model;

import com.kavak.core.dto.UserDTO;
import com.kavak.core.util.Constants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "user")
public class User {

	private Long id;
	private String name;
	private String userName;
	private String email;
	private String password;
	private String role;
	private String source;
	private boolean active;
	private Timestamp creationDate;
	private String ipUsuario;
	private List<OfferCheckpoint> inspection;
	private List<UserMeta> listUserMeta;
	private boolean sentNetsuite;
	private String googlePlusId;
	private String facebookUserId;
	private List<FinancingData> financingData;
	private List<CustomerNotifications> customerNotifications;
	private List<CarscoutAlert> carscoutAlert;
	private List<AppNotificationSetting> listAppNotificationSetting;
	private boolean userRegisterSmsSent;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "username")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "role")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Column(name = "source")
	public String getSource() {
	    return source;
	}

	public void setSource(String source) {
	    this.source = source;
	}

	@Column(name = "is_active")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(name = "creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "ip_usuario")
	public String getIpUsuario() {
		return ipUsuario;
	}

	public void setIpUsuario(String ipUsuario) {
		this.ipUsuario = ipUsuario;
	}

	@OneToMany(mappedBy = "user")
	public List<UserMeta> getListUserMeta() {
		return listUserMeta;
	}

	public void setListUserMeta(List<UserMeta> listUserMeta) {
		this.listUserMeta = listUserMeta;
	}

	@OneToMany(mappedBy = "user")
	public List<OfferCheckpoint> getInspection() {
		return inspection;
	}

	public void setInspection(List<OfferCheckpoint> inspection) {
		this.inspection = inspection;
	}

	@Column(name = "enviado_netsuite")
	public boolean isSentNetsuite() {
		return sentNetsuite;
	}

	public void setSentNetsuite(boolean sentNetsuite) {
		this.sentNetsuite = sentNetsuite;
	}

	@Column(name = "google_plus_user_id")
	public String getGooglePlusId() {
		return googlePlusId;
	}

	public void setGooglePlusId(String googlePlusId) {
		this.googlePlusId = googlePlusId;
	}

	@Column(name = "facebook_user_id")
	public String getFacebookUserId() {
		return facebookUserId;
	}

	public void setFacebookUserId(String facebookUserId) {
		this.facebookUserId = facebookUserId;
	}

	@OneToMany(mappedBy = "user")
	public List<FinancingData> getFinancingData() {
		return financingData;
	}

	public void setFinancingData(List<FinancingData> financingData) {
		this.financingData = financingData;
	}

	@OneToMany(mappedBy = "user")
	public List<CustomerNotifications> getCustomerNotifications() {
		return customerNotifications;
	}

	public void setCustomerNotifications(List<CustomerNotifications> customerNotifications) {
		this.customerNotifications = customerNotifications;
	}

	@OneToMany(mappedBy = "user")
	public List<CarscoutAlert> getCarscoutAlert() {
		return carscoutAlert;
	}

	public void setCarscoutAlert(List<CarscoutAlert> carscoutAlert) {
		this.carscoutAlert = carscoutAlert;
	}
	
	@OneToMany(mappedBy = "user")
	public List<AppNotificationSetting> getListAppNotificationSetting() {
	    return listAppNotificationSetting;
	}

	public void setListAppNotificationSetting(List<AppNotificationSetting> listAppNotificationSetting) {
	    this.listAppNotificationSetting = listAppNotificationSetting;
	}

	@Column(name = "	user_register_sms_sent")
	public boolean isUserRegisterSmsSent() {
	    return userRegisterSmsSent;
	}

	public void setUserRegisterSmsSent(boolean userRegisterSmsSent) {
	    this.userRegisterSmsSent = userRegisterSmsSent;
	}

	@Transient
	public UserDTO getDTO() {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(this.id);
		userDTO.setName(this.name);
		userDTO.setUsername(this.userName);
		userDTO.setEmail(this.email);
		userDTO.setPassword(this.password);
		userDTO.setRole(this.role);
		userDTO.setActive(this.active);
		userDTO.setCreationDate(this.creationDate);
		userDTO.setIpUsuario(this.ipUsuario);
		userDTO.setgooglePlusId(this.googlePlusId);
		userDTO.setFacebookUserId(this.facebookUserId);
		

		if (getListUserMeta() != null) {
			for (UserMeta userMetaActual : getListUserMeta()) {
				if (userMetaActual.getMetaKey().equalsIgnoreCase(Constants.META_VALUE_PHONE)) {
					userDTO.setPhone(userMetaActual.getMetaValue());
				}
				if (userMetaActual.getMetaKey().equalsIgnoreCase(Constants.META_VALUE_ADDRESS)) {
					userDTO.setAddress(userMetaActual.getMetaValue());
				}
			}
		}

		StringBuilder lastName = new StringBuilder();
		String[] parts = userDTO.getName().split(" ");
		userDTO.setFirstName(parts[0]);
		if(parts.length >= 2){
		    for(int i=1; i<parts.length; i++) {
			lastName.append(parts[i] + " ");
		    }
		    userDTO.setLastName(lastName.toString().trim());
		}
		
		return userDTO;
	}



}