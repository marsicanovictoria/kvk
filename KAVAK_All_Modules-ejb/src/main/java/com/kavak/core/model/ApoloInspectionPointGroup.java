package com.kavak.core.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Enrique on 27-Jun-17.
 */
@Entity
@Table(name="puntos_inspeccion_grupos")
public class ApoloInspectionPointGroup {

    private Long id;
    private String name;
    private List<ApoloInspectionPointsItem> listApoloInspectionPointsItem;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="nombre")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @OneToMany(mappedBy="apoloInspectionPointGroup")
    public List<ApoloInspectionPointsItem> getListApoloInspectionPointsItem() {
        return listApoloInspectionPointsItem;
    }

    public void setListApoloInspectionPointsItem(List<ApoloInspectionPointsItem> listApoloInspectionPointsItem) {
        this.listApoloInspectionPointsItem = listApoloInspectionPointsItem;
    }
}
