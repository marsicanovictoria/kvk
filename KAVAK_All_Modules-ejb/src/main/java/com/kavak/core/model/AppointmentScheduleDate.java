package com.kavak.core.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.ScheduleDateDTO;

@Entity
@Table(name="horarios_citas")
public class AppointmentScheduleDate {
	
	private Long id;
	private Date visitDate;
	private Long hourSlot;
	private Long status;
	private Timestamp selectedDate;
	private Long carId;
	private Long userId;
	private String visitAddress;
	private boolean appointmentReminderNotificationSent;
	private String starHourAppointment;
	private String endHourAppointment;
	private Long appointmentLocationId;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="fecha_cita")
	public Date getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}
	
	@Column(name="bloque_hora")
	public Long getHourSlot() {
		return hourSlot;
	}
	public void setHourSlot(Long hourSlot) {
		this.hourSlot = hourSlot;
	}
	
	@Column(name="estatus")
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	
	@Column(name="fecha_seleccion")
	public Timestamp getSelectedDate() {
		return selectedDate;
	}
	public void setSelectedDate(Timestamp selectedDate) {
		this.selectedDate = selectedDate;
	}
	
	@Column(name="id_auto")
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	
	@Column(name="id_usuario")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Column(name="direccion_cita")
	public String getVisitAddress() {
		return visitAddress;
	}
	public void setVisitAddress(String visitAddress) {
		this.visitAddress = visitAddress;
	}
	
	@Column(name="appointment_reminder_notification_sent")
	public boolean isAppointmentReminderNotificationSent() {
	    return appointmentReminderNotificationSent;
	}
	public void setAppointmentReminderNotificationSent(boolean appointmentReminderNotificationSent) {
	    this.appointmentReminderNotificationSent = appointmentReminderNotificationSent;
	}
	
	@Column(name="hora_inicio")
	public String getStarHourAppointment() {
	    return starHourAppointment;
	}
	public void setStarHourAppointment(String starHourAppointment) {
	    this.starHourAppointment = starHourAppointment;
	}

	
	@Column(name="hora_fin")
	public String getEndHourAppointment() {
	    return endHourAppointment;
	}
	public void setEndHourAppointment(String endHourAppointment) {
	    this.endHourAppointment = endHourAppointment;
	}
	
	@Column(name="appointment_location_id")
	public Long getAppointmentLocationId() {
	    return appointmentLocationId;
	}
	public void setAppointmentLocationId(Long appointmentLocationId) {
	    this.appointmentLocationId = appointmentLocationId;
	}
	
	@Transient
	public ScheduleDateDTO getDTO(){
		
		ScheduleDateDTO scheduleDateDTO = new ScheduleDateDTO();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		scheduleDateDTO.setId(this.carId);
		scheduleDateDTO.setHourSlot(this.hourSlot);
		scheduleDateDTO.setStatus(this.status);
		scheduleDateDTO.setCarId(this.carId);
		scheduleDateDTO.setUserId(this.userId);
		scheduleDateDTO.setVisitAddress(this.visitAddress);
		
		if (this.visitDate != null){
			Date tDate = new Date(this.visitDate.getTime());
			String visitDate = formatter.format(tDate);
			
			scheduleDateDTO.setVisitDate(visitDate);
		}

		if (this.selectedDate != null){
			Date tDate = new Date(this.selectedDate.getTime());
			String selectedDate = formatter.format(tDate);
			
			scheduleDateDTO.setSelectedDate(selectedDate);
		}
		
		return scheduleDateDTO;
	}
	
	
	
}
