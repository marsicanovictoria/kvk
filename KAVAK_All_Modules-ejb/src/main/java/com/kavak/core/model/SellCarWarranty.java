package com.kavak.core.model;

import com.kavak.core.dto.SellCarWarrantyDTO;

import javax.persistence.*;

@Entity
@Table(name = "sell_car_warranty")
public class SellCarWarranty {

	private Long id;
	private Long idSellCarDetail;
	private Long idMetaValue;
	private Long warrantyValue;
	private SellCarDetail sellCarDetail;// Campo solo para la relacion ManyTone con @SellCarDetail
	
	@Id
	@Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "sell_car_id",insertable= false, updatable=false)
	public Long getIdSellCarDetail() {
		return idSellCarDetail;
	}
	public void setIdSellCarDetail(Long idSellCarDetail) {
		this.idSellCarDetail = idSellCarDetail;
	}

	@Column(name = "meta_values_id")
	public Long getIdMetaValue() {
		return idMetaValue;
	}
	public void setIdMetaValue(Long idMetaValue) {
		this.idMetaValue = idMetaValue;
	}

	@Column(name = "value")
	public Long getWarrantyValue() {
		return warrantyValue;
	}
	public void setWarrantyValue(Long warrantyValue) {
		this.warrantyValue = warrantyValue;
	}

	@ManyToOne
	@JoinColumn(name="sell_car_id")
	public SellCarDetail getSellCarDetail() {
		return sellCarDetail;
	}
	public void setSellCarDetail(SellCarDetail sellCarDetail) {
		this.sellCarDetail = sellCarDetail;
	}
	
	@Transient
	public SellCarWarrantyDTO getDTO(){
		SellCarWarrantyDTO sellCarWarrantyDTO = new SellCarWarrantyDTO();
		sellCarWarrantyDTO.setId(this.id);
		sellCarWarrantyDTO.setIdSellCarDetail(this.idSellCarDetail);
		sellCarWarrantyDTO.setIdMetaValue(this.idMetaValue);
		sellCarWarrantyDTO.setWarrantyValue(this.warrantyValue);
		return sellCarWarrantyDTO;
	}
}