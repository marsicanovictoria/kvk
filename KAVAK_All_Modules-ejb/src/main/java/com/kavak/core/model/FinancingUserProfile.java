package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.financing.FinancingUserProfileDTO;

@Entity
@Table(name = "v2_financing_user_profile")
public class FinancingUserProfile {

    private Long id;
    private String content;
    private String name;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
	return id;
    }
    public void setId(Long id) {
	this.id = id;
    }

    @Column(name = "content")
    public String getContent() {
	return content;
    }
    public void setContent(String content) {
	this.content = content;
    }

    @Column(name = "name")
    public String getName() {
	return name;
    }
    public void setName(String name) {
	this.name = name;
    }
    
    
    @Transient
    public FinancingUserProfileDTO getDTO() {
	FinancingUserProfileDTO financingUserProfileDTO = new FinancingUserProfileDTO();
	financingUserProfileDTO.setId(this.id);
	financingUserProfileDTO.setContent(this.content);
	financingUserProfileDTO.setName(this.name);

	return financingUserProfileDTO;
    }

}