package com.kavak.core.model;

import com.kavak.core.dto.InspectionSummaryDTO;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="v2_inspection_summary")
public class InspectionSummary {

	private Long id;
	private Long idInspection;
	private String inspectorEmail;
	private Long idOfferType;
	private Double baseOfferAmount;
	private Double totalDiscount;
	private Double finalOfferAmount;
	private boolean selected; 
	private Date creationDate;
	private boolean inspectionApproved;
	private Long inspectionStatus;
	private Long totalRepairsAmount;
	private Long totalBonusAmount;
	private String customerSignatureImage;
	private Long inspectionTypeId;
	private BigDecimal latitudeInspection;
    private BigDecimal longitudeInspection;
    private String inspectorComment;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}

	@Column(name="inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}

	@Column(name="offer_type_id")
	public Long getIdOfferType() {
		return idOfferType;
	}
	public void setIdOfferType(Long idOfferType) {
		this.idOfferType = idOfferType;
	}

	@Column(name="base_offer_amount")
	public Double getBaseOfferAmount() {
		return baseOfferAmount;
	}
	public void setBaseOfferAmount(Double baseOfferAmount) {
		this.baseOfferAmount = baseOfferAmount;
	}

	@Column(name="total_discounts")
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	@Column(name="final_offer_amount")
	public Double getFinalOfferAmount() {
		return finalOfferAmount;
	}
	public void setFinalOfferAmount(Double finalOfferAmount) {
		this.finalOfferAmount = finalOfferAmount;
	}

	@Column(name="is_selected")
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name="inspection_approved")
	public boolean isInspectionApproved() {
		return inspectionApproved;
	}
    public void setInspectionApproved(boolean inspectionApproved) {
		this.inspectionApproved = inspectionApproved;
	}

    @Column(name="total_repairs")
    public Long getTotalRepairsAmount() {
        return totalRepairsAmount;
    }
    public void setTotalRepairsAmount(Long totalRepairsAmount) {
        this.totalRepairsAmount = totalRepairsAmount;
    }

    @Column(name="total_bonus_amount")
    public Long getTotalBonusAmount() {
        return totalBonusAmount;
    }
    public void setTotalBonusAmount(Long totalBonusAmount) {
        this.totalBonusAmount = totalBonusAmount;
    }

	@Column(name="customer_signature_image")
	public String getCustomerSignatureImage() {
		return customerSignatureImage;
	}
	public void setCustomerSignatureImage(String customerSignatureImage) {
		this.customerSignatureImage = customerSignatureImage;
	}

	@Column(name="inspection_type_id")
	public Long getInspectionTypeId() {
		return inspectionTypeId;
	}
	public void setInspectionTypeId(Long inspectionTypeId) {
		this.inspectionTypeId = inspectionTypeId;
	}

    @Column(name="latitude_inspection")
    public BigDecimal getLatitudeInspection() {
        return latitudeInspection;
    }
    public void setLatitudeInspection(BigDecimal latitudeInspection) {
        this.latitudeInspection = latitudeInspection;
    }

    @Column(name="longitude_inspection")
    public BigDecimal getLongitudeInspection() {
        return longitudeInspection;
    }
    public void setLongitudeInspection(BigDecimal longitudeInspection) {
        this.longitudeInspection = longitudeInspection;
    }
    
    @Column(name="inspection_status")
	public Long getInspectionStatus() {
		return inspectionStatus;
	}
	public void setInspectionStatus(Long inspectionStatus) {
		this.inspectionStatus = inspectionStatus;
	}

	@Column(name="inspector_comment")
	public String getInspectorComment() {
	    return inspectorComment;
	}
	public void setInspectorComment(String inspectorComment) {
	    this.inspectorComment = inspectorComment;
	}


	@Transient
	public InspectionSummaryDTO getDTO(){
		InspectionSummaryDTO inspectionSummaryDTO = new InspectionSummaryDTO();
		inspectionSummaryDTO.setIdInspection(this.idInspection);
		inspectionSummaryDTO.setInspectorEmail(this.inspectorEmail);
		inspectionSummaryDTO.setInspectionApproved(this.inspectionApproved);
		inspectionSummaryDTO.setInspectionStatus(this.inspectionStatus);
		inspectionSummaryDTO.setBaseOfferAmount(this.baseOfferAmount);
		inspectionSummaryDTO.setTotalDiscount(this.totalDiscount);
		inspectionSummaryDTO.setTotalRepairsAmount(this.totalRepairsAmount);
		inspectionSummaryDTO.setTotalBonusAmount(this.totalBonusAmount);
		inspectionSummaryDTO.setFinalOfferAmount(this.finalOfferAmount);
		inspectionSummaryDTO.setSelected(this.selected);
		inspectionSummaryDTO.setIdOfferType(this.idOfferType);
		inspectionSummaryDTO.setCustomerSignatureImage(this.customerSignatureImage);

		return inspectionSummaryDTO;
	}

}
