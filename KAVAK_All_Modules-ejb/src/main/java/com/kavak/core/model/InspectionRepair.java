package com.kavak.core.model;

import com.kavak.core.dto.InspectionRepairDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="v2_inspection_repair")
public class InspectionRepair {
	
	private Long id;
	private Long idInspection;
	private Long idInspectionPointCar;
	private Long idInspectionRepairCategory;
	private Long idInspectionRepairSubCategory;
	private Long idInspectionRepairItem;
	private Long repairAmount;
	private Date creationDate;
    private String inspectorEmail;
	private boolean paidByKavak;
	private String repairImage;
	private Long inspectionTypeId;
    private InspectionPointCar inspectionPointCar; // Campo solo para asociar InspectionPointCar No se mapea en DTO
    private String repairOtherText;
    
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}

	@Column(name = "inspection_point_car_id", insertable = false ,  updatable = false)
	public Long getIdInspectionPointCar() {
		return idInspectionPointCar;
	}
	public void setIdInspectionPointCar(Long idInspectionPointCar) {
		this.idInspectionPointCar = idInspectionPointCar;
	}

	@Column(name = "inspection_point_repair_category_id")
	public Long getIdInspectionRepairCategory() {
		return idInspectionRepairCategory;
	}
	public void setIdInspectionRepairCategory(Long idInspectionRepairCategory) {
		this.idInspectionRepairCategory = idInspectionRepairCategory;
	}

	@Column(name = "inspection_point_repair_subcategory_id")
	public Long getIdInspectionRepairSubCategory() {
		return idInspectionRepairSubCategory;
	}
	public void setIdInspectionRepairSubCategory(Long idInspectionRepairSubCategory) {
		this.idInspectionRepairSubCategory = idInspectionRepairSubCategory;
	}

	@Column(name = "inspection_point_repair_item_id")
	public Long getIdInspectionRepairItem() {
		return idInspectionRepairItem;
	}
	public void setIdInspectionRepairItem(Long idInspectionRepairItem) {
		this.idInspectionRepairItem = idInspectionRepairItem;
	}

	@Column(name = "repair_amount")
	public Long getRepairAmount() {
		return repairAmount;
	}
	public void setRepairAmount(Long repairAmount) {
		this.repairAmount = repairAmount;
	}

	@Column(name = "creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}

    @Column(name = "is_paid_by_kavak")
	public boolean isPaidByKavak() {
        return paidByKavak;
    }
    public void setPaidByKavak(boolean paidByKavak) {
        this.paidByKavak = paidByKavak;
    }

	@Column(name = "repair_image")
	public String getRepairImage() {
		return repairImage;
	}
	public void setRepairImage(String repairImage) {
		this.repairImage = repairImage;
	}

	@Column(name="inspection_type_id")
	public Long getInspectionTypeId() {
		return inspectionTypeId;
	}
	public void setInspectionTypeId(Long inspectionTypeId) {
		this.inspectionTypeId = inspectionTypeId;
	}

    @ManyToOne
    @JoinColumn(name="inspection_point_car_id")
    public InspectionPointCar getInspectionPointCar() {
        return inspectionPointCar;
    }
    public void setInspectionPointCar(InspectionPointCar inspectionPointCar) {
        this.inspectionPointCar = inspectionPointCar;
    }

	@Column(name="repair_other_text")
	public String getRepairOtherText() {
		return repairOtherText;
	}
	public void setRepairOtherText(String repairOtherText) {
		this.repairOtherText = repairOtherText;
	}

    @Transient
	public InspectionRepairDTO getDTO(){
		InspectionRepairDTO inspectionRepairDTO = new InspectionRepairDTO();
        inspectionRepairDTO.setId(this.id);
		inspectionRepairDTO.setIdInspectionRepairCategory(this.idInspectionRepairCategory);;
		inspectionRepairDTO.setIdInspectionRepairSubCategory(this.idInspectionRepairSubCategory);;
		inspectionRepairDTO.setIdInspectionRepairItem(this.idInspectionRepairItem);;
		inspectionRepairDTO.setRepairAmount(Double.valueOf(this.repairAmount));;
        inspectionRepairDTO.setPaidByKavak(this.paidByKavak);
        inspectionRepairDTO.setRepairImage(this.repairImage);
        inspectionRepairDTO.setRepairOtherText(this.repairOtherText);
		return inspectionRepairDTO;
	}

}
