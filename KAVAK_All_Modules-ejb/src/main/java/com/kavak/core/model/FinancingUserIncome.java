package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.financing.FinancingUserIncomeDTO;

@Entity
@Table(name = "v2_financing_user_income_profile")
public class FinancingUserIncome {

    private Long id;
    private Long financingId;
    private Long netIncomeVerified;
    private String incomeProfile;
    private String automotiveCarUseType;
    private Long automotiveCarPrice;
    private Long automotiveDownPayment;
//    private String codeArf;
//    private String escore;
//    private String escoreText;
//    private String disqualified;
//    private String escoreRazon;
//    private String financingAmount;
//    private String maximumFinancingAmount;
    private Timestamp creationDate;
    private boolean active;
    private Long positionAge;


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO) 
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "net_income_verified")
    public Long getNetIncomeVerified() {
        return netIncomeVerified;
    }
    public void setNetIncomeVerified(Long netIncomeVerified) {
        this.netIncomeVerified = netIncomeVerified;
    }

    @Column(name = "income_profile")
    public String getIncomeProfile() {
        return incomeProfile;
    }
    public void setIncomeProfile(String incomeProfile) {
        this.incomeProfile = incomeProfile;
    }

    @Column(name = "automotive_car_use_type")
    public String getAutomotiveCarUseType() {
        return automotiveCarUseType;
    }
    public void setAutomotiveCarUseType(String automotiveCarUseType) {
        this.automotiveCarUseType = automotiveCarUseType;
    }

    @Column(name = "automotive_car_price")
    public Long getAutomotiveCarPrice() {
        return automotiveCarPrice;
    }
    public void setAutomotiveCarPrice(Long automotiveCarPrice) {
        this.automotiveCarPrice = automotiveCarPrice;
    }

    @Column(name = "automotive_down_payment")
    public Long getAutomotiveDownPayment() {
        return automotiveDownPayment;
    }
    public void setAutomotiveDownPayment(Long automotiveDownPayment) {
        this.automotiveDownPayment = automotiveDownPayment;
    }


//    @Column(name = "code_arf")
//    public String getCodeArf() {
//        return codeArf;
//    }
//    public void setCodeArf(String codeArf) {
//        this.codeArf = codeArf;
//    }
//
//    @Column(name = "escore")
//    public String getEscore() {
//        return escore;
//    }
//    public void setEscore(String escore) {
//        this.escore = escore;
//    }
//
//    @Column(name = "escore_text")
//    public String getEscoreText() {
//        return escoreText;
//    }
//    public void setEscoreText(String escoreText) {
//        this.escoreText = escoreText;
//    }
//
//    @Column(name = "disqualified")
//    public String getDisqualified() {
//        return disqualified;
//    }
//    public void setDisqualified(String disqualified) {
//        this.disqualified = disqualified;
//    }
//
//    @Column(name = "escore_razon")
//    public String getEscoreRazon() {
//        return escoreRazon;
//    }
//    public void setEscoreRazon(String escoreRazon) {
//        this.escoreRazon = escoreRazon;
//    }
//
//    @Column(name = "financing_amount")
//    public String getFinancingAmount() {
//        return financingAmount;
//    }
//    public void setFinancingAmount(String financingAmount) {
//        this.financingAmount = financingAmount;
//    }
//
//    @Column(name = "maximum_financing_amount")
//    public String getMaximumFinancingAmount() {
//        return maximumFinancingAmount;
//    }
//    public void setMaximumFinancingAmount(String maximumFinancingAmount) {
//        this.maximumFinancingAmount = maximumFinancingAmount;
//    }

    @Column(name = "creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name = "financing_id")
    public Long getFinancingId() {
        return financingId;
    }
    public void setFinancingId(Long financingId) {
        this.financingId = financingId;
    }
    
    @Column(name = "position_age")
    public Long getPositionAge() {
        return positionAge;
    }
    public void setPositionAge(Long positionAge) {
        this.positionAge = positionAge;
    }
    
    @Transient
    public FinancingUserIncomeDTO getDTO() {
	FinancingUserIncomeDTO financingUserIncomeDTO = new FinancingUserIncomeDTO();
	financingUserIncomeDTO.setId(this.id);
	financingUserIncomeDTO.setFinancingId(this.financingId);
	financingUserIncomeDTO.setActive(this.active);
	financingUserIncomeDTO.setAutomotiveCarPrice(this.automotiveCarPrice);
	financingUserIncomeDTO.setAutomotiveCarUseType(this.automotiveCarUseType);
	financingUserIncomeDTO.setAutomotiveDownPayment(this.automotiveDownPayment);
//	financingUserIncomeDTO.setCodeArf(this.codeArf);
//	financingUserIncomeDTO.setDisqualified(this.disqualified);
//	financingUserIncomeDTO.setEscore(this.escore);
//	financingUserIncomeDTO.setEscoreRazon(this.escoreRazon);
//	financingUserIncomeDTO.setEscoreText(this.escoreText);
//	financingUserIncomeDTO.setFinancingAmount(this.financingAmount);
	financingUserIncomeDTO.setIncomeProfile(this.incomeProfile);
//	financingUserIncomeDTO.setMaximumFinancingAmount(this.maximumFinancingAmount);
	financingUserIncomeDTO.setNetIncomeVerified(this.netIncomeVerified);
	financingUserIncomeDTO.setActive(this.active);
	financingUserIncomeDTO.setCreationDate(this.creationDate);

	return financingUserIncomeDTO;
    }


}