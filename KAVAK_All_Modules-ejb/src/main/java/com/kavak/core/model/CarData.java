package com.kavak.core.model;

import com.kavak.core.dto.CarDataDTO;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "car_data")
public class CarData {

    private Long id;
    private String sku;
    private Long carYear;
    private String carMake;
    private String carModel;
    private String carTrim;
    private String salePrice;
    private String buyPrice;
    private String originalPrice30D;
    private String originalPriceInstance;
    private String originalPriceConsignation;
    private String salePriceKavak;
    private String marketPrice;
    private String quantitySamples;
    private String averageDateSamples;
    private Timestamp updateDateOffer;
    private String versionGuiaAutometrica;
    private String fullVersion;
    private String offerDetail;
    private String trustedOffer;
    private String punishedOffer;
    private String doors;
    private String trasmissions;
    private String seats;
    private String rim;
    private String turbo;
    private String hp;
    private String sunroof;
    private String cylinder;
    private String navigation;
    private String traction;
    private String wishList;
    private String offer30Days;
    private String lowerRange30Days;
    private String highRange30Days;
    private String offerInstant;
    private String lowerRangeInstant;
    private String highRangeInstant;
    private String offerConsignation;
    private String lowerRangeConsession;
    private String highRangeConsession;
    private String minKm;
    private String minKmStandarReward;
    private String minKmStandarRewardFactor;
    private String minKmTopRewardFactor;
    private String maxKm;
    private String maxKmStandarPunishment;
    private String maxKmTopPunishmentFactor;
    private String maxKmStandarPunishmentFactor;
    private String others;
    private String other3;
    private String other4;
    private String other5;
    private String other6;
    private String other7;
    private String other8;
    private Long yearId;
    private Long doorsId;
    private Long cylindroId;
    private Long seatsId;
    private Long traccionId;
    private Long transmissionid;
    private Long fuelId;
    private Long makeId;
    private Long styleId;
    private Long modelId;
    private Long versionId;
    private String segmentType;
    private List<CarscoutCatalogue> carscoutCatalogue;
    private String pcVendeTuAuto;
    private String pcEbc;
    private String pvEbc;
    private String versionEbc;
    private Long avgKm;
    private Long setPriceInst;
    private boolean isActive;
    private String awsKarsnapOffer;
    private String awsLowestRange;
    private String awsHeighestRange;
    private String awsInstantOffer;
    private String awsLrInstantOffer;
    private String awsHrInstantOffer;
    private String awsCncOffer;
    private String awsLrCncOffer;
    private String awsHrCncOffer;
    private String awsPc30dOrg;
    private String awsPcInsOrg;
    private String awsPcCncOrg;
    private String awsOffer;
    private String awsPrecioVenta;
    private String rimMaterial;
    private String lights;
    private String air;
    private String liters;
    private Timestamp awsFechaActOffer;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Column(name = "sku")
    public String getSku() {
	return sku;
    }

    public void setSku(String sku) {
	this.sku = sku;
    }

    @Column(name = "year")
    public Long getCarYear() {
	return carYear;
    }

    public void setCarYear(Long carYear) {
	this.carYear = carYear;
    }

    @Column(name = "make")
    public String getCarMake() {
	return carMake;
    }

    public void setCarMake(String carMake) {
	this.carMake = carMake;
    }

    @Column(name = "model")
    public String getCarModel() {
	return carModel;
    }

    public void setCarModel(String carModel) {
	this.carModel = carModel;
    }

    @Column(name = "trim")
    public String getCarTrim() {
	return carTrim;
    }

    public void setCarTrim(String carTrim) {
	this.carTrim = carTrim;
    }

    @Column(name = "doors")
    public String getDoors() {
	return doors;
    }

    public void setDoors(String doors) {
	this.doors = doors;
    }

    @Column(name = "trasmissions")
    public String getTrasmissions() {
	return trasmissions;
    }

    public void setTrasmissions(String trasmissions) {
	this.trasmissions = trasmissions;
    }

    @Column(name = "seats")
    public String getSeats() {
	return seats;
    }

    public void setSeats(String seats) {
	this.seats = seats;
    }

    @Column(name = "rim")
    public String getRim() {
	return rim;
    }

    public void setRim(String rim) {
	this.rim = rim;
    }

    @Column(name = "turbo")
    public String getTurbo() {
	return turbo;
    }

    public void setTurbo(String turbo) {
	this.turbo = turbo;
    }

    @Column(name = "hp")
    public String getHp() {
	return hp;
    }

    public void setHp(String hp) {
	this.hp = hp;
    }

    @Column(name = "sunroof")
    public String getSunroof() {
	return sunroof;
    }

    public void setSunroof(String sunroof) {
	this.sunroof = sunroof;
    }

    @Column(name = "cylindro")
    public String getCylinder() {
	return cylinder;
    }

    public void setCylinder(String cylinder) {
	this.cylinder = cylinder;
    }

    @Column(name = "nevigation")
    public String getNavigation() {
	return navigation;
    }

    public void setNavigation(String navigation) {
	this.navigation = navigation;
    }

    @Column(name = "traccion")
    public String getTraction() {
	return traction;
    }

    public void setTraction(String traction) {
	this.traction = traction;
    }

    @Column(name = "OTRO1")
    public String getOthers() {
	return others;
    }

    public void setOthers(String others) {
	this.others = others;
    }

    @Column(name = "OTRO2")
    public String getWishList() {
	return wishList;
    }

    public void setWishList(String wishList) {
	this.wishList = wishList;
    }

    @Column(name = "karsnap_offer")
    public String getOffer30Days() {
	return offer30Days;
    }

    public void setOffer30Days(String offer30Days) {
	this.offer30Days = offer30Days;
    }

    @Column(name = "min_km")
    public String getMinKm() {
	return minKm;
    }

    public void setMinKm(String minKm) {
	this.minKm = minKm;
    }

    @Column(name = "max_km")
    public String getMaxKm() {
	return maxKm;
    }

    public void setMaxKm(String maxKm) {
	this.maxKm = maxKm;
    }

    @Column(name = "lowest_range")
    public String getLowerRange30Days() {
	return lowerRange30Days;
    }

    public void setLowerRange30Days(String lowerRange30Days) {
	this.lowerRange30Days = lowerRange30Days;
    }

    @Column(name = "heighest_range")
    public String getHighRange30Days() {
	return highRange30Days;
    }

    public void setHighRange30Days(String highRange30Days) {
	this.highRange30Days = highRange30Days;
    }

    @Column(name = "instant_offer")
    public String getOfferInstant() {
	return offerInstant;
    }

    public void setOfferInstant(String offerInstant) {
	this.offerInstant = offerInstant;
    }

    @Column(name = "lr_instant_offer")
    public String getLowerRangeInstant() {
	return lowerRangeInstant;
    }

    public void setLowerRangeInstant(String lowerRangeInstant) {
	this.lowerRangeInstant = lowerRangeInstant;
    }

    @Column(name = "hr_instant_offer")
    public String getHighRangeInstant() {
	return highRangeInstant;
    }

    public void setHighRangeInstant(String highRangeInstant) {
	this.highRangeInstant = highRangeInstant;
    }

    @Column(name = "cnc_offer")
    public String getOfferConsignation() {
	return offerConsignation;
    }

    public void setOfferConsignation(String offerConsignation) {
	this.offerConsignation = offerConsignation;
    }

    @Column(name = "lr_cnc_offer")
    public String getLowerRangeConsession() {
	return lowerRangeConsession;
    }

    public void setLowerRangeConsession(String lowerRangeConsession) {
	this.lowerRangeConsession = lowerRangeConsession;
    }

    @Column(name = "hr_cnc_offer")
    public String getHighRangeConsession() {
	return highRangeConsession;
    }

    public void setHighRangeConsession(String highRangeConsession) {
	this.highRangeConsession = highRangeConsession;
    }

    @Column(name = "min_km_std_reward")
    public String getMinKmStandarReward() {
	return minKmStandarReward;
    }

    public void setMinKmStandarReward(String minKmStandarReward) {
	this.minKmStandarReward = minKmStandarReward;
    }

    @Column(name = "min_km_std_reward_factor")
    public String getMinKmStandarRewardFactor() {
	return minKmStandarRewardFactor;
    }

    public void setMinKmStandarRewardFactor(String minKmStandarRewardFactor) {
	this.minKmStandarRewardFactor = minKmStandarRewardFactor;
    }

    @Column(name = "min_km_top_reward_factor")
    public String getMinKmTopRewardFactor() {
	return minKmTopRewardFactor;
    }

    public void setMinKmTopRewardFactor(String minKmTopRewardFactor) {
	this.minKmTopRewardFactor = minKmTopRewardFactor;
    }

    @Column(name = "max_km_std_punishment")
    public String getMaxKmStandarPunishment() {
	return maxKmStandarPunishment;
    }

    public void setMaxKmStandarPunishment(String maxKmStandarPunishment) {
	this.maxKmStandarPunishment = maxKmStandarPunishment;
    }

    @Column(name = "max_km_top_punishment_factor")
    public String getMaxKmTopPunishmentFactor() {
	return maxKmTopPunishmentFactor;
    }

    public void setMaxKmTopPunishmentFactor(String maxKmTopPunishmentFactor) {
	this.maxKmTopPunishmentFactor = maxKmTopPunishmentFactor;
    }

    @Column(name = "max_km_std_punishment_factor")
    public String getMaxKmStandarPunishmentFactor() {
	return maxKmStandarPunishmentFactor;
    }

    public void setMaxKmStandarPunishmentFactor(String maxKmStandarPunishmentFactor) {
	this.maxKmStandarPunishmentFactor = maxKmStandarPunishmentFactor;
    }

    @Column(name = "sale_price")
    public String getSalePrice() {
	return salePrice;
    }

    public void setSalePrice(String salePrice) {
	this.salePrice = salePrice;
    }

    @Column(name = "buy_price")
    public String getBuyPrice() {
	return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
	this.buyPrice = buyPrice;
    }

    @Column(name = "pc_30d_org")
    public String getOriginalPrice30D() {
	return originalPrice30D;
    }

    public void setOriginalPrice30D(String originalPrice30D) {
	this.originalPrice30D = originalPrice30D;
    }

    @Column(name = "pc_ins_org")
    public String getOriginalPriceInstance() {
	return originalPriceInstance;
    }

    public void setOriginalPriceInstance(String originalPriceInstance) {
	this.originalPriceInstance = originalPriceInstance;
    }

    @Column(name = "pc_cnc_org")
    public String getOriginalPriceConsignation() {
	return originalPriceConsignation;
    }

    public void setOriginalPriceConsignation(String originalPriceConsignation) {
	this.originalPriceConsignation = originalPriceConsignation;
    }

    @Column(name = "precio_venta")
    public String getSalePriceKavak() {
	return salePriceKavak;
    }

    public void setSalePriceKavak(String salePriceKavak) {
	this.salePriceKavak = salePriceKavak;
    }

    @Column(name = "precio_mercado")
    public String getMarketPrice() {
	return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
	this.marketPrice = marketPrice;
    }

    @Column(name = "cant_muestras")
    public String getQuantitySamples() {
	return quantitySamples;
    }

    public void setQuantitySamples(String quantitySamples) {
	this.quantitySamples = quantitySamples;
    }

    @Column(name = "med_date_muestras")
    public String getAverageDateSamples() {
	return averageDateSamples;
    }

    public void setAverageDateSamples(String averageDateSamples) {
	this.averageDateSamples = averageDateSamples;
    }

    @Column(name = "fecha_act_offer")
    public Timestamp getUpdateDateOffer() {
	return updateDateOffer;
    }

    public void setUpdateDateOffer(Timestamp updateDateOffer) {
	this.updateDateOffer = updateDateOffer;
    }

    @Column(name = "version_ga")
    public String getVersionGuiaAutometrica() {
	return versionGuiaAutometrica;
    }

    public void setVersionGuiaAutometrica(String versionGuiaAutometrica) {
	this.versionGuiaAutometrica = versionGuiaAutometrica;
    }

    @Column(name = "full_version")
    public String getFullVersion() {
	return fullVersion;
    }

    public void setFullVersion(String fullVersion) {
	this.fullVersion = fullVersion;
    }

    @Column(name = "detalle_oferta")
    public String getOfferDetail() {
	return offerDetail;
    }

    public void setOfferDetail(String offerDetail) {
	this.offerDetail = offerDetail;
    }

    @Column(name = "oferta_confianza")
    public String getTrustedOffer() {
	return trustedOffer;
    }

    public void setTrustedOffer(String trustedOffer) {
	this.trustedOffer = trustedOffer;
    }

    @Column(name = "oferta_castigada")
    public String getPunishedOffer() {
	return punishedOffer;
    }

    public void setPunishedOffer(String punishedOffer) {
	this.punishedOffer = punishedOffer;
    }

    @Column(name = "OTRO3")
    public String getOther3() {
	return other3;
    }

    public void setOther3(String other3) {
	this.other3 = other3;
    }

    @Column(name = "OTRO4")
    public String getOther4() {
	return other4;
    }

    public void setOther4(String other4) {
	this.other4 = other4;
    }

    @Column(name = "OTRO5")
    public String getOther5() {
	return other5;
    }

    public void setOther5(String other5) {
	this.other5 = other5;
    }

    @Column(name = "OTRO6")
    public String getOther6() {
	return other6;
    }

    public void setOther6(String other6) {
	this.other6 = other6;
    }

    @Column(name = "OTRO7")
    public String getOther7() {
	return other7;
    }

    public void setOther7(String other7) {
	this.other7 = other7;
    }

    @Column(name = "OTRO8")
    public String getOther8() {
	return other8;
    }

    public void setOther8(String other8) {
	this.other8 = other8;
    }

    @Column(name = "year_id")
    public Long getYearId() {
	return yearId;
    }

    public void setYearId(Long yearId) {
	this.yearId = yearId;
    }

    @Column(name = "doors_id")
    public Long getDoorsId() {
	return doorsId;
    }

    public void setDoorsId(Long doorsId) {
	this.doorsId = doorsId;
    }

    @Column(name = "cylindro_id")
    public Long getCylindroId() {
	return cylindroId;
    }

    public void setCylindroId(Long cylindroId) {
	this.cylindroId = cylindroId;
    }

    @Column(name = "seats_id")
    public Long getSeatsId() {
	return seatsId;
    }

    public void setSeatsId(Long seatsId) {
	this.seatsId = seatsId;
    }

    @Column(name = "traccion_id")
    public Long getTraccionId() {
	return traccionId;
    }

    public void setTraccionId(Long traccionId) {
	this.traccionId = traccionId;
    }

    @Column(name = "fuel_id")
    public Long getFuelId() {
	return fuelId;
    }

    public void setFuelId(Long fuelId) {
	this.fuelId = fuelId;
    }

    @Column(name = "transmission_id")
    public Long getTransmissionid() {
	return transmissionid;
    }

    public void setTransmissionid(Long transmissionid) {
	this.transmissionid = transmissionid;
    }

    @Column(name = "make_id")
    public Long getMakeId() {
	return makeId;
    }

    public void setMakeId(Long makeId) {
	this.makeId = makeId;
    }

    @Column(name = "style_id")
    public Long getStyleId() {
	return styleId;
    }

    public void setStyleId(Long styleId) {
	this.styleId = styleId;
    }

    @Column(name = "model_id")
    public Long getModelId() {
	return modelId;
    }

    public void setModelId(Long modelId) {
	this.modelId = modelId;
    }

    @Column(name = "version_id")
    public Long getVersionId() {
	return versionId;
    }

    public void setVersionId(Long versionId) {
	this.versionId = versionId;
    }

    @Column(name = "segment_type")
    public String getSegmentType() {
	return segmentType;
    }

    public void setSegmentType(String segmentType) {
	this.segmentType = segmentType;
    }

    @OneToMany(mappedBy = "carData")
    public List<CarscoutCatalogue> getCarscoutCatalogue() {
	return carscoutCatalogue;
    }

    public void setCarscoutCatalogue(List<CarscoutCatalogue> carscoutCatalogue) {
	this.carscoutCatalogue = carscoutCatalogue;
    }

    @Column(name = "pc_vendetuauto")
    public String getPcVendeTuAuto() {
	return pcVendeTuAuto;
    }

    public void setPcVendeTuAuto(String pcVendeTuAuto) {
	this.pcVendeTuAuto = pcVendeTuAuto;
    }

    @Column(name = "pc_ebc")
    public String getPcEbc() {
	return pcEbc;
    }

    public void setPcEbc(String pcEbc) {
	this.pcEbc = pcEbc;
    }

    @Column(name = "pv_ebc")
    public String getPvEbc() {
	return pvEbc;
    }

    public void setPvEbc(String pvEbc) {
	this.pvEbc = pvEbc;
    }

    @Column(name = "version_ebc")
    public String getVersionEbc() {
	return versionEbc;
    }

    public void setVersionEbc(String versionEbc) {
	this.versionEbc = versionEbc;
    }

    @Column(name = "avg_km")
    public Long getAvgKm() {
	return avgKm;
    }

    public void setAvgKm(Long avgKm) {
	this.avgKm = avgKm;
    }

    @Column(name = "SET_PRICE_INST")
    public Long getSetPriceInst() {
	return setPriceInst;
    }

    public void setSetPriceInst(Long setPriceInst) {
	this.setPriceInst = setPriceInst;
    }

    @Column(name = "is_active")
    public boolean isActive() {
	return isActive;
    }

    public void setActive(boolean isActive) {
	this.isActive = isActive;
    }
    
    @Column(name = "aws_karsnap_offer")
    public String getAwsKarsnapOffer() {
        return awsKarsnapOffer;
    }

    public void setAwsKarsnapOffer(String awsKarsnapOffer) {
        this.awsKarsnapOffer = awsKarsnapOffer;
    }

    @Column(name = "aws_lowest_range")
    public String getAwsLowestRange() {
        return awsLowestRange;
    }

    public void setAwsLowestRange(String awsLowestRange) {
        this.awsLowestRange = awsLowestRange;
    }

    @Column(name = "aws_heighest_range")
    public String getAwsHeighestRange() {
        return awsHeighestRange;
    }

    public void setAwsHeighestRange(String awsHeighestRange) {
        this.awsHeighestRange = awsHeighestRange;
    }

    @Column(name = "aws_instant_offer")
    public String getAwsInstantOffer() {
        return awsInstantOffer;
    }

    public void setAwsInstantOffer(String awsInstantOffer) {
        this.awsInstantOffer = awsInstantOffer;
    }

    @Column(name = "aws_lr_instant_offer")
    public String getAwsLrInstantOffer() {
        return awsLrInstantOffer;
    }

    public void setAwsLrInstantOffer(String awsLrInstantOffer) {
        this.awsLrInstantOffer = awsLrInstantOffer;
    }

    @Column(name = "aws_hr_instant_offer")
    public String getAwsHrInstantOffer() {
        return awsHrInstantOffer;
    }

    public void setAwsHrInstantOffer(String awsHrInstantOffer) {
        this.awsHrInstantOffer = awsHrInstantOffer;
    }

    @Column(name = "aws_cnc_offer")
    public String getAwsCncOffer() {
        return awsCncOffer;
    }

    public void setAwsCncOffer(String awsCncOffer) {
        this.awsCncOffer = awsCncOffer;
    }

    @Column(name = "aws_lr_cnc_offer")
    public String getAwsLrCncOffer() {
        return awsLrCncOffer;
    }

    public void setAwsLrCncOffer(String awsLrCncOffer) {
        this.awsLrCncOffer = awsLrCncOffer;
    }

    @Column(name = "aws_hr_cnc_offer")
    public String getAwsHrCncOffer() {
        return awsHrCncOffer;
    }

    public void setAwsHrCncOffer(String awsHrCncOffer) {
        this.awsHrCncOffer = awsHrCncOffer;
    }

    @Column(name = "aws_pc_30d_org")
    public String getAwsPc30dOrg() {
        return awsPc30dOrg;
    }

    public void setAwsPc30dOrg(String awsPc30dOrg) {
        this.awsPc30dOrg = awsPc30dOrg;
    }

    @Column(name = "aws_pc_ins_org")
    public String getAwsPcInsOrg() {
        return awsPcInsOrg;
    }

    public void setAwsPcInsOrg(String awsPcInsOrg) {
        this.awsPcInsOrg = awsPcInsOrg;
    }

    @Column(name = "aws_pc_cnc_org")
    public String getAwsPcCncOrg() {
        return awsPcCncOrg;
    }

    public void setAwsPcCncOrg(String awsPcCncOrg) {
        this.awsPcCncOrg = awsPcCncOrg;
    }

    @Column(name = "aws_offer")
    public String getAwsOffer() {
        return awsOffer;
    }

    public void setAwsOffer(String awsOffer) {
        this.awsOffer = awsOffer;
    }

    @Column(name = "aws_precio_venta")
    public String getAwsPrecioVenta() {
        return awsPrecioVenta;
    }

    public void setAwsPrecioVenta(String awsPrecioVenta) {
        this.awsPrecioVenta = awsPrecioVenta;
    }

    @Column(name = "rim_material")
    public String getRimMaterial() {
        return rimMaterial;
    }

    public void setRimMaterial(String rimMaterial) {
        this.rimMaterial = rimMaterial;
    }

    @Column(name = "lights")
    public String getLights() {
        return lights;
    }

    public void setLights(String lights) {
        this.lights = lights;
    }

    @Column(name = "air")
    public String getAir() {
        return air;
    }

    public void setAir(String air) {
        this.air = air;
    }

    @Column(name = "litros")
    public String getLiters() {
	return liters;
    }

    public void setLiters(String liters) {
	this.liters = liters;
    }

    @Column(name = "aws_fecha_act_offer")
    public Timestamp getAwsFechaActOffer() {
        return awsFechaActOffer;
    }

    public void setAwsFechaActOffer(Timestamp awsFechaActOffer) {
        this.awsFechaActOffer = awsFechaActOffer;
    }

    @Transient
    public CarDataDTO getDTO() {
	CarDataDTO carDataDTO = new CarDataDTO();
	carDataDTO.setIdCarData(this.id);
	carDataDTO.setSku(this.sku);
	carDataDTO.setCarYear(this.carYear);
	carDataDTO.setCarMake(this.carMake);
	carDataDTO.setCarModel(this.carModel);
	carDataDTO.setCarTrim(this.carTrim);
	carDataDTO.setDoors(this.doors);
	carDataDTO.setTrasmissions(this.trasmissions);
	carDataDTO.setSeats(this.seats);
	carDataDTO.setRim(this.rim);
	carDataDTO.setTurbo(this.turbo);
	carDataDTO.setHp(this.hp);
	carDataDTO.setSunroof(this.sunroof);
	carDataDTO.setSunroof(this.sunroof);
	carDataDTO.setCylinder(this.cylinder);
	carDataDTO.setOffer30Days(this.offer30Days);
	carDataDTO.setOthers(this.others);
	carDataDTO.setFullVersionCar(this.fullVersion);
	carDataDTO.setTraction(this.traction);
	carDataDTO.setSegmentType(this.segmentType);
	if (this.getWishList().equalsIgnoreCase("1")) {
	    carDataDTO.setWishList(true);
	} else {
	    carDataDTO.setWishList(false);
	}

	return carDataDTO;
    }

}