package com.kavak.core.model;

import javax.persistence.*;

@Entity
@Table(name = "v2_car_feature_item")
public class InspectionCarFeatureItem {
	
	private Long id;
	private Long idCategory;
	private String name;
	private String iconImage;
	private boolean active;
	private InspectionCarFeature inspectionCarFeature;
	
	@Id
	@Column(name ="id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name ="category_id")
	public Long getIdCategory() {
		return idCategory;
	}
	public void setIdCategory(Long idCategory) {
		this.idCategory = idCategory;
	}
	@Column(name ="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name ="icon_image")
	public String getIconImage() {
		return iconImage;
	}
	public void setIconImage(String iconImage) {
		this.iconImage = iconImage;
	}
	@Column(name ="active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@OneToOne(mappedBy="inspectionCarFeatureItem")	
	public InspectionCarFeature getInspectionCarFeature() {
		return inspectionCarFeature;
	}
	public void setInspectionCarFeature(InspectionCarFeature inspectionCarFeature) {
		this.inspectionCarFeature = inspectionCarFeature;
	}
}
