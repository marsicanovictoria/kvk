package com.kavak.core.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "v2_carscout_category")
public class CarscoutCategory {

	private Long id;
	private String description;
	private Timestamp creationDate;
	private List<CarMetaAccesories> carMetaAccesories;
	private List<CarscoutAlertDetail> carscoutAlertDetail;

	@Id
	@Column(name = "category_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@OneToMany(mappedBy = "carscoutCategory")
	public List<CarMetaAccesories> getCarMetaAccesories() {
		return carMetaAccesories;
	}

	public void setCarMetaAccesories(List<CarMetaAccesories> carMetaAccesories) {
		this.carMetaAccesories = carMetaAccesories;
	}

	@OneToMany(mappedBy = "carscoutCategory")
	public List<CarscoutAlertDetail> getCarscoutAlertDetail() {
		return carscoutAlertDetail;
	}

	public void setCarscoutAlertDetail(List<CarscoutAlertDetail> carscoutAlertDetail) {
		this.carscoutAlertDetail = carscoutAlertDetail;
	}
	
	
	
	

}
