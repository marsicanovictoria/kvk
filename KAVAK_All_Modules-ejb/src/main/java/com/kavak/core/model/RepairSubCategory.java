package com.kavak.core.model;

import com.kavak.core.dto.RepairSubCategoryDTO;
import com.kavak.core.dto.RepairTypeDTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="v2_repair_item_subcategory")
public class RepairSubCategory {
	
	private Long id;
	private String name;
	private RepairCategory repairCategory; // Campo Agregada solo por relacion ManyToOne RepairCategory
	private List<RepairType> listRepairType;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@ManyToOne
	@JoinColumn(name="category_id")
	public RepairCategory getRepairCategory() {
		return repairCategory;
	}
	public void setRepairCategory(RepairCategory repairCategory) {
		this.repairCategory = repairCategory;
	}
	@OneToMany(mappedBy="repairSubCategory")
	public List<RepairType> getListRepairType() {
		return listRepairType;
	}
	public void setListRepairType(List<RepairType> listRepairType) {
		this.listRepairType = listRepairType;
	}
	
	@Transient
	public RepairSubCategoryDTO getDTO(){
		RepairSubCategoryDTO repairSubCategoryDTO = new RepairSubCategoryDTO();
		repairSubCategoryDTO.setId(this.id);
		repairSubCategoryDTO.setName(this.name);
		List<RepairTypeDTO> listRepairTypeDTO = new ArrayList<>(); 
		
		for(RepairType repairTypeActual: this.getListRepairType()){
			listRepairTypeDTO.add(repairTypeActual.getDTO());
		}
		repairSubCategoryDTO.setListRepairTypeDTO(listRepairTypeDTO);
		
		return repairSubCategoryDTO;
	}
}