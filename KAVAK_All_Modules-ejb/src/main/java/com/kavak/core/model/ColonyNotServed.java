package com.kavak.core.model;

import com.kavak.core.dto.ColonyNotServedDTO;

import javax.persistence.*;

@Entity
@Table(name="colonia_no_servida")
public class ColonyNotServed {

    private Long id;
    private Long zipCode;
    private String name;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="codigo_postal")
    public Long getZipCode() {
        return zipCode;
    }
    public void setZipCode(Long zipCode) {
        this.zipCode = zipCode;
    }

    @Column(name="nombre_colonia")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Transient
    public ColonyNotServedDTO getDTO(){
        ColonyNotServedDTO colonyNotServedDTO = new ColonyNotServedDTO();
        colonyNotServedDTO.setName(this.name);
        return colonyNotServedDTO;
    }
}
