package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="page_meta")
public class PageMeta {

    private Long id;
    private Long pid;
    private String metaKey; 
    private String metaValue;

    

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="pid")
    public Long getPid() {
        return pid;
    }
    public void setPid(Long pid) {
        this.pid = pid;
    }
    
    @Column(name="meta_key")
    public String getMetaKey() {
        return metaKey;
    }
    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }
    
    @Column(name="meta_value")
    public String getMetaValue() {
        return metaValue;
    }
    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }


}
