package com.kavak.core.model;



import com.kavak.core.dto.ZipCodeDTO;

import javax.persistence.*;

@Entity
@Table(name="costo_envio_codigo_postal")
public class ZipCode {

	private Long id;
	private Long zipCode;
	private Long shippingCost;
	private boolean activo;
	private boolean servedZone;
	
	@Id
	@Column(name="id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="codigo_postal")
	public Long getZipCode() {
		return zipCode;
	}
	public void setZipCode(Long zipCode) {
		this.zipCode = zipCode;
	}
	@Column(name="costo_envio")
	public Long getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(Long shippingCost) {
		this.shippingCost = shippingCost;
	}
	@Column(name="activo")
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	@Column(name="permite_inspeccion")
	public boolean isServedZone() {
		return servedZone;
	}
	public void setServedZone(boolean servedZone) {
		this.servedZone = servedZone;
	}

	@Transient
	public ZipCodeDTO getDTO(){
		ZipCodeDTO zipCodeDTO = new ZipCodeDTO();
		zipCodeDTO.setShippingCost(this.shippingCost);
		return zipCodeDTO;
	}
}