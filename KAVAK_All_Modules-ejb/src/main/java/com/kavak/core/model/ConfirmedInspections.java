package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.kavak.core.dto.ConfirmedInspectionDTO;

@Entity
@Table(name = "v2_confirmed_inspection")
public class ConfirmedInspections {
	
	private Long id;
	private Long offerId;
	private Long userId;
	private Long inspectorId;
	private Long leadManagerId;
	private String thirtyOfferMax;
	private String instantOfferMax;
	private String consignmentOfferMax;
	private String carYear;
	private String carMake;
	private String carModel;
	private String inspectionDate;
	private String inspectionTimeStart;
	private String inspectionTimeEnd;
	private String inspectionAddress;
	private Timestamp registerDate;
	private Long active;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "offer_id")
	public Long getOfferId() {
		return offerId;
	}
	public void setOfferId(Long offerId) {
		this.offerId = offerId;
	}
	
	@Column(name = "user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Column(name = "inspector_id")
	public Long getInspectorId() {
		return inspectorId;
	}
	public void setInspectorId(Long inspectorId) {
		this.inspectorId = inspectorId;
	}
	
	@Column(name = "lead_manager_id")
	public Long getLeadManagerId() {
		return leadManagerId;
	}
	public void setLeadManagerId(Long leadManagerId) {
		this.leadManagerId = leadManagerId;
	}
	
	@Column(name = "thirty_days_offer_max")
	public String getThirtyOfferMax() {
		return thirtyOfferMax;
	}
	public void setThirtyOfferMax(String thirtyOfferMax) {
		this.thirtyOfferMax = thirtyOfferMax;
	}
	
	@Column(name = "instant_offer_max")
	public String getInstantOfferMax() {
		return instantOfferMax;
	}
	public void setInstantOfferMax(String instantOfferMax) {
		this.instantOfferMax = instantOfferMax;
	}
	
	@Column(name = "consignment_offer_max")
	public String getConsignmentOfferMax() {
		return consignmentOfferMax;
	}
	public void setConsignmentOfferMax(String consignmentOfferMax) {
		this.consignmentOfferMax = consignmentOfferMax;
	}
	
	@Column(name = "car_year")
	public String getCarYear() {
		return carYear;
	}
	public void setCarYear(String carYear) {
		this.carYear = carYear;
	}
	
	@Column(name = "car_make")
	public String getCarMake() {
		return carMake;
	}
	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}
	
	@Column(name = "car_model")
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	
	@Column(name = "register_date")
	public Timestamp getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Timestamp registerDate) {
		this.registerDate = registerDate;
	}
	
	@Column(name = "active")
	public Long getActive() {
		return active;
	}
	public void setActive(Long active) {
		this.active = active;
	}
	
	@Column(name = "inspection_time_start")
	public String getInspectionTimeStart() {
		return inspectionTimeStart;
	}
	public void setInspectionTimeStart(String inspectionTimeStart) {
		this.inspectionTimeStart = inspectionTimeStart;
	}
	
	@Column(name = "inspection_time_end")
	public String getInspectionTimeEnd() {
		return inspectionTimeEnd;
	}
	public void setInspectionTimeEnd(String inspectionTimeEnd) {
		this.inspectionTimeEnd = inspectionTimeEnd;
	}
	
	@Column(name = "inspection_date")
	public String getInspectionDate() {
		return inspectionDate;
	}
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}
	
	@Column(name = "inspection_address")
	public String getInspectionAddress() {
		return inspectionAddress;
	}
	public void setInspectionAddress(String inspectionAddress) {
		this.inspectionAddress = inspectionAddress;
	}
	@Transient
	public ConfirmedInspectionDTO getDTO() {
		ConfirmedInspectionDTO confirmedInspectionDTO = new ConfirmedInspectionDTO();
		
		confirmedInspectionDTO.setId(this.id);
		confirmedInspectionDTO.setOfferId(this.offerId);
		confirmedInspectionDTO.setUserId(this.userId);
		confirmedInspectionDTO.setInspectorId(this.inspectorId);
		confirmedInspectionDTO.setLeadManagerId(this.leadManagerId);
		confirmedInspectionDTO.setThirtyOfferMax(this.thirtyOfferMax);
		confirmedInspectionDTO.setInstantOfferMax(this.instantOfferMax);
		confirmedInspectionDTO.setConsignmentOfferMax(this.consignmentOfferMax);
		confirmedInspectionDTO.setCarYear(this.carYear);
		confirmedInspectionDTO.setCarMake(this.carMake);
		confirmedInspectionDTO.setCarModel(this.carModel);
		confirmedInspectionDTO.setRegisterDate(this.registerDate);
		confirmedInspectionDTO.setActive(this.active);
		confirmedInspectionDTO.setInspectionDate(this.inspectionDate);
		confirmedInspectionDTO.setInspectionTimeStart(this.inspectionTimeStart);
		confirmedInspectionDTO.setInspectionTimeEnd(this.inspectionTimeEnd);
		confirmedInspectionDTO.setInspectionAddress(this.inspectionAddress);
		
		return confirmedInspectionDTO;
	}
	
	
	
}
