package com.kavak.core.model;

import com.kavak.core.dto.InspectionCarPhotoTypeDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "v2_inspection_car_photo_type")
public class InspectionCarPhotoType {

    private Long id;
    private String name;
    private Long position;
    private String image_reference;
    private boolean active;
    private Date creationDate;

    @Id
    @Column(name = "id")
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Column(name = "name")
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Column(name = "position")
    public Long getPosition() {
	return position;
    }

    public void setPosition(Long position) {
	this.position = position;
    }

    @Column(name = "image_reference")
    public String getImage_reference() {
	return image_reference;
    }

    public void setImage_reference(String image_reference) {
	this.image_reference = image_reference;
    }

    @Column(name = "is_active")
    public boolean isActive() {
	return active;
    }

    public void setActive(boolean active) {
	this.active = active;
    }

    @Column(name = "creation_date")
    public Date getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
    }

    @Transient
    public InspectionCarPhotoTypeDTO getDTO() {
	InspectionCarPhotoTypeDTO inpectionCarPhotoTypeDTO = new InspectionCarPhotoTypeDTO();
	inpectionCarPhotoTypeDTO.setId(this.id);
	inpectionCarPhotoTypeDTO.setName(this.name);
	inpectionCarPhotoTypeDTO.setImage_reference(this.image_reference);
	return inpectionCarPhotoTypeDTO;
    }

}
