package com.kavak.core.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "datos_financiamiento")
public class FinancingData {

	private Long id;
	private User user; 
	private Long personType; 
	private String identificationType; 
	private String idNumber; 
	private String electorKey; 
	private Date birthday; 
	private Long educationLevel; 
	private Long maritalStatusId; 
	private Long nationalityId; 
	private Long anddressId; 
	private Long propertyStatusId; 
	private String gender;
	private String dependents; 
	private String isBancomerCustomer; 
	private String rfc;
	private String yearsInHouse; 
	private String colony; 
	private String delegation; 
	private String city; 
	private String state; 
	private String company; 
	private String street; 
	private String exteriorNumber; 
	private String interiorNumber; 
	private String postalCode; 
	private String colonyJob; 
	private String colonyOther; 
	private String delegationJob; 
	private String cityJob; 
	private String StateJob; 
	private String phoneJob; 
	private String economicActivity; 
	private String companyType; 
	private String montlyIncome;
	private String variableIncome; 
	private Long position; 
	private String yearOld; 
	private String monthOld; 
	private Long profession; 
	private Long insuranceAffiliation; 
	private String companyPreviousJob; 
	private String yearsWorked; 
	private String monthsWorked;
	private String companyPhone;
	private Long referenceFamilyId; 
	private Long referencePersonalId; 
	private Long referencePersonalId2; 
	private Long creditStatus; 
	private Long checkpointId; 
	private SellCarDetail sellCarDetail; 
	private String hookMount; 
	private String financingAmount; 
	private String amount; 
	private String month; 
	private Timestamp currentDate; 
	private String delay; 
	private String carUse; 
	private String income; 
	private String mortgageCredit; 
	private String automotiveCredit; 
	private String creditCard; 
	private String creditCardLimit; 
	private String creditCardDigits; 
	private Long activo;
	private String Steps; 
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "tipo_persona_id")
	public Long getPersonType() {
		return personType;
	}

	public void setPersonType(Long personType) {
		this.personType = personType;
	}

	@Column(name = "tipo_identificacion_id")
	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	@Column(name = "numero_identificacion")
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	@Column(name = "clave_elector")
	public String getElectorKey() {
		return electorKey;
	}

	public void setElectorKey(String electorKey) {
		this.electorKey = electorKey;
	}

	@Column(name = "fecha_nacimiento")
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Column(name = "estudios_id")
	public Long getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(Long educationLevel) {
		this.educationLevel = educationLevel;
	}

	@Column(name = "estado_civil_id")
	public Long getMaritalStatusId() {
		return maritalStatusId;
	}

	public void setMaritalStatusId(Long maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}

	@Column(name = "nacionalidad_id")
	public Long getNationalityId() {
		return nationalityId;
	}

	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}

	@Column(name = "direccion_id")
	public Long getAnddressId() {
		return anddressId;
	}

	public void setAnddressId(Long anddressId) {
		this.anddressId = anddressId;
	}

	@Column(name = "tipo_vivienda_id")
	public Long getPropertyStatusId() {
		return propertyStatusId;
	}

	public void setPropertyStatusId(Long propertyStatusId) {
		this.propertyStatusId = propertyStatusId;
	}

	@Column(name = "sexo")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "cantidad_dependientes")
	public String getDependents() {
		return dependents;
	}

	public void setDependents(String dependents) {
		this.dependents = dependents;
	}

	@Column(name = "cliente_bancomer")
	public String getIsBancomerCustomer() {
		return isBancomerCustomer;
	}

	public void setIsBancomerCustomer(String isBancomerCustomer) {
		this.isBancomerCustomer = isBancomerCustomer;
	}

	@Column(name = "rfc")
	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@Column(name = "anios_vivienda")
	public String getYearsInHouse() {
		return yearsInHouse;
	}

	public void setYearsInHouse(String yearsInHouse) {
		this.yearsInHouse = yearsInHouse;
	}

	@Column(name = "colonia_vivienda")
	public String getColony() {
		return colony;
	}

	public void setColony(String colony) {
		this.colony = colony;
	}

	@Column(name = "delegacion_municipio_vivienda")
	public String getDelegation() {
		return delegation;
	}

	public void setDelegation(String delegation) {
		this.delegation = delegation;
	}

	@Column(name = "ciudad_vivienda")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "estado_vivienda")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "nombre_empresa")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Column(name = "calle_empresa")
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Column(name = "numero_direccion_empresa")
	public String getExteriorNumber() {
		return exteriorNumber;
	}

	public void setExteriorNumber(String exteriorNumber) {
		this.exteriorNumber = exteriorNumber;
	}

	@Column(name = "numero_interior_direccion_empresa")
	public String getInteriorNumber() {
		return interiorNumber;
	}

	public void setInteriorNumber(String interiorNumber) {
		this.interiorNumber = interiorNumber;
	}

	@Column(name = "codigo_postal_empresa")
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Column(name = "colonia_empresa")
	public String getColonyJob() {
		return colonyJob;
	}

	public void setColonyJob(String colonyJob) {
		this.colonyJob = colonyJob;
	}

	@Column(name = "colonia_empresa_otro")
	public String getColonyOther() {
		return colonyOther;
	}

	public void setColonyOther(String colonyOther) {
		this.colonyOther = colonyOther;
	}

	@Column(name = "delegacion_municipio_empresa")
	public String getDelegationJob() {
		return delegationJob;
	}

	public void setDelegationJob(String delegationJob) {
		this.delegationJob = delegationJob;
	}

	@Column(name = "ciudad_empresa")
	public String getCityJob() {
		return cityJob;
	}

	public void setCityJob(String cityJob) {
		this.cityJob = cityJob;
	}

	@Column(name = "estado_empresa")
	public String getStateJob() {
		return StateJob;
	}

	public void setStateJob(String stateJob) {
		StateJob = stateJob;
	}

	@Column(name = "telefono_empresa")
	public String getPhoneJob() {
		return phoneJob;
	}

	public void setPhoneJob(String phoneJob) {
		this.phoneJob = phoneJob;
	}

	@Column(name = "actividad_economica_empresa")
	public String getEconomicActivity() {
		return economicActivity;
	}

	public void setEconomicActivity(String economicActivity) {
		this.economicActivity = economicActivity;
	}

	@Column(name = "tipo_empresa_id")
	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	@Column(name = "ingreso_mensual_fijo")
	public String getMontlyIncome() {
		return montlyIncome;
	}

	public void setMontlyIncome(String montlyIncome) {
		this.montlyIncome = montlyIncome;
	}

	@Column(name = "ingreso_mensual_variable")
	public String getVariableIncome() {
		return variableIncome;
	}

	public void setVariableIncome(String variableIncome) {
		this.variableIncome = variableIncome;
	}

	@Column(name = "cargo_id")
	public Long getPosition() {
		return position;
	}

	public void setPosition(Long position) {
		this.position = position;
	}

	@Column(name = "anios_antiguedad_empresa")
	public String getYearOld() {
		return yearOld;
	}

	public void setYearOld(String yearOld) {
		this.yearOld = yearOld;
	}

	@Column(name = "meses_antiguedad_empresa")
	public String getMonthOld() {
		return monthOld;
	}

	public void setMonthOld(String monthOld) {
		this.monthOld = monthOld;
	}

	@Column(name = "profesion_id")
	public Long getProfession() {
		return profession;
	}

	public void setProfession(Long profession) {
		this.profession = profession;
	}

	@Column(name = "seguro_afiliacion_id")
	public Long getInsuranceAffiliation() {
		return insuranceAffiliation;
	}

	public void setInsuranceAffiliation(Long insuranceAffiliation) {
		this.insuranceAffiliation = insuranceAffiliation;
	}

	@Column(name = "nombre_empresa_anterior")
	public String getCompanyPreviousJob() {
		return companyPreviousJob;
	}

	public void setCompanyPreviousJob(String companyPreviousJob) {
		this.companyPreviousJob = companyPreviousJob;
	}

	@Column(name = "anios_empresa_anterior")
	public String getYearsWorked() {
		return yearsWorked;
	}

	public void setYearsWorked(String yearsWorked) {
		this.yearsWorked = yearsWorked;
	}

	@Column(name = "meses_empresa_anterior")
	public String getMonthsWorked() {
		return monthsWorked;
	}

	public void setMonthsWorked(String monthsWorked) {
		this.monthsWorked = monthsWorked;
	}

	@Column(name = "telefono_empresa_anterior")
	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	@Column(name = "referencia_familiar_id")
	public Long getReferenceFamilyId() {
		return referenceFamilyId;
	}

	public void setReferenceFamilyId(Long referenceFamilyId) {
		this.referenceFamilyId = referenceFamilyId;
	}

	@Column(name = "referencia_personal_1_id")
	public Long getReferencePersonalId() {
		return referencePersonalId;
	}

	public void setReferencePersonalId(Long referencePersonalId) {
		this.referencePersonalId = referencePersonalId;
	}

	@Column(name = "referencia_personal_2_id")
	public Long getReferencePersonalId2() {
		return referencePersonalId2;
	}

	public void setReferencePersonalId2(Long referencePersonalId2) {
		this.referencePersonalId2 = referencePersonalId2;
	}

	@Column(name = "estatus_financiamiento_id")
	public Long getCreditStatus() {
		return creditStatus;
	}

	public void setCreditStatus(Long creditStatus) {
		this.creditStatus = creditStatus;
	}

	@Column(name = "compra_id")
	public Long getCheckpointId() {
		return checkpointId;
	}

	public void setCheckpointId(Long checkpointId) {
		this.checkpointId = checkpointId;
	}

	@ManyToOne
	@JoinColumn(name = "auto_id", referencedColumnName = "id")
	public SellCarDetail getSellCarDetail() {
		return sellCarDetail;
	}

	public void setSellCarDetail(SellCarDetail sellCarDetail) {
		this.sellCarDetail = sellCarDetail;
	}

	@Column(name = "monto_enganche")
	public String getHookMount() {
		return hookMount;
	}

	public void setHookMount(String hookMount) {
		this.hookMount = hookMount;
	}

	@Column(name = "monto_financiamiento")
	public String getFinancingAmount() {
		return financingAmount;
	}

	public void setFinancingAmount(String financingAmount) {
		this.financingAmount = financingAmount;
	}

	@Column(name = "monto_mensual")
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Column(name = "meses_financiamiento")
	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	@Column(name = "fecha_solicitud")
	public Timestamp getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Timestamp currentDate) {
		this.currentDate = currentDate;
	}


	@Column(name = "comprobar_retraso")
	public String getDelay() {
		return delay;
	}

	public void setDelay(String delay) {
		this.delay = delay;
	}

	@Column(name = "uso_auto")
	public String getCarUse() {
		return carUse;
	}

	public void setCarUse(String carUse) {
		this.carUse = carUse;
	}

	@Column(name = "comprobar_ingreso")
	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	@Column(name = "credito_casa")
	public String getMortgageCredit() {
		return mortgageCredit;
	}

	public void setMortgageCredit(String mortgageCredit) {
		this.mortgageCredit = mortgageCredit;
	}

	@Column(name = "credito_auto")
	public String getAutomotiveCredit() {
		return automotiveCredit;
	}

	public void setAutomotiveCredit(String automotiveCredit) {
		this.automotiveCredit = automotiveCredit;
	}

	@Column(name = "posee_tarjeta")
	public String getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	@Column(name = "limite_tarjeta")
	public String getCreditCardLimit() {
		return creditCardLimit;
	}

	public void setCreditCardLimit(String creditCardLimit) {
		this.creditCardLimit = creditCardLimit;
	}

	@Column(name = "digitos_tarjeta")
	public String getCreditCardDigits() {
		return creditCardDigits;
	}

	public void setCreditCardDigits(String creditCardDigits) {
		this.creditCardDigits = creditCardDigits;
	}

	@Column(name = "activo")
	public Long getActivo() {
		return activo;
	}

	public void setActivo(Long activo) {
		this.activo = activo;
	}

	@Column(name = "pasos")
	public String getSteps() {
		return Steps;
	}

	public void setSteps(String steps) {
		Steps = steps;
	}

}
