package com.kavak.core.model;

import com.kavak.core.dto.MetaValueDTO;

import javax.persistence.*;

@Entity
@Table(name = "meta_values")
public class MetaValue {

	private Long id;
	private String groupCategory;
	private String groupName;
	private String optionName;
	private String alias;
	private boolean active;
	private OfferCheckpoint inspectionOverview;

	@Id
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "group_category")
	public String getGroupCategory() {
		return groupCategory;
	}

	public void setGroupCategory(String groupCategory) {
		this.groupCategory = groupCategory;
	}

	@Column(name = "group_name")
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Column(name = "option_name")
	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	@Column(name = "alias")
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	@Column(name = "activo")
	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@OneToOne
	@JoinColumn(name = "id")
	public OfferCheckpoint getInspectionOverview() {
		return inspectionOverview;
	}

	public void setInspectionOverview(OfferCheckpoint inspectionOverview) {
		this.inspectionOverview = inspectionOverview;
	}

	@Transient
	public MetaValueDTO getDTO() {
		MetaValueDTO metaValueDTO = new MetaValueDTO();
		metaValueDTO.setId(this.id);
		metaValueDTO.setGroupCategory(this.groupCategory);
		metaValueDTO.setGroupName(this.groupName);
		metaValueDTO.setOptionName(this.optionName);
		metaValueDTO.setAlias(this.alias);
		metaValueDTO.setActive(this.active);
		metaValueDTO.setInspectionOverviewDTO(this.inspectionOverview.getDTO());
		return metaValueDTO;
	}
}