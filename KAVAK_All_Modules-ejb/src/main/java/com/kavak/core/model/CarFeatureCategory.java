package com.kavak.core.model;

import com.kavak.core.dto.CarFeatureCategoryDTO;
import com.kavak.core.dto.CarFeatureItemDTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "v2_car_feature_category")
public class CarFeatureCategory {

	private Long id;
	private String name;
	private boolean active; 
	private List<CarFeatureItem> listCarFeatureItem;
	
	@Id
	@Column(name= "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(name= "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Column(name= "active")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	@OneToMany(mappedBy ="carFeatureCategory")
	public List<CarFeatureItem> getListCarFeatureItem() {
		return listCarFeatureItem;
	}

	public void setListCarFeatureItem(List<CarFeatureItem> listCarFeatureItem) {
		this.listCarFeatureItem = listCarFeatureItem;
	}
	
	@Transient
	public CarFeatureCategoryDTO getDTO(){
		CarFeatureCategoryDTO carFeatureCategoryDTO = new CarFeatureCategoryDTO();
		carFeatureCategoryDTO.setId(this.id);
		carFeatureCategoryDTO.setName(this.name);
		List<CarFeatureItemDTO> newListCarFeatureItemDTO = new ArrayList<>();
		for(CarFeatureItem carFeatureItemActual:this.getListCarFeatureItem()){
			newListCarFeatureItemDTO.add(carFeatureItemActual.getDTO());
		}
		carFeatureCategoryDTO.setListCarFeatureItemDTO(newListCarFeatureItemDTO);	
				
				
		return carFeatureCategoryDTO;
	}

	
}
