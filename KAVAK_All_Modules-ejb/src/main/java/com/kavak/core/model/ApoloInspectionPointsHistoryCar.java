package com.kavak.core.model;

import javax.persistence.*;

/**
 * Created by Enrique on 05-Jun-17.
 */
@Entity
@Table(name = "puntos_historia_vehicular_auto")
public class ApoloInspectionPointsHistoryCar {

    private Long id;
    private Long idSellCarDetail;// haciendo referencia a auto_id como identificador del carro
    private Long idApoloinspectionPointHistory;
    private boolean status;
    private ApoloInspectionPointsHistory apoloInspectionPointsHistory;

    @Id
    @Column(name ="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name ="auto_id")
    public Long getIdSellCarDetail() {
        return idSellCarDetail;
    }
    public void setIdSellCarDetail(Long idSellCarDetail) {
        this.idSellCarDetail = idSellCarDetail;
    }

    @Column(name ="puntos_historia_vehicular_id",insertable = false,updatable = false)
    public Long getIdApoloinspectionPointHistory() {
        return idApoloinspectionPointHistory;
    }

    public void setIdApoloinspectionPointHistory(Long idApoloinspectionPointHistory) {
        this.idApoloinspectionPointHistory = idApoloinspectionPointHistory;
    }
    @Column(name ="estatus_punto_vehicular")
    public boolean isStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }

    @ManyToOne
    @JoinColumn(name="puntos_historia_vehicular_id")
    public ApoloInspectionPointsHistory getApoloInspectionPointsHistory() {
        return apoloInspectionPointsHistory;
    }

    public void setApoloInspectionPointsHistory(ApoloInspectionPointsHistory apoloInspectionPointsHistory) {
        this.apoloInspectionPointsHistory = apoloInspectionPointsHistory;
    }
}
