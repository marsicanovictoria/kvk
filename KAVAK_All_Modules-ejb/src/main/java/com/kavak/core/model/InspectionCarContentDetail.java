package com.kavak.core.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="v2_inspection_car_content_details")
public class InspectionCarContentDetail {

	private Long id;
	private Long idInspection;
	private String inspectorEmail;
	private String whyCarText;
	private String whyCarAudio;
	private Date creationDate;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(name = "inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}

	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}
	@Column(name = "inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}

	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	@Column(name = "why_kavak_text")
	public String getWhyCarText() {
		return whyCarText;
	}

	public void setWhyCarText(String whyCarText) {
		this.whyCarText = whyCarText;
	}
	@Column(name = "why_kavak_audio")
	public String getWhyCarAudio() {
		return whyCarAudio;
	}

	public void setWhyCarAudio(String whyCarAudio) {
		this.whyCarAudio = whyCarAudio;
	}
	@Column(name = "creation_date")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
