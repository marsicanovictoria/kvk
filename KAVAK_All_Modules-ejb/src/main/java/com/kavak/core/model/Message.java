package com.kavak.core.model;

import com.kavak.core.dto.MessageDTO;
import com.kavak.core.enumeration.MessageEnum;

import javax.persistence.*;

@Entity
@Table(name="v2_message")
public class Message {

	private Long id;
	private String code;
	private String apiMessage;
	private String userMessage;
	private String basicRequirements;
	
	@Id
	@Column(name="id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="code")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	@Column(name="api_message")
	public String getApiMessage() {
		return apiMessage;
	}
	public void setApiMessage(String apiMessage) {
		this.apiMessage = apiMessage;
	}

	@Column(name="user_message")
	public String getUserMessage() {
		return userMessage;
	}
	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	@Column(name="basic_requirements")
	public String getBasicRequirements() {
		return basicRequirements;
	}
	public void setBasicRequirements(String basicRequirements) {
		this.basicRequirements = basicRequirements;
	}
	
	@Transient
	public MessageDTO getDTO(){
		MessageDTO messageDTO = new MessageDTO();
		messageDTO.setCode(MessageEnum.getByCode(this.code));
		messageDTO.setApiMessage(this.apiMessage);
		messageDTO.setUserMessage(this.userMessage);
		messageDTO.setBasicRequirements(this.basicRequirements);
		return messageDTO;
	}

	@Transient
	public MessageDTO getRequestDTO(String parameter){

		MessageDTO messageDTO = new MessageDTO();
		messageDTO.setCode(MessageEnum.getByCode(this.code));
		messageDTO.setApiMessage((this.apiMessage + parameter).substring(0,(this.apiMessage + parameter).length()-2));
		messageDTO.setUserMessage(this.userMessage);
		messageDTO.setBasicRequirements(this.basicRequirements);
		return messageDTO;
	}

	@Transient
	public MessageDTO getRemplaceParameterDTO(String parameter){

		MessageDTO messageDTO = new MessageDTO();
		messageDTO.setCode(MessageEnum.getByCode(this.code));
		messageDTO.setApiMessage((this.apiMessage).replace("(var)",parameter));
		if(this.userMessage != null) {
			messageDTO.setUserMessage((this.userMessage).replace("(var)", parameter));
		}
		messageDTO.setBasicRequirements(this.basicRequirements);
		return messageDTO;
	}

	@Transient
	public MessageDTO getRemplaceParameterTypeValueDTO(String parameter, String type){

		MessageDTO messageDTO = new MessageDTO();
		messageDTO.setCode(MessageEnum.getByCode(this.code));
		messageDTO.setApiMessage((this.apiMessage).replace("(var)",parameter).replace("(type)",type));
		if(this.userMessage != null) {
		messageDTO.setUserMessage((this.userMessage).replace("(var)",parameter).replace("(type)",type));
		}
		messageDTO.setBasicRequirements(this.basicRequirements);
		return messageDTO;
	}

	@Transient
	public MessageDTO getRequestWarrantyDTO(String parameter,String requestWarrantyType){

		MessageDTO messageDTO = new MessageDTO();
		messageDTO.setCode(MessageEnum.getByCode(this.code));
		messageDTO.setApiMessage((this.apiMessage + parameter).substring(0,(this.apiMessage + parameter).length()-2).replace("(var)",requestWarrantyType));
		messageDTO.setUserMessage(this.userMessage);
		messageDTO.setBasicRequirements(this.basicRequirements);
		return messageDTO;
	}
}