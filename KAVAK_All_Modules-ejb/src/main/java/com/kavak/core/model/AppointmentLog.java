package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "v2_appointment_log")
public class AppointmentLog {

    private Long id;     
    private Long userId;     
    private Long carId;      
    private Long locationId;    
    private String appointmentDate;   
    private Long appointmentHourBlockId;   
    private String deviceSessionId;   
    private String tokenId;   
    private String paymenType;    
    private String carPrice;  
    private String source;


    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="user_id")
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    @Column(name="car_id")
    public Long getCarId() {
        return carId;
    }
    public void setCarId(Long carId) {
        this.carId = carId;
    }
    
    @Column(name="location_id")
    public Long getLocationId() {
        return locationId;
    }
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }
    
    @Column(name="appointment_date")
    public String getAppointmentDate() {
        return appointmentDate;
    }
    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }
    
    @Column(name="appointment_hour_block_id")
    public Long getAppointmentHourBlockId() {
        return appointmentHourBlockId;
    }
    public void setAppointmentHourBlockId(Long appointmentHourBlockId) {
        this.appointmentHourBlockId = appointmentHourBlockId;
    }
    
    @Column(name="device_session_id")
    public String getDeviceSessionId() {
        return deviceSessionId;
    }
    public void setDeviceSessionId(String deviceSessionId) {
        this.deviceSessionId = deviceSessionId;
    }
    
    @Column(name="token_id")
    public String getTokenId() {
        return tokenId;
    }
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }
    
    @Column(name="payment_type")
    public String getPaymenType() {
        return paymenType;
    }
    public void setPaymenType(String paymenType) {
        this.paymenType = paymenType;
    }
    
    @Column(name="car_price")
    public String getCarPrice() {
        return carPrice;
    }
    public void setCarPrice(String carPrice) {
        this.carPrice = carPrice;
    }
    
    @Column(name="source")
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

}