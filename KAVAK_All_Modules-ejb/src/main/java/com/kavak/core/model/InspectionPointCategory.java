package com.kavak.core.model;

import com.kavak.core.dto.InspectionPointCategoryDTO;
import com.kavak.core.dto.InspectionPointDTO;
import com.kavak.core.dto.InspectionPointSubCategoryDTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="v2_inspection_point_category")
public class InspectionPointCategory {
	
	private Long id;
	private String name;
	private boolean active;
    private List<InspectionPoint> listInspectionPoint;
	private List<InspectionPointSubCategory> listInspectionPointSubCategory;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="inspectionPointCategory")
	public List<InspectionPointSubCategory> getListInspectionPointSubCategory() {
		return listInspectionPointSubCategory;
	}
	public void setListInspectionPointSubCategory(List<InspectionPointSubCategory> listInspectionPointSubCategory) {
		this.listInspectionPointSubCategory = listInspectionPointSubCategory;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="inspectionPointCategory")
	public List<InspectionPoint> getListInspectionPoint() {
		return listInspectionPoint;
	}
	public void setListInspectionPoint(List<InspectionPoint> listInspectionPoint) {
		this.listInspectionPoint = listInspectionPoint;
	}

	@Transient
	public InspectionPointCategoryDTO getDTO(){
		InspectionPointCategoryDTO inspectionPointCategoryDTO = new InspectionPointCategoryDTO();
		inspectionPointCategoryDTO.setId(this.id);
		inspectionPointCategoryDTO.setName(this.name);
		inspectionPointCategoryDTO.setActive(this.active);
		List<InspectionPointSubCategoryDTO> listInspectionPointSubCategoryDTO = new ArrayList<>();
		List<InspectionPointDTO> listInspectionPointDTO = new ArrayList<>();

		for(InspectionPoint inspectionPointActual: this.getListInspectionPoint()){
			listInspectionPointDTO.add(inspectionPointActual.getDTO());
		}

        for(InspectionPointSubCategory inspectionPointSubCategoryActual: this.getListInspectionPointSubCategory()){
            listInspectionPointSubCategoryDTO.add(inspectionPointSubCategoryActual.getDTO());
        }
		inspectionPointCategoryDTO.setListInspectionPointSubCategoryDTO(listInspectionPointSubCategoryDTO);
		inspectionPointCategoryDTO.setListInspectionPointDTO(listInspectionPointDTO);
		return inspectionPointCategoryDTO;
	}
}