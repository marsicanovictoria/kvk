package com.kavak.core.model;

import javax.persistence.*;

@Entity
@Table(name = "referecia_financiamiento")
public class FinancingReferece {

	private Long id;
	private String name;
	private String firstLastname;
	private String secondLastname;
	private String Phone;
	private String address;
	private Long relationship;
	private String typeReference;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "nombres")
	public String getName() {
		return name;
	}

	@Column(name = "nombres")
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "primer_apellido")
	public String getFirstLastname() {
		return firstLastname;
	}

	public void setFirstLastname(String firstLastname) {
		this.firstLastname = firstLastname;
	}

	@Column(name = "segundo_apellido")
	public String getSecondLastname() {
		return secondLastname;
	}

	public void setSecondLastname(String secondLastname) {
		this.secondLastname = secondLastname;
	}

	@Column(name = "telefono")
	public String getPhone() {
		return Phone;
	}

	public void setPhone(String Phone) {
		this.Phone = Phone;
	}

	@Column(name = "direccion")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "parentesco_id")
	public Long getRelationship() {
		return relationship;
	}

	public void setRelationship(Long relationship) {
		this.relationship = relationship;
	}

	@Column(name = "tipo_referencia")
	public String getTypeReference() {
		return typeReference;
	}

	public void setTypeReference(String typeReference) {
		this.typeReference = typeReference;
	}

}
