package com.kavak.core.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "v2_car_model")
public class CarModel {

	private Long id;
	private String description;
	private CarMake carMake;
	private Timestamp creation_date;
	private boolean active;
	private List<CarVersion> carVersion;
	private List<CarscoutCatalogue> carscoutCatalogue;

	@Id
	@Column(name = "model_id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne
	@JoinColumn(name = "make_id", referencedColumnName = "id")
	public CarMake getCarMake() {
		return carMake;
	}

	public void setCarMake(CarMake carMake) {
		this.carMake = carMake;
	}

	@Column(name = "creation_date")
	public Timestamp getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Timestamp creation_date) {
		this.creation_date = creation_date;
	}

	@Column(name = "active_ind")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}


	@OneToMany(mappedBy="carModel")
	public List<CarVersion> getCarVersion() {
		return carVersion;
	}

	public void setCarVersion(List<CarVersion> carVersion) {
		this.carVersion = carVersion;
	}

	@OneToMany(mappedBy = "carModel")
	public List<CarscoutCatalogue> getCarscoutCatalogue() {
		return carscoutCatalogue;
	}

	public void setCarscoutCatalogue(List<CarscoutCatalogue> carscoutCatalogue) {
		this.carscoutCatalogue = carscoutCatalogue;
	}

}
