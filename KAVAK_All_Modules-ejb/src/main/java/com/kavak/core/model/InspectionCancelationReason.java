package com.kavak.core.model;

import com.kavak.core.dto.InspectionCancelationReasonDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="v2_inspection_cancellation_reason")
public class InspectionCancelationReason {

    private Long id;
    private String name;
    private boolean active;
    private Date creationDate;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Column(name="is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    @Column(name="creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Transient
    public InspectionCancelationReasonDTO getDTO(){
        InspectionCancelationReasonDTO inspectionCancelationReasonDTO = new InspectionCancelationReasonDTO();
        inspectionCancelationReasonDTO.setId(this.id);
        inspectionCancelationReasonDTO.setName(this.name);
        return inspectionCancelationReasonDTO;
    }
}
