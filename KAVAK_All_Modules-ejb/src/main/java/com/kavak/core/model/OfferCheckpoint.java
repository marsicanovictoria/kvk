package com.kavak.core.model;

import com.kavak.core.dto.OfferCheckpointDTO;
import com.kavak.core.util.Constants;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "oferta_checkpoints")
public class OfferCheckpoint {

    private Long id;
    private Long idUser;
    private String customerName;
    private String customerLastName;
    private String email;
    private String sku;
    private String carMake;
    private String carModel;
    private String carVersion;
    private Long carYear;
    private Long carKm;
    private String fullVersion;
    private String min30DaysOffer;
    private String max30DaysOffer;
    private String minInstantOffer;
    private String maxInstantOffer;
    private String minConsignationOffer;
    private String maxConsignationOffer;
    private String marketPrice;
    private Long offerType;
    private String descriptionOfferType;
    private User user;
    private Long wishList;
    private String offerPunished;
    private String factorKmApplied;
    private String zipCode;
    private Long idStatus;
    private String statusDetail;
    private String urlRecoverOffer;
    private String salePrice; // es el precio de la guia autometrica
			      // (sale_price_ga)
    private String buyPrice;
    private String originalPrice30D;
    private String originalPriceInstance;
    private String originalPriceConsignation;
    private String salePriceKavak; // es el precio de venta de kavak
				   // sale_price_kavak
    private String quantitySamples;
    private String averageDateSamples;
    private Timestamp updateDateOffer;
    private String versionGuiaAutometrica;
    private String offerDetail;
    private String trustedOffer;
    private String other3;
    private String other4;
    private String other5;
    private String other6;
    private String other7;
    private String other8;
    private Long sentNetsuite;
    private String circulationCardUrl;
    private String carInvoiceUrl;
    private Timestamp offerDate;
    private String offerRecovery;
    private Integer rootOffer;
    private boolean sendEmail;
    private boolean sentInternalEmail;
    private Long idHourInpectionSlot;
    private String hourInpectionSlotText;
    private String inspectionDate;
    private Long inspectionLocation;
    private String inspectionLocationDescription;
    private String inspectionCenterDescription;
    private Long codeNetsuiteItem;
    private String minKm;
    private String maxKm;
    private Long duplicatedOfferControl;
    private Long idExteriorColor;
    private String carPhoto;
    private String startHour;
    private String endHour;
    private Long assignedEm;
    private Long netsuiteOpportunityId;
    private String netsuiteOpportunityURL;
    private Timestamp netsuiteIdUpdateDate;
    private String offerSource;
    private Long netsuiteIdAttemps;
    private boolean followUpEmailSent;
    private boolean notAcceptedOfferEmailSent;
    private boolean newOfferEmailSent;
    private String segmentType;
    private String comment;
    private String pcVendeTuAuto;
    private String pcEbc;
    private String pvEbc;
    private String versionEbc;
    private Long avgKm;
    private Long setPriceInst;
    private Long internalUserId;
    private boolean selectDayPlaceCustomerSmsSent;
    private boolean selectDayPlaceCenterSmsSent;
    private boolean releaseAuditOfferSmsSent;
    private boolean acceptOfferNotContinueSmsSent;
    private boolean inspectionReminderNotificationSent;
    private String customerComment;
    private String customerVoice;
    private String awsKarsnapOffer;
    private String awsLowestRange;
    private String awsHeighestRange;
    private String awsInstantOffer;
    private String awsLrInstantOffer;
    private String awsHrInstantOffer;
    private String awsCncOffer;
    private String awsLrCncOffer;
    private String awsHrCncOffer;
    private String awsPc30dOrg;
    private String awsPcInsOrg;
    private String awsPcCncOrg;
    private String awsOffer;
    private String awsPrecioVenta;
    private String rimMaterial;
    private String lights;
    private String air;
    private Timestamp awsFechaActOffer;
    private Long abTestResult;
    private String abTestCode;
    private boolean newOfferAuditEmailSent;
    


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Column(name = "id_usuario", insertable = false, updatable = false)
    public Long getIdUser() {
	return idUser;
    }

    public void setIdUser(Long idUser) {
	this.idUser = idUser;
    }

    @Column(name = "nombre")
    public String getCustomerName() {
	return customerName;
    }

    public void setCustomerName(String customerName) {
	this.customerName = customerName;
    }

    @Column(name = "apellido")
    public String getCustomerLastName() {
	return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
	this.customerLastName = customerLastName;
    }

    @Column(name = "email")
    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    @Column(name = "correo_interno_enviado")
    public boolean isSentInternalEmail() {
	return sentInternalEmail;
    }

    public void setSentInternalEmail(boolean sentInternalEmail) {
	this.sentInternalEmail = sentInternalEmail;
    }

    @Column(name = "sku")
    public String getSku() {
	return sku;
    }

    public void setSku(String sku) {
	this.sku = sku;
    }

    @Column(name = "marca")
    public String getCarMake() {
	return carMake;
    }

    public void setCarMake(String carMake) {
	this.carMake = carMake;
    }

    @Column(name = "modelo")
    public String getCarModel() {
	return carModel;
    }

    public void setCarModel(String carModel) {
	this.carModel = carModel;
    }

    @Column(name = "version")
    public String getCarVersion() {
	return carVersion;
    }

    public void setCarVersion(String carVersion) {
	this.carVersion = carVersion;
    }

    @Column(name = "anio")
    public Long getCarYear() {
	return carYear;
    }

    public void setCarYear(Long carYear) {
	this.carYear = carYear;
    }

    @Column(name = "km")
    public Long getCarKm() {
	return carKm;
    }

    public void setCarKm(Long carKm) {
	this.carKm = carKm;
    }

    @Column(name = "full_version")
    public String getFullVersion() {
	return fullVersion;
    }

    public void setFullVersion(String fullVersion) {
	this.fullVersion = fullVersion;
    }

    @Column(name = "oferta_caruvi_min")
    public String getMin30DaysOffer() {
	return min30DaysOffer;
    }

    public void setMin30DaysOffer(String min30DaysOffer) {
	this.min30DaysOffer = min30DaysOffer;
    }

    @Column(name = "oferta_caruvi_max")
    public String getMax30DaysOffer() {
	return max30DaysOffer;
    }

    public void setMax30DaysOffer(String max30DaysOffer) {
	this.max30DaysOffer = max30DaysOffer;
    }

    @Column(name = "oferta_instantanea_min")
    public String getMinInstantOffer() {
	return minInstantOffer;
    }

    public void setMinInstantOffer(String minInstantOffer) {
	this.minInstantOffer = minInstantOffer;
    }

    @Column(name = "oferta_instantanea_max")
    public String getMaxInstantOffer() {
	return maxInstantOffer;
    }

    public void setMaxInstantOffer(String maxInstantOffer) {
	this.maxInstantOffer = maxInstantOffer;
    }

    @Column(name = "oferta_cnc_min")
    public String getMinConsignationOffer() {
	return minConsignationOffer;
    }

    public void setMinConsignationOffer(String minConsignationOffer) {
	this.minConsignationOffer = minConsignationOffer;
    }

    @Column(name = "oferta_cnc_max")
    public String getMaxConsignationOffer() {
	return maxConsignationOffer;
    }

    public void setMaxConsignationOffer(String maxConsignationOffer) {
	this.maxConsignationOffer = maxConsignationOffer;
    }

    @Column(name = "buy_price")
    public String getBuyPrice() {
	return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
	this.buyPrice = buyPrice;
    }

    @Column(name = "precio_mercado")
    public String getMarketPrice() {
	return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
	this.marketPrice = marketPrice;
    }

    @Column(name = "tipo_oferta")
    public Long getOfferType() {
	return offerType;
    }

    public void setOfferType(Long offerType) {
	this.offerType = offerType;
    }

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    public User getUser() {
	return user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    @Column(name = "wishlist")
    public Long getWishList() {
	return wishList;
    }

    public void setWishList(Long wishList) {
	this.wishList = wishList;
    }

    @Column(name = "oferta_castigada")
    public String getOfferPunished() {
	return offerPunished;
    }

    public void setOfferPunished(String offerPunished) {
	this.offerPunished = offerPunished;
    }

    @Column(name = "factor_km_aplicado")
    public String getFactorKmApplied() {
	return factorKmApplied;
    }

    public void setFactorKmApplied(String factorKmApplied) {
	this.factorKmApplied = factorKmApplied;
    }

    @Column(name = "codigo_postal")
    public String getZipCode() {
	return zipCode;
    }

    public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
    }

    @Column(name = "estatus")
    public Long getIdStatus() {
	return idStatus;
    }

    public void setIdStatus(Long idStatus) {
	this.idStatus = idStatus;
    }

    @Column(name = "estatus_detalle")
    public String getStatusDetail() {
	return statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
	this.statusDetail = statusDetail;
    }

    @Column(name = "descripcion_tipo_oferta")
    public String getDescriptionOfferType() {
	return descriptionOfferType;
    }

    public void setDescriptionOfferType(String descriptionOfferType) {
	this.descriptionOfferType = descriptionOfferType;
    }

    @Column(name = "url_recuperar_oferta")
    public String getUrlRecoverOffer() {
	return urlRecoverOffer;
    }

    public void setUrlRecoverOffer(String urlRecoverOffer) {
	this.urlRecoverOffer = urlRecoverOffer;
    }

    @Column(name = "sale_price")
    public String getSalePrice() {
	return salePrice;
    }

    public void setSalePrice(String salePrice) {
	this.salePrice = salePrice;
    }

    @Column(name = "pc_30d_org")
    public String getOriginalPrice30D() {
	return originalPrice30D;
    }

    public void setOriginalPrice30D(String originalPrice30D) {
	this.originalPrice30D = originalPrice30D;
    }

    @Column(name = "pc_ins_org")
    public String getOriginalPriceInstance() {
	return originalPriceInstance;
    }

    public void setOriginalPriceInstance(String originalPriceInstance) {
	this.originalPriceInstance = originalPriceInstance;
    }

    @Column(name = "pc_cnc_org")
    public String getOriginalPriceConsignation() {
	return originalPriceConsignation;
    }

    public void setOriginalPriceConsignation(String originalPriceConsignation) {
	this.originalPriceConsignation = originalPriceConsignation;
    }

    @Column(name = "precio_venta")
    public String getSalePriceKavak() {
	return salePriceKavak;
    }

    public void setSalePriceKavak(String salePriceKavak) {
	this.salePriceKavak = salePriceKavak;
    }

    @Column(name = "cant_muestras")
    public String getQuantitySamples() {
	return quantitySamples;
    }

    public void setQuantitySamples(String quantitySamples) {
	this.quantitySamples = quantitySamples;
    }

    @Column(name = "med_date_muestras")
    public String getAverageDateSamples() {
	return averageDateSamples;
    }

    public void setAverageDateSamples(String averageDateSamples) {
	this.averageDateSamples = averageDateSamples;
    }

    @Column(name = "fecha_act_offer")
    public Timestamp getUpdateDateOffer() {
	return updateDateOffer;
    }

    public void setUpdateDateOffer(Timestamp updateDateOffer) {
	this.updateDateOffer = updateDateOffer;
    }

    @Column(name = "version_ga")
    public String getVersionGuiaAutometrica() {
	return versionGuiaAutometrica;
    }

    public void setVersionGuiaAutometrica(String versionGuiaAutometrica) {
	this.versionGuiaAutometrica = versionGuiaAutometrica;
    }

    @Column(name = "detalle_oferta")
    public String getOfferDetail() {
	return offerDetail;
    }

    public void setOfferDetail(String offerDetail) {
	this.offerDetail = offerDetail;
    }

    @Column(name = "oferta_confianza")
    public String getTrustedOffer() {
	return trustedOffer;
    }

    public void setTrustedOffer(String trustedOffer) {
	this.trustedOffer = trustedOffer;
    }

    @Column(name = "OTRO3")
    public String getOther3() {
	return other3;
    }

    public void setOther3(String other3) {
	this.other3 = other3;
    }

    @Column(name = "OTRO4")
    public String getOther4() {
	return other4;
    }

    public void setOther4(String other4) {
	this.other4 = other4;
    }

    @Column(name = "OTRO5")
    public String getOther5() {
	return other5;
    }

    public void setOther5(String other5) {
	this.other5 = other5;
    }

    @Column(name = "OTRO6")
    public String getOther6() {
	return other6;
    }

    public void setOther6(String other6) {
	this.other6 = other6;
    }

    @Column(name = "OTRO7")
    public String getOther7() {
	return other7;
    }

    public void setOther7(String other7) {
	this.other7 = other7;
    }

    @Column(name = "OTRO8")
    public String getOther8() {
	return other8;
    }

    public void setOther8(String other8) {
	this.other8 = other8;
    }

    @Column(name = "enviado_netsuite")
    public Long isSentNetsuite() {
	return sentNetsuite;
    }

    public void setSentNetsuite(Long sentNetsuite) {
	this.sentNetsuite = sentNetsuite;
    }

    @Column(name = "tarjeta_circulacion")
    public String getCirculationCardUrl() {
	return circulationCardUrl;
    }

    public void setCirculationCardUrl(String circulationCardUrl) {
	this.circulationCardUrl = circulationCardUrl;
    }

    @Column(name = "factura_auto")
    public String getCarInvoiceUrl() {
	return carInvoiceUrl;
    }

    public void setCarInvoiceUrl(String carInvoiceUrl) {
	this.carInvoiceUrl = carInvoiceUrl;
    }

    @Column(name = "fecha_oferta")
    public Timestamp getOfferDate() {
	return offerDate;
    }

    public void setOfferDate(Timestamp offerDate) {
	this.offerDate = offerDate;
    }
    
    @Column(name = "recupero_oferta")
    public String getOfferRecovery() {
		return offerRecovery;
	}

	public void setOfferRecovery(String offerRecovery) {
		this.offerRecovery = offerRecovery;
	}
	
	@Column(name = "id_oferta_padre")
	public Integer getRootOffer() {
		return rootOffer;
	}

	public void setRootOffer(Integer rootOffer) {
		this.rootOffer = rootOffer;
	}

	@Column(name = "correo_enviado")
    public boolean isSendEmail() {
	return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
	this.sendEmail = sendEmail;
    }

    @Column(name = "hora_inspeccion")
    public Long getIdHourInpectionSlot() {
	return idHourInpectionSlot;
    }

    public void setIdHourInpectionSlot(Long idHourInpectionSlot) {
	this.idHourInpectionSlot = idHourInpectionSlot;
    }

    @Column(name = "hora_inspeccion_texto")
    public String getHourInpectionSlotText() {
	return hourInpectionSlotText;
    }

    public void setHourInpectionSlotText(String hourInpectionSlotText) {
	this.hourInpectionSlotText = hourInpectionSlotText;
    }

    @Column(name = "fecha_inspeccion")
    public String getInspectionDate() {
	return inspectionDate;
    }

    public void setInspectionDate(String inspectionDate) {
	this.inspectionDate = inspectionDate;
    }

    @Column(name = "lugar_inspeccion")
    public Long getInspectionLocation() {
	return inspectionLocation;
    }

    public void setInspectionLocation(Long inspectionLocation) {
	this.inspectionLocation = inspectionLocation;
    }

    @Column(name = "lugar_inspeccion_texto")
    public String getInspectionLocationDescription() {
	return inspectionLocationDescription;
    }

    public void setInspectionLocationDescription(String inspectionLocationDescription) {
	this.inspectionLocationDescription = inspectionLocationDescription;
    }

    @Column(name = "centro_inspeccion")
    public String getInspectionCenterDescription() {
	return inspectionCenterDescription;
    }

    public void setInspectionCenterDescription(String inspectionCenterDescription) {
	this.inspectionCenterDescription = inspectionCenterDescription;
    }

    @Column(name = "codigo_item_netsuite")
    public Long getCodeNetsuiteItem() {
	return codeNetsuiteItem;
    }

    public void setCodeNetsuiteItem(Long codeNetsuiteItem) {
	this.codeNetsuiteItem = codeNetsuiteItem;
    }

    @Column(name = "min_km")
    public String getMinKm() {
	return minKm;
    }

    public void setMinKm(String minKm) {
	this.minKm = minKm;
    }

    @Column(name = "max_km")
    public String getMaxKm() {
	return maxKm;
    }

    public void setMaxKm(String maxKm) {
	this.maxKm = maxKm;
    }

    @Column(name = "control_oferta_duplicada")
    public Long getDuplicatedOfferControl() {
	return duplicatedOfferControl;
    }

    public void setDuplicatedOfferControl(Long duplicatedOfferControl) {
	this.duplicatedOfferControl = duplicatedOfferControl;
    }

    @Column(name = "exterior_color_id")
    public Long getIdExteriorColor() {
	return idExteriorColor;
    }

    public void setIdExteriorColor(Long idExteriorColor) {
	this.idExteriorColor = idExteriorColor;
    }

    @Column(name = "car_photo")
    public String getCarPhoto() {
	return carPhoto;
    }

    public void setCarPhoto(String carPhoto) {
	this.carPhoto = carPhoto;
    }

    @Column(name = "hora_inicio")
    public String getStartHour() {
	return startHour;
    }

    public void setStartHour(String startHour) {
	this.startHour = startHour;
    }

    @Column(name = "hora_fin")
    public String getEndHour() {
	return endHour;
    }

    public void setEndHour(String endHour) {
	this.endHour = endHour;
    }

    @Column(name = "assigned_em")
    public Long getAssignedEm() {
	return assignedEm;
    }

    public void setAssignedEm(Long assignedEm) {
	this.assignedEm = assignedEm;
    }

    @Column(name = "netsuite_opportunity_id")
    public Long getNetsuiteOpportunityId() {
	return netsuiteOpportunityId;
    }

    public void setNetsuiteOpportunityId(Long netsuiteOpportunityId) {
	this.netsuiteOpportunityId = netsuiteOpportunityId;
    }

    @Column(name = "url_netsuite_opportunity")
    public String getNetsuiteOpportunityURL() {
	return netsuiteOpportunityURL;
    }

    public void setNetsuiteOpportunityURL(String netsuiteOpportunityURL) {
	this.netsuiteOpportunityURL = netsuiteOpportunityURL;
    }

    @Column(name = "sent_to_netsuite_date")
    public Timestamp getNetsuiteIdUpdateDate() {
	return netsuiteIdUpdateDate;
    }

    public void setNetsuiteIdUpdateDate(Timestamp netsuiteIdUpdateDate) {
	this.netsuiteIdUpdateDate = netsuiteIdUpdateDate;
    }

    @Column(name = "origen_oferta")
    public String getOfferSource() {
	return offerSource;
    }

    public void setOfferSource(String offerSource) {
	this.offerSource = offerSource;
    }

    @Column(name = "intentos_netsuite_opportunity_id")
    public Long getNetsuiteIdAttemps() {
	return netsuiteIdAttemps;
    }

    public void setNetsuiteIdAttemps(Long netsuiteIdAttemps) {
	this.netsuiteIdAttemps = netsuiteIdAttemps;
    }

    @Column(name = "followup_email_sent")
    public boolean isFollowUpEmailSent() {
	return followUpEmailSent;
    }

    public void setFollowUpEmailSent(boolean followUpEmailSent) {
	this.followUpEmailSent = followUpEmailSent;
    }

    @Column(name = "notacceptedoffer_email_sent")
    public boolean isNotAcceptedOfferEmailSent() {
	return notAcceptedOfferEmailSent;
    }

    public void setNotAcceptedOfferEmailSent(boolean notAcceptedOfferEmailSent) {
	this.notAcceptedOfferEmailSent = notAcceptedOfferEmailSent;
    }

    @Column(name = "segment_type")
    public String getSegmentType() {
	return segmentType;
    }

    public void setSegmentType(String segmentType) {
	this.segmentType = segmentType;
    }

    @Column(name = "new_offer_email_sent")
    public boolean isNewOfferEmailSent() {
	return newOfferEmailSent;
    }

    public void setNewOfferEmailSent(boolean newOfferEmailSent) {
	this.newOfferEmailSent = newOfferEmailSent;
    }

    @Column(name = "comentarios")
    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    @Column(name = "pc_vendetuauto")
    public String getPcVendeTuAuto() {
	return pcVendeTuAuto;
    }

    public void setPcVendeTuAuto(String pcVendeTuAuto) {
	this.pcVendeTuAuto = pcVendeTuAuto;
    }

    @Column(name = "pc_ebc")
    public String getPcEbc() {
	return pcEbc;
    }

    public void setPcEbc(String pcEbc) {
	this.pcEbc = pcEbc;
    }

    @Column(name = "pv_ebc")
    public String getPvEbc() {
	return pvEbc;
    }

    public void setPvEbc(String pvEbc) {
	this.pvEbc = pvEbc;
    }

    @Column(name = "version_ebc")
    public String getVersionEbc() {
	return versionEbc;
    }

    public void setVersionEbc(String versionEbc) {
	this.versionEbc = versionEbc;
    }

    @Column(name = "avg_km")
    public Long getAvgKm() {
	return avgKm;
    }

    public void setAvgKm(Long avgKm) {
	this.avgKm = avgKm;
    }

    @Column(name = "SET_PRICE_INST")
    public Long getSetPriceInst() {
	return setPriceInst;
    }

    public void setSetPriceInst(Long setPriceInst) {
	this.setPriceInst = setPriceInst;
    }

    @Column(name = "internal_user_id")
    public Long getInternalUserId() {
	return internalUserId;
    }

    public void setInternalUserId(Long internalUserId) {
	this.internalUserId = internalUserId;
    }

    @Column(name = "select_day_place_customer_sms_sent")
    public boolean isSelectDayPlaceCustomerSmsSent() {
        return selectDayPlaceCustomerSmsSent;
    }

    public void setSelectDayPlaceCustomerSmsSent(boolean selectDayPlaceCustomerSmsSent) {
        this.selectDayPlaceCustomerSmsSent = selectDayPlaceCustomerSmsSent;
    }

    @Column(name = "select_day_place_center_sms_sent")
    public boolean isSelectDayPlaceCenterSmsSent() {
        return selectDayPlaceCenterSmsSent;
    }

    public void setSelectDayPlaceCenterSmsSent(boolean selectDayPlaceCenterSmsSent) {
        this.selectDayPlaceCenterSmsSent = selectDayPlaceCenterSmsSent;
    }

    @Column(name = "release_audit_offer_sms_sent")
    public boolean isReleaseAuditOfferSmsSent() {
        return releaseAuditOfferSmsSent;
    }

    public void setReleaseAuditOfferSmsSent(boolean releaseAuditOfferSmsSent) {
        this.releaseAuditOfferSmsSent = releaseAuditOfferSmsSent;
    }

    @Column(name = "accept_offer_not_continue_sms_sent")
    public boolean isAcceptOfferNotContinueSmsSent() {
        return acceptOfferNotContinueSmsSent;
    }

    public void setAcceptOfferNotContinueSmsSent(boolean acceptOfferNotContinueSmsSent) {
        this.acceptOfferNotContinueSmsSent = acceptOfferNotContinueSmsSent;
    }

    @Column(name = "inspection_reminder_notification_sent")
    public boolean isInspectionReminderNotificationSent() {
        return inspectionReminderNotificationSent;
    }

    public void setInspectionReminderNotificationSent(boolean inspectionReminderNotificationSent) {
        this.inspectionReminderNotificationSent = inspectionReminderNotificationSent;
    }

    @Column(name = "customer_comment")
    public String getCustomerComment() {
        return customerComment;
    }

    public void setCustomerComment(String customerComment) {
        this.customerComment = customerComment;
    }

    @Column(name = "customer_voice")
    public String getCustomerVoice() {
        return customerVoice;
    }

    public void setCustomerVoice(String customerVoice) {
        this.customerVoice = customerVoice;
    }
    
    @Column(name = "ab_test_result")
    public Long getAbTestResult() {
        return abTestResult;
    }

    public void setAbTestResult(Long abTestResult) {
        this.abTestResult = abTestResult;
    }

    @Column(name = "ab_test_code")
    public String getAbTestCode() {
        return abTestCode;
    }

    public void setAbTestCode(String abTestCode) {
        this.abTestCode = abTestCode;
    }

    @Column(name = "aws_karsnap_offer")
    public String getAwsKarsnapOffer() {
        return awsKarsnapOffer;
    }

    public void setAwsKarsnapOffer(String awsKarsnapOffer) {
        this.awsKarsnapOffer = awsKarsnapOffer;
    }

    @Column(name = "aws_lowest_range")
    public String getAwsLowestRange() {
        return awsLowestRange;
    }

    public void setAwsLowestRange(String awsLowestRange) {
        this.awsLowestRange = awsLowestRange;
    }

    @Column(name = "aws_heighest_range")
    public String getAwsHeighestRange() {
        return awsHeighestRange;
    }

    public void setAwsHeighestRange(String awsHeighestRange) {
        this.awsHeighestRange = awsHeighestRange;
    }

    @Column(name = "aws_instant_offer")
    public String getAwsInstantOffer() {
        return awsInstantOffer;
    }

    public void setAwsInstantOffer(String awsInstantOffer) {
        this.awsInstantOffer = awsInstantOffer;
    }

    @Column(name = "aws_lr_instant_offer")
    public String getAwsLrInstantOffer() {
        return awsLrInstantOffer;
    }

    public void setAwsLrInstantOffer(String awsLrInstantOffer) {
        this.awsLrInstantOffer = awsLrInstantOffer;
    }

    @Column(name = "aws_hr_instant_offer")
    public String getAwsHrInstantOffer() {
        return awsHrInstantOffer;
    }

    public void setAwsHrInstantOffer(String awsHrInstantOffer) {
        this.awsHrInstantOffer = awsHrInstantOffer;
    }

    @Column(name = "aws_cnc_offer")
    public String getAwsCncOffer() {
        return awsCncOffer;
    }

    public void setAwsCncOffer(String awsCncOffer) {
        this.awsCncOffer = awsCncOffer;
    }

    @Column(name = "aws_lr_cnc_offer")
    public String getAwsLrCncOffer() {
        return awsLrCncOffer;
    }

    public void setAwsLrCncOffer(String awsLrCncOffer) {
        this.awsLrCncOffer = awsLrCncOffer;
    }

    @Column(name = "aws_hr_cnc_offer")
    public String getAwsHrCncOffer() {
        return awsHrCncOffer;
    }

    public void setAwsHrCncOffer(String awsHrCncOffer) {
        this.awsHrCncOffer = awsHrCncOffer;
    }

    @Column(name = "aws_pc_30d_org")
    public String getAwsPc30dOrg() {
        return awsPc30dOrg;
    }

    public void setAwsPc30dOrg(String awsPc30dOrg) {
        this.awsPc30dOrg = awsPc30dOrg;
    }

    @Column(name = "aws_pc_ins_org")
    public String getAwsPcInsOrg() {
        return awsPcInsOrg;
    }

    public void setAwsPcInsOrg(String awsPcInsOrg) {
        this.awsPcInsOrg = awsPcInsOrg;
    }

    @Column(name = "aws_pc_cnc_org")
    public String getAwsPcCncOrg() {
        return awsPcCncOrg;
    }

    public void setAwsPcCncOrg(String awsPcCncOrg) {
        this.awsPcCncOrg = awsPcCncOrg;
    }

    @Column(name = "aws_offer")
    public String getAwsOffer() {
        return awsOffer;
    }

    public void setAwsOffer(String awsOffer) {
        this.awsOffer = awsOffer;
    }

    @Column(name = "aws_precio_venta")
    public String getAwsPrecioVenta() {
        return awsPrecioVenta;
    }

    public void setAwsPrecioVenta(String awsPrecioVenta) {
        this.awsPrecioVenta = awsPrecioVenta;
    }

    @Column(name = "rim_material")
    public String getRimMaterial() {
        return rimMaterial;
    }

    public void setRimMaterial(String rimMaterial) {
        this.rimMaterial = rimMaterial;
    }

    @Column(name = "lights")
    public String getLights() {
        return lights;
    }

    public void setLights(String lights) {
        this.lights = lights;
    }

    @Column(name = "air")
    public String getAir() {
        return air;
    }

    public void setAir(String air) {
        this.air = air;
    }

    @Column(name = "aws_fecha_act_offer")
    public Timestamp getAwsFechaActOffer() {
        return awsFechaActOffer;
    }

    public void setAwsFechaActOffer(Timestamp awsFechaActOffer) {
        this.awsFechaActOffer = awsFechaActOffer;
    }

    @Column(name = "new_offer_audit_email_sent")
    public boolean isNewOfferAuditEmailSent() {
        return newOfferAuditEmailSent;
    }

    public void setNewOfferAuditEmailSent(boolean newOfferAuditEmailSent) {
        this.newOfferAuditEmailSent = newOfferAuditEmailSent;
    }

    @Transient
    public OfferCheckpointDTO getDTO() {
	OfferCheckpointDTO offerCheckpointDTO = new OfferCheckpointDTO();
	// SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	offerCheckpointDTO.setId(this.id);
	offerCheckpointDTO.setIdUser(this.idUser);
	offerCheckpointDTO.setCustomerName(this.customerName + this.customerLastName);
	offerCheckpointDTO.setEmail(this.email);
	offerCheckpointDTO.setSku(this.sku);
	offerCheckpointDTO.setCarName(this.carMake + " " + this.carModel + " " + this.carVersion);
	offerCheckpointDTO.setCarMake(this.carMake);
	offerCheckpointDTO.setCarModel(this.carModel);
	offerCheckpointDTO.setCarVersion(this.carVersion);
	offerCheckpointDTO.setCarYear(this.carYear);
	offerCheckpointDTO.setCarKm(this.carKm);
	offerCheckpointDTO.setFullVersion(this.fullVersion);
	offerCheckpointDTO.setOfferRange30Days(this.min30DaysOffer + " - " + this.max30DaysOffer);
	offerCheckpointDTO.setOfferRangeInstant(this.minInstantOffer + " - " + this.maxInstantOffer);
	offerCheckpointDTO.setOfferRangeCnc(this.minConsignationOffer + " - " + this.maxConsignationOffer);
	offerCheckpointDTO.setBuyPrice(this.buyPrice);
	offerCheckpointDTO.setMarketPrice(this.marketPrice);
	offerCheckpointDTO.setWishList(this.wishList);
	offerCheckpointDTO.setMax30DaysOffer(this.max30DaysOffer);
	offerCheckpointDTO.setMin30DaysOffer(this.min30DaysOffer);
	offerCheckpointDTO.setMaxInstantOffer(this.maxInstantOffer);
	offerCheckpointDTO.setMinInstantOffer(this.minInstantOffer);
	offerCheckpointDTO.setMaxConsigmentOffer(this.maxConsignationOffer);
	offerCheckpointDTO.setMinConsigmentOffer(this.minConsignationOffer);
	offerCheckpointDTO.setOfferPunished(this.offerPunished);
	offerCheckpointDTO.setIdHourInpectionSlot(this.idHourInpectionSlot);
	offerCheckpointDTO.setHourInpectionSlotText(this.hourInpectionSlotText);
	offerCheckpointDTO.setInspectionLocationDescription(this.inspectionLocationDescription);
	offerCheckpointDTO.setOfferPunished(this.offerPunished);
	offerCheckpointDTO.setStatusId(this.idStatus);
	offerCheckpointDTO.setStatusDetail(this.statusDetail);
	offerCheckpointDTO.setZipCode(this.zipCode);
	offerCheckpointDTO.setCodeNetsuiteItem(this.codeNetsuiteItem);
	offerCheckpointDTO.setOfferType(this.offerType);
	offerCheckpointDTO.setOfferTypeDescription(this.descriptionOfferType);
	offerCheckpointDTO.setDuplicatedOfferControl(this.duplicatedOfferControl);
	offerCheckpointDTO.setSalePriceGa(this.salePrice);
	offerCheckpointDTO.setSalepriceKavak(this.salePriceKavak);
	offerCheckpointDTO.setAssignedEmId(this.assignedEm);
	offerCheckpointDTO.setInspectionLocationId(this.inspectionLocation);
	offerCheckpointDTO.setInspectionCenterDescription(this.inspectionCenterDescription);
	offerCheckpointDTO.setNetsuiteOpportunityId(this.netsuiteOpportunityId);
	offerCheckpointDTO.setNetsuiteOpportunityURL(this.netsuiteOpportunityURL);
	offerCheckpointDTO.setNetsuiteIdUpdateDate(this.netsuiteIdUpdateDate);
	offerCheckpointDTO.setReliabilitySample(this.trustedOffer);
	offerCheckpointDTO.setOfferSource(this.offerSource);
	offerCheckpointDTO.setSentInternalEmail(this.sentInternalEmail);
	offerCheckpointDTO.setNetsuiteIdAttemps(this.netsuiteIdAttemps);
	offerCheckpointDTO.setSegmentType(this.segmentType);
	offerCheckpointDTO.setInternalUserId(this.internalUserId);
	offerCheckpointDTO.setExteriorColorId(this.idExteriorColor);
	offerCheckpointDTO.setAbTestCode(this.abTestCode);
	offerCheckpointDTO.setAbTestResult(this.abTestResult);
	
	if (this.circulationCardUrl != null && !this.circulationCardUrl.isEmpty()) {
	    offerCheckpointDTO.setCirculationCardUrl(Constants.URL_KAVAK + this.circulationCardUrl);
	} else {
	    offerCheckpointDTO.setCirculationCardUrl(null);
	}

	if (this.carInvoiceUrl != null && !this.carInvoiceUrl.isEmpty()) {
	    offerCheckpointDTO.setCarInvoiceUrl(Constants.URL_KAVAK + this.carInvoiceUrl);
	} else {
	    offerCheckpointDTO.setCarInvoiceUrl(null);
	}

	if (this.inspectionDate != null) {
	    offerCheckpointDTO.setInspectionDate(this.inspectionDate);
	}

	if (this.offerDate != null) {
	    offerCheckpointDTO.setRegisterDate(this.offerDate.toString());
	}

	if (this.updateDateOffer != null) {
	    offerCheckpointDTO.setUpdateDateOffer(this.updateDateOffer.toString());
	}

	return offerCheckpointDTO;
    }

}