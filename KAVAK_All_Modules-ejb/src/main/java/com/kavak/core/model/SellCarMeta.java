package com.kavak.core.model;

import com.kavak.core.dto.SellCarMetaDTO;

import javax.persistence.*;

@Entity
@Table(name="sell_car_meta")
public class SellCarMeta{

	private Long id;
	private Long sellCarId;
	private String metaValue;
	private SellCarDetail sellCarDetail;
	
	@Id 
    @Column(name="id")
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="sell_car_id", insertable= false, updatable=false)
	public Long getSellCarId() {
		return sellCarId;
	}
	public void setSellCarId(Long sellCarId) {
		this.sellCarId = sellCarId;
	}

	@Column(name="meta_value")
	public String getMetaValue() {
		return metaValue;
	}
	public void setMetaValue(String metaValue) {
		this.metaValue = metaValue;
	}

	@OneToOne
	@JoinColumn(name="sell_car_id")	
	public SellCarDetail getSellCarDetail() {
		return sellCarDetail;
	}
	public void setSellCarDetail(SellCarDetail sellCarDetail) {
		this.sellCarDetail = sellCarDetail;
	}
	
	@Transient
	public SellCarMetaDTO getDTO(){
		SellCarMetaDTO sellCarMetaDTO = new SellCarMetaDTO();
		sellCarMetaDTO.setId(this.id);
		sellCarMetaDTO.setSellCarId(this.sellCarId);
		sellCarMetaDTO.setMetaValue(this.metaValue);
		return sellCarMetaDTO;
	}
}