package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.CarscoutQuestionAnswerDTO;

@Entity
@Table(name = "v2_carscout_question_answer")
public class CarscoutQuestionAnswer {

    private Long id;
    private Long userId;
    private Long alertId;
    private Long catalogueId;
    private Long questionId;
    private Long optionId;
    private Timestamp updateDate;


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Column(name = "question_id")
    public Long getQuestionId() {
        return questionId;
    }
    
    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }


    @Column(name = "option_id")
    public Long getOptionId() {
        return optionId;
    }

    public void setOptionId(Long optionId) {
        this.optionId = optionId;
    }

    @Column(name = "update_date")
    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }
    
    @Column(name = "alert_id")
    public Long getAlertId() {
        return alertId;
    }

    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    @Column(name = "catalogue_id")
    public Long getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(Long catalogueId) {
        this.catalogueId = catalogueId;
    }

    @Transient
    public CarscoutQuestionAnswerDTO getDTO(){
	CarscoutQuestionAnswerDTO carscoutQuestionAnswerDTO= new CarscoutQuestionAnswerDTO();
	carscoutQuestionAnswerDTO.setId(this.id);
	carscoutQuestionAnswerDTO.setUserId(this.userId);
	carscoutQuestionAnswerDTO.setAlertId(this.alertId);
	carscoutQuestionAnswerDTO.setQuestionId(this.getQuestionId());
	carscoutQuestionAnswerDTO.setOptionId(this.getOptionId());
        return carscoutQuestionAnswerDTO;
    }

}
