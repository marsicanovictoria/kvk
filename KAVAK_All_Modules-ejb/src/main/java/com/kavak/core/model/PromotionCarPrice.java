package com.kavak.core.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="v2_promotion_car_price")
public class PromotionCarPrice {

    private Long id;
    private Long idSellCarDetail;
    private Long idPromotion;
    private Long price;
    private boolean active;
    private Date creationDate;
    private Promotion promotion;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="car_id")
    public Long getIdSellCarDetail() {
        return idSellCarDetail;
    }
    public void setIdSellCarDetail(Long idSellCarDetail) {
        this.idSellCarDetail = idSellCarDetail;
    }

    @Column(name="promotion_id", insertable=false,updatable=false)
    public Long getIdPromotion() {
        return idPromotion;
    }
    public void setIdPromotion(Long idPromotion) {
        this.idPromotion = idPromotion;
    }

    @Column(name="price")
    public Long getPrice() {
        return price;
    }
    public void setPrice(Long price) {
        this.price = price;
    }

    @Column(name="is_active")
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name="creation_date")
    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @ManyToOne
    @JoinColumn(name="promotion_id")
    public Promotion getPromotion() {
        return promotion;
    }
    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }
    
}
