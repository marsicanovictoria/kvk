package com.kavak.core.model;

import com.kavak.core.dto.SaleCarOrderDTO;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="orders")
public class SaleCarOrder {
	
	private Long id; 
	private Long itemId;
	private Long buyerId;
	private String transactionId;
	private String itemName;
	private Timestamp registrationDate;
	private String totalAmt;
	private String paidAmt;
	private String pendingAmt;
	private String buyerName;
	private String paymentMode;
	private String paymentType;
	private String email;
	private String phone;
	private String address;
	private String city;
	private String state;
	private String country;
	private String zipCode;
	private boolean active;
	private boolean complete;
	private boolean canceled;
	private String holdTime;
	private String paymentStatus;
	private String reservationTime;
	private String deliverStatus;
	private Integer daysToReturn;
	private String loanDetails;
	private String pickupType;
	private Timestamp actionDate;
	private Long financialAmount;
	private Integer financialMonths;
	private Integer hookingAmount;
	private SellCarDetail sellCarDetail; // Objeto solo para la relacion oneToMany en sellCarDetail

	@Id 
    @Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="item_id", insertable= false, updatable=false)
	public Long getItemId() {
		return itemId;
	}
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	@Column(name="buyer_id")
	public Long getBuyerId() {
		return buyerId;
	}
	public void setBuyerId(Long buyerId) {
		this.buyerId = buyerId;
	}

	@Column(name="transaction_id")
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Column(name="item_name")
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	@Column(name="date")
	public Timestamp getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Column(name="total_amt")
	public String getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(String totalAmt) {
		this.totalAmt = totalAmt;
	}

	@Column(name="paid_amt")
	public String getPaidAmt() {
		return paidAmt;
	}
	public void setPaidAmt(String paidAmt) {
		this.paidAmt = paidAmt;
	}

	@Column(name="pending_amt")
	public String getPendingAmt() {
		return pendingAmt;
	}
	public void setPendingAmt(String pendingAmt) {
		this.pendingAmt = pendingAmt;
	}

	@Column(name="buyer_name")
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	@Column(name="payment_mode")
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	@Column(name="payment_type")
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="phn_no")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name="address")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	@Column(name="state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	@Column(name="country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name="pincode")
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Column(name="is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(name="is_complete")
	public boolean isComplete() {
		return complete;
	}
	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	@Column(name="is_cancelled")
	public boolean isCanceled() {
		return canceled;
	}
	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}

	@Column(name="hold_time")
	public String getHoldTime() {
		return holdTime;
	}
	public void setHoldTime(String holdTime) {
		this.holdTime = holdTime;
	}

	@Column(name="payment_status")
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@Column(name="reservatoin_time")
	public String getReservationTime() {
		return reservationTime;
	}
	public void setReservationTime(String reservationTime) {
		this.reservationTime = reservationTime;
	}

	@Column(name="deliver_status")
	public String getDeliverStatus() {
		return deliverStatus;
	}
	public void setDeliverStatus(String deliverStatus) {
		this.deliverStatus = deliverStatus;
	}

	@Column(name="days_to_return")
	public Integer getDaysToReturn() {
		return daysToReturn;
	}
	public void setDaysToReturn(Integer daysToReturn) {
		this.daysToReturn = daysToReturn;
	}

	@Column(name="loan_details")
	public String getLoanDetails() {
		return loanDetails;
	}
	public void setLoanDetails(String loanDetails) {
		this.loanDetails = loanDetails;
	}

	@Column(name="pickup_type")
	public String getPickupType() {
		return pickupType;
	}
	public void setPickupType(String pickupType) {
		this.pickupType = pickupType;
	}

	@Column(name="action_date")
	public Timestamp getActionDate() {
		return actionDate;
	}
	public void setActionDate(Timestamp actionDate) {
		this.actionDate = actionDate;
	}

	@Column(name="monto_financiamiento")
	public Long getFinancialAmount() {
		return financialAmount;
	}
	public void setFinancialAmount(Long financialAmount) {
		this.financialAmount = financialAmount;
	}

	@Column(name="meses_financiamiento")
	public Integer getFinancialMonths() {
		return financialMonths;
	}
	public void setFinancialMonths(Integer financialMonths) {
		this.financialMonths = financialMonths;
	}

	@Column(name="monto_enganche")
	public Integer getHookingAmount() {
		return hookingAmount;
	}
	public void setHookingAmount(Integer hookingAmount) {
		this.hookingAmount = hookingAmount;
	}

	@ManyToOne
	@JoinColumn(name="item_id")
	public SellCarDetail getSellCarDetail() {
		return sellCarDetail;
	}
	public void setSellCarDetail(SellCarDetail sellCarDetail) {
		this.sellCarDetail = sellCarDetail;
	}
	
	@Transient
	public SaleCarOrderDTO getDTO(){
		SaleCarOrderDTO saleCarOrderDTO = new SaleCarOrderDTO();
		saleCarOrderDTO.setId(this.id);
		saleCarOrderDTO.setItemId(this.itemId);
		saleCarOrderDTO.setBuyerId(this.buyerId);
		saleCarOrderDTO.setTransactionId(this.transactionId);
		saleCarOrderDTO.setItemName(this.itemName);
		saleCarOrderDTO.setRegistrationDate(this.registrationDate);
		saleCarOrderDTO.setTotalAmt(this.totalAmt);
		saleCarOrderDTO.setPaidAmt(this.paidAmt);
		saleCarOrderDTO.setBuyerName(this.buyerName);
		saleCarOrderDTO.setPaymentMode(this.paymentMode);
		saleCarOrderDTO.setPaymentType(this.paymentType);
		saleCarOrderDTO.setEmail(this.email);
		saleCarOrderDTO.setPhone(this.phone);
		saleCarOrderDTO.setAddress(this.address);
		saleCarOrderDTO.setCity(this.city);
		saleCarOrderDTO.setState(this.state);
		saleCarOrderDTO.setCountry(this.country);
		saleCarOrderDTO.setZipCode(this.zipCode);
		saleCarOrderDTO.setActive(this.active);
		saleCarOrderDTO.setComplete(this.complete);
		saleCarOrderDTO.setCanceled(this.canceled);
		saleCarOrderDTO.setHoldTime(this.holdTime);
		saleCarOrderDTO.setPaymentStatus(this.paymentStatus);
		saleCarOrderDTO.setReservationTime(this.reservationTime);
		saleCarOrderDTO.setDeliverStatus(this.deliverStatus);
		saleCarOrderDTO.setDaysToReturn(this.daysToReturn);
		saleCarOrderDTO.setLoanDetails(this.loanDetails);
		saleCarOrderDTO.setPickupType(this.pickupType);
		saleCarOrderDTO.setActionDate(this.actionDate);
		saleCarOrderDTO.setFinancialAmount(this.financialAmount);
		saleCarOrderDTO.setFinancialMonths(this.financialMonths);
		saleCarOrderDTO.setHookingAmount(this.hookingAmount);
		return saleCarOrderDTO;
	}
}