package com.kavak.core.model;

import com.kavak.core.dto.AppointmentScheduleBlockDTO;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Enrique on 29-Jun-17.
 */
@Entity
@Table(name="v2_appointment_schedule_block")
public class AppointmentScheduleBlock {

    private Long id;
    private String hourFrom;
    private String hourTo;
    private String block;
    private boolean active;
    private Date creationDate;

    @Id
    @Column(name ="id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name ="hour_from")
    public String getHourFrom() {
        return hourFrom;
    }
    public void setHourFrom(String hourFrom) {
        this.hourFrom = hourFrom;
    }
    @Column(name ="hour_to")
    public String getHourTo() {
        return hourTo;
    }
    public void setHourTo(String hourTo) {
        this.hourTo = hourTo;
    }

    @Column(name ="block")
    public String getBlock() {
        return block;
    }
    public void setBlock(String block) {
        this.block = block;
    }

    @Column(name ="is_active")
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name ="creation_date")
    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Transient
    public AppointmentScheduleBlockDTO getDTO(){
        AppointmentScheduleBlockDTO appointmentScheduleBlockDTO = new AppointmentScheduleBlockDTO();
        appointmentScheduleBlockDTO.setId(this.id);
        appointmentScheduleBlockDTO.setBlock(this.block);

        return appointmentScheduleBlockDTO;
    }
}
