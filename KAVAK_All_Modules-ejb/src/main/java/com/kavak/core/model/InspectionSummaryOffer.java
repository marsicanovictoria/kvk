package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "v2_inspection_summary_offer")
public class InspectionSummaryOffer {

    private Long id;
    private Long idInspection;
    private Long idInspectionSummary;
    private String inspectorEmail;
    private Long idOfferType;
    private Double offerAmount;
    private Timestamp creationDate;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Column(name = "inspection_id")
    public Long getIdInspection() {
	return idInspection;
    }

    public void setIdInspection(Long idInspection) {
	this.idInspection = idInspection;
    }

    @Column(name = "inspector_email")
    public String getInspectorEmail() {
	return inspectorEmail;
    }

    public void setInspectorEmail(String inspectorEmail) {
	this.inspectorEmail = inspectorEmail;
    }

    @Column(name = "offer_type_id")
    public Long getIdOfferType() {
	return idOfferType;
    }

    public void setIdOfferType(Long idOfferType) {
	this.idOfferType = idOfferType;
    }

    @Column(name = "offer_amount")
    public Double getOfferAmount() {
	return offerAmount;
    }

    public void setOfferAmount(Double offerAmount) {
	this.offerAmount = offerAmount;
    }

    @Column(name = "creation_date")
    public Timestamp getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
	this.creationDate = creationDate;
    }

    @Column(name = "inspection_summary_id")
    public Long getIdInspectionSummary() {
	return idInspectionSummary;
    }

    public void setIdInspectionSummary(Long idInspectionSummary) {
	this.idInspectionSummary = idInspectionSummary;
    }

    @Transient
    public InspectionSummaryOffer getDTO() {
	InspectionSummaryOffer InspectionSummaryOffer = new InspectionSummaryOffer();
	InspectionSummaryOffer.setIdInspection(this.idInspection);
	InspectionSummaryOffer.setIdInspection(this.idInspectionSummary);
	InspectionSummaryOffer.setInspectorEmail(this.inspectorEmail);
	InspectionSummaryOffer.setIdOfferType(this.idOfferType);
	InspectionSummaryOffer.setOfferAmount(this.offerAmount);
	InspectionSummaryOffer.setCreationDate(this.creationDate);

	return InspectionSummaryOffer;
    }

}
