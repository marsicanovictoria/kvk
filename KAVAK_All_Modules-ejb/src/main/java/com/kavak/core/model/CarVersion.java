package com.kavak.core.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "v2_car_version")
public class CarVersion {

	private Long id;
	private String description;
	private Long makeId;
	private boolean active;
	private CarModel carModel;
	private Timestamp creation_date;
	private List<CarscoutCatalogue> carscoutCatalogue;

	@Id
	@Column(name = "version_id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "make_id")
	public Long getMakeId() {
		return makeId;
	}

	public void setMakeId(Long makeId) {
		this.makeId = makeId;
	}

	@Column(name = "active_ind")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	@ManyToOne
	@JoinColumn(name = "model_id")
	public CarModel getCarModel() {
		return carModel;
	}

	public void setCarModel(CarModel carModel) {
		this.carModel = carModel;
	}

	public void setCreation_date(Timestamp creation_date) {
		this.creation_date = creation_date;
	}

	@Column(name = "creation_date")
	public Timestamp getCreation_date() {
		return creation_date;
	}

	@OneToMany(mappedBy = "carVersion")
	public List<CarscoutCatalogue> getCarscoutCatalogue() {
		return carscoutCatalogue;
	}

	public void setCarscoutCatalogue(List<CarscoutCatalogue> carscoutCatalogue) {
		this.carscoutCatalogue = carscoutCatalogue;
	}

}
