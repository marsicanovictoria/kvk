package com.kavak.core.model;

import com.kavak.core.dto.InspectionCarFeatureDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name= "v2_inspection_car_feature")
public class InspectionCarFeature {

	private Long id;
	private Long idInspection;
	private String inspectorEmail;
	private Long idFeature;
	private String featureLabel;
	private Date creationDate;
	private InspectionCarFeatureItem inspectionCarFeatureItem;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name = "inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}
	@Column(name = "inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	@Column(name = "feature_id", insertable= false, updatable=false)
	public Long getIdFeature() {
		return idFeature;
	}
	public void setIdFeature(Long idFeature) {
		this.idFeature = idFeature;
	}
	@Column(name = "feature_label")
	public String getFeatureLabel() {
		return featureLabel;
	}
	public void setFeatureLabel(String featureLabel) {
		this.featureLabel = featureLabel;
	}
	@Column(name = "creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@OneToOne
	@JoinColumn(name="feature_id")	
	public InspectionCarFeatureItem getInspectionCarFeatureItem() {
		return inspectionCarFeatureItem;
	}
	public void setInspectionCarFeatureItem(InspectionCarFeatureItem inspectionCarFeatureItem) {
		this.inspectionCarFeatureItem = inspectionCarFeatureItem;
	}

	@Transient
	public InspectionCarFeatureDTO getDTO(){
        InspectionCarFeatureDTO inspectionCarFeatureDTO = new InspectionCarFeatureDTO();
        inspectionCarFeatureDTO.setIdFeature(this.idFeature);
        inspectionCarFeatureDTO.setFeatureLabel(this.featureLabel);
        return inspectionCarFeatureDTO;
    }
}
