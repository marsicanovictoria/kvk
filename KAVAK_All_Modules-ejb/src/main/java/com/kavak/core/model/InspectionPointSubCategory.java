package com.kavak.core.model;

import com.kavak.core.dto.InspectionPointDTO;
import com.kavak.core.dto.InspectionPointSubCategoryDTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="v2_inspection_point_subcategory")
public class InspectionPointSubCategory{
	
	private Long id;
    private Long categoryId;
    private String name;
	private boolean active;
	private InspectionPointCategory inspectionPointCategory;
	private List<InspectionPoint> listInspectionPoint;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

    @Column(name="category_id", insertable = false, updatable = false)
    public Long getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

    @ManyToOne
    @JoinColumn(name="category_id")
    public InspectionPointCategory getInspectionPointCategory() {
        return inspectionPointCategory;
    }
    public void setInspectionPointCategory(InspectionPointCategory inspectionPointCategory) {
        this.inspectionPointCategory = inspectionPointCategory;
    }

	@OneToMany(fetch=FetchType.LAZY, mappedBy="inspectionPointSubCategory")
	public List<InspectionPoint> getListInspectionPoint() {
		return listInspectionPoint;
	}
	public void setListInspectionPoint(List<InspectionPoint> listInspectionPoint) {
		this.listInspectionPoint = listInspectionPoint;
	}
	
	@Transient
	public InspectionPointSubCategoryDTO getDTO(){
		InspectionPointSubCategoryDTO inspectionPointSubCategoryDTO = new InspectionPointSubCategoryDTO();
		List<InspectionPointDTO> listInspectionPointDTO = new ArrayList<>();
		inspectionPointSubCategoryDTO.setId(this.getId());
		inspectionPointSubCategoryDTO.setName(this.getName());
		inspectionPointSubCategoryDTO.setActive(this.isActive());
		for(InspectionPoint inspectionPointActual: this.getListInspectionPoint()){
			listInspectionPointDTO.add(inspectionPointActual.getDTO());
		}
		inspectionPointSubCategoryDTO.setListInspectionPointDTO(listInspectionPointDTO);
		return inspectionPointSubCategoryDTO;
	}
}
