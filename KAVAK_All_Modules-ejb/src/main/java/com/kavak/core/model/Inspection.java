package com.kavak.core.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;



@Entity
@Table(name= "v2_inspection")
public class Inspection {

    private Long id;
    private Long idOffertCheckPoint;
    private String inspectorEmail;
    private Date inspectionDate;
    private Long idHourBlock;
    private String inspectionAddress;
    private String customerName;
    private String customerPhone;
    private String customerEmail;
    private String customerAddress;
    private String sku;
    private String carMake;
    private String carModel;
    private Long carYear;
    private Long carKm;
    private String carTrim;
    private String carFullVersion;
    private Long marketPrice;
    private Long buyPriceGa;
    private Long salePriceGa;
    private Long salePriceKavak;
    private String nameExperimentManager;
    private String nameTowerContact;
    private String comments;
    private Timestamp creationDate;
    private String zipCode;
    private boolean canceled;
    private String canceledReason;
    private boolean rescheduled;
    private String rescheduledReason;
    private Long sendNetsuite;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="offer_checkpoint_id")
    public Long getIdOffertCheckPoint() {
        return idOffertCheckPoint;
    }
    public void setIdOffertCheckPoint(Long idOffertCheckPoint) {
        this.idOffertCheckPoint = idOffertCheckPoint;
    }

    @Column(name="inspector_email")
    public String getInspectorEmail() {
        return inspectorEmail;
    }
    public void setInspectorEmail(String inspectorEmail) {
        this.inspectorEmail = inspectorEmail;
    }

    @Column(name="date")
    public Date getInspectionDate() {
        return inspectionDate;
    }
    public void setInspectionDate(Date inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    @Column(name="block_hour_id")
    public Long getIdHourBlock() {
        return idHourBlock;
    }
    public void setIdHourBlock(Long idHourBlock) {
        this.idHourBlock = idHourBlock;
    }

    @Column(name="address")
    public String getInspectionAddress() {
        return inspectionAddress;
    }
    public void setInspectionAddress(String inspectionAddress) {
        this.inspectionAddress = inspectionAddress;
    }

    @Column(name="customer_name")
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Column(name="customer_phone")
    public String getCustomerPhone() {
        return customerPhone;
    }
    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    @Column(name="customer_email")
    public String getCustomerEmail() {
        return customerEmail;
    }
    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    @Column(name="customer_address")
    public String getCustomerAddress() {
        return customerAddress;
    }
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    @Column(name="sku")
    public String getSku() {
        return sku;
    }
    public void setSku(String sku) {
        this.sku = sku;
    }

    @Column(name="car_make")
    public String getCarMake() {
        return carMake;
    }
    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }

    @Column(name="car_model")
    public String getCarModel() {
        return carModel;
    }
    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    @Column(name="car_trim")
    public String getCarTrim() {
        return carTrim;
    }
    public void setCarTrim(String carTrim) {
        this.carTrim = carTrim;
    }

    @Column(name="car_year")
    public Long getCarYear() {
        return carYear;
    }
    public void setCarYear(Long carYear) {
        this.carYear = carYear;
    }

    @Column(name="car_km")
    public Long getCarKm() {
        return carKm;
    }
    public void setCarKm(Long carKm) {
        this.carKm = carKm;
    }

    @Column(name="car_full_version")
    public String getCarFullVersion() {
        return carFullVersion;
    }
    public void setCarFullVersion(String carFullVersion) {
        this.carFullVersion = carFullVersion;
    }

    @Column(name="market_price")
    public Long getMarketPrice() {
        return marketPrice;
    }
    public void setMarketPrice(Long marketPrice) {
        this.marketPrice = marketPrice;
    }

    @Column(name="buy_price_ga")
    public Long getBuyPriceGa() {
        return buyPriceGa;
    }
    public void setBuyPriceGa(Long buyPriceGa) {
        this.buyPriceGa = buyPriceGa;
    }

    @Column(name="sale_price_ga")
    public Long getSalePriceGa() {
        return salePriceGa;
    }
    public void setSalePriceGa(Long salePriceGa) {
        this.salePriceGa = salePriceGa;
    }

    @Column(name="sale_price_kavak")
    public Long getSalePriceKavak() {
        return salePriceKavak;
    }
    public void setSalePriceKavak(Long salePriceKavak) {
        this.salePriceKavak = salePriceKavak;
    }

    @Column(name="em_contact")
    public String getNameExperimentManager() {
        return nameExperimentManager;
    }
    public void setNameExperimentManager(String nameExperimentManager) {
        this.nameExperimentManager = nameExperimentManager;
    }

    @Column(name="tower_contact")
    public String getNameTowerContact() {
        return nameTowerContact;
    }
    public void setNameTowerContact(String nameTowerContact) {
        this.nameTowerContact = nameTowerContact;
    }

    @Column(name="comments")
    public String getComments() {
        return comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name="creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name="zip_code")
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Column(name="is_canceled")
    public boolean isCanceled() {
        return canceled;
    }
    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    @Column(name="is_canceled_reason")
    public String getCanceledReason() {
        return canceledReason;
    }
    public void setCanceledReason(String canceledReason) {
        this.canceledReason = canceledReason;
    }

    @Column(name="is_rescheduled")
    public boolean isRescheduled() {
        return rescheduled;
    }
    public void setRescheduled(boolean rescheduled) {
        this.rescheduled = rescheduled;
    }

    @Column(name="is_rescheduled_reason")
    public String getRescheduledReason() {
        return rescheduledReason;
    }
    public void setRescheduledReason(String rescheduledReason) {
        this.rescheduledReason = rescheduledReason;
    }
    
    @Column(name="send_netsuite")
	public Long getSendNetsuite() {
		return sendNetsuite;
	}
	public void setSendNetsuite(Long sendNetsuite) {
		this.sendNetsuite = sendNetsuite;
	}
}
