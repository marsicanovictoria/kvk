package com.kavak.core.model;

import javax.persistence.*;

/**
 * Created by Enrique on 05-Jun-17.
 */

@Entity
@Table(name = "puntos_historia_vehicular")
public class ApoloInspectionPointsHistory {

    private Long id;
    private String name;
    private boolean active;
    private boolean deselect;
    private Long idUser;
    //private ApoloInspectionPointsHistoryCar apoloInspectionPointsHistoryCar;

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "nombre")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "activo")
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name = "desmarcar")
    public boolean isDeselect() {
        return deselect;
    }
    public void setDeselect(boolean deselect) {
        this.deselect = deselect;
    }

    @Column(name = "usuario_id")
    public Long getIdUser() {
        return idUser;
    }
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

//    @OneToOne(mappedBy="apoloInspectionPointsHistory")
//    public ApoloInspectionPointsHistoryCar getApoloInspectionPointsHistoryCar() {
//        return apoloInspectionPointsHistoryCar;
//    }
//
//    public void setApoloInspectionPointsHistoryCar(ApoloInspectionPointsHistoryCar apoloInspectionPointsHistoryCar) {
//        this.apoloInspectionPointsHistoryCar = apoloInspectionPointsHistoryCar;
//    }
}
