package com.kavak.core.model;

import javax.persistence.*;

@Entity
@Table(name="v2_inspection_offer")
public class InspectionOffer {

	private Long id;
	private Long idInspection;
	private Long statusOffer;
	private Long offerType;
	private String minValue;
	private String maxValue;
	private boolean selectedOffer;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name = "inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}
	@Column(name = "offer_status")
	public Long getStatusOffer() {
		return statusOffer;
	}
	public void setStatusOffer(Long statusOffer) {
		this.statusOffer = statusOffer;
	}
	@Column(name = "offer_type")
	public Long getOfferType() {
		return offerType;
	}
	public void setOfferType(Long offerType) {
		this.offerType = offerType;
	}
	@Column(name = "min_value")
	public String getMinValue() {
		return minValue;
	}
	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}
	@Column(name = "max_value")
	public String getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}
	@Column(name = "selected_offer")
	public boolean getSelectedOffer() {
		return selectedOffer;
	}
	public void setSelectedOffer(boolean selectedOffer) {
		this.selectedOffer = selectedOffer;
	}
}
