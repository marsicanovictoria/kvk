package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "v2_customer_notification_question")
public class CustomerNotificationQuestion {

	private Long id;
	private Long notificationType;
	private String question;
	private Timestamp creationDate;
	private Timestamp updateDate;
	//private MetaValue metaValue;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "notification_type")
	public Long getNotificationType() {
	    return notificationType;
	}

	public void setNotificationType(Long notificationType) {
	    this.notificationType = notificationType;
	}
	
	@Column(name = "question")
	public String getQuestion() {
	    return question;
	}

	public void setQuestion(String question) {
	    this.question = question;
	}

	@Column(name = "creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "update_date")
	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

}
