package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "v2_carscout_question_option")
public class CarscoutQuestionOptions {

	private Long id;
	private Long questionId;
	private String option;
	private boolean isActive;
	private Timestamp creationDate;
	private Timestamp updateDate;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "question_id")
	public Long getQuestionId() {
	    return questionId;
	}

	public void setQuestionId(Long questionId) {
	    this.questionId = questionId;
	}
	
	@Column(name = "option")
	public String getOption() {
	    return option;
	}

	public void setOption(String option) {
	    this.option = option;
	}

	@Column(name = "creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "update_date")
	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name = "is_active")
	public boolean isActive() {
	    return isActive;
	}

	public void setActive(boolean isActive) {
	    this.isActive = isActive;
	}





	

}
