package com.kavak.core.model;

import javax.persistence.*;

@Entity
@Table(name = "sell_car_buyer")
public class SellCarBuyer {

    private Long id;
    private Long detailId;
    private Long sellerId;
    private Long buyerId;

    @Id
    @Column(name ="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name ="detail_id")
    public Long getDetailId() {
        return detailId;
    }
    public void setDetailId(Long detailId) {
        this.detailId = detailId;
    }

    @Column(name ="seller_id")
    public Long getSellerId() {
        return sellerId;
    }
    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Column(name ="buyer_id")
    public Long getBuyerId() {
        return buyerId;
    }
    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

}
