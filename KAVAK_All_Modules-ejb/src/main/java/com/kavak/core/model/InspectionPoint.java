package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.InspectionPointDTO;

@Entity
@Table(name = "v2_inspection_point_definition")
public class InspectionPoint {

    private Long id;
    private Long categoryId;
    private Long subCategoryId;
    private String name;
    private boolean feature;
    private Long idFeature;
    private boolean reprovesInspection;
    private boolean active;
    private String availableRepairs;
    private InspectionPointCategory inspectionPointCategory;
    private InspectionPointSubCategory inspectionPointSubCategory;
    private boolean photoRequired;
    private boolean naButtonEnabled;
    private boolean standarButtonEnabled;
    private boolean repairButtonEnabled;
    private boolean reprovedButtonEnabled;
    private boolean showAdditionalInfo;
    private String showAdditionalInfoKey;
    private boolean captureAdditionalInfo;
    private String captureAdditionalInfoKey;
    private boolean generateDiscount;
    private boolean generateSummaryComment;
    private boolean generateEmailAlert;
    private Long discountTypeId;
    private Long inspectionPointTypeId;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Column(name = "category_id", insertable = false, updatable = false)
    public Long getCategoryId() {
	return categoryId;
    }

    public void setCategoryId(Long categoryId) {
	this.categoryId = categoryId;
    }

    @Column(name = "subcategory_id", insertable = false, updatable = false)
    public Long getSubCategoryId() {
	return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
	this.subCategoryId = subCategoryId;
    }

    @Column(name = "name")
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Column(name = "is_feature")
    public boolean isFeature() {
	return feature;
    }

    public void setFeature(boolean feature) {
	this.feature = feature;
    }

    @Column(name = "feature_id")
    public Long getIdFeature() {
	return idFeature;
    }

    public void setIdFeature(Long idFeature) {
	this.idFeature = idFeature;
    }

    @Column(name = "reproves_inspection")
    public boolean isReprovesInspection() {
	return reprovesInspection;
    }

    public void setReprovesInspection(boolean reprovesInspection) {
	this.reprovesInspection = reprovesInspection;
    }

    @Column(name = "is_active")
    public boolean isActive() {
	return active;
    }

    public void setActive(boolean active) {
	this.active = active;
    }

    @ManyToOne
    @JoinColumn(name = "category_id")
    public InspectionPointCategory getInspectionPointCategory() {
	return inspectionPointCategory;
    }

    public void setInspectionPointCategory(InspectionPointCategory inspectionPointCategory) {
	this.inspectionPointCategory = inspectionPointCategory;
    }

    @ManyToOne
    @JoinColumn(name = "subcategory_id")
    public InspectionPointSubCategory getInspectionPointSubCategory() {
	return inspectionPointSubCategory;
    }

    public void setInspectionPointSubCategory(InspectionPointSubCategory inspectionPointSubCategory) {
	this.inspectionPointSubCategory = inspectionPointSubCategory;
    }

    @Column(name = "available_repairs")
    public String getAvailableRepairs() {
	return availableRepairs;
    }

    public void setAvailableRepairs(String availableRepairs) {
	this.availableRepairs = availableRepairs;
    }

    @Column(name = "is_photo_required")
    public boolean isPhotoRequired() {
	return photoRequired;
    }

    public void setPhotoRequired(boolean photoRequired) {
	this.photoRequired = photoRequired;
    }

    @Column(name = "is_na_button_enabled")
    public boolean isNaButtonEnabled() {
	return naButtonEnabled;
    }

    public void setNaButtonEnabled(boolean naButtonEnabled) {
	this.naButtonEnabled = naButtonEnabled;
    }

    @Column(name = "is_standard_button_enabled")
    public boolean isStandarButtonEnabled() {
	return standarButtonEnabled;
    }

    public void setStandarButtonEnabled(boolean standarButtonEnabled) {
	this.standarButtonEnabled = standarButtonEnabled;
    }

    @Column(name = "is_repair_button_enabled")
    public boolean isRepairButtonEnabled() {
	return repairButtonEnabled;
    }

    public void setRepairButtonEnabled(boolean repairButtonEnabled) {
	this.repairButtonEnabled = repairButtonEnabled;
    }

    @Column(name = "is_reproved_button_enabled")
    public boolean isReprovedButtonEnabled() {
	return reprovedButtonEnabled;
    }

    public void setReprovedButtonEnabled(boolean reprovedButtonEnabled) {
	this.reprovedButtonEnabled = reprovedButtonEnabled;
    }

    @Column(name = "is_show_additional_info")
    public boolean isShowAdditionalInfo() {
	return showAdditionalInfo;
    }

    public void setShowAdditionalInfo(boolean showAdditionalInfo) {
	this.showAdditionalInfo = showAdditionalInfo;
    }

    @Column(name = "show_additional_info_key")
    public String getShowAdditionalInfoKey() {
	return showAdditionalInfoKey;
    }

    public void setShowAdditionalInfoKey(String showAdditionalInfoKey) {
	this.showAdditionalInfoKey = showAdditionalInfoKey;
    }

    @Column(name = "is_capture_additional_info")
    public boolean isCaptureAdditionalInfo() {
	return captureAdditionalInfo;
    }

    public void setCaptureAdditionalInfo(boolean captureAdditionalInfo) {
	this.captureAdditionalInfo = captureAdditionalInfo;
    }

    @Column(name = "capture_additional_info_key")
    public String getCaptureAdditionalInfoKey() {
	return captureAdditionalInfoKey;
    }

    public void setCaptureAdditionalInfoKey(String captureAdditionalInfoKey) {
	this.captureAdditionalInfoKey = captureAdditionalInfoKey;
    }

    @Column(name = "is_generates_discount")
    public boolean isGenerateDiscount() {
	return generateDiscount;
    }

    public void setGenerateDiscount(boolean generateDiscount) {
	this.generateDiscount = generateDiscount;
    }

    @Column(name = "is_generates_summary_comment")
    public boolean isGenerateSummaryComment() {
	return generateSummaryComment;
    }

    public void setGenerateSummaryComment(boolean generateSummaryComment) {
	this.generateSummaryComment = generateSummaryComment;
    }

    @Column(name = "is_generates_email_alert")
    public boolean isGenerateEmailAlert() {
	return generateEmailAlert;
    }

    public void setGenerateEmailAlert(boolean generateEmailAlert) {
	this.generateEmailAlert = generateEmailAlert;
    }

    @Column(name = "discount_type_id")
    public Long getDiscountTypeId() {
	return discountTypeId;
    }

    public void setDiscountTypeId(Long discountTypeId) {
	this.discountTypeId = discountTypeId;
    }

    @Column(name = "inspection_point_type_id")
    public Long getInspectionPointTypeId() {
	return inspectionPointTypeId;
    }

    public void setInspectionPointTypeId(Long inspectionPointTypeId) {
	this.inspectionPointTypeId = inspectionPointTypeId;
    }

    @Transient
    public InspectionPointDTO getDTO() {
	InspectionPointDTO inspectionPointDTO = new InspectionPointDTO();
	inspectionPointDTO.setId(this.id);
	inspectionPointDTO.setName(this.name);
	inspectionPointDTO.setIdFeature(this.idFeature);
	inspectionPointDTO.setFeature(this.feature);
	inspectionPointDTO.setActive(this.active);
	inspectionPointDTO.setReprovesInspection(this.reprovesInspection);
	inspectionPointDTO.setAvailableRepairs(this.availableRepairs);
	inspectionPointDTO.setPhotoRequired(this.photoRequired);
	inspectionPointDTO.setNaButtonEnabled(this.naButtonEnabled);
	inspectionPointDTO.setStandartButtonEnabled(this.standarButtonEnabled);
	inspectionPointDTO.setRepairButtonEnabled(this.repairButtonEnabled);
	inspectionPointDTO.setReprovedButtonEnabled(this.reprovedButtonEnabled);
	inspectionPointDTO.setShowAdditionalInfo(this.showAdditionalInfo);
	inspectionPointDTO.setShowAdditionalInfoKey(this.showAdditionalInfoKey);
	inspectionPointDTO.setCaptureAdditionalInfo(this.captureAdditionalInfo);
	inspectionPointDTO.setCaptureAdditionalInfoKey(this.captureAdditionalInfoKey);
	inspectionPointDTO.setGenerateDiscount(this.generateDiscount);
	inspectionPointDTO.setGenerateSummaryComment(this.generateSummaryComment);
	inspectionPointDTO.setGenerateEmailAlert(this.generateEmailAlert);
	inspectionPointDTO.setDiscountTypeId(this.discountTypeId);
	return inspectionPointDTO;
    }
}
