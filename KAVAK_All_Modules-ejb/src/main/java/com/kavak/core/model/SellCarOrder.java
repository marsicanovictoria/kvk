package com.kavak.core.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name ="sell_car_order")
public class SellCarOrder {

    private Long id;
    private Long detailId;
    private Long sellerId;
    private Long buyerId;
    private Timestamp dealDate;
    private Timestamp pickupDate;
    private String price;
    private Long sellCarWarrantyId;
    private Long sellCarInsuranceId;
    private boolean complete;
    private boolean dealPen;
    private boolean closed;

    @Id
    @Column(name ="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name ="detail_id")
    public Long getDetailId() {
        return detailId;
    }
    public void setDetailId(Long detailId) {
        this.detailId = detailId;
    }

    @Column(name ="seller_id")
    public Long getSellerId() {
        return sellerId;
    }
    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Column(name ="buyer_id")
    public Long getBuyerId() {
        return buyerId;
    }
    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    @Column(name ="deal_date")
    public Timestamp getDealDate() {
        return dealDate;
    }
    public void setDealDate(Timestamp dealDate) {
        this.dealDate = dealDate;
    }

    @Column(name ="pickup_date")
    public Timestamp getPickupDate() {
        return pickupDate;
    }
    public void setPickupDate(Timestamp pickupDate) {
        this.pickupDate = pickupDate;
    }

    @Column(name ="price")
    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    @Column(name ="sell_car_warranty_id")
    public Long getSellCarWarrantyId() {
        return sellCarWarrantyId;
    }
    public void setSellCarWarrantyId(Long sellCarWarrantyId) {
        this.sellCarWarrantyId = sellCarWarrantyId;
    }

    @Column(name ="sell_car_insurance_id")
    public Long getSellCarInsuranceId() {
        return sellCarInsuranceId;
    }
    public void setSellCarInsuranceId(Long sellCarInsuranceId) {
        this.sellCarInsuranceId = sellCarInsuranceId;
    }

    @Column(name ="is_complete")
    public boolean isComplete() {
        return complete;
    }
    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    @Column(name ="is_deal_pen")
    public boolean isDealPen() {
        return dealPen;
    }
    public void setDealPen(boolean dealPen) {
        this.dealPen = dealPen;
    }

    @Column(name ="is_closed")
    public boolean isClosed() {
        return closed;
    }
    public void setClosed(boolean closed) {
        this.closed = closed;
    }

}
