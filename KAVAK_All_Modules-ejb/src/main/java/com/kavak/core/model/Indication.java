package com.kavak.core.model;

import com.kavak.core.dto.IndicationDTO;

import javax.persistence.*;

@Entity
@Table(name = "v2_inspection_indications")
public class Indication {

	private Long id;
	private String numberIndex;
	private String indicationText;
	private boolean active;
	
	@Id
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name = "number_index")
	public String getNumberIndex() {
		return numberIndex;
	}
	public void setNumberIndex(String numberIndex) {
		this.numberIndex = numberIndex;
	}
	@Column(name = "indication_text")
	public String getIndicationText() {
		return indicationText;
	}
	public void setIndicationText(String indicationText) {
		this.indicationText = indicationText;
	}
	@Column(name = "is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@Transient
	public IndicationDTO getDTO(){
		IndicationDTO indicationDTO = new IndicationDTO();
		indicationDTO.setNumberIndex(this.numberIndex);
		indicationDTO.setIndicationText(this.indicationText);
		return indicationDTO;
	}
}

