package com.kavak.core.model;

import javax.persistence.*;

@Entity
@Table(name = "sell_car_dimples")
public class SellCarDimple {

    private Long id;
    private Long idSellCarDetail;
    private String coordanateX;
    private String coordanateY;
    private String dimpleName;
    private String dimpleImageUrl;
    private String dimpleType;
    private String moment;
    private SellCarDetail sellCarDetail; // Campo solo para la relacion ManyTone con @SellCarDetail

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "sell_car_id", insertable = false, updatable = false)
    public Long getIdSellCarDetail() {
        return idSellCarDetail;
    }

    public void setIdSellCarDetail(Long idSellCarDetail) {
        this.idSellCarDetail = idSellCarDetail;
    }

    @Column(name = "x_co")
    public String getCoordanateX() {
        return coordanateX;
    }

    public void setCoordanateX(String coordanateX) {
        this.coordanateX = coordanateX;
    }

    @Column(name = "y_co")
    public String getCoordanateY() {
        return coordanateY;
    }

    public void setCoordanateY(String coordanateY) {
        this.coordanateY = coordanateY;
    }

    @Column(name = "msg")
    public String getDimpleName() {
        return dimpleName;
    }

    public void setDimpleName(String dimpleName) {
        this.dimpleName = dimpleName;
    }

    @Column(name = "dimple_img")
    public String getDimpleImageUrl() {
        return dimpleImageUrl;
    }

    public void setDimpleImageUrl(String dimpleImageUrl) {
        this.dimpleImageUrl = dimpleImageUrl;
    }

    @Column(name = "dimple_type")
    public String getDimpleType() {
        return dimpleType;
    }

    public void setDimpleType(String dimpleType) {
        this.dimpleType = dimpleType;
    }

    @Column(name = "moment")
    public String getMoment() {
        return moment;
    }

    public void setMoment(String moment) {
        this.moment = moment;
    }

    @ManyToOne
    @JoinColumn(name = "sell_car_id")
    public SellCarDetail getSellCarDetail() {
        return sellCarDetail;
    }

    public void setSellCarDetail(SellCarDetail sellCarDetail) {
        this.sellCarDetail = sellCarDetail;
    }
}