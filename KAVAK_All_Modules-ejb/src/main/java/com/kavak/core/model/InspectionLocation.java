package com.kavak.core.model;

import com.kavak.core.dto.InspectionLocationDTO;

import javax.persistence.*;

@Entity
@Table(name="v2_inspection_location")
public class InspectionLocation {
	
	private Long id;
	private String name;
	private String address;
	private String country;
	private String state;
	private String city;
	private String colony;
	private String zip;
	private Double latitude;
	private Double longitude;
	private boolean active;
	private String shortUrlMap;
	private String shortUrlVideo;
	private String urlMap;
	private Long inspectionsPerDay;
	private Long inspectionsPerBlock;
	private String alias;
	
	@Id
	@Column(name= "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name= "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name= "country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Column(name= "state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column(name= "city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
    @Column(name= "colony")
	public String getColony() {
		return colony;
	}
	public void setColony(String colony) {
		this.colony = colony;
	}
	@Column(name= "postal_code")
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}

	@Column(name= "latitude")
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Column(name= "longitude")
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Column(name= "address")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name= "is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(name= "short_url_map")
	public String getShortUrlMap() {
	    return shortUrlMap;
	}
	public void setShortUrlMap(String shortUrlMap) {
	    this.shortUrlMap = shortUrlMap;
	}
	
	@Column(name= "short_url_video")
	public String getShortUrlVideo() {
	    return shortUrlVideo;
	}
	public void setShortUrlVideo(String shortUrlVideo) {
	    this.shortUrlVideo = shortUrlVideo;
	}
	
	@Column(name= "url_map")
	public String getUrlMap() {
	    return urlMap;
	}
	public void setUrlMap(String urlMap) {
	    this.urlMap = urlMap;
	}
	
	@Column(name= "inspections_per_day")
	public Long getInspectionsPerDay() {
	    return inspectionsPerDay;
	}
	public void setInspectionsPerDay(Long inspectionsPerDay) {
	    this.inspectionsPerDay = inspectionsPerDay;
	}
	
	@Column(name= "inspections_per_block")
	public Long getInspectionsPerBlock() {
	    return inspectionsPerBlock;
	}
	public void setInspectionsPerBlock(Long inspectionsPerBlock) {
	    this.inspectionsPerBlock = inspectionsPerBlock;
	}
	
	@Column(name= "alias")
	public String getAlias() {
	    return alias;
	}
	public void setAlias(String alias) {
	    this.alias = alias;
	}
	
	@Transient
	public InspectionLocationDTO getDTO(){
		InspectionLocationDTO inspectioLocationDTO = new InspectionLocationDTO();
		inspectioLocationDTO.setId(this.id);
		inspectioLocationDTO.setName(this.name);
		inspectioLocationDTO.setCountry(this.country);
		inspectioLocationDTO.setState(this.state);
		inspectioLocationDTO.setCity(this.city);
		inspectioLocationDTO.setColony(this.colony);
		inspectioLocationDTO.setZip(this.zip);
		inspectioLocationDTO.setActive(this.active);
		inspectioLocationDTO.setAddress(this.address);
		inspectioLocationDTO.setLongitude(this.longitude);
		inspectioLocationDTO.setLatitude(this.latitude);
        inspectioLocationDTO.setFullAddress(this.address + "." + this.colony + "." + this.zip + "." +
                this.city + "." + this.state + "." + this.country);
		return inspectioLocationDTO;
	} 
}