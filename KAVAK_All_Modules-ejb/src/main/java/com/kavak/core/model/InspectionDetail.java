package com.kavak.core.model;

import javax.persistence.*;

@Entity
@Table(name= "inspection_detail")
public class InspectionDetail {
	
	private Long id;
	private String jsonInspectionDetail;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="json_inspection_detail")
	public String getJsonInspectionDetail() {
		return jsonInspectionDetail;
	}
	public void setJsonInspectionDetail(String jsonInspectionDetail) {
		this.jsonInspectionDetail = jsonInspectionDetail;
	}
}