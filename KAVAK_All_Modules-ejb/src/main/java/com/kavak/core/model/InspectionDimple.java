package com.kavak.core.model;

import com.kavak.core.dto.InspectionDimpleDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "v2_inspection_dimple")
public class InspectionDimple {

    private Long id;
    private Long idInspection;
    private Long idInspectionPointCar;
    private Long idDimpleType;
    private String coordinateX;
    private String coordinateY;
    private Long originalHeight;
    private Long originalWidth;
    private String dimpleImage;
    private String dimpleImageLocation;
    private Date creationDate;
    private String inspectorEmail;
    private int dimpleLocation;
    private String dimpleName;
    private InspectionPointCar inspectionPointCar; // Campo solo para asociar InspectionPointCar No se mapea en DTO

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "inspection_id")
    public Long getIdInspection() {
        return idInspection;
    }
    public void setIdInspection(Long idInspection) {
        this.idInspection = idInspection;
    }

    @Column(name = "inspection_point_car_id", insertable = false ,  updatable = false)
    public Long getIdInspectionPointCar() {
        return idInspectionPointCar;
    }
    public void setIdInspectionPointCar(Long idInspectionPointCar) {
        this.idInspectionPointCar = idInspectionPointCar;
    }

    @Column(name = "dimple_type_id")
    public Long getIdDimpleType() {
        return idDimpleType;
    }
    public void setIdDimpleType(Long idDimpleType) {
        this.idDimpleType = idDimpleType;
    }

    @Column(name = "x_coordinate")
    public String getCoordinateX() {
        return coordinateX;
    }
    public void setCoordinateX(String coordinateX) {
        this.coordinateX = coordinateX;
    }

    @Column(name = "y_coordinate")
    public String getCoordinateY() {
        return coordinateY;
    }
    public void setCoordinateY(String coordinateY) {
        this.coordinateY = coordinateY;
    }

    @Column(name = "original_height")
    public Long getOriginalHeight() {
        return originalHeight;
    }
    public void setOriginalHeight(Long originalHeight) {
        this.originalHeight = originalHeight;
    }

    @Column(name = "original_width")
    public Long getOriginalWidth() {
        return originalWidth;
    }
    public void setOriginalWidth(Long originalWidth) {
        this.originalWidth = originalWidth;
    }

    @Column(name = "dimple_image")
    public String getDimpleImage() {
        return dimpleImage;
    }
    public void setDimpleImage(String dimpleImage) {
        this.dimpleImage = dimpleImage;
    }

    @Column(name = "dimple_location_image")
    public String getDimpleImageLocation() {
        return dimpleImageLocation;
    }
    public void setDimpleImageLocation(String dimpleImageLocation) {
        this.dimpleImageLocation = dimpleImageLocation;
    }

    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "inspector_email")
    public String getInspectorEmail() {
        return inspectorEmail;
    }
    public void setInspectorEmail(String inspectorEmail) {
        this.inspectorEmail = inspectorEmail;
    }

    @Column(name = "dimple_location")
    public int getDimpleLocation() {
        return dimpleLocation;
    }
    public void setDimpleLocation(int dimpleLocation) {
        this.dimpleLocation = dimpleLocation;
    }

    @Column(name = "dimple_name")
    public String getDimpleName() {
        return dimpleName;
    }
    public void setDimpleName(String dimpleName) {
        this.dimpleName = dimpleName;
    }

    @ManyToOne
    @JoinColumn(name="inspection_point_car_id")
    public InspectionPointCar getInspectionPointCar() {
        return inspectionPointCar;
    }
    public void setInspectionPointCar(InspectionPointCar inspectionPointCar) {
        this.inspectionPointCar = inspectionPointCar;
    }

    @Transient
    public InspectionDimpleDTO getDTO(){
        InspectionDimpleDTO inspectionDimpleDTO = new InspectionDimpleDTO();
        inspectionDimpleDTO.setIdDimple(this.id);
        inspectionDimpleDTO.setInspectionId(this.idInspection);
        inspectionDimpleDTO.setInspectorEmail(this.inspectorEmail);
        inspectionDimpleDTO.setIdInspectionPointCar(this.idInspectionPointCar);
        inspectionDimpleDTO.setDimpleLocation(this.dimpleLocation);
        inspectionDimpleDTO.setIdDimpleType(this.idDimpleType);
        inspectionDimpleDTO.setDimpleName(this.dimpleName);
        inspectionDimpleDTO.setCoordinateX(this.coordinateX);
        inspectionDimpleDTO.setCoordinateY(this.coordinateY);
        inspectionDimpleDTO.setOriginalHeight(this.originalHeight);
        inspectionDimpleDTO.setOriginalWidth(this.originalWidth);
        inspectionDimpleDTO.setDimpleImage(this.dimpleImage);
        inspectionDimpleDTO.setDimpleImageLocation(this.dimpleImageLocation);
        return inspectionDimpleDTO;
    }
}
