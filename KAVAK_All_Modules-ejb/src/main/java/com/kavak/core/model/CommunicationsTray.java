package com.kavak.core.model;

import com.kavak.core.dto.CommunicationsTrayDTO;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name ="v2_communications_tray")
public class CommunicationsTray{

    private Long id;
    private String providerName;
    private String type;
    private String subType;
    private String csvName;
    private String phone;
    private String email;
    private String message;
    private Timestamp scheduledDate;
    private String startDate;
    private boolean  smsDeleteregdup;
    private Long voiceAttempts;
    private Long voiceTimering;
    private Long voiceDelayretry;
    private boolean readId;
    private Integer sendProviderId;
    private boolean sendStateId;
    private Integer sendId;
    private Timestamp sendDate;
    private Timestamp createDate;

    @Id
    @Column(name= "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name= "provider_name")
    public String getProviderName() {
        return providerName;
    }
    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    @Column(name= "type")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    @Column(name= "subtype")
    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    @Column(name ="csv_name")
    public String getCsvName() {
        return csvName;
    }
    public void setCsvName(String csvName) {
        this.csvName = csvName;
    }

    @Column(name ="phone")
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name ="email")
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name ="message")
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    @Column(name ="scheduled_date")
    public Timestamp getScheduledDate() {
        return scheduledDate;
    }
    public void setScheduledDate(Timestamp scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    @Column(name ="start_date")
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @Column(name ="is_sms_deleteregdup")
    public boolean isSmsDeleteregdup() {
        return smsDeleteregdup;
    }
    public void setSmsDeleteregdup(boolean smsDeleteregdup) {
        this.smsDeleteregdup = smsDeleteregdup;
    }

    @Column(name ="voice_attempts")
    public Long getVoiceAttempts() {
        return voiceAttempts;
    }
    public void setVoiceAttempts(Long voiceAttempts) {
        this.voiceAttempts = voiceAttempts;
    }

    @Column(name ="voice_timering")
    public Long getVoiceTimering() {
        return voiceTimering;
    }
    public void setVoiceTimering(Long voiceTimering) {
        this.voiceTimering = voiceTimering;
    }

    @Column(name ="voice_delayretry")
    public Long getVoiceDelayretry() {
        return voiceDelayretry;
    }
    public void setVoiceDelayretry(Long voiceDelayretry) {
        this.voiceDelayretry = voiceDelayretry;
    }

    @Column(name ="read_id")
    public boolean isReadId() {
        return readId;
    }
    public void setReadId(boolean readId) {
        this.readId = readId;
    }

    @Column(name ="send_provider_id")
    public Integer getSendProviderId() {
        return sendProviderId;
    }
    public void setSendProviderId(Integer sendProviderId) {
        this.sendProviderId = sendProviderId;
    }

    @Column(name ="is_send_state_id")
    public boolean isSendStateId() {
        return sendStateId;
    }
    public void setSendStateId(boolean sendStateId) {
        this.sendStateId = sendStateId;
    }

    @Column(name ="send_id")
    public Integer getSendId() {
        return sendId;
    }
    public void setSendId(Integer sendId) {
        this.sendId = sendId;
    }

    @Column(name ="send_date")
    public Timestamp getSendDate() {
        return sendDate;
    }
    public void setSendDate(Timestamp sendDate) {
        this.sendDate = sendDate;
    }

    @Column(name ="create_date")
    public Timestamp getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }


    @Transient
    public CommunicationsTrayDTO getDTO(){
        CommunicationsTrayDTO communicationsTrayDTO = new CommunicationsTrayDTO();
        communicationsTrayDTO.setId(this.id);
        communicationsTrayDTO.setMessage(this.message);
        communicationsTrayDTO.setPhone(this.phone);
        communicationsTrayDTO.setCsvName(this.csvName);
        communicationsTrayDTO.setEmail(this.email);
        communicationsTrayDTO.setType(this.type);
        communicationsTrayDTO.setSubType(this.subType);
        return communicationsTrayDTO;
    }
}
