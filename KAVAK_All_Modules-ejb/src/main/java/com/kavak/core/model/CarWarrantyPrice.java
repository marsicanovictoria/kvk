package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Enrique on 03-Jul-17.
 */

@Entity
@Table(name="v2_car_warranty_price")
public class CarWarrantyPrice {

    private Long id;
    private String company;
    private String category;
    private int yearsFrom;
    private int yearsTo;
    private Long treeMonthsPrice;
    private Long oneYearPrice;
    private Date creationDate;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="company")
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
    @Column(name="category")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    @Column(name="years_from")
    public int getYearsFrom() {
        return yearsFrom;
    }

    public void setYearsFrom(int yearsFrom) {
        this.yearsFrom = yearsFrom;
    }
    @Column(name="years_to")
    public int getYearsTo() {
        return yearsTo;
    }

    public void setYearsTo(int yearsTo) {
        this.yearsTo = yearsTo;
    }
    @Column(name="3_months_price")
    public Long getTreeMonthsPrice() {
        return treeMonthsPrice;
    }

    public void setTreeMonthsPrice(Long treeMonthsPrice) {
        this.treeMonthsPrice = treeMonthsPrice;
    }
    @Column(name="1_year_price")
    public Long getOneYearPrice() {
        return oneYearPrice;
    }

    public void setOneYearPrice(Long oneYearPrice) {
        this.oneYearPrice = oneYearPrice;
    }
    @Column(name="creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
