package com.kavak.core.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="v2_inspection_car_photo")
public class InspectionCarPhoto {

	private Long id;
	private Long idInspection; 
	private String inspectorEmail;
	private String imageUrl;
	private Date creationDate;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}

	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}
	@Column(name="inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}

	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	@Column(name="image_url")
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
