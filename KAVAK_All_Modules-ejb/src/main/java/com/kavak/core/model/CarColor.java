package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Created by Enrique on 19-Jun-17.
 */

import com.kavak.core.dto.CarColorDTO;

@Entity
@Table(name = "v2_car_color")
public class CarColor {

    private Long id;
    private String name;
    private String hexadecimal;
    private boolean active;
    private Timestamp creationDate;
    private Integer position;

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Column(name = "hexadecimal")
    public String getHexadecimal() {
        return hexadecimal;
    }

    public void setHexadecimal(String hexadecimal) {
        this.hexadecimal = hexadecimal;
    }
    @Column(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    @Column(name = "creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "position")
    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Transient
    public CarColorDTO getDTO(){
        CarColorDTO carColorDTO= new CarColorDTO();
        carColorDTO.setId(this.id);
        carColorDTO.setName(this.name);
        carColorDTO.setHexadecimal(this.hexadecimal);
        return carColorDTO;
    }
}
