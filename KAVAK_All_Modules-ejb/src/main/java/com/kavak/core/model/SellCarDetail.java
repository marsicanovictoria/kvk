package com.kavak.core.model;

import com.kavak.core.dto.SaleCarOrderDTO;
import com.kavak.core.dto.SellCarDetailDTO;
import com.kavak.core.dto.SellCarMetaDTO;
import com.kavak.core.util.Constants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sell_car_details")
public class SellCarDetail {

    private Long id;
    private Long idInspection;
    private Long sellerId;
    private String carKm;
    private Long price;
    private String carYear;
    private String carMake;
    private String carModel;
    private String carTrim;
    private String carPackage;
    private boolean certified;
    private boolean valide;
    private boolean active;
    private boolean sale;
    private SellCarMeta sellCarMeta;
    private List<SaleCarOrder> listSaleCarOrders;
    private String carColor;
    private String uberType;
    private String imageCar;
    private List<SellCarWarranty> listSellCarWarranty;
    private List<SellCarInsurance> listSellCarInsurance;
    private List<SellCarDimple> listSellCarDimple;
    private String transmission;
    private String vin;
    private String isNewArrival;
    private String phone;
    private String zipCode;
    private String offerType;
    private String postDate;
    private String preloadDate;
    private Timestamp deliveredDate;
    private Timestamp saleDate;
    private Timestamp bookingDate;
    private Timestamp cancelDate;
    private String whyCarText;
    private String whyCarAudio;
    private boolean allowsAppointments;
    private Long idWarehouseLocation;
    private String warehouseLocationDetail;
    private String sku;
    private Long netsuiteItemId;
    private Long sendNetsuite;
    private List<FinancingData> financingData;

    private Long deliveryDays;
    private String plateState;
    private boolean experienceEmailSent;
    private String shortUrl;
    private Long userId;
    private boolean newCarNotificationSent;
    private Long fakeBooking;
    private String currentWarranty;
    private Timestamp contractDate;
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
    	return id;
    }

    public void setId(Long id) {
    	this.id = id;
    }

    @Column(name = "seller_id")
    public Long getSellerId() {
    	return sellerId;
    }

    public void setSellerId(Long sellerId) {
    	this.sellerId = sellerId;
    }

    @Column(name = "inspection_id")
    public Long getIdInspection() {
    	return idInspection;
    }

    public void setIdInspection(Long idInspection) {
    	this.idInspection = idInspection;
    }

    @Column(name = "car_km")
    public String getCarKm() {
    	return carKm;
    }

    public void setCarKm(String carKm) {
    	this.carKm = carKm;
    }

    @Column(name = "price")
    public Long getPrice() {
    	return price;
    }

    public void setPrice(Long price) {
    	this.price = price;
    }

    @Column(name = "car_year")
    public String getCarYear() {
    	return carYear;
    }

    public void setCarYear(String carYear) {
    	this.carYear = carYear;
    }

    @Column(name = "car_make")
    public String getCarMake() {
	return carMake;
    }

    public void setCarMake(String carMake) {
	this.carMake = carMake;
    }

    @Column(name = "car_model")
    public String getCarModel() {
	return carModel;
    }

    public void setCarModel(String carModel) {
    	this.carModel = carModel;
    }

    @Column(name = "car_trim")
    public String getCarTrim() {
    	return carTrim;
    }

    public void setCarTrim(String carTrim) {
    	this.carTrim = carTrim;
    }

    @OneToOne(mappedBy = "sellCarDetail")
    public SellCarMeta getSellCarMeta() {
    	return sellCarMeta;
    }

    public void setSellCarMeta(SellCarMeta sellCarMeta) {
	this.sellCarMeta = sellCarMeta;
    }

    @OneToMany(mappedBy = "sellCarDetail")
    public List<SaleCarOrder> getListSaleCarOrders() {
	return listSaleCarOrders;
    }

    public void setListSaleCarOrders(List<SaleCarOrder> listSaleCarOrders) {
	this.listSaleCarOrders = listSaleCarOrders;
    }

    @Column(name = "is_certified")
    public boolean isCertified() {
    	return certified;
    }

    public void setCertified(boolean certified) {
    	this.certified = certified;
    }

    @Column(name = "is_valide")
    public boolean isValide() {
    	return valide;
    }

    public void setValide(boolean valide) {
	this.valide = valide;
    }

    @Column(name = "is_active")
    public boolean isActive() {
	return active;
    }

    public void setActive(boolean active) {
    	this.active = active;
    }

    @Column(name = "is_sale")
    public boolean isSale() {
    	return sale;
    }

    public void setSale(boolean sale) {
    	this.sale = sale;
    }

    @Column(name = "color")
    public String getCarColor() {
    	return carColor;
    }

    public void setCarColor(String carColor) {
	this.carColor = carColor;
    }

    @Column(name = "uber_type")
    public String getUberType() {
    	return uberType;
    }

    public void setUberType(String uberType) {
    	this.uberType = uberType;
    }

    @Column(name = "package")
    public String getCarPackage() {
    	return carPackage;
    }

    public void setCarPackage(String carPackage) {
    	this.carPackage = carPackage;
    }

    @Column(name = "images")
    public String getImageCar() {
    	return imageCar;
    }

    public void setImageCar(String imageCar) {
    	this.imageCar = imageCar;
    }

    @OneToMany(mappedBy = "sellCarDetail")
    public List<SellCarWarranty> getListSellCarWarranty() {
    	return listSellCarWarranty;
    }

    public void setListSellCarWarranty(List<SellCarWarranty> listSellCarWarranty) {
    	this.listSellCarWarranty = listSellCarWarranty;
    }

    @OneToMany(mappedBy = "sellCarDetail")
    public List<SellCarInsurance> getListSellCarInsurance() {
    	return listSellCarInsurance;
    }

    public void setListSellCarInsurance(List<SellCarInsurance> listSellCarInsurance) {
    	this.listSellCarInsurance = listSellCarInsurance;
    }

    @OneToMany(mappedBy = "sellCarDetail")
    public List<SellCarDimple> getListSellCarDimple() {
    	return listSellCarDimple;
    }

    public void setListSellCarDimple(List<SellCarDimple> listSellCarDimple) {
	this.listSellCarDimple = listSellCarDimple;
    }

    @Column(name = "transmission")
    public String getTransmission() {
    	return transmission;
    }

    public void setTransmission(String transmission) {
    	this.transmission = transmission;
    }

    @Column(name = "vin")
    public String getVin() {
	return vin;
    }

    public void setVin(String vin) {
	this.vin = vin;
    }

    @Column(name = "is_new_arrival")
    public String getIsNewArrival() {
    	return isNewArrival;
    }

    public void setIsNewArrival(String isNewArrival) {
    	this.isNewArrival = isNewArrival;
    }

    @Column(name = "phn_no")
    public String getPhone() {
    	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    @Column(name = "pincode")
    public String getZipCode() {
	return zipCode;
    }

    public void setZipCode(String zipCode) {
    	this.zipCode = zipCode;
    }

    @Column(name = "offer_type")
    public String getOfferType() {
    	return offerType;
    }

    public void setOfferType(String offerType) {
    	this.offerType = offerType;
    }

    @Column(name = "postdate")
    public String getPostDate() {
    	return postDate;
    }

    public void setPostDate(String postDate) {
	this.postDate = postDate;
    }

    @Column(name = "why_car")
    public String getWhyCarText() {
    	return whyCarText;
    }

    public void setWhyCarText(String whyCarText) {
    	this.whyCarText = whyCarText;
    }

    @Column(name = "why_car_audio")
    public String getWhyCarAudio() {
    	return whyCarAudio;
    }

    public void setWhyCarAudio(String whyCarAudio) {
    	this.whyCarAudio = whyCarAudio;
    }

    @Column(name = "permite_citas")
    public boolean isAllowsAppointments() {
	return allowsAppointments;
    }

    public void setAllowsAppointments(boolean allowsAppointments) {
    	this.allowsAppointments = allowsAppointments;
    }

    @Column(name = "warehouse_location")
    public Long getIdWarehouseLocation() {
    	return idWarehouseLocation;
    }

    public void setIdWarehouseLocation(Long idWarehouseLocation) {
    	this.idWarehouseLocation = idWarehouseLocation;
    }

    @Column(name = "warehouse_location_detail")
	public String getWarehouseLocationDetail() {
		return warehouseLocationDetail;
	}

	public void setWarehouseLocationDetail(String warehouseLocationDetail) {
		this.warehouseLocationDetail = warehouseLocationDetail;
	}

	@Column(name = "sku")
    public String getSku() {
    	return sku;
    }

    public void setSku(String sku) {
	this.sku = sku;
    }

    @Column(name = "codigo_item_netsuite")
    public Long getNetsuiteItemId() {
    	return netsuiteItemId;
    }

    public void setNetsuiteItemId(Long netsuiteItemId) {
    	this.netsuiteItemId = netsuiteItemId;
    }

    @Column(name = "enviado_netsuite")
    public Long getSendNetsuite() {
    	return sendNetsuite;
    }

    public void setSendNetsuite(Long sendNetsuite) {
    	this.sendNetsuite = sendNetsuite;
    }

    @OneToMany(mappedBy = "sellCarDetail")
    public List<FinancingData> getFinancingData() {
    	return financingData;
    }

    public void setFinancingData(List<FinancingData> financingData) {
	this.financingData = financingData;
    }

    @Column(name = "booking_date")
    public Timestamp getBookingDate() {
    	return bookingDate;
    }

    public void setBookingDate(Timestamp bookingDate) {
    	this.bookingDate = bookingDate;
    }

    @Column(name = "cancel_date")
    public Timestamp getCancelDate() {
    	return cancelDate;
    }

    public void setCancelDate(Timestamp cancelDate) {
    	this.cancelDate = cancelDate;
    }

    @Column(name = "delivery_days")
    public Long getDeliveryDays() {
    	return deliveryDays;
    }

    public void setDeliveryDays(Long deliveryDays) {
    	this.deliveryDays = deliveryDays;
    }

    @Column(name = "estado_placas")
    public String getPlateState() {
    	return plateState;
    }

    public void setPlateState(String plateState) {
    	this.plateState = plateState;
    }

    @Column(name = "experience_email_sent")
    public boolean isExperienceEmailSent() {
    	return experienceEmailSent;
    }

    public void setExperienceEmailSent(boolean experienceEmailSent) {
	this.experienceEmailSent = experienceEmailSent;
    }

    @Column(name = "delivered_date")
    public Timestamp getDeliveredDate() {
    	return deliveredDate;
    }

    public void setDeliveredDate(Timestamp deliveredDate) {
    	this.deliveredDate = deliveredDate;
    }

    @Column(name = "sale_date")
    public Timestamp getSaleDate() {
    	return saleDate;
    }

    public void setSaleDate(Timestamp saleDate) {
    	this.saleDate = saleDate;
    }

    @Column(name = "new_car_notification_sent")
    public boolean isNewCarNotificationSent() {
        return newCarNotificationSent;
    }

    public void setNewCarNotificationSent(boolean newCarNotificationSent) {
        this.newCarNotificationSent = newCarNotificationSent;
    }

    @Column(name = "short_url")
    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    @Column(name = "user_id")
    public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
   
	@Column(name = "fake_booking")
	public Long getFakeBooking() {
		return fakeBooking;
	}

	public void setFakeBooking(Long fakeBooking) {
		this.fakeBooking = fakeBooking;
	}

	@Column(name = "preload_date")
	public String getPreloadDate() {
		return preloadDate;
	}

	public void setPreloadDate(String preloadDate) {
		this.preloadDate = preloadDate;
	}

	@Column(name = "garantia_vigente")
	public String getCurrentWarranty() {
	    return currentWarranty;
	}

	public void setCurrentWarranty(String currentWarranty) {
	    this.currentWarranty = currentWarranty;
	}

	@Column(name = "contract_date")
	public Timestamp getContractDate() {
	    return contractDate;
	}

	public void setContractDate(Timestamp contractDate) {
	    this.contractDate = contractDate;
	}

	@Transient 
    public SellCarDetailDTO getDTO() {
		SellCarDetailDTO sellCarDetailDTO = new SellCarDetailDTO();
		sellCarDetailDTO.setId(this.id);
		sellCarDetailDTO.setCarKm(this.carKm);
		sellCarDetailDTO.setCarPackage(this.carPackage);
		sellCarDetailDTO.setPrice(this.price);
		sellCarDetailDTO.setCarYear(this.carYear);
		sellCarDetailDTO.setCarMake(this.carMake);
		sellCarDetailDTO.setCarModel(this.carModel);
		sellCarDetailDTO.setCarTrim(this.carTrim);
		sellCarDetailDTO.setSku(this.sku);
		sellCarDetailDTO.setCertified(this.certified);
		sellCarDetailDTO.setValide(this.valide);
		sellCarDetailDTO.setActive(this.active);
		sellCarDetailDTO.setSale(this.sale);
		sellCarDetailDTO.setTransmission(this.transmission);
		if (this.sellCarMeta != null) {
		    sellCarDetailDTO.setSellCarMetaDTO(this.sellCarMeta.getDTO());
		} else {
		    sellCarDetailDTO.setSellCarMetaDTO(new SellCarMetaDTO());
		}
		if (this.listSaleCarOrders != null) {
		    List<SaleCarOrderDTO> listSaleCarOrdersDTO = new ArrayList<>();
		    for (SaleCarOrder saleCarOrderActual : listSaleCarOrders) {
			listSaleCarOrdersDTO.add(saleCarOrderActual.getDTO());
		    }
		    sellCarDetailDTO.setSaleCarOrdersDTO(listSaleCarOrdersDTO);
		} else {
		    sellCarDetailDTO.setSaleCarOrdersDTO(new ArrayList<SaleCarOrderDTO>());
		}
		sellCarDetailDTO.setUberType(this.uberType);
		String[] arrayImagesCar = this.imageCar.split("#");
		List<String> listImages = new ArrayList<String>();
        for (String imageActual : arrayImagesCar) {

            CharSequence http = "http";
            CharSequence https = "https";

            if (imageActual.contains(http) || imageActual.contains(https)) {
                    listImages.add(imageActual);
            } else {
                listImages.add(Constants.URL_BASE_IMAGES_PRODUCT_GALERY + imageActual);
            }
        }
        sellCarDetailDTO.setListImagesCar(listImages);
		sellCarDetailDTO.setListSellCarWarranty(this.listSellCarWarranty);
		sellCarDetailDTO.setListSellCarInsurance(this.listSellCarInsurance);
		sellCarDetailDTO.setListSellCarDimple(this.listSellCarDimple);
		sellCarDetailDTO.setListSellCarDimple(this.listSellCarDimple);
		sellCarDetailDTO.setWhyCarText(this.whyCarText);
		sellCarDetailDTO.setWhyCarAudio(this.whyCarAudio);
		sellCarDetailDTO.setSendNetsuite(this.sendNetsuite);
		sellCarDetailDTO.setNetsuiteItemId(this.netsuiteItemId);
		sellCarDetailDTO.setShortUrl(this.shortUrl);
		sellCarDetailDTO.setUserId(this.userId);
		sellCarDetailDTO.setFakeBooking(this.fakeBooking);
		sellCarDetailDTO.setPlateState(this.plateState);
		sellCarDetailDTO.setFakeBooking(this.fakeBooking);
		
		if (this.deliveryDays != null && this.deliveryDays >= 0) {
		    sellCarDetailDTO.setDeliveryDate(this.deliveryDays);
		}
	
		return sellCarDetailDTO;
    }

}