package com.kavak.core.model;

import com.kavak.core.dto.DimpleDTO;

import javax.persistence.*;

@Entity
@Table(name="v2_dimple_type")
public class DimpleType {
	
	private Long id;
	private String name;
	private boolean active;

	@Id
	@Column(name="id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@Transient
	public DimpleDTO getDTO(){
		DimpleDTO dimpleDTO = new DimpleDTO();
		dimpleDTO.setId(this.id);
		dimpleDTO.setName(this.name);
		dimpleDTO.setActive(this.active);
		return dimpleDTO;
	}
}