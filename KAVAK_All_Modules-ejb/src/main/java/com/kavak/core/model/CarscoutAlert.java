package com.kavak.core.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "v2_carscout_alert")
public class CarscoutAlert {

    private Long id;
    private User user;
    private Integer activeInd;
    private Integer sms;
    private Integer whatsapp;
    private Integer phone;
    private String email;
    private String push;
    private Timestamp creationDate;
    private Timestamp lastUpdate;
    private Timestamp blacklistDate;
    private Timestamp cancelDate;
    private List<CarscoutAlertDetail> carscoutAlertDetail;
    private List<CarscoutAlertLkpSku> carscoutAlertLkpSku;
    private List<CarscoutNotify> carscoutNotify;
    private boolean customerEmailSent;
    private String source;
    private String versionApp;

    @Id
    @Column(name = "alert_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "active_ind")
    public Integer getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(Integer activeInd) {
        this.activeInd = activeInd;
    }

    @Column(name = "sms")
    public Integer getSms() {
        return sms;
    }

    public void setSms(Integer sms) {
        this.sms = sms;
    }

    @Column(name = "whatsapp")
    public Integer getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(Integer whatsapp) {
        this.whatsapp = whatsapp;
    }

    @Column(name = "telf")
    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "push")
    public String getPush() {
        return push;
    }

    public void setPush(String push) {
        this.push = push;
    }

    @Column(name = "creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "last_update")
    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Column(name = "blacklist_date")
    public Timestamp getBlacklistDate() {
        return blacklistDate;
    }

    public void setBlacklistDate(Timestamp blacklistDate) {
        this.blacklistDate = blacklistDate;
    }

    @Column(name = "cancel_date")
    public Timestamp getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Timestamp cancelDate) {
        this.cancelDate = cancelDate;
    }

    @OneToMany(mappedBy = "carscoutAlert")
    public List<CarscoutAlertDetail> getCarscoutAlertDetail() {
        return carscoutAlertDetail;
    }

    public void setCarscoutAlertDetail(List<CarscoutAlertDetail> carscoutAlertDetail) {
        this.carscoutAlertDetail = carscoutAlertDetail;
    }

    @OneToMany(mappedBy = "carscoutAlert")
    public List<CarscoutAlertLkpSku> getCarscoutAlertLkpSku() {
        return carscoutAlertLkpSku;
    }

    public void setCarscoutAlertLkpSku(List<CarscoutAlertLkpSku> carscoutAlertLkpSku) {
        this.carscoutAlertLkpSku = carscoutAlertLkpSku;
    }

    @OneToMany(mappedBy = "carscoutAlert")
    public List<CarscoutNotify> getCarscoutNotify() {
        return carscoutNotify;
    }

    public void setCarscoutNotify(List<CarscoutNotify> carscoutNotify) {
        this.carscoutNotify = carscoutNotify;
    }

    @Column(name = "customer_email_sent")
    public boolean isCustomerEmailSent() {
        return customerEmailSent;
    }

    public void setCustomerEmailSent(boolean customerEmailSent) {
        this.customerEmailSent = customerEmailSent;
    }

    @Column(name = "source")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Column(name = "version_app")
    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }
}
