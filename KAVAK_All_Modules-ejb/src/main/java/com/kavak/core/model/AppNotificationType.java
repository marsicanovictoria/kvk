package com.kavak.core.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "v2_app_notification_type")
public class AppNotificationType {

	private Long id;
	private String title;
	private String description;
	private String iconUrl;
	private boolean active;
	private Timestamp creationDate;
	private List<AppNotificationSetting> listAppNotificationSetting;
	

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "title")
	public String getTitle() {
	    return title;
	}

	public void setTitle(String title) {
	    this.title = title;
	}

	@Column(name = "description")
	public String getDescription() {
	    return description;
	}

	public void setDescription(String description) {
	    this.description = description;
	}

	@Column(name = "icon_url")
	public String getIconUrl() {
	    return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
	    this.iconUrl = iconUrl;
	}

	@Column(name = "creation_date")
	public Timestamp getCreationDate() {
	    return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
	    this.creationDate = creationDate;
	}

	@Column(name = "is_active")
	public boolean isActive() {
	    return active;
	}

	public void setActive(boolean active) {
	    this.active = active;
	}

	@OneToMany(mappedBy="appNotificationType")
	public List<AppNotificationSetting> getListAppNotificationSetting() {
	    return listAppNotificationSetting;
	}

	public void setListAppNotificationSetting(List<AppNotificationSetting> listAppNotificationSetting) {
	    this.listAppNotificationSetting = listAppNotificationSetting;
	}












}
