package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

/**
 * Created by Enrique on 27-Jun-17.
 */
@Entity
@Table(name = "v2_car_make")
public class CarMake {

	private Long id;
	private String name;
	private String imageMakeUrl;
	private boolean active;
	private Date creationDate;
	private List<CarscoutCatalogue> carscoutCatalogue;
	private List<CarModel> carModels;

	@Id
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "image")
	public String getImageMakeUrl() {
		return imageMakeUrl;
	}

	public void setImageMakeUrl(String imageMakeUrl) {
		this.imageMakeUrl = imageMakeUrl;
	}

	@Column(name = "active")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(name = "creation_date")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@OneToMany(mappedBy = "carMake")
	public List<CarscoutCatalogue> getCarscoutCatalogue() {
		return carscoutCatalogue;
	}

	public void setCarscoutCatalogue(List<CarscoutCatalogue> carscoutCatalogue) {
		this.carscoutCatalogue = carscoutCatalogue;
	}

	@OneToMany(mappedBy = "carMake")
	public List<CarModel> getCarModels() {
		return carModels;
	}

	public void setCarModels(List<CarModel> carModels) {
		this.carModels = carModels;
	}
}
