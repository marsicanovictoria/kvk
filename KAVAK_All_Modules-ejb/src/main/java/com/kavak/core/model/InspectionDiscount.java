package com.kavak.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.InspectionDiscountDTO;

@Entity
@Table(name = "v2_inspection_discount")
public class InspectionDiscount {

	private Long id;
	private Long idInspection;
	private String inspectorEmail;
	private String discountType;
	private Long discountAmount;
	private boolean removed;
	private boolean paidByKavak;
	private String removedReason;
	private Date creationDate;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "inspection_id")
	public Long getIdInspection() {
		return idInspection;
	}
	public void setIdInspection(Long idInspection) {
		this.idInspection = idInspection;
	}

	@Column(name = "discount_type")
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	@Column(name = "discount_amount")
	public Long getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Long discountAmount) {
		this.discountAmount = discountAmount;
	}

    @Column(name = "is_paid_by_kavak")
    public boolean isPaidByKavak() {
        return paidByKavak;
    }
    public void setPaidByKavak(boolean paidByKavak) {
        this.paidByKavak = paidByKavak;
    }

	@Column(name = "is_removed")
	public boolean isRemoved() {
		return removed;
	}
	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	@Column(name = "removed_reason")
	public String getRemovedReason() {
		return removedReason;
	}
	public void setRemovedReason(String removedReason) {
		this.removedReason = removedReason;
	}

	@Column(name = "creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "inspector_email")
	public String getInspectorEmail() {
		return inspectorEmail;
	}
	public void setInspectorEmail(String inspectorEmail) {
		this.inspectorEmail = inspectorEmail;
	}
	
	@Transient
	public InspectionDiscountDTO getDTO(){
		InspectionDiscountDTO inspectionDiscountDTO = new InspectionDiscountDTO();
		inspectionDiscountDTO.setId(this.id);
		inspectionDiscountDTO.setIdInspection(this.idInspection);
		inspectionDiscountDTO.setInspectorEmail(this.inspectorEmail);
		inspectionDiscountDTO.setDiscountType(this.discountType);
		inspectionDiscountDTO.setDiscountAmount(this.discountAmount);
		inspectionDiscountDTO.setRemoved(this.removed);
		inspectionDiscountDTO.setPaidByKavak(this.paidByKavak);
		inspectionDiscountDTO.setRemovedReason(this.removedReason);
		inspectionDiscountDTO.setCreationDate(this.creationDate);
		
		return inspectionDiscountDTO;
	}

}
