package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.financing.FinancingUserDomicileDTO;

@Entity
@Table(name = "v2_financing_user_domicile")
public class FinancingUserDomicile {

    private Long id;
    private Long userId;
    private Long financingId;
    private String street;
    private String externalNumber;
    private String internalNumber;
    private String colony;
    private String delegation;
    private String state;
    private String zipCode;
    private String residentYears;
    private String livingType;
    private Timestamp creationDate;
    private boolean active;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO) 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Column(name = "street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Column(name = "external_number")
    public String getExternalNumber() {
        return externalNumber;
    }

    public void setExternalNumber(String externalNumber) {
        this.externalNumber = externalNumber;
    }
    
    @Column(name = "internal_number")
    public String getInternalNumber() {
        return internalNumber;
    }

    public void setInternalNumber(String internalNumber) {
        this.internalNumber = internalNumber;
    }

    @Column(name = "colony")
    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    @Column(name = "delegation")
    public String getDelegation() {
        return delegation;
    }

    public void setDelegation(String delegation) {
        this.delegation = delegation;
    }

    @Column(name = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name = "zip_code")
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Column(name = "resident_years")
    public String getResidentYears() {
        return residentYears;
    }

    public void setResidentYears(String residentYears) {
        this.residentYears = residentYears;
    }

    @Column(name = "living_type")
    public String getLivingType() {
        return livingType;
    }

    public void setLivingType(String livingType) {
        this.livingType = livingType;
    }

    @Column(name = "creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    @Column(name = "financing_id")
    public Long getFinancingId() {
        return financingId;
    }

    public void setFinancingId(Long financingId) {
        this.financingId = financingId;
    }

    @Transient
    public FinancingUserDomicileDTO getDTO() {
	FinancingUserDomicileDTO financingUserDomicileDTO = new FinancingUserDomicileDTO();
	financingUserDomicileDTO.setId(this.id);
	financingUserDomicileDTO.setUserId(this.userId);
	financingUserDomicileDTO.setStreet(this.street);
	financingUserDomicileDTO.setExternalNumber(this.externalNumber);
	financingUserDomicileDTO.setColony(this.colony);
	financingUserDomicileDTO.setDelegation(this.delegation);
	financingUserDomicileDTO.setState(this.state);
	financingUserDomicileDTO.setZipCode(this.zipCode);
	financingUserDomicileDTO.setResidentYears(this.residentYears);
	financingUserDomicileDTO.setLivingType(this.livingType);
	financingUserDomicileDTO.setActive(this.active);
	financingUserDomicileDTO.setCreationDate(this.creationDate);
	financingUserDomicileDTO.setFinancingId(this.financingId);

	return financingUserDomicileDTO;
    }



}