package com.kavak.core.model;

import com.kavak.core.dto.RepairCategoryDTO;
import com.kavak.core.dto.RepairSubCategoryDTO;
import com.kavak.core.dto.RepairTypeDTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="v2_repair_item_category")
public class RepairCategory {
	
	private Long id;
	private String name;
	private boolean active;
	private List<RepairSubCategory> listRepairSubCategory;
	private List<RepairType> listRepairType;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@OneToMany(mappedBy="repairCategory")
	public List<RepairSubCategory> getListRepairSubCategory() {
		return listRepairSubCategory;
	}
	public void setListRepairSubCategory(List<RepairSubCategory> listRepairSubCategory) {
		this.listRepairSubCategory = listRepairSubCategory;
	}
	@OneToMany(mappedBy="repairCategory")
	public List<RepairType> getListRepairType() {
		return listRepairType;
	}
	public void setListRepairType(List<RepairType> listRepairType) {
		this.listRepairType = listRepairType;
	}
	
	@Transient
	public RepairCategoryDTO getDTO(){
		RepairCategoryDTO repairCategoryDTO = new RepairCategoryDTO();
		repairCategoryDTO.setId(this.id);
		repairCategoryDTO.setName(this.name);
		repairCategoryDTO.setActive(this.active);
		List<RepairSubCategoryDTO> listRepairSubCategoryDTO = new ArrayList<>();
		List<RepairTypeDTO> listRepairTypeDTO = new ArrayList<>();
		for(RepairSubCategory repairSubCategoryActual: this.getListRepairSubCategory()){
			listRepairSubCategoryDTO.add(repairSubCategoryActual.getDTO());
		}
		for(RepairType repairTypeActual: this.getListRepairType()){
			listRepairTypeDTO.add(repairTypeActual.getDTO());
		}
		repairCategoryDTO.setListRepairSubCategory(listRepairSubCategoryDTO);
		repairCategoryDTO.setListRepairType(listRepairTypeDTO);
		return repairCategoryDTO;
	}
}