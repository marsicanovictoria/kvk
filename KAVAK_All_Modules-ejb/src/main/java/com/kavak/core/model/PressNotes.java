package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="press_logos")
public class PressNotes {

    private Long id;
    private String userName; //  1 - Compra  2 - Venta
    private String imageAddress;
    private String userDescription;
    private boolean active;
    

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="client_name")
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    @Column(name="image_address")
    public String getImageAddress() {
        return imageAddress;
    }
    public void setImageAddress(String imageAddress) {
        this.imageAddress = imageAddress;
    }
    
    @Column(name="client_description")
    public String getUserDescription() {
        return userDescription;
    }
    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }
    
    @Column(name="active")
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }



}
