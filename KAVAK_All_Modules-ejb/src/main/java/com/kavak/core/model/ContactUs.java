package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="v2_contact_us")
public class ContactUs{

    private Long id;
    private String names;
    private String lastNames;
    private String phone;
    private String zipCode;
    private String email;
    private String comments;
    private Long subjectId;
    private boolean internalEmailSent;


    @Id
    @Column(name= "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name= "names")
    public String getNames() {
        return names;
    }
    public void setNames(String names) {
        this.names = names;
    }
    
    @Column(name= "lastnames")
    public String getLastNames() {
        return lastNames;
    }
    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }
    
    @Column(name= "telefono")
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    @Column(name= "codigo_postal")
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    
    @Column(name= "email")
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    @Column(name= "comments")
    public String getComments() {
        return comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }
    
    @Column(name= "subject_id")
    public Long getSubjectId() {
        return subjectId;
    }
    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }
    
    @Column(name= "internal_email_sent")
    public boolean isInternalEmailSent() {
        return internalEmailSent;
    }
    public void setInternalEmailSent(boolean internalEmailSent) {
        this.internalEmailSent = internalEmailSent;
    }


}
