package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "v2_app_notification_log")
public class AppNotificationLog {

	private Long id;
	private Long userId;
	private Long tokenId;
	private String description;
	private Timestamp creationDate;
	private AppNotificationType appNotificationType;
	private boolean successfulNotification;
	private String notificationMessage;
	private String notificationCode;
	private String fcmMessage;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "user_id")
	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}

	@ManyToOne
	@JoinColumn(name = "type_id", referencedColumnName = "id")
	public AppNotificationType getAppNotificationType() {
	    return appNotificationType;
	}

	public void setAppNotificationType(AppNotificationType appNotificationType) {
	    this.appNotificationType = appNotificationType;
	}
	
	@Column(name = "token_id")
	public Long getTokenId() {
	    return tokenId;
	}

	public void setTokenId(Long tokenId) {
	    this.tokenId = tokenId;
	}
	
	@Column(name = "description")
	public String getDescription() {
	    return description;
	}

	public void setDescription(String description) {
	    this.description = description;
	}
	
	@Column(name = "creation_date")
	public Timestamp getCreationDate() {
	    return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
	    this.creationDate = creationDate;
	}

	
	@Column(name = "successful_notification")
	public boolean isSuccessfulNotification() {
	    return successfulNotification;
	}

	public void setSuccessfulNotification(boolean successfulNotification) {
	    this.successfulNotification = successfulNotification;
	}

	@Column(name = "notification_message")
	public String getNotificationMessage() {
	    return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
	    this.notificationMessage = notificationMessage;
	}

	@Column(name = "notification_code")
	public String getNotificationCode() {
	    return notificationCode;
	}

	public void setNotificationCode(String notificationCode) {
	    this.notificationCode = notificationCode;
	}
	
	@Column(name = "fcm_message")
	public String getFcmMessage() {
	    return fcmMessage;
	}

	public void setFcmMessage(String fcmMessage) {
	    this.fcmMessage = fcmMessage;
	}

























}
