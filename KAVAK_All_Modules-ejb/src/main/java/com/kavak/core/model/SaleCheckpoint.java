package com.kavak.core.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kavak.core.dto.SaleCheckpointDTO;

@Entity
@Table(name = "compra_checkpoint")
public class SaleCheckpoint {

    private Long id;
    private Long userId;
    private String customerName;
    private String customerLastName;
    private String email;
    private Long carId;
    private String sku;
    private Long status;
    private Long carYear;
    private String carMake;
    private String carModel;
    private String carVersion;
    private Long carKm;
    private String zipCode;
    private Long shippingAddressId;
    private String shippingAddressDescription;
    private String warrantyName;
    private String warrantyTime;
    private Long warrantyAmount;
    private String warrantyRange;
    private String insuranceName;
    private String insuranceTime;
    private String insuranceAmount;
    private String insuranceModality;
    private String typePaymentMethodTypeId;
    private String isFinanced;
    private String isFinancingCompleted;
    private String downPayment;
    private String amountFinancing;
    private String monthFinancing;
    private String checkoutResult;
    private String paymentPlatform;
    private String paymentPlatformId;
    private String transactionNumber;
    private Long paymentStatus;
    private String carCost;
    private String totalAmount;
    private String carReserve;
    private String carShippingCost;
    private Timestamp registerDate;
    private Timestamp updateDate;
    private Long saleOpportunityTypeId;
    private Long scheduleDateId;
    private Long duplicatedOfferControl;
    private String appointmentContact;
    private String appointmentDate;
    private String comments;
    private String otherInterestedCar;
    private String watchedCar;
    private User user;
    private Long assignedEm;
    private Long sentNetsuite;
    private String searchKavak;
    private boolean hasTradeinCar;
    private Long offerCheckPointId;
    private Long netsuiteOpportunityId;
    private String netsuiteOpportunityURL;
    private Timestamp netsuiteIdUpdateDate;
    private String appliedCoupon;
    private boolean sendEmail;
    private String source;
    private String versionApp;
    private boolean appointmentEmailSent;
    private boolean bookedCarSmsSent;
    private boolean scheduledAppointmentSmsSent;
    private Long internalUserId;
    private boolean bookingTimeReminderNotificationSent;
    private boolean bookingTimeExpiredNotificationSent;
    private boolean bookedRegisteredNotificationSent;
    private boolean appointmentBookedNotificationSent;
    private boolean appointmentRegisteredNotificationSent;
    private String customerComment;
    private String customerVoice;
    private boolean bookedEmailSent;
    private Long financingId;
    private boolean financingEmailSent;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Column(name = "id_usuario", insertable = false, updatable = false)
    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    @Column(name = "nombre")
    public String getCustomerName() {
	return customerName;
    }

    public void setCustomerName(String customerName) {
	this.customerName = customerName;
    }

    @Column(name = "apellido")
    public String getCustomerLastName() {
	return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
	this.customerLastName = customerLastName;
    }

    @Column(name = "correo")
    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    @Column(name = "id_auto")
    public Long getCarId() {
	return carId;
    }

    public void setCarId(Long carId) {
	this.carId = carId;
    }

    @Column(name = "sku")
    public String getSku() {
	return sku;
    }

    public void setSku(String sku) {
	this.sku = sku;
    }

    @Column(name = "estatus")
    public Long getStatus() {
	return status;
    }

    public void setStatus(Long status) {
	this.status = status;
    }

    @Column(name = "anio")
    public Long getCarYear() {
	return carYear;
    }

    public void setCarYear(Long carYear) {
	this.carYear = carYear;
    }

    @Column(name = "marca")
    public String getCarMake() {
	return carMake;
    }

    public void setCarMake(String carMake) {
	this.carMake = carMake;
    }

    @Column(name = "modelo")
    public String getCarModel() {
	return carModel;
    }

    public void setCarModel(String carModel) {
	this.carModel = carModel;
    }

    @Column(name = "version")
    public String getCarVersion() {
	return carVersion;
    }

    public void setCarVersion(String carVersion) {
	this.carVersion = carVersion;
    }

    @Column(name = "km")
    public Long getCarKm() {
	return carKm;
    }

    public void setCarKm(Long carKm) {
	this.carKm = carKm;
    }

    @Column(name = "codigo_postal")
    public String getZipCode() {
	return zipCode;
    }

    public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
    }

    @Column(name = "direccion_entrega")
    public Long getShippingAddressId() {
	return shippingAddressId;
    }

    public void setShippingAddressId(Long shippingAddressId) {
	this.shippingAddressId = shippingAddressId;
    }

    @Column(name = "direccion_entrega_texto")
    public String getShippingAddressDescription() {
	return shippingAddressDescription;
    }

    public void setShippingAddressDescription(String shippingAddressDescription) {
	this.shippingAddressDescription = shippingAddressDescription;
    }

    @Column(name = "nombre_garantia")
    public String getWarrantyName() {
	return warrantyName;
    }

    public void setWarrantyName(String warrantyName) {
	this.warrantyName = warrantyName;
    }

    @Column(name = "tiempo_garantia")
    public String getWarrantyTime() {
	return warrantyTime;
    }

    public void setWarrantyTime(String warrantyTime) {
	this.warrantyTime = warrantyTime;
    }

    @Column(name = "monto_garantia")
    public Long getWarrantyAmount() {
	return warrantyAmount;
    }

    public void setWarrantyAmount(Long warrantyAmount) {
	this.warrantyAmount = warrantyAmount;
    }

    @Column(name = "rango_garantia")
    public String getWarrantyRange() {
	return warrantyRange;
    }

    public void setWarrantyRange(String warrantyRange) {
	this.warrantyRange = warrantyRange;
    }

    @Column(name = "nombre_seguro")
    public String getInsuranceName() {
	return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
	this.insuranceName = insuranceName;
    }

    @Column(name = "tiempo_seguro")
    public String getInsuranceTime() {
	return insuranceTime;
    }

    public void setInsuranceTime(String insuranceTime) {
	this.insuranceTime = insuranceTime;
    }

    @Column(name = "monto_seguro")
    public String getInsuranceAmount() {
	return insuranceAmount;
    }

    public void setInsuranceAmount(String insuranceAmount) {
	this.insuranceAmount = insuranceAmount;
    }

    @Column(name = "seguro_modalidad")
    public String getInsuranceModality() {
	return insuranceModality;
    }

    public void setInsuranceModality(String insuranceModality) {
	this.insuranceModality = insuranceModality;
    }

    @Column(name = "tipo_metodo_pago")
    public String getTypePaymentMethodTypeId() {
	return typePaymentMethodTypeId;
    }

    public void setTypePaymentMethodTypeId(String typePaymentMethodTypeId) {
	this.typePaymentMethodTypeId = typePaymentMethodTypeId;
    }

    @Column(name = "solicito_financiamiento")
    public String getIsFinanced() {
	return isFinanced;
    }

    public void setIsFinanced(String isFinanced) {
	this.isFinanced = isFinanced;
    }

    @Column(name = "completo_solicitud")
    public String getIsFinancingCompleted() {
	return isFinancingCompleted;
    }

    public void setIsFinancingCompleted(String isFinancingCompleted) {
	this.isFinancingCompleted = isFinancingCompleted;
    }

    @Column(name = "monto_enganche")
    public String getDownPayment() {
	return downPayment;
    }

    public void setDownPayment(String downPayment) {
	this.downPayment = downPayment;
    }

    @Column(name = "monto_financiamiento")
    public String getAmountFinancing() {
	return amountFinancing;
    }

    public void setAmountFinancing(String amountFinancing) {
	this.amountFinancing = amountFinancing;
    }

    @Column(name = "meses_financiamiento")
    public String getMonthFinancing() {
	return monthFinancing;
    }

    public void setMonthFinancing(String monthFinancing) {
	this.monthFinancing = monthFinancing;
    }

    @Column(name = "checkout_resultado")
    public String getCheckoutResult() {
	return checkoutResult;
    }

    public void setCheckoutResult(String checkoutResult) {
	this.checkoutResult = checkoutResult;
    }

    @Column(name = "plataforma_pago")
    public String getPaymentPlatform() {
	return paymentPlatform;
    }

    public void setPaymentPlatform(String paymentPlatform) {
	this.paymentPlatform = paymentPlatform;
    }

    @Column(name = "id_plataforma_pago")
    public String getPaymentPlatformId() {
	return paymentPlatformId;
    }

    public void setPaymentPlatformId(String paymentPlatformId) {
	this.paymentPlatformId = paymentPlatformId;
    }

    @Column(name = "num_transaccion")
    public String getTransactionNumber() {
	return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
	this.transactionNumber = transactionNumber;
    }

    @Column(name = "estatus_pago")
    public Long getPaymentStatus() {
	return paymentStatus;
    }

    public void setPaymentStatus(Long paymentStatus) {
	this.paymentStatus = paymentStatus;
    }

    @Column(name = "costo_auto")
    public String getCarCost() {
	return carCost;
    }

    public void setCarCost(String carCost) {
	this.carCost = carCost;
    }

    @Column(name = "monto_total")
    public String getTotalAmount() {
	return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
	this.totalAmount = totalAmount;
    }

    @Column(name = "reserva_auto")
    public String getCarReserve() {
	return carReserve;
    }

    public void setCarReserve(String carReserve) {
	this.carReserve = carReserve;
    }

    @Column(name = "costo_envio_auto")
    public String getCarShippingCost() {
	return carShippingCost;
    }

    public void setCarShippingCost(String carShippingCost) {
	this.carShippingCost = carShippingCost;
    }

    @Column(name = "fecha_registro")
    public Timestamp getRegisterDate() {
	return registerDate;
    }

    public void setRegisterDate(Timestamp registerDate) {
	this.registerDate = registerDate;
    }

    @Column(name = "fecha_actualizacion")
    public Timestamp getUpdateDate() {
	return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
	this.updateDate = updateDate;
    }

    @Column(name = "tipo_oportunidad_venta_id")
    public Long getSaleOpportunityTypeId() {
	return saleOpportunityTypeId;
    }

    public void setSaleOpportunityTypeId(Long saleOpportunityTypeId) {
	this.saleOpportunityTypeId = saleOpportunityTypeId;
    }

    @Column(name = "horarios_citas_id")
    public Long getScheduleDateId() {
	return scheduleDateId;
    }

    public void setScheduleDateId(Long scheduleDateId) {
	this.scheduleDateId = scheduleDateId;
    }

    /*
     * @Column(name="codigo_item_netsuite") public String getNetsuiteItemId() {
     * return netsuiteItemId; } public void setNetsuiteItemId(String
     * netsuiteItemId) { this.netsuiteItemId = netsuiteItemId; }
     */
    @Column(name = "control_compra_duplicada")
    public Long getDuplicatedOfferControl() {
	return duplicatedOfferControl;
    }

    public void setDuplicatedOfferControl(Long duplicatedOfferControl) {
	this.duplicatedOfferControl = duplicatedOfferControl;
    }

    @Column(name = "appointment_contact")
    public String getAppointmentContact() {
	return appointmentContact;
    }

    public void setAppointmentContact(String appointmentContact) {
	this.appointmentContact = appointmentContact;
    }

    @Column(name = "appointment_date")
    public String getAppointmentDate() {
	return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
	this.appointmentDate = appointmentDate;
    }

    @Column(name = "comments")
    public String getComments() {
	return comments;
    }

    public void setComments(String comments) {
	this.comments = comments;
    }

    @Column(name = "other_cars")
    public String getOtherInterestedCar() {
	return otherInterestedCar;
    }

    public void setOtherInterestedCar(String otherInterestedCar) {
	this.otherInterestedCar = otherInterestedCar;
    }

    @Column(name = "watched_car")
    public String getWatchedCar() {
	return watchedCar;
    }

    public void setWatchedCar(String watchedCar) {
	this.watchedCar = watchedCar;
    }

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    public User getUser() {
	return user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    @Column(name = "enviado_netsuite")
    public Long isSentNetsuite() {
	return sentNetsuite;
    }

    public void setSentNetsuite(Long sentNetsuite) {
	this.sentNetsuite = sentNetsuite;
    }

    @Column(name = "assigned_em")
    public Long getAssignedEm() {
	return assignedEm;
    }

    public void setAssignedEm(Long assignedEm) {
	this.assignedEm = assignedEm;
    }

    @Column(name = "buscar_kavak")
    public String getSearchKavak() {
	return searchKavak;
    }

    public void setSearchKavak(String searchKavak) {
	this.searchKavak = searchKavak;
    }

    @Column(name = "has_tradein_car")
    public boolean isHasTradeinCar() {
	return hasTradeinCar;
    }

    public void setHasTradeinCar(boolean hasTradeinCar) {
	this.hasTradeinCar = hasTradeinCar;
    }

    @Column(name = "offer_checkpoint_id")
    public Long getOfferCheckPointId() {
	return offerCheckPointId;
    }

    public void setOfferCheckPointId(Long offerCheckPointId) {
	this.offerCheckPointId = offerCheckPointId;
    }

    @Column(name = "netsuite_opportunity_id")
    public Long getNetsuiteOpportunityId() {
	return netsuiteOpportunityId;
    }

    public void setNetsuiteOpportunityId(Long netsuiteOpportunityId) {
	this.netsuiteOpportunityId = netsuiteOpportunityId;
    }

    @Column(name = "url_netsuite_opportunity")
    public String getNetsuiteOpportunityURL() {
	return netsuiteOpportunityURL;
    }

    public void setNetsuiteOpportunityURL(String netsuiteOpportunityURL) {
	this.netsuiteOpportunityURL = netsuiteOpportunityURL;
    }

    @Column(name = "sent_to_netsuite_date")
    public Timestamp getNetsuiteIdUpdateDate() {
	return netsuiteIdUpdateDate;
    }

    public void setNetsuiteIdUpdateDate(Timestamp netsuiteIdUpdateDate) {
	this.netsuiteIdUpdateDate = netsuiteIdUpdateDate;
    }

    @Column(name = "applied_coupon")
    public String getAppliedCoupon() {
	return appliedCoupon;
    }

    public void setAppliedCoupon(String appliedCoupon) {
	this.appliedCoupon = appliedCoupon;
    }

    @Column(name = "correo_enviado")
    public boolean isSendEmail() {
	return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
	this.sendEmail = sendEmail;
    }

    @Column(name = "origen_compra")
    public String getSource() {
	return source;
    }

    public void setSource(String source) {
	this.source = source;
    }

    @Column(name = "version_app")
    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    @Column(name = "appointment_email_sent")
    public boolean isAppointmentEmailSent() {
	return appointmentEmailSent;
    }
    public void setAppointmentEmailSent(boolean appointmentEmailSent) {
	this.appointmentEmailSent = appointmentEmailSent;
    }

    @Column(name = "booked_car_sms_sent")
    public boolean isBookedCarSmsSent() {
        return bookedCarSmsSent;
    }

    public void setBookedCarSmsSent(boolean bookedCarSmsSent) {
        this.bookedCarSmsSent = bookedCarSmsSent;
    }

    @Column(name = "scheduled_appointment_sms_sent")
    public boolean isScheduledAppointmentSmsSent() {
        return scheduledAppointmentSmsSent;
    }

    public void setScheduledAppointmentSmsSent(boolean scheduledAppointmentSmsSent) {
        this.scheduledAppointmentSmsSent = scheduledAppointmentSmsSent;
    }

    @Column(name = "internal_user_id")
    public Long getInternalUserId() {
	return internalUserId;
    }

    public void setInternalUserId(Long internalUserId) {
	this.internalUserId = internalUserId;
    }

    @Column(name = "booking_time_reminder_notification_sent")
    public boolean isBookingTimeReminderNotificationSent() { 
	return bookingTimeReminderNotificationSent;
    }

    public void setBookingTimeReminderNotificationSent(boolean bookingTimeReminderNotificationSent) {
	this.bookingTimeReminderNotificationSent = bookingTimeReminderNotificationSent;
    }
    
    @Column(name = "booking_time_expired_notification_sent")
    public boolean isBookingTimeExpiredNotificationSent() {
	return bookingTimeExpiredNotificationSent;
    }

    public void setBookingTimeExpiredNotificationSent(boolean bookingTimeExpiredNotificationSent) {
	this.bookingTimeExpiredNotificationSent = bookingTimeExpiredNotificationSent;
    }

    @Column(name = "booked_registered_notification_sent")
    public boolean isBookedRegisteredNotificationSent() {
        return bookedRegisteredNotificationSent;
    }

    public void setBookedRegisteredNotificationSent(boolean bookedRegisteredNotificationSent) {
        this.bookedRegisteredNotificationSent = bookedRegisteredNotificationSent;
    }

    @Column(name = "appointment_booked_notification_sent")
    public boolean isAppointmentBookedNotificationSent() {
        return appointmentBookedNotificationSent;
    }

    public void setAppointmentBookedNotificationSent(boolean appointmentBookedNotificationSent) {
        this.appointmentBookedNotificationSent = appointmentBookedNotificationSent;
    }

    @Column(name = "appointment_registered_notification_sent")
    public boolean isAppointmentRegisteredNotificationSent() {
        return appointmentRegisteredNotificationSent;
    }

    public void setAppointmentRegisteredNotificationSent(boolean appointmentRegisteredNotificationSent) {
        this.appointmentRegisteredNotificationSent = appointmentRegisteredNotificationSent;
    }

    @Column(name = "customer_comment")
    public String getCustomerComment() {
        return customerComment;
    }

    public void setCustomerComment(String customerComment) {
        this.customerComment = customerComment;
    }

    @Column(name = "customer_voice")
    public String getCustomerVoice() {
        return customerVoice;
    }

    public void setCustomerVoice(String customerVoice) {
        this.customerVoice = customerVoice;
    }
    
    @Column(name = "booked_email_sent")
    public boolean isBookedEmailSent() {
        return bookedEmailSent;
    }

    public void setBookedEmailSent(boolean bookedEmailSent) {
        this.bookedEmailSent = bookedEmailSent;
    }
    
        @Column(name = "financing_id")
    public Long getFinancingId() {
        return financingId;
    }

    public void setFinancingId(Long financingId) {
        this.financingId = financingId;
    }

    @Column(name = "financing_email_sent")
    public boolean isFinancingEmailSent() {
        return financingEmailSent;
    }

    public void setFinancingEmailSent(boolean financingEmailSent) {
        this.financingEmailSent = financingEmailSent;
    }

    @Transient
    public SaleCheckpointDTO getDTO() {
	SaleCheckpointDTO saleCheckpointDTO = new SaleCheckpointDTO();
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	saleCheckpointDTO.setId(this.id);
	saleCheckpointDTO.setIdUser(this.userId);
	saleCheckpointDTO.setCarId(this.carId);
	saleCheckpointDTO.setSku(this.sku);
	saleCheckpointDTO.setStatus(this.status);
	saleCheckpointDTO.setCarYear(this.carYear);
	saleCheckpointDTO.setCarMake(this.carMake);
	saleCheckpointDTO.setCarModel(this.carModel);
	saleCheckpointDTO.setCarVersion(this.carVersion);
	saleCheckpointDTO.setCarKm(this.carKm);
	saleCheckpointDTO.setZipCode(this.zipCode);
	saleCheckpointDTO.setShippingAddressId(this.shippingAddressId);
	saleCheckpointDTO.setShippingAddressDescription(this.shippingAddressDescription);
	saleCheckpointDTO.setWarrantyName(this.warrantyName);
	saleCheckpointDTO.setWarrantyTime(this.warrantyTime);
	saleCheckpointDTO.setWarrantyAmount(this.warrantyAmount);
	saleCheckpointDTO.setWarrantyRange(this.warrantyRange);
	saleCheckpointDTO.setInsuranceName(this.insuranceName);
	saleCheckpointDTO.setInsuranceTime(this.insuranceTime);
	saleCheckpointDTO.setInsuranceAmount(this.insuranceAmount);
	saleCheckpointDTO.setInsuranceModality(this.insuranceModality);
	saleCheckpointDTO.setPaymentMethodTypeId(this.typePaymentMethodTypeId);
	saleCheckpointDTO.setIsFinanced(this.isFinanced);
	saleCheckpointDTO.setIsFinancingCompleted(this.isFinancingCompleted);
	saleCheckpointDTO.setDownPayment(this.downPayment);
	saleCheckpointDTO.setAmountFinancing(this.amountFinancing);
	saleCheckpointDTO.setMonthFinancing(this.monthFinancing);
	saleCheckpointDTO.setCheckoutResult(this.checkoutResult);
	saleCheckpointDTO.setPaymentPlatform(this.paymentPlatform);
	saleCheckpointDTO.setPaymentPlatformId(this.paymentPlatformId);
	saleCheckpointDTO.setTransactionNumber(this.transactionNumber);
	saleCheckpointDTO.setPaymentStatus(this.paymentStatus);
	saleCheckpointDTO.setCarCost(this.carCost);
	saleCheckpointDTO.setTotalAmount(this.totalAmount);
	saleCheckpointDTO.setCarReserve(this.carReserve);
	saleCheckpointDTO.setCarShippingCost(this.carShippingCost);
	saleCheckpointDTO.setSaleOpportunityTypeId(this.saleOpportunityTypeId);
	saleCheckpointDTO.setScheduleDateId(this.scheduleDateId);
	saleCheckpointDTO.setDuplicatedOfferControl(this.duplicatedOfferControl);
	saleCheckpointDTO.setAppointmentContact(this.appointmentContact);
	saleCheckpointDTO.setWatchedCar(this.watchedCar);
	saleCheckpointDTO.setComments(this.comments);
	saleCheckpointDTO.setOtherInterestedCar(this.otherInterestedCar);
	saleCheckpointDTO.setAppointmentDate(this.appointmentDate);
	saleCheckpointDTO.setNetsuiteOpportunityId(this.netsuiteOpportunityId);
	saleCheckpointDTO.setNetsuiteOpportunityURL(this.netsuiteOpportunityURL);
	saleCheckpointDTO.setNetsuiteIdUpdateDate(this.netsuiteIdUpdateDate);
	saleCheckpointDTO.setCustomerName(this.customerName);
	saleCheckpointDTO.setCustomerLastName(this.customerLastName);
	saleCheckpointDTO.setAppliedCoupon(this.appliedCoupon);
	saleCheckpointDTO.setInternalUserId(this.internalUserId);
	saleCheckpointDTO.setSource(this.source);
	
	if (this.registerDate != null) {
	    Date tDate = new Date(this.registerDate.getTime());
	    String registerDate = formatter.format(tDate);
	    saleCheckpointDTO.setRegisterDate(registerDate);
	}

	if (this.updateDate != null) {
	    Date uDate = new Date(this.updateDate.getTime());
	    String updateDate = formatter.format(uDate);
	    saleCheckpointDTO.setUpdateDate(updateDate);
	}

	return saleCheckpointDTO;
    }


}
