package com.kavak.core.model;

import com.kavak.core.dto.InspectionScheduleDTO;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
    @Table(name="horarios_inspeccion")
public class InspectionSchedule {

    private Long id;
    private Date inspectionDate;
    private Long idHourBlock;
    private boolean status;
    private Timestamp selectionDate;
    private Long inspectionLocationId;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="fecha_inspeccion")
    public Date getInspectionDate() {
        return inspectionDate;
    }
    public void setInspectionDate(Date inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    @Column(name="bloque_hora")
    public Long getIdHourBlock() {
        return idHourBlock;
    }
    public void setIdHourBlock(Long idHourBlock) {
        this.idHourBlock = idHourBlock;
    }

    @Column(name="estatus")
    public boolean isStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }

    @Column(name="fecha_seleccion")
    public Timestamp getSelectionDate() {
        return selectionDate;
    }
    public void setSelectionDate(Timestamp selectionDate) {
        this.selectionDate = selectionDate;
    }
    
	@Column(name ="inspection_location")
	public Long getInspectionLocationId() {
		return inspectionLocationId;
	}
	public void setInspectionLocationId(Long inspectionLocationId) {
		this.inspectionLocationId = inspectionLocationId;
	}
    
	@Transient
    public InspectionScheduleDTO getDTO(){
		InspectionScheduleDTO inspectionScheduleDTO = new InspectionScheduleDTO();
		inspectionScheduleDTO.setId(this.id);
		inspectionScheduleDTO.setInspectionDate(this.inspectionDate);
		inspectionScheduleDTO.setIdHourBlock(this.idHourBlock);
		inspectionScheduleDTO.setStatus(this.status);
		inspectionScheduleDTO.setSelectionDate(this.selectionDate);;
		inspectionScheduleDTO.setInspectionLocationId(this.inspectionLocationId);
		return inspectionScheduleDTO;
    }
 
}
