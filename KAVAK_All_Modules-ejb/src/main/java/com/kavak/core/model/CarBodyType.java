package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

/**
 * Created by Enrique on 22-Jun-17.
 */
@Entity
@Table(name = "v2_car_bodytype")
public class CarBodyType {

	private Long id;
	private String name;
	private String image;
	private boolean active;
	private Date creatiomDate;
	private List<CarscoutCatalogue> carscoutCatalogue;

	@Id
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "image")
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Column(name = "active")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(name = "creation_date")
	public Date getCreatiomDate() {
		return creatiomDate;
	}

	public void setCreatiomDate(Date creatiomDate) {
		this.creatiomDate = creatiomDate;
	}

	@OneToMany(mappedBy = "carBodyType")
	public List<CarscoutCatalogue> getCarscoutCatalogue() {
		return carscoutCatalogue;
	}

	public void setCarscoutCatalogue(List<CarscoutCatalogue> carscoutCatalogue) {
		this.carscoutCatalogue = carscoutCatalogue;
	}

}
