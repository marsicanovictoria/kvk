package com.kavak.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "v2_checkout_log")
public class CheckoutLog {

    private Long id;
    private Integer step;      
    private Long userId;     
    private Long carId;      
    private Long paymentType;    
    private Long checkpointId;   
    private Long deliveryAddressId;   
    private Long shipmentCost;   
    private Long warrantyId;    
    private Long warrantyAmount;   
    private Long insuranceId;   
    private Long insuranceAmount;  
    private String deviceSessionId;    
    private String tokenId;    
    private String carPrice;     
    private String totalPrice;    
    private Integer downPayment;     
    private Integer financingMonths;   
    private Long offerCheckpointId;  
    private String source;
    private String versionApp;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="step")
    public Integer getStep() {
        return step;
    }
    public void setStep(Integer step) {
        this.step = step;
    }

    @Column(name="user_id")
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Column(name="car_id")
    public Long getCarId() {
        return carId;
    }
    public void setCarId(Long carId) {
        this.carId = carId;
    }

    @Column(name="payment_type")
    public Long getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(Long paymentType) {
        this.paymentType = paymentType;
    }

    @Column(name="checkpoint_id")
    public Long getCheckpointId() {
        return checkpointId;
    }
    public void setCheckpointId(Long checkpointId) {
        this.checkpointId = checkpointId;
    }

    @Column(name="delivery_address_id")
    public Long getDeliveryAddressId() {
        return deliveryAddressId;
    }
    public void setDeliveryAddressId(Long deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    @Column(name="shipment_cost")
    public Long getShipmentCost() {
        return shipmentCost;
    }
    public void setShipmentCost(Long shipmentCost) {
        this.shipmentCost = shipmentCost;
    }

    @Column(name="warranty_id")
    public Long getWarrantyId() {
        return warrantyId;
    }
    public void setWarrantyId(Long warrantyId) {
        this.warrantyId = warrantyId;
    }

    @Column(name="warranty_amount")
    public Long getWarrantyAmount() {
        return warrantyAmount;
    }
    public void setWarrantyAmount(Long warrantyAmount) {
        this.warrantyAmount = warrantyAmount;
    }

    @Column(name="insurance_id")
    public Long getInsuranceId() {
        return insuranceId;
    }
    public void setInsuranceId(Long insuranceId) {
        this.insuranceId = insuranceId;
    }

    @Column(name="insurance_amount")
    public Long getInsuranceAmount() {
        return insuranceAmount;
    }
    public void setInsuranceAmount(Long insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    @Column(name="device_session_id")
    public String getDeviceSessionId() {
        return deviceSessionId;
    }
    public void setDeviceSessionId(String deviceSessionId) {
        this.deviceSessionId = deviceSessionId;
    }

    @Column(name="token_id")
    public String getTokenId() {
        return tokenId;
    }
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    @Column(name="car_price")
    public String getCarPrice() {
        return carPrice;
    }
    public void setCarPrice(String carPrice) {
        this.carPrice = carPrice;
    }

    @Column(name="total_price")
    public String getTotalPrice() {
        return totalPrice;
    }
    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Column(name="down_payment")
    public Integer getDownPayment() {
        return downPayment;
    }
    public void setDownPayment(Integer downPayment) {
        this.downPayment = downPayment;
    }

    @Column(name="financing_months")
    public Integer getFinancingMonths() {
        return financingMonths;
    }
    public void setFinancingMonths(Integer financingMonths) {
        this.financingMonths = financingMonths;
    }

    @Column(name="offer_checkpoint_id")
    public Long getOfferCheckpointId() {
        return offerCheckpointId;
    }
    public void setOfferCheckpointId(Long offerCheckpointId) {
        this.offerCheckpointId = offerCheckpointId;
    }
    
    @Column(name="source")
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    @Column(name="version_app")
    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }
}
