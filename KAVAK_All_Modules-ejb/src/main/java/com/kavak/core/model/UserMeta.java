package com.kavak.core.model;

import javax.persistence.*;

@Entity
@Table(name="user_meta")
public class UserMeta {

	private Long id;
	private Long userId;
	private String metaKey;
	private String metaValue;
	private User user; // Campo solo para la relacion ManyToOne con @User
	
	@Id
	@Column(name="meta_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="user_id", insertable= false, updatable=false)
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Column(name="meta_key")
	public String getMetaKey() {
		return metaKey;
	}
	public void setMetaKey(String metaKey) {
		this.metaKey = metaKey;
	}
	@Column(name="meta_value")
	public String getMetaValue() {
		return metaValue;
	}
	public void setMetaValue(String metaValue) {
		this.metaValue = metaValue;
	}
	@ManyToOne
	@JoinColumn(name="user_id")
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
