package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "v2_car_meta_accesories")
public class CarMetaAccesories {

	private Long id;
	private String description;
	private CarscoutCategory carscoutCategory;
	private Timestamp creationDate;
	private Boolean active;

	@Id
	@Column(name="accesories_id")
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne
	@JoinColumn(name = "category_id")
	public CarscoutCategory getCarscoutCategory() {
		return carscoutCategory;
	}

	public void setCarscoutCategory(CarscoutCategory carscoutCategory) {
		this.carscoutCategory = carscoutCategory;
	}

	@Column(name="creation_date")
	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name="active_ind")
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
