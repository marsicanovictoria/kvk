package com.kavak.core.model;

import com.kavak.core.dto.RepairTypeDTO;

import javax.persistence.*;

@Entity
@Table(name="v2_repair_item_type")
public class RepairType {
	
	private Long id;
	private String name;
	private Long repairAmount; 
	private RepairCategory repairCategory; // Campo Agregad solo por relacion ManyToOne RepairCategory
	private RepairSubCategory repairSubCategory; // Campo Agregad solo por relacion ManyToOne RepairSubCategory
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@ManyToOne
	@JoinColumn(name="category_id")
	public RepairCategory getRepairCategory() {
		return repairCategory;
	}
	public void setRepairCategory(RepairCategory repairCategory) {
		this.repairCategory = repairCategory;
	}
	@ManyToOne
	@JoinColumn(name="subcategory_id")
	public RepairSubCategory getRepairSubCategory() {
		return repairSubCategory;
	}
	public void setRepairSubCategory(RepairSubCategory repairSubCategory) {
		this.repairSubCategory = repairSubCategory;
	}	
	@Column(name="repair_amount")
	public Long getRepairAmount() {
		return repairAmount;
	}
	public void setRepairAmount(Long repairAmount) {
		this.repairAmount = repairAmount;
	}
	
	@Transient
	public RepairTypeDTO getDTO(){
		RepairTypeDTO repairTypeDTO = new RepairTypeDTO();
		repairTypeDTO.setId(this.id);
		repairTypeDTO.setName(this.name);
		repairTypeDTO.setRepairAmount(this.repairAmount);
		return	repairTypeDTO;
	}
	
}