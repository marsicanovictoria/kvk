package com.kavak.core.model;

import com.kavak.core.dto.InspectionScheduleBlockDTO;

import javax.persistence.*;

@Entity
@Table(name="v2_inspection_schedule_block")
public class InspectionScheduleBlock {

	private Long id;
	private Long idHourBlock;
	private String block;
	private String checkpointLabel;
	private String hourFrom;
    private String hourTo;
	private boolean available;
	private boolean active;
	
	@Id
	@Column(name="id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="id_block")
	public Long getIdHourBlock() {
		return idHourBlock;
	}
	public void setIdHourBlock(Long idHourBlock) {
		this.idHourBlock = idHourBlock;
	}

	@Column(name="block")
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}

    @Column(name="checkpoint_label")
    public String getCheckpointLabel() {
        return checkpointLabel;
    }
    public void setCheckpointLabel(String checkpointLabel) {
        this.checkpointLabel = checkpointLabel;
    }

	@Column(name="is_available")
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}

	@Column(name="is_active")
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(name="hour_from")
    public String getHourFrom() {
        return hourFrom;
    }
    public void setHourFrom(String hourFrom) {
        this.hourFrom = hourFrom;
    }

    @Column(name="hour_to")
    public String getHourTo() {
        return hourTo;
    }
    public void setHourTo(String hourTo) {
        this.hourTo = hourTo;
    }

    @Transient
	public InspectionScheduleBlockDTO getDTO(){
		InspectionScheduleBlockDTO inspectionScheduleBlockDTO = new InspectionScheduleBlockDTO();
		inspectionScheduleBlockDTO.setId(this.id);
		inspectionScheduleBlockDTO.setBlock(this.block);
        inspectionScheduleBlockDTO.setCheckpointLabel(this.checkpointLabel);
		inspectionScheduleBlockDTO.setIdHourBlock(this.idHourBlock);
		inspectionScheduleBlockDTO.setAvailable(this.available);
		inspectionScheduleBlockDTO.setActive(this.active);
		return inspectionScheduleBlockDTO;
	}
}

