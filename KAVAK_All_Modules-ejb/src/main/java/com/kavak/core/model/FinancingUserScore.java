package com.kavak.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "v2_financing_user_score")
public class FinancingUserScore {

    private Long id;
    private Long financingUserId;
    private String code;
    private String score;
    private String scoreText;
    private String verticalType;
    private String maximumFinancingAmount;
    private String preapprovedAmount;
    private String financiersCount;
    private String minInterestRate;
    private String maxInterestRate;
    private String monthlyPayment;
    private String nextUrl;
    private String userFullName;
    private String financingUserScoreId;
    private String type;
    private Timestamp creationDate;
    private Long carId;
    private boolean scoreEmailSent;


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Column(name = "financing_user_id")
    public Long getFinancingUserId() {
        return financingUserId;
    }

    public void setFinancingUserId(Long financingUserId) {
        this.financingUserId = financingUserId;
    }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "score")
    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Column(name = "score_text")
    public String getScoreText() {
        return scoreText;
    }

    public void setScoreText(String scoreText) {
        this.scoreText = scoreText;
    }

    @Column(name = "vertical_type")
    public String getVerticalType() {
        return verticalType;
    }

    public void setVerticalType(String verticalType) {
        this.verticalType = verticalType;
    }

    @Column(name = "maximum_financing_amount")
    public String getMaximumFinancingAmount() {
        return maximumFinancingAmount;
    }

    public void setMaximumFinancingAmount(String maximumFinancingAmount) {
        this.maximumFinancingAmount = maximumFinancingAmount;
    }

    @Column(name = "preapproved_amount")
    public String getPreapprovedAmount() {
        return preapprovedAmount;
    }

    public void setPreapprovedAmount(String preapprovedAmount) {
        this.preapprovedAmount = preapprovedAmount;
    }

    @Column(name = "financiers_count")
    public String getFinanciersCount() {
        return financiersCount;
    }

    public void setFinanciersCount(String financiersCount) {
        this.financiersCount = financiersCount;
    }

    @Column(name = "min_interest_rate")
    public String getMinInterestRate() {
        return minInterestRate;
    }

    public void setMinInterestRate(String minInterestRate) {
        this.minInterestRate = minInterestRate;
    }

    @Column(name = "max_interest_rate")
    public String getMaxInterestRate() {
        return maxInterestRate;
    }

    public void setMaxInterestRate(String maxInterestRate) {
        this.maxInterestRate = maxInterestRate;
    }

    @Column(name = "monthly_payment")
    public String getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(String monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    @Column(name = "next_url")
    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    @Column(name = "user_full_name")
    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    @Column(name = "creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "financing_user_score_id")
    public String getFinancingUserScoreId() {
        return financingUserScoreId;
    }

    public void setFinancingUserScoreId(String financingUserScoreId) {
        this.financingUserScoreId = financingUserScoreId;
    }
    
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "stock_id")
    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    @Column(name = "score_email_sent")
    public boolean isScoreEmailSent() {
        return scoreEmailSent;
    }

    public void setScoreEmailSent(boolean scoreEmailSent) {
        this.scoreEmailSent = scoreEmailSent;
    }


}