package com.kavak.core.model;

import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Table(name = "v2_carscout_notify")
public class CarscoutNotify {

	private Long id;
	private CarscoutAlert carscoutAlert;
	private CarscoutCatalogue carscoutCatalogue;
	private Timestamp startShowDate;
	private Timestamp interesteDate;
	private Long interestedInd;
	private Integer showCount;

	@Id
	@Column(name = "id_carscout_notify")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "alert_id")
	public CarscoutAlert getCarscoutAlert() {
		return carscoutAlert;
	}

	public void setCarscoutAlert(CarscoutAlert carscoutAlert) {
		this.carscoutAlert = carscoutAlert;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "catalogue_id")
	public CarscoutCatalogue getCarscoutCatalogue() {
		return carscoutCatalogue;
	}

	public void setCarscoutCatalogue(CarscoutCatalogue carscoutCatalogue) {
		this.carscoutCatalogue = carscoutCatalogue;
	}

	@Column(name = "start_show_date")
	public Timestamp getStartShowDate() {
		return startShowDate;
	}

	public void setStartShowDate(Timestamp startShowDate) {
		this.startShowDate = startShowDate;
	}

	@Column(name = "interested_date")
	public Timestamp getInteresteDate() {
		return interesteDate;
	}

	public void setInteresteDate(Timestamp interesteDate) {
		this.interesteDate = interesteDate;
	}

	@Column(name = "interested_ind")
	public Long getInterestedInd() {
		return interestedInd;
	}

	public void setInterestedInd(Long interestedInd) {
		this.interestedInd = interestedInd;
	}

	@Column(name = "show_count")
	public Integer getShowCount() {
		return showCount;
	}

	public void setShowCount(Integer showCount) {
		this.showCount = showCount;
	}

}
