package com.kavak.core.model;

import com.kavak.core.dto.InspectionDiscountTypeDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="v2_inspection_discount_type")
public class InspectionDiscountType {

    private Long id;
    private String name;
    private boolean active;
    private Date creationDate;

    @Id
    @Column(name="id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name="is_active")
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name="creation_date")
    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Transient
    public InspectionDiscountTypeDTO getDTO(){
        InspectionDiscountTypeDTO inspectionDiscountTypeDTO = new InspectionDiscountTypeDTO();
        inspectionDiscountTypeDTO.setId(this.id);
        inspectionDiscountTypeDTO.setName(this.name);
        return inspectionDiscountTypeDTO;
    }
}
