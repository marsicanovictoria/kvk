package com.kavak.core.model;

import javax.persistence.*;

/**
 * Created by Enrique on 05-Jun-17.
 */

@Entity
@Table(name = "puntos_inspeccion_items")
public class ApoloInspectionPointsItem {

    private Long id;
    private String name;
    private boolean active;
    private boolean deselect;
    //private ApoloInspectionPointCar apoloInspectionPointCar;
    private ApoloInspectionPointGroup apoloInspectionPointGroup;

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "activo")
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name = "desmarcar")
    public boolean isDeselect() {
        return deselect;
    }
    public void setDeselect(boolean deselect) {
        this.deselect = deselect;
    }

    @Column(name = "nombre")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    //@OneToMany(mappedBy = "apoloInspectionPointsItem")
//    public ApoloInspectionPointCar getApoloInspectionPointCar() {
//        return apoloInspectionPointCar;
//    }
//    public void setApoloInspectionPointCar(ApoloInspectionPointCar apoloInspectionPointCar) {
//        this.apoloInspectionPointCar = apoloInspectionPointCar;
//    }

    @ManyToOne
    @JoinColumn(name = "puntos_inspeccion_grupos_id")
    public ApoloInspectionPointGroup getApoloInspectionPointGroup() {
        return apoloInspectionPointGroup;
    }
    public void setApoloInspectionPointGroup(ApoloInspectionPointGroup apoloInspectionPointGroup) {
        this.apoloInspectionPointGroup = apoloInspectionPointGroup;
    }
}
