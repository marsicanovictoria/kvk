package com.kavak.core.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "v2_carscout_catalogue")
public class CarscoutCatalogue {

	private Long id;
	private Long source;
	private String itemId;
	private CarData carData;
	private CarMake carMake;
	private CarModel carModel;
	private CarVersion carVersion;
	private Long yearId;
	private CarBodyType carBodyType;
	private Long transmisionId;
	private Integer km;
	private Integer salePrice;
	private Long colorId;
	private Long activeInd;
	private Long priorityId;
	private Long stockId;
	private Date createDate;
	private String carStatus;
	private String possibility;
	private String probability;
	private Date lastUpdate;
	private List<CarscoutNotify> carscoutNotify;
	private List<CarscoutAlertLkpSku> carscoutAlertLkpSkus;

	@Id
	@Column(name = "catalogue_id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "source")
	public Long getSource() {
		return source;
	}

	public void setSource(Long source) {
		this.source = source;
	}

	@Column(name = "item_id")
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	@ManyToOne
	@JoinColumn(name = "sku_id", referencedColumnName = "id")
	public CarData getCarData() {
		return carData;
	}

	public void setCarData(CarData carData) {
		this.carData = carData;
	}

	@ManyToOne
	@JoinColumn(name = "make_id", referencedColumnName = "id")
	public CarMake getCarMake() {
		return carMake;
	}

	public void setCarMake(CarMake carMake) {
		this.carMake = carMake;
	}

	@ManyToOne
	@JoinColumn(name = "model_id")
	public CarModel getCarModel() {
		return carModel;
	}

	public void setCarModel(CarModel carModel) {
		this.carModel = carModel;
	}

	@ManyToOne
	@JoinColumn(name = "version_id")
	public CarVersion getCarVersion() {
		return carVersion;
	}

	public void setCarVersion(CarVersion carVersion) {
		this.carVersion = carVersion;
	}

	@Column(name = "year_id")
	public Long getYearId() {
		return yearId;
	}

	public void setYearId(Long yearId) {
		this.yearId = yearId;
	}

	@ManyToOne
	@JoinColumn(name = "style_id", referencedColumnName = "id")
	public CarBodyType getCarBodyType() {
		return carBodyType;
	}

	public void setCarBodyType(CarBodyType carBodyType) {
		this.carBodyType = carBodyType;
	}

	@Column(name = "transmission_id")
	public Long getTransmisionId() {
		return transmisionId;
	}

	public void setTransmisionId(Long transmisionId) {
		this.transmisionId = transmisionId;
	}

	@Column(name = "km")
	public Integer getKm() {
		return km;
	}

	public void setKm(Integer km) {
		this.km = km;
	}

	@Column(name = "sale_price")
	public Integer getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Integer salePrice) {
		this.salePrice = salePrice;
	}

	@Column(name = "color_id")
	public Long getColorId() {
		return colorId;
	}

	public void setColorId(Long colorId) {
		this.colorId = colorId;
	}

	@Column(name = "active_ind")
	public Long getActiveInd() {
		return activeInd;
	}

	public void setActiveInd(Long activeInd) {
		this.activeInd = activeInd;
	}

	@Column(name = "priority_id")
	public Long getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}

	@Column(name = "stock_id")
	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	@Column(name = "create_date")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "car_status")
	public String getCarStatus() {
		return carStatus;
	}

	public void setCarStatus(String carStatus) {
		this.carStatus = carStatus;
	}

	@Column(name = "possibility")
	public String getPossibility() {
		return possibility;
	}

	public void setPossibility(String possibility) {
		this.possibility = possibility;
	}

	@Column(name = "probability")
	public String getProbability() {
		return probability;
	}

	public void setProbability(String probability) {
		this.probability = probability;
	}

	@Column(name = "last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy = "carscoutCatalogue")
	public List<CarscoutNotify> getCarscoutNotify() {
		return carscoutNotify;
	}

	public void setCarscoutNotify(List<CarscoutNotify> carscoutNotify) {
		this.carscoutNotify = carscoutNotify;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy = "carscoutCatalogue")
	public List<CarscoutAlertLkpSku> getCarscoutAlertLkpSkus() {
		return carscoutAlertLkpSkus;
	}

	public void setCarscoutAlertLkpSkus(List<CarscoutAlertLkpSku> carscoutAlertLkpSkus) {
		this.carscoutAlertLkpSkus = carscoutAlertLkpSkus;
	}
}
