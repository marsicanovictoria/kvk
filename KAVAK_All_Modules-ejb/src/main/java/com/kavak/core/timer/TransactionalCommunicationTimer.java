package com.kavak.core.timer;

import java.text.ParseException;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.mail.MessagingException;

import org.apache.commons.configuration.SystemConfiguration;

import com.kavak.core.service.SellCarServices;
import com.kavak.core.service.StagePurchaseServices;
import com.kavak.core.service.TransactionalEmailsServices;
import com.kavak.core.service.TransactionalSmsServices;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;

@Singleton(mappedName = "TransactionalCommunicationTimer")
@LocalBean
@Startup
public class TransactionalCommunicationTimer {

    @Resource
    private TimerService timerService;

    @EJB(mappedName = LookUpNames.SESSION_TransactionalEmailsServices)
    private TransactionalEmailsServices transactionalEmailsServices;

    @EJB(mappedName = LookUpNames.SESSION_SellCarServices)
    private SellCarServices sellCarServices;
    
    @EJB(mappedName = LookUpNames.SESSION_TransactionalSmsServices)
    private TransactionalSmsServices transactionalSmsServices;
    
    @EJB(mappedName = LookUpNames.SESSION_StagePurchaseOpportunities)
    private StagePurchaseServices stagePurchaseServices;
    
      

    public final String TIMER_NAME = this.getClass().getSimpleName();
    public final String INIT_TIMER = "com.kavak.core.timer.TransactionalCommunicationTimer.INIT_TIMER";
    public final String INTERVAL_MINUTES = "com.kavak.core.timer.TransactionalCommunicationTimer.INTERVAL_MINUTES";

    private static final SystemConfiguration systemConfiguration = new SystemConfiguration();

    public boolean active;

    public long interval;
    public long intervalMinutes;

    public TransactionalCommunicationTimer() throws ParseException {
	LogService.logger.debug("INIT_TIMER [" + systemConfiguration.getString(INIT_TIMER) + "].");
	active = Boolean.valueOf((String) systemConfiguration.getString(INIT_TIMER));
	LogService.logger.debug("INTERVAL_MINUTES [" + systemConfiguration.getString(INTERVAL_MINUTES) + "].");
	intervalMinutes = Long.parseLong((String) systemConfiguration.getString(INTERVAL_MINUTES));
	interval = intervalMinutes * 60000; // intervalMinutes minutos 60000 = 1
					    // minuto

    }

    /**
     * Método que se ejecuta al instanciar el Timer para configurar o detener su
     * ejecución.
     */
    @PostConstruct
    public void onStartup() {
	LogService.logger.info("Timer [" + TIMER_NAME + "] - ACTIVAR [" + isActive() + "].");
	removeTimer();
	if (isActive()) {
	    setTimer();
	}
    }

    /**
     * Método que establece la programación del Timer
     */
    public Timer setTimer() {
	LogService.logger.info("Programando Timer [" + TIMER_NAME + "] ");
	Calendar calendarTimeout = Calendar.getInstance();

	if (calendarTimeout.before(Calendar.getInstance())) {
	    calendarTimeout.add(Calendar.DATE, 1);
	}
	Timer timer = timerService.createTimer(calendarTimeout.getTime(), interval, TIMER_NAME);
	LogService.logger.info("Se ha programado el Timer [" + TIMER_NAME + "] para ejecutarse cada " + intervalMinutes + " minutos. Proxima ejecucion [" + String.valueOf(timer.getNextTimeout()) + "]");
	return timer;
    }

    @Timeout
    public void scheduledTimeout(Timer timer) throws MessagingException {
	LogService.logger.info("TIMEOUT [" + TIMER_NAME + "] ");

	// Etapa 5
	transactionalEmailsServices.offerCheckpointFifthStageConsignmentCompletedRegister();
	transactionalEmailsServices.offerCheckpointFifthStageConsignmentNOSelectedDayAndTimeCompletedRegister();
	transactionalEmailsServices.offerCheckpointFifthStageConsignmentNoWLCompletedRegisterAttachedTC();
	transactionalEmailsServices.offerCheckpointFifthStageConsignmentNoWlNOSelectedDayAndTimeCompletedRegister();

	// Etapa 4
	transactionalEmailsServices.offerCheckpointFourthStageConsignmentNotAtachmentTC();
	transactionalEmailsServices.offerCheckpointFourthStageConsignmentNotWlNotAtachmentTC();
	transactionalEmailsServices.offerCheckpointFourthStageConsignmentNotWlNotDayAndTimeNotAtachmentTC();
	transactionalEmailsServices.offerCheckpointFourthStageConsignmentNotDayAndTimeNotAtachmentTC();

	// Etapa 3
	transactionalEmailsServices.offerCheckpointThirdStageConsignmentNotWlSelectedAddressNotDayAndTime();
	transactionalEmailsServices.offerCheckpointThirdStageConsignmentSelectedAddressNoDayAndTime();

	// Etapa 2
	transactionalEmailsServices.offerCheckpointSecondStageConsignmentAcceptedOffersNotAddress();
	transactionalEmailsServices.offerCheckpointSecondStageConsignmentNotWlAcceptedOffersNotAddress();

	// Etapa 1
	transactionalEmailsServices.offerCheckpointFirstStageSectionNotRequirements();
	transactionalEmailsServices.offerCheckpointFirstStageConsignmentRegisterNotAcceptedOffer();
	transactionalEmailsServices.offerCheckpointFirstStageConsignmentNotWlRegisterNotAcceptedOffer();
	
	//checkoutPaymentError
	transactionalEmailsServices.checkoutPaymentError();
	
	//availableCarsNotification
	transactionalEmailsServices.availableCarsNotification();
	
	//offerCheckpointFourthAndFifthStageFollowUp
	transactionalEmailsServices.offerCheckpointFourthAndFifthStageFollowUp();
	
	//saleCustomerExperiencie
	transactionalEmailsServices.saleCustomerExperiencie();
	
	//newOfferWhislist
	transactionalEmailsServices.newOfferWhislist();
	
	//newOfferNotWhislist
	transactionalEmailsServices.newOfferNotWhislist();
	
	//appointmentBooked
	transactionalEmailsServices.appointmentBooked();
	
	//appointmentNotBooked
	transactionalEmailsServices.appointmentNotBooked();
	
	//carscoutEmailSent
	transactionalEmailsServices.carscoutEmailSent();
	
	//financingScoreEmailSent
	transactionalEmailsServices.financingScoreEmailSent();
	
	//reserva
	transactionalEmailsServices.sendBookedEmail();
	
	//contactFormSubmitEmailSent
	transactionalEmailsServices.contactFormSubmitEmailSent();
	
	//newOfferAuditWhislist
	transactionalEmailsServices.newOfferAuditWhislist();
	
	//newOfferAuditNotWhislist
	transactionalEmailsServices.newOfferAuditNotWhislist();
	
	//SMS
	
	//bookedCarSms  (cuando se realiza exitosamente una reserva)
	transactionalSmsServices.bookedCarSms();
	//carNotificationBookedSms (Registrar en tabla de SMS postCarNotification, cuando el notification_type = 'AUTO_RESERVADO')
	transactionalSmsServices.carNotificationBookedSms();
	//cancelledBookedCarSms (Al cancelarse una reserva)
	transactionalSmsServices.cancelledBookedCarSms();
	//scheduledAppointment (Generar registro en tabla de SMS cuando se agenda la cita)
	transactionalSmsServices.scheduledAppointment();
	// selectDayAndPlaceCustomer SMS C-204 Etapa 4 y 5, Selecciona día, hora y lugar (cliente)
	transactionalSmsServices.selectDayAndHourAndPlaceCustomer();  //LISTO
	//selectDayAndPlaceCenter  SMS C-204 Etapa 4 y 5, Selecciona día, hora y lugar (Centro)
	transactionalSmsServices.selectDayAndHourAndPlaceCenter();   //LISTO
	//userRegister  SMS G-200 Se registra usuario en KAVAK
	transactionalSmsServices.userRegister(); //LISTO
	//se libera oferta de auditoria
	transactionalSmsServices.releaseAuditOffer();
	//Acepta oferta pero no continua
	transactionalSmsServices.acceptOfferNotContinue();
	
	//stagePurchaseServices
	stagePurchaseServices.getFirstStagePurchaseOpportunities();
	stagePurchaseServices.getSecondStagePurchaseOpportunities();
	stagePurchaseServices.getThirdStagePurchaseOpportunities();
	stagePurchaseServices.getFourthStagePurchaseOpportunities();
	stagePurchaseServices.getFifthStagePurchaseOpportunities();
	stagePurchaseServices.getIncompleteFifthStagePurchaseOpportunities();
	
	LogService.logger.info("Finaliza [" + TIMER_NAME + "]. Proxima ejecucion del Timer [" + TIMER_NAME + "]: " + String.valueOf(timer.getNextTimeout()));
    }

    public void removeTimer() {
	LogService.logger.debug("removeTimer :: ACTIVO [ " + isActive() + "]");
	if (timerService.getTimers() != null) {
	    for (Timer timer : timerService.getTimers()) {
		String timerInfo = (String) timer.getInfo();
		if (TIMER_NAME.equalsIgnoreCase(timerInfo)) {
		    LogService.logger.info("Timer encontrado: " + timerInfo);
		    timer.cancel();
		    LogService.logger.info("Se ha cancelado el Timer: " + timerInfo);
		}
	    }
	}
    }

    @PreDestroy
    public void onShutdown() {
	removeTimer();
    }

    public boolean isActive() {
	return active;
    }
}
