package com.kavak.core.timer;

import com.kavak.core.service.SellCarServices;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;
import org.apache.commons.configuration.SystemConfiguration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import java.text.ParseException;
import java.util.Calendar;

@Singleton(mappedName = "ConfirmSaleCarBookingTimer")
@LocalBean
@Startup
public class ConfirmSaleCarBookingTimer {

	@Resource
	private TimerService timerService;

	@EJB(mappedName = LookUpNames.SESSION_SellCarServices)
	private SellCarServices sellCarServices;

	public final String TIMER_NAME = this.getClass().getSimpleName();
	public final String INIT_TIMER = "com.kavak.core.timer.ConfirmSaleCarBookingTimer.INIT_TIMER";
	public final String TIMEOUT_HOUR = "com.kavak.core.timer.ConfirmSaleCarBookingTimer.TIMEOUT_HOUR";
	public final String TIMEOUT_MINUTE = "com.kavak.core.timer.ConfirmSaleCarBookingTimer.TIMEOUT_MINUTE";
	public final String INTERVAL_HOURS = "com.kavak.core.timer.ConfirmSaleCarBookingTimer.INTERVAL_HOURS";

	private static final SystemConfiguration systemConfiguration = new SystemConfiguration();

	public boolean active;

	public long interval;
	public long intervalHours;

	public ConfirmSaleCarBookingTimer() throws ParseException {
		LogService.logger.debug("INIT_TIMER [" + systemConfiguration.getString(INIT_TIMER) + "].");
		active = Boolean.valueOf((String) systemConfiguration.getString(INIT_TIMER));
		LogService.logger.debug("INTERVAL_HOUR [" + systemConfiguration.getString(INTERVAL_HOURS) + "].");
		intervalHours = Long.parseLong ((String) systemConfiguration.getString(INTERVAL_HOURS));
		interval = intervalHours * 60 * 60000; // intervalHour horas
	}

	/**
	 * Método que se ejecuta al instanciar el Timer para configurar o detener su
	 * ejecución.
	 */
	@PostConstruct
	public void onStartup() {
		LogService.logger.info("Timer [" + TIMER_NAME + "] - ACTIVAR [" + isActive() + "].");
		removeTimer();
		if (isActive()) {
			setTimer();
		}
	}

	/**
	 * Método que establece la programación del Timer
	 */
	public Timer setTimer() {
		LogService.logger.info("Programando Timer [" + TIMER_NAME + "] ");
		Calendar calendarTimeout = Calendar.getInstance();

		if (calendarTimeout.before(Calendar.getInstance())) {
			calendarTimeout.add(Calendar.DATE, 1);
		}
		Timer timer = timerService.createTimer(calendarTimeout.getTime(), interval, TIMER_NAME);
		LogService.logger.info("Se ha programado el Timer [" + TIMER_NAME + "] para ejecutarse cada " + intervalHours + " horas. Proxima ejecucion [" + String.valueOf(timer.getNextTimeout()) + "]");
		return timer;
	}    
	
	@Timeout
    public void scheduledTimeout(Timer timer) {
		LogService.logger.info("TIMEOUT [" + TIMER_NAME + "] ");
		sellCarServices.confirmSaleCarBookings();
		LogService.logger.info("Finaliza [" + TIMER_NAME + "]. Proxima ejecucion del Timer [" + TIMER_NAME + "]: " + String.valueOf(timer.getNextTimeout()));
    }

	public void removeTimer() {
		LogService.logger.debug("removeTimer :: ACTIVO [ " + isActive() + "]");
		if (timerService.getTimers() != null) {
			for (Timer timer: timerService.getTimers()) {
				String timerInfo = (String) timer.getInfo();
				if (TIMER_NAME.equalsIgnoreCase(timerInfo)) {
					LogService.logger.info("Timer encontrado: " + timerInfo);
					timer.cancel();
					LogService.logger.info("Se ha cancelado el Timer: " + timerInfo);
				}
			}
		}
	}

	@PreDestroy
	public void onShutdown() {
		removeTimer();
	}

	public boolean isActive() {
		return active;
	}
}
