package com.kavak.core.timer;

import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;

import org.apache.commons.configuration.SystemConfiguration;

import com.kavak.core.service.StagePurchaseServices;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;

@Singleton(mappedName = "TimerPurchaseOpportunityStages")
@LocalBean
@Startup
public class PurchaseOpportunityStagesTimer {

	@Resource
	TimerService timerService;
	
	@EJB(mappedName = LookUpNames.SESSION_StagePurchaseOpportunities)
	StagePurchaseServices stagePurchaseServices;
	
	private static SystemConfiguration systemConfiguration = new SystemConfiguration();

	public final String TIMER_NAME = this.getClass().getSimpleName();
	public final String INIT_TIMER = "com.kavak.core.timer.TimerPurchaseOpportunityStages.INIT_TIMER";
	public final String INTERVAL_MINUTES = "com.kavak.core.timer.TimerPurchaseOpportunityStages.INTERVAL_MINUTES";
	
	/**
	 * Indica si se activará o no el Timer
	 */
	private boolean active;
	/**
	 *	Timer que busca cada (intervalMinutes) minutos los registros en oferta_checkpoint en sus 5 diferentes etapas y envía un correo interno con su infomración 
	 */
	private Integer intervalMinutes;
	private long interval;

	public PurchaseOpportunityStagesTimer(){
		active = Boolean.valueOf((String) systemConfiguration.getString(INIT_TIMER));
		LogService.logger.debug("INIT_TimerInspectionSchedule [" + INIT_TIMER + "].");
		intervalMinutes = Integer.parseInt(systemConfiguration.getString(INTERVAL_MINUTES));
		LogService.logger.debug("INTERVAL_MINUTES [" + intervalMinutes + "].");
		interval = intervalMinutes * 60000;
	}
	
	/**
	 *	Metodo para Iniciar o no el Timer
	 */
	@PostConstruct
	public void onStartup() {
		LogService.logger.info("Timer [" + TIMER_NAME + "] - ACTIVAR [" + isActive() + "].");
		removeTimer();

		if (isActive()) {
			setTimer();
		}
	}
	

	/**
	 * Método que establece la programación del Timer
	 */
	public Timer setTimer() {
		LogService.logger.info("Programando Timer [" + TIMER_NAME + "] ");
		Calendar calInitTimeout = Calendar.getInstance();
		Timer timer = timerService.createTimer(calInitTimeout.getTime(), interval, TIMER_NAME);
		LogService.logger.info("Se ha programado el Timer [" + TIMER_NAME + "] para ejecutarse cada "+intervalMinutes+" minutos. Proxima ejecucion [" + String.valueOf(timer.getNextTimeout()) + "]");
		return timer;
	}
	
	/**
	 * Método que se ejecuta al instanciar el Timer para setear o detener su ejecución.
	 */
	@Timeout
	public void scheduledTimeout(Timer timer) {
		LogService.logger.info("TIMEOUT [" + TIMER_NAME + "] ");
		
		stagePurchaseServices.getFirstStagePurchaseOpportunities();
		stagePurchaseServices.getSecondStagePurchaseOpportunities();
		stagePurchaseServices.getThirdStagePurchaseOpportunities();
		stagePurchaseServices.getFourthStagePurchaseOpportunities();
		stagePurchaseServices.getFifthStagePurchaseOpportunities();
		stagePurchaseServices.getIncompleteFifthStagePurchaseOpportunities();
		
		LogService.logger.info("Finaliza [" + TIMER_NAME + "]. Proxima ejecucion del Timer [" + TIMER_NAME + "] : " + String.valueOf(timer.getNextTimeout()));
	}

	public void removeTimer() {
		LogService.logger.info("removeTimer :: ACTIVO [ " + isActive() + "]");
		if (timerService.getTimers() != null) {
			for (Timer timer : timerService.getTimers()) {
				String timerInfo = (String) timer.getInfo();
				if (TIMER_NAME.equalsIgnoreCase(timerInfo)) {
					LogService.logger.info("Timer encontrado: " + timerInfo);
					timer.cancel();
					LogService.logger.info("Se ha cancelado el Timer: " + timerInfo);
				}
			}
		}
	}
	
	@PreDestroy
	public void onShutdown() {
		removeTimer();
	}

	public boolean isActive() {
		return active;
	}
}
