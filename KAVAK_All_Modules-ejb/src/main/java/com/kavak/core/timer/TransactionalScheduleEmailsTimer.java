package com.kavak.core.timer;

import com.kavak.core.service.SellCarServices;
import com.kavak.core.service.TransactionalEmailsServices;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;
import org.apache.commons.configuration.SystemConfiguration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.mail.MessagingException;

import java.text.ParseException;
import java.util.Calendar;

@Singleton(mappedName = "TransactionalScheduleEmailsTimer")
@LocalBean
@Startup
public class TransactionalScheduleEmailsTimer {

    @Resource
    private TimerService timerService;

    @EJB(mappedName = LookUpNames.SESSION_TransactionalEmailsServices)
    private TransactionalEmailsServices transactionalEmailsServices;

    @EJB(mappedName = LookUpNames.SESSION_SellCarServices)
    private SellCarServices sellCarServices;

    public final String TIMER_NAME = this.getClass().getSimpleName();
    public final String INIT_TIMER = "com.kavak.core.timer.TransactionalScheduleEmailsTimer.INIT_TIMER";
    public final String TIMER_HOUR = "com.kavak.core.timer.TransactionalScheduleEmailsTimer.HOUR";

    private static final SystemConfiguration systemConfiguration = new SystemConfiguration();

    public boolean active;

    public long interval;
    public long timerHour;

    public TransactionalScheduleEmailsTimer() throws ParseException {
	LogService.logger.debug("INIT_TIMER [" + systemConfiguration.getString(INIT_TIMER) + "].");
	active = Boolean.valueOf((String) systemConfiguration.getString(INIT_TIMER));
	timerHour = Long.parseLong((String) systemConfiguration.getString(TIMER_HOUR));
	interval =  1 * 24 * 60 * 60000; // interval cada dia

    }

    /**
     * Método que se ejecuta al instanciar el Timer para configurar o detener su
     * ejecución.
     */
    @PostConstruct
    public void onStartup() {
	LogService.logger.info("Timer [" + TIMER_NAME + "] - ACTIVAR [" + isActive() + "].");
	removeTimer();
	if (isActive()) {
	    setTimer();
	}
    }

    /**
     * Método que establece la programación del Timer
     */
    public Timer setTimer() {
	LogService.logger.info("Programando Timer [" + TIMER_NAME + "] ");
	Calendar calendarTimeout = Calendar.getInstance();
	Integer timerHourI = (int) (long) timerHour;
	calendarTimeout.set(Calendar.HOUR_OF_DAY, timerHourI);
	calendarTimeout.set(Calendar.MINUTE, 0);
	calendarTimeout.set(Calendar.SECOND, 0);
	calendarTimeout.set(Calendar.MILLISECOND, 0);

	if (calendarTimeout.before(Calendar.getInstance())) {
	    calendarTimeout.add(Calendar.DATE, 1);
	}
	Timer timer = timerService.createTimer(calendarTimeout.getTime(), interval, TIMER_NAME);
	LogService.logger.info("Se ha programado el Timer [" + TIMER_NAME + "] para ejecutarse cada dia. Proxima ejecucion [" + String.valueOf(timer.getNextTimeout()) + "]");
	
	LogService.logger.info("Programando Timer2 [" + TIMER_NAME + "] ");
	Calendar calendarTimeout2 = Calendar.getInstance();
	calendarTimeout2.set(Calendar.HOUR_OF_DAY, timerHourI);
	calendarTimeout2.set(Calendar.MINUTE, 30);
	calendarTimeout2.set(Calendar.SECOND, 0);
	calendarTimeout2.set(Calendar.MILLISECOND, 0);

	if (calendarTimeout2.before(Calendar.getInstance())) {
	    calendarTimeout2.add(Calendar.DATE, 1);
	}
	Timer timer2 = timerService.createTimer(calendarTimeout2.getTime(), interval, TIMER_NAME);
	LogService.logger.info("Se ha programado el Timer2 [" + TIMER_NAME + " 2] para ejecutarse cada dia. Proxima ejecucion [" + String.valueOf(timer2.getNextTimeout()) + "]");
	
	
	LogService.logger.info("Programando Timer3 [" + TIMER_NAME + "] ");
	Calendar calendarTimeout3 = Calendar.getInstance();
	Integer timerHourITwo = (int) (long) (timerHour + 1L);
	calendarTimeout3.set(Calendar.HOUR_OF_DAY, timerHourITwo);
	calendarTimeout3.set(Calendar.MINUTE, 0);
	calendarTimeout3.set(Calendar.SECOND, 0);
	calendarTimeout3.set(Calendar.MILLISECOND, 0);

	if (calendarTimeout3.before(Calendar.getInstance())) {
	    calendarTimeout3.add(Calendar.DATE, 1);
	}
	Timer timer3 = timerService.createTimer(calendarTimeout3.getTime(), interval, TIMER_NAME);
	LogService.logger.info("Se ha programado el Timer3 [" + TIMER_NAME + " 3] para ejecutarse cada dia. Proxima ejecucion [" + String.valueOf(timer3.getNextTimeout()) + "]");
	
	
	LogService.logger.info("Programando Timer4 [" + TIMER_NAME + "] ");
	Calendar calendarTimeout4 = Calendar.getInstance();
	calendarTimeout4.set(Calendar.HOUR_OF_DAY, timerHourITwo);
	calendarTimeout4.set(Calendar.MINUTE, 30);
	calendarTimeout4.set(Calendar.SECOND, 0);
	calendarTimeout4.set(Calendar.MILLISECOND, 0);

	if (calendarTimeout4.before(Calendar.getInstance())) {
	    calendarTimeout4.add(Calendar.DATE, 1);
	}
	Timer timer4 = timerService.createTimer(calendarTimeout4.getTime(), interval, TIMER_NAME);
	LogService.logger.info("Se ha programado el Timer4 [" + TIMER_NAME + " 4] para ejecutarse cada dia. Proxima ejecucion [" + String.valueOf(timer4.getNextTimeout()) + "]");
	
	
	LogService.logger.info("Programando Timer5 [" + TIMER_NAME + "] ");
	Calendar calendarTimeout5 = Calendar.getInstance();
	Integer timerHourIThree = (int) (long) (timerHour + 2L);
	calendarTimeout5.set(Calendar.HOUR_OF_DAY, timerHourIThree);
	calendarTimeout5.set(Calendar.MINUTE, 0);
	calendarTimeout5.set(Calendar.SECOND, 0);
	calendarTimeout5.set(Calendar.MILLISECOND, 0);

	if (calendarTimeout5.before(Calendar.getInstance())) {
	    calendarTimeout5.add(Calendar.DATE, 1);
	}
	Timer timer5 = timerService.createTimer(calendarTimeout5.getTime(), interval, TIMER_NAME);
	LogService.logger.info("Se ha programado el Timer5 [" + TIMER_NAME + " 5] para ejecutarse cada dia. Proxima ejecucion [" + String.valueOf(timer5.getNextTimeout()) + "]");
	
	
	LogService.logger.info("Programando Timer6 [" + TIMER_NAME + "] ");
	Calendar calendarTimeout6 = Calendar.getInstance();
	calendarTimeout6.set(Calendar.HOUR_OF_DAY, timerHourIThree);
	calendarTimeout6.set(Calendar.MINUTE, 30);
	calendarTimeout6.set(Calendar.SECOND, 0);
	calendarTimeout6.set(Calendar.MILLISECOND, 0);

	if (calendarTimeout6.before(Calendar.getInstance())) {
	    calendarTimeout6.add(Calendar.DATE, 1);
	}
	Timer timer6 = timerService.createTimer(calendarTimeout6.getTime(), interval, TIMER_NAME);
	LogService.logger.info("Se ha programado el Timer6 [" + TIMER_NAME + " 6] para ejecutarse cada dia. Proxima ejecucion [" + String.valueOf(timer6.getNextTimeout()) + "]");
	
	
	return timer;
    }

    @Timeout
    public void scheduledTimeout(Timer timer) throws MessagingException {
	LogService.logger.info("TIMEOUT [" + TIMER_NAME + "] ");
	
	//offerCheckpointNotAceptedOffer
	transactionalEmailsServices.offerCheckpointNotAceptedOffer();
	
	
	LogService.logger.info("Finaliza [" + TIMER_NAME + "]. Proxima ejecucion del Timer [" + TIMER_NAME + "]: " + String.valueOf(timer.getNextTimeout()));
    }

    public void removeTimer() {
	LogService.logger.debug("removeTimer :: ACTIVO [ " + isActive() + "]");
	if (timerService.getTimers() != null) {
	    for (Timer timer : timerService.getTimers()) {
		String timerInfo = (String) timer.getInfo();
		if (TIMER_NAME.equalsIgnoreCase(timerInfo)) {
		    LogService.logger.info("Timer encontrado: " + timerInfo);
		    timer.cancel();
		    LogService.logger.info("Se ha cancelado el Timer: " + timerInfo);
		}
	    }
	}
    }

    @PreDestroy
    public void onShutdown() {
	removeTimer();
    }

    public boolean isActive() {
	return active;
    }
}
