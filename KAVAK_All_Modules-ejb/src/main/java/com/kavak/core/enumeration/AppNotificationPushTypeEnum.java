package com.kavak.core.enumeration;

public enum AppNotificationPushTypeEnum {
	/** NOTIFICACION DE AUTO DISPONIBLE v-207*/
    	AVAILABLE_CARS_APP_NOTIFICATION("AvailableCarsAppNotification", 1L , "NOTIFICACION DE AUTO DISPONIBLE"),
    	/** NOTIFICATION DE RESERVA DE AUTO V-204 */
    	BOOKED_REGISTERED_APP_NOTIFICATION("BookedRegisteredAppNotification", 2L, "NOTIFICATION DE RESERVA REGISTRADA KAVAK"),
    	/** NOTIFICACION DE VISITA REGISTRADA V-201.1 = Pedregal / V-201.2 = Poniente / V-201.3 = Londre 189 (SE CANCELA RESERVA)*/
    	APPOINTMENT_REGISTERED_APP_NOTIFICATION("AppointmentRegistered", 3L, "NOTIFICACION DE VISITA KAVAK"),
    	/** NOTIFICACION DE VISITA CON RESERVA*/
    	APPOINTMENT_BOOKED_APP_NOTIFICATION("AppointmentBookedRegistered", 4L, "NOTIFICACION DE VISITA CON RESERVA KAVAK"),
    	/** NOTIFICACION ACTIVA V-206 */
    	BOOKED_NOTIFICATION_APP_NOTIFICATION("ActiveNotification", 5L, "NOTIFICACION ACTIVA AUTO RESERVADO KAVAK"),
	/** RECORDATORIO DE TIEMPO DE RESERVA POR VENCER*/
    	BOOKED_TIME_REMINDER_APP_NOTIFICATION("BookingTimeRemindersAppNotification", 6L, "RECORDATORIO DE TIEMPO DE RESERVA POR VENCER"),
	/** NOTIFICACION DE TIEMPO DE RESERVA VENCIDA*/
    	BOOKED_TIME_EXPIRED_APP_NOTIFICATION("BookingTimeExpiredAppNotification", 7L, "NOTIFICACION DE TIEMPO DE RESERVA VENCIDA"),
	/** RECORDATORIO DE CITA PARA INSPECCION DE AUTO*/
    	INSPECTION_REMINDER_APP_NOTIFICATION("InspectionReminderAppNotification", 8L, "RECORDATORIO DE CITA PARA INSPECCION DE AUTO"),
        /** RECORDATORIO DE CITA PARA VER AUTO KAVAK*/
    	APPOINTMENT_REMINDER_APP_NOTIFICATION("AppointmentReminderAppNotification", 9L, "RECORDATORIO DE CITA PARA VER AUTO KAVAK"),
    	/** NOTIFICATION DE NUEVOS AUTOS EN CATÁLOGO KAVAK */
    	NEW_CAR_APP_NOTIFICATION("NewCarsAppNotification", 10L, "NOTIFICATION DE NUEVOS AUTOS EN CATÁLOGO KAVAK"), 	
	OTRO("OTRO", 500L, "No Existe");
	
	String code;
	Long value;
	String status;
    
	AppNotificationPushTypeEnum(String code, Long value, String status) {
		this.code = code;
		this.value = value;
		this.status = status;
    }

	public Long getValue() {
		return this.value;
	}

	public String getStatus() {
		return this.status;
	}

	public static AppNotificationPushTypeEnum getByCode(Integer code){
    	switch (code){
    		case 1 : return AppNotificationPushTypeEnum.AVAILABLE_CARS_APP_NOTIFICATION;
    		case 2 : return AppNotificationPushTypeEnum.BOOKED_REGISTERED_APP_NOTIFICATION;
    		case 3 : return AppNotificationPushTypeEnum.APPOINTMENT_REGISTERED_APP_NOTIFICATION;
    		case 4 : return AppNotificationPushTypeEnum.APPOINTMENT_BOOKED_APP_NOTIFICATION;
    		case 5 : return AppNotificationPushTypeEnum.BOOKED_NOTIFICATION_APP_NOTIFICATION; 
    		case 6 : return AppNotificationPushTypeEnum.BOOKED_TIME_REMINDER_APP_NOTIFICATION;
    		case 7 : return AppNotificationPushTypeEnum.BOOKED_TIME_EXPIRED_APP_NOTIFICATION;
    		case 8 : return AppNotificationPushTypeEnum.INSPECTION_REMINDER_APP_NOTIFICATION;
    		case 9 : return AppNotificationPushTypeEnum.APPOINTMENT_REMINDER_APP_NOTIFICATION;
    		case 10 : return AppNotificationPushTypeEnum.NEW_CAR_APP_NOTIFICATION;
    		default : return AppNotificationPushTypeEnum.OTRO;
    	}
    }

}