package com.kavak.core.enumeration;

public enum CatalogueCarStatusEnum {
	AVAILABLE,
	BOOKED, // Autos Reservado
	SOLD,
	PRELOADED,
	NEW_ARRIVAL,
    STATUS_NA
}