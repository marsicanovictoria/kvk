package com.kavak.core.enumeration;

public enum AppNotificationTokenTypeEnum {
    IOS("iOS"), ANDROID("Android"), NOT_EQUAL("NotEqual");

    String name;

    AppNotificationTokenTypeEnum(String name) {
	this.name = name;
    }

    public String getName() {
	return name;
    }

    public static AppNotificationTokenTypeEnum getByName(String name) {
	switch (name) {
	case "iOS":
	    return AppNotificationTokenTypeEnum.IOS;
	case "Android":
	    return AppNotificationTokenTypeEnum.ANDROID;
	default:
	    return AppNotificationTokenTypeEnum.NOT_EQUAL;
	}
    }

}