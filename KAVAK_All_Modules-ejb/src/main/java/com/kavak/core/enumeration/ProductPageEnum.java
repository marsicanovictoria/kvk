package com.kavak.core.enumeration;

public enum ProductPageEnum {

    CARACTERISTICAS("CARACTERISTICAS","CARACTERÍSTICAS", 1L),
    REPORTEDEINSPECCION("REPORTEDEINSPECCION","REPORTE DE INSPECCIÓN", 2L),
    SIMULADORDEFINANCIAMIENTO("SIMULADOR DE FINANCIAMIENTO","SIMULADOR DE FINANCIAMIENTO", 3L),
    APLICOPARACREDITO("APLICO PARA CRÉDITO","APLICO PARA CRÉDITO", 4L),
    PORQUENOSENCANTO("PORQUENOSENCANTO","¿POR QUÉ NOS ENCANTO?", 5L),
    PORQUEKAVAK("PORQUEKAVAK","¿POR QUÉ KAVAK?", 6L),
    PERSONALIZAMIKAVAK("PERSONALIZAMIKAVAK","PERSONALIZA MI KAVAK", 7L);

    String code;
    String name;
    Long value;

    public String getName() {
	return this.name;
    }
    
    public Long getValue() {
	return this.value;
    }
    ProductPageEnum(String code, String name, Long value) {
	this.code = code;
	this.name = name;
	this.value = value;
    }
}
