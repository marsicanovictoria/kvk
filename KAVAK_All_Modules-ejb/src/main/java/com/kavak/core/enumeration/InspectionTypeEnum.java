package com.kavak.core.enumeration;

public enum InspectionTypeEnum {

    INSPECCION(1L,"#Inspeccion"),
    POSTINSPECCION_N1(2L,"Post_Inspeccion_N1"),
    POSTINSPECCION_N2(3L,"Post_Inspeccion_N2"),
    SANITYCHECK(4L,"Sanity_Check");

    Long id;
    String  name;

    public Long getId(){
        return this.id;
    }

    InspectionTypeEnum(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
