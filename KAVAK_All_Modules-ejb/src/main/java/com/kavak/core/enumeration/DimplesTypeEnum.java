package com.kavak.core.enumeration;

public enum DimplesTypeEnum {

    EXTERIOR("exterior", "ext"),
    INTERIOR("interior", "inner"),
    NOT_EQUAL("NotEqual", "NotEqual");

    String name;
    String opcion;

    DimplesTypeEnum(String name, String opcion) {
        this.name = name;
        this.opcion = opcion;
    }

    public String getName() {
        return name;
    }

    public String getOpcion() {
        return opcion;
    }

    public static DimplesTypeEnum getByName(String name) {
        switch (name) {
            case "exterior":
                return DimplesTypeEnum.EXTERIOR;
            case "interior":
                return DimplesTypeEnum.INTERIOR;
            default:
                return DimplesTypeEnum.NOT_EQUAL;
        }
    }

}
