package com.kavak.core.enumeration;

public enum StatusCheckpointOfferEnum {
	/**	Primer paso del checkpoint dentro del proceso de resgistrar una oferta */
	CPCD("CPCD","Completó datos del auto y se registro/ingreso","CPCD", 1L),
	CPAO("CPAO","Aceptó Oferta","CPAO", 2L),
	CPDI("CPDI","Indicó Dirección de Inspección","CPDI", 3L),
	CPPI("CPPI","Programó Inspección","CPPI", 4L),
	CPCP("CPCP","Completó Proceso de Venta","CPCP", 5L),
	OTRO("OTRO","Status no registrado","NA", 0L); 
	
	String code;
	String spanishName;
	String alias;
	Long step;
    
	StatusCheckpointOfferEnum(String code, String spanishName, String alias, Long step) {
		this.code = code;
		this.spanishName = spanishName;
		this.alias = alias;
		this.step = step;
    }
    
	public String getSpanishName() {
		return this.spanishName;
    } 
	public String getAlias(){
		return this.alias;
	}
	
    public static StatusCheckpointOfferEnum getByType(String name){
    	switch (name){
    		case "COMPDATAREGISTER" : return StatusCheckpointOfferEnum.CPCD;
    		default : return StatusCheckpointOfferEnum.OTRO;
    	}
    }
    
    public static StatusCheckpointOfferEnum getByStep(Long step){
    	int stepInt = step.intValue();
    	switch (stepInt){
    		case 1 : return StatusCheckpointOfferEnum.CPCD;
    		case 2 : return StatusCheckpointOfferEnum.CPAO;
    		case 3 : return StatusCheckpointOfferEnum.CPDI;
    		case 4 : return StatusCheckpointOfferEnum.CPPI;
    		case 5 : return StatusCheckpointOfferEnum.CPCP;
    		default : return StatusCheckpointOfferEnum.OTRO;
    	}
    }
    
}