package com.kavak.core.enumeration;

public enum StatusDetailOfferEnum {
	/**	NO existen ofertas en BD */
	NOOFFERBD("NOOFFERBD","No hay oferta en BD", "EONBD"),
	/** No acepto l aoferta */
	NOACCEPTOFFER("NOACCEPTOFFER","No aceptó oferta","EONAO"),
	/** El carro no es zona servida */
	NOZONESERVED("NOZONESERVED","No es zona servida","EONZS"),
	OTRO("OTRO","Tipo de Status no definido","NA"); 
	
	String code;
	String spanishName;
	String alias;
    
	StatusDetailOfferEnum(String code, String spanishName, String alias) {
		this.code = code;
		this.spanishName = spanishName;
		this.alias = alias;
    }
    
	public String getSpanishName() {
		return this.spanishName;
    } 
	public String getAlias(){
		return this.alias;
	}
	
    public static StatusDetailOfferEnum getByType(String name){
    	switch (name){
    		case "NOOFFERBD" : return StatusDetailOfferEnum.NOOFFERBD;
    		case "NOACCEPTOFFER" : return StatusDetailOfferEnum.NOACCEPTOFFER;
    		case "NOZONESERVED" : return StatusDetailOfferEnum.NOZONESERVED;
    		default : return StatusDetailOfferEnum.OTRO;
    	}
    }
}