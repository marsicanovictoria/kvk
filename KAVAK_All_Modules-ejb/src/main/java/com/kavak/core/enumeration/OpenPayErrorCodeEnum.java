package com.kavak.core.enumeration;

public enum OpenPayErrorCodeEnum {

    //Generales
    Code0 (0, "Ocurrió un error con la conexión hacia OpenPay, revise su conexión de Internet e intente más tarde (Error de red 28)."),
    Code1000 (1000, "Ocurrió un error interno en el servidor de Openpay."),
    Code1001 (1001, "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos."),
    Code1002 (1002, "La llamada no está autenticada o la autenticación es incorrecta."),
    Code1003 (1003, "La operacion no se pudo completar porque la tarjeta fue declinada."),
    Code1004 (1004, "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible."),
    Code1005 (1005, "Uno de los recursos requeridos no existe."),
    Code1006 (1006, "Ya existe una transacción con el mismo ID de orden."),
    Code1007 (1007, "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada."),
    Code1008 (1008, "Una de las cuentas requeridas en la petición se encuentra desactivada."),
    Code1009 (1009, "El cuerpo de la petición es demasiado grande."),
    Code1010 (1010, "Se está utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se está usando la llave privada desde JavaScript."),
    Code1011 (1011, "Se solicita un recurso que está marcado como eliminado."),
    Code1012 (1012, "El monto transacción está fuera de los límites permitidos."),
    Code1013 (1013, "La operación no está permitida para el recurso."),
    Code1014 (1014, "La cuenta está inactiva."),
    Code1015 (1015, "No se ha obtenido respuesta de la solicitud realizada al servicio."),
    Code1016 (1016, "El mail del comercio ya ha sido procesada."),

    // Cargo en banco
    Code2001 (2001, "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente."),
    Code2002 (2002, "La tarjeta con este número ya se encuentra registrada en el cliente."),
    Code2003 (2003, "El cliente con este identificador externo (External ID) ya existe."),
    Code2004 (2004, "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn."),
    Code2005 (2005, "La fecha de expiración de la tarjeta es anterior a la fecha actual."),
    Code2006 (2006, "El código de seguridad de la tarjeta (CVV2) no fue proporcionado."),
    Code2007 (2007, "El número de tarjeta es de prueba, solamente puede usarse en Sandbox."),
    Code2008 (2008, "La tarjeta no es válida para puntos Santander."),

    // Cargo en Tarjeta
    Code3001 (3001, "La tarjeta fue declinada."),
    Code3002 (3002, "La tarjeta ha expirado."),
    Code3003 (3003, "La tarjeta no tiene fondos suficientes."),
    Code3004 (3004, "La tarjeta ha sido identificada como una tarjeta robada."),
    Code3005 (3005, "La tarjeta ha sido rechazada por el sistema antifraudes."),
    Code3006 (3006, "La operación no está permitida para este cliente o esta transacción"),
    Code3007 (3007, "Deprecado. La tarjeta fue declinada."),
    Code3008 (3008, "La tarjeta no es soportada en transacciones en línea."),
    Code3009 (3009, "La tarjeta fue reportada como perdida."),
    Code3010 (3010, "El banco ha restringido la tarjeta."),
    Code3011 (3011, "El banco ha solicitado que la tarjeta sea retenida. Contacte al banco."),
    Code3012 (3012, "Se requiere solicitar al banco autorización para realizar este pago."),

    // Cuentas
    Code4001 (4001, "La cuenta de Openpay no tiene fondos suficientes."),
    Code4002 (4002, "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes."),

    // Órdenes
    Code5001 (5001, "La orden con este identificador externo (external_order_id) ya existe."),

    // Webhooks
    Code6001 (6001, "El webhook ya ha sido procesado."),
    Code6002 (6002, "No se ha podido conectar con el servicio de webhook."),
    Code6003 (6003, "El servicio respondió con errores.");


    public static OpenPayErrorCodeEnum getByCode(Integer code){
        switch (code){
            case 0: return OpenPayErrorCodeEnum.Code0;
            case 1000: return OpenPayErrorCodeEnum.Code1000;
            case 1001: return OpenPayErrorCodeEnum.Code1001;
            case 1002: return OpenPayErrorCodeEnum.Code1002;
            case 1003: return OpenPayErrorCodeEnum.Code1003;
            case 1004: return OpenPayErrorCodeEnum.Code1004;
            case 1005: return OpenPayErrorCodeEnum.Code1005;
            case 1006: return OpenPayErrorCodeEnum.Code1006;
            case 1007: return OpenPayErrorCodeEnum.Code1007;
            case 1008: return OpenPayErrorCodeEnum.Code1008;
            case 1009: return OpenPayErrorCodeEnum.Code1009;
            case 1010: return OpenPayErrorCodeEnum.Code1010;
            case 1011: return OpenPayErrorCodeEnum.Code1011;
            case 1012: return OpenPayErrorCodeEnum.Code1012;
            case 1013: return OpenPayErrorCodeEnum.Code1013;
            case 1014: return OpenPayErrorCodeEnum.Code1014;
            case 1015: return OpenPayErrorCodeEnum.Code1015;
            case 1016: return OpenPayErrorCodeEnum.Code1016;
            case 2001: return OpenPayErrorCodeEnum.Code2001;
            case 2002: return OpenPayErrorCodeEnum.Code2002;
            case 2003: return OpenPayErrorCodeEnum.Code2003;
            case 2004: return OpenPayErrorCodeEnum.Code2004;
            case 2005: return OpenPayErrorCodeEnum.Code2005;
            case 2006: return OpenPayErrorCodeEnum.Code2006;
            case 2007: return OpenPayErrorCodeEnum.Code2007;
            case 2008: return OpenPayErrorCodeEnum.Code2008;
            case 3001: return OpenPayErrorCodeEnum.Code3001;
            case 3002: return OpenPayErrorCodeEnum.Code3002;
            case 3003: return OpenPayErrorCodeEnum.Code3003;
            case 3004: return OpenPayErrorCodeEnum.Code3004;
            case 3005: return OpenPayErrorCodeEnum.Code3005;
            case 3006: return OpenPayErrorCodeEnum.Code3006;
            case 3007: return OpenPayErrorCodeEnum.Code3007;
            case 3008: return OpenPayErrorCodeEnum.Code3008;
            case 3009: return OpenPayErrorCodeEnum.Code3009;
            case 3010: return OpenPayErrorCodeEnum.Code3010;
            case 3011: return OpenPayErrorCodeEnum.Code3011;
            case 3012: return OpenPayErrorCodeEnum.Code3012;
            case 4001: return OpenPayErrorCodeEnum.Code4001;
            case 4002: return OpenPayErrorCodeEnum.Code4002;
            case 5001: return OpenPayErrorCodeEnum.Code5001;
            case 6001: return OpenPayErrorCodeEnum.Code6001;
            case 6002: return OpenPayErrorCodeEnum.Code6002;
            case 6003 : return OpenPayErrorCodeEnum.Code6003;
            default : return OpenPayErrorCodeEnum.Code1000;
        }
    }

    Integer code;
    String message;

    OpenPayErrorCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public Integer getCode() {
        return this.code;
    }


}
