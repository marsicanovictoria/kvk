package com.kavak.core.enumeration;

public enum OfferTypeEnum {
	/** Oferta compra a 30 dias */
	BUY30DAYS("BUY30DAYS","Compra a 30 días", "TO30D", "TO30NC", 1L),
	/** Oferta compra inmediata */
	BUYINMEDIATE("BUYINMEDIATE","Compra Inmediata", "TOIN","TOINNC", 2L),
	/** Oferta Compra por consignación*/
	BUYCONSIGMENT("BUYCONSIGMENT","Oferta Consignación","TOOC", "TOOCNC",3L),
	/** Varias ofertas ofrecidas  */
	VARIOUSOFFERS("VARIOUSOFFERS","Varias Ofertas","TO30DIN", "NA", -1L),
	/** No existen ofertas */
	NOOFFERSBD("NOOFFERSBD","Sin Oferta (No hay oferta en la BD)","TONA", "NA", -1L),
	
	OTRO("OTRO","Tipo de Oferta no definido","NA", "NA", -1L);
	
	String code;
	String type;
	String alias;
	String aliasNotServed;
	Long id;
    
	OfferTypeEnum(String code, String type, String alias, String aliasNotServed, Long id) {
		this.code = code;
		this.type = type;
		this.alias = alias;
		this.aliasNotServed = aliasNotServed;
		this.id = id;
    }
    
	public String getCode() {
		return this.code;
    } 
	
	public String getType() {
		return this.type;
    } 
	
	public String getAlias(){
		return this.alias;
	}
	public String getAliasNotServed(){
		return this.aliasNotServed;
	}
	public Long getId(){
		return this.id;
	}

    public static OfferTypeEnum getByType(String name){
    	switch (name){
    		case "BUY30DAYS": return OfferTypeEnum.BUY30DAYS;
    		case "BUYINMEDIATE": return OfferTypeEnum.BUYINMEDIATE;
    		case "BUYCONSIGMENT": return OfferTypeEnum.BUYCONSIGMENT;
    		case "VARIOUSOFFERS": return OfferTypeEnum.VARIOUSOFFERS;
    		case "NOOFFERSBD": return OfferTypeEnum.NOOFFERSBD;
    		default: return OfferTypeEnum.OTRO;
    	}
    }
    
    public static OfferTypeEnum getByAlias(String alias){
    	switch (alias){
    		case "TO30D": return OfferTypeEnum.BUY30DAYS;
    		case "TO30NC": return OfferTypeEnum.BUY30DAYS;
    		case "TOIN": return OfferTypeEnum.BUYINMEDIATE;
    		case "TOINNC": return OfferTypeEnum.BUYINMEDIATE;
    		case "TOOC": return OfferTypeEnum.BUYCONSIGMENT;
    		case "TOOCNC": return OfferTypeEnum.BUYCONSIGMENT;
    		default: return OfferTypeEnum.OTRO;
    	}
    }
}