package com.kavak.core.enumeration;

import com.kavak.core.model.Message;

public enum MessageEnum {

    /*
     * Los codigos que empizan con F(Field) estan asociados a los campos de entrada de los EndPoints
     */
    /** Informa no registros en BD */
    M0000("M0000"),
    /** Informa que el Año del carro es menor al Minimo permitido por Kavak */
    M0001("M0001"),
    /** Informa que no hay registros en CostoEnvioCodigoPostal para el code de entrada */
    M0002("M0002"),
    /** Informa que el KM para el sku indicado es mayor al maximo permitido por Kavak */
    M0003("M0003"),
    /** Informa que no hay registros en CarData para el sku de entrada */
    M0004("M0004"),
    /** Informa que no se genero ninguna Oferta pero el carro es WishList */
    M0005("M0005"),
    /** Informa que no se genero ninguna Oferta pero el carro NO es WishList */
    M0006("M0006"),
    /** Informa que el KM del carro es mayor al Maximo permitido por Kavak */
    M0007("M0007"),
    /** Uusario no existe, mensaje de sesion expirada */
    M0008("M0008"),
    /** Informa que el ID de entrada para SellCarDetail no existe en la tabla */
    M0009("M0009"),
    /** Informa que el ID de entrada para CompraCheckpoint no existe en la tabla */
    M0010("M0010"),
    /** Informa que el ID de entrada para InspectionSchedule no existe en la tabla */
    M0011("M0011"),
    /** Informa que el email ya es utilizado en el registro de un usuario */
    M0012("M0012"),
    /** Informa que el email no tiene el formato correcto */
    M0013("M0013"),
    /** Informa que el email ingresado debe tener entre 8 y 24 caracteres */
    M0014("M0014"),
    /** Informa que la direccion IP no tiene un formato valido */
    M0015("M0015"),
    /** Informa que las credenciales que suministraron son inválidas. */
    M0016("M0016"),
    /** El token de google suministrado no es valido. */
    M0017("M0017"),
    /** La información de email de Google+ es privada. */
    M0018("M0018"),
    /** La información de email de Facebook es privada. */
    M0019("M0019"),
    /** La información de nombre de Facebook es privada. */
    M0020("M0020"),
    /** Debe Ingresar la Data requerida para Agregar Usuario. */
    M0021("M0021"),
    M0024("M0024"),
    /** El registro de usuario no existe en Base de datos. */
    M0025("M0025"),
    /** El registro de car_id no existe en Base de datos. */
    M0026("M0026"),
    /** Ingresar financing_id valido */
    M0027("M0027"),
    /** Tu nombre en Google+ es privado */
    M0028("M0028"),
    /** El token de Facebook suministrado no es valido. */
    M0029("M0029"),
    /** La fecha suministrada no posee un formato valido dd/MM/yyyy */
    M0030("M0030"),
    /** Debe enviar data de coincidencias de Carscout */
    M0033("M0033"),
    /** Un valor no existe en BD */
    M0034("M0034"),
    /** No existen coincidencias de carscout */
    M0035("M0035"),
    /** El alert_id no corresponde al user_id indicado */
    M0036("M0036"),
    /** El alert_id no existe en BD */
    M0037("M0037"),
    /** Debe incluir el rango de precio */
    M0039("M0039"),
    /** El catalogue_id no existe en BD */
    M0040("M0040"),
    /** Debe ingresar la data requerida para actualizar Notificacion */
    M0041("M0041"),
    /** El registro de sellCarDetail no existe */
    M0043("M0043"),
    /** La fecha suministrada no posee un formato valido dd/MM/yyyy */
    M0044("M0044"),
    /** El registro de metaValue no existe */
    M0045("M0045"),
    /** No existe un registro en v2_carscout_notify para el alert_id y catalogue_id recibidos */
    M0046("M0046"),
    /** No existen registros en Request para un userId */
    M0048("M0048"),
    /** No existen documentos para un requestId */
    M0049("M0049"),
    /** Ya existe un documentClass con ese mismo Nombre */
    M0050("M0050"),
    /** No existe registro en Question para ese id */
    M0051("M0051"),
    /** No existe registro en Question para ese id */
    M0052("M0052"),
    /** se exedio la cantidad de autos */
    M0053("M0053"),
    /** error los parametros de la url estan mal */
    M0054("M0054"),
    /** No file asociado a ese MRdocument */
    M0055("M0055"),
    /** No existe un MrDocumentClass con ese nombre */
    M0056("M0056"),
    /** No existen mas MRDocumentRequest a revisar */
    M0057("M0057"),
    /** error los parametros del Json estan null o incompletos */
    M0059("M0059"),
    /** No se consigue registro con los parametros recibidos */
    M0061("M0061"),
    /** Documento repetido para esta solicitud */
    M0065("M0065"),
    /** Máximo numero de ofertas por día excedido */
    M0066("M0066"),    
    /** Máximo numero de ofertas por minuto excedido */
    M0074("M0074"),
    /** ID de oferta Checkpoint no encontrado */
    M0075("M0075"),
    /** Máximo numero de alertas registradas para un usuario */
    M0076("M0076"),
    /** Máximo numero de alertas registradas para un usuario */
    M0077("M0077"),
    /** Usuario Bloqueado */
    M0078("M0078"),
    /**Not found the offer to recovery.*/
    M0081("M0081"),
    /** Date fields must be provided in the following format YYY-MM-DD */
    M0079("M0079"),
    /** Invalid email */
    M0080("M0080"),
    /** Invalid token for password recovery */
    M0084("M0084"),
    /** OFFERT OFF */
    M0082("M0082"),
    M0083("M0083"),
    M0085("M0085"),
    /** OFFERT INVALID URL */
    OTRO("OTRO");

    String code;

    MessageEnum(String code) {
	this.code = code;
    }

    public String getCode() {
	return code;
    }

    public static MessageEnum getByCode(String code) {
	switch (code) {
	case "M0000":
	    return MessageEnum.M0000;
	case "M0001":
	    return MessageEnum.M0001;
	case "M0002":
	    return MessageEnum.M0002;
	case "M0003":
	    return MessageEnum.M0003;
	case "M0004":
	    return MessageEnum.M0004;
	case "M0005":
	    return MessageEnum.M0005;
	case "M0006":
	    return MessageEnum.M0006;
	case "M0007":
	    return MessageEnum.M0007;
	case "M0008":
	    return MessageEnum.M0008;
	case "M0009":
	    return MessageEnum.M0009;
	case "M0010":
	    return MessageEnum.M0010;
	case "M0011":
	    return MessageEnum.M0011;
	case "M0012":
	    return MessageEnum.M0012;
	case "M0013":
	    return MessageEnum.M0013;
	case "M0014":
	    return MessageEnum.M0014;
	case "M0015":
	    return MessageEnum.M0015;
	case "M0016":
	    return MessageEnum.M0016;
	case "M0017":
	    return MessageEnum.M0017;
	case "M0018":
	    return MessageEnum.M0018;
	case "M0019":
	    return MessageEnum.M0019;
	case "M0020":
	    return MessageEnum.M0020;
	case "M0021":
	    return MessageEnum.M0021;
    case "M0024":
        return MessageEnum.M0024;
	case "M0025":
	    return MessageEnum.M0025;
	case "M0026":
	    return MessageEnum.M0026;
	case "M0027":
	    return MessageEnum.M0027;
	case "M0028":
	    return MessageEnum.M0028;
	case "M0029":
	    return MessageEnum.M0029;
	case "M0030":
	    return MessageEnum.M0030;
	case "M0033":
	    return MessageEnum.M0033;
	case "M0034":
        return MessageEnum.M0034;
	case "M0035":
	    return MessageEnum.M0035;
	case "M0036":
	    return MessageEnum.M0036;
	case "M0037":
	    return MessageEnum.M0037;
	case "M0039":
	    return MessageEnum.M0039;
	case "M0040":
	    return MessageEnum.M0040;
	case "M0041":
	    return MessageEnum.M0041;
	case "M0043":
	    return MessageEnum.M0044;
	case "M0044":
	    return MessageEnum.M0044;
	case "M0045":
	    return MessageEnum.M0045;
	case "M0046":
	    return MessageEnum.M0046;
	case "M0048":
	    return MessageEnum.M0048;
	case "M0049":
	    return MessageEnum.M0049;
	case "M0050":
	    return MessageEnum.M0050;
	case "M0051":
	    return MessageEnum.M0051;
	case "M0052":
	    return MessageEnum.M0052;
	case "M0053":
	    return MessageEnum.M0053;
	case "M0054":
	    return MessageEnum.M0054;
	case "M0055":
	    return MessageEnum.M0055;
	case "M0056":
	    return MessageEnum.M0056;
	case "M0057":
	    return MessageEnum.M0057;
	case "M0059":
	    return MessageEnum.M0059;
	case "M0061":
	    return MessageEnum.M0061;
	case "M0065":
	    return MessageEnum.M0065;
	case "M0066":
	    return MessageEnum.M0066;
	case "M0074":
	    return MessageEnum.M0074;
	case "M0075":
	    return MessageEnum.M0075;
	case "M0076":
	    return MessageEnum.M0076;
	case "M0077":
	    return MessageEnum.M0077;
	case "M0078":
	    return MessageEnum.M0078;
	case "M0081":
	    return MessageEnum.M0081;
	    	    case "M0079":
            return MessageEnum.M0079;
        case "M0080":
            return MessageEnum.M0080;
       case "M0082":
        return MessageEnum.M0082;
       case "MOO83":
        return MessageEnum.M0083;
        case "M0084":
            return MessageEnum.M0084;
        case "M0085":
            return MessageEnum.M0085;
	default:
	    return MessageEnum.OTRO;
	}
    }
}