package com.kavak.core.enumeration;

public enum ColorEnum {
	
	AZUL("AZUL","#0000FF"),
	ACERO("ACERO","#43464B"),
	AMARILLO("AMARILLO","#FFFF00"),
	BLANCO("BLANCO","#FFFFFF"),
	BEIGE("BEIGE","#F5F5DC"),
	CHAMPAGNE("CHANPAGNE","#F7E7CE"),
	GRIS("GRIS","#BEBEBE"),
	MARRON("MARRON","#804000"),
	NEGRO("NEGRO","#000000"),
	ROJO("ROJO","#FF0000"), 
	PLATEADO("PLATEADO", "#C0C0C0"),
	PURPURA("PÚRPURA","#4a192c"),
	OTRO("OTRO","COLOR NO DEFINIDO");
	
	String name;
	String hexadecimal;
    
	ColorEnum(String name, String hexadecimal) {
		this.name = name;
		this.hexadecimal = hexadecimal;
    }
    
	public String getHexadecimal() {
      return hexadecimal;
    } 
    public static ColorEnum getByName(String name){
    	switch (name){
    		case "AZUL" : return ColorEnum.AZUL;
    		case "ACERO" : return ColorEnum.ACERO;
    		case "AMARILLO" : return ColorEnum.AMARILLO;
    		case "BLANCO" : return ColorEnum.BLANCO;
    		case "BEIGE" : return ColorEnum.BEIGE;
    		case "CHAMPAGNE" : return ColorEnum.CHAMPAGNE;
    		case "GRIS" : return ColorEnum.GRIS;
    		case "MARRÓN" : return ColorEnum.MARRON;
    		case "NEGRO" : return ColorEnum.NEGRO;
    		case "ROJO" : return ColorEnum.ROJO;
    		case "PLATEADO" : return ColorEnum.PLATEADO;
    		case "PÚRPURA" : return ColorEnum.PURPURA;
    		default : return ColorEnum.OTRO;
    	}
    }
}