package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CarscoutAlertLkpSku;

import java.util.List;

@Eager
public interface CarscoutAlertLkpSkuRepository extends JpaRepository<CarscoutAlertLkpSku, Long> {

    List<CarscoutAlertLkpSku> findByCarscoutAlertId(Long alertId);

}
