package com.kavak.core.repositories;

import com.kavak.core.model.CarVersion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface CarVersionRepository extends JpaRepository<CarVersion, Long> {

	@Query("SELECT ca "
			+ "FROM CarVersion ca "
			+ "WHERE CONCAT(ca.carModel.carMake.name,' ', ca.carModel.description,' ', ca.description) LIKE CONCAT('%',?1,'%') "
			+ "AND ca.active=true "
			+ "AND ca.carModel.active=true "
			+ "AND ca.carModel.carMake.active=true ORDER BY ca.carModel.description asc ")
	List<CarVersion> findByDescriptionContainingOrderByCarModelDescriptionAsc(String version);

}
