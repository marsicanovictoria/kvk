package com.kavak.core.repositories;

import com.kavak.core.model.RepairType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface RepairTypeRepository extends JpaRepository<RepairType, Long> {

}
