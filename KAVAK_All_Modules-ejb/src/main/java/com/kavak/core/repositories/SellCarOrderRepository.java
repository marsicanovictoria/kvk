package com.kavak.core.repositories;

import com.kavak.core.model.SellCarOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface SellCarOrderRepository extends JpaRepository<SellCarOrder, Long> {

}
