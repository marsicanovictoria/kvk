package com.kavak.core.repositories;

import com.kavak.core.model.FinancingData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface FinancingDataRepository extends JpaRepository<FinancingData, Long> {

	FinancingData findByUserIdAndSellCarDetailId(Long userId, Long autoId);
}
