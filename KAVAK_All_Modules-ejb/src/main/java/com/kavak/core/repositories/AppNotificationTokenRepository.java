package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.AppNotificationToken;

@Eager
public interface AppNotificationTokenRepository extends JpaRepository<AppNotificationToken, Long> {
    
	@Query("SELECT app FROM AppNotificationToken app WHERE app.userId=?1 AND app.deviceId=?2")
	AppNotificationToken findByIdUserAndDeviceIdAndTokenDevice(Long userId, String deviceId);
	
	@Query("SELECT app FROM AppNotificationToken app WHERE app.userId=?1 AND app.deviceId=?2 AND app.tokenType=?3")
	AppNotificationToken findAllByIdUserAndDeviceIdAndTokenType(Long userId, String deviceId, String tokenType);
	
	@Query("SELECT app FROM AppNotificationToken app WHERE app.userId=?1 AND app.tokenType=?2")
	AppNotificationToken findAllByIdUserAndTokenType(Long userId, String tokenType);

	List<AppNotificationToken> findByUserId(Long userId);
}
