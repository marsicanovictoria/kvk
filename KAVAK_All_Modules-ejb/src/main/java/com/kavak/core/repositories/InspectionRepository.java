package com.kavak.core.repositories;

import com.kavak.core.model.Inspection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Eager
public interface InspectionRepository extends JpaRepository<Inspection, Long> {

	Inspection findByIdOffertCheckPoint(Long idOffertCheckPoint);

	@Query("SELECT i FROM Inspection i WHERE i.id IN :ids")
	List<Inspection> findByInspectionIds(@Param("ids") List<Long> inspectionsIds);

	@Query("SELECT i FROM Inspection i WHERE i.sendNetsuite = 0")
	List<Inspection> findBySendNetsuite();
}
