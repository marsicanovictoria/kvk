package com.kavak.core.repositories;

import com.kavak.core.model.Indication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface IndicationRepository extends JpaRepository<Indication, Long> {

}
