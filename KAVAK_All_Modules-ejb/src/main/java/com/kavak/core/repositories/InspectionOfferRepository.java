package com.kavak.core.repositories;

import com.kavak.core.model.InspectionOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface InspectionOfferRepository extends JpaRepository<InspectionOffer, Long>{

	InspectionOffer findByIdInspectionAndSelectedOfferTrue(Long idInspection);
	
}
