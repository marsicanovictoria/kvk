package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.PageMeta;

@Eager
public interface PageMetaRepository extends JpaRepository<PageMeta, Long>{

    PageMeta findByMetaKey(String string);


	
}
