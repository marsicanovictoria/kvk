package com.kavak.core.repositories;

import com.kavak.core.model.CarFeatureCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager 
public interface CarFeatureCategoryRepository extends JpaRepository<CarFeatureCategory, Long>{

}
