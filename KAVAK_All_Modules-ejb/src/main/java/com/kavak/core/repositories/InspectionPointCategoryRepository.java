package com.kavak.core.repositories;

import com.kavak.core.model.InspectionPointCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface InspectionPointCategoryRepository extends JpaRepository<InspectionPointCategory, Long>{

//    @Query(value = "select p.id id, c.name as category, '' as subcategory, p.name as inspection_point, inspection_point_type_id as inspection_point_type_id from v2_inspection_point_definition p join v2_inspection_point_category c on p.category_id = c.id where p.inspection_point_type_id in :ids UNION select p.id, c.name as category, s.name as subcategory, p.name as inspection_point,  inspection_point_type_id as inspection_point_type_id from v2_inspection_point_definition p join v2_inspection_point_subcategory s on p.subcategory_id = s.id join v2_inspection_point_category c on s.category_id = c.id where p.inspection_point_type_id in :ids ORDER BY category, subcategory",
//            nativeQuery=true)
//    List<Object[]> findByInspectionPointTypeId( @Param("ids") List<Long> inspectionsPointTypeIds);



//    @Query("SELECT ip.id as id, insppc.name as category,'' as subcategory ,ip.name as inspectionpoint FROM InspectionPoint ip JOIN InspectionPointCategory insppc ON ip.inspectionPointCategory.id = insppc.id WHERE :inspectionPointType IN :ids ")
//    List<InspectionPointCategoryDataDTO> findCategoryByInspectionPointTypeId(@Param("inspectionPointType") Long inspectionPointType, @Param("ids") List<Long> inspectionsPointTypeIds);

    //    FROM A aEntity WHERE 'mohamede1945' = ANY (SELECT bEntity.name FROM aEntity.bs bEntity)
//    @Query("SELECT ip FROM InspectionPointCategory ip WHERE ?1 = ALL (SELECT bEntity.inspectionPointTypeId FROM ip.listInspectionPoint bEntity ) ")
//    List<InspectionPointCategory> findByInspectionPointTypeIds(Long inspectionTypeId);

    // Busca los InspectionPoint (Join InspectionPointCategory) Actual
//    @Query("SELECT ipc FROM InspectionPointCategory ipc,InspectionPoint ip  WHERE ip.inspectionPointTypeId IN ?1 AND ipc.id = ip.categoryId")
//    List<InspectionPointCategory> findByInspectionPointTypeIds(List<Long> listInspectionPointTypeIds);

// NUEVO
    @Query("SELECT ipc FROM InspectionPointCategory ipc INNER JOIN ipc.listInspectionPoint ip  WHERE ip.inspectionPointTypeId IN ?1 ")
    List<InspectionPointCategory> findByInspectionPointTypeIds(List<Long> listInspectionPointTypeIds);

    // Busca los InspectionPoint (Join InspectionPointSubCategory)
    @Query("SELECT ipc FROM InspectionPointCategory ipc, InspectionPointSubCategory ips, InspectionPoint ip WHERE ip.inspectionPointTypeId IN ?1 AND  ips.categoryId = ipc.id AND  ip.subCategoryId = ips.id ")
    List<InspectionPointCategory> findSubcategorysByInspectionPointTypeIds(List<Long> listInspectionPointTypeIds);

//    @Query("SELECT ipc FROM InspectionPointCategory ipc ")
//    List<InspectionPointCategory> findBySubCategoryName(String subCategoryName);


//    select p.id, c.name as category, '' as subcategory, p.name as inspection_point
//      from v2_inspection_point_definition p
//    join v2_inspection_point_category c
//    on p.category_id = c.id
//    where p.inspection_point_type_id in (1,2)
}