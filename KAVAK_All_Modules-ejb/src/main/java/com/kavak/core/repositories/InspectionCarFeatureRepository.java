package com.kavak.core.repositories;

import com.kavak.core.model.InspectionCarFeature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface InspectionCarFeatureRepository extends JpaRepository<InspectionCarFeature, Long>{

	@Query("SELECT icf from InspectionCarFeature icf WHERE icf.idInspection=?1 AND icf.inspectionCarFeatureItem.idCategory=?2")
	List<InspectionCarFeature> findByInspectionAndCategory(Long idInspection,Long idCategory);

    @Query("SELECT icf from InspectionCarFeature icf WHERE icf.idInspection=?1 AND icf.inspectionCarFeatureItem.iconImage is not null")
    List<InspectionCarFeature> findByInspectionAndIconImageNotNull(Long idInspection);

    List<InspectionCarFeature> findByIdInspection(Long idInspection);
}
