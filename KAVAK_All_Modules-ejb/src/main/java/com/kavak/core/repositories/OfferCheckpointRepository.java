package com.kavak.core.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import com.kavak.core.model.OfferCheckpoint;

@Eager
public interface OfferCheckpointRepository extends JpaRepository<OfferCheckpoint, Long> {

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.id=?1")
    OfferCheckpoint findByIdAndMetakey(Long idInspection);

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.id IN :ids ORDER BY oc.id ASC")
    List<OfferCheckpoint> findByIdsAndMetakey(@Param("ids") List<Long> idInspection);

    Optional<OfferCheckpoint> findById(Long id);
    
    @Query(value="SELECT * FROM oferta_checkpoints WHERE id_usuario = ?1 AND fecha_oferta > DATE_SUB(now(), INTERVAL 1 DAY)", nativeQuery=true)
    List<OfferCheckpoint> findByIdUserAndCurrentDate(Long idUser);
    
    @Query(value="SELECT count(*) FROM oferta_checkpoints WHERE id_usuario = ?1 AND fecha_oferta > DATE_SUB(now(), INTERVAL 5 MINUTE)", nativeQuery=true)
    Long findByIdUserAndLastFiveMinutes(Long idUser);
    
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.id IN :ids")
    List<OfferCheckpoint> findByIds(@Param("ids") List<Long> purchaseOpportunitiesIds);

    // @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.sentNetsuite = 0 AND
    // oc.netsuiteItemId IS NOT EMPTY AND oc.offerType > 24 AND oc.idStatus > 19
    // AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) < ?1 ORDER BY
    // oc.id ASC")
    // List<OfferCheckpoint> findBySentNetsuite(int minutes);

    @Query(value="SELECT * FROM oferta_checkpoints oc WHERE oc.enviado_netsuite = 0 AND oc.codigo_item_netsuite IS NOT NULL AND oc.tipo_oferta >= 24 AND oc.estatus >= 19 ORDER BY origen_oferta ASC LIMIT 10", nativeQuery=true)
    List<OfferCheckpoint> findBySendNetsuite();

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.sentNetsuite = 0 OR oc.sentNetsuite = 2 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) >= ?1 ORDER BY oc.id ASC")
    List<OfferCheckpoint> findBySentNetsuitePastMinute(int minute);

    List<OfferCheckpoint> findByIdUserAndCarYearAndCarMakeAndCarModelOrderByIdAsc(Long idUser, Long carYear, String carMake, String carModel);

    @Query(value="SELECT * FROM oferta_checkpoints oc WHERE oc.enviado_netsuite = 2 AND oc.codigo_item_netsuite IS NOT NULL AND oc.tipo_oferta >= 24 AND oc.estatus >= 19 ORDER BY origen_oferta ASC LIMIT 10", nativeQuery=true)
    List<OfferCheckpoint> findByResendNetsuite();

    // @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.id >= 54462 AND
    // oc.netsuiteOpportunityId IS NULL OR oc.netsuiteOpportunityId = 0 AND
    // oc.duplicatedOfferControl IS NOT NULL ORDER BY oc.id ASC")
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.offerDate >= '2018-01-15 00:00:00' AND (oc.netsuiteOpportunityId IS NULL OR oc.netsuiteOpportunityId = 0) AND oc.duplicatedOfferControl IS NOT NULL AND oc.netsuiteIdAttemps < 3 ORDER BY oc.id ASC")
    List<OfferCheckpoint> findByNetsuiteOpportunityIdEmpty();

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.duplicatedOfferControl=?1")
    List<OfferCheckpoint> findByDuplicatedOfferControl(Long id);

    // @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 19 AND
    // oc.offerType = 24 AND oc.sentInternalEmail = 0 AND
    // (oc.netsuiteOpportunityId IS NOT NULL OR oc.netsuiteOpportunityId != 0)
    // AND oc.id >= 55446 ORDER BY oc.id ASC")
    // List<OfferCheckpoint> findByStatusAndOfferType();

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.offerDate >= '2018-01-14 00:00:00' AND oc.idStatus = 19 AND oc.offerType = 24 AND oc.sentInternalEmail = 0 AND (oc.netsuiteOpportunityId IS NOT NULL AND oc.netsuiteOpportunityId != 0) ORDER BY oc.id ASC")
    List<OfferCheckpoint> findFirstStagePurchaseOpportunities();

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.offerDate >= '2018-01-14 00:00:00' AND oc.idStatus = 20 AND oc.offerType IN (25,26,170,171,180,181) AND  oc.sentInternalEmail = 0 AND  (oc.netsuiteOpportunityId IS NOT NULL AND oc.netsuiteOpportunityId != 0) ORDER BY oc.id ASC")
    List<OfferCheckpoint> findSecondStagePurchaseOpportunities();

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.offerDate >= '2018-01-14 00:00:00' AND oc.idStatus = 21 AND oc.offerType IN (25,26,170,171,180,181) AND oc.sentInternalEmail = 0 AND (oc.netsuiteOpportunityId IS NOT NULL AND oc.netsuiteOpportunityId != 0) ORDER BY oc.id ASC")
    List<OfferCheckpoint> findThirdStagePurchaseOpportunities();

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.offerDate >= '2018-01-14 00:00:00' AND oc.idStatus = 22 AND oc.offerType IN (25,26,170,171,180,181) AND  oc.sentInternalEmail = 0 AND (oc.netsuiteOpportunityId IS NOT NULL AND oc.netsuiteOpportunityId != 0) ORDER BY oc.id ASC")
    List<OfferCheckpoint> findFourthStagePurchaseOpportunities();

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.offerDate >= '2018-01-14 00:00:00' AND oc.idStatus = 23 AND oc.offerType IN (25,26,170,171,180,181) AND  oc.sentInternalEmail = 0 AND (oc.netsuiteOpportunityId IS NOT NULL AND oc.netsuiteOpportunityId != 0) ORDER BY oc.id ASC")
    List<OfferCheckpoint> findFifthStagePurchaseOpportunities();

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.offerDate >= '2018-01-14 00:00:00' AND oc.idStatus = 182 AND oc.offerType IN (25,26,170,171,180,181) AND  oc.sentInternalEmail = 0 AND (oc.netsuiteOpportunityId IS NOT NULL AND oc.netsuiteOpportunityId != 0) ORDER BY oc.id ASC")
    List<OfferCheckpoint> findIncompleteFourthStagePurchaseOpportunities();

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.offerDate >= '2018-01-14 00:00:00' AND oc.idStatus = 183 AND oc.offerType IN (25,26,170,171,180,181) AND oc.sentInternalEmail = 0 AND (oc.netsuiteOpportunityId IS NOT NULL AND oc.netsuiteOpportunityId != 0) ORDER BY oc.id ASC")
    List<OfferCheckpoint> findIncompleteFifthStagePurchaseOpportunities();

    // ================================================ >>>
    // Emails Transaccionales

    // Stage 1
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 19 AND oc.offerType = 28 AND oc.sendEmail = 0 AND oc.offerSource != 'pricing' AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 20")
    List<OfferCheckpoint> findFirstStageSectionNotRequirements(); // 1A

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 19 AND oc.offerType in (24,180,181) AND oc.wishList = 1 AND oc.offerSource != 'pricing' AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 20")
    List<OfferCheckpoint> findFirstStageConsignmentRegisterNotAcceptedOffer(); // 1B

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 19 AND oc.offerType in (24,180,181) AND oc.wishList = 0 AND oc.offerSource != 'pricing' AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 20")
    List<OfferCheckpoint> findFirstStageConsignmentNotWlRegisterNotAcceptedOffer(); // 1C

    // Stage 2
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 20 AND oc.offerType in (26,25,170,171,181,180) AND oc.wishList = 1 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findSecondStageConsignmentAcceptedOffersNotaddress(); // 2A

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 20 AND oc.offerType in (181,180) AND oc.wishList = 0 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findSecondStageConsignmentNotWlAcceptedOffersNotaddress(); // 2B

    // Stage 3
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 21 AND oc.offerType = 181 AND oc.wishList = 0 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findThirdStageConsignmentNotWlSelectedAddressNotDayAndTime(); // 3A

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 21 AND oc.offerType = 180 AND oc.wishList = 1 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findThirdStageConsignmentSelectedAddressNotDayAndTime(); // 3B

    // Stage 4
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 22 AND oc.offerType in (26,25,170,171,181,180) AND oc.wishList = 1 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findFourthStageConsignmentSelectedDateAndTimeNoAttachedTc(); // 4A

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 22 AND oc.offerType in (181,180) AND oc.wishList = 0 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findFourthStageConsignmentNotWlNoAttachedTc(); // 4B

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 182 AND oc.offerType in (181,180) AND oc.wishList = 0 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findFourthStageConsignmentNotWlNotDateAndTimeNotAtachmentTC(); // 4C

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 182 AND oc.offerType in (26,25,170,171,181,180) AND oc.wishList = 1 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findFourthStageConsignmentNotDayAndTimeNotAtachmentTC(); // 4D

    // Stage 5
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 23 AND oc.offerType in (26,25,170,171,181,180) AND oc.wishList = 1 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findFifthStageConsignmentCompletedRegister(); // 5A

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 183 AND oc.offerType in (26,25,170,171,181,180) AND oc.wishList = 1 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findFifthStageConsignmentNoSelectedDayAndTimeCompletedRegister(); // 5B

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 23 AND oc.offerType in (181,180) AND oc.wishList = 0 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findFifthStageConsignmentNoWLCompletedRegister(); // 5C

    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 183 AND oc.offerType in (181,180) AND oc.wishList = 0 AND oc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findFifthStageConsignmentNoWlNOSelectedDayAndTimeCompletedRegister();
    
    //Correo Escenario 4 y 5 Agendó inspección Kavak Tower
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus in (22,23,182,183) AND oc.followUpEmailSent = 0 AND oc.assignedEm > 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findOfferCheckpointFourthAndFifthStageFollowUp();
    
    //Correo Escenario 3 no aceptó oferta Kavak Daniel Villa
    //@Query("SELECT oc FROM OfferCheckpoint oc WHERE ((oc.idStatus = 19 AND oc.offerType in (24,180,181) AND offerPunished = 'NO') OR (oc.idStatus = 20 AND oc.offerType IN (25,26,180,181))) AND oc.notAcceptedOfferEmailSent = 0 AND oc.offerSource != 'pricing' AND (oc.offerDate > CURDATE()-1) AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    @Query(value="SELECT * FROM oferta_checkpoints oc WHERE ((oc.estatus = 19 AND oc.tipo_oferta in (24,180,181) AND oferta_castigada = 'NO') OR (oc.estatus = 20 AND oc.tipo_oferta IN (25,26,180,181))) AND oc.notacceptedoffer_email_sent = 0 AND oc.origen_oferta != 'pricing' AND (oc.fecha_oferta >= CURDATE()-1) AND (oc.fecha_oferta <= CURDATE()) LIMIT 125",nativeQuery=true)
    List<OfferCheckpoint> findOfferCheckpointNotAceptedOffer();
    
    //Count Escenario 3 no aceptó oferta Kavak Daniel Villa
    @Query("SELECT COUNT(oc) FROM OfferCheckpoint oc WHERE ((oc.idStatus = 19 AND oc.offerType in (24,180,181) AND offerPunished = 'NO') OR (oc.idStatus = 20 AND oc.offerType IN (25,26,180,181))) AND oc.notAcceptedOfferEmailSent = 1 AND oc.offerSource != 'pricing' AND oc.idUser = ?1 AND oc.notAcceptedOfferEmailSent = 1 AND (oc.offerDate > CURDATE()-7)")
    Long findCountOfferCheckpointNotAceptedOfferByUser(Long userId);    
    
    //Count Escenario 3 no aceptó oferta Kavak Daniel Villa
    @Query("SELECT COUNT(oc) FROM OfferCheckpoint oc WHERE oc.idUser=?1 AND oc.idStatus NOT IN (19,20) AND (oc.offerDate > CURDATE()-7)")
    Long findCountOfferCheckpointNotOfferByUser(Long idUser);  

    // Fin Emails Transaccionales
    // ================================================ >>>

    @Query("SELECT COUNT(oc) FROM OfferCheckpoint oc WHERE oc.duplicatedOfferControl =?1 ORDER BY oc.id DESC")
    Long findOfferQuantityByDuplicatedOfferControl(Long duplicatedOfferControl);
    
    //Corrreos Transaccionales Nueva Oferta Varias ofertas
    @Query(value="SELECT * FROM oferta_checkpoints oc WHERE oc.estatus = 19 AND oc.tipo_oferta = 24 AND oc.new_offer_email_sent = 0 AND oc.origen_oferta = 'pricing' AND oc.comentarios like '%REAU0%' LIMIT 50",nativeQuery=true)
    List<OfferCheckpoint> findNewOfferWhislist(); 
    
    //Corrreos Transaccionales Nueva Oferta Consignacion
    @Query(value="SELECT * FROM oferta_checkpoints oc WHERE oc.estatus = 19 AND oc.tipo_oferta = 181 AND oc.new_offer_email_sent = 0 AND oc.origen_oferta = 'pricing' AND oc.comentarios like '%REAU0%' LIMIT 50",nativeQuery=true)
    List<OfferCheckpoint> findNewOfferNotWhislist(); 
    
    //Corrreos Transaccionales Nueva Oferta de auditoriaVarias ofertas
    @Query(value="SELECT * FROM oferta_checkpoints oc WHERE oc.estatus = 19 AND oc.tipo_oferta = 24 AND oc.wishList = 1 AND oc.new_offer_audit_email_sent = 0 AND oc.origen_oferta = 'pricing' AND oc.comentarios like '%REOM0%' LIMIT 20",nativeQuery=true)
    List<OfferCheckpoint> findNewOfferAuditWhislist();
    
    //Corrreos Transaccionales Nueva Oferta Consignacion
    @Query(value="SELECT * FROM oferta_checkpoints oc WHERE oc.estatus = 19 AND oc.tipo_oferta IN (180,181) AND oc.wishList = 0 AND oc.new_offer_audit_email_sent = 0 AND oc.origen_oferta = 'pricing' AND oc.comentarios like '%REOM0%' LIMIT 20",nativeQuery=true)
    List<OfferCheckpoint> findNewOfferAuditNotWhislist(); 
    
    //
    //SMS  INICIO
    // ================================================ >>>
    
    // SMS: SMS C-204 Etapa 4 y 5, Selecciona día, hora y lugar (Casa cliente)
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus IN (22,23) AND oc.selectDayPlaceCustomerSmsSent = 0 AND oc.inspectionDate IS NOT NULL AND oc.inspectionLocation = 31 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findSelectDayAndHourAndPlaceCustomer();
    
    // SMS: SMS C-204 Etapa 4 y 5, Selecciona día, hora y lugar (Centro)
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus IN (22,23) AND oc.selectDayPlaceCenterSmsSent = 0 AND oc.inspectionDate IS NOT NULL AND oc.inspectionLocation = 30 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findSelectDayAndHourAndPlaceCenter();
    
    // SMS: SMS Liberan oferta en auditoría
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 19 AND oc.releaseAuditOfferSmsSent = 0 AND oc.offerSource = 'pricing' AND oc.comment like '%REAU0%'")
    List<OfferCheckpoint> findReleaseAuditOffer(); 
    
    // SMS: SMS Acepta oferta pero no continua
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.idStatus = 20 AND oc.acceptOfferNotContinueSmsSent = 0 AND ABS(TIMESTAMPDIFF(MINUTE, oc.offerDate, sysdate())) > 10")
    List<OfferCheckpoint> findAcceptOfferNotContinue(); 
    
    //
    //SMS  FIN
    // ================================================ >>>
    
    
    //isInspectionReminderNotificationSent
    @Query("SELECT COUNT(oc) FROM OfferCheckpoint oc WHERE oc.inspectionDate is not null "
    	+ "AND oc.idHourInpectionSlot is not null "
    	+ "AND oc.inspectionDate > sysdate() "
    	+ "AND DATEDIFF(oc.inspectionDate, sysdate()) <= 1 "
    	+ "AND oc.inspectionReminderNotificationSent = 0")
    List<OfferCheckpoint> findIsInspectionReminderNotificationSent();
    
    //Recuperar Oferta
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.urlRecoverOffer=?1")
    List<OfferCheckpoint> findByUrlRecoverOffer(String hash);
    //recuperar ulmmita oferta para settear el url de recuperacion
    @Query("SELECT oc FROM OfferCheckpoint oc WHERE oc.id=?1")
    OfferCheckpoint findByIdOfferCheckpoints(Long id);
    
    

    @Query(value="SELECT * FROM oferta_checkpoints oc WHERE oc.id_usuario=?1 AND (oc.ab_test_result = 0 OR oc.ab_test_result = 1) AND oc.ab_test_code = 'PCAB001' ORDER BY oc.id DESC LIMIT 1",nativeQuery=true)
    OfferCheckpoint findByUserAndHiddenInstantOfferNotNullOrderDesc(Long idUsuario);

}