package com.kavak.core.repositories;

import com.kavak.core.model.AppointmentSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

/**
 * Created by Enrique on 11-Jul-17.
 */
@Eager
public interface AppointmentScheduleRepository extends JpaRepository <AppointmentSchedule, Long> {
}
