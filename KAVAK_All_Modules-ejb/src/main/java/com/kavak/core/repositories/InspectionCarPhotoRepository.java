package com.kavak.core.repositories;

import com.kavak.core.model.InspectionCarPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface InspectionCarPhotoRepository extends JpaRepository<InspectionCarPhoto, Long>{

	List<InspectionCarPhoto> findByIdInspection(Long idInspection);
}
