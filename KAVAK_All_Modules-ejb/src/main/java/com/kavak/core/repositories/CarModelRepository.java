package com.kavak.core.repositories;

import com.kavak.core.model.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface CarModelRepository extends JpaRepository<CarModel, Long> {

	List<CarModel> findByDescriptionContainingAndActiveTrueAndCarMakeActiveTrueOrderByDescriptionAsc(String model);

}
