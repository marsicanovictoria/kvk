package com.kavak.core.repositories;

import com.kavak.core.model.CarData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Eager 
public interface CarDataRepository extends JpaRepository<CarData, Long>, JpaSpecificationExecutor<CarData> {

	@Query("SELECT DISTINCT cd.carYear FROM CarData cd WHERE cd.active = true ORDER BY cd.carYear DESC")
	List<Integer> findCarYears();

	@Query("SELECT DISTINCT cd.carMake FROM CarData cd WHERE cd.carYear=?1 AND cd.active = true ORDER BY cd.carMake ")
	List<String> findCarMakesByYear(Long year);

	@Query("SELECT DISTINCT cd.carModel FROM CarData cd WHERE cd.carYear=?1 AND cd.carMake=?2 AND cd.active = true ORDER BY cd.carModel ")
	List<String> findModelsByYearAndMake(Long year, String carMake);
	
	@Query("SELECT cd FROM CarData cd WHERE cd.carYear=?1 AND cd.carMake=?2 AND cd.carModel=?3 AND cd.active = true ORDER BY cd.carTrim ")
	List<CarData> findCarTrimsByCarYearAndCarMakeAndCarModelOrderByCarTrim(Long year, String carMake, String carModel);
	
	CarData findBySku(String sku);

	@Query("SELECT cd FROM CarData cd WHERE cd.carMake LIKE CONCAT(:keyword,'%') AND cd.active = true ORDER BY cd.carMake ")
	List<CarData> findMakeAndModelByKeyWord(@Param("keyword") String keyword);
	
	
}