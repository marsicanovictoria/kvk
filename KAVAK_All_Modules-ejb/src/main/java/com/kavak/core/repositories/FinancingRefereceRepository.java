package com.kavak.core.repositories;

import com.kavak.core.model.FinancingReferece;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface FinancingRefereceRepository extends JpaRepository<FinancingReferece, Long> {


}
