
package com.kavak.core.repositories;

import com.kavak.core.model.InspectionDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface InspectionDetailRepository extends JpaRepository<InspectionDetail, Long> {

}

