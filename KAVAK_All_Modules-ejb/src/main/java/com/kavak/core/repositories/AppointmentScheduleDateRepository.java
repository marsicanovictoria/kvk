package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.AppointmentScheduleDate;

@Eager
public interface AppointmentScheduleDateRepository extends JpaRepository<AppointmentScheduleDate, Long>{

	//@Query("SELECT MAX(sd.visitDate) FROM ScheduleDate sd WHERE sd.carId = ?1")
	//@Query("SELECT sd FROM (SELECT MAX(sd.visitDate) FROM ScheduleDate sd WHERE sd.carId =?1)")
	@Query("SELECT sd FROM AppointmentScheduleDate sd WHERE sd.carId = ?1 ORDER BY sd.visitDate DESC")
	List<AppointmentScheduleDate> findMaxDateByCarId(Long stockId);
	
	// RECORDATORIO DE CITA PARA VER AUTO KAVAK
	@Query("SELECT sd FROM AppointmentScheduleDate sd WHERE sd.visitDate > sysdate() "
		+ "AND DATEDIFF(sd.visitDate, sysdate()) <= 1 "
		+ "AND sd.appointmentReminderNotificationSent = 0")
	List<AppointmentScheduleDate> findAppointmentReminderAppNotification();
}
