package com.kavak.core.repositories;

import com.kavak.core.model.ProductCarCompareSection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

/**
 * Created by Enrique on 09-Jun-17.
 */
@Eager
public interface ProductCarCompareSectionRepository extends JpaRepository<ProductCarCompareSection, Long>{

}
