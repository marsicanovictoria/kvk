package com.kavak.core.repositories;

import com.kavak.core.model.PromotionCarPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Eager
public interface PromotionCarPriceRepository extends JpaRepository<PromotionCarPrice, Long>{

    @Query("SELECT pcp FROM PromotionCarPrice pcp INNER JOIN pcp.promotion p WHERE (CURDATE() BETWEEN pcp.promotion.starDate AND pcp.promotion.endDate) AND pcp.idSellCarDetail IN :ids AND p.active = 1")
    List<PromotionCarPrice> findByIds(@Param("ids")List<Long> idsListSellCarDetail);

    @Query("SELECT pcp FROM PromotionCarPrice pcp INNER JOIN pcp.promotion p WHERE (CURDATE() BETWEEN pcp.promotion.starDate AND pcp.promotion.endDate) AND pcp.idSellCarDetail = ?1 AND p.active = 1")
    PromotionCarPrice findBy_Id(Long id);

}
