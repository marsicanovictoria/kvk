package com.kavak.core.repositories;

import com.kavak.core.model.InspectionSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Eager
public interface InspectionSummaryRepository extends JpaRepository<InspectionSummary, Long>{
	
	InspectionSummary findByIdInspectionAndSelectedTrue(Long idInspection);
	
	@Query("SELECT i FROM InspectionSummary i WHERE i.idInspection  = ?1 ORDER BY i.id ASC")
	List<InspectionSummary> findByInspectionId(Long id);
	
	@Query("SELECT i FROM InspectionSummary i WHERE i.idInspection IN :ids ORDER BY i.id ASC")
	List<InspectionSummary> findByInspectionIdsForNetsuite(@Param("ids") List<Long> inspectionsIds);

//	@Query("SELECT i FROM InspectionSummary i WHERE i.idInspection  = ?1")
	List<InspectionSummary> findByIdInspectionAndInspectionTypeId(Long inspectionId, Long inspectionTypeId);

}
