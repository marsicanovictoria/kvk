package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CarscoutCategory;

@Eager
public interface CarscoutCategoryRepository  extends JpaRepository<CarscoutCategory, Long> {

}
