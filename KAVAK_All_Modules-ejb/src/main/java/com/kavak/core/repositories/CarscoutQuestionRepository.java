package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CarscoutQuestion;

@Eager
public interface CarscoutQuestionRepository extends JpaRepository<CarscoutQuestion, Long> {

    List<CarscoutQuestion> findByActiveTrueAndMultipleFalse();

    List<CarscoutQuestion> findByActiveTrueAndMultipleTrue();


}
