package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.FinancingUserDomicile;

@Eager 
public interface FinancingUserDomicileRepository extends JpaRepository<FinancingUserDomicile, Long>, JpaSpecificationExecutor<FinancingUserDomicile> {

    FinancingUserDomicile findByUserIdAndActiveTrue(Long userId);

    FinancingUserDomicile findByFinancingIdAndActiveTrue(Long financingUserId);

}