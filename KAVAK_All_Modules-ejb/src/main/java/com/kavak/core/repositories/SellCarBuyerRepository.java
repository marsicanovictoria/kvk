package com.kavak.core.repositories;

import com.kavak.core.model.SellCarBuyer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface SellCarBuyerRepository extends JpaRepository<SellCarBuyer, Long> {
}
