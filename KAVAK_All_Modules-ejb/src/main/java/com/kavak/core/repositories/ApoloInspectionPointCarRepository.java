package com.kavak.core.repositories;

import com.kavak.core.model.ApoloInspectionPointCar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;


/**
 * Created by Enrique on 05-Jun-17.
 */
@Eager
public interface ApoloInspectionPointCarRepository extends JpaRepository<ApoloInspectionPointCar, Long> {

    List<ApoloInspectionPointCar> findByIdSellCarDetail(Long idSellCarDetail);

//    @Query("SELECT apc FROM ApoloInspectionPointCar apc, ApoloInspectionPointsItem api WHERE apc.idApoloInspectionPointsItem = api.id AND apc.idSellcarCarDetail =?1")
//    List<ApoloInspectionPointCar> findByIdSellcarCarDetailAndApoloInspectionPointsItem(Long idSellcarCarDetail);

}
