package com.kavak.core.repositories;

import com.kavak.core.model.InsuranceDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

/**
 * Created by Enrique Marin on 6/28/2017.
 */
@Eager
public interface InsuranceDetailRepository extends JpaRepository<InsuranceDetail, Long>  {

    InsuranceDetail findByIdInsurance(Long idInsurance);

}
