package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CarscoutQuestionAnswer;

@Eager
public interface CarscoutQuestionAnswerRepository extends JpaRepository<CarscoutQuestionAnswer, Long> {
    
    @Query("SELECT cq FROM CarscoutQuestionAnswer cq WHERE cq.userId =?1 AND cq.alertId=?2 AND cq.questionId=?3")
    CarscoutQuestionAnswer findQuestionAndAnswer(Long userId, Long alertId, Long questionId);

    CarscoutQuestionAnswer findByUserIdAndCatalogueIdAndQuestionId(Long userId, Long catalogueId, Long questionId);
    
}
