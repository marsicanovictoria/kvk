package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.FinancingUserProfile;

@Eager 
public interface FinancingUserProfileRepository extends JpaRepository<FinancingUserProfile, Long>, JpaSpecificationExecutor<FinancingUserProfile> {


}