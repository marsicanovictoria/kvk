package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import com.kavak.core.model.User;

@Eager
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmailContainingIgnoreCase(String email);

    @Query("SELECT u FROM User u WHERE u.email LIKE CONCAT('%',:email,'%')")
    List<User> findManyByEmailContainingIgnoreCase(@Param("email") String email);

    List<User> findTop30BySentNetsuite(boolean sentNetsuite);

    User findByEmailAndRole(String email, String role);

    //@Query("SELECT u.username FROM User u WHERE u.username LIKE CONCAT('%',:username,'%')")
    //@Query("SELECT u FROM User u WHERE u.name LIKE CONCAT('\''%',:name,'\''%') OR u.userName LIKE CONCAT('\''%',:userName,'\''%') OR u.email LIKE CONCAT('\''%',:email,'\''%')")

    //@Query("SELECT u FROM User u WHERE u.name LIKE CONCAT('%',:name,'%') OR u.userName LIKE CONCAT('%',:userName,'%')")

    List<User> findByNameContainingIgnoreCaseOrIdOrEmailContainingIgnoreCase(String name, Long id, String email);

    List<User> findByNameContainingIgnoreCaseOrId(String name, Long id);

    List<User> findByNameContainingIgnoreCase(String name);
    //List<User> findByNameOrUserNameOrEmail(@Param("name") String name, String userName, String email);
    
    @Query("SELECT u FROM User u WHERE u.userRegisterSmsSent = 0 AND ABS(TIMESTAMPDIFF(MINUTE, u.creationDate, sysdate())) > 30")
    List<User> findUserReister();
    
}
