package com.kavak.core.repositories;

import com.kavak.core.model.SaleCarOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface SaleCarOrderRepository extends JpaRepository<SaleCarOrder, Long> {
}
