package com.kavak.core.repositories;

import com.kavak.core.model.SaleCarOrderTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager 
public interface SaleCarOrderTransactionRepository  extends JpaRepository<SaleCarOrderTransaction, Long>{
	
	// El query busca por status 0 - No confirmada
	@Query("SELECT s FROM SaleCarOrderTransaction s WHERE s.status=0 AND s.paymentCompleted=0 AND DATEDIFF(sysdate(),s.registerDate) >= ?1")
	List<SaleCarOrderTransaction> findPendingBookings(Integer days);	

	SaleCarOrderTransaction findByOrderId(String orderId);
}
