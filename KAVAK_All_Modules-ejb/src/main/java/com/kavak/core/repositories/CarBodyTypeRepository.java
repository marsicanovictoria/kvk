package com.kavak.core.repositories;

import com.kavak.core.model.CarBodyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

/**
 * Created by Enrique on 22-Jun-17.
 */
@Eager
public interface CarBodyTypeRepository extends JpaRepository<CarBodyType, Long> {

	CarBodyType findByName(String name);

	List<CarBodyType> findByActiveTrue();
}
