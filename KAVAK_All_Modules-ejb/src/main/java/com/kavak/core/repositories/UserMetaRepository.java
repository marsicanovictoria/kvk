package com.kavak.core.repositories;

import com.kavak.core.model.UserMeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface UserMetaRepository extends JpaRepository<UserMeta, Long> {

	@Query("SELECT um FROM UserMeta um WHERE um.metaKey= ?1 AND um.userId= ?2")
	UserMeta findUserMetaByMetaKeyAndId(String metaKey, Long userId);

	// El cable soluciona, pero no debe ser....
	@Query("SELECT me FROM UserMeta me WHERE me.metaKey IN ('direccion_financiamiento','telefono_fijo_financiamiento','telefono_celular_financiamiento') AND me.userId = ?1")
	List<UserMeta> findByMetaKeyInAndUserId(Long userId);

	//Optional<UserMeta> findByMetaKeyAndUserId(String metaKey, Long id);
	
	UserMeta findByMetaKeyAndUserId(String metaKey, Long id);

	List<UserMeta> findByMetaKeyAndUserIdOrderById(String metaKey, Long id);

	UserMeta findByMetaKeyAndMetaValue(String metaKey, String metaValue);

}
