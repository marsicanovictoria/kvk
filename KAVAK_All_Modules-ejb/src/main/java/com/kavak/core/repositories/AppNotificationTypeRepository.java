package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.AppNotificationType;

@Eager
public interface AppNotificationTypeRepository extends JpaRepository<AppNotificationType, Long> {

	@Query("SELECT app FROM AppNotificationType app WHERE app.active= 1")
	List<AppNotificationType> findAllActive();
	
}
