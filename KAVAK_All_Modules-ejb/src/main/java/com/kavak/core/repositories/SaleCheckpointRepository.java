package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import com.kavak.core.model.SaleCheckpoint;


@Eager
public interface SaleCheckpointRepository extends JpaRepository<SaleCheckpoint, Long> {
	
	@Query(value="SELECT * FROM compra_checkpoint sc WHERE sc.enviado_netsuite = 0 AND sc.estatus >= 145 ORDER BY sc.id ASC LIMIT 10", nativeQuery=true)
	List<SaleCheckpoint> findBySentNetsuite();
	
	//@Query("SELECT sc FROM SaleCheckpoint sc WHERE sc.sentNetsuite = 0 AND sc.status >= 145 AND ?1 > ABS(TIMESTAMPDIFF(MINUTE, sc.registerDate, sysdate())) ORDER BY sc.id ASC")
	//List<SaleCheckpoint> findBySentNetsuite(int minutes);
	
	@Query(value="SELECT * FROM compra_checkpoint sc WHERE sc.enviado_netsuite = 2 AND sc.estatus >= 145 ORDER BY sc.id ASC LIMIT 10", nativeQuery=true)
	List<SaleCheckpoint> findByResendNetsuite();
	
	@Query("SELECT sc FROM SaleCheckpoint sc WHERE sc.sentNetsuite = 0 OR sc.sentNetsuite = 2 AND ?1 <= ABS(TIMESTAMPDIFF(MINUTE, sc.registerDate, sysdate())) ORDER BY sc.id ASC")
	List<SaleCheckpoint> findBySentNetsuitePastMinute(int minute);

	List<SaleCheckpoint> findByUserIdAndCarIdOrderByIdAsc(Long idUser, Long carId);
	
	@Query("SELECT sc FROM SaleCheckpoint sc WHERE sc.netsuiteOpportunityId = null AND sc.registerDate >= '2017-06-01 00:00:00' ORDER BY sc.id ASC")
	Page<SaleCheckpoint> findByNetsuiteOpportunityIdEmpty(Pageable pageable);
	
	@Query("SELECT scd FROM SaleCheckpoint scd WHERE scd.id IN :ids")
	List<SaleCheckpoint> findByIds(@Param("ids") List<Long> saleIds);
	
	// Correos
	@Query("SELECT cc FROM SaleCheckpoint cc WHERE cc.checkoutResult like 'Error%'  AND cc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, cc.updateDate, sysdate())) > 5")
	List<SaleCheckpoint> findCheckoutPaymentError(); // 
	
	@Query("SELECT scd FROM SaleCheckpoint scd WHERE scd.carId = ?1 ORDER BY scd.id DESC")
	List<SaleCheckpoint> findByCarId(Long carId);
	
	// Citas con Reserva
	@Query("SELECT sc FROM SaleCheckpoint sc WHERE sc.appointmentEmailSent = 0 AND sc.saleOpportunityTypeId=275 and sc.scheduleDateId != null")
	List<SaleCheckpoint> findByAppointmentBookedEmailSent();
	
	// Citas sin Reserva
	@Query("SELECT sc FROM SaleCheckpoint sc WHERE sc.appointmentEmailSent = 0 AND sc.saleOpportunityTypeId=200")
	List<SaleCheckpoint> findByAppointmentEmailSent();
	
	// Correos
	//@Query("SELECT cc FROM SaleCheckpoint cc WHERE cc.checkoutResult like 'Pago de reserva con éxito'  AND cc.sendEmail = 0 AND ABS(TIMESTAMPDIFF(MINUTE, cc.updateDate, sysdate())) > 5")
	//List<SaleCheckpoint> findOpenPayPaymentSucces(); // 
	
	// SMS bookedCarSms 
	@Query("SELECT sc FROM SaleCheckpoint sc WHERE sc.bookedCarSmsSent = 0 AND sc.saleOpportunityTypeId in (201,245,275) and sc.status in (152,244)")
	List<SaleCheckpoint> findBookedCarSms();
	
	// SMS findScheduledAppointmentSms
	@Query("SELECT sc FROM SaleCheckpoint sc WHERE sc.scheduledAppointmentSmsSent = 0 AND sc.saleOpportunityTypeId IN (200,275) AND sc.status in (152,200)")
	List<SaleCheckpoint> findScheduledAppointmentSms();
	
	// bookingTimeReminderAppNotification
	@Query("SELECT cc FROM SaleCheckpoint cc WHERE cc.checkoutResult IN ('Pago de reserva con éxito','Reservado desde Minerva') "
		+ "AND cc.bookingTimeReminderNotificationSent = 0 "
		+ "AND  cc.registerDate < sysdate() "
		+ "AND ABS(TIMESTAMPDIFF(HOUR, cc.registerDate, sysdate())) > 48 "
		+ "AND ABS(TIMESTAMPDIFF(HOUR, cc.registerDate, sysdate())) < 72")
	List<SaleCheckpoint> findBookingTimeRemindersAppNotification(); // ALTER TABLE compra_checkpoint ADD booking_time_expired_notification_sent int(1) DEFAULT 1 
	
	// BookingTimeExpiredAppNotification
	@Query("SELECT cc FROM SaleCheckpoint cc WHERE cc.checkoutResult IN ('Pago de reserva con éxito','Reservado desde Minerva') "
		+ "AND cc.bookingTimeExpiredNotificationSent = 0 "
		+ "AND  cc.registerDate < sysdate() "
		+ "AND ABS(TIMESTAMPDIFF(HOUR, cc.registerDate, sysdate())) >= 72")
	List<SaleCheckpoint> findBookingTimeExpiredAppNotification(); 
	
	// Push V-204 Se registra reserva en la pagina
	@Query("SELECT cc FROM SaleCheckpoint cc WHERE cc.checkoutResult IN ('Pago de reserva con éxito','Reservado desde Minerva') AND cc.saleOpportunityTypeId in (201,245)  AND cc.bookedRegisteredNotificationSent = 0 ")
	List<SaleCheckpoint> findBookedRegisteredAppNotification(); 
	
	// Push V-201.1 / V-201.2 / V-201.3 Se registra cita con reserva en la pagina
	@Query("SELECT cc FROM SaleCheckpoint cc WHERE cc.checkoutResult IN ('Pago de reserva con éxito','Reservado desde Minerva') AND cc.saleOpportunityTypeId in (275) AND cc.appointmentBookedNotificationSent = 0 ")
	List<SaleCheckpoint> findAppointmentBookedRegisteredAppNotification(); 
	
	// Push V-201 Se registra una cita 
	@Query("SELECT cc FROM SaleCheckpoint cc WHERE cc.saleOpportunityTypeId in (200) AND cc.appointmentRegisteredNotificationSent = 0 ")
	List<SaleCheckpoint> findAppointmentRegisteredAppNotification(); 
	
	//Booked Emails
	@Query("SELECT sc FROM SaleCheckpoint sc WHERE sc.status = 152 AND sc.bookedEmailSent = 0 ")
	List<SaleCheckpoint> findBySendBookedEmail();
	
}

