package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.ContactUs;

@Eager
public interface ContactUsRepository extends JpaRepository<ContactUs, Long> {

    List<ContactUs> findByInternalEmailSentFalse();


}
