package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.InspectionDiscount;

@Eager
public interface InspectionDiscountRepository extends JpaRepository<InspectionDiscount, Long>{

	InspectionDiscount findByIdInspectionAndDiscountType(Long idInspection, String discountType);
	
	@Query("SELECT id FROM InspectionDiscount id WHERE id.idInspection = ?1 ORDER BY id.id ASC")
	List<InspectionDiscount> findByIdInspection(Long inspectionId);
	
}
