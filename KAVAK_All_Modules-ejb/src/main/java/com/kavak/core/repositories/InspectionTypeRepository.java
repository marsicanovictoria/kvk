package com.kavak.core.repositories;

import com.kavak.core.model.InspectionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface InspectionTypeRepository extends JpaRepository<InspectionType, Long> {
}
