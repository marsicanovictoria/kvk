package com.kavak.core.repositories;

import com.kavak.core.model.InspectionPointCar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface InspectionPointCarRepository extends JpaRepository<InspectionPointCar, Long> {

	List<InspectionPointCar> findByIdInspection(Long idInspection);

	List<InspectionPointCar> findByIdInspectionAndIdInspectioPointItem(Long idInspection, Long idInspectioPointItem);

}
