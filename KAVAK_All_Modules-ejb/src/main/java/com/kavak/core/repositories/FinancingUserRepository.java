package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.FinancingUser;

@Eager 
public interface FinancingUserRepository extends JpaRepository<FinancingUser, Long>, JpaSpecificationExecutor<FinancingUser> {

    List<FinancingUser> findByUserIdAndActiveTrue(Long userId);

    FinancingUser findByEmail(String email);

    List<FinancingUser> findByUserId(Long userId);
    
    @Query(value="SELECT f FROM FinancingUser f WHERE f.userId = ?1 AND f.active= 1 ORDER BY f.creationDate DESC")
    List<FinancingUser> findByUserIdOrderBycreationDateDescAndActiveTrue(Long userId);

    FinancingUser findByFinancingUserIdAndActiveTrue(Long idUserFinancing);

    @Query(value="SELECT f FROM FinancingUser f WHERE f.phone= ?1")
    List<FinancingUser> findByPhone(String phone);
    
    @Query(value="SELECT f FROM FinancingUser f WHERE f.email= ?1")
    List<FinancingUser> findByEmails(String email);
    
    @Query(value="SELECT f FROM FinancingUser f WHERE f.userId= ?1 And f.active= 1")
    List<FinancingUser> findByUserIdAndActiveTruee(Long userId);

    FinancingUser findByFinancingUserIdAndUserIdAndActiveTrue(Long financingId, Long idUser);

    List<FinancingUser> findByRfc(String rfc);

    FinancingUser findByFinancingUserId(Long financingUserId);



}