package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CarscoutCatalogue;

@Eager
public interface CarscoutCatalogueRepository extends JpaRepository<CarscoutCatalogue, Long>, JpaSpecificationExecutor<CarscoutCatalogue>{

    @Query(value = "select carscoutca0_.* "
            + "from v2_carscout_catalogue carscoutca0_ "
            + "left outer join v2_carscout_alert_lkp_sku carscoutal1_ on "
            + "carscoutca0_.sku_id=carscoutal1_.sku_id "
            + "left outer join v2_carscout_alert carscoutal2_ on "
            + "carscoutal1_.alert_id=carscoutal2_.alert_id "
            + "left outer join v2_car_meta_accesories carmetaacc3_ on "
            + "carscoutca0_.year_id=carmetaacc3_.accesories_id "
            + "left outer join v2_carscout_category carscoutca4_ on "
            + "carmetaacc3_.category_id=carscoutca4_.category_id "
            + "where carscoutal2_.alert_id=?1 "
            + "and carscoutca4_.category_id=?2 "
            + "and carscoutca0_.active_ind =?3 "
            + "limit 60 ",nativeQuery=true)
    List<CarscoutCatalogue> findByCarscoutAlertLkpSkuCarscoutAlertIdAndCarMetaAccesoriesCarscoutCategoryId(Long alertId, Long categoryId, Long activeInd);

    @Query(value = "SELECT MAX(cc.km) as max FROM v2_carscout_catalogue cc where cc.active_ind = ?1", nativeQuery = true)
    Integer finByMaxKm(Long activeInd);

    @Query(value = "SELECT MIN(cc.km) as max FROM v2_carscout_catalogue cc where cc.active_ind = ?1", nativeQuery = true)
    Integer finByMinKm(Long activeInd);

    @Query(value = "SELECT MAX(cc.sale_price) as max FROM v2_carscout_catalogue cc where cc.active_ind = ?1", nativeQuery = true)
    Integer findByMaxSalePrice(Long activeInd);

	@Query(value = "SELECT MIN(cc.sale_price) as min FROM v2_carscout_catalogue cc where cc.active_ind = ?1", nativeQuery = true)
	Integer findByMinSalePrice(Long activeInd);

    CarscoutCatalogue findByIdAndActiveInd(Long catalogueId,Long activeInd);
}
