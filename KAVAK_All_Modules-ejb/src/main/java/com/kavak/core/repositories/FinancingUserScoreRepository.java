package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.FinancingUserScore;

@Eager 
public interface FinancingUserScoreRepository extends JpaRepository<FinancingUserScore, Long>, JpaSpecificationExecutor<FinancingUserScore> {

    @Query("SELECT f FROM FinancingUserScore f WHERE f.scoreEmailSent = 0")
    List<FinancingUserScore> findByScoreEmailSentFalse();

 
}