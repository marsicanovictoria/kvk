package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CustomerNotificationQuestion;

@Eager
public interface CustomerNotificationQuestionNotificationsRepository extends JpaRepository<CustomerNotificationQuestion, Long> {

	List<CustomerNotificationQuestion> findByNotificationType(Long notificationType); // 
}
