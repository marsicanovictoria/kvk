package com.kavak.core.repositories;

import com.kavak.core.model.ApoloInspectionPointsHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

/**
 * Created by Enrique on 05-Jun-17.
 */
@Eager
public interface ApoloInspectionPointsHistoryRepository extends JpaRepository<ApoloInspectionPointsHistory,Long> {

    List<ApoloInspectionPointsHistory> findByActiveTrueAndDeselectTrue();

}
