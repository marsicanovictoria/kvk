package com.kavak.core.repositories;

import com.kavak.core.model.Inspector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface InspectorRepository extends JpaRepository<Inspector, Long>{

	Inspector findByEmail(String email);
	
}
