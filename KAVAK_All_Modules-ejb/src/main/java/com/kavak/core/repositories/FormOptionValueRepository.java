package com.kavak.core.repositories;

import com.kavak.core.model.FormOptionValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface FormOptionValueRepository extends JpaRepository<FormOptionValue, Long> {

    List<FormOptionValue> findByCategory(String category);

    List<FormOptionValue> findByCategoryAndActiveTrue(String category);

    List<FormOptionValue> findByCategoryAndActiveTrueOrderById(String financingAutomotiveCarUseTypes);

    @Query(value="SELECT fv FROM FormOptionValue fv WHERE fv.value= ?1 And fv.active = 1")
    FormOptionValue findByValue(String trim);

}
