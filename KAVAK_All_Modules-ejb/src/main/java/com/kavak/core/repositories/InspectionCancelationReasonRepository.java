package com.kavak.core.repositories;

import com.kavak.core.model.InspectionCancelationReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface InspectionCancelationReasonRepository extends JpaRepository<InspectionCancelationReason, Long> {

    List<InspectionCancelationReason> findByActiveTrue();

}
