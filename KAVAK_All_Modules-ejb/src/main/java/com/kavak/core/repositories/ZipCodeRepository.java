package com.kavak.core.repositories;

import com.kavak.core.model.ZipCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface ZipCodeRepository extends JpaRepository<ZipCode, Long>{

	ZipCode findByZipCode(Long zipCode);

}
