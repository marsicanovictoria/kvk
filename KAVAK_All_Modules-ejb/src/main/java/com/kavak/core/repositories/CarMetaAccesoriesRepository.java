package com.kavak.core.repositories;

import com.kavak.core.model.CarMetaAccesories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface CarMetaAccesoriesRepository extends JpaRepository<CarMetaAccesories, Long> {

//	@Query("SELECT cm FROM CarMetaAccesories cm WHERE cm.accesoriesId IN (SELECT DISTINCT cd.yearId FROM CarData cd)")
//	List<CarMetaAccesories> findDistinctYear();

	List<CarMetaAccesories> findByCarscoutCategoryIdAndActiveTrue(Long categoryId);

}