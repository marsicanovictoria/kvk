package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import com.kavak.core.model.SellCarDetail;

@Eager
public interface SellCarDetailRepository extends JpaRepository<SellCarDetail, Long> {

    @Query("SELECT scd FROM SellCarDetail scd INNER JOIN FETCH scd.sellCarMeta LEFT OUTER JOIN scd.listSaleCarOrders sco ON sco.canceled=false WHERE scd.certified=true and scd.active=true AND scd.valide=true ORDER BY scd.sale,scd.id")
    List<SellCarDetail> getCatalogueCarByCertifiedAndActiveAndValide();

    SellCarDetail findByIdInspection(Long idInspection);

    @Query("SELECT scd FROM SellCarDetail scd INNER JOIN FETCH scd.sellCarMeta LEFT OUTER JOIN scd.listSaleCarOrders sco ON sco.canceled=false WHERE scd.certified=true and scd.active=true AND scd.valide=true AND scd.netsuiteItemId IS NOT EMPTY AND scd.sendNetsuite = 0 ORDER BY scd.sale,scd.id")
    List<SellCarDetail> getInventoryCars();

    @Query("SELECT scd FROM SellCarDetail scd INNER JOIN FETCH scd.sellCarMeta LEFT OUTER JOIN scd.listSaleCarOrders sco ON sco.canceled=false WHERE scd.certified=true AND scd.valide=false AND scd.sale=false AND scd.active=true AND scd.isNewArrival='yes' AND scd.netsuiteItemId IS NOT EMPTY AND scd.sendNetsuite = 0 ORDER BY scd.id")
    List<SellCarDetail> getPreloadedCars();

    @Query("SELECT scd FROM SellCarDetail scd INNER JOIN FETCH scd.sellCarMeta LEFT OUTER JOIN scd.listSaleCarOrders sco ON sco.canceled=false WHERE scd.certified=true AND scd.valide=false AND scd.sale=false AND scd.active=true AND scd.isNewArrival='no' AND scd.netsuiteItemId IS NOT EMPTY AND scd.sendNetsuite = 0 ORDER BY scd.id")
    List<SellCarDetail> getUnpublishedCars();
    
    @Query("SELECT scd FROM SellCarDetail scd WHERE scd.id IN :ids")
    List<SellCarDetail> findByIds(@Param("ids") List<Long> inventoyIds);

    List<SellCarDetail> findByActiveTrue();

    // Email de Experiencias de Venta
    @Query("SELECT sd FROM SellCarDetail sd WHERE sd.experienceEmailSent = 0 AND ABS(TIMESTAMPDIFF(MINUTE, sd.saleDate, sysdate())) > 10")
    List<SellCarDetail> findSaleCustomerExperiencie();
    
    // NOTIFICATION DE NUEVOS AUTOS EN CATÁLOGO KAVAK
    @Query("SELECT sd FROM SellCarDetail sd WHERE sd.postDate is not NULL AND sd.newCarNotificationSent = 0")
    List<SellCarDetail> findNewCarsAppNotification();

}