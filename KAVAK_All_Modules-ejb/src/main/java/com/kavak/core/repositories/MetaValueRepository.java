package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.MetaValue;

@Eager
public interface MetaValueRepository extends JpaRepository<MetaValue, Long>{
	
	MetaValue findByAlias(String alias);
	
	MetaValue findByIdAndActiveTrue(Long id);
	
	List<MetaValue> findByGroupCategory(String groupCategory);

	List<MetaValue> findByGroupCategoryAndGroupNameAndActiveTrue(String groupCategory, String groupName);

	MetaValue findByGroupCategoryAndGroupNameAndOptionName(String groupCategory, String groupName, String optionName);

    List<MetaValue> findByGroupNameAndActiveTrue(String groupName);

    MetaValue findByGroupCategoryAndGroupNameAndAliasAndActiveTrue(String groupCategory, String groupName, String alias);

    MetaValue findByAliasAndActiveTrue(String string);

	
}
