package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import com.kavak.core.model.CarscoutAlert;
import com.kavak.core.model.User;

import java.util.Date;
import java.util.List;

@Eager
public interface CarscoutAlertRepository extends JpaRepository<CarscoutAlert, Long> {

	CarscoutAlert findByIdAndUser(@Param("id") Long id, @Param("user") User userBD);

	List<CarscoutAlert> findByUserIdAndActiveInd(Long userId,Integer activeInd);

	@Query("SELECT count(ca.id) from CarscoutAlert ca where ca.user.id =?1 and ca.activeInd=?2")
	Integer findbyCountAlertUser(Long userId,Integer activeInd);
	
		
	List<CarscoutAlert> findByUserIdAndActiveIndAndId(Long userId,Integer activeInd, Long alertId);
	
	// Email carscout Alert
	@Query("SELECT ca FROM CarscoutAlert ca WHERE ca.activeInd = 1  AND ca.customerEmailSent = 0  AND ABS(TIMESTAMPDIFF(MINUTE, ca.lastUpdate, sysdate())) > 10")
	List<CarscoutAlert> findByCarscoutAlertEmailSent(); //

	@Query(value = "SELECT ca.* "
			 + "FROM v2_carscout_alert ca "
			 + "WHERE  DATE_ADD(ca.creation_date, INTERVAL ?1 MONTH) <= ?2 "
			 + "and ca.active_ind = ?3  " ,nativeQuery=true)
	List<CarscoutAlert> findByIntervalMonth(Integer mount, Date date, Long activeInd);

}
