package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CustomerNotificationQuestionOptions;

@Eager
public interface CustomerNotificationQuestionOptionRepository extends JpaRepository<CustomerNotificationQuestionOptions, Long> {
    
    List<CustomerNotificationQuestionOptions> findByQuestionId(Long questionId); // 

}
