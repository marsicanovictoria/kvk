package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.InspectionLocation;

@Eager
public interface InspectionLocationRepository extends JpaRepository<InspectionLocation, Long> {

	@Query("SELECT il FROM InspectionLocation il WHERE il.name = ?1")
	InspectionLocation findByName(String name);
	
	@Query("SELECT il FROM InspectionLocation il WHERE active = true")
	List<InspectionLocation> findAllActive();
	
	@Query("SELECT il FROM InspectionLocation il WHERE id = ?1")
	InspectionLocation findByIdLocation(Long idLocation);
	
	@Query("SELECT il.alias FROM InspectionLocation il WHERE active = true and alias != null")
	List<String> findAllActiveAndAliasNotNull();
	
	@Query("SELECT il FROM InspectionLocation il WHERE active = true and alias != null and il.id = ?1 ")
	InspectionLocation findAllActiveAndAliasNotNullAndIdWarehouseLocation(Long idWarehouseLocation);
}
