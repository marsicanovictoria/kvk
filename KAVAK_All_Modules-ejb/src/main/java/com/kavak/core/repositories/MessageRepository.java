package com.kavak.core.repositories;

import com.kavak.core.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.io.Serializable;

@Eager
public interface MessageRepository extends JpaRepository<Message, Serializable>{
	
	Message findByCode(String code);
	
}