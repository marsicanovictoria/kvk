package com.kavak.core.repositories;

import com.kavak.core.model.CarWarrantyPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

/**
 * Created by Enrique on 03-Jul-17.
 */

@Eager
public interface CarWarrantyPriceRepository extends JpaRepository<CarWarrantyPrice, Long> {

    @Query("SELECT cwp FROM CarWarrantyPrice cwp WHERE cwp.category = ?1 AND  ?2 >= cwp.yearsFrom  AND cwp.yearsTo >= ?2  ")
    CarWarrantyPrice findByOther7AndCarAge(String other7, int carAge);

}
