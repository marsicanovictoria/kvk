
package com.kavak.core.repositories;

import com.kavak.core.model.DimpleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface DimpleRepository extends JpaRepository<DimpleType, Long>{

}
