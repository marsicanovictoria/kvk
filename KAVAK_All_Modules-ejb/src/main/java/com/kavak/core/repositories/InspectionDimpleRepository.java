package com.kavak.core.repositories;

import com.kavak.core.model.InspectionDimple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface InspectionDimpleRepository extends JpaRepository<InspectionDimple, Long>{

    List<InspectionDimple> findByIdInspection(Long idInspection);

}
