package com.kavak.core.repositories;

import com.kavak.core.model.SellCarDetail;
import com.kavak.core.model.SellCarDimple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface SellCarDimpleRepository extends JpaRepository<SellCarDimple, Long>{
	
	List<SellCarDimple> findByIdSellCarDetailOrderByDimpleType(Long idSellCarDetail);

	List<SellCarDimple> findByIdSellCarDetail(Long idSellCarDetail);

	List<SellCarDimple> findBySellCarDetailAndMoment(SellCarDetail sellCarDetail, String moment);

//    List<SellCarDimple> findByIdSellCarDetailAndDimpleType(Long idSellCarDetail, String dimpleType);

}
