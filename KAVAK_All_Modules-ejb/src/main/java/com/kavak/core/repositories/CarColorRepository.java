package com.kavak.core.repositories;

import com.kavak.core.model.CarColor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

/**
 * Created by Enrique on 19-Jun-17.
 */
@Eager
public interface CarColorRepository extends JpaRepository<CarColor, Long> {

    CarColor findByName(String name);

    List<CarColor> findAllByActiveTrueOrderByPosition();
}
