package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.OfferType;

@Eager
public interface OfferTypeRepository extends JpaRepository<OfferType, Long> {
}
