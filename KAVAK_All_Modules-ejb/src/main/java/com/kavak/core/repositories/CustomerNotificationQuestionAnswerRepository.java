package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CustomerNotificationQuestionAnswer;

@Eager
public interface CustomerNotificationQuestionAnswerRepository extends JpaRepository<CustomerNotificationQuestionAnswer, Long> {
    
    
    // Email de Experiencias de Venta
    @Query("SELECT cq FROM CustomerNotificationQuestionAnswer cq WHERE cq.userId =?1 AND cq.carId=?2 AND cq.questionId=?3")
    CustomerNotificationQuestionAnswer findQuestionAndAnswer(Long userId, Long carId, Long questionId);

    
}
