package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.InspectionSummaryOffer;

@Eager
public interface InspectionSummaryOfferRepository extends JpaRepository<InspectionSummaryOffer, Long>{
	
}
