package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kavak.core.model.CustomerReviews;

public interface CustomerReviewRepository extends JpaRepository<CustomerReviews, Long>{

}
