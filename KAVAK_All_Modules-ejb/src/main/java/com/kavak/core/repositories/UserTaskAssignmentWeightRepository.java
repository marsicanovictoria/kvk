package com.kavak.core.repositories;

import com.kavak.core.model.UserTaskAssignmentWeight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface UserTaskAssignmentWeightRepository extends JpaRepository<UserTaskAssignmentWeight, Long> {

    @Query(value = "SELECT aw.id id, (aw.distribution_task-((SELECT count(*) FROM oferta_checkpoints oc WHERE oc.assigned_em = aw.id AND oc.assigned_em IS NOT NULL AND oc.fecha_oferta >= aw.date_from AND oc.estatus IN (22,23,182,183))/(select count(*) from oferta_checkpoints oc where oc.assigned_em is not NULL and oc.fecha_oferta >= aw.date_from and oc.estatus in (22,23,182,183)))) weight_gap FROM v2_user_task_assignment_weight aw  WHERE aw.task_name = 'schedule_inspection' GROUP BY aw.id, aw.distribution_task ORDER BY weight_gap DESC LIMIT 1", nativeQuery = true)
    UserTaskAssignmentWeight findEMOfferCheckpointByQueryOfWeight();

    @Query(value = "SELECT aw.id id, (aw.distribution_task-((SELECT count(*) FROM compra_checkpoint cc WHERE cc.assigned_em = aw.id AND cc.assigned_em IS NOT NULL AND cc.fecha_registro >= aw.date_from AND cc.estatus IN (22,23,182,183))/(SELECT count(*) FROM compra_checkpoint cc WHERE cc.assigned_em IS NOT NULL AND cc.fecha_registro >= aw.date_from  AND cc.estatus IN (22,23,182,183)))) weight_gap FROM v2_user_task_assignment_weight aw WHERE aw.task_name = 'scheduled_appointment' GROUP BY aw.id, aw.distribution_task ORDER BY weight_gap DESC LIMIT 1", nativeQuery = true)
    UserTaskAssignmentWeight findEMSaleCheckpointByQueryOfWeight();

    @Query(value = "select assigned_em id from oferta_checkpoints where id_usuario = ?1 and assigned_em in (select distinct id from v2_user_task_assignment_weight where task_name = 'schedule_inspection') order by fecha_oferta desc limit 1", nativeQuery = true)
    UserTaskAssignmentWeight findEMSaleCheckpointByQueryOfWeightAndUserId(Long userId);

}
