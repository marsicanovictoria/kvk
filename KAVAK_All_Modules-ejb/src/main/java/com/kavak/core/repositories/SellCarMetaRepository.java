package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.SellCarMeta;

@Eager
public interface SellCarMetaRepository extends JpaRepository<SellCarMeta, Long>{


    SellCarMeta findBySellCarId(Long id);

}
