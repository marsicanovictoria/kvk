package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.AppNotificationSetting;

@Eager
public interface AppNotificationSettingRepository extends JpaRepository<AppNotificationSetting, Long> {


	@Query("SELECT app FROM AppNotificationSetting app WHERE app.user.id=?1")
	List<AppNotificationSetting> findAllByIdUser(Long userId);
	
	@Query("SELECT app FROM AppNotificationSetting app WHERE app.user.id=?1 AND app.appNotificationType.id=?2 AND app.active= true")
	AppNotificationSetting findAllByIdUserAndTypeId(Long userId, Long appNotificationType);
	
	@Query("SELECT app FROM AppNotificationSetting app WHERE app.appNotificationType.id=?1 AND app.active= true")
	List<AppNotificationSetting> findAllByTypeId(Long appNotificationType);


}
