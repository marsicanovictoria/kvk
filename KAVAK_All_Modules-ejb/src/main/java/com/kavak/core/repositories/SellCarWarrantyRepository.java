package com.kavak.core.repositories;

import com.kavak.core.model.SellCarWarranty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

/**
 * Created by Enrique on 06-Jul-17.
 */
@Eager
public interface SellCarWarrantyRepository extends JpaRepository<SellCarWarranty, Long>{



}
