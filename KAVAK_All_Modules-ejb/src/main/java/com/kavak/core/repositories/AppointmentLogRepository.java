package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.AppointmentLog;


@Eager
public interface AppointmentLogRepository extends JpaRepository<AppointmentLog, Long> {
	

}

