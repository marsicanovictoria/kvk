package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CustomerNotifications;
import com.kavak.core.model.User;

@Eager
public interface CustomerNotificationsRepository extends JpaRepository<CustomerNotifications, Long> {

	List<CustomerNotifications> findByUserIdAndCarId(Long user_id, Long car_id);

	CustomerNotifications findByUserAndCarIdAndTypeAndSendNotification(User user, Long carId, Long typeId, Integer sendNotification);

	// Correos
	@Query("SELECT cn FROM CustomerNotifications cn WHERE cn.type = 251  AND cn.emailSent = 0  AND ABS(TIMESTAMPDIFF(MINUTE, cn.updateDate, sysdate())) > 10")
	List<CustomerNotifications> findAvailableCarsNotification(); // 
	
	@Query("SELECT cn FROM CustomerNotifications cn WHERE cn.carId = ?1 AND cn.type = 251 ORDER BY cn.creationDate DESC")
	List<CustomerNotifications> findByCarId(Long id); // 
	
	@Query("SELECT DISTINCT(cn.carId) FROM CustomerNotifications cn WHERE cn.type = 251  AND cn.emailSent = 0  AND ABS(TIMESTAMPDIFF(MINUTE, cn.updateDate, sysdate())) > 10")
	List<Long> findAvailableCarsNotificationDistinct();
	
	//SMS postCarNotification
	@Query("SELECT cn FROM CustomerNotifications cn WHERE cn.carNotificationSmsSent = 0 AND cn.type = 247")
	List<CustomerNotifications> findCarNotificationBookedSms(); // 
	
	//SMS  Al cancelarse una reserva
	@Query("SELECT cn FROM CustomerNotifications cn WHERE cn.cancelledCarBookedSmsSent = 0 AND cn.type = 251 AND ABS(TIMESTAMPDIFF(MINUTE, cn.updateDate, sysdate())) > 30")
	List<CustomerNotifications> findCancelledBookedCarSms(); // 
	
	//Actualizar Notificaciones al Reservar Auto desde open pay
	@Query("SELECT cn FROM CustomerNotifications cn WHERE (cn.cancelledCarBookedSmsSent = 0 OR cn.emailSent = 0) AND cn.type = 251 AND cn.carId = ?1 ")
	List<CustomerNotifications> findNotificationBookedCar(Long idCar); // 
	
	// Notification AvailableCarsNotification
	@Query("SELECT cn FROM CustomerNotifications cn WHERE cn.type = 251  AND cn.carAppNotificationSent = 0  AND ABS(TIMESTAMPDIFF(MINUTE, cn.updateDate, sysdate())) > 10")
	List<CustomerNotifications> findAvailableCarsAppNotification(); //
	
	//PUSH postCarNotification
	@Query("SELECT cn FROM CustomerNotifications cn WHERE cn.carNotificationActiveAppNotificationSent = 0 AND cn.type = 247")
	List<CustomerNotifications> findCarNotificationBookedPush(); // 
	
}
