package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CarscoutNotify;

@Eager
public interface CarscoutNotifyRepository extends JpaRepository<CarscoutNotify, Long> {

	CarscoutNotify findByCarscoutAlertIdAndCarscoutCatalogueId(Long alertId, Long catalogueId);

	@Query(value = "select * " +
			"from v2_carscout_notify carscoutno0_, v2_carscout_alert carscoutal1_ " +
			"where carscoutno0_.alert_id=carscoutal1_.alert_id " +
			"and carscoutal1_.client_id=?1 " +
			"and carscoutno0_.interested_ind=?2 ",nativeQuery = true)
	List<CarscoutNotify> findByCarscoutAlertUserIdAndInterestedInd(Long userId, Long interestedInd);

	CarscoutNotify findByCarscoutAlertUserIdAndId(Long userId,Long idCarscoutNotify);

}
