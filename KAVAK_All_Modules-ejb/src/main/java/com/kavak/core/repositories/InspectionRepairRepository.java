package com.kavak.core.repositories;

import com.kavak.core.model.InspectionRepair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface InspectionRepairRepository extends JpaRepository<InspectionRepair, Long> {

	@Query("SELECT ir FROM InspectionRepair ir WHERE ir.idInspection=?1 AND ir.idInspectionRepairCategory=?2 AND ir.idInspectionRepairSubCategory=?3 AND ir.idInspectionRepairItem=?4 AND ir.idInspectionPointCar=?5")
	InspectionRepair findIfExist(Long idInspection,Long idInspectionRepairCategory,Long idInspectionRepairSubCategory,Long idInspectionRepairItem, Long idInspectionPointCar);

	@Query("SELECT ir FROM InspectionRepair ir WHERE ir.idInspection = ?1 ORDER BY ir.id ASC")
	List<InspectionRepair> findByInspectionId(Long id);
}
