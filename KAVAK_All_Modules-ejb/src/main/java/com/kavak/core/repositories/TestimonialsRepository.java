package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.Testimonials;

@Eager
public interface TestimonialsRepository extends JpaRepository<Testimonials, Long>{

	Testimonials findByIdSellCarDetail(Long idSellCarDetail);
	
	@Query(value="SELECT * FROM testimonials oc ORDER BY oc.id DESC LIMIT ?1",nativeQuery=true)
	List<Testimonials> findAllOrderByIdDesc(Long limit);
	
}
