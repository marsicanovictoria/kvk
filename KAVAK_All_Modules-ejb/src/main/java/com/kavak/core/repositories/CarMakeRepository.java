package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CarMake;

/**
 * Created by Enrique on 27-Jun-17.
 */
@Eager
public interface CarMakeRepository extends JpaRepository<CarMake, Long> {

    CarMake findByName(String name);

    List<CarMake> findByActiveTrue();
}
