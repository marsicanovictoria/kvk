package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kavak.core.model.InspectionCarPhotoType;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface InspectionCarPhotoTypeRepository extends JpaRepository<InspectionCarPhotoType, Long> {
    
    List<InspectionCarPhotoType> findAllByOrderByPositionAsc(); 

}
