package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.FinancingUserIncome;

@Eager 
public interface FinancingUserIncomeRepository extends JpaRepository<FinancingUserIncome, Long>, JpaSpecificationExecutor<FinancingUserIncome> {


    FinancingUserIncome findByFinancingIdAndActiveTrue(Long financingId);



}