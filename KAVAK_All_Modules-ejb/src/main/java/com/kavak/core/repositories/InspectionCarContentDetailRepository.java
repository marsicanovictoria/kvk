package com.kavak.core.repositories;

import com.kavak.core.model.InspectionCarContentDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface InspectionCarContentDetailRepository extends JpaRepository<InspectionCarContentDetail, Long>{

	InspectionCarContentDetail findByIdInspection(Long idInspection);
	
}
