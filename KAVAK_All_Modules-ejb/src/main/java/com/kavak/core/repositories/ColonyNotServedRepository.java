package com.kavak.core.repositories;

import com.kavak.core.model.ColonyNotServed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface ColonyNotServedRepository extends JpaRepository<ColonyNotServed, Long>{

    List<ColonyNotServed> findByZipCode(Long zipCode);

}
