package com.kavak.core.repositories;

import com.kavak.core.model.InspectionPoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface InspectionPointRepository extends JpaRepository<InspectionPoint, Long>{

//    @Query("SELECT ipc FROM InspectionPointCategory ipc INNER JOIN ipc.listInspectionPoint ip  WHERE ip.inspectionPointTypeId IN ?1 ")

    @Query("SELECT ip FROM InspectionPoint ip WHERE ip.inspectionPointTypeId IN ?1 ")
    List<InspectionPoint> findByInspectionPointTypeIds(List<Long> listInspectionPointTypeIds);



//    select p.id, c.name as category, '' as subcategory, p.name as inspection_point
//    from v2_inspection_point_definition p
//    join v2_inspection_point_category c
//    on p.category_id = c.id
//    where p.inspection_point_type_id in (1,2)

}
