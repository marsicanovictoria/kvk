package com.kavak.core.repositories;

import com.kavak.core.model.CommunicationsTray;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.List;

@Eager
public interface CommunicationsTrayRepository extends JpaRepository<CommunicationsTray, Long> {

//1 - envíe aquellos mensajes donde el scheduled_date sea menor al sysdate y el send_id = 0.
    @Query(value="SELECT * FROM v2_communications_tray ct WHERE (ct.scheduled_date < sysdate()) AND ct.send_id = 0 ORDER BY ct.id LIMIT 100" , nativeQuery=true)
    List<CommunicationsTray> findByScheduledDateAndSendId();

}
