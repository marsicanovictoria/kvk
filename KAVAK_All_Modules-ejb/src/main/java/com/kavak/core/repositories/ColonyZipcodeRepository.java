package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import com.kavak.core.model.ColonyZipcode;

@Eager
public interface ColonyZipcodeRepository extends JpaRepository<ColonyZipcode, Long> {


    @Query(value = "select cz.id, cz.zipcode, cz.municipality, cz.city, cz.state, GROUP_CONCAT(cz.colony SEPARATOR '|') as colony,cz.active,cz.creation_date "
            + "from v2_colonies_by_zipcode cz "
            + "where zipcode = ?1 "
            + "group by cz.zipcode, cz.municipality, cz.city, cz.state ",nativeQuery=true)
    ColonyZipcode findByZipCodeLocal(String zipCode);

}
