package com.kavak.core.repositories;

import com.kavak.core.model.InspectionSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import java.util.Date;
import java.util.List;

@Eager
public interface InspectionScheduleRepository extends JpaRepository<InspectionSchedule, Long> {

    // Metodo que busca horarios reservados pero no se completo la operacion en tantos minutos
    @Query("SELECT s FROM InspectionSchedule s WHERE s.status=0 and TIMESTAMPDIFF (MINUTE , s.selectionDate , SYSDATE() ) >= ?1")
    List<InspectionSchedule> findForLiberate(Integer minutes);

    //Metodo para consultar los horarios de inspección que se encuentran reservados
    @Query("SELECT s FROM InspectionSchedule s WHERE s.inspectionDate BETWEEN ?1 AND ?2 ORDER BY s.inspectionDate ASC")
    List<InspectionSchedule> findInspectionSchedules(Date dateFrom, Date dateTo );
    
    @Query("SELECT s FROM InspectionSchedule s WHERE s.inspectionDate > ?1 and s.inspectionLocationId =?2 ORDER BY s.inspectionDate ASC")
    List<InspectionSchedule> findByDateWithLimitByInspectionLocationId(Date dateFrom, Long inspectionLocationId);

    List<InspectionSchedule> findByInspectionDate(Date dateFrom);

    List<InspectionSchedule> findByInspectionDateAndIdHourBlock(Date dateFrom, Long idHourBlock );

    List<InspectionSchedule> findByInspectionDateAndInspectionLocationId(Date dateFrom, Long locationId);

    List<InspectionSchedule> findByInspectionDateAndIdHourBlockAndInspectionLocationId(Date dateFrom, Long idHourBlock, Long locationId);
    
}
