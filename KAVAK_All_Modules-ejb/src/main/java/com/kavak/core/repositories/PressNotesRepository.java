package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.PressNotes;

@Eager
public interface PressNotesRepository extends JpaRepository<PressNotes, Long>{

    List<PressNotes> findAllByActiveTrue();

	
}
