package com.kavak.core.repositories;

import com.kavak.core.model.InspectionScheduleBlock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface InspectionScheduleBlockRepository extends JpaRepository<InspectionScheduleBlock, Long>{

    InspectionScheduleBlock findByIdHourBlock(Long id);

    InspectionScheduleBlock findByHourFrom (String hourFrom);



}

