package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.AppointmentScheduleBlock;

/**
 * Created by Enrique on 29-Jun-17.
 */
@Eager
public interface AppointmentScheduleBlockRepository extends JpaRepository<AppointmentScheduleBlock, Long> {
    
    List<AppointmentScheduleBlock> findAllByActiveTrueOrderById();
    
}
