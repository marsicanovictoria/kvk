package com.kavak.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.AppNotificationLog;

import java.util.List;

@Eager
public interface AppNotificationLogRepository extends JpaRepository<AppNotificationLog, Long> {
    
	@Query("SELECT app FROM AppNotificationLog app WHERE app.userId=?1")
	List<AppNotificationLog> findbyIdUser(Long userId);

}
