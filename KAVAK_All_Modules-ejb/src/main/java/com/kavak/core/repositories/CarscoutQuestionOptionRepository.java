package com.kavak.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

import com.kavak.core.model.CarscoutQuestionOptions;

@Eager
public interface CarscoutQuestionOptionRepository extends JpaRepository<CarscoutQuestionOptions, Long> {
    
    List<CarscoutQuestionOptions> findByQuestionId(Long questionId); 

}
