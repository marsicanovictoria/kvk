//package com.kavak.core.repositories;
//
//import com.kavak.core.openpay.model.Verification;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.cdi.Eager;
//
//
//
//@Eager
//public interface VerificationRepository extends JpaRepository<Verification, Long> {
//
//	public Verification findByIdVerification(Long id);
//
//	public Verification findByVerificationCode(String verificationCode);
//
//}
