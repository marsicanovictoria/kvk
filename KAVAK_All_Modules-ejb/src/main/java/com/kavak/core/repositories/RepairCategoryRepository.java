package com.kavak.core.repositories;

import com.kavak.core.model.RepairCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface RepairCategoryRepository extends JpaRepository<RepairCategory, Long> {

}
