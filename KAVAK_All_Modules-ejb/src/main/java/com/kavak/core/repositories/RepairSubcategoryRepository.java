package com.kavak.core.repositories;

import com.kavak.core.model.RepairSubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;

@Eager
public interface RepairSubcategoryRepository extends JpaRepository<RepairSubCategory, Long> {

}
