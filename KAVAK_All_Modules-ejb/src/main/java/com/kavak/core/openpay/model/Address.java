//package com.kavak.core.openpay.model;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//
//public class Address {
//
//	private String line1;
//	private String line2;
//	private String line3;
//	private String state;
//	private String city;
//	private String postalCode;
//	private String countryCode;
//
//	@JsonProperty("line1")
//	public String getLine1() {
//		return line1;
//	}
//	public void setLine1(String line1) {
//		this.line1 = line1;
//	}
//	@JsonProperty("line2")
//	public String getLine2() {
//		return line2;
//	}
//	public void setLine2(String line2) {
//		this.line2 = line2;
//	}
//	@JsonProperty("line3")
//	public String getLine3() {
//		return line3;
//	}
//	public void setLine3(String line3) {
//		this.line3 = line3;
//	}
//	@JsonProperty("state")
//	public String getState() {
//		return state;
//	}
//	public void setState(String state) {
//		this.state = state;
//	}
//	@JsonProperty("city")
//	public String getCity() {
//		return city;
//	}
//	public void setCity(String city) {
//		this.city = city;
//	}
//	@JsonProperty("postal_code")
//	public String getPostalCode() {
//		return postalCode;
//	}
//	public void setPostalCode(String postalCode) {
//		this.postalCode = postalCode;
//	}
//	@JsonProperty("country_code")
//	public String getCountryCode() {
//		return countryCode;
//	}
//	public void setCountryCode(String countryCode) {
//		this.countryCode = countryCode;
//	}
//}
