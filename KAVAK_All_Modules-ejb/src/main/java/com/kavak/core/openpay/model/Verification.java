//package com.kavak.core.openpay.model;
//
//import javax.persistence.*;
//import java.io.Serializable;
//
//@Entity
//@Table(name="verification_openpay")
//public class Verification implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//	private Long idVerification;
//	private String type;
//	private String eventDate;
//	private String verificationCode;
//
//	@Id
//	@Column(name="id")
//	@GeneratedValue(strategy=GenerationType.AUTO)
//	public Long getIdVerification() {
//		return idVerification;
//	}
//	public void setIdVerification(Long idVerification) {
//		this.idVerification = idVerification;
//	}
//	@Column(name="type")
//	public String getType() {
//		return type;
//	}
//	public void setType(String type) {
//		this.type = type;
//	}
//	@Column(name="event_date")
//	public String getEventDate() {
//		return eventDate;
//	}
//	public void setEventDate(String eventDate) {
//		this.eventDate = eventDate;
//	}
//	@Column(name="verification_code")
//	public String getVerificationCode() {
//		return verificationCode;
//	}
//	public void setVerificationCode(String verificationCode) {
//		this.verificationCode = verificationCode;
//	}
//
//}
