//package com.kavak.core.openpay.model;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//
//public class Card {
//
//	private String type;
//	private String brand;
//	private Address address;
//	private String cardNumber;
//	private String holderName;
//	private String expirationMonth;
//	private String expirationYear;
//	private String allowsCharges;
//	private String allowsPayouts;
//	private String creationDate;
//	private String bankName;
//	private String bankCode;
//
//	@JsonProperty("type")
//	public String getType() {
//		return type;
//	}
//	public void setType(String type) {
//		this.type = type;
//	}
//	@JsonProperty("brand")
//	public String getBrand() {
//		return brand;
//	}
//	public void setBrand(String brand) {
//		this.brand = brand;
//	}
//	@JsonProperty("address")
//	public Address getAddress() {
//		return address;
//	}
//	public void setAddress(Address address) {
//		this.address = address;
//	}
//	@JsonProperty("card_number")
//	public String getCardNumber() {
//		return cardNumber;
//	}
//	public void setCardNumber(String cardNumber) {
//		this.cardNumber = cardNumber;
//	}
//	@JsonProperty("holder_name")
//	public String getHolderName() {
//		return holderName;
//	}
//	public void setHolderName(String holderName) {
//		this.holderName = holderName;
//	}
//	@JsonProperty("expiration_month")
//	public String getExpirationMonth() {
//		return expirationMonth;
//	}
//	public void setExpirationMonth(String expirationMonth) {
//		this.expirationMonth = expirationMonth;
//	}
//	@JsonProperty("expiration_year")
//	public String getExpirationYear() {
//		return expirationYear;
//	}
//	public void setExpirationYear(String expirationYear) {
//		this.expirationYear = expirationYear;
//	}
//	@JsonProperty("allows_charges")
//	public String getAllowsCharges() {
//		return allowsCharges;
//	}
//	public void setAllowsCharges(String allowsCharges) {
//		this.allowsCharges = allowsCharges;
//	}
//	@JsonProperty("allows_payouts")
//	public String getAllowsPayouts() {
//		return allowsPayouts;
//	}
//	public void setAllowsPayouts(String allowsPayouts) {
//		this.allowsPayouts = allowsPayouts;
//	}
//	@JsonProperty("creation_date")
//	public String getCreationDate() {
//		return creationDate;
//	}
//	public void setCreationDate(String creationDate) {
//		this.creationDate = creationDate;
//	}
//	@JsonProperty("bank_name")
//	public String getBankName() {
//		return bankName;
//	}
//	public void setBankName(String bankName) {
//		this.bankName = bankName;
//	}
//	@JsonProperty("bank_code")
//	public String getBankCode() {
//		return bankCode;
//	}
//	public void setBankCode(String bankCode) {
//		this.bankCode = bankCode;
//	}
//
//}
