package com.kavak.core.controller;

import javax.ejb.EJB;
import javax.mail.MessagingException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.request.PostAppointmentRequestDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.AppointmentServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class AppointmentController {

    @EJB(mappedName = LookUpNames.SESSION_AppointmentServices)
    private AppointmentServices appointmentServices;

    @GetMapping("/api/v1/appointments/scheduleblocks")
    public @ResponseBody ResponseDTO scheduleBlocks(@RequestParam("date_from") String dateFrom, 
	                                            @RequestParam(value = "is_appointment_with_booking", required=false) boolean isAppointmentWithBooking) {
	return appointmentServices.getAppointmentScheduleblocks(dateFrom, isAppointmentWithBooking);
    }

    @PostMapping("/api/v1/appointments")
    public @ResponseBody ResponseDTO appointment(@RequestBody PostAppointmentRequestDTO postAppointmentRequestDTO) throws MessagingException {
	return appointmentServices.postAppointment(postAppointmentRequestDTO);
    }
}