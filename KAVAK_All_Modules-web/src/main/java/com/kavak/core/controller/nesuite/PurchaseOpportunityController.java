package com.kavak.core.controller.nesuite;


import java.util.List;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.RequestNetsuiteDTO;
import com.kavak.core.dto.netsuite.PurchaseOpportunityResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.netsuite.PurchaseOpportunityServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class PurchaseOpportunityController {

	@EJB(mappedName = LookUpNames.SESSION_PurchaseOpportunityServices)
	private PurchaseOpportunityServices purchaseOpportunityServices;
	
	//Obtiene los datos de un Purchase Opportunity por su id
	@GetMapping("/netsuite/api/v1/purchaseopportunity/{id}")
	public @ResponseBody ResponseDTO purcharseOpportunityById(@PathVariable("id") Long purchaseOpportunityId){
		return purchaseOpportunityServices.getPurchaseOpportunityById(purchaseOpportunityId);
	}
	
	//Obtiene todos los nuevos Purchase Opportunities 
	@GetMapping("/netsuite/api/v1/purchaseopportunities/")
	public @ResponseBody ResponseDTO purcharseOpportunity(){
		return purchaseOpportunityServices.getNewPurchaseOpportunities();
	}
	
	//Actualiza el status de envío de los Purchase Opportunities que ya fueron enviados a Netsuite previamente 
	@PutMapping("/netsuite/api/v1/purchaseopportunities")
	public @ResponseBody ResponseDTO sentPurchaseOpportunities(@RequestParam String purchaseOpportunitiesIds){
		return purchaseOpportunityServices.putSentPurchaseOpportunities(purchaseOpportunitiesIds);
	} 
	
	//Obtiene los checkpoints que no poseen ID de netsuite 
	@GetMapping("/netsuite/api/v1/purchaseopportunitieswithoutnetsuiteid/")
	public @ResponseBody ResponseDTO purchaseopportunitieswithoutnetsuiteid(){
		return purchaseOpportunityServices.getPurchaseOpportunityWithoutNetsuiteId();
	}
	
	//Actualiza el campo netsuite id en la tabla oferta_checkpoint obtenido en netsuite. 
	@PutMapping("/netsuite/api/v1/purchaseopportunitieswithoutnetsuiteid")
	public @ResponseBody ResponseDTO putNetsuiteOpportunityIdIntoMinerva(@RequestParam String netsuiteMinervaIds){
		return purchaseOpportunityServices.putNetsuiteOpportunityIdIntoMinerva(netsuiteMinervaIds);
	}
	
	//Envía una lista de Purchase Opportunities hacia Netsuite
	@PostMapping("/netsuite/api/v1/postpurchaseopportunities")
	public @ResponseBody ResponseDTO postPurchaseToNetsuite(@RequestBody List<PurchaseOpportunityResponseDTO> purchaseOpportunityDTO){
		return purchaseOpportunityServices.postPurchaseOpportunitiesToNetsuite(purchaseOpportunityDTO);
	}
	
	
	//Actualiza los registros recibidos desde Netsuite hacia nuestra BD. 
	@PutMapping("/netsuite/api/v1/PutPurchaseOpportunity")
	public @ResponseBody ResponseDTO PutPurchaseOpportunity(@RequestBody RequestNetsuiteDTO requestNetsuiteDTO){
		return purchaseOpportunityServices.PutPurchaseOpportunity(requestNetsuiteDTO);
	}
	
	//NUEVO: Obtiene uno o más registros de Purchase Opportunities para ser enviados a Netsuite
	@GetMapping("/netsuite/api/v1/getpurchaseopportunities/")
	public @ResponseBody ResponseDTO getpurcharseOpportunity(){
		return purchaseOpportunityServices.getPurchaseOpportunities();
	}
	
}
