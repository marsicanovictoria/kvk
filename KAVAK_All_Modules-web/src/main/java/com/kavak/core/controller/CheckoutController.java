package com.kavak.core.controller;

import com.kavak.core.dto.request.PutCheckoutRequestDTO;
import com.kavak.core.dto.request.RegisterFinancingRequestDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.CheckoutServices;
import com.kavak.core.service.CommonServices;
import com.kavak.core.util.LookUpNames;
import org.springframework.web.bind.annotation.*;

import javax.ejb.EJB;
import javax.mail.MessagingException;

@RestController
public class CheckoutController {

    @EJB(mappedName = LookUpNames.SESSION_CheckoutServices)
    private CheckoutServices checkoutServices;

    @EJB(mappedName = LookUpNames.SESSION_CommonServices)
    private CommonServices commonServices;

    @GetMapping("/api/v1/checkout/financingform_values")
    public @ResponseBody ResponseDTO financingFormValues() {
	return checkoutServices.getFinancingFormValues();
    }

    @GetMapping("/api/v1/checkout/payment_types")
    public @ResponseBody ResponseDTO paymentTypes() {
	return checkoutServices.getPaymentTypes(); 
    }

    @GetMapping("/api/v1/checkout/delivery_centers")
    public @ResponseBody ResponseDTO inspectionCenters(@RequestParam(value = "user_latitude", required = false) String userLatitude, 
	                                               @RequestParam(value = "user_longitude", required = false) String userLongitude, 
	                                               @RequestParam(value = "date", required = false) String date,
	                                               @RequestParam(value = "hour_block", required = false) Long hourBlockId) {
	return commonServices.getKavakCenters(userLatitude, userLongitude, date, hourBlockId, null);
    }

    @PutMapping("/api/v1/checkout")
    public @ResponseBody ResponseDTO putCheckout(@RequestBody PutCheckoutRequestDTO putCheckoutDTO) throws MessagingException {
	return checkoutServices.putCheckout(putCheckoutDTO);
    }

    @PostMapping("/api/v1/checkout/financing")
    public @ResponseBody ResponseDTO registerFinancingForm(@RequestBody RegisterFinancingRequestDTO postRegisterFinancingRequestDTO) {
	return checkoutServices.registerFinancingForm(postRegisterFinancingRequestDTO);
    }

    @GetMapping("/api/v1/checkout/financing")
    public @ResponseBody ResponseDTO getFinancingForm(@RequestParam("financing_id") Long financingId) {
	return checkoutServices.getFinancingForm(financingId);
    }

}