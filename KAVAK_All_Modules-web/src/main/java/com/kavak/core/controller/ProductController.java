package com.kavak.core.controller;

import javax.ejb.EJB;

import com.kavak.core.dto.GenericDTO;
import com.kavak.core.dto.request.PostDimplesCarRequestDTO;
import org.springframework.web.bind.annotation.*;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.CommonServices;
import com.kavak.core.service.ProductServices;
import com.kavak.core.util.LookUpNames;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
public class ProductController {

    @EJB(mappedName = LookUpNames.SESSION_ProductServices)
    private ProductServices productServices;

    @EJB(mappedName = LookUpNames.SESSION_CommonServices)
    private CommonServices commonServices;

    @GetMapping("/api/v1/cars/{id}")
    public @ResponseBody ResponseDTO car(@PathVariable("id") Long id,
	                                 @RequestParam(value = "version", required=false) Long version){
        return productServices.getCar(id,version = 1L);
    }
    
    @GetMapping("/api/v2/cars/{id}")
    public @ResponseBody ResponseDTO carV2(@PathVariable("id") Long id,
	    @RequestParam(value = "version", required=false) Long version){
	return productServices.getCar(id,version = 2L);
    }

    @GetMapping("/api/v1/cars/{id}/features")
    public @ResponseBody ResponseDTO features(@PathVariable("id") Long id){
        return productServices.getCarFeaturesById(id);
    }

    @GetMapping("/api/v1/cars/{id}/why_love")
    public @ResponseBody ResponseDTO whyLove(@PathVariable("id") Long id){
        return productServices.getWhyLove(id);
    }

    @GetMapping("/api/v1/cars/{id}/shipping_cost")
    public @ResponseBody ResponseDTO shippingCost(@RequestParam("postal_code") String zipCode){
        return productServices.getShippingCost(zipCode);
    }

    @GetMapping("/api/v1/cars/{id}/why_kavak")
    public @ResponseBody ResponseDTO whyKavak(@PathVariable("id") Long idSellCarDetail){
        return productServices.getWhyKavak(idSellCarDetail);
    }

    @GetMapping("/api/v1/cars/content_overview/{inspection_id}")
    public @ResponseBody ResponseDTO getCarContentOverview(@PathVariable("inspection_id") Long idInspection) {
        return productServices.getCarContentOverview(idInspection);
    }

    @GetMapping("/api/v1/cars/colors")
    public @ResponseBody ResponseDTO gerCarColors() {
        return productServices.gerCarColors();
    }

    @GetMapping("/api/v1/cars/{id}/inspection_report")
    public @ResponseBody ResponseDTO inspectionReport(@PathVariable("id") Long idSellCarDetail){
        return productServices.getInspectionReport(idSellCarDetail);
    }

    @GetMapping("/api/v1/cars/{car_id}/appointment_locations")
    public @ResponseBody ResponseDTO locationAppointments(@RequestParam(value = "user_latitude", required=false) String userLatitude,
                                                          @RequestParam(value = "user_longitude", required=false) String userLongitude,
                                                          @RequestParam(value = "date", required=false) String date,
                                                          @RequestParam(value = "hour_block", required=false) Long hourBlockId,
                                                          @PathVariable(value = "car_id", required=false) Long idSellCarDetail){
        return commonServices.getKavakCenters(userLatitude,userLongitude,date, hourBlockId,idSellCarDetail);
    }

    @PostMapping("api/v1/cars/{car_id}/images")
    public @ResponseBody ResponseDTO postImages(@PathVariable("car_id") String carId, @RequestBody GenericDTO genericDTO){
        return productServices.postImages(carId,genericDTO.getUrlImages());
    }

    @PostMapping("api/v1/cars/{car_id}/dimples")
    public @ResponseBody ResponseDTO postDimplesCar(@PathVariable("car_id") String carId, @RequestBody PostDimplesCarRequestDTO postDimplesCarRequestDTO){
        return productServices.postDimplesCar(carId,postDimplesCarRequestDTO);
    }
}