package com.kavak.core.controller;

import java.util.List;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.PostInspectionPointsDetailRequestDTO;
import com.kavak.core.dto.request.PutInspectionOverviewRequestDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.CommonServices;
import com.kavak.core.service.InspectionScheduleServices;
import com.kavak.core.service.InspectionServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class InspectionController {

	@EJB(mappedName = LookUpNames.SESSION_InspectionServices)
	private InspectionServices inspectionServices;

	@EJB(mappedName = LookUpNames.SESSION_InspectionScheduleServices)
	private InspectionScheduleServices inspectionScheduleServices;

	@EJB(mappedName = LookUpNames.SESSION_CommonServices)
	private CommonServices commonServices;

	@GetMapping("/api/v1/inspections/overviews")
	public @ResponseBody ResponseDTO overviews(@RequestParam("ids") List<String> idsOfferCheckpoint){
		return inspectionServices.getInspectionsOverviews(idsOfferCheckpoint);
	}

	@PutMapping("/api/v1/inspections/overviews")
	public @ResponseBody ResponseDTO overviews(@RequestBody PutInspectionOverviewRequestDTO putInspectionOverviewRequestDTO){
		return inspectionServices.putInspectionOverview(putInspectionOverviewRequestDTO);
	}

	@GetMapping("/api/v1/inspections/inspectionpoints/categories")
	public @ResponseBody ResponseDTO inspectionsCategories(@RequestParam(value = "inspection_type_id", required=false) Long inspectionTypeId,
							       @RequestParam(value = "inspection_id", required=false) Long inspectionId){
		return  inspectionServices.getInspectionPointsCategories(inspectionTypeId,inspectionId);
	}

	@GetMapping("/api/v1/inspections/inspectionpoints/dimples")
	public @ResponseBody ResponseDTO dimples(){
		return inspectionServices.getDimples();
	}

	@DeleteMapping("/api/v1/inspections/inspectionschedules/{id}")
	public @ResponseBody ResponseDTO deleteInspectionSchedule(@PathVariable("id") Long idInspectionSchedule){
		return inspectionScheduleServices.deleteInspectionSchedule(idInspectionSchedule);
	}

	@GetMapping("/api/v1/inspections/repairs/categories")
	public @ResponseBody ResponseDTO repairsCategories(){
		return inspectionServices.repairsCategories();
	}

	@GetMapping("/api/v1/inspections/scheduleblocks")
	public @ResponseBody ResponseDTO scheduleBlocks(@RequestParam("date_from") String dateFrom,
							@RequestParam(value = "inspection_location_id", required=false) Long idInspectionLocation){
		return inspectionScheduleServices.getScheduleBlocks(dateFrom,idInspectionLocation);
	}

	@PostMapping("/api/v1/inspections/inspectionpoints")
	public @ResponseBody ResponseDTO inspectionpoints(@RequestBody PostInspectionPointsDetailRequestDTO inspectionpointRequestDTO){
		return inspectionServices.postInspectionPointsDetail(inspectionpointRequestDTO);
	}

	@GetMapping("/api/v1/inspections/inspectioncenters")
	public @ResponseBody ResponseDTO inspectionCenters(@RequestParam(value = "user_latitude", required=false) String userLatitude,
	                                                   @RequestParam(value = "user_longitude", required=false) String userLongitude,
						           @RequestParam(value = "date", required=false) String date,
							   @RequestParam(value = "hour_block", required=false) Long hourBlockId){
		return commonServices.getKavakCenters(userLatitude, userLongitude, date, hourBlockId,null);
	}

	@GetMapping("/api/v1/inspections/carphototypes")
	public @ResponseBody ResponseDTO carphotoTypes(){
		return inspectionServices.getCarPhotoTypes();
	}

	@GetMapping("/api/v1/inspections/features")
	public @ResponseBody ResponseDTO features(){
		return inspectionServices.getFeatures();
	}

	/*@PutMapping("/api/v1/inspections/summary")
	public @ResponseBody ResponseDTO summary(@RequestBody PutSummaryRequesDTO putSummaryRequesDTO ){
		return inspectionServices.putSummary(putSummaryRequesDTO);
	}*/

	@GetMapping("/api/v1/inspections/cancellationreasons")
	public @ResponseBody ResponseDTO cancellationReasons(){
		return inspectionServices.getCancellationReasons();
	}

	@GetMapping("/api/v1/inspections/discounttypes")
	public @ResponseBody ResponseDTO discountTypes(){
		return inspectionServices.getDiscountTypes();
	}

	@GetMapping("/api/v1/inspections/{id}/summary")
	public @ResponseBody ResponseDTO summary(@PathVariable("id") Long id,
						 @RequestParam(value = "inspection_type_id") Long inspectionTypeId  ){
		return inspectionServices.getSummary(id, inspectionTypeId);
	}

}