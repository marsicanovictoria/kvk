package com.kavak.core.controller.nesuite;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.netsuite.ArticleServices;
import com.kavak.core.util.LookUpNames;
import org.springframework.web.bind.annotation.*;

import javax.ejb.EJB;

@RestController
public class ArticleController {

	@EJB(mappedName = LookUpNames.SESSION_ArticleServices)
	private ArticleServices articleServices;
	
	
	//Obtiene los datos de un articulo por su id
	@GetMapping("/netsuite/api/v1/article/{id}")
	public @ResponseBody ResponseDTO customerById(@PathVariable("id") Long articleId){
		return articleServices.getArticleById(articleId);
	}
	
	//Obtiene los datos de un articulo por su sku
	@GetMapping("/netsuite/api/v1/article")
	public @ResponseBody ResponseDTO customerByEmail(@RequestParam String sku){
		return articleServices.getArticleBySku(sku);
	}
	
}
