package com.kavak.core.controller.nesuite;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.netsuite.InspectionsServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class InspectionsController {

	@EJB(mappedName = LookUpNames.SESSION_InspectionsNetsuite)
	private InspectionsServices inspectionsServices;
	
	//Obtiene todos los nuevos Sales Opportunities 
	@GetMapping("/netsuite/api/v1/inspectionsfornetsuite/")
	public @ResponseBody ResponseDTO inspectionsForNetsuite(){
		return inspectionsServices.getInspectionsForNetsuite();
	}
	
	@PutMapping("/netsuite/api/v1/inspectionsfornetsuite")
	public @ResponseBody ResponseDTO sentPurchaseOpportunities(@RequestParam String inspectionsIds){
		return inspectionsServices.putSendStatusInspection(inspectionsIds);
	}
}
