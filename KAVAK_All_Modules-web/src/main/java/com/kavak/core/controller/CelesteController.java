package com.kavak.core.controller;

import java.util.List;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.PurchaseInternalEmailDTO;
import com.kavak.core.dto.netsuite.ConfirmedInspectionNetsuiteDTO;
import com.kavak.core.dto.netsuite.PaperworkCustomerEmailDTO;
import com.kavak.core.dto.netsuite.PaperworkInternalEmailDTO;
import com.kavak.core.dto.netsuite.PaperworkShippingManagerDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.CelesteServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class CelesteController {

	@EJB(mappedName = LookUpNames.SESSION_CelesteServices)
	CelesteServices celesteServices;
	
	/*********************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * 																INTERNAL EMAILS
	 * 
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 */
	
	@PostMapping("/netsuite/api/v1/purchaseinternalemail/")
	public @ResponseBody ResponseDTO sendPurchaseInternalEmail(@RequestBody List<PurchaseInternalEmailDTO> netsuiteEmailData ){
		return celesteServices.sendPurchaseInternalEmail(netsuiteEmailData);
	}
	
	@PostMapping("/netsuite/api/v1/paperworkinternalemail/")
	public @ResponseBody ResponseDTO sendPaperworkInternalEmail(@RequestBody List<PaperworkInternalEmailDTO> paperworkEmailData ){
		return celesteServices.sendPaperworkInternalEmail(paperworkEmailData);
	}
	
	
	@PostMapping("/api/v1/availablecarsnotificationsexcel/{id}")
	public @ResponseBody ResponseDTO sendCancelledReservationExcelEmail(@PathVariable("id") Long stockId){
		return celesteServices.sendAvailableCarsNotificationsExcelEmail(stockId);
	}
	
	
	/*********************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * 																CUSTOMER EMAILS
	 * 
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 */
	
	@PostMapping("/netsuite/api/v1/paperworkcustomeremail/")
	public @ResponseBody ResponseDTO sendPaperworkCustomerEmail(@RequestBody List<PaperworkCustomerEmailDTO> paperworkData ){
		return celesteServices.sendPaperworkCustomerEmail(paperworkData);
	}
	
	@PostMapping("/netsuite/api/v1/confirmedinspectionscustomeremail/")
	public @ResponseBody ResponseDTO sendConfirmedInspections(@RequestBody List<ConfirmedInspectionNetsuiteDTO> confirmedInspectionsList ){
		return celesteServices.sendConfirmedInspection(confirmedInspectionsList);
	}

	
	/*********************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * 																SHIPPING MANAGER EMAILS
	 * 
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 * *******************************************************************************************************************************************************************
	 */
	
	@PostMapping("/netsuite/api/v1/newpaperworkshippingmanageremail/")
	public @ResponseBody ResponseDTO sendNewPaperworkShippingManagerEmail(@RequestBody List<PaperworkShippingManagerDTO> paperworkDataList){
		return celesteServices.sendNewPaperworkShippingManagerEmail(paperworkDataList);
	}
	
	
	@PostMapping("/netsuite/api/v1/delaypaperworkshippingmanageremail/")
	public @ResponseBody ResponseDTO sendDelayPaperworkShippingManagerEmail(@RequestBody List<PaperworkShippingManagerDTO> paperworkDataList){
		return celesteServices.sendDelayPaperworkShippingManagerEmail(paperworkDataList);
	}
	
	
}
