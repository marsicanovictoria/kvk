package com.kavak.core.controller;

import com.kavak.core.dto.request.PostCarContentOverviewRequesDTO;
import com.kavak.core.dto.request.PostCarNotificationAnswersRequestDTO;
import com.kavak.core.dto.request.PostCarNotificationRequestDTO;
import com.kavak.core.dto.request.PostDimpleRequestDTO;
import com.kavak.core.dto.request.PostPhotoRequestDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.SellCarServices;
import com.kavak.core.util.LookUpNames;
import org.springframework.web.bind.annotation.*;

import javax.ejb.EJB;
import javax.mail.MessagingException;

@RestController
public class SellCarController {

	@EJB(mappedName = LookUpNames.SESSION_SellCarServices)
	private SellCarServices sellCarServices;

	@GetMapping("/api/v1/catalogue/cars")
	public @ResponseBody ResponseDTO cars(@RequestParam(value = "page", required=false) Long page,
		                              @RequestParam(value = "items", required=false) Long items) {
		return sellCarServices.getCatalogueCars(page, items);
	}

	@GetMapping("/api/v1/catalogue/filters")
	public @ResponseBody ResponseDTO filters() {
		return sellCarServices.getFilters();
	}

	@PostMapping("/api/v1/catalogue/car/photo")
	public @ResponseBody ResponseDTO photo(@RequestBody PostPhotoRequestDTO postPhotoRequestDTO) {
		return sellCarServices.postPhoto(postPhotoRequestDTO);
	}

	@PostMapping("/api/v1/catalogue/car/dimple")
	public @ResponseBody ResponseDTO dimple(@RequestBody PostDimpleRequestDTO postPhotoRequestDTO) {
		return sellCarServices.postDimple(postPhotoRequestDTO);
	}

	@PostMapping("/api/v1/catalogue/car/overview")
	public @ResponseBody ResponseDTO overview(
			@RequestBody PostCarContentOverviewRequesDTO postCarContentOverviewRequesDTO) {
		return sellCarServices.postCarContentOverview(postCarContentOverviewRequesDTO);
	}

	@GetMapping("/api/v1/catalogue/warranty/{type}/detail")
	public @ResponseBody ResponseDTO warranty(@PathVariable("type") String type) {
		return sellCarServices.getWarranty(type);
	}

	@PostMapping("/api/v1/catalogue/car/notification")
	public @ResponseBody ResponseDTO postCarNotification(
			@RequestBody PostCarNotificationRequestDTO postCarNotificationRequestDTO) throws MessagingException {
		return sellCarServices.postCarNotification(postCarNotificationRequestDTO);
	}

	@GetMapping("/api/v1/catalogue/car/notification")
	public @ResponseBody ResponseDTO getCarNotification(@RequestParam Long user_id, @RequestParam Long car_id,
			@RequestParam String notification_type) {
		return sellCarServices.getCarNotification(user_id, car_id, notification_type);
	}
	
	@GetMapping("/api/v1/catalogue/car/notification/questions")
	public @ResponseBody ResponseDTO cars(@RequestParam(value = "notification_type", required=false) String notification_type) {
		return sellCarServices.getCarNotificationQuestionsByNotificationType(notification_type);
	}
	
	@PostMapping("/api/v1/catalogue/car/notification/answers")
	public @ResponseBody ResponseDTO postCarNotificationAnswers(
			@RequestBody PostCarNotificationAnswersRequestDTO postCarNotificationAnswersRequestDTO) throws MessagingException {
		return sellCarServices.postCarNotificationAnswers(postCarNotificationAnswersRequestDTO);
	}
}