package com.kavak.core.controller.nesuite;

import com.kavak.core.dto.UserDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.netsuite.CustomerServices;
import com.kavak.core.util.LookUpNames;
import org.springframework.web.bind.annotation.*;

import javax.ejb.EJB;
import java.util.List;

@RestController
public class CustomerController {

	@EJB(mappedName = LookUpNames.SESSION_CustomerServices)
	private CustomerServices userServices;
	
	//Registro de un nuevo usuario para Kavak
	@PostMapping("/netsuite/api/v1/customer/")
	public @ResponseBody ResponseDTO newCustomer(@RequestBody UserDTO userData ){
		return userServices.postNewCustomer(userData);
	}
	
	//Obtiene los datos de un cliente por su id
	@GetMapping("/netsuite/api/v1/customer/{id}")
	public @ResponseBody ResponseDTO customerById(@PathVariable("id") Long userId){
		return userServices.getCustomerById(userId);
	}
	
	//Obtiene los datos de un cliente por su email
	@GetMapping("/netsuite/api/v1/customer")
	public @ResponseBody ResponseDTO customerByEmail(@RequestParam String emailUser){
		return userServices.getCustomerByEmail(emailUser);
	}
	
	//Obtiene los nuevos clientes para netsuite
	@GetMapping("/netsuite/api/v1/customers")
	public @ResponseBody ResponseDTO newCustomersNetsuite(){
		return userServices.getNewUsersNetsuite();
	}
	
	//Actualiza los estatus de envio a netsuite de un(os) cliente(s)
	@PutMapping("/netsuite/api/v1/netsuiteuserssent")
	public @ResponseBody ResponseDTO sentNetsuiteUsers(@RequestParam List<String> usersIds){
		return userServices.putSentNetsuiteUsers(usersIds);
	}
}
