package com.kavak.core.controller.nesuite;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.netsuite.InventoryServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class InventoryController {

	@EJB(mappedName = LookUpNames.SESSION_InventoryServices)
	private InventoryServices inventoryServices;
	
	@GetMapping("/netsuite/api/v1/inventory/")
	public @ResponseBody ResponseDTO inventoryCars(){
		return inventoryServices.getInventoryCars();
	}
	
	@GetMapping("/netsuite/api/v1/lastscheduledappointment/{id}")
	public @ResponseBody ResponseDTO lastscheduledappointment(@PathVariable("id") Long stockId){
		return inventoryServices.getLastScheduledAppointment(stockId);
	}
	
	//Actualiza el status de envío de autos del inventario
	@PutMapping("/netsuite/api/v1/inventory")
	public @ResponseBody ResponseDTO sendStatusInventory(@RequestParam String inventoryIds){
		return inventoryServices.putSendStatusInventory(inventoryIds);
	}
	
	@GetMapping("/netsuite/api/v1/matchmakeandmodel/{keyword}")
	public @ResponseBody ResponseDTO matchMakeAndModel(@PathVariable("keyword") String keyWord){
		return inventoryServices.matchMakeAndModel(keyWord);
	}
}
