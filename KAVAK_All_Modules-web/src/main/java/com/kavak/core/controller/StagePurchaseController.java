package com.kavak.core.controller;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.StagePurchaseServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class StagePurchaseController {

	@EJB(mappedName = LookUpNames.SESSION_StagePurchaseOpportunities)
	StagePurchaseServices stagePurchaseServices;
	
	/**
	 * Retrieve Purchase Opportunities at a first stage
	 * 
	 * @return ResponseDTO 
	 */
	
	@GetMapping("/netsuite/api/v1/firststagepurchaseopportunities/")
	public @ResponseBody ResponseDTO firstStagePurchaseOpportunities(){
		return stagePurchaseServices.getFirstStagePurchaseOpportunities();
	}
	
	/**
	 * Retrieve Purchase Opportunities at a second stage
	 * 
	 * @return ResponseDTO 
	 */
	
	@GetMapping("/netsuite/api/v1/secondstagepurchaseopportunities/")
	public @ResponseBody ResponseDTO secondStagePurchaseOpportunities(){
		return stagePurchaseServices.getSecondStagePurchaseOpportunities();
	}
	
	/**
	 * Retrieve Purchase Opportunities at a third stage
	 * 
	 * @return ResponseDTO 
	 */
	
	@GetMapping("/netsuite/api/v1/thirdstagepurchaseopportunities/")
	public @ResponseBody ResponseDTO thirdStagePurchaseOpportunities(){
		return stagePurchaseServices.getThirdStagePurchaseOpportunities();
	}
	
	/**
	 * Retrieve Purchase Opportunities at a fourth stage
	 * 
	 * @return ResponseDTO 
	 */
	
	@GetMapping("/netsuite/api/v1/fourthstagepurchaseopportunities/")
	public @ResponseBody ResponseDTO fourthStagePurchaseOpportunities(){
		return stagePurchaseServices.getFourthStagePurchaseOpportunities();
	}
	
	/**
	 * Retrieve Purchase Opportunities at a fourth stage without an appointment schedule
	 * 
	 * @return ResponseDTO 
	 */
	
	@GetMapping("/netsuite/api/v1/incompletefourthstagepurchaseopportunities/")
	public @ResponseBody ResponseDTO incompleteFourthStagePurchaseOpportunities(){
		return stagePurchaseServices.getIncompleteFourthStagePurchaseOpportunities();
	}
	
	/**
	 * Retrieve Purchase Opportunities at a fifth stage
	 * 
	 * @return ResponseDTO 
	 */
	
	@GetMapping("/netsuite/api/v1/fifthstagepurchaseopportunities/")
	public @ResponseBody ResponseDTO fifthStagePurchaseOpportunities(){
		return stagePurchaseServices.getFifthStagePurchaseOpportunities();
	}
	
	/**
	 * Retrieve Purchase Opportunities at a fifth stage without an appointment schedule
	 * 
	 * @return ResponseDTO 
	 */
	
	@GetMapping("/netsuite/api/v1/incompletefifthstagepurchaseopportunities/")
	public @ResponseBody ResponseDTO incompleteFifthStagePurchaseOpportunities(){
		return stagePurchaseServices.getIncompleteFifthStagePurchaseOpportunities();
	}
}
