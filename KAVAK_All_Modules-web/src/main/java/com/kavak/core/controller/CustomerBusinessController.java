package com.kavak.core.controller;

import javax.ejb.EJB;

import com.kavak.core.dto.request.PostCustomerOpportunityCommentRequestDTO;
import com.kavak.core.service.CustomerBusinessServices;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.CustomerReviewDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.util.LookUpNames;

@RestController
public class CustomerBusinessController {

	@EJB(mappedName = LookUpNames.SESSION_CustomerBusinessServices)
	CustomerBusinessServices customerBusinessServices;
	
	@PostMapping("/api/v1/customerreview")
	public @ResponseBody ResponseDTO postCustomerReview(@RequestBody CustomerReviewDTO customerReview ){
		return customerBusinessServices.setCustomerReview(customerReview);
	}

	@PostMapping("/api/v1/customer/comment")
	public @ResponseBody ResponseDTO postCustomerOpportunityComment(@RequestBody PostCustomerOpportunityCommentRequestDTO postCustomerOpportunityCommentRequestDTO){
		return customerBusinessServices.postCustomerOpportunityComment(postCustomerOpportunityCommentRequestDTO);
	}
}
