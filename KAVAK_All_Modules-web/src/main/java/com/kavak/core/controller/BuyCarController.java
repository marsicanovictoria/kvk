package com.kavak.core.controller;

import com.kavak.core.dto.request.PostOfferRequestDTO;
import com.kavak.core.dto.request.PutOfferRequestDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.BuyCarServices;
import com.kavak.core.util.LookUpNames;
import org.springframework.web.bind.annotation.*;

import javax.ejb.EJB;

@RestController
public class BuyCarController {

	@EJB(mappedName = LookUpNames.SESSION_BuyCarServices)
    private BuyCarServices buyCarServices;
	
	@GetMapping("/api/v1/cars/years")
	public @ResponseBody ResponseDTO years(){
		return buyCarServices.getCarYears();
	}

	@GetMapping("/api/v1/cars/makes")
	public @ResponseBody ResponseDTO makes(@RequestParam String year){
		return buyCarServices.getCarMake(Long.valueOf(year));
	}

	@GetMapping("/api/v1/cars/models")
	public @ResponseBody ResponseDTO models(@RequestParam String year, @RequestParam String make){
		return buyCarServices.getCarModel(Long.valueOf(year), make);
	}

	@GetMapping("/api/v1/cars/trims")
	public @ResponseBody ResponseDTO trims(@RequestParam String year, @RequestParam String make , @RequestParam String model){
		return buyCarServices.getCarTrims(Long.valueOf(year), make, model);
	}
	
	@PostMapping("/api/v1/cars/offers")
	public @ResponseBody ResponseDTO offers(@RequestBody PostOfferRequestDTO postOfferRequestDTO ){
	    postOfferRequestDTO.setVersion(1L);
		return buyCarServices.postOffers(postOfferRequestDTO);
	}
	
	@PostMapping("/api/v2/cars/offers/generate-by-token")
	public @ResponseBody ResponseDTO offer(@RequestBody PostOfferRequestDTO postOfferRequestDTO ){
	    postOfferRequestDTO.setVersion(2L);
		return buyCarServices.postOffers(postOfferRequestDTO);
	}
	
	@PutMapping("/api/v1/cars/offers/{id}")
	public @ResponseBody ResponseDTO offers(@PathVariable("id") Long idOfferCheckpoint, @RequestBody PutOfferRequestDTO putOfferRequestDTO){
		return buyCarServices.putOffers(idOfferCheckpoint,putOfferRequestDTO);
	}
	
	@GetMapping("/api/v1/cars/offer_by_hash/{hash}")
	public @ResponseBody ResponseDTO getOfferByHash(@PathVariable("hash") String hash){
		return buyCarServices.getOfferByHash(hash);
	}
	
}