package com.kavak.core.controller;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.InsuranceServices;
import com.kavak.core.util.LookUpNames;
import org.springframework.web.bind.annotation.*;

import javax.ejb.EJB;

@RestController
public class InsuranceController {


	@EJB(mappedName = LookUpNames.SESSION_InsuranceServices)
	private InsuranceServices insuranceServices;

	@GetMapping("/api/v1/insurances/detail")
	public @ResponseBody ResponseDTO detail(@RequestParam("name") String name,@RequestParam("type") String type){
		return insuranceServices.getInsuranceDetail(name,type);
	}

}