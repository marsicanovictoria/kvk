package com.kavak.core.controller;


import javax.ejb.EJB;
import javax.mail.MessagingException;

import org.springframework.web.bind.annotation.*;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.dto.response.UserRequestDTO;
import com.kavak.core.service.UserServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class UserController {

	@EJB(mappedName = LookUpNames.SESSION_UserServices)
	private UserServices userServices;

	@PostMapping("/api/v1/user")
	public @ResponseBody ResponseDTO RegisterUser(@RequestBody UserRequestDTO userRequestDTO) throws MessagingException {
		userRequestDTO.setVersion(1L);
		return userServices.switcherLoginUser(userRequestDTO);

	}

	@PostMapping("/api/v2/user")
	public @ResponseBody ResponseDTO RegisterUserV2(@RequestBody UserRequestDTO userRequestDTO) throws MessagingException {
		userRequestDTO.setVersion(2L);
		return userServices.switcherLoginUser(userRequestDTO);

	}

	@PostMapping("/api/v1/user/login")
	public @ResponseBody ResponseDTO LoginUser(@RequestBody UserRequestDTO userRequestDTO ) {
		return userServices.loginUser(userRequestDTO);
	}

	@PutMapping("/api/v1/user")
	public @ResponseBody ResponseDTO putUser(@RequestBody UserRequestDTO userRequestDTO ) throws MessagingException {
		return userServices.putUser(userRequestDTO);
	}

	@GetMapping("/api/v1/user/recoverpassword")
	public @ResponseBody
	ResponseDTO getRecoverPassword(@RequestParam("user_email") String userEmail) throws MessagingException {
		return userServices.getRecoverPassword(userEmail);
	}

	@GetMapping("/api/v1/user/recoverpassword/validate")
	public @ResponseBody
	ResponseDTO getValidateRecoverPassword(@RequestParam("token") String token, @RequestParam("id_sha1") String idSha1) {
		return userServices.getValidateRecoverPassword(token, idSha1);
	}
}
