package com.kavak.core.controller.nesuite;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.netsuite.SaleOpportunityServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class SaleOpportunityController {

	@EJB(mappedName = LookUpNames.SESSION_SaleOpportunityServices)
	private SaleOpportunityServices saleOpportunityServices;
	
	//Obtiene todos los nuevos Sales Opportunities 
	@GetMapping("/netsuite/api/v1/salesopportunities/")
	public @ResponseBody ResponseDTO saleOpportunity(){
		return saleOpportunityServices.getNewSalesOpportunities();
	}
	
	//Actualiza el status de envío de los Sales Opportunities que ya fueron enviados a Netsuite previamente 
	
	 @PutMapping("/netsuite/api/v1/salesopportunities")
	 public @ResponseBody ResponseDTO sentSalesOpportunities(@RequestParam String saleOpportunitiesIds){
		return saleOpportunityServices.putSentSalesOpportunities(saleOpportunitiesIds);
	}
	
	/*
	//Actualiza el status de envío de los Sales Opportunities pasados los 15 minutos de haberse registrado
	@PutMapping("/netsuite/api/v1/salesopportunities")
	 public @ResponseBody ResponseDTO sentSalesOpportunities(){
		return saleOpportunityServices.putSentSalesOpportunities();http://localhost:8082/businesscore/netsuite/api/v1/salesopportunities/
	}
	*/
	//Obtiene los checkpoints que no poseen ID de netsuite 
	@GetMapping("/netsuite/api/v1/saleopportunitieswithoutnetsuiteid/")
	public @ResponseBody ResponseDTO purchaseopportunitieswithoutnetsuiteid(){
		return saleOpportunityServices.getSaleOpportunityWithoutNetsuiteId();
	}
	
	//Actualiza el campo netsuite id en la tabla oferta_checkpoint obtenido en netsuite. 
	@PutMapping("/netsuite/api/v1/saleopportunitieswithoutnetsuiteid")
	public @ResponseBody ResponseDTO putNetsuiteOpportunityIdIntoMinerva(@RequestParam String netsuiteMinervaIds){
		return saleOpportunityServices.putNetsuiteOpportunityIdIntoMinerva(netsuiteMinervaIds);
	}
	
	//Conecta con netsuite. 
	@PostMapping("/netsuite/api/v1/salesopportunitiesauthnetsuite")
	public @ResponseBody ResponseDTO salesOpportunitiesAuthNetsuite(){
		return saleOpportunityServices.salesOpportunitiesAuthNetsuite();
	}
	
}
