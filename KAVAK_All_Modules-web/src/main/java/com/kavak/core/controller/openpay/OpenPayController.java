package com.kavak.core.controller.openpay;

import com.kavak.core.dto.openpay.OpenPayNotificationRequestDTO;
import com.kavak.core.dto.openpay.VerificationDTO;
import com.kavak.core.service.openpay.OpenPayTransactionServices;
import com.kavak.core.util.Constants;
import com.kavak.core.util.LogService;
import com.kavak.core.util.LookUpNames;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import java.io.IOException;

@RestController
public class OpenPayController {

	@EJB(mappedName = LookUpNames.SESSION_OpenPayTransactionServices)
    private OpenPayTransactionServices openPayTransactionServices;

	@PostMapping("/openpayNotification")
	protected void openpayNotification(@RequestBody OpenPayNotificationRequestDTO openPayNotificationDTO) throws ServletException, IOException {
		LogService.logger.info("Invocando WebHook OpenPayServlet[openpayNotification] " );
		LogService.logger.info("Tipo de Operacion: " + openPayNotificationDTO.getType() );
		// Metodo usado para que OpenPay verifique la estructura de nuestros EndPoints
		if(openPayNotificationDTO.getType().equalsIgnoreCase(Constants.VERIFICATION)){
			VerificationDTO verificationDTO  = new VerificationDTO();
			verificationDTO.setType(openPayNotificationDTO.getType());
			verificationDTO.setEventDate(openPayNotificationDTO.getEvenDate());
			verificationDTO.setVerificationCode(openPayNotificationDTO.getVerificationCode());
			LogService.logger.info("Parametros verificationDTO [verificationCode: " + verificationDTO.getVerificationCode()+"]" + " [eventDate: "+ verificationDTO.getEventDate() +"]" );
//			openPayTransactionServices.doVerification(verificationDTO);
		}

		// Metodo usado Notificcar que el cargo de la transaction se ha realizado exitosamente
//		if(openPayNotificationDTO.getType().equalsIgnoreCase(Constants.CHARGESUCCEEDED)){
//			openPayTransactionServices.doChargeSucceded();
//		}
	}
}
