package com.kavak.core.controller;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.request.PostCarscoutAlertRequestDTO;
import com.kavak.core.dto.request.PostCarscoutFilterCoincidencesRequestDTO;
import com.kavak.core.dto.request.PostCarscoutQuestionsAnswersRequestDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.CarscoutServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class CarscoutController {

	@EJB(mappedName = LookUpNames.SESSION_CarscoutServices)
	private CarscoutServices carscoutServices;

	@GetMapping("/api/v1/carscout/filtervalues")
	public @ResponseBody ResponseDTO getCarscoutFilterValues() {
		return carscoutServices.getCarscoutFilterValues();

	}

	@GetMapping("/api/v1/carscout/filtervalues/modelversion")
	public @ResponseBody ResponseDTO getCarscoutModelVersionFilterValues(@RequestParam("search") String search) {
		return carscoutServices.getCarscoutModelVersionFilterValues(search);

	}

	@PostMapping("/api/v1/carscout/filtercount")
	public @ResponseBody ResponseDTO postCarscoutFilterCoincidences(
			@RequestBody PostCarscoutFilterCoincidencesRequestDTO postCarscoutFilterCoincidencesRequestDTO) {
		return carscoutServices.getCarscoutFilterCoincidences(postCarscoutFilterCoincidencesRequestDTO);

	}

	@PostMapping("/api/v1/carscout/alert")
	public @ResponseBody ResponseDTO postCarscoutAlert(
			@RequestBody PostCarscoutAlertRequestDTO postCarscoutAlertRequestDTO) {
		return carscoutServices.postCarscoutAlert(postCarscoutAlertRequestDTO);

	}

	@GetMapping("/api/v1/carscout/alert")
	public @ResponseBody ResponseDTO getCarscoutAlert(@RequestParam("user_id") Long userId,
							  @RequestParam(value = "alert_id", required=false) Long alertId){
		return carscoutServices.getCarscoutAlert(userId, alertId);

	}

	@DeleteMapping("/api/v1/carscout/alert")
	public @ResponseBody ResponseDTO deleteCarscoutAlert(@RequestParam("user_id") Long userId,
														 @RequestParam("alert_id") Long alertId) {
		return carscoutServices.deleteCarscoutAlert(userId, alertId);
	}

	@GetMapping("/api/v1/carscout/results")
	public @ResponseBody ResponseDTO getCarscoutResults(@RequestParam("alert_id") Long alertId, @RequestParam(value = "page", required=false) Long page,
														@RequestParam(value = "items", required=false) Long items) {
		return carscoutServices.getCarscoutResults(alertId,page,items);

	}

	@GetMapping("/api/v1/carscout/notify")
	public @ResponseBody ResponseDTO getCarscoutNotify(@RequestParam("user_id") Long userId) {
		return carscoutServices.getCarscoutNotify(userId);

	}

	@PutMapping("/api/v1/carscout/notify")
	public @ResponseBody ResponseDTO putCarscoutNotify(@RequestParam("alert_id") Long alertId,
													   @RequestParam("catalogue_id") Long catalogueId) {
		return carscoutServices.putCarscoutNotify(alertId, catalogueId);

	}

	@DeleteMapping("/api/v1/carscout/notify")
	public @ResponseBody ResponseDTO deleteCarscoutNotify(@RequestParam("user_id") Long userId,
														  @RequestParam("id_carscout_notify") Long idCarscoutNotify) {
		return carscoutServices.deleteCarscoutNotify(userId, idCarscoutNotify);

	}
	
	@GetMapping("/api/v1/carscout/questions")
	public @ResponseBody ResponseDTO getCarscoutQuestions(@RequestParam("multiple") String multiple) {
		return carscoutServices.getCarscoutQuestions(multiple);
	}
	
	@PostMapping("/api/v1/carscout/questions/answers")
	public @ResponseBody ResponseDTO postCarscoutQuestionAnswers(
			@RequestBody PostCarscoutQuestionsAnswersRequestDTO postCarscoutQuestionsAnswers) {
		return carscoutServices.postCarscoutQuestionAnswers(postCarscoutQuestionsAnswers);

	}
	
	

}
