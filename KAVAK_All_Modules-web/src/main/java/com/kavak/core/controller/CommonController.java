package com.kavak.core.controller;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.request.PostCarscoutFilterCoincidencesRequestDTO;
import com.kavak.core.dto.request.PostContactRequestDTO;
import com.kavak.core.dto.PostMessageRequestDTO;
import com.kavak.core.dto.response.ApiUrlShortenerResponseDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.CommonServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class CommonController {

	@EJB(mappedName = LookUpNames.SESSION_CommonServices)
	private CommonServices commonServices;

	@GetMapping("/api/v1/commons/colonies/{zipCode}")
	public @ResponseBody
	ResponseDTO colonies(@PathVariable("zipCode") String zipCode) {
		return commonServices.getColonies(zipCode);
	}

	@GetMapping("/api/v1/commons/apiGoogleUrlShortener")
	public @ResponseBody ApiUrlShortenerResponseDTO UrlShortener(@RequestParam(value = "url", required=true) String url) {
		return commonServices.postApiUrlShortener(url);
	}
	@GetMapping("/api/v1/commons/contactKavak")
	public @ResponseBody
	ResponseDTO getContactKavak() {
		return commonServices.getContactKavak();

	}
	
	@GetMapping("/api/v1/commons/testimonials")
	public @ResponseBody ResponseDTO Testimonials(@RequestParam(value = "items", required=false) Long items) {
		return commonServices.getTestimonials(items);
	}
	
	@GetMapping("/api/v1/commons/pressnotes")
	public @ResponseBody
	ResponseDTO getPressNotes() {
		return commonServices.getPressNotes();
	}
	
	@GetMapping("/api/v1/commons/contact/subjects")
	public @ResponseBody
	ResponseDTO getSubjects() {
		return commonServices.getContactSubjects();
	}
	
	@PostMapping("/api/v1/commons/contact")
	public @ResponseBody ResponseDTO postContact(
			@RequestBody PostContactRequestDTO postContactRequestDTO) {
		return commonServices.postContact(postContactRequestDTO);
	}
	
	@GetMapping("/api/v1/commons/faqs")
	public @ResponseBody ResponseDTO TestimonialsListResponse(@RequestParam(value = "type", required=false) String type) {
		return commonServices.getTestimonialsList(type);
	}
	
		
	@PutMapping("/api/v1/commons/message/send")
	public @ResponseBody ResponseDTO putCommunicationsTraySms() {
	    return commonServices.putCommunicationsTraySms();
	}

	@PostMapping("/api/v1/commons/message")
	public @ResponseBody ResponseDTO postMessage(@RequestBody PostMessageRequestDTO postMessageDTO) {
	    return commonServices.postMessage(postMessageDTO);
	}
	
}