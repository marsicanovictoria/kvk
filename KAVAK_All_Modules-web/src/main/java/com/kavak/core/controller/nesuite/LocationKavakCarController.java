package com.kavak.core.controller.nesuite;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.netsuite.LocationKavakCarServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class LocationKavakCarController {

	@EJB(mappedName = LookUpNames.SESSION_LocationKavakCarServices)
	private LocationKavakCarServices locationKavakServices;
	
	//Obtiene los datos de un articulo por su id
	@GetMapping("/netsuite/api/v1/locationkavakcar/{id}")
	public @ResponseBody ResponseDTO locationKavakCarByStockId(@PathVariable("id") Long stockId){
		return locationKavakServices.getLocationKavakCar(stockId);
	}
	
	//Actualiza el status de envío de autos del inventario
	
	@PutMapping("/netsuite/api/v1/putlocationkavakcar/{stockids}/{location}/{locationdetail}")
	public @ResponseBody ResponseDTO updateLocationKavakCar(@PathVariable("stockids") String stockId, @PathVariable("location") Long location, @PathVariable("locationdetail") String locationDetail){
		return locationKavakServices.putLocationKavakCar(stockId, location, locationDetail);
	}
	
}
