package com.kavak.core.controller;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.financing.request.FinancingUserDomicileRequestDTO;
import com.kavak.core.dto.financing.request.FinancingUserIncomeRequestDTO;
import com.kavak.core.dto.financing.request.FinancingUserScoreRequestDTO;
import com.kavak.core.dto.financing.request.FinancingVerifyUserInformationRequestDTO;
import com.kavak.core.dto.request.PostFinancingUserPinCodeAuthorizationRequestDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.FinancingServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class FinancingController {

    @EJB(mappedName = LookUpNames.SESSION_FinancingServices)
    private FinancingServices financingServices;


    @GetMapping("/api/v1/financing/user/{id}/exist-profile")
    public @ResponseBody ResponseDTO existprofile(@PathVariable("id") Long id){
	return financingServices.getFinancingUserProfileExist(id);
    }

    @GetMapping("/api/v1/financing/user/profiles")
    public @ResponseBody ResponseDTO profiles() {
	return financingServices.getFinancingUserProfiles();
    }

    @GetMapping("/api/v1/financing/user/rfc") 
    public @ResponseBody ResponseDTO rfc(
	    				 @RequestParam(value = "father_surname", required = true) String firstSurname,
	    				 @RequestParam(value = "mother_surname", required = true) String secondSurname,
	                                 @RequestParam(value = "birthdate", required = true) String birthdate,
	                                 @RequestParam(value = "name", required = true) String name) {
	return financingServices.getFinancingUserRfcArbol(firstSurname,secondSurname,birthdate,name);
    }

    
    @GetMapping("/api/v1/financing/user/living-types")
    public @ResponseBody ResponseDTO livingtypes() {
	return financingServices.getFinancingLivingTypes();
    }
    
    @GetMapping("/api/v1/financing/user/income-profile")
    public @ResponseBody ResponseDTO incomeprofile() {
	return financingServices.getFinancingIncomeProfiles();
    }
    
    @GetMapping("/api/v1/financing/user/automotive-car-use-types")
    public @ResponseBody ResponseDTO automotivecarusetypes() {
	return financingServices.getFinancingAutomotiveCarUseTypes();
    }
    
    @GetMapping("/api/v1/financing/user/signature")
    public @ResponseBody ResponseDTO signature() {
	return financingServices.getFinancingSignature();
    }
    
    @PostMapping("/api/v1/financing/user/verify-user-information")
    public @ResponseBody ResponseDTO verifyuserinformation(@RequestBody FinancingVerifyUserInformationRequestDTO financingVerifyUserInformationRequestDTO) {
	return financingServices.postFinancingVerifyUserInformation(financingVerifyUserInformationRequestDTO);
    }
    
    @PostMapping("/api/v1/financing/user/{id}/domicile")
    public @ResponseBody ResponseDTO domicile(@PathVariable("id") Long id,
	                                      @RequestBody FinancingUserDomicileRequestDTO financingUserDomicileRequestDTO) {
	return financingServices.postFinancingUserDomicile(id, financingUserDomicileRequestDTO);
    }
    
    @PostMapping("/api/v1/financing/user/{id}/income")
    public @ResponseBody ResponseDTO income(@PathVariable("id") Long id,
	                                      @RequestBody FinancingUserIncomeRequestDTO financingUserIncomeRequestDTO) {
	return financingServices.postFinancingUserIncome(id, financingUserIncomeRequestDTO);
    }
    
    @PostMapping("/api/v1/financing/user/{id}/pincode")
    public @ResponseBody ResponseDTO pincode(@PathVariable("id") Long id) {
	return financingServices.postFinancingUserPinCode(id);
    }
    
    @PostMapping("/api/v1/financing/user/{id}/report-pincode")
    public @ResponseBody ResponseDTO reportpincode(@PathVariable("id") Long id,
	    					   @RequestBody PostFinancingUserPinCodeAuthorizationRequestDTO postFinancingUserPinCodeAuthorizationRequestDTO) {
	return financingServices.postFinancingUserPinCodeAuthorization(id, postFinancingUserPinCodeAuthorizationRequestDTO);
    }
    
    @PostMapping("/api/v1/financing/user/{id}/escore")
    public @ResponseBody ResponseDTO escore(@PathVariable("id") Long id,
	     				    @RequestBody FinancingUserScoreRequestDTO financingUserScoreRequestDTO) {
	return financingServices.postFinancingUserEscore(id,financingUserScoreRequestDTO);
    }
    
    @GetMapping("/api/v1/financing/user/financing-profiles")
    public @ResponseBody ResponseDTO FinancingUserProfiles(@RequestParam(value = "user_id", required = true) Long userId) {
	return financingServices.getFinancingUserProfiles(userId);
    }
    
    @GetMapping("/api/v1/financing/user/{id}/financing-profile")
    public @ResponseBody ResponseDTO FinancingUser(@PathVariable("id") Long id, @RequestParam(value = "financing_id", required = true) Long userId) {
	return financingServices.getFinancingUser(id, userId);
    }

}