package com.kavak.core.controller;

import javax.ejb.EJB;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.GenerateExcelFileServices;
import com.kavak.core.util.LookUpNames;

@RestController
public class GenerateExcelFileController {

    @EJB(mappedName = LookUpNames.SESSION_GenerateExcelFileServices)
    private GenerateExcelFileServices generateExcelFileServices;
    
	@GetMapping("/api/v1/excel/availablecarsnotificationsfile/{carid}")
    public @ResponseBody ResponseDTO postNotificationToken(@PathVariable("carid") Long carId) {
        return generateExcelFileServices.generateAvailableCarsNotificationExcel(carId);
    }
	
}
