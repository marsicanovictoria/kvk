package com.kavak.core.controller;

import com.kavak.core.dto.request.PostAppNotificationTokenRequestDTO;
import com.kavak.core.dto.request.PutAppNotificationSettingRequestDTO;
import com.kavak.core.dto.response.ResponseDTO;
import com.kavak.core.service.AppNotificationServices;
import com.kavak.core.util.LookUpNames;
import org.springframework.web.bind.annotation.*;

import javax.ejb.EJB;

@RestController
public class AppNotificationController {

	@EJB(mappedName = LookUpNames.SESSION_AppNotificationServices)
    private AppNotificationServices appNotificationServices;
	
	
	@PostMapping("/api/v1/app/notification/token")
	public @ResponseBody ResponseDTO postNotificationToken(@RequestBody PostAppNotificationTokenRequestDTO postAppNotificationTokenRequestDTO ){
		return appNotificationServices.postAppNotificationToken(postAppNotificationTokenRequestDTO);
	}
	
	@DeleteMapping("/api/v1/app/notification/token")
	public @ResponseBody ResponseDTO deletedNotificationToken(@RequestParam(value = "user_id", required=true) Long userId,
								  @RequestParam(value = "device_id", required=true) String deviceId,
								  @RequestParam(value = "token_type", required=false) String tokenType){
		return appNotificationServices.deleteAppNotificationToken(userId, deviceId, tokenType);
	}
	
	@GetMapping("/api/v1/app/notifications/settings")
	public @ResponseBody ResponseDTO notificationSetting(@RequestParam(value = "user_id", required=true) Long userId){
		return appNotificationServices.getAppNotificationSettingsByUser(userId);
	}

	@PutMapping("/api/v1/app/notifications/settings")
	public @ResponseBody ResponseDTO putNotificationSettings(@RequestBody PutAppNotificationSettingRequestDTO putAppNotificationSettingRequestDTO){
		return appNotificationServices.putAppNotificationSettings(putAppNotificationSettingRequestDTO);
	}
	
	@GetMapping("/api/v1/app/notifications")
	public @ResponseBody ResponseDTO notification(@RequestParam(value = "user_id", required=true) Long userId){
		return appNotificationServices.getAppNotificationsByUser(userId);
	} 
	
	
	
}